#!/bin/bash
#SBATCH --nodes=1 --ntasks-per-node=1 
##--qos=devel
#SBATCH --partition=bigmem
#SBATCH --time=00-01:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=2000M
#SBATCH --mem=64GB
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
##SBATCH --array=1-366


module purge
module load NCO/4.6.6-intel-2017a

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp} 2>&1
set -x
# Set origin path
BASE=/cluster/work/users/$USER

cd ${BASE}

years="2014"

infile=/cluster/shared/arcticfjord/Annettes_files/filtered/filtered_2014/sep2014_filtered.nc
infilestrip=$(basename -- "$infile")
#ncap2 -O -s "var_tmp=aicen; where (aicen < 0.0001) var_tmp=0.0001; aicen=var_tmp; tmp_ice=vicen/aicen; tmp_snow=vsnon/aicen; tmp_vicen=vicen; tmp_vsnon=vsnon; where( tmp_ice == 0) tmp_vicen=0.0000001;  where (tmp_snow==0) tmp_vsnon=0.000000001; vicen=tmp_vicen; vsnon=tmp_vsnon; test_ice=vicen/aicen; test_snow=vsnon/aicen" $infile ${infilestrip}_san 
#ncks -O -x -v tmp_vicen,tmp_vsnon,var_tmp,tmp_ice,tmp_snow,test_ice,test_snow ${infilestrip}_san  ${infilestrip}_full
source ~/.bashrc

python ~/pytools_git/bry_from_topaz.py -i ${infilestrip}_full


set +x
exit
