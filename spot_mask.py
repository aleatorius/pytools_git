#!/usr/bin/env python
##!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm
import numpy.ma as ma
import matplotlib.dates as mdates
parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)



args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa



f = Dataset(args.inf)
mask_rho = unpack(f.variables["mask_rho"])
mask_u = unpack(f.variables["mask_u"])
mask_v =unpack(f.variables["mask_v"])
mask_psi =unpack(f.variables["mask_psi"])
bath=unpack(f.variables["h"])
e_pos=[]
x_pos=[]

print mask_rho.shape, mask_u.shape, mask_v.shape,mask_psi.shape, "rho,u,v-shaes"
for i in range(0,mask_u.shape[0]):
    for j in range(0,mask_u.shape[1]):
        if mask_u[i,j]-mask_rho[i,j]*mask_rho[i,j+1]!=0.0:
            print mask_u[i,j]-mask_rho[i,j]*mask_rho[i,j+1],i,j,"u", bath[i,j]
            x_pos.append(i)
            e_pos.append(j)

for i in range(0,mask_v.shape[0]):
    for j in range(0,mask_v.shape[1]):
        if mask_v[i,j]-mask_rho[i,j]*mask_rho[i+1,j]!=0.0:
            print mask_v[i,j]-mask_rho[i,j]*mask_rho[i+1,j],i,j,"v", bath[i,j]
            x_pos.append(i)
            e_pos.append(j)

for i in range(0,mask_psi.shape[0]):
    for j in range(0,mask_psi.shape[1]):
        if mask_psi[i,j]-mask_rho[i,j]*mask_rho[i+1,j]*mask_rho[i+1,j+1]*mask_rho[i,j+1]!=0.0:
            print mask_psi[i,j]-mask_rho[i,j]*mask_rho[i+1,j]*mask_rho[i+1,j+1]*mask_rho[i,j+1],i,j,"psi", bath[i,j]
            x_pos.append(i)
            e_pos.append(j)

var_masked=ma.masked_where(mask_rho==0,bath)
fig = plt.figure(figsize=(12,8))
ax = fig.add_subplot(111)
palette = plt.cm.plasma
p=ax.pcolormesh(var_masked, cmap=palette)
ax.plot(e_pos,x_pos,'gs', markersize=1)
plt.show()


