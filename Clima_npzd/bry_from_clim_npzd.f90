PROGRAM bry_from_clim
! Program to obtain boundary conditions from climatology file
! 09-07-2014: nilsmk@met.no: Added reading of scale_factor and add_offset
! 12.05.2015 - mitya, dmitry.shcherbin@gmail.com - NPZD only
! Define if ice variables are available on climfile (ICE_IN) and/or ice variables are wanted on bryfile

use netcdf

implicit none

!NPZD
INTEGER DetritusVarId
INTEGER Detritus_westVarId, Detritus_eastVarId, Detritus_southVarId, Detritus_northVarId
INTEGER ZoopVarId
INTEGER Zoop_westVarId, Zoop_eastVarId, Zoop_southVarId, Zoop_northVarId
INTEGER PhytVarId
INTEGER Phyt_westVarId, Phyt_eastVarId, Phyt_southVarId, Phyt_northVarId
INTEGER NO3VarId
INTEGER NO3_westVarId, NO3_eastVarId, NO3_southVarId, NO3_northVarId


!------------------


INTEGER Bry_timeVarId, Clim_timeVarId, zBry_timeVarId

INTEGER XiDimID, EtaDimID, SDimId, TimeDimId, Lp, Mp, L, M, N, ntime, ztime, zTimeDimId
INTEGER XiRhoDimId, EtaRhoDimId
INTEGER dim_xi_rho, dim_eta_rho, dim_s_rho
INTEGER dim_time
INTEGER status, nc_clim, nc

INTEGER itime
REAL time

!biopar
REAL, ALLOCATABLE, DIMENSION(:,:,:) :: detritus
REAL, ALLOCATABLE, DIMENSION(:,:) :: detritus_west, detritus_east, detritus_south, detritus_north
REAL, ALLOCATABLE, DIMENSION(:,:,:) :: zooplankton
REAL, ALLOCATABLE, DIMENSION(:,:) :: zoop_west, zoop_east, zoop_south, zoop_north
REAL, ALLOCATABLE, DIMENSION(:,:,:) :: NO3
REAL, ALLOCATABLE, DIMENSION(:,:) :: NO3_west, NO3_east, NO3_south, NO3_north
REAL, ALLOCATABLE, DIMENSION(:,:,:) :: phytoplankton
REAL, ALLOCATABLE, DIMENSION(:,:) :: phyt_west, phyt_east, phyt_south, phyt_north
!---------------------------------
CHARACTER*80 climfile, bryfile

WRITE(6,'(A)',ADVANCE='NO') "Name of climfile: "
READ(5,'(A)') climfile
WRITE(6,'(A)',ADVANCE='NO') "Name of boundary file: "
READ(5,'(A)') bryfile

write(*,*) 'Starting - ', TRIM(climfile), TRIM(bryfile)
! Get info on climatology file
status = nf90_open(TRIM(climfile),nf90_nowrite,nc_clim)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_inq_dimid(nc_clim, "xi_rho", XiDimID)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_inq_dimid(nc_clim, "eta_rho", EtaDimID)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_inq_dimid(nc_clim, "s_rho", SDimID)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_inq_dimid(nc_clim, "clim_time", TimeDimID)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_Inquire_Dimension(nc_clim,XiDimID,len = Lp)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_Inquire_Dimension(nc_clim,EtaDimID,len = Mp)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_Inquire_Dimension(nc_clim,SDimID,len = N)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_Inquire_Dimension(nc_clim,TimeDimID,len = ntime)
if(status /= nf90_NoErr) call handle_err(status)


L = Lp-1
M = Mp-1

write(*,*) 'Lp = ',Lp,',  Mp = ',Mp,',  N = ',N,',  ntime = ',ntime !,',  ztime = ',ztime

!NPZD
ALLOCATE(detritus(Lp,Mp,N))
ALLOCATE(detritus_west(Mp,N))
ALLOCATE(detritus_east(Mp,N))
ALLOCATE(detritus_south(Lp,N))
ALLOCATE(detritus_north(Lp,N))
ALLOCATE(zooplankton(Lp,Mp,N))
ALLOCATE(zoop_west(Mp,N))
ALLOCATE(zoop_east(Mp,N))
ALLOCATE(zoop_south(Lp,N))
ALLOCATE(zoop_north(Lp,N))
ALLOCATE(NO3(Lp,Mp,N))
ALLOCATE(NO3_west(Mp,N))
ALLOCATE(NO3_east(Mp,N))
ALLOCATE(NO3_south(Lp,N))
ALLOCATE(NO3_north(Lp,N))
ALLOCATE(phytoplankton(Lp,Mp,N))
ALLOCATE(phyt_west(Mp,N))
ALLOCATE(phyt_east(Mp,N))
ALLOCATE(phyt_south(Lp,N))
ALLOCATE(phyt_north(Lp,N))

! Get var ids in climatology file

status = nf90_inq_varid(nc_clim, "detritus",DetritusVarId)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_inq_varid(nc_clim, "zooplankton",ZoopVarId)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_inq_varid(nc_clim, "NO3",NO3VarId)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_inq_varid(nc_clim, "phytoplankton",PhytVarId)
if(status /= nf90_NoErr) call handle_err(status)



status = nf90_inq_varid(nc_clim, "clim_time",Clim_timeVarId)
if(status /= nf90_NoErr) call handle_err(status)

! ------------------------------------------------------                        
! Prepare output netCDF file                                                    
status = nf90_create(trim(bryfile),nf90_clobber,nc)
if(status /= nf90_NoErr) call handle_err(status)

!                                                                               
! Define dimensions                                                             
!                                   
status = nf90_def_dim(nc,'xi_rho',Lp,dim_xi_rho)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_dim(nc,'eta_rho',Mp,dim_eta_rho)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_dim(nc,'s_rho',N,dim_s_rho)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_dim(nc,'bry_time',nf90_unlimited,dim_time)
if(status /= nf90_NoErr) call handle_err(status)

!status = nf90_def_dim(nc,'zbry_time',ztime,dim_ztime)
!if(status /= nf90_NoErr) call handle_err(status)
!                                                                               
! Define variables and attributes                  
!                                
! Global attributes
!
status = nf90_put_att(nc,nf90_global,'type','ROMS/TOMS boundary file')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,nf90_global,'title','ROMS fine-scale simulation')
if(status /= nf90_NoErr) call handle_err(status)


!
! Variables and variable attributes
!
status = nf90_def_var(nc,'bry_time',nf90_double,dim_time,Bry_timeVarId)
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Bry_timeVarId,'long_name','open boundary conditions time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Bry_timeVarId,'units','day of year')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Bry_timeVarId,'field','time, scalar, series')
if(status /= nf90_NoErr) call handle_err(status)
!status = nf90_put_att(nc,Bry_timeVarId,'cycle_length',365.25)
!status = nf90_put_att(nc,Bry_timeVarId,'cycle_length',365.0)
if(status /= nf90_NoErr) call handle_err(status)

! status = nf90_def_var(nc,'zbry_time',nf90_double,dim_ztime,zBry_timeVarId)
! if(status /= nf90_NoErr) call handle_err(status)
! status = nf90_put_att(nc,Bry_timeVarId,'long_name','open boundary conditions time')
! if(status /= nf90_NoErr) call handle_err(status)
! status = nf90_put_att(nc,Bry_timeVarId,'units','day of year')
! if(status /= nf90_NoErr) call handle_err(status)
! status = nf90_put_att(nc,Bry_timeVarId,'field','time, scalar, series')
! if(status /= nf90_NoErr) call handle_err(status)
! !status = nf90_put_att(nc,Bry_timeVarId,'cycle_length',365.25)
! !status = nf90_put_att(nc,Bry_timeVarId,'cycle_length',365.0)
! if(status /= nf90_NoErr) call handle_err(status)

!bio

status = nf90_def_var(nc,'detritus_west',nf90_float,                           &
         (/dim_eta_rho, dim_s_rho, dim_time/),Detritus_westVarId)
status = nf90_put_att(nc,Detritus_westVarId,'long_name',                      &
         'detritus western boundary condition')
status = nf90_put_att(nc,Detritus_westVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Detritus_westVarId,'field',                          &
          'detritus_west, scalar, series')
status = nf90_put_att(nc,Detritus_westVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'detritus_east',nf90_float,                           &
         (/dim_eta_rho, dim_s_rho, dim_time/),Detritus_eastVarId)
status = nf90_put_att(nc,Detritus_eastVarId,'long_name',                      &
         'detritus eastern boundary condition')
status = nf90_put_att(nc,Detritus_eastVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Detritus_eastVarId,'field',                          &
          'detritus_east, scalar, series')
status = nf90_put_att(nc,Detritus_eastVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'detritus_south',nf90_float,                          &
         (/dim_xi_rho, dim_s_rho, dim_time/),Detritus_southVarId)
status = nf90_put_att(nc,Detritus_southVarId,'long_name',                      &
         'detritus southern boundary condition')
status = nf90_put_att(nc,Detritus_southVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Detritus_southVarId,'field',                          &
          'detritus_south, scalar, series')
status = nf90_put_att(nc,Detritus_southVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'detritus_north',nf90_float,                          &
         (/dim_xi_rho, dim_s_rho, dim_time/),Detritus_northVarId)
status = nf90_put_att(nc,Detritus_northVarId,'long_name',                      &
         'detritus northern boundary condition')
status = nf90_put_att(nc,Detritus_northVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Detritus_northVarId,'field',                          &
          'detritus_north, scalar, series')
status = nf90_put_att(nc,Detritus_northVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)

status = nf90_def_var(nc,'zoop_west',nf90_float,                           &
         (/dim_eta_rho, dim_s_rho, dim_time/),Zoop_westVarId)
status = nf90_put_att(nc,Zoop_westVarId,'long_name',                      &
         'zooplankton western boundary condition')
status = nf90_put_att(nc,Zoop_westVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Zoop_westVarId,'field',                          &
          'zoop_west, scalar, series')
status = nf90_put_att(nc,Zoop_westVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'zoop_east',nf90_float,                           &
         (/dim_eta_rho, dim_s_rho, dim_time/),Zoop_eastVarId)
status = nf90_put_att(nc,Zoop_eastVarId,'long_name',                      &
         'zooplankton eastern boundary condition')
status = nf90_put_att(nc,Zoop_eastVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Zoop_eastVarId,'field',                          &
          'zoop_east, scalar, series')
status = nf90_put_att(nc,Zoop_eastVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'zoop_south',nf90_float,                          &
         (/dim_xi_rho, dim_s_rho, dim_time/),Zoop_southVarId)
status = nf90_put_att(nc,Zoop_southVarId,'long_name',                      &
         'zooplankton southern boundary condition')
status = nf90_put_att(nc,Zoop_southVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Zoop_southVarId,'field',                          &
          'zoop_south, scalar, series')
status = nf90_put_att(nc,Zoop_southVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'zoop_north',nf90_float,                          &
         (/dim_xi_rho, dim_s_rho, dim_time/),Zoop_northVarId)
status = nf90_put_att(nc,Zoop_northVarId,'long_name',                      &
         'zooplankton northern boundary condition')
status = nf90_put_att(nc,Zoop_northVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Zoop_northVarId,'field',                          &
          'zoop_north, scalar, series')
status = nf90_put_att(nc,Zoop_northVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)

status = nf90_def_var(nc,'NO3_west',nf90_float,                           &
         (/dim_eta_rho, dim_s_rho, dim_time/),NO3_westVarId)
status = nf90_put_att(nc,NO3_westVarId,'long_name',                      &
         'NO3 western boundary condition')
status = nf90_put_att(nc,NO3_westVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,NO3_westVarId,'field',                          &
          'NO3_west, scalar, series')
status = nf90_put_att(nc,NO3_westVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'NO3_east',nf90_float,                           &
         (/dim_eta_rho, dim_s_rho, dim_time/),NO3_eastVarId)
status = nf90_put_att(nc,NO3_eastVarId,'long_name',                      &
         'NO3 eastern boundary condition')
status = nf90_put_att(nc,NO3_eastVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,NO3_eastVarId,'field',                          &
          'NO3_east, scalar, series')
status = nf90_put_att(nc,NO3_eastVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'NO3_south',nf90_float,                          &
         (/dim_xi_rho, dim_s_rho, dim_time/),NO3_southVarId)
status = nf90_put_att(nc,NO3_southVarId,'long_name',                      &
         'NO3 southern boundary condition')
status = nf90_put_att(nc,NO3_southVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,NO3_southVarId,'field',                          &
          'NO3_south, scalar, series')
status = nf90_put_att(nc,NO3_southVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'NO3_north',nf90_float,                          &
         (/dim_xi_rho, dim_s_rho, dim_time/),NO3_northVarId)
status = nf90_put_att(nc,NO3_northVarId,'long_name',                      &
         'NO3 northern boundary condition')
status = nf90_put_att(nc,NO3_northVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,NO3_northVarId,'field',                          &
          'NO3_north, scalar, series')
status = nf90_put_att(nc,NO3_northVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)

status = nf90_def_var(nc,'phyt_west',nf90_float,                           &
         (/dim_eta_rho, dim_s_rho, dim_time/),Phyt_westVarId)
status = nf90_put_att(nc,Phyt_westVarId,'long_name',                      &
         'phytoplankton western boundary condition')
status = nf90_put_att(nc,Phyt_westVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Phyt_westVarId,'field',                          &
          'phyt_west, scalar, series')
status = nf90_put_att(nc,Phyt_westVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'phyt_east',nf90_float,                           &
         (/dim_eta_rho, dim_s_rho, dim_time/),Phyt_eastVarId)
status = nf90_put_att(nc,Phyt_eastVarId,'long_name',                      &
         'phytoplankton eastern boundary condition')
status = nf90_put_att(nc,Phyt_eastVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Phyt_eastVarId,'field',                          &
          'phyt_east, scalar, series')
status = nf90_put_att(nc,Phyt_eastVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'phyt_south',nf90_float,                          &
         (/dim_xi_rho, dim_s_rho, dim_time/),Phyt_southVarId)
status = nf90_put_att(nc,Phyt_southVarId,'long_name',                      &
         'phytoplankton southern boundary condition')
status = nf90_put_att(nc,Phyt_southVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Phyt_southVarId,'field',                          &
          'phyt_south, scalar, series')
status = nf90_put_att(nc,Phyt_southVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_def_var(nc,'phyt_north',nf90_float,                          &
         (/dim_xi_rho, dim_s_rho, dim_time/),Phyt_northVarId)
status = nf90_put_att(nc,Phyt_northVarId,'long_name',                      &
         'phytoplankton northern boundary condition')
status = nf90_put_att(nc,Phyt_northVarId,'units',' ')
if(status /= nf90_NoErr) call handle_err(status)
status = nf90_put_att(nc,Phyt_northVarId,'field',                          &
          'phyt_north, scalar, series')
status = nf90_put_att(nc,Phyt_northVarId,'time','bry_time')
if(status /= nf90_NoErr) call handle_err(status)

!---------------------------------

write(*,*) 'after overall defs'

!                                                                               
! Exit definition mode                                                          
!                                                                               
status = nf90_enddef(nc)
if(status /= nf90_NoErr) call handle_err(status)
!
write(*,*) 'Finished file definition'


! -------------------------------------------------------------------
!
! Read in climatology arrays and output boundary arrays for each variable
!
! Loop through time steps
!


DO itime = 1,ntime

   status = nf90_get_var(nc_clim,Clim_timeVarId,time,                        &
                start=(/itime/))
   status = nf90_put_var(nc,Bry_timeVarId,time,(/itime/))
   if(status /= nf90_NoErr) call handle_err(status)

   status = nf90_get_var(nc_clim,DetritusVarId,detritus,                             &
                start=(/ 1, 1, 1, itime/))
   detritus_west(:,:) = detritus(1,:,:)
   detritus_east(:,:) = detritus(Lp,:,:)
   detritus_south(:,:) = detritus(:,1,:)
   detritus_north(:,:) = detritus(:,Mp,:)

   status = nf90_put_var(nc,Detritus_westVarId,detritus_west,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Detritus_eastVarId,detritus_east,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Detritus_southVarId,detritus_south,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Detritus_northVarId,detritus_north,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)

   status = nf90_get_var(nc_clim,ZoopVarId,zooplankton,                             &
                start=(/ 1, 1, 1, itime/))
   zoop_west(:,:) = zooplankton(1,:,:)
   zoop_east(:,:) = zooplankton(Lp,:,:)
   zoop_south(:,:) = zooplankton(:,1,:)
   zoop_north(:,:) = zooplankton(:,Mp,:)
   status = nf90_put_var(nc,Zoop_westVarId,zoop_west,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Zoop_eastVarId,zoop_east,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Zoop_southVarId,zoop_south,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Zoop_northVarId,zoop_north,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   write(*,*) 'after zoop and detritus'

   status = nf90_get_var(nc_clim,NO3VarId,NO3,                             &
                start=(/ 1, 1, 1, itime/))
   NO3_west(:,:) = NO3(1,:,:)
   NO3_east(:,:) = NO3(Lp,:,:)
   NO3_south(:,:) = NO3(:,1,:)
   NO3_north(:,:) = NO3(:,Mp,:)
   status = nf90_put_var(nc,NO3_westVarId,NO3_west,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,NO3_eastVarId,NO3_east,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,NO3_southVarId,NO3_south,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,NO3_northVarId,NO3_north,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)

   status = nf90_get_var(nc_clim,PhytVarId,phytoplankton,                             &
                start=(/ 1, 1, 1, itime/))
   phyt_west(:,:) = phytoplankton(1,:,:)
   phyt_east(:,:) = phytoplankton(Lp,:,:)
   phyt_south(:,:) = phytoplankton(:,1,:)
   phyt_north(:,:) = phytoplankton(:,Mp,:)
   status = nf90_put_var(nc,Phyt_westVarId,phyt_west,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Phyt_eastVarId,phyt_east,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Phyt_southVarId,phyt_south,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)
   status = nf90_put_var(nc,Phyt_northVarId,phyt_north,(/1,1,itime/))
   if(status /= nf90_NoErr) call handle_err(status)

   write(*,*) 'Fields at record ',itime,', time = ',time,' days written'

ENDDO

status = nf90_close(nc_clim)
if(status /= nf90_NoErr) call handle_err(status)

status = nf90_close(nc)
if(status /= nf90_NoErr) call handle_err(status)

END PROGRAM bry_from_clim
