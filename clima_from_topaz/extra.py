#!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm
import numpy.ma as ma
import matplotlib.dates as mdates
parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)
parser.add_argument(
'-mask', 
help='kasn file', 
dest='mask', 
action="store"
)
parser.add_argument(
'-v', 
help='variable', 
dest='var', 
action="store"
)


args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa




#for i in files:
#    print i
f = Dataset(args.inf,'r+')
m = Dataset(args.mask,'r')
#    print f.dimensions
    #     lat.append(unpack(f.variables['lat_rho']))
    #     lon.append(unpack(f.variables['lon_rho']))
try:
    var = unpack(f.variables[args.var])[0,:]
    mask = unpack(m.variables["var_mask"])[:]
    print var.shape,mask.shape, "var shape, here"
except:
    print "no var ", args.var

#    nc = Dataset(i+"_mask", 'w', format='NETCDF3_64BIT')
#    nc.createDimension('y', var.shape[0])
#    nc.createDimension('x',var.shape[1])
#    nc.createVariable('var_mask',np.short,('y','x'))
 #   print  nc.variables('var_mask')
 #   nc.variables('var_mask')[:]=np.zeros(var.shape)[:]
 #   mask_var=nc.variables('var_mask')
 #   print mask_var.shape,var.shape, "both"
     

#    e_pos=[]
#    x_pos=[]

#    #for i,j in np.ndenumerate(ma.masked_where(mask_rho[0]==0, var[-1,-1,0,:])):
#    for i,j in np.ndenumerate(var):
#        if j < -5:
#            print i,j
#            x_pos.append(i[0])
##            e_pos.append(i[1])
#            print var[i[0],i[1]],j
#            nc.variables["var_mask"][i[0],i[1]]=1
#        else:
#            pass
#            nc.variables["var_mask"][i[0],i[1]]=0
#    print len(x_pos)

undef = 2.0e+35
tx = 0.9 * undef
critx = 0.01
cor = 1.6
mxs = 10
field = var
field = np.where(mask == 1, undef, field)
import extrapolate as ex
field = ex.extrapolate.fill(1, int(var.shape[1]),
                            1, int(var.shape[0]),
                            float(tx), float(critx), float(cor), float(mxs),
                            np.asarray(field, order='Fortran'),
                            int(var.shape[1]),int(var.shape[0]))


#nc.close()
f.variables[args.var][0,:]=field[:]
f.close()
m.close()
exit()




fig = plt.figure(figsize=(12,8))
ax = fig.add_subplot(111)
palette = plt.cm.plasma
#p=ax.pcolormesh(ma.masked_outside(salt,-1e+36,1e+36), cmap=palette
#p=ax.pcolormesh(ma.masked_outside(bath,-1e+36,1e+36), cmap=palette)
p=ax.pcolormesh(field, cmap=palette)
plt.colorbar(p)
#p.set_clim(vmin=0,vmax=30)
    #delta = 1
    #plt.Rectangle((river_x[hplot-1], river_e[hplot-1]),width=delta,height=delta,color='red') 
#xp_pos[:]= [x - 1 for x in xp_pos]
    
#ax.plot(e_pos,x_pos,'bs', markersize=1)
#ax.plot(ep_pos,xp_pos,'g^', markersize=2)
plt.show()
exit()
g = Dataset("/global/work/thinf/metroms_input/s800_cice_processed/svalbard_800m_river.nc")
epos=unpack(g.variables["river_Eposition"])
xpos=unpack(g.variables["river_Xposition"])
rivpos=zip(epos,xpos)
for i in rivpos:
    print i, bath[i],salt[i], mask_rho[0][i]
exit()
#info = zip(river_num[0], river_e[0], river_x[0], river_direction[0],np.sum(river_transport[0], axis=0))



#sealand = (1.0,0.0)

fig = plt.figure(figsize=(12,8))



counter= 0
for hplot in [1]:
    rho_x, rho_e = [],[]
    ax = fig.add_subplot(111)
    maxval = np.amax(bath[hplot-1])
    minval = np.amin(bath[hplot-1])
    limit = max(abs(minval), abs(maxval))

#    for i,j in enumerate(river_x[hplot-1]):
#        if river_transport[hplot-1][args.num-1,i]>0:
#            bath[hplot-1][int(river_e[hplot-1][i]),int(river_x[hplot-1][i])] = limit+1000.
#            rho_x.append(river_x[hplot-1][i])
#            rho_e.append(river_e[hplot-1][i])
#        else:
#            if river_direction[hplot-1][i]==0:
#                bath[hplot-1][int(river_e[hplot-1][i]),int(river_x[hplot-1][i])-1] = limit+1000.
#                rho_x.append(river_x[hplot-1][i]-1)
#                rho_e.append(river_e[hplot-1][i])
#            else:
#                bath[hplot-1][int(river_e[hplot-1][i])-1,int(river_x[hplot-1][i])] = limit+1000.
#                rho_x.append(river_x[hplot-1][i])
#                rho_e.append(river_e[hplot-1][i]-1)
#    counter = 0
    palette = plt.cm.spectral
    palette.set_over('c', limit+100.)        
    p=ax.pcolormesh(ma.masked_where(mask[hplot-1]==0, bath[hplot-1]), cmap=palette)
    plt.colorbar(p)
    p.set_clim(vmin=minval,vmax=maxval)
    ax.plot(e_pos,x_pos,'ro', markersize=5)






if args.clicks=='yes':
    fig.canvas.mpl_connect('button_press_event', onclick)
else:
    pass


plt.show()




