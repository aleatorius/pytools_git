#!/bin/bash
#PBS -o /global/work/mitya/metno/ERA-Interim_convert/o.log
#PBS -j oe
#PBS -k oe
#PBS -N erainterp
#PBS -l select=1:ncpus=32:mpiprocs=16:ompthreads=16:mem=29000mb
#PBS -l walltime=03:00:00
#PBS -A nn9300k
#PBS -V
#-----------------------------------------------
#-----------------------------------------------
#source /etc/profile.d/modules.sh
#module load udunits/2.1.24
#module unload intelcomp/12.0.5.220
#module load intelcomp/13.0.1 mpt/2.06 nco/4.3.1 netcdf/4.3.0 proj/4.8.0 boost/1.53.0 hdf5 
set -x
module load NCO/4.2.2 netCDF/4.2.1.1-intel-13.0  intel/13.0 
module load Boost/1.52.0-intel-13.0-OpenMPI-1.8.1

workdir=/global/work/$USER/bioclim/s800
homedir=/home/mitya
pathTools=/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
outdir=$workdir

cd $workdir
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>$workdir/bioclim.log_${datstamp} 2>&1

datadir=/global/work/mitya/mftp.cmems.met.no/Core/ARCTIC_REANALYSIS_BIO_002_005/dataset-bio-ran-arc-myoceanv2-be
for filein in ${datadir}/*NERSC-*.nc; do

    echo $filein

    ncatted -O -a units,time,d,, $filein temp.nc

    xvals="0,25000,...,1440800"
    yvals="0,25000,...,1080800"

    ${path2Fimex} --input.file=temp.nc  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=temp_cut.nc   --interpolate.method=coord_kdtree  --interpolate.preprocess='fill2d(0.01, 1.6, 100)' 
#--output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml
#done

    xvals="0,800,...,1440800"
    yvals="0,800,...,1080800"

    ${path2Fimex} --input.file=temp_cut.nc  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=${filein##*/}_s800.nc --interpolate.method=coord_nearestneighbor  
#--output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml
    rm -f temp.nc temp_cut.nc 
done
set +x
exit