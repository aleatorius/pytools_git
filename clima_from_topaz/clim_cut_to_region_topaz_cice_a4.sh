#!/bin/bash
#SBATCH -J matlab_spatial_int
#SBATCH --account nn9300k
##SBATCH --qos=preproc
##SBATCH --time=10:10:00
#SBATCH --partition=bigmem
#SBATCH --time=0-10:50:0
#SBATCH --nodes=1 --ntasks-per-node=1 
#SBATCH --mem=64G

year="2007"

workdir=/cluster/work/users/$USER/clima_temp/${year}

if [ ! -d "$workdir" ]; then
    mkdir $workdir
fi                                                                                       

homedir=/cluster/home/$USER
#pathTools=/home/mitya/bin
path2Fimex=${homedir}/bin/fimex


cd $workdir
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>$workdir/bioclim.log_${datstamp} 2>&1
cd $workdir
datadir=/cluster/shared/arcticfjord/Annettes_files/2015/
set -x
for filein in /cluster/shared/arcticfjord/Annettes_files/${year}/TP4DAILY_*.nc; do
#for filein in test.nc; do

    echo $filein

    ncatted  --bfr_sz=4194304 -O -a units,time,d,, $filein temp.nc
    ncks  --bfr_sz=4194304 -O -v hice,fice,fy_age,hsnow temp.nc temp.nc
    set +x
    source $HOME/pytools_git/clima_from_topaz/modules_fram_fimex.sh
    set -x


    xvals="2012000,2024000,...,4796000"
    yvals="688000,700000,...,2504000"
    projline="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0"

    ${path2Fimex} --input.file=temp.nc  --interpolate.projString="${projline}" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=temp_02.nc   --interpolate.method=coord_kdtree  --interpolate.preprocess='fill2d(0.01, 1.6, 100)' 
#--output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml
#done

    xvals="2012000,2016000,...,4796000"
    yvals="688000,692000,...,2504000"


    ${path2Fimex} --input.file=temp_02.nc  --interpolate.projString="${projline}" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=${filein##*/}_a4.nc --interpolate.method=coord_nearestneighbor  

    rm -f temp* 
done
set +x
exit
