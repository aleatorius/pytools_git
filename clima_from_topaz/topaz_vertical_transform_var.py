#!/home/mitya/testenv/bin/python -B                                                                                   
import os
import sys
from datetime import datetime

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import *
import numpy.ma as ma
import netcdftime
import time as tm
from calendar import monthrange
import datetime 
from datetime import date, time
import argparse
import z_coord
import vert


parser = argparse.ArgumentParser(description='transect write 0.1')

parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)
parser.add_argument(
'-zeta', 
help='input zeta file', 
dest='zeta', 
action="store"
)
parser.add_argument(
'-o', 
help='output file', 
dest='outf', 
action="store"
)
parser.add_argument(
'-var', 
help='vars', 
dest='var', 
action="store"
)
parser.add_argument(
'-grid', 
help='grid file for output', 
dest='grid', 
action="store",
default="/cluster/shared/arcticfjord/svalbard_800m_grid_uvpsi.nc"
)

args = parser.parse_args()


#vertical coordinates parameters
Ni=75
No=35
theta_so=8.0; theta_bo=0.1; Tclineo=20.0
Vtransformo=2; Vstretchingo=2



try:
    mask_rho=f.variables["mask_rho"][:]
    mask_u=f.variables["mask_u"][:]
    mask_v=f.variables["mask_v"][:]
    hi = f.variables["h"][:]
except:
    grid = Dataset(args.grid)
    mask_rho=grid.variables["mask_rho"][:]
    mask_u=grid.variables["mask_u"][:]
    mask_v=grid.variables["mask_v"][:]
    hi = grid.variables["h"][:]
    grid.close()


f = Dataset(args.inf)
depth = f.variables["depth"][:]

try:
    zeta = f.variables["zeta"][0,:]
except OSError:
    zeta_f = Dataset(args.zeta)
    zeta=zeta_f.variables["zeta"][0.:]
    zeta_f.close()
except:
    try:
        zeta = f.variables["ssh"][0,:]
        print "ssh", zeta.min(), zeta.max()
    except:
        print "zero zeta case"
        zeta = np.zeros(np.shape(hi))



print "hi", hi.shape

z_ro = z_coord.spec_vert_grid(No,hi,zeta,Tclineo,theta_bo,theta_so,Vtransformo,Vstretchingo,[zeta.shape[0],zeta.shape[1]])

print z_ro.shape
print np.amax(z_ro), np.amin(z_ro)
print "...."
print "depth shape", depth.shape
depth = depth[::-1]


undef=1.e+37


if not args.outf:
    nc = Dataset(args.inf+"_vert_"+args.var, 'w', format='NETCDF3_64BIT')
else:
    nc = Dataset(args.outf, 'w', format='NETCDF3_64BIT')
nc.createDimension('s_rho', No)   
nc.createDimension('eta_rho', mask_rho.shape[0])
nc.createDimension('xi_rho',mask_rho.shape[1])
nc.createDimension('eta_u', mask_u.shape[0])
nc.createDimension('xi_u',mask_u.shape[1])
 
nc.createDimension('eta_v', mask_v.shape[0])
nc.createDimension('xi_v',mask_v.shape[1])


nc.createDimension('clim_time', None) # unlimited axis (can be appended to).
nc.createVariable('clim_time',np.float64,('clim_time'))
#nc.setncattr("title","field "+args.field)
counter="in"
#for argsfield in ["salinity","temperature", "u", "v"]:
argsfield =args.var
if argsfield == "u" or argsfield == "v":
    print argsfield
    field = f.variables[argsfield][0,::-1,:]
    print field.shape, "field"
    print f.variables[argsfield].add_offset, f.variables[argsfield].scale_factor, "ofset,scale"
    field_tr = np.transpose(field, (1,2,0))
    print field_tr.shape, "transpose"
    if counter == "in":
        z_ri= np.zeros(field.shape)
        print "z_ri.shape", z_ri.shape
        z_ri.T[:]=-depth
        z_ri_tran= np.transpose(z_ri, (1,2,0))
        print z_ri_tran.shape, "z_ri_tran shape"
        counter = "out"
    else:
        pass
    array_out = vert.vert_int(field_tr,z_ri_tran,z_ro,undef,mask_rho,mask_rho,[mask_rho.shape[1],mask_rho.shape[0],Ni,No])
    out_field = nc.createVariable(argsfield, np.short, ('clim_time','s_rho','eta_'+argsfield,'xi_'+argsfield))
    try:
        out_field.long_name = f.variables[argsfield].long_name
    except:
        pass
    try:
        out_field.time = f.variables[argsfield].time
    except:
        pass
    try:
        out_field.units = f.variables[argsfield].units
    except:
        pass

    try:
        out_field.standard_name = f.variables[argsfield].standard_name
    except:
        pass

    try:
        out_field.grid_mapping = f.variables[argsfield].grid_mapping
    except:
        pass

    try:
        out_field.field = f.variables[argsfield].field
    except:
        pass

    try:
        out_field.scale_factor =  f.variables[argsfield].scale_factor
    except:
        pass

    try:
        out_field.add_offset =  f.variables[argsfield].add_offset
    except:
        pass
    if argsfield == "u":
        print np.transpose(array_out, (2,0,1))[:,:,1:].shape
        nc.variables[argsfield][0,:] = (np.transpose(array_out, (2,0,1))[:,:,1:]+np.transpose(array_out, (2,0,1))[:,:,:-1])/2.0
    else:
        nc.variables[argsfield][0,:] = (np.transpose(array_out, (2,0,1))[:,1:,:]+np.transpose(array_out, (2,0,1))[:,:-1,:])/2.0



else:
    print argsfield
    field = f.variables[argsfield][0,::-1,:]
    print field.shape, "field"
    print f.variables[argsfield].add_offset, f.variables[argsfield].scale_factor, "ofset,scale"
    field_tr = np.transpose(field, (1,2,0))
    print field_tr.shape, "transpose"
    if counter == "in":
        z_ri= np.zeros(field.shape)
        print "z_ri.shape", z_ri.shape
        z_ri.T[:]=-depth
        z_ri_tran= np.transpose(z_ri, (1,2,0))
        print z_ri_tran.shape, "z_ri_tran shape"
        counter = "out"
    else:
        pass
    array_out = vert.vert_int(field_tr,z_ri_tran,z_ro,undef,mask_rho,mask_rho,[mask_rho.shape[1],mask_rho.shape[0],Ni,No])
    out_field = nc.createVariable(argsfield, np.short, ('clim_time','s_rho','eta_rho','xi_rho'))
    try:
        out_field.long_name = f.variables[argsfield].long_name
    except:
        pass
    try:
        out_field.time = f.variables[argsfield].time
    except:
        pass
    try:
        out_field.units = f.variables[argsfield].units
    except:
        pass

    try:
        out_field.standard_name = f.variables[argsfield].standard_name
    except:
        pass

    try:
        out_field.grid_mapping = f.variables[argsfield].grid_mapping
    except:
        pass

    try:
        out_field.field = f.variables[argsfield].field
    except:
        pass

    try:
        out_field.scale_factor =  f.variables[argsfield].scale_factor
    except:
        pass

    try:
        out_field.add_offset =  f.variables[argsfield].add_offset
    except:
        pass

    nc.variables[argsfield][0,:] = np.transpose(array_out, (2,0,1))

    #sanitizing, we deal with concentrations, no negatives (which may pop up when interpolating)
  #  array_out[ array_out<0 ] = 0
   # print array_out.shape, "array_out" 



    #sanitizing, we deal with concentrations, no negatives (which may pop up when interpolating)
  #  array_out[ array_out<0 ] = 0
   # print array_out.shape, "array_out" 


ref_roms = date(1970,01,01)
ref_time = time(0,0,0)
refer_roms= datetime.datetime.combine(ref_roms, ref_time)
ref_topaz = date(1970,01,01)
refer_topaz = datetime.datetime.combine(ref_topaz, ref_time)

nc.variables['clim_time'][:]= np.array(f.variables['time'][:]*60*60-(ref_roms-ref_topaz).total_seconds())

nc.close()
f.close()
