#!/bin/bash
#SBATCH -J topaz-to-cice
#SBATCH --account nn9300k
##SBATCH --qos=preproc
##SBATCH --time=10:10:00
#SBATCH --partition=bigmem
#SBATCH --time=0-03:59:0
#SBATCH --nodes=1 --ntasks-per-node=1 
#SBATCH --mem=32G

year="2011"

workdir=/cluster/work/users/mitya/topaz/a4_cice/${year}

if [ ! -d "$workdir" ]; then
    mkdir $workdir
fi                                                                                       


homedir=/cluster/home/$USER

path2Fimex=${homedir}/bin/fimex


cd $workdir
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>$workdir/bioclim.log_${datstamp} 2>&1
cd $workdir
#datadir=/cluster/shared/arcticfjord/Annettes_files/2015/
set -x
for filein in /cluster/shared/arcticfjord/Annettes_files/${year}/TP4DAILY_*.nc; do
    echo $filein
    ncatted  --bfr_sz=4194304 -O -a units,time,d,, $filein temp.nc
    ncks  --bfr_sz=4194304 -O -v uice,vice,time,hice,fice,fy_age,hsnow temp.nc temp.nc
    set +x
    source $HOME/pytools_git/clima_from_topaz/modules_fram_fimex.sh
    set -x
    cp temp.nc temp_01.nc
#    fimex --input.file=temp.nc --process.rotateVectorToLatLonX=uice --process.rotateVectorToLatLonY=vice  --output.file=temp_01.nc
    outfile=${filein##*/}_a4_uvice.nc
    xvals="2012000,2024000,...,4796000"
    yvals="688000,700000,...,2504000"
    projline="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0"
    ${path2Fimex} --input.file=temp_01.nc  --interpolate.projString="${projline}" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=temp_02.nc   --interpolate.method=coord_kdtree  --interpolate.preprocess='fill2d(0.01, 1.6, 100)' 



    xvals="2012000,2016000,...,4792000"
    yvals="688000,692000,...,2500000"


    ${path2Fimex} --input.file=temp_02.nc  --interpolate.projString="${projline}" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=coord_nearestneighbor  


#rotate vector
    ncrename --bfr_sz=4194304 -d y,eta_rho -d x,xi_rho -v x,xi_rho -v y,eta_rho $outfile
#angle from grid file
    angle=/cluster/home/${USER}/pytools_git/clima_from_topaz/angle_sa4.nc

    ncks --bfr_sz=4194304 -A $angle $outfile
    ncap --bfr_sz=4194304 -O -s "U=(uice*cos(angle)+vice*sin(angle));V=(vice*cos(angle)-uice*sin(angle));uice=U;vice=V" -v $outfile temp.nc_rot
    ncap2  --bfr_sz=4194304 -O -s "uice=uice/9.15667063455728e-05;vice=vice/9.15667063455728e-05" temp.nc_rot temp.nc_rot
    ncap2  --bfr_sz=4194304 -O -s "uice=short(uice);vice=short(vice);uice@add_offset=0.0;vice@add_offset=0.0;uice@scale_factor=9.15667063455728e-05;vice@scale_factor=9.15667063455728e-05"  temp.nc_rot temp.nc_rot

    ncks --bfr_sz=4194304 -A -v uice,vice temp.nc_rot ${outfile}
    set +x
    source $HOME/.bashrc
    set -x



#    ncrename --bfr_sz=4194304 -d time,clim_time -v time,clim_time  ${outfile} 
#    ncap2 --bfr_sz=4194304 -O -s "clim_time=clim_time/86400" ${outfile} ${outfile} 
#    ncatted  --bfr_sz=4194304 -O   -a time,,a,c,clim_time -a units,clim_time,a,c,"days" -a  field,clim_time,a,c,"time, scalar, series"  -a long_name,clim_time,a,c,"days since 1948-01-01 00:00:00" ${outfile} ${outfile}  
    
#--output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml
    rm -f temp* 
#    rm -f   ${outfile}_vert_u  ${outfile}_vert_v ${outfile}_vert_salinity  ${outfile}_vert_temperature ${outfile}_bar ${outfile}_baruv

done
set +x
exit
