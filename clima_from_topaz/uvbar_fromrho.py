#!/home/mitya/testenv/bin/python -B                                                                                   
import os
import sys
from datetime import datetime

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import *
import numpy.ma as ma
import netcdftime
import time as tm
from calendar import monthrange
import datetime 
from datetime import date, time
import argparse
import z_coord
import vert


parser = argparse.ArgumentParser(description='transect write 0.1')

parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)

parser.add_argument(
'-o', 
help='output file', 
dest='outf', 
action="store"
)

parser.add_argument(
'-grid', 
help='grid file for output', 
dest='grid', 
action="store",
default="/cluster/shared/arcticfjord/svalbard_800m_grid_uvpsi.nc"
)

args = parser.parse_args()


try:
    mask_u=f.variables["mask_u"][:]
    mask_v=f.variables["mask_v"][:]
    clim_time=f.variables["clim_time"][:]
except:
    grid = Dataset(args.grid)
    mask_u=grid.variables["mask_u"][:]
    mask_v=grid.variables["mask_v"][:]
    grid.close()


f = Dataset(args.inf)

undef=1.e+37


if not args.outf:
    nc = Dataset(args.inf+"_uv_", 'w', format='NETCDF3_64BIT')
else:
    nc = Dataset(args.outf, 'w', format='NETCDF3_64BIT')


nc.createDimension('eta_u', mask_u.shape[0])
nc.createDimension('xi_u',mask_u.shape[1])
nc.createDimension('eta_v', mask_v.shape[0])
nc.createDimension('xi_v',mask_v.shape[1])


nc.createDimension('clim_time', None) # unlimited axis (can be appended to).
#nc.createVariable('clim_time',np.float64,('clim_time'))
#nc.setncattr("title","field "+args.field)
counter="in"
for argsfield in ["ubar","vbar"]:
    print argsfield
    field = f.variables[argsfield][:]
    print field.shape, "field"
    print f.variables[argsfield].add_offset, f.variables[argsfield].scale_factor, "ofset,scale"

    out_field = nc.createVariable(argsfield, np.short, ('clim_time','eta_'+argsfield.replace("bar",""),'xi_'+argsfield.replace("bar","")))
    try:
        out_field.long_name = f.variables[argsfield].long_name
    except:
        pass
    try:
        out_field.time = f.variables[argsfield].time
    except:
        pass
    try:
        out_field.units = f.variables[argsfield].units
    except:
        pass

    try:
        out_field.standard_name = f.variables[argsfield].standard_name
    except:
        pass

    try:
        out_field.grid_mapping = f.variables[argsfield].grid_mapping
    except:
        pass

    try:
        out_field.field = f.variables[argsfield].field
    except:
        pass

    try:
        out_field.scale_factor =  f.variables[argsfield].scale_factor
    except:
        pass

    try:
        out_field.add_offset =  f.variables[argsfield].add_offset
    except:
        pass
    if argsfield == "ubar":
        nc.variables[argsfield][:] = (field[:,:,1:]+field[:,:,:-1])/2.0
    else:
        nc.variables[argsfield][:] = (field[:,1:,:]+field[:,:-1,:])/2.0



#ref_roms = date(1970,01,01)
#ref_time = time(0,0,0)
#refer_roms= datetime.datetime.combine(ref_roms, ref_time)
#ref_topaz = date(1970,01,01)
#refer_topaz = datetime.datetime.combine(ref_topaz, ref_time)

#nc.variables['clim_time'][:]= np.array(f.variables['clim_time'][:]

nc.close()
f.close()
