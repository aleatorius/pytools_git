#!/bin/bash
#SBATCH -J matlab_spatial_int
#SBATCH --account nn9300k
#SBATCH --qos=preproc
##SBATCH --time=10:10:00
##SBATCH --partition=bigmem
#SBATCH --time=0-10:50:0
#SBATCH --nodes=1 --ntasks-per-node=1 
##SBATCH --mem=16G

year="2010"

workdir=/cluster/work/users/mitya/topaz/a4_roms/${year}/

if [ ! -d "$workdir" ]; then
    mkdir $workdir
fi                                                                                       


homedir=/cluster/home/$USER

path2Fimex=${homedir}/bin/fimex


cd $workdir
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>$workdir/bioclim.log_${datstamp} 2>&1
cd $workdir
#datadir=/cluster/shared/arcticfjord/Annettes_files/2015/
set -x
#for filein in /cluster/shared/arcticfjord/Annettes_files/${year}/TP4DAILY_*.nc; do
for filein in /cluster/shared/arcticfjord/Annettes_files/2009/TP4DAILY_start200908* /cluster/shared/arcticfjord/Annettes_files/2009/TP4DAILY_start200909* /cluster/shared/arcticfjord/Annettes_files/2009/TP4DAILY_start20091*; do
#for filein in /cluster/shared/arcticfjord/Annettes_files/2014/TP4DAILY_start20141112_dump20141112.nc; do

    echo $filein

    ncatted  --bfr_sz=4194304 -O -a units,time,d,, $filein temp.nc
#    ncks  --bfr_sz=4194304 -x -O -v hice,fice,fy_age,hsnow,uice,vice,tauy,taux,tauyi,tauxi temp.nc temp.nc
    ncrename  --bfr_sz=4194304 -O -v ssh,zeta temp.nc temp.nc
    set +x
    source $HOME/pytools_git/clima_from_topaz/modules_fram_fimex.sh
    set -x
#    cp temp.nc temp_01.nc
#    fimex --input.file=temp.nc --process.rotateVectorToLatLonX=u --process.rotateVectorToLatLonY=v  --process.rotateVectorToLatLonX=ubarotrop --process.rotateVectorToLatLonY=vbarotrop --output.file=temp_01.nc
    outfile=${filein##*/}

    xvals="2012000,2024000,...,4796000"
    yvals="688000,700000,...,2504000"

    ${path2Fimex} --input.file=temp.nc  --interpolate.projString="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=temp_02.nc   --interpolate.method=coord_kdtree  --interpolate.preprocess='fill2d(0.01, 1.6, 100)' 




    xvals="2012000,2016000,...,4792000"
    yvals="688000,692000,...,2500000"
#    xvals="2012000,2016000,...,4796000"
#    yvals="688000,692000,...,2504000"



    ${path2Fimex} --input.file=temp_02.nc  --interpolate.projString="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=${outfile} --interpolate.method=coord_nearestneighbor  


#rotate vector

    ncrename --bfr_sz=4194304 -d y,eta_rho -d x,xi_rho -v x,xi_rho -v y,eta_rho $outfile
#angle from grid file
    angle=/cluster/home/${USER}/pytools_git/clima_from_topaz/angle_sa4.nc

    ncks --bfr_sz=4194304 -A $angle $outfile
    ncap --bfr_sz=4194304 -O -s "U=(u*cos(angle)+v*sin(angle));V=(v*cos(angle)-u*sin(angle));ubar=(ubarotrop*cos(angle)+vbarotrop*sin(angle));vbar=(vbarotrop*cos(angle)-ubarotrop*sin(angle))" -v $outfile temp.nc_rot
    ncrename --bfr_sz=4194304 -v U,u -v V,v temp.nc_rot 
    ncap2  --bfr_sz=4194304 -O -s "u=u/9.15667063455728e-05;v=v/9.15667063455728e-05;ubar=ubar/9.15667063455728e-05;vbar=vbar/9.15667063455728e-05" temp.nc_rot temp.nc_rot
    ncap2  --bfr_sz=4194304 -O -s "u=short(u);v=short(v);u@add_offset=0.0;v@add_offset=0.0;u@scale_factor=9.15667063455728e-05;v@scale_factor=9.15667063455728e-05;ubar=short(ubar);vbar=short(vbar);ubar@add_offset=0.0;vbar@add_offset=0.0;ubar@scale_factor=9.15667063455728e-05;vbar@scale_factor=9.15667063455728e-05"  temp.nc_rot temp.nc_rot

    ncrename --bfr_sz=4194304 -v ubarotrop,ubar -v vbarotrop,vbar ${outfile} 
#ncks -x -O -v v,u,vbarotrop,ubarotrop ${outfile} ${outfile}
#ncks --bfr_sz=4194304 -A temp.nc_rot ${outfile} 
#    ncks --bfr_sz=4194304 -A -v u,v,ubar,vbar temp.nc_rot ${outfile}
    ncks --bfr_sz=4194304 -A -v u,v temp.nc_rot ${outfile}
    ncks --bfr_sz=4194304 -A -v ubar,vbar temp.nc_rot ${outfile}_bar
    set +x
    source $HOME/.bashrc
    set -x
    python $HOME/pytools_git/clima_from_topaz/uvbar_fromrho.py -i ${outfile}_bar -o ${outfile}_baruv -grid /cluster/home/mitya/pytools_git/clima_from_topaz/arctic4km_grd_svalbard.nc
    python  $HOME/pytools_git/clima_from_topaz/topaz_vertical_transform_var.py -i ${outfile} -var u -grid /cluster/home/mitya/pytools_git/clima_from_topaz/arctic4km_grd_svalbard.nc
    python  $HOME/pytools_git/clima_from_topaz/topaz_vertical_transform_var.py -i ${outfile} -var v -grid /cluster/home/mitya/pytools_git/clima_from_topaz/arctic4km_grd_svalbard.nc
    python  $HOME/pytools_git/clima_from_topaz/topaz_vertical_transform_var.py -i ${outfile} -var salinity -grid /cluster/home/mitya/pytools_git/clima_from_topaz/arctic4km_grd_svalbard.nc
    python  $HOME/pytools_git/clima_from_topaz/topaz_vertical_transform_var.py -i ${outfile} -var temperature -grid /cluster/home/mitya/pytools_git/clima_from_topaz/arctic4km_grd_svalbard.nc
    ncks -A  ${outfile}_vert_u  ${outfile}_vert_v
    ncks -A  ${outfile}_vert_salinity  ${outfile}_vert_temperature
    ncks -A  ${outfile}_vert_v  ${outfile}_vert_temperature
    mv  ${outfile}_vert_temperature  ${outfile}_vert
    ncrename --bfr_sz=4194304 -d time,clim_time  ${outfile} 
    ncks --bfr_sz=4194304 -A -v zeta  ${outfile}  ${outfile}_vert
    ncks --bfr_sz=4194304 -A -v ubar,vbar  ${outfile}_baruv  ${outfile}_vert
    mv  ${outfile}_vert ${outfile}
    ncrename --bfr_sz=4194304 -v salinity,salt -v temperature,temp ${outfile}
    ncap2 --bfr_sz=4194304 -O -s "clim_time=clim_time/86400" ${outfile} ${outfile} 
    ncatted  --bfr_sz=4194304 -O   -a time,,a,c,clim_time -a units,clim_time,a,c,"days" -a  field,clim_time,a,c,"time, scalar, series"  -a long_name,clim_time,a,c,"days since 1948-01-01 00:00:00" ${outfile} ${outfile}  
    
#--output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml
    rm -f temp* 
    rm -f   ${outfile}_vert_u  ${outfile}_vert_v ${outfile}_vert_salinity  ${outfile}_vert_temperature ${outfile}_bar ${outfile}_baruv

done
set +x
exit
