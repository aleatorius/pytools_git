#!/bin/bash

# Set origin path
BASE=/cluster/work/users/mitya/topaz/s800
cd ${BASE}

echo "Make initial-file for 2014"
ncea -d clim_time,0,0 $BASE/TP4DAILY_start20140730_dump20140801.nc_s800.nc ${BASE}/svalbard_800m_ini.nc  # Get initial fields
ncrename -h -O -v clim_time,ocean_time ${BASE}/svalbard_800m_ini.nc                        # Change name of time variable
ncrename -h -O -d clim_time,time       ${BASE}/svalbard_800m_ini.nc                        # Change name of time dimension
for var in zeta ubar vbar u v salt temp; do  # Remove clim_time as attribute for every variable
  ncatted -h -O -a time,${var},d,c,"clim_time" ${BASE}/svalbard_800m_ini.nc
done

#ncap -O -h -s "snow_thick=hsno" ${BASE}/svalbard_800m_ini.nc -o ./tmp.nc  # Change variable name for hsno
#mv ./tmp.nc ${BASE}/svalbard_800m_ini.nc
#ncap -O -h -s "ti=tice"         ${BASE}/svalbard_800m_ini.nc -o ./tmp.nc  # Change variable name for tice
#mv ./tmp.nc ${BASE}/svalbard_800m_ini.nc
#for var in tisrf tau_iw chu_iw s0mk t0mk; do
#  ncap -O -h -s "${var}=zeta*0." ${BASE}/svalbard_800m_ini.nc -o ./tmp.nc  # Add sea ice variables to initial file (with 0-values)
#  mv ./tmp.nc ${BASE}/svalbard_800m_ini.nc
#done

ncatted -O -a units,ocean_time,c,c,"days since 1948-01-01 00:00:00" ${BASE}/svalbard_800m_ini.nc

exit
