#!/bin/bash

# Stallo


module --force  purge
module load StdEnv
module load intel/2017a
module load netCDF/4.4.1.1-intel-2017a-HDF5-1.8.18
module load Boost/1.63.0-intel-2017a-Python-2.7.13
module load PROJ/4.9.3-intel-2017a
module load UDUNITS/2.2.24-intel-2017a
module load NCO/4.6.6-intel-2017a
