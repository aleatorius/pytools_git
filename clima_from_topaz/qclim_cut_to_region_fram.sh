#!/bin/bash
#SBATCH -J matlab_spatial_int
#SBATCH --account nn9300k
##SBATCH --qos=preproc
##SBATCH --time=10:10:00
#SBATCH --partition=bigmem
#SBATCH --time=0-10:50:0
#SBATCH --nodes=1 --ntasks-per-node=1 
#SBATCH --mem=64G



workdir=/cluster/work/users/mitya/topaz/s800
#workdir=/cluster/home/mitya/pytools_git_uit/bioclim_from_topaz
homedir=/cluster/home/$USER
#pathTools=/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
#outdir=$workdir

cd $workdir
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>$workdir/bioclim.log_${datstamp} 2>&1
cd $workdir
datadir=/cluster/shared/arcticfjord/Annettes_files/2015/
set -x
for filein in /cluster/shared/arcticfjord/Annettes_files/2014/TP4DAILY_*201409*nc; do
#for filein in test.nc; do

    echo $filein

    ncatted -O -a units,time,d,, $filein temp.nc
    ncks -x -O -v hice,fice,fy_age,hsnow temp.nc temp.nc
    ncrename -O -v ssh,zeta temp.nc temp.nc
    set +x
    source $HOME/pytools_git/clima_from_topaz/modules_fram_fimex.sh
    set -x
    fimex --input.file=temp.nc --process.rotateVectorToLatLonX=u --process.rotateVectorToLatLonY=v  --process.rotateVectorToLatLonX=ubarotrop --process.rotateVectorToLatLonY=vbarotrop --output.file=temp_01.nc
#    fimex --input.file=temp.nc --process.rotateVectorToLatLonX=u --process.rotateVectorToLatLonY=v --output.file=temp_01.nc

    xvals="0,25000,...,1440800"
    yvals="0,25000,...,1080800"

    ${path2Fimex} --input.file=temp_01.nc  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=temp_02.nc   --interpolate.method=coord_kdtree  --interpolate.preprocess='fill2d(0.01, 1.6, 100)' 
#--output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml
#done

    xvals="0,800,...,1440800"
    yvals="0,800,...,1080800"

    ${path2Fimex} --input.file=temp_02.nc  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=${filein##*/}_s800.nc --interpolate.method=coord_nearestneighbor  

#cp ${filein##*/}_s800.nc ${filein##*/}_s800.nc_unrot
#rotate vector
    outfile=${filein##*/}_s800.nc
    ncrename --bfr_sz=4194304 -d y,eta_rho -d x,xi_rho -v x,xi_rho -v y,eta_rho $outfile

    angle=/cluster/home/${USER}/pytools_git/fimex_wtf/angle.nc

    ncks --bfr_sz=4194304 -A $angle $outfile
    ncap --bfr_sz=4194304 -O -s "U=(u*cos(angle)+v*sin(angle));V=(v*cos(angle)-u*sin(angle));ubar=(ubarotrop*cos(angle)+vbarotrop*sin(angle));vbar=(vbarotrop*cos(angle)-ubarotrop*sin(angle))" -v $outfile temp.nc_rot
    ncrename --bfr_sz=4194304 -v U,u -v V,v temp.nc_rot 
    ncap2  --bfr_sz=4194304 -O -s "u=u/9.15667063455728e-05;v=v/9.15667063455728e-05;ubar=ubar/9.15667063455728e-05;vbar=vbar/9.15667063455728e-05" temp.nc_rot temp.nc_rot
    ncap2  --bfr_sz=4194304 -O -s "u=short(u);v=short(v);u@add_offset=0.0;v@add_offset=0.0;u@scale_factor=9.15667063455728e-05;v@scale_factor=9.15667063455728e-05;ubar=short(ubar);vbar=short(vbar);ubar@add_offset=0.0;vbar@add_offset=0.0;ubar@scale_factor=9.15667063455728e-05;vbar@scale_factor=9.15667063455728e-05"  temp.nc_rot temp.nc_rot

    ncrename --bfr_sz=4194304 -v ubarotrop,ubar -v vbarotrop,vbar ${outfile} 
#ncks -x -O -v v,u,vbarotrop,ubarotrop ${outfile} ${outfile}
#ncks --bfr_sz=4194304 -A temp.nc_rot ${outfile} 
    ncks --bfr_sz=4194304 -A -v u,v,ubar,vbar temp.nc_rot ${outfile}
    set +x
    source $HOME/.bashrc
    set -x
    python  $HOME/pytools_git/clima_from_topaz/topaz_vertical_transform_var.py -i ${outfile} -var u
    python  $HOME/pytools_git/clima_from_topaz/topaz_vertical_transform_var.py -i ${outfile} -var v
    python  $HOME/pytools_git/clima_from_topaz/topaz_vertical_transform_var.py -i ${outfile} -var salinity
    python  $HOME/pytools_git/clima_from_topaz/topaz_vertical_transform_var.py -i ${outfile} -var temperature
    ncks -A  ${outfile}_vert_u  ${outfile}_vert_v
    ncks -A  ${outfile}_vert_salinity  ${outfile}_vert_temperature
    ncks -A  ${outfile}_vert_v  ${outfile}_vert_temperature
    mv  ${outfile}_vert_temperature  ${outfile}_vert
    rm -f   ${outfile}_vert_u  ${outfile}_vert_v ${outfile}_vert_salinity  ${outfile}_vert_temperature
    ncrename --bfr_sz=4194304 -d time,clim_time  ${outfile} 
    ncks --bfr_sz=4194304 -A -v ubar,vbar,zeta  ${outfile}  ${outfile}_vert
    mv  ${outfile}_vert ${outfile}
    ncrename --bfr_sz=4194304 -v salinity,salt -v temperature,temp ${outfile}
#--output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml
    rm -f temp* 
done
set +x
exit
