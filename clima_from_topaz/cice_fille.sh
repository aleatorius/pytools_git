#!/bin/bash
#SBATCH -J matlab_spatial_int
#SBATCH --account nn9300k
##SBATCH --qos=preproc
##SBATCH --time=10:10:00
#SBATCH --partition=bigmem
#SBATCH --time=0-10:50:0
#SBATCH --nodes=1 --ntasks-per-node=1 
#SBATCH --mem=64G



workdir=/cluster/work/users/$USER/
#workdir=/cluster/home/mitya/pytools_git_uit/bioclim_from_topaz
homedir=/cluster/home/$USER
#pathTools=/home/mitya/bin


cd $workdir
datstamp=`date +%Y_%m_%d_%H_%M`
#exec 1>$workdir/bioclim.log_${datstamp} 2>&1
cd $workdir

set -x
for filein in /cluster/work/users/mitya/tair_2014.nc; do
#for filein in test.nc; do

    echo $filein
    for i in {365..1469}; do

	ncks --bfr_sz=4194304 -O -d time,$i,$i $filein ${filein}_$i
	python $HOME/pytools_git/extra.py -v Tair -i ${filein}_$i -mask $HOME/pytools_git/mask_3.nc
    done
    
    string=""
    for i in {0..1469}; do
	string="$string ${filein}_$i"
    done
    ncrcat  --bfr_sz=4194304 $string output.nc
done
set +x
exit
