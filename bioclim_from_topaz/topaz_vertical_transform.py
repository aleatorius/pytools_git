#!/home/mitya/testenv/bin/python -B                                                                                   
import os
import sys
from datetime import datetime

import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import *
import numpy.ma as ma
import netcdftime
import time as tm
from calendar import monthrange
import datetime 
from datetime import date, time
import argparse
import z_coord
import vert


parser = argparse.ArgumentParser(description='transect write 0.1')

parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)
parser.add_argument(
'-zeta', 
help='input zeta file', 
dest='zeta', 
action="store"
)
parser.add_argument(
'-o', 
help='output file', 
dest='outf', 
action="store"
)
parser.add_argument(
'-grid', 
help='grid file for output', 
dest='grid', 
action="store",
default="/home/mitya/models/NorROMS/Apps/Common/Grid/arctic4km_grd.nc"
)

args = parser.parse_args()


#vertical coordinates parameters
Ni=75
No=35
theta_so=8.0; theta_bo=0.1; Tclineo=20.0
Vtransformo=2; Vstretchingo=2



try:
    mask_rho=f.variables["mask_rho"][:]
    hi = f.variables["h"][:]
except:
    grid = Dataset(args.grid)
    mask_rho=grid.variables["mask_rho"][:]
    hi = grid.variables["h"][:]
    grid.close()


f = Dataset(args.inf)
depth = f.variables["depth"][:]

try:
    zeta = f.variables["zeta"][0,:]
except OSError:
    zeta_f = Dataset(args.zeta)
    zeta=zeta_f.variables["zeta"][0.:]
    zeta_f.close()
except:
    print "zero zeta case"
    zeta = np.zeros(np.shape(hi))



print "hi", hi.shape

z_ro = z_coord.spec_vert_grid(No,hi,zeta,Tclineo,theta_bo,theta_so,Vtransformo,Vstretchingo,[zeta.shape[0],zeta.shape[1]])

print z_ro.shape
print np.amax(z_ro), np.amin(z_ro)
print "...."
print "depth shape", depth.shape
depth = depth[::-1]


undef=1.e+37


if not args.outf:
    nc = Dataset(args.inf.split(".")[0]+"_s800_vert_test_node.nc", 'w', format='NETCDF3_64BIT')
else:
    nc = Dataset(args.outf, 'w', format='NETCDF3_64BIT')


nc.createDimension('s_rho', No)   
nc.createDimension('eta_rho', mask_rho.shape[0])
nc.createDimension('xi_rho',mask_rho.shape[1])
nc.createDimension('clim_time', None) # unlimited axis (can be appended to).
nc.createVariable('clim_time',np.float64,('clim_time'))





#nc.setncattr("title","field "+args.field)


#

counter="in"



#for argsfield in ["nitrat"]:
for argsfield in ["OXY","NIT", "PHO", "SIL", "FLA", "DIA", "DET", "SIS", "DETP", "MICRO", "MESO"]: 
#for argsfield in ["OXY","NIT", "PHO"]: 
    print argsfield
    field = f.variables[argsfield][0,::-1,:]

    print field.shape, "field"
    field_tr = np.transpose(field, (1,2,0))
    print field_tr.shape, "transpose"
    if counter == "in":
        z_ri= np.zeros(field.shape)
        print "z_ri.shape", z_ri.shape
        z_ri.T[:]=-depth
        z_ri_tran= np.transpose(z_ri, (1,2,0))
        print z_ri_tran.shape, "z_ri_tran shape"
        counter = "out"
    else:
        pass
    array_out = vert.vert_int(field_tr,z_ri_tran,z_ro,undef,mask_rho,mask_rho,[mask_rho.shape[1],mask_rho.shape[0],Ni,No])
    #sanitizing, we deal with concentrations, no negatives (which may pop up when interpolating)
    array_out[ array_out<0 ] = 0
    print array_out.shape, "array_out" 
    molar_mass_nit = 62.0049
    molar_mass_pho = 94.9714
    molar_mass_sil = 96.1149
    molar_mass_o2 = 31.99880
        
    if argsfield == "NIT":
        out_field =  nc.createVariable('NO3', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "Mole Concentration of Nitrate in Sea Water" 
        out_field.units = "mmol.m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series" 
        nc.variables["NO3"][0,:] = (1/molar_mass_nit)*np.transpose(array_out, (2,0,1))
    elif argsfield == "PHO":
        out_field =  nc.createVariable('PO4', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "mole_concentration_of_phosphate_in_sea_water" 
        out_field.units = "mmol m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["PO4"][0,:] = (1/molar_mass_pho)*np.transpose(array_out, (2,0,1))
    elif argsfield == "SIL":
        out_field =  nc.createVariable('SiOH4', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "mole_concentration_of_Si_in_sea_water" 
        out_field.units = "mmol m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["SiOH4"][0,:] = (1/molar_mass_sil)*np.transpose(array_out, (2,0,1))
    elif argsfield == "FLA":
        out_field =  nc.createVariable('Phy_N', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "mole_concentration_of_fla_in_sea_water" 
        out_field.units = "mmol m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["Phy_N"][0,:] = (1/molar_mass_nit)*np.transpose(array_out, (2,0,1))

    elif argsfield == "DIA":
        out_field =  nc.createVariable('diatomN', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "mole_concentration_of_diatom_N_in_sea_water" 
        out_field.units = "mmol m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["diatomN"][0,:] = (1/molar_mass_nit)*np.transpose(array_out, (2,0,1))

    elif argsfield == "DET":
        out_field =  nc.createVariable('SdetritusN', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "mole_concentration_of_small fraction nitrogen detritus concentration_in_sea_water" 
        out_field.units = "mmol m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["SdetritusN"][0,:] = (1/molar_mass_nit)*np.transpose(array_out, (2,0,1))

    elif argsfield == "DETP":
        out_field =  nc.createVariable('SdetritusP', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "Mole Concentration of small fraction phosphorus detritus concentration in sea water" 
        out_field.units = "mmol.m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["SdetritusP"][0,:] = (1/molar_mass_pho)*np.transpose(array_out, (2,0,1))
    elif argsfield == "MESO":
        out_field =  nc.createVariable('zooplanktonN', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "Mole Concentration of Zooplankton expressed as nitrogen in sea water" 
        out_field.units = "mmol_N m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["zooplanktonN"][0,:] = (1/molar_mass_nit)*np.transpose(array_out, (2,0,1))
    elif argsfield == "MICRO":
        out_field =  nc.createVariable('micro_zooplankton', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "mole zooplankton concentration expressed as phosphorus_in_sea_water" 
        out_field.units = "mmol_N m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["micro_zooplankton"][0,:] = (1/molar_mass_nit)*np.transpose(array_out, (2,0,1))
    elif argsfield == "OXY":
        out_field =  nc.createVariable('oxygen', np.float64, ('clim_time','s_rho','eta_rho','xi_rho'))
        out_field.long_name = "millimole concentration_of_oxygen_in_sea_water" 
        out_field.units = "mmol_O m-3" 
        out_field.time = "clim_time" 
        out_field.field = "scalar, series"             
        nc.variables["oxygen"][0,:] = (1/molar_mass_o2)*np.transpose(array_out, (2,0,1))


    else:
        pass

ref_roms = date(1970,01,01)
ref_time = time(0,0,0)
refer_roms= datetime.datetime.combine(ref_roms, ref_time)
ref_topaz = date(1950,01,01)
refer_topaz = datetime.datetime.combine(ref_topaz, ref_time)

nc.variables['clim_time'][:]= np.array(f.variables['time'][:]*60*60-(ref_roms-ref_topaz).total_seconds())

nc.close()
f.close()
