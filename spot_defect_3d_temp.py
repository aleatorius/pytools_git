#!/usr/bin/env python
##!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm
import numpy.ma as ma
import matplotlib.dates as mdates
parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)
parser.add_argument(
'-grid', 
help='input file', 
dest='grid', 
action="store",
default="/cluster/projects/nn9300k/metroms_input/svalbard_800m_grid_uvpsi.nc"
)
parser.add_argument(
'-v', 
help='variable', 
dest='var', 
action="store"
)
parser.add_argument(
'-defect', 
help='input grid file', 
dest='defect', 
action="store",
default=-2,
type=float
)


parser.add_argument('--var_min', help='minimum value of variable', dest ='var_min', action="store", type=float, default = None)
parser.add_argument('--var_max', help='minimum value of variable', dest ='var_max', action="store", type=float, default = None)


args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa



f = Dataset(args.inf)
var = unpack(f.variables[args.var])
bath=unpack(f.variables["h"])
print var.shape, "var_shape"


g = Dataset(args.grid)
mask_rho = unpack(g.variables["mask_rho"])
mask_u = unpack(g.variables["mask_u"])
mask_v =unpack(g.variables["mask_v"])
mask_psi =unpack(g.variables["mask_psi"])
g.close()
e_pos=[]
x_pos=[]
var_masked=ma.masked_outside(var[-1,-1,:,:],-1e+35,1e+35)
print var_masked
print var_masked.mask
print var_masked.min(), var_masked.max(), "minmax"
for i,j in np.ndenumerate(var_masked.mask):
    if j==False:
        if var_masked[i]<args.defect:
            print i,j, var_masked[i],bath[i],mask_rho[i]
            x_pos.append(i[0])
            e_pos.append(i[1])
print len(x_pos)

fig = plt.figure(figsize=(12,8))
ax = fig.add_subplot(111)
palette = plt.cm.plasma
p=ax.pcolormesh(var_masked, cmap=palette)
#p=ax.pcolormesh(ma.masked_outside(bath,-1e+36,1e+36), cmap=palette)
#p=ax.pcolormesh(ma.masked_where(mask_rho[0]==0,bath), cmap=palette)
plt.colorbar(p)
if args.var_min or args.var_max:
    if args.var_min and args.var_max:
        if args.var_min < args.var_max:
            p.set_clim(float(args.var_min), float(args.var_max))
        else:
            print 'incorrect var_min shoud be less than var_max, defaults then'
            pass
    else:
        p.set_clim(args.var_min, args.var_max)
else:
     pass

    #delta = 1
    #plt.Rectangle((river_x[hplot-1], river_e[hplot-1]),width=delta,height=delta,color='red') 
    
ax.plot(e_pos,x_pos,'rs', markersize=3)
plt.show()

