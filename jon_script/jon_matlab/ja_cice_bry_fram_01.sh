#!/bin/bash
##SBATCH --nodes=1 --ntasks-per-node=1 --qos=devel
##SBATCH --partition=normal
#SBATCH --time=00-12:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-192
#SBATCH --qos=preproc                                                                                                         

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1



set -x



## chunk*number of array should give a number of records exactly


#infilestrip=$(basename -- "$filein")
#outfile=${infilestrip%.*}_${range1}_${range2}.nc

year="2014"

SCRATCH_DIRECTORY=/cluster/work/users/${USER}/job_array/${year}_01/${SLURM_ARRAY_TASK_ID}     #${SLURM_JOBID}

if [ ! -d "${SCRATCH_DIRECTORY}" ]; then
    mkdir -p ${SCRATCH_DIRECTORY}
fi

source $HOME/jon_script/jon_matlab/modules.sh
cd ${SCRATCH_DIRECTORY}

START_TIME=$SECONDS
if [ ! -f "${SCRATCH_DIRECTORY}/slice.nc" ]; then
    ncks --bfr_sz=4194304 -d Time,$(( SLURM_ARRAY_TASK_ID - 1 )) /cluster/shared/arcticfjord/Annettes_files/topaz/s800/Processed_TOPAZ_ice_data_2014_day1_day190.nc  ${SCRATCH_DIRECTORY}/slice.nc 
fi

ELAPSED_TIME=$(($SECONDS - $START_TIME))

ncgen -o ${SCRATCH_DIRECTORY}/bry.nc ${HOME}/jon_script/jon_matlab/BRY_${year}.cdl

cp ${HOME}/jon_script/jon_matlab/ja_cice_bry_slice.m .
sed -i "s/SLICEREC/$(( SLURM_ARRAY_TASK_ID -1 ))/g" ja_cice_bry_slice.m

START_TIME=$SECONDS
module load MATLAB/2020a

/cluster/software/MATLAB/2020a/bin/matlab -nodisplay -nodesktop -nosplash -r "run ja_cice_bry_slice.m"
mv bry.nc ../bry.nc_${SLURM_ARRAY_TASK_ID}
ELAPSED_TIME=$(($SECONDS - $START_TIME))	



exit 0


