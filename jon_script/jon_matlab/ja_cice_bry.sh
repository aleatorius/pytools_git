#!/bin/bash
##SBATCH --nodes=1 --ntasks-per-node=1 --qos=devel
##SBATCH --partition=normal
#SBATCH --time=00-08:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-366
                                                                                                                     

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1



set -x



## chunk*number of array should give a number of records exactly


#infilestrip=$(basename -- "$filein")
#outfile=${infilestrip%.*}_${range1}_${range2}.nc

year="2010"

SCRATCH_DIRECTORY=/global/work/${USER}/job_array/$year/${SLURM_ARRAY_TASK_ID}     #${SLURM_JOBID}

if [ ! -d "${SCRATCH_DIRECTORY}" ]; then
    mkdir -p ${SCRATCH_DIRECTORY}
fi

source /home/mitya/jon_script/jon_matlab/modules.sh
cd ${SCRATCH_DIRECTORY}

START_TIME=$SECONDS
if [ ! -f "${SCRATCH_DIRECTORY}/slice.nc" ]; then
    ncks -d Time,$(( SLURM_ARRAY_TASK_ID - 1 )) /global/work/pduarte/tmproms/run/S800/Processed_A4km_ice_data_${year}.nc  ${SCRATCH_DIRECTORY}/slice.nc 
fi

ELAPSED_TIME=$(($SECONDS - $START_TIME))

ncgen -o ${SCRATCH_DIRECTORY}/bry.nc /home/${USER}/jon_script/jon_matlab/BRY_${year}.cdl

cp /home/${USER}/jon_script/jon_matlab/ja_cice_bry_slice.m .
sed -i "s/SLICEREC/$(( SLURM_ARRAY_TASK_ID -1 ))/g" ja_cice_bry_slice.m

START_TIME=$SECONDS
module load MATLAB/R2015a
/global/apps/MATLAB/R2015a/bin/matlab -nodisplay -nodesktop -nosplash -r "run ja_cice_bry_slice.m"
mv bry.nc ../bry.nc_${SLURM_ARRAY_TASK_ID}
ELAPSED_TIME=$(($SECONDS - $START_TIME))	



exit 0


