clear all; close all;

year = 2014;

% CICE-file from A4: Can be located at: stallo:/global/work/thinf/metroms_input/s800_cice_bc
%cfil = ['/global/work/pduarte/tmproms/run/S800/New_processed_A4km_ice_data_',int2str(year),'.nc'];
cfil = ['/cluster/work/users/mitya/ice_bry/slice_0.nc'];

% New BRY output-file
%bryfil = ['/gloBRY_',int2str(year),'.nc'];  % Template must be generated before script is executed: ncgen -o BRY_${year}.nc BRY_${year}.cdl
bryfil = ['/cluster/work/users/mitya/ice_bry/bry_slice_0.nc'];  % Template must be generated before script is executed: ncgen -o BRY_${year}.nc BRY_${year}.cdl

% ROMS grid-file
rfil = '/cluster/shared/arcticfjord/input_data/s800_cice_processed/svalbard_800m_grid_uvpsi.nc';
lon_rho = ncread(rfil,'lon_rho');
lat_rho = ncread(rfil,'lat_rho');

% Define lon/lat vectors for the boundary CICE-grid
TLON_N = lon_rho(1:end,end); TLAT_N = lat_rho(1:end,end);
TLON_S = lon_rho(1:end,1);   TLAT_S = lat_rho(1:end,1);
TLON_E = lon_rho(end,1:end); TLAT_E = lat_rho(end,1:end);
TLON_W = lon_rho(1,1:end);   TLAT_W = lat_rho(1,1:end);

% Print lon/lat to BRY-file
ncwrite(bryfil,'TLON_N',TLON_N); ncwrite(bryfil,'TLAT_N',TLAT_N);
ncwrite(bryfil,'TLON_S',TLON_S); ncwrite(bryfil,'TLAT_S',TLAT_S);
ncwrite(bryfil,'TLON_E',TLON_E); ncwrite(bryfil,'TLAT_E',TLAT_E);
ncwrite(bryfil,'TLON_W',TLON_W); ncwrite(bryfil,'TLAT_W',TLAT_W);

% Transfer Ice_layers and Snow_layers to BRY-file
Ice_layers = ncread(cfil,'Ice_layers'); ncwrite(bryfil,'Ice_layers',Ice_layers);
Snow_layers = ncread(cfil,'Snow_layers'); ncwrite(bryfil,'Snow_layers',Snow_layers);

% Transfer time-variable to BRY-file
%day = ncread(cfil,'Time');
%if year == 2007
%  startday = 365 - length(day) + 1;
%  dayout = [0:364];
%else
%  startday = 1;
%  dayout = day;
%end
startday = 1;
dayout = [0:0];
ncwrite(bryfil,'Time',dayout);

% Read lon/lat from coarse field
lon_coarse = double(ncread(cfil,'TLON'));
for i=1:size(lon_coarse,1)
for j=1:size(lon_coarse,2)
  if lon_coarse(i,j) > 180; lon_coarse(i,j) = lon_coarse(i,j) - 360.; end
end
end
lat_coarse = double(ncread(cfil,'TLAT'));

% Read, interpolate and write ice age
varname = 'iage';
ifield = ncread(cfil,varname);
for t=1:1 %length(day):length(day)
  F = scatteredInterpolant(reshape(lon_coarse,numel(lon_coarse),1),reshape(lat_coarse,numel(lat_coarse),1),reshape(ifield(:,:,t),numel(lon_coarse),1),'linear','none');
  ofield_N(1:length(TLON_N),t+startday-1) = F(TLON_N,TLAT_N);
  ofield_S(1:length(TLON_S),t+startday-1) = F(TLON_S,TLAT_S);
  ofield_E(1:length(TLON_E),t+startday-1) = F(TLON_E,TLAT_E);
  ofield_W(1:length(TLON_W),t+startday-1) = F(TLON_W,TLAT_W);
  clear F
  disp(['Finished ',varname]);
end  % t
ofield_N(isnan(ofield_N)) = 0.; ofield_S(isnan(ofield_S)) = 0.; ofield_E(isnan(ofield_E)) = 0.; ofield_W(isnan(ofield_W)) = 0.;
ncwrite(bryfil,[varname,'_N_bry'],ofield_N);
ncwrite(bryfil,[varname,'_S_bry'],ofield_S);
ncwrite(bryfil,[varname,'_E_bry'],ofield_E);
ncwrite(bryfil,[varname,'_W_bry'],ofield_W);
clear ifield t ofield_N ofield_S ofield_E ofield_W varname

% Read, interpolate and write ice variables
npars = 11;
for par=1:npars
  switch par
  case  1; varname = 'aicen';
  case  2; varname = 'vicen';
  case  3; varname = 'vsnon';
  case  4; varname = 'Tsfc';
  case  5; varname = 'alvln';
  case  6; varname = 'vlvln';
  case  7; varname = 'apondn';
  case  8; varname = 'hpondn';
  case  9; varname = 'ipondn';
  case 10; varname = 'hbrine';
  case 11; varname = 'fbrine';
  end
  ifield = ncread(cfil,varname); NIT = size(ifield,3);
  if 0  % Plot coarse field example and position of boundary point to check positioning
    figure(1)
    clf
    m_proj('lambert','long',[min(min(lon_rho))-5. max(max(lon_rho))+5.],'lat',[min(min(lat_rho))-2. max(max(lat_rho))+2.]);
    m_contourf(lon_coarse,lat_coarse,ifield(:,:,1),'LineWidth',0.1); colorbar; hold on;
    m_plot(TLON_N,TLAT_N,'r*'); hold on;
    m_plot(TLON_S,TLAT_S,'r*'); hold on;
    m_plot(TLON_E,TLAT_E,'m*'); hold on;
    m_plot(TLON_W,TLAT_W,'m*'); hold on;
    m_grid('box','fancy','tickdir','in');
    print('-dpng','-r300',['svalbard_800m_bry.png']);
  end  % if 0
  for t=1:1 %length(day):length(day)
    for it=1:NIT
      F = scatteredInterpolant(reshape(lon_coarse,numel(lon_coarse),1),reshape(lat_coarse,numel(lat_coarse),1),reshape(ifield(:,:,it,t),numel(lon_coarse),1),'linear','none');
      ofield_N(1:length(TLON_N),it,t+startday-1) = F(TLON_N,TLAT_N);
      ofield_S(1:length(TLON_S),it,t+startday-1) = F(TLON_S,TLAT_S);
      ofield_E(1:length(TLON_E),it,t+startday-1) = F(TLON_E,TLAT_E);
      ofield_W(1:length(TLON_W),it,t+startday-1) = F(TLON_W,TLAT_W);
      clear F
    end  % it
    disp(['Finished ',varname]);
  end  % t
  ofield_N(isnan(ofield_N)) = 0.; ofield_S(isnan(ofield_S)) = 0.; ofield_E(isnan(ofield_E)) = 0.; ofield_W(isnan(ofield_W)) = 0.;
  ncwrite(bryfil,[varname,'_N_bry'],ofield_N);
  ncwrite(bryfil,[varname,'_S_bry'],ofield_S);
  ncwrite(bryfil,[varname,'_E_bry'],ofield_E);
  ncwrite(bryfil,[varname,'_W_bry'],ofield_W);
  clear ifield t it ofield_N ofield_S ofield_E ofield_W varname
end  % par

% Read, interpolate and write ice layer dependent ice variables
for par=1:2
  switch par
  case 1; varname = 'Tinz';
  case 2; varname = 'Sinz';
  end
  for t=1:1 %length(day):length(day)
    ifield = ncread(cfil,varname,[1 1 1 1 t],[Inf Inf Inf Inf 1]);
    for it=1:NIT
      for il=1:length(Ice_layers)
        F = scatteredInterpolant(reshape(lon_coarse,numel(lon_coarse),1),reshape(lat_coarse,numel(lat_coarse),1),reshape(ifield(:,:,il,it),numel(lon_coarse),1),'linear','none');
        ofield_N(1:length(TLON_N),il,it,t+startday-1) = F(TLON_N,TLAT_N);
        ofield_S(1:length(TLON_S),il,it,t+startday-1) = F(TLON_S,TLAT_S);
        ofield_E(1:length(TLON_E),il,it,t+startday-1) = F(TLON_E,TLAT_E);
        ofield_W(1:length(TLON_W),il,it,t+startday-1) = F(TLON_W,TLAT_W);
        clear F
      end  % il
    end  % it
    clear ifield
    disp(['Finished ',varname]);
  end  % t
  ofield_N(isnan(ofield_N)) = 0.; ofield_S(isnan(ofield_S)) = 0.; ofield_E(isnan(ofield_E)) = 0.; ofield_W(isnan(ofield_W)) = 0.;
  ncwrite(bryfil,[varname,'_N_bry'],ofield_N);
  ncwrite(bryfil,[varname,'_S_bry'],ofield_S);
  ncwrite(bryfil,[varname,'_E_bry'],ofield_E);
  ncwrite(bryfil,[varname,'_W_bry'],ofield_W);
  clear t it ofield_N ofield_S ofield_E ofield_W varname
end

% Read, interpolate and write snow layer dependent ice variables
for par=1:1
  switch par
  case 1; varname = 'Tsnz';
  end
  for t=1:1 %length(day):length(day)
    ifield = ncread(cfil,varname,[1 1 1 1 t],[Inf Inf Inf Inf 1]);
    for it=1:NIT
      for il=1:length(Snow_layers)
        F = scatteredInterpolant(reshape(lon_coarse,numel(lon_coarse),1),reshape(lat_coarse,numel(lat_coarse),1),reshape(ifield(:,:,il,it),numel(lon_coarse),1),'linear','none');
        ofield_N(1:length(TLON_N),il,it,t+startday-1) = F(TLON_N,TLAT_N);
        ofield_S(1:length(TLON_S),il,it,t+startday-1) = F(TLON_S,TLAT_S);
        ofield_E(1:length(TLON_E),il,it,t+startday-1) = F(TLON_E,TLAT_E);
        ofield_W(1:length(TLON_W),il,it,t+startday-1) = F(TLON_W,TLAT_W);
        clear F
      end  % il
    end  % it
    clear ifield
    disp(['Finished ',varname]);
  end  % t
  ofield_N(isnan(ofield_N)) = 0.; ofield_S(isnan(ofield_S)) = 0.; ofield_E(isnan(ofield_E)) = 0.; ofield_W(isnan(ofield_W)) = 0.;
  ncwrite(bryfil,[varname,'_N_bry'],ofield_N);
  ncwrite(bryfil,[varname,'_S_bry'],ofield_S);
  ncwrite(bryfil,[varname,'_E_bry'],ofield_E);
  ncwrite(bryfil,[varname,'_W_bry'],ofield_W);
  clear t it ofield_N ofield_S ofield_E ofield_W varname
end
