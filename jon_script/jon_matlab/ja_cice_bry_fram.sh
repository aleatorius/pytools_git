#!/bin/bash
##SBATCH --nodes=1 --ntasks-per-node=1 --qos=devel
##SBATCH --partition=normal
#SBATCH --time=00-10:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-252
#SBATCH --qos=preproc                                                                                                         

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1

module purge
module load NCO/4.6.6-intel-2017a

set -x



## chunk*number of array should give a number of records exactly


#infilestrip=$(basename -- "$filein")
#outfile=${infilestrip%.*}_${range1}_${range2}.nc

year="2015"

SCRATCH_DIRECTORY=/cluster/work/users/${USER}/job_array/${year}_filtered/${SLURM_ARRAY_TASK_ID}     #${SLURM_JOBID}

if [ ! -d "${SCRATCH_DIRECTORY}" ]; then
    mkdir -p ${SCRATCH_DIRECTORY}
fi

source $HOME/jon_script/jon_matlab/modules.sh
cd ${SCRATCH_DIRECTORY}

START_TIME=$SECONDS
if [ ! -f "${SCRATCH_DIRECTORY}/slice.nc" ]; then
    ncks --bfr_sz=4194304 -d Time,$(( SLURM_ARRAY_TASK_ID - 1 )) /cluster/shared/arcticfjord/Annettes_files/filtered/processed_2015_filtered.nc  ${SCRATCH_DIRECTORY}/slice.nc 
fi

ncap2 -O -s "var_tmp=aicen; where (aicen < 0.0001) var_tmp=0.0001; aicen=var_tmp; tmp_ice=vicen/aicen; tmp_snow=vsnon/aicen; tmp_vicen=vicen; tmp_vsnon=vsnon; where( tmp_ice == 0) tmp_vicen=0.0000001;  where (tmp_snow==0) tmp_vsnon=0.000000001; vicen=tmp_vicen; vsnon=tmp_vsnon; test_ice=vicen/aicen; test_snow=vsnon/aicen" slice.nc slice.nc  
#ncks -O -x -v tmp_vicen,tmp_vsnon,var_tmp,tmp_ice,tmp_snow slice.nc slice.nc

ELAPSED_TIME=$(($SECONDS - $START_TIME))

ncgen -o ${SCRATCH_DIRECTORY}/bry.nc ${HOME}/jon_script/jon_matlab/BRY_${year}.cdl

cp ${HOME}/jon_script/jon_matlab/ja_cice_bry_slice.m .
sed -i "s/SLICEREC/$(( SLURM_ARRAY_TASK_ID -1 ))/g" ja_cice_bry_slice.m

START_TIME=$SECONDS
module load MATLAB/2020a

/cluster/software/MATLAB/2020a/bin/matlab -nodisplay -nodesktop -nosplash -r "run ja_cice_bry_slice.m"
mv bry.nc ../bry.nc_${SLURM_ARRAY_TASK_ID}
ELAPSED_TIME=$(($SECONDS - $START_TIME))	



exit 0


