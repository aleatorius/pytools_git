PROGRAM roms2roms_horverinterp
! Program to obtain nesting (clima) conditions from ROMS history files
! Created by Jon Albretsen, IMR, Dec 2010, based on programs by Paul Budgell, IMR.
! Edited by Andre Staalstrom, NIVA, Apr 2011
! 2013.04.10 Add reading sea ice variables (Jon Albretsen)
! 2015.02.23 Modified to be able to read fields stored as short (Jon Albretsen)
! 2016.10.26 Read vertical grid parameters from input files and find output grid locations in input grid based on lon/lat-arrays (Jon Albretsen)

use netcdf

implicit none

INTEGER :: statusi, ncgridi, ncidi, nc, feof
INTEGER :: dim_xi_rhoi, dim_eta_rhoi, dim_s_rhoi, dim_xi_ui, dim_eta_ui, dim_xi_vi, dim_eta_vi
INTEGER :: dim_timei, dim_timet, dim_timec
INTEGER :: UVarIdi, VVarIdi, SaltVarIdi, TempVarIdi, TimeVarIdi
INTEGER :: UbarVarIdi, VbarVarIdi, ZetaVarIdi
INTEGER :: id_vtransformi, id_vstretchingi, id_theta_si, id_theta_bi, id_tclinei
INTEGER :: id_lon_rhoi, id_lat_rhoi, id_anglei, id_hi
INTEGER :: id_rmaski, id_umaski, id_vmaski
INTEGER :: Lpi, Mpi, Li, Mi, Ni, Iti, Kt, Itc
REAL, DIMENSION(:,:), allocatable :: lon_rhoi
REAL, DIMENSION(:,:), allocatable :: lat_rhoi
REAL, DIMENSION(:,:), allocatable :: anglei
REAL, DIMENSION(:,:), allocatable :: hi
REAL, DIMENSION(:,:), allocatable :: rmaski
REAL, DIMENSION(:,:), allocatable :: umaski
REAL, DIMENSION(:,:), allocatable :: vmaski
REAL, DIMENSION(:),   allocatable :: sc_ri
REAL, DIMENSION(:),   allocatable :: Cs_ri
REAL, DIMENSION(:,:,:), allocatable :: z_ri
REAL, DIMENSION(:,:,:), allocatable :: z_ri_interp
REAL :: hmini, Tclinei, theta_si, theta_bi, hci, cff1i, cff2i
REAL :: cffi, cff_ri, cff1_ri, cff2_ri

REAL :: Vtransformi, Vstretchingi
REAL :: ds, Csur, Cbot, Aweight, Bweight, cweight, hinv
REAL :: time_in, time_in_prev, tday
character(len=80) :: xi_dimnamei, eta_dimnamei, s_dimnamei, time_dimnamei

REAL :: Temp_scale_factor, Temp_add_offset, Salt_scale_factor, Salt_add_offset
REAL :: U_scale_factor, U_add_offset, V_scale_factor, V_add_offset
REAL :: Zeta_scale_factor, Zeta_add_offset
REAL :: Ubar_scale_factor, Ubar_add_offset, Vbar_scale_factor, Vbar_add_offset

INTEGER :: statuso, ncgrido, ncido
INTEGER :: dim_xi_rhoo, dim_eta_rhoo
INTEGER :: dim_xi_uo, dim_eta_uo
INTEGER :: dim_xi_vo, dim_eta_vo
INTEGER :: dim_s_rhoo, dim_timeo
INTEGER :: UVarIdo, VVarIdo, SaltVarIdo, TempVarIdo, TimeVarIdo
INTEGER :: UbarVarIdo, VbarVarIdo, ZetaVarIdo
INTEGER :: id_lon_rhoo, id_lat_rhoo, id_angleo, id_ho
INTEGER :: Lpo, Mpo, Lo, Mo, No

REAL, DIMENSION(:,:), allocatable :: lon_rhoo
REAL, DIMENSION(:,:), allocatable :: lat_rhoo
REAL, DIMENSION(:,:), allocatable :: angleo
REAL, DIMENSION(:,:), allocatable :: ho
REAL, DIMENSION(:), allocatable   :: sc_ro
REAL, DIMENSION(:), allocatable   :: Cs_ro

REAL, DIMENSION(:,:,:), allocatable :: z_ro, ds_ro
REAL, DIMENSION(:,:), allocatable :: sum_ds
REAL :: hmino, Tclineo, theta_so, theta_bo, hco, cff1o, cff2o
REAL :: cffo, cff_ro, cff1_ro, cff2_ro
! ans@niva.no included some variables
REAL :: Vtransformo, Vstretchingo
REAL :: alfao, betao
character(len=99) :: xi_dimnameo, eta_dimnameo

INTEGER :: i, j, k, itime, otime
INTEGER, PARAMETER :: iunit = 5

character (len=200) :: gridfilei, gridfileo, avgfilei, avg_path, avgfile
character (len=200) :: avgfilei_lst
character (len=200) :: clim_root, outfile, clim_path

INTEGER :: nof, ii, jj

REAL :: pi, DTOR
REAL :: xp, yp, ylon
REAL :: dx, phi, lambda, rearth, r, alpha, phinull
REAL :: dist, mindist

REAL, DIMENSION(:,:), allocatable :: ipos
REAL, DIMENSION(:,:), allocatable :: jpos
REAL, DIMENSION(:,:), allocatable :: work
REAL, DIMENSION(:,:), allocatable :: scr
REAL, DIMENSION(:,:), allocatable :: scr1
REAL, DIMENSION(:,:), allocatable :: scra
REAL, DIMENSION(:,:), allocatable :: scrb
REAL, DIMENSION(:,:), allocatable :: scru
REAL, DIMENSION(:,:), allocatable :: scrv
REAL, DIMENSION(:,:), allocatable :: scr1o
REAL, DIMENSION(:,:), allocatable :: scr2o
REAL, DIMENSION(:,:), allocatable :: scrao
REAL, DIMENSION(:,:), allocatable :: scrbo
REAL, DIMENSION(:,:), allocatable :: scruo
REAL, DIMENSION(:,:), allocatable :: scrvo
REAL, DIMENSION(:,:), allocatable :: error

REAL, DIMENSION(:,:,:), allocatable :: coef
REAL, DIMENSION(:), allocatable   :: clim_time

INTEGER :: nvalue
REAL, PARAMETER    :: undef = 2.E+35            ! Undefined land value
REAL :: tx, critx, cor
INTEGER :: mxs
REAL :: rimin, rimax, rjmin, rjmax, rx, ry, rxm, rym, rz1, rz2
INTEGER :: i1, i2, j1, j2, ia, ib, ja, margin

REAL, DIMENSION(:,:,:), ALLOCATABLE :: temp_in
REAL, DIMENSION(:,:,:), ALLOCATABLE :: salt_in
REAL, DIMENSION(:,:,:), ALLOCATABLE :: u_in
REAL, DIMENSION(:,:,:), ALLOCATABLE :: v_in
REAL, DIMENSION(:,:,:), ALLOCATABLE :: u_rhoi
REAL, DIMENSION(:,:,:), ALLOCATABLE :: v_rhoi
REAL, DIMENSION(:,:), ALLOCATABLE :: ubar_in
REAL, DIMENSION(:,:), ALLOCATABLE :: vbar_in
REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_in

REAL, DIMENSION(:,:,:), ALLOCATABLE :: temp_out
REAL, DIMENSION(:,:,:), ALLOCATABLE :: salt_out
REAL, DIMENSION(:,:,:), ALLOCATABLE :: u_out
REAL, DIMENSION(:,:,:), ALLOCATABLE :: v_out
REAL, DIMENSION(:,:,:), ALLOCATABLE :: u_rhoo
REAL, DIMENSION(:,:,:), ALLOCATABLE :: v_rhoo
REAL, DIMENSION(:,:), ALLOCATABLE :: ubar_out
REAL, DIMENSION(:,:), ALLOCATABLE :: vbar_out
REAL, DIMENSION(:,:), ALLOCATABLE :: zeta_out

REAL, DIMENSION(:,:,:), ALLOCATABLE :: scr3d

REAL :: minv, maxv

! -----------------------------------------------------------
! Read standard input

read(5,'(a)') gridfilei                    ! Name of one of the files listed in file list
!JAread(5,*) Ni, theta_si, theta_bi, Tclinei  ! Vertical grid parameters for input grid
!JAread(5,*) Vtransformi, Vstretchingi        ! Vertical transform and stretching
!JAread(5,*) xp, yp, dx, ylon                 ! Pol. stereographic paramaters of the input grid
read(5,'(a)') avgfilei_lst                 ! List of input files
read(5,'(a)') avg_path                     ! Path where input files are located
read(5,'(a)') gridfileo                    ! Grid file for the output domain
read(5,*) No, theta_so, theta_bo, Tclineo  ! Vertical grid parameters for output grid
read(5,*) Vtransformo, Vstretchingo        ! Vertical transform and stretching
read(5,'(a)') clim_path                    ! Path where new clima file is located
read(5,'(a)') clim_root                    ! Used in name of clima-file

pi = ATAN(1.)*4.
DTOR = pi/180.
tx = 0.9*undef
critx = 0.01
cor = 1.6
mxs = 100

! ------------------------------------------------------------
! Open input grid file and get info

statusi = nf90_open(trim(gridfilei),nf90_nowrite,ncgridi)
statusi = nf90_inq_dimid(ncgridi,'xi_rho',dim_xi_rhoi)
statusi = nf90_inq_dimid(ncgridi,'eta_rho',dim_eta_rhoi)
statusi = nf90_inq_dimid(ncgridi,'s_rho',dim_s_rhoi)
statusi = nf90_Inquire_Dimension(ncgridi,dim_xi_rhoi,xi_dimnamei,Lpi)
statusi = nf90_Inquire_Dimension(ncgridi,dim_eta_rhoi,eta_dimnamei,Mpi)
statusi = nf90_Inquire_Dimension(ncgridi,dim_s_rhoi,s_dimnamei,Ni)

Mi = Mpi-1
Li = Lpi-1

write(*,*) 'Lpi, Mpi, Ni = ',Lpi, Mpi, Ni

allocate(lon_rhoi(Lpi,Mpi))
allocate(lat_rhoi(Lpi,Mpi))
allocate(anglei(Lpi,Mpi))
allocate(hi(Lpi,Mpi))
allocate(rmaski(Lpi,Mpi))
allocate(umaski(Li,Mpi))
allocate(vmaski(Lpi,Mi))
allocate(work(Lpi,Mpi))
allocate(scr(Lpi,Mpi))
allocate(scr1(Lpi,Mpi))
allocate(scra(Lpi,Mpi))
allocate(scrb(Lpi,Mpi))
allocate(scru(Li,Mpi))
allocate(scrv(Lpi,Mi))
allocate(error(Lpi,Mpi))

statusi = nf90_inq_varid(ncgridi,'Vtransform',id_vtransformi);   if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_vtransformi,Vtransformi);      if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncgridi,'Vstretching',id_vstretchingi); if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_vstretchingi,Vstretchingi);    if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncgridi,'theta_s',id_theta_si);         if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_theta_si,theta_si);            if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncgridi,'theta_b',id_theta_bi);         if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_theta_bi,theta_bi);            if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncgridi,'Tcline',id_tclinei);           if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_tclinei,Tclinei);              if(statusi /= nf90_NoErr) call handle_err(statusi)

PRINT *
PRINT *,'Read vertical grid details for input grid: '
PRINT *,'Vtransform, Vstretching: ', Vtransformi, Vstretchingi
PRINT *,'theta_s, theta_b, Tcline: ', theta_si, theta_bi, Tclinei
PRINT *

statusi = nf90_inq_varid(ncgridi,'lon_rho',id_lon_rhoi);         if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_lon_rhoi,lon_rhoi);            if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncgridi,'lat_rho',id_lat_rhoi);         if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_lat_rhoi,lat_rhoi);            if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncgridi,'angle',id_anglei);             if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_anglei,anglei);                if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncgridi,'h',id_hi);                     if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_hi,hi);                        if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncgridi,'mask_rho',id_rmaski);          if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_get_var(ncgridi,id_rmaski,rmaski);                if(statusi /= nf90_NoErr) call handle_err(statusi)
!statusi = nf90_inq_varid(ncgridi,'mask_u',id_umaski);            if(statusi /= nf90_NoErr) call handle_err(statusi)
!statusi = nf90_get_var(ncgridi,id_umaski,umaski);                if(statusi /= nf90_NoErr) call handle_err(statusi)
!statusi = nf90_inq_varid(ncgridi,'mask_v',id_vmaski);            if(statusi /= nf90_NoErr) call handle_err(statusi)
!statusi = nf90_get_var(ncgridi,id_vmaski,vmaski);                if(statusi /= nf90_NoErr) call handle_err(statusi)

! Calculate mask_u and mask_v from mask_rho
!DO j=1,Mpi
!  DO i=1,Li
!    umaski(i,j) = rmaski(i,j)*rmaski(i+1,j)
!  ENDDO
!ENDDO
!DO i=1,Lpi
!  DO j=1,Mi
!    vmaski(i,j) = rmaski(i,j)*rmaski(i,j+1)
!  ENDDO
!ENDDO

!---------------------------------------------------------
! Vertical grid specification for input

hmini = 1.0e+23
do j=1,Mpi
do i=1,Lpi
  hmini = min(hmini,hi(i,j))
enddo
enddo

allocate(z_ri(Lpi,Mpi,Ni))
allocate(sc_ri(Ni))
allocate(Cs_ri(Ni))

!-----------------------------------------------------------------------
! Code from set_scoord.F from the roms code:
! Edited by A.Staalstrom (ans@niva.no)
!-----------------------------------------------------------------------
!  Set hc <= hmin, in the original formulation (Vtransform=1)
IF (Vtransformi.eq.1) THEN
  hci=MIN(hmini,Tclinei)
ELSE IF (Vtransformi.eq.2) THEN
  hci=Tclinei
ELSE IF (Vtransformi.eq.3) THEN
  hci=Tclinei
END IF
!-----------------------------------------------------------------------
!  Original vertical strectching function, Song and Haidvogel (1994).
!-----------------------------------------------------------------------
!
IF (Vstretchingi.eq.1) THEN
  IF (theta_si.ne.0.0) THEN
    cff1i=1.0/SINH(theta_si)
    cff2i=0.5/TANH(0.5*theta_si)
  END IF
  cffi=1.0/REAL(Ni)
  DO k=1,Ni
    sc_ri(k)=cffi*(REAL(k-Ni)-0.5)
    IF (theta_si.ne.0.0) THEN
      Cs_ri(k)=(1.0-theta_bi)*cff1i*SINH(theta_si*sc_ri(k))+         &
             theta_bi*(cff2i*TANH(theta_si*(sc_ri(k)+0.5))- 0.5)
    ELSE
    Cs_ri(k)=sc_ri(k)
    END IF
  END DO
!-----------------------------------------------------------------------
!  A. Shchepetkin new vertical stretching function.
!-----------------------------------------------------------------------
!
ELSE IF (Vstretchingi.eq.2) THEN
  Aweight=1.0
  Bweight=1.0
  ds=1.0/REAL(Ni)
!
  DO k=1,Ni
    sc_ri(k)=ds*(REAL(k-Ni)-0.5)
    IF (theta_si.gt.0.0) THEN
      Csur=(1.0-COSH(theta_si*sc_ri(k)))/(COSH(theta_si)-1.0)
      IF (theta_bi.gt.0.0) THEN
        Cbot=SINH(theta_bi*(sc_ri(k)+1.0))/SINH(theta_bi)-1.0
        Cweight=(sc_ri(k)+1.0)**Aweight*(1.0+(Aweight/Bweight)*      &
                             (1.0-(sc_ri(k)+1.0)**Bweight))
        Cs_ri(k)=Cweight*Csur+(1.0-Cweight)*Cbot
      ELSE
        Cs_ri(k)=Csur
      END IF
    ELSE
        Cs_ri(k)=sc_ri(k)
    END IF
  END DO
!
!-----------------------------------------------------------------------
!  R. Geyer stretching function for high bottom boundary layer
!  resolution. (NOT INCLUDED)
!-----------------------------------------------------------------------
!
!
!-----------------------------------------------------------------------
!  A. Shchepetkin improved double vertical stretching functions with
!  bottom refiment.
!-----------------------------------------------------------------------
!
ELSE IF (Vstretchingi.eq.4) THEN
  ds=1.0/REAL(Ni)
!
  DO k=1,Ni
    sc_ri(k)=ds*(REAL(k-Ni)-0.5)
    IF (theta_si.gt.0.0) THEN
      Csur=(1.0-COSH(theta_si*sc_ri(k)))/(COSH(theta_si)-1.0)
    ELSE
      Csur=-sc_ri(k)**2
    END IF
    IF (theta_bi.gt.0.0) THEN
      Cbot=(EXP(theta_bi*Csur)-1.0)/(1.0-EXP(-theta_bi))
      Cs_ri(k)=Cbot
    ELSE
      Cs_ri(k)=Csur
    END IF
  END DO
!
!-----------------------------------------------------------------------
!  Pure sigma coordinates C(s)=s
!  If Vtransform is set to 1 or 2 and hc/Tcline=0 tradinonal
!  sigma coordinates are achived.
!  Changed by ans@niva.no
!-----------------------------------------------------------------------
!
ELSE IF (Vstretchingi.eq.5) THEN
  ds=1.0/REAL(Ni)
  DO k=1,Ni
    sc_ri(k)=ds*(REAL(k-Ni)-0.5)
    Cs_ri(k)=sc_ri(k)
  END DO
END IF
!--------------------------------------------------------------------------
! End of code from set_scoord.F
!--------------------------------------------------------------------------
!-----------------------------------------------------------------------
! Code from set_depth.F from the roms code:
! Edited by A.Staalstrom (ans@niva.no)
! NB! Sealevel is set to zero
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!  Original formulation: Compute vertical depths (meters, negative) at
!  RHO-pints , and vertical grid thicknesses.
!  Various stretching functions are possible.
!-----------------------------------------------------------------------
!
IF (Vtransformi.eq.1) THEN
  DO j=1,Mpi
    DO k=1,Ni
      cff_ri=hci*(sc_ri(k)-Cs_ri(k))
      cff1_ri=Cs_ri(k)
      cff2_ri=sc_ri(k)+1.0
      DO i=1,Lpi
        z_ri(i,j,k)=cff_ri+cff1_ri*hi(i,j)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!  New formulation: Compute vertical depths (meters, negative) at
!  RHO-points, and vertical grid thicknesses.
!  Various stretching functions are possible.
!-----------------------------------------------------------------------
!
ELSE IF (Vtransformi.eq.2) THEN
  DO j=1,Mpi
    DO k=1,Ni
      cff_ri=hci*sc_ri(k)
      cff1_ri=Cs_ri(k)
      DO i=1,Lpi
        hinv=1.0/(hci+hi(i,j))
        cff2_ri=(cff_ri+cff1_ri*hi(i,j))*hinv
        z_ri(i,j,k)=hi(i,j)*cff2_ri
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
! Even newer formulation:
! by Andre Staalstrom (ans@niva.no) Feb 22 2011:
! Compute vertical depths (meters, negative) at RHO- and W-points,
! and vertical grid thicknesses.
! Various stretching functions are NOT possible, but is built into
! the vertical transform. This is because Cs_w is not constant in the
! horizontal direction, but vary with depth.
! The stretching function is of the Vstretching=2 type, but the Cweigth
! is now a function of depth, so enhanced vertical resolution can be
! attained at a given depth.
!-----------------------------------------------------------------------
!
ELSE IF (Vtransformi.eq.3) THEN
  DO j=1,Mpi
    DO k=1,Ni
      cff_ri=sc_ri(k)
      cff1_ri=Cs_ri(k)
      DO i=1,Lpi
! Calculate the weight of the stretching function, scaled by depth
! Cweight is set to zero if it is shallower than Tcline
        cffi=(hi(i,j)-Tclinei)/hi(i,j)
        Cweight=MAX(cffi,0.0)
! cff2_ri are the actual stretching function
        Csur=(1.0-COSH(theta_si*cff_ri))/(COSH(theta_si)-1.0)
        Cbot=SINH(theta_bi*(cff_ri+1.0))/SINH(theta_bi)-1.0
        cff2_ri=Cweight*Csur+(1.0-Cweight)*Cbot
! Calculate z coordinates
        z_ri(i,j,k)=hi(i,j)*cff2_ri
      END DO
    END DO
  END DO
END IF
!--------------------------------------------------------------------------
! End of code from set_depth.F
!--------------------------------------------------------------------------

statusi = nf90_close(ncgridi)

allocate(temp_in(Lpi,Mpi,Ni))
allocate(salt_in(Lpi,Mpi,Ni))
allocate(u_in(Li,Mpi,Ni))
allocate(v_in(Lpi,Mi,Ni))
allocate(ubar_in(Li,Mpi))
allocate(vbar_in(Lpi,Mi))
allocate(zeta_in(Lpi,Mpi))

! ---------------------------------------------------------------------
! Open and get info on output file

statuso = nf90_open(trim(gridfileo),nf90_nowrite,ncgrido)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_inq_dimid(ncgrido,'xi_rho',dim_xi_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_inq_dimid(ncgrido,'eta_rho',dim_eta_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_Inquire_Dimension(ncgrido,dim_xi_rhoo,xi_dimnameo,Lpo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_Inquire_Dimension(ncgrido,dim_eta_rhoo,eta_dimnameo,Mpo)
if(statuso /= nf90_NoErr) call handle_err(statuso)

Mo=Mpo-1
Lo=Lpo-1

write(*,*) 'Lpo, Mpo = ',Lpo,Mpo

allocate(lon_rhoo(Lpo,Mpo))
allocate(lat_rhoo(Lpo,Mpo))
allocate(angleo(Lpo,Mpo))
allocate(ho(Lpo,Mpo))
allocate(sc_ro(No))
allocate(Cs_ro(No))

allocate(z_ro(Lpo,Mpo,No))
allocate(ds_ro(Lpo,Mpo,No))
allocate(sum_ds(Lpo,Mpo))
allocate(z_ri_interp(Lpo,Mpo,No))
allocate(scr3d(Lpo,Mpo,No))
allocate(scr1o(Lpo,Mpo))
allocate(scr2o(Lpo,Mpo))
allocate(scrao(Lpo,Mpo))
allocate(scrbo(Lpo,Mpo))
allocate(scruo(Lo,Mpo))
allocate(scrvo(Lpo,Mo))
allocate(u_rhoo(Lpo,Mpo,No))
allocate(v_rhoo(Lpo,Mpo,No))

allocate(temp_out(Lpo,Mpo,No))
allocate(salt_out(Lpo,Mpo,No))
allocate(u_out(Lo,Mpo,No))
allocate(v_out(Lpo,Mo,No))
allocate(u_rhoi(Lpi,Mpi,Ni))
allocate(v_rhoi(Lpi,Mpi,Ni))
allocate(ubar_out(Lo,Mpo))
allocate(vbar_out(Lpo,Mo))
allocate(zeta_out(Lpo,Mpo))

allocate(ipos(Lpo,Mpo))
allocate(jpos(Lpo,Mpo))

statuso = nf90_inq_varid(ncgrido,'lon_rho',id_lon_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_inq_varid(ncgrido,'lat_rho',id_lat_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_inq_varid(ncgrido,'angle',id_angleo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_inq_varid(ncgrido,'h',id_ho)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_get_var(ncgrido,id_lon_rhoo,lon_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_get_var(ncgrido,id_lat_rhoo,lat_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_get_var(ncgrido,id_angleo,angleo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_get_var(ncgrido,id_ho,ho)
if(statuso /= nf90_NoErr) call handle_err(statuso)

!---------------------------------------------------------
! Vertical grid specification for output

hmino = 1.0e+23
do j=1,Mpo
do i=1,Lpo
  hmino = min(hmino,ho(i,j))
enddo
enddo

!-----------------------------------------------------------------------
! Code from set_scoord.F from the roms code:
! Edited by A.Staalstrom (ans@niva.no)
!-----------------------------------------------------------------------
!  Set hc <= hmin, in the original formulation (Vtransform=1)
IF (Vtransformo.eq.1) THEN
  hco=MIN(hmino,Tclineo)
ELSE IF (Vtransformo.eq.2) THEN
  hco=Tclineo    !i  !<jonal: Am not sure about this, but Tclini does not exist...>
ELSE IF (Vtransformo.eq.3) THEN
  hco=Tclineo
END IF

!-----------------------------------------------------------------------
!  Original vertical strectching function, Song and Haidvogel (1994).
!-----------------------------------------------------------------------
!
IF (Vstretchingo.eq.1) THEN
  IF (theta_so.ne.0.0) THEN
    cff1o=1.0/SINH(theta_so)
    cff2o=0.5/TANH(0.5*theta_so)
  END IF
  cffo=1.0/REAL(No)
  DO k=1,No
    sc_ro(k)=cffo*(REAL(k-No)-0.5)
    IF (theta_so.ne.0.0) THEN
      Cs_ro(k)=(1.0-theta_bo)*cff1o*SINH(theta_so*sc_ro(k))+         &
             theta_bo*(cff2o*TANH(theta_so*(sc_ro(k)+0.5))- 0.5)
    ELSE
    Cs_ro(k)=sc_ro(k)
    END IF
  END DO
!-----------------------------------------------------------------------
!  A. Shchepetkin new vertical stretching function.
!-----------------------------------------------------------------------
!
ELSE IF (Vstretchingo.eq.2) THEN
  Aweight=1.0
  Bweight=1.0
  ds=1.0/REAL(No)
!
  DO k=1,No
    sc_ro(k)=ds*(REAL(k-No)-0.5)
    IF (theta_so.gt.0.0) THEN
      Csur=(1.0-COSH(theta_so*sc_ro(k)))/(COSH(theta_so)-1.0)
      IF (theta_bo.gt.0.0) THEN
        Cbot=SINH(theta_bo*(sc_ro(k)+1.0))/SINH(theta_bo)-1.0
        Cweight=(sc_ro(k)+1.0)**Aweight*(1.0+(Aweight/Bweight)*      &
                             (1.0-(sc_ro(k)+1.0)**Bweight))
        Cs_ro(k)=Cweight*Csur+(1.0-Cweight)*Cbot
      ELSE
        Cs_ro(k)=Csur
      END IF
    ELSE
        Cs_ro(k)=sc_ro(k)
    END IF
  END DO
!
!-----------------------------------------------------------------------
!  R. Geyer stretching function for high bottom boundary layer
!  resolution. (NOT INCLUDED)
!-----------------------------------------------------------------------
!
!
!-----------------------------------------------------------------------
!  A. Shchepetkin improved double vertical stretching functions with
!  bottom refiment.
!-----------------------------------------------------------------------
!
ELSE IF (Vstretchingo.eq.4) THEN
  ds=1.0/REAL(No)
!
  DO k=1,No
    sc_ro(k)=ds*(REAL(k-No)-0.5)
    IF (theta_so.gt.0.0) THEN
      Csur=(1.0-COSH(theta_so*sc_ro(k)))/(COSH(theta_so)-1.0)
    ELSE
      Csur=-sc_ro(k)**2
    END IF
    IF (theta_bo.gt.0.0) THEN
      Cbot=(EXP(theta_bo*Csur)-1.0)/(1.0-EXP(-theta_bo))
      Cs_ro(k)=Cbot
    ELSE
      Cs_ro(k)=Csur
    END IF
  END DO
!
!-----------------------------------------------------------------------
!  Pure sigma coordinates C(s)=s
!  If Vtransform is set to 1 or 2 and hc/Tcline=0 tradinonal
!  sigma coordinates are achived.
!  Changed by ans@niva.no
!-----------------------------------------------------------------------
!
ELSE IF (Vstretchingo.eq.5) THEN
  ds=1.0/REAL(No)
  DO k=1,No
    sc_ro(k)=ds*(REAL(k-No)-0.5)
    Cs_ro(k)=sc_ro(k)
  END DO
ELSE
  PRINT *,"This choice of Vstretching is not valid: ",Vstretchingo
  STOP " Program terminates!"
END IF
!--------------------------------------------------------------------------
! End of code from roms
!--------------------------------------------------------------------------
!-----------------------------------------------------------------------
! Code from set_depth.F from the roms code:
! Edited by A.Staalstrom (ans@niva.no)
! NB! Sealevel is set to zero
!-----------------------------------------------------------------------
!-----------------------------------------------------------------------
!  Original formulation: Compute vertical depths (meters, negative) at
!  RHO-pints , and vertical grid thicknesses.
!  Various stretching functions are possible.
!-----------------------------------------------------------------------
!
IF (Vtransformo.eq.1) THEN
  DO j=1,Mpo
    DO k=1,No
      cff_ro=hco*(sc_ro(k)-Cs_ro(k))
      cff1_ro=Cs_ro(k)
      cff2_ro=sc_ro(k)+1.0
      DO i=1,Lpo
        z_ro(i,j,k)=cff_ro+cff1_ro*ho(i,j)
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
!  New formulation: Compute vertical depths (meters, negative) at
!  RHO-points, and vertical grid thicknesses.
!  Various stretching functions are possible.
!-----------------------------------------------------------------------
!
ELSE IF (Vtransformo.eq.2) THEN
  DO j=1,Mpo
    DO k=1,No
      cff_ro=hco*sc_ro(k)   ! hci*sc_ro(k)  ! <jonal: not sure about this one, but hci does not exist>
      cff1_ro=Cs_ro(k)
      DO i=1,Lpo
        hinv=1.0/(hco+ho(i,j))
        cff2_ro=(cff_ro+cff1_ro*ho(i,j))*hinv
        z_ro(i,j,k)=ho(i,j)*cff2_ro
      END DO
    END DO
  END DO
!
!-----------------------------------------------------------------------
! Even newer formulation:
! by Andre Staalstrom (ans@niva.no) Feb 22 2011:
! Compute vertical depths (meters, negative) at RHO- and W-points,
! and vertical grid thicknesses.
! Various stretching functions are NOT possible, but is built into
! the vertical transform. This is because Cs_w is not constant in the
! horizontal direction, but vary with depth.
! The stretching function is of the Vstretching=2 type, but the Cweigth
! is now a function of depth, so enhanced vertical resolution can be
! attained at a given depth.
!-----------------------------------------------------------------------
!
ELSE IF (Vtransformo.eq.3) THEN
  DO j=1,Mpo
    DO k=1,No
      cff_ro=sc_ro(k)
      cff1_ro=Cs_ro(k)
      DO i=1,Lpo
! Calculate the weight of the stretching function, scaled by depth
! Cweight is set to zero if it is shallower than Tcline
        cffo=(ho(i,j)-Tclineo)/ho(i,j)
        Cweight=MAX(cffo,0.0)
! cff2_ri are the actual stretching function
        Csur=(1.0-COSH(theta_so*cff_ro))/(COSH(theta_so)-1.0)
        Cbot=SINH(theta_bo*(cff_ro+1.0))/SINH(theta_bo)-1.0
        cff2_ro=Cweight*Csur+(1.0-Cweight)*Cbot
! Calculate z coordinates
        z_ro(i,j,k)=ho(i,j)*cff2_ro
      END DO
    END DO
  END DO
ELSE
  PRINT *,"This choice of Vtransform for output grid is not valid: ",Vtransformo
  STOP
END IF
!--------------------------------------------------------------------------
! End of code from set_depth.F
!--------------------------------------------------------------------------

statuso = nf90_close(ncgrido)

! Proportion of output height of every cell relative to total depth
DO k=1,No
  DO j=1,Mpo
    DO i=1,Lpo
      IF (k == 1) THEN
        ds_ro(i,j,k) = 0.
      ELSE
        ds_ro(i,j,k)=(z_ro(i,j,k)-z_ro(i,j,k-1))/ho(i,j)
      ENDIF
    END DO
  END DO
END DO

! ---------------------------------------------------------------------
! Find output grid point locations on input grid
! Get interpolation indices for bilinear interpolation

!!BAA Compute ipos, jpos based on lon_rhoo and lat_rhoo

!! Verdier for input grid
!! Må deklareres og sette inn verdier
!JAalpha = ylon * DTOR
!JAphinull = 60.0 * DTOR
!rearth = 6378.137   ! earth radius [km] (equatorial)
!JArearth = 6371.0   ! earth radius [km] (average)

!JADO j = 1, Mpo
!JA  DO i = 1, Lpo
!JA    phi = lon_rhoo(i,j) * DTOR
!JA    lambda = lat_rhoo(i,j) * DTOR
!JA    r = rearth * cos(lambda) * (1 + sin(phinull)) / (1 + sin(lambda))
!JA    ipos(i,j) = xp + r * sin(phi-alpha) / dx
!JA    jpos(i,j) = yp - r * cos(phi-alpha) / dx
!JA  ENDDO
!JAENDDO

! JA: Find ipos and jpos based on lon/lat and nearest grid point
! - ipos and jpos will be defined as integers, assume that difference in resolution is acceptable low
! - assume that this simplification is acceptable when clima-file is used as initial field and OBC only.
! - Improvement: use bilinear interpolation to find ipos,jpos as floating numbers

PRINT *,'Start loop to find indices for horizontal interpolation'
DO j=1,Mpo
  DO i=1,Lpo
    mindist = 1000.
    DO jj=1,Mpi
      DO ii=1,Lpi
        dist = SQRT((lon_rhoi(ii,jj)-lon_rhoo(i,j))**2 + (lat_rhoi(ii,jj)-lat_rhoo(i,j))**2)
        IF (dist < mindist) THEN
          ipos(i,j) = ii
          jpos(i,j) = jj
          mindist = dist
        ENDIF
      ENDDO
    ENDDO
  ENDDO
  PRINT *,'Finished ',j,' of ',Mpo,' in loop'
ENDDO

! Find subarea in Large-Area model containing output grid
rimin = 1.e+23
rimax =-1.e+23
rjmin = 1.e+23
rjmax =-1.e+23
DO j=1,Mpo
DO i=1,Lpo
  rimin = MIN(rimin,ipos(i,j))
  rimax = MAX(rimax,ipos(i,j))
  rjmin = MIN(rjmin,jpos(i,j))
  rjmax = MAX(rjmax,jpos(i,j))
ENDDO
ENDDO
margin=5
i1 = MAX(FLOOR(rimin)-margin,1)
i2 = MIN(CEILING(rimax)+margin,Lpi)
j1 = MAX(FLOOR(rjmin)-margin,1)
j2 = MIN(CEILING(rjmax)+margin,Mpi)

print '(A,4I6,A,I4)', 'Need subarea from input grid (i1,i2,j1,j2)= ', i1, i2, j1, j2, ' where added rim is ', margin

! Get depth locations on interpolated grid

DO j=1,Mpo
DO i=1,Lpo
   ia = ifix(ipos(i,j))
   ja = ifix(jpos(i,j))
   rx = (ipos(i,j)-ia)
   ry = (jpos(i,j)-ja)
   rxm = 1.0-rx
   rym = 1.0-ry
   DO k=1,Ni
      z_ri_interp(i,j,k) = rxm*rym*z_ri(ia,ja,k)    +   &
                    rx*rym*z_ri(ia+1,ja,k)          +   &
                    rx*ry*z_ri(ia+1,ja+1,k)         +   &
                    rxm*ry*z_ri(ia,ja+1,k)
   ENDDO
ENDDO
ENDDO

outfile = TRIM(clim_path) // TRIM(clim_root) // '_clima.nc'
write(*,'(2a)') 'outfile: ', TRIM(outfile)

! Prepare output netCDF file
statuso = nf90_create(trim(outfile),IOR(nf90_clobber,nf90_64BIT_OFFSET),ncido)
if(statuso /= nf90_NoErr) call handle_err(statuso)
!
! Define dimensions
!
statuso = nf90_def_dim(ncido,'xi_rho',Lpo,dim_xi_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_dim(ncido,'eta_rho',Mpo,dim_eta_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_dim(ncido,'xi_u',Lo,dim_xi_uo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_dim(ncido,'eta_u',Mpo,dim_eta_uo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_dim(ncido,'xi_v',Lpo,dim_xi_vo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_dim(ncido,'eta_v',Mo,dim_eta_vo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_dim(ncido,'s_rho',No,dim_s_rhoo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_dim(ncido,'clim_time',nf90_unlimited,dim_timeo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
!
! Define variables
!
statuso = nf90_def_var(ncido,'u',nf90_float,                              &
         (/dim_xi_uo, dim_eta_uo, dim_s_rhoo, dim_timeo/),UVarIdo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_var(ncido,'v',nf90_float,                              &
         (/dim_xi_vo, dim_eta_vo, dim_s_rhoo, dim_timeo/),VVarIdo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_var(ncido,'salt',nf90_float,                           &
         (/dim_xi_rhoo, dim_eta_rhoo, dim_s_rhoo, dim_timeo/),SaltVarIdo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_var(ncido,'temp',nf90_float,                           &
         (/dim_xi_rhoo, dim_eta_rhoo, dim_s_rhoo, dim_timeo/),TempVarIdo)
if(statuso /= nf90_NoErr) call handle_err(statuso )
statuso = nf90_def_var(ncido,'zeta',nf90_float,                            &
         (/dim_xi_rhoo, dim_eta_rhoo, dim_timeo/),ZetaVarIdo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_var(ncido,'ubar',nf90_float,                            &
         (/dim_xi_uo, dim_eta_uo, dim_timeo/),UbarVarIdo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_def_var(ncido,'vbar',nf90_float,                            &
         (/dim_xi_vo, dim_eta_vo, dim_timeo/),VbarVarIdo)
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_def_var(ncido,'clim_time',nf90_double,dim_timeo,TimeVarIdo)
if(statuso /= nf90_NoErr) call handle_err(statuso)
!
! Include variable attributes
!
statuso = nf90_put_att(ncido,UVarIdo,'long_name','u-momentum component')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,UVarIdo,'units','meter second-1')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,UVarIdo,'time','clim_time')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,UVarIdo,'field','u-velocity, scalar, series')
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_put_att(ncido,VVarIdo,'long_name','v-momentum component')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,VVarIdo,'units','meter second-1')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,VVarIdo,'time','clim_time')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,VVarIdo,'field','v-velocity, scalar, series')
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_put_att(ncido,SaltVarIdo,'long_name','salinity')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,SaltVarIdo,'units','PSU')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,SaltVarIdo,'time','clim_time')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,SaltVarIdo,'field','salinity, scalar, series')
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_put_att(ncido,TempVarIdo,'long_name','potential temperature')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,TempVarIdo,'units','Celsius')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,TempVarIdo,'time','clim_time')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,TempVarIdo,'field','temperature, scalar, series')
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_put_att(ncido,ZetaVarIdo,'long_name','sea level')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,ZetaVarIdo,'units','meter')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,ZetaVarIdo,'time','clim_time')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,ZetaVarIdo,'field','sea level, scalar, series')
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_put_att(ncido,UbarVarIdo,'long_name','u-2D momentum component')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,UbarVarIdo,'units','meter second-1')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,UbarVarIdo,'time','clim_time')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,UbarVarIdo,'field','u-2D velocity, scalar, series')
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_put_att(ncido,VbarVarIdo,'long_name','v-2D momentum component')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,VbarVarIdo,'units','meter second-1')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,VbarVarIdo,'time','clim_time')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,VbarVarIdo,'field','v-2D velocity, scalar, series')
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_put_att(ncido,TimeVarIdo,'long_name','days since 1970-01-01 00:00:00')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,TimeVarIdo,'units','days')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,TimeVarIdo,'field','time, scalar, series')
if(statuso /= nf90_NoErr) call handle_err(statuso)

statuso = nf90_put_att(ncido,nf90_global,'type','ROMS/TOMS climatology file')
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_put_att(ncido,nf90_global,'title','ROMS/TOMS 3.2')
if(statuso /= nf90_NoErr) call handle_err(statuso)
!
! Exit definition mode
!
statuso = nf90_enddef(ncido)
if(statuso /= nf90_NoErr) call handle_err(statuso)

write(*,*) 'Finished file definition on output file'

! ..........................

! Open list of input files
open(unit=55,file=TRIM(avgfilei_lst),form='formatted')
print *, 'Opened filelist: ', TRIM(avgfilei_lst)

read(55,*) nof
print *, 'No. of files to be read: ', nof
! Loop through input files
! ---------------------------------------------------------------

otime = 0  ! Count time steps for output file
time_in = 0.


DO feof=1,nof

read(55,'(a)',end=999) avgfile
avgfilei = TRIM(avg_path) // TRIM(avgfile)
print *, 'avgfilei = ', TRIM(avgfilei)

! Open input averages file
statusi = nf90_open(trim(avgfilei),nf90_nowrite,ncidi)
if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_dimid(ncidi,'ocean_time',dim_timei)
if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_Inquire_Dimension(ncidi,dim_timei,time_dimnamei,Iti)
if(statusi /= nf90_NoErr) call handle_err(statusi)
statusi = nf90_inq_varid(ncidi,'ocean_time',TimeVarIdi)
if(statusi /= nf90_NoErr) call handle_err(statusi)

print *,'Time array on file read. No. of time steps on file is ',Iti

! Loop through time steps on file
! ---------------------------------------------------------------

DO itime = 1,Iti

! ---
! Date
   time_in_prev = time_in
   statusi = nf90_get_var(ncidi,TimeVarIdi,time_in,start=(/ itime /))
   if(statusi /= nf90_NoErr) call handle_err(statusi)
   print *,'Input time stamp is ',time_in

   IF (time_in <= time_in_prev) CYCLE  ! Prevent repetition of duplicates in time and/or time step backward in time

! Count for output file
  otime = otime + 1

! .......................................................................
!
! Temperature

! Read in temperature
   statusi = nf90_inq_varid(ncidi,'temp',TempVarIdi)
   if(statusi /= nf90_NoErr) call handle_err(statusi)
   statusi = nf90_get_att(ncidi, TempVarIdi, "scale_factor", Temp_scale_factor)
   if(statusi /= nf90_noerr) then
     Temp_scale_factor = 1.
   endif
   statusi = nf90_get_att(ncidi, TempVarIdi, "add_offset", Temp_add_offset)
   if(statusi /= nf90_noerr) then
     Temp_add_offset = 0.
   endif
   statusi = nf90_get_var(ncidi,TempVarIdi,temp_in,start=(/1,1,1,itime/))
   if(statusi /= nf90_NoErr) call handle_err(statusi)

   WHERE(temp_in > -32765.) temp_in = temp_in * Temp_scale_factor + Temp_add_offset

! Fill in masked-out values
   DO k=1,Ni
      scr = 0.
      scr = temp_in(:,:,k)
      WHERE(rmaski<0.01) scr = undef
      CALL fill(Lpi,Mpi,i1,i2,j1,j2,scr,tx,critx,cor,mxs,work,error,nvalue)
      temp_in(:,:,k) = scr
   ENDDO

! Horizontal interpolation
   scr3d = undef
   DO j=1,Mpo
   DO i=1,Lpo
      ia = INT(ipos(i,j))
      ja = INT(jpos(i,j))
      IF (ia < 1 .OR. ia > Lpi .OR. ja < 1 .OR. ja > Mpi) CYCLE
      rx = (ipos(i,j)-ia)
      ry = (jpos(i,j)-ja)
      rxm = 1.0-rx
      rym = 1.0-ry
      DO k=1,Ni
         scr3d(i,j,k) = rxm*rym*temp_in(ia,ja,k)          +   &
                       rx*rym*temp_in(ia+1,ja,k)          +   &
                       rx*ry*temp_in(ia+1,ja+1,k)         +   &
                       rxm*ry*temp_in(ia,ja+1,k)
      ENDDO
   ENDDO
   ENDDO

! Vertical interpolation
   temp_out = undef
   DO j=1,Mpo
   DO i=1,Lpo
      DO k=1,No
         IF(z_ro(i,j,k).LE.z_ri_interp(i,j,1)) THEN
            temp_out(i,j,k) = scr3d(i,j,1)
         ELSEIF(z_ro(i,j,k).GE.z_ri_interp(i,j,Ni)) THEN
            temp_out(i,j,k) = scr3d(i,j,Ni)
         ELSE
            DO kT=1,Ni
               IF(z_ro(i,j,k).LT.z_ri_interp(i,j,kT+1).AND.              &
                  z_ro(i,j,k).GE.z_ri_interp(i,j,kT)) THEN
                  rz2 = (z_ro(i,j,k)-z_ri_interp(i,j,kT))/               &
                          (z_ri_interp(i,j,kT+1)-z_ri_interp(i,j,kT))
                  rz1 = 1.0-rz2
                  temp_out(i,j,k) = rz1*scr3d(i,j,kT) + rz2*scr3d(i,j,kT+1)
                  EXIT
               ENDIF
            ENDDO
         ENDIF
      ENDDO
   ENDDO
   ENDDO

  ! Fill in masked-out values
   DO k=1,No
      scr1o = 0.
      scr1o = temp_out(:,:,k)
      WHERE (scr1o > 50.) scr1o = undef
      WHERE (scr1o < -2.) scr1o = undef
      CALL fill(Lpo,Mpo,1,Lpo,1,Mpo,scr1o,tx,critx,cor,mxs,work,error,nvalue)
      temp_out(:,:,k) = scr1o
   ENDDO

  ! Output to netCDF file
   statuso = nf90_put_var(ncido,TempVarIdo,temp_out,start=(/1,1,1,otime/))
   if(statuso /= nf90_NoErr) call handle_err(statuso)

   write(*,*) 'Completed temperature'
! ........................................................................
!
! Salinity

! Read in salinity
   statusi = nf90_inq_varid(ncidi,'salt',SaltVarIdi)
   if(statusi /= nf90_NoErr) call handle_err(statusi)
   statusi = nf90_get_att(ncidi, SaltVarIdi, "scale_factor", Salt_scale_factor)
   if(statusi /= nf90_noerr) then
     Salt_scale_factor = 1.
   endif
   statusi = nf90_get_att(ncidi, SaltVarIdi, "add_offset", Salt_add_offset)
   if(statusi /= nf90_noerr) then
     Salt_add_offset = 0.
   endif
   statusi = nf90_get_var(ncidi,SaltVarIdi,salt_in,start=(/ 1, 1, 1, itime/))
   if(statusi /= nf90_NoErr) call handle_err(statusi)

   WHERE(salt_in > -32765.) salt_in = salt_in * Salt_scale_factor + Salt_add_offset

! ---
! Fill in masked-out values
   DO k=1,Ni
      scr = 0.
      scr = salt_in(:,:,k)
      WHERE (rmaski < 0.01) scr = undef
      CALL fill(Lpi,Mpi,i1,i2,j1,j2,scr,tx,critx,cor,mxs,work,error,nvalue)
      salt_in(:,:,k) = scr
   ENDDO

! Horizontal interpolation
   scr3d = undef
   DO j=1,Mpo
   DO i=1,Lpo
      ia = INT(ipos(i,j))
      ja = INT(jpos(i,j))
      IF (ia < 1 .OR. ia > Lpi .OR. ja < 1 .OR. ja > Mpi) CYCLE
      rx = (ipos(i,j)-ia)
      ry = (jpos(i,j)-ja)
      rxm = 1.0-rx
      rym = 1.0-ry
      DO k=1,Ni
         scr3d(i,j,k) = rxm*rym*salt_in(ia,ja,k)          +   &
                       rx*rym*salt_in(ia+1,ja,k)          +   &
                       rx*ry*salt_in(ia+1,ja+1,k)         +   &
                       rxm*ry*salt_in(ia,ja+1,k)
      ENDDO
   ENDDO
   ENDDO

! Vertical interpolation
   salt_out = undef
   DO j=1,Mpo
   DO i=1,Lpo
      DO k=1,No
         IF(z_ro(i,j,k).LE.z_ri_interp(i,j,1)) THEN
            salt_out(i,j,k) = scr3d(i,j,1)
         ELSEIF(z_ro(i,j,k).GE.z_ri_interp(i,j,Ni)) THEN
            salt_out(i,j,k) = scr3d(i,j,Ni)
         ELSE
            DO kT=1,Ni
               IF(z_ro(i,j,k).LT.z_ri_interp(i,j,kT+1).AND.              &
                  z_ro(i,j,k).GE.z_ri_interp(i,j,kT)) THEN
                  rz2 = (z_ro(i,j,k)-z_ri_interp(i,j,kT))/               &
                          (z_ri_interp(i,j,kT+1)-z_ri_interp(i,j,kT))
                  rz1 = 1.0-rz2
                  salt_out(i,j,k) = rz1*scr3d(i,j,kT) + rz2*scr3d(i,j,kT+1)
                  EXIT
               ENDIF
            ENDDO
         ENDIF
      ENDDO
   ENDDO
   ENDDO

  ! Fill in masked-out values
   DO k=1,No
      scr1o = 0.
      scr1o = salt_out(:,:,k)
      WHERE (scr1o > 50.) scr1o = undef
      WHERE (scr1o <  0.) scr1o = undef
      CALL fill(Lpo,Mpo,1,Lpo,1,Mpo,scr1o,tx,critx,cor,mxs,work,error,nvalue)
      salt_out(:,:,k) = scr1o
   ENDDO

  ! Output to netCDF file
   statuso = nf90_put_var(ncido,SaltVarIdo,salt_out,(/1,1,1,otime/))
   if(statuso /= nf90_NoErr) call handle_err(statuso)

  write(*,*) 'Completed salinity'
! ----------------------------------------------------------------
!
! 3D Velocity

! Read in velocities
   statusi = nf90_inq_varid(ncidi,'u',UVarIdi)
   if(statusi /= nf90_NoErr) call handle_err(statusi)
   statusi = nf90_get_att(ncidi, UVarIdi, "scale_factor", U_scale_factor)
   if(statusi /= nf90_noerr) then
     U_scale_factor = 1.
   endif
   statusi = nf90_get_att(ncidi, UVarIdi, "add_offset", U_add_offset)
   if(statusi /= nf90_noerr) then
     U_add_offset = 0.
   endif
   statusi = nf90_get_var(ncidi,UVarIdi,u_in,start=(/ 1, 1, 1, itime/))
   if(statusi /= nf90_NoErr) call handle_err(statusi)

   if (otime == 1) then
     PRINT *,"Find umaski based on u-currents (u_in(1,1,Ni)=", u_in(1,1,Ni)
     k = Ni
     DO i=1,Li
       DO j=1,Mpi
         IF (u_in(i,j,k) < -32765.) THEN
           umaski(i,j) = 0.
         ELSE
           umaski(i,j) = 1.
         ENDIF
       ENDDO
     ENDDO
   endif

   statusi = nf90_inq_varid(ncidi,'v',VVarIdi)
   if(statusi /= nf90_NoErr) call handle_err(statusi)
   statusi = nf90_get_att(ncidi, VVarIdi, "scale_factor", V_scale_factor)
   if(statusi /= nf90_noerr) then
     V_scale_factor = 1.
   endif
   statusi = nf90_get_att(ncidi, VVarIdi, "add_offset", V_add_offset)
   if(statusi /= nf90_noerr) then
     V_add_offset = 0.
   endif
   statusi = nf90_get_var(ncidi,VVarIdi,v_in,start=(/ 1, 1, 1, itime/))
   if(statusi /= nf90_NoErr) call handle_err(statusi)

   if (otime == 1) then
     PRINT *,"Find vmaski based on v-currents (v_in(1,1,Ni)=", v_in(1,1,Ni)
     k = Ni
     DO i=1,Lpi
       DO j=1,Mi
         IF (v_in(i,j,k) < -32765.) THEN
           vmaski(i,j) = 0.
         ELSE
           vmaski(i,j) = 1.
         ENDIF
       ENDDO
     ENDDO
   endif

   WHERE(u_in > -32765.) u_in = u_in * U_scale_factor + U_add_offset
   WHERE(v_in > -32765.) v_in = v_in * V_scale_factor + V_add_offset

! Fill in masked-out values
   DO k=1,Ni
      scru = 0.
      scru = u_in(:,:,k)
      WHERE (umaski < 0.01) scru = undef
      CALL fill(Li,Mpi,i1,i2,j1,j2,scru,tx,critx,cor,mxs,work,error,nvalue)
      u_in(:,:,k) = scru
   ENDDO
   DO k=1,Ni
      scrv = 0.
      scrv = v_in(:,:,k)
      WHERE (vmaski < 0.01) scrv = undef
      CALL fill(Lpi,Mi,i1,i2,j1,j2,scrv,tx,critx,cor,mxs,work,error,nvalue)
      v_in(:,:,k) = scrv
   ENDDO

! Average u and v to rho locations
   u_rhoi(2:Li,:,:) = 0.5*(u_in(1:Li-1,:,:)+u_in(2:Li,:,:))
   v_rhoi(:,2:Mi,:) = 0.5*(v_in(:,1:Mi-1,:)+v_in(:,2:Mi,:))

! Rotate velocity components
   DO k=1,Ni
      scr  = 0.
      scr1 = 0.
      scr  =  u_rhoi(:,:,k)*COS(anglei) - v_rhoi(:,:,k)*SIN(anglei)
      scr1 =  v_rhoi(:,:,k)*COS(anglei) + u_rhoi(:,:,k)*SIN(anglei)
      u_rhoi(:,:,k) = scr
      v_rhoi(:,:,k) = scr1
   ENDDO

! u_rho

! Horizontal interpolation
   scr3d = undef
   DO j=1,Mpo
   DO i=1,Lpo
      ia = INT(ipos(i,j))
      ja = INT(jpos(i,j))
      IF (ia < 1 .OR. ia > Lpi .OR. ja < 1 .OR. ja > Mpi) CYCLE
      rx = (ipos(i,j)-ia)
      ry = (jpos(i,j)-ja)
      rxm = 1.0-rx
      rym = 1.0-ry
      DO k=1,Ni
         scr3d(i,j,k) = rxm*rym*u_rhoi(ia,ja,k)          +   &
                       rx*rym*u_rhoi(ia+1,ja,k)          +   &
                       rx*ry*u_rhoi(ia+1,ja+1,k)         +   &
                       rxm*ry*u_rhoi(ia,ja+1,k)
      ENDDO
   ENDDO
   ENDDO

! Vertical interpolation
   u_rhoo = undef
   DO j=1,Mpo
   DO i=1,Lpo
      DO k=1,No
         IF(z_ro(i,j,k).LE.z_ri_interp(i,j,1)) THEN
            u_rhoo(i,j,k) = scr3d(i,j,1)
         ELSEIF(z_ro(i,j,k).GE.z_ri_interp(i,j,Ni)) THEN
            u_rhoo(i,j,k) = scr3d(i,j,Ni)
         ELSE
            DO kT=1,Ni
               IF(z_ro(i,j,k).LT.z_ri_interp(i,j,kT+1).AND.              &
                  z_ro(i,j,k).GE.z_ri_interp(i,j,kT)) THEN
                  rz2 = (z_ro(i,j,k)-z_ri_interp(i,j,kT))/               &
                          (z_ri_interp(i,j,kT+1)-z_ri_interp(i,j,kT))
                  rz1 = 1.0-rz2
                  u_rhoo(i,j,k) = rz1*scr3d(i,j,kT) + rz2*scr3d(i,j,kT+1)
                  EXIT
               ENDIF
            ENDDO
         ENDIF
      ENDDO
   ENDDO
   ENDDO

! Fill in masked-out values
   DO k=1,No
      scr1o = 0.
      scr1o = u_rhoo(:,:,k)
      WHERE (ABS(scr1o) > 5.) scr1o = undef
      CALL fill(Lpo,Mpo,1,Lpo,1,Mpo,scr1o,tx,critx,cor,mxs,work,error,nvalue)
      u_rhoo(:,:,k) = scr1o
   ENDDO

! v_rho

! Horizontal interpolation
   scr3d = undef
   DO j=1,Mpo
   DO i=1,Lpo
      ia = INT(ipos(i,j))
      ja = INT(jpos(i,j))
      IF (ia < 1 .OR. ia > Lpi .OR. ja < 1 .OR. ja > Mpi) CYCLE
      rx = (ipos(i,j)-ia)
      ry = (jpos(i,j)-ja)
      rxm = 1.0-rx
      rym = 1.0-ry
      DO k=1,Ni
         scr3d(i,j,k) = rxm*rym*v_rhoi(ia,ja,k)          +   &
                       rx*rym*v_rhoi(ia+1,ja,k)          +   &
                       rx*ry*v_rhoi(ia+1,ja+1,k)         +   &
                       rxm*ry*v_rhoi(ia,ja+1,k)
      ENDDO
   ENDDO
   ENDDO

! Vertical interpolation
   v_rhoo = undef
   DO j=1,Mpo
   DO i=1,Lpo
      DO k=1,No
         IF(z_ro(i,j,k).LE.z_ri_interp(i,j,1)) THEN
            v_rhoo(i,j,k) = scr3d(i,j,1)
         ELSEIF(z_ro(i,j,k).GE.z_ri_interp(i,j,Ni)) THEN
            v_rhoo(i,j,k) = scr3d(i,j,Ni)
         ELSE
            DO kT=1,Ni
               IF(z_ro(i,j,k).LT.z_ri_interp(i,j,kT+1).AND.              &
                  z_ro(i,j,k).GE.z_ri_interp(i,j,kT)) THEN
                  rz2 = (z_ro(i,j,k)-z_ri_interp(i,j,kT))/               &
                          (z_ri_interp(i,j,kT+1)-z_ri_interp(i,j,kT))
                  rz1 = 1.0-rz2
                  v_rhoo(i,j,k) = rz1*scr3d(i,j,kT) + rz2*scr3d(i,j,kT+1)
                  EXIT
               ENDIF
            ENDDO
         ENDIF
      ENDDO
   ENDDO
   ENDDO

! Fill in masked-out values
   DO k=1,No
      scr1o = 0.
      scr1o = v_rhoo(:,:,k)
      WHERE (ABS(scr1o) > 5.) scr1o = undef
      CALL fill(Lpo,Mpo,1,Lpo,1,Mpo,scr1o,tx,critx,cor,mxs,work,error,nvalue)
      v_rhoo(:,:,k) = scr1o
   ENDDO

! Rotate u_rhoo, v_rhoo to output grid
   DO k=1,No
      scr1o = 0.
      scr2o = 0.
      scr1o =  u_rhoo(:,:,k)*COS(angleo) + v_rhoo(:,:,k)*SIN(angleo)
      scr2o =  v_rhoo(:,:,k)*COS(angleo) - u_rhoo(:,:,k)*SIN(angleo)
      u_rhoo(:,:,k) = scr1o
      v_rhoo(:,:,k) = scr2o
   ENDDO

! Put u and v on to staggered grid
   u_out(:,:,:) = 0.5*(u_rhoo(1:Lo,:,:)+u_rhoo(2:Lpo,:,:))
   v_out(:,:,:) = 0.5*(v_rhoo(:,1:Mo,:)+v_rhoo(:,2:Mpo,:))

   ! Used in debugging
!   minv = 50000.; maxv = -50000.
!   DO k=1,size(u_out,3)
!   DO j=1,size(u_out,2)
!   DO i=1,size(u_out,1)
!     if (u_out(i,j,k) < minv) then
!       minv = u_out(i,j,k)
!     endif
!     if (u_out(i,j,k) > maxv) then
!       maxv = u_out(i,j,k)
!     endif
!   ENDDO
!   ENDDO
!   ENDDO
!   PRINT '(A,2F8.3)','Check reading input (8): Minimum and maximum value of U-speed: Umin,Umax = ', minv, maxv

! Output to netCDF file
   statuso = nf90_put_var(ncido,UVarIdo,u_out,start=(/1,1,1,otime/))
   if(statuso /= nf90_NoErr) call handle_err(statuso)
   statuso = nf90_put_var(ncido,VVarIdo,v_out,start=(/1,1,1,otime/))
   if(statuso /= nf90_NoErr) call handle_err(statuso)

  write(*,*) 'Completed velocities'
! ........................................................................
!
! Sea level

! Read in sea level
   statusi = nf90_inq_varid(ncidi,'zeta',ZetaVarIdi)
   if(statusi /= nf90_NoErr) call handle_err(statusi)
   statusi = nf90_get_att(ncidi, ZetaVarIdi, "scale_factor", Zeta_scale_factor)
   if(statusi /= nf90_noerr) then
     Zeta_scale_factor = 1.
   endif
   statusi = nf90_get_att(ncidi, ZetaVarIdi, "add_offset", Zeta_add_offset)
   if(statusi /= nf90_noerr) then
     Zeta_add_offset = 0.
   endif
   statusi = nf90_get_var(ncidi,ZetaVarIdi,zeta_in,start=(/ 1, 1, itime/))
   if(statusi /= nf90_NoErr) call handle_err(statusi)

   WHERE(zeta_in > -32766.) zeta_in = zeta_in * Zeta_scale_factor + Zeta_add_offset  ! Output is m

! Fill in masked-out values
      scr = 0.
      scr = zeta_in
      WHERE (rmaski < 0.01) scr = undef
      CALL fill(Lpi,Mpi,i1,i2,j1,j2,scr,tx,critx,cor,mxs,work,error,nvalue)
      zeta_in = scr

! Horizontal interpolation
   zeta_out = undef
   DO j=1,Mpo
   DO i=1,Lpo
      ia = INT(ipos(i,j))
      ja = INT(jpos(i,j))
      IF (ia < 1 .OR. ia > Lpi .OR. ja < 1 .OR. ja > Mpi) CYCLE
      rx = (ipos(i,j)-ia)
      ry = (jpos(i,j)-ja)
      rxm = 1.0-rx
      rym = 1.0-ry
         zeta_out(i,j) = rxm*rym*zeta_in(ia,ja)          +   &
                        rx*rym*zeta_in(ia+1,ja)          +   &
                        rx*ry*zeta_in(ia+1,ja+1)         +   &
                        rxm*ry*zeta_in(ia,ja+1)
   ENDDO
   ENDDO

! Fill in masked-out values
    WHERE (ABS(zeta_out) > 50.) zeta_out = undef
    CALL fill(Lpo,Mpo,1,Lpo,1,Mpo,zeta_out,tx,critx,cor,mxs,work,error,nvalue)

! Output to netCDF file
   statuso = nf90_put_var(ncido,ZetaVarIdo,zeta_out,start=(/1,1,otime/))
   if(statuso /= nf90_NoErr) call handle_err(statuso)

  write(*,*) 'Completed sea level'

! ........................................................................
!
! 2D Velocities

! Read in ubar-component
   statusi = nf90_inq_varid(ncidi,'ubar',UbarVarIdi)
   if(statusi /= nf90_NoErr) then      !call handle_err(statusi)

     ! ubar/vbar NOT on file, calculate from u/v
     ubar_out(:,:) = 0.
     vbar_out(:,:) = 0.
     DO j=1,Mpo
     DO i=1,Lo
       DO k=1,No
         ubar_out(i,j) = ubar_out(i,j) + u_out(i,j,k)*ds_ro(i,j,k)
       ENDDO
     ENDDO
     ENDDO
     DO j=1,Mo
     DO i=1,Lpo
       DO k=1,No
         vbar_out(i,j) = vbar_out(i,j) + v_out(i,j,k)*ds_ro(i,j,k)
       ENDDO
     ENDDO
     ENDDO
     sum_ds(:,:) = 0.
     DO k=1,No
       DO j=1,Mpo
       DO i=1,Lpo
         sum_ds(i,j) = sum_ds(i,j) + ds_ro(i,j,k)
       ENDDO
       ENDDO
     ENDDO
     ! sum_ds hould ideally be equal to 1, but ubar/vbar is divided to compensate for its small deviation
     ubar_out(:,:) = ubar_out(:,:)/sum_ds(1:Lo,1:Mpo)
     vbar_out(:,:) = vbar_out(:,:)/sum_ds(1:Lpo,1:Mo)

     print *,'ubar, vbar calculated from u, v'

     i = 300; j = 800;
     print '(A)','##############################################################'
     print '(A,2I6)','Test that calculation of ubar/vbar is correct, grid point:', i, j
     print '(A,35F8.3)','u(i,j,:)= ',u_out(i,j,:)
     print '(A,35F8.3)','v(i,j,:)= ',v_out(i,j,:)
     print '(A,35F10.3)','ds_ro(i,j,:)= ',ds_ro(i,j,:)
     print '(A,F10.3)','sum_ds(i,j)= ',sum_ds(i,j)
     print '(A,F8.3)','ubar_out(i,j) = ',ubar_out(i,j)
     print '(A,F8.3)','vbar_out(i,j) = ',vbar_out(i,j)
     print '(A)','##############################################################'

   else

   statusi = nf90_get_att(ncidi, UbarVarIdi, "scale_factor", Ubar_scale_factor)
   if(statusi /= nf90_noerr) then
     Ubar_scale_factor = 1.
   endif
   statusi = nf90_get_att(ncidi, UbarVarIdi, "add_offset", Ubar_add_offset)
   if(statusi /= nf90_noerr) then
     Ubar_add_offset = 0.
   endif
   statusi = nf90_get_var(ncidi,UbarVarIdi,ubar_in)
   if(statusi /= nf90_NoErr) call handle_err(statusi)

! Read in vbar-component
   statusi = nf90_inq_varid(ncidi,'vbar',VbarVarIdi)
   if(statusi /= nf90_NoErr) call handle_err(statusi)
   statusi = nf90_get_att(ncidi, VbarVarIdi, "scale_factor", Vbar_scale_factor)
   if(statusi /= nf90_noerr) then
     Vbar_scale_factor = 1.;
   endif
   statusi = nf90_get_att(ncidi, VbarVarIdi, "add_offset", Vbar_add_offset)
   if(statusi /= nf90_noerr) then
     Vbar_add_offset = 0.;
   endif
   statusi = nf90_get_var(ncidi,VbarVarIdi,vbar_in)
   if(statusi /= nf90_NoErr) call handle_err(statusi)

   WHERE(ubar_in > -32766.) ubar_in = ubar_in * Ubar_scale_factor + Ubar_add_offset
   WHERE(vbar_in > -32766.) vbar_in = vbar_in * Vbar_scale_factor + Vbar_add_offset

! Fill in masked-out values
   scru = 0.
   scru = ubar_in
   WHERE (umaski < 0.01) scru = undef
   CALL fill(Li,Mpi,i1,i2,j1,j2,scru,tx,critx,cor,mxs,work,error,nvalue)
   ubar_in = scru

   scrv = 0.
   scrv = vbar_in
   WHERE (vmaski < 0.01) scrv = undef
   CALL fill(Lpi,Mi,i1,i2,j1,j2,scrv,tx,critx,cor,mxs,work,error,nvalue)
   vbar_in = scrv

   scra = 0.
   scrb = 0.
! Average u and v to rho locations
   scra(2:Li,:) = 0.5*(ubar_in(1:Li-1,:)+ubar_in(2:Li,:))
   scra(1,:) = scra(2,:)
   scra(Lpi,:) = scra(Li,:)
   scrb(:,2:Mi) = 0.5*(vbar_in(:,1:Mi-1)+vbar_in(:,2:Mi))
   scrb(:,1) = scrb(:,2)
   scrb(:,Mpi) = scrb(:,Mi)

! Rotate velocity components
   scr  = 0.
   scr1 = 0.
   scr  =  scra*COS(anglei) - scrb*SIN(anglei)
   scr1 =  scrb*COS(anglei) + scra*SIN(anglei)
   scra = scr
   scrb = scr1

! u_rho

! Horizontal interpolation
   scr1o = undef
   DO j=1,Mpo
   DO i=1,Lpo
      ia = INT(ipos(i,j))
      ja = INT(jpos(i,j))
      IF (ia < 1 .OR. ia > Lpi .OR. ja < 1 .OR. ja > Mpi) CYCLE
      rx = (ipos(i,j)-ia)
      ry = (jpos(i,j)-ja)
      rxm = 1.0-rx
      rym = 1.0-ry
         scr1o(i,j) = rxm*rym*scra(ia,ja)           +   &
                      rx*rym*scra(ia+1,ja)          +   &
                      rx*ry*scra(ia+1,ja+1)         +   &
                      rxm*ry*scra(ia,ja+1)
   ENDDO
   ENDDO

! v_rho

! Horizontal interpolation
   scr2o = undef
   DO j=1,Mpo
   DO i=1,Lpo
      ia = INT(ipos(i,j))
      ja = INT(jpos(i,j))
      IF (ia < 1 .OR. ia > Lpi .OR. ja < 1 .OR. ja > Mpi) CYCLE
      rx = (ipos(i,j)-ia)
      ry = (jpos(i,j)-ja)
      rxm = 1.0-rx
      rym = 1.0-ry
         scr2o(i,j) = rxm*rym*scrb(ia,ja)           +   &
                      rx*rym*scrb(ia+1,ja)          +   &
                      rx*ry*scrb(ia+1,ja+1)         +   &
                      rxm*ry*scrb(ia,ja+1)
   ENDDO
   ENDDO

! Fill in masked-out values
   WHERE (ABS(scr1o) > 50.) scr1o = undef
   CALL fill(Lpo,Mpo,1,Lpo,1,Mpo,scr1o,tx,critx,cor,mxs,work,error,nvalue)
   WHERE (ABS(scr2o) > 50.) scr2o = undef
   CALL fill(Lpo,Mpo,1,Lpo,1,Mpo,scr2o,tx,critx,cor,mxs,work,error,nvalue)

! Rotate u_rhoo, v_rhoo to output grid
   scrao = 0.
   scrbo = 0.
   scrao =  scr1o*COS(angleo) + scr2o*SIN(angleo)
   scrbo =  scr2o*COS(angleo) - scr1o*SIN(angleo)

! Put u and v on to staggered grid
   scruo = 0.5*(scrao(1:Lo,:)+scrao(2:Lpo,:))
   scrvo = 0.5*(scrbo(:,1:Mo)+scrbo(:,2:Mpo))

   ubar_out = scruo
   vbar_out = scrvo

  write(*,*) 'ubar, vbar read from input file'

  endif

! Output to netCDF file
   statuso = nf90_put_var(ncido,UbarVarIdo,ubar_out,start=(/1,1,otime/))
   if(statuso /= nf90_NoErr) call handle_err(statuso)
   statuso = nf90_put_var(ncido,VbarVarIdo,vbar_out,start=(/1,1,otime/))
   if(statuso /= nf90_NoErr) call handle_err(statuso)

  write(*,*) 'Completed ubar, vbar'


! ----------------------------------------------------------------

! Write out time
   tday = time_in/86400. + 8036  ! Input in ROMS is in s, output should be in days

   statuso = nf90_put_var(ncido,TimeVarIdo,tday,start=(/otime/))
   if(statuso /= nf90_NoErr) call handle_err(statuso)

   write(*,*) 'Wrote time = ',tday,' days, time_in = ', time_in

ENDDO

statusi = nf90_close(ncidi)
if(statusi /= nf90_NoErr) call handle_err(statusi)

! ----------------------------------------------------------------
ENDDO
999 CONTINUE

! ----------------------------------------------------------------
statuso = nf90_sync(ncido)
if(statuso /= nf90_NoErr) call handle_err(statuso)
statuso = nf90_close(ncido)
if(statuso /= nf90_NoErr) call handle_err(statuso)

END PROGRAM roms2roms_horverinterp
