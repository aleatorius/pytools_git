#!/bin/bash

BASE=$PWD
cd ${BASE}

echo "Make initial-file for 2006"
ncea -F -d clim_time,1,1 ${BASE}/ini_2014.nc ${BASE}/svalbard_800m_ini.nc  # Get initial fields
ncrename -h -O -v clim_time,ocean_time ${BASE}/svalbard_800m_ini.nc                        # Change name of time variable
ncrename -h -O -d clim_time,time       ${BASE}/svalbard_800m_ini.nc                        # Change name of time dimension
for var in zeta ubar vbar u v salt temp; do  # Remove clim_time as attribute for every variable
  ncatted -h -O -a time,${var},d,c,"clim_time" ${BASE}/svalbard_800m_ini.nc
done
ncatted -O -a units,ocean_time,c,c,"days since 1948-01-01 00:00:00" ${BASE}/svalbard_800m_ini.nc

exit
