#!/bin/bash
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>extract.log_${datstamp} 2>&1
set -x
#yyy="2002 2003 2004 2005 2006 2007 2008 2009 2010"
yyy="2002 2003 2004 2005 2006"
#yyy="2001"
list=sizeordered.txt

for yy in $yyy; do
    echo $yy
    file=/global/work/norstosl/thinf/${yy}_files_jens_zipped.zip
   # file=$l
    if [ -f $file ]; then
        echo "file exist"
	if [ -s "$file" ]
	then
	    echo "$file has some data."
	    grep "$yy" $list | while read -r line ; do
		echo "Processing"
		IFS=' ' read -a file2ex <<< "$line"
		LENGTH=${#file2ex[@]}
		echo "file to extract: ${file2ex[$LENGTH-1]}"
		file2ex=${file2ex[$LENGTH-1]}
		if unzip -j $file $file2ex -d "/global/work/mitya/recovered"; then
		    echo "success: $file2ex"
		else
		    echo "FAILURE: $file2ex"
		fi

	    done
	else
	    echo "ERROR $file is empty. Check norstore"

	fi
    else
        echo "A norstore file doesnt exist"
    fi
done
set +x