#!/usr/bin/sh

#source ~/modules_python.sh


set -x
#datstamp=`date +%Y_%m_%d_%H_%M`
pathout=/nird/projects/NS9081K/NORSTORE_OSL_DISK/NS9081K/Mitya_Stuff/S4K_2014_2015_snow/spinup_01/
pathin=${PWD#*}
echo $pathin
#WORK=/work/mitya
#homedir=/home/ntnu/mitya

#EXPECTED_ARGS=2
#E_BADARGS=65

#if [ $# -ne $EXPECTED_ARGS ]
#then
#  echo "Usage: `basename $0` {arg1 arg2}"
#  exit $E_BADARGS
#fi

#echo $1 $2  ' -> echo $1 $2'
#args=("$@")
#echo ${args[0]} ${args[1]} ' -> args=("$@"); echo ${args[0]} ${args[1]}'
#echo $@ ' -> echo $@'
#echo Number of arguments passed: $# ' -> echo Number of arguments passed: $#' 

#echo {$i..$j}
server=mitya@login.nird.sigma2.no
for file in "$@"
do
    echo $file
   # file=$l
    if [ -f $file ]; then
	echo "file exists"
	echo $file
	fileout=${pathout}${file}   # %.*}_${array[2]}.${file#*.}
	    #if ssh mitya@stallo.uit.no stat $fileout \> /dev/null 2\>\&1; then
		#echo 'A remote file already exists, do nothing'
	    #else
	#	echo "Proceed with  ssh-copying of this file"
	if scp -p $file ${server}:$fileout; then
		rm $file
	else
	    echo "scp doesn't work!"
	    exit 1
	fi
    else
	echo "A local file does not exist"
    fi
done


set +x



exit
