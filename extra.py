#!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm
import numpy.ma as ma
import matplotlib.dates as mdates
parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)
parser.add_argument(
'-mask', 
help='kasn file', 
dest='mask', 
action="store"
)
parser.add_argument(
'-v', 
help='variable', 
dest='var', 
action="store"
)


args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa




#for i in files:
#    print i
f = Dataset(args.inf,'r+')
m = Dataset(args.mask,'r')

try:
    var = unpack(f.variables[args.var])[0,:]
    mask = unpack(m.variables["var_mask"])[:]
    print var.shape,mask.shape, "var shape, here"
except:
    print "no var ", args.var


undef = 2.0e+35
tx = 0.9 * undef
critx = 0.01
cor = 1.6
mxs = 10
field = var
field = np.where(mask == 1, undef, field)
import extrapolate as ex
field = ex.extrapolate.fill(1, int(var.shape[1]),
                            1, int(var.shape[0]),
                            float(tx), float(critx), float(cor), float(mxs),
                            np.asarray(field, order='Fortran'),
                            int(var.shape[1]),int(var.shape[0]))



f.variables[args.var][0,:]=field[:]
f.close()
m.close()
exit()




