#!/usr/bin/env python 
##!/home/ntnu/mitya/virt_env/virt1/bin/python -B
## Dmitry Shcherbin, 2014.07.01
import numpy as np
from netCDF4 import *
import sys, re, glob, os
import argparse
import pprint
from pprint import *
parser = argparse.ArgumentParser(description='pyview 0.1')

parser.add_argument('-i', help='input file to process', dest='inf', action="store")
parser.add_argument('-domain', help='source', dest ='domain', action="store")
parser.add_argument('-o', help='output', dest ='outf', action="store")
parser.add_argument('-v', help='variable', dest ='variable', action="store")
args = parser.parse_args()

if not (args.inf):
    parser.error('please give a file name')
if not os.path.isfile(args.inf):
     print 'file does not exists'
     parser.error('please give a file name')
     

def print_list(data):
    col_width = max(len(i) for i in data) + 2  # padding
    count = 0
    a= []
    for i in data:
        count = count +1
        if count < 6:
            a.append(i)
        else:
            print "".join(j.ljust(col_width) for j in a)
            a = []
            count = 0


if not (args.variable):
    f = Dataset(args.inf)
    print_list(f.variables.keys())
    f.close()



def unpack(ina):
     outa = np.zeros(ina.shape)
     outa[:] = ina[:]
     return outa

f = Dataset(args.inf)
print f
print f.variables["latitude"]
lat = unpack(f.variables["latitude"])
lon = unpack(f.variables["longitude"])
fyear = unpack(f.variables["fyear"])
print fyear
f.close()

print lat.shape, lon.shape

g = Dataset(args.domain)
#print g
lat_d = unpack(g.variables["lat_rho"])
lon_d = unpack(g.variables["lon_rho"])
g.close()
print lat_d.shape, lon_d.shape
print lat_d[1,1], lon_d[1,1]

def find_nearest(array1,array2, value1, value2):
    array1_res = np.abs(array1-value1)#.argmin()
    array2_res = np.abs(array2-value2)#.argmin()
    array_sum = np.sin(np.radians(array1_res)/2.)*np.sin(np.radians(array1_res)/2.)+np.cos(np.radians(array1))*np.cos(np.radians(value1*np.ones(array1.shape)))*np.sin(np.radians(array2_res)/2.)*np.sin(np.radians(array2_res)/2.)
    idx = array_sum.argmin()
    index = np.unravel_index(idx, array_sum.shape)
    return index

ind = []
ex  = []
if args.outf:
    file = open(args.outf,"w")
else:
    file = open("mapping.txt","w")
for index,val in np.ndenumerate(lat_d):
#    print index, val
#    print lat_d[index], lon_d[index] 
    index_n = find_nearest(lat,lon,lat_d[index], lon_d[index]) 
#    print lat[index_n], lon[index_n]
#    print index, index_n
#    ind.append(index)
#    ex.append(index_n)
#    print "______"
    string = str(index[0])+" "+str(index[1])+" "+str(index_n[0])+" "+str(index_n[1])+"\n"
#    print string
    file.write(string)
file.close()
    



#lat_lon = zip(lat,lon)
#print size(lat_lon)
#print lat_lon[1]
#for i in lat_lon:
#    print i

#ncvar,ot = extract(args.inf, args.variable, args.time, args.vert)


sys.exit("here I stop")
#fillval = 1e+36
