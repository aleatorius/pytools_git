#!/usr/bin/env python 
##!/home/ntnu/mitya/virt_env/virt1/bin/python -B
## Dmitry Shcherbin, 2014.07.01
import numpy as np
from numpy import zeros, size
from netCDF4 import *
import sys, re, glob, os
import numpy.ma as ma
import argparse
import os.path
import datetime
from datetime import date, time
import pprint
from pprint import *
parser = argparse.ArgumentParser(description='pyview 0.1')

parser.add_argument('-i', help='input file to process', dest='inf', action="store")
parser.add_argument('-map', help='source', dest ='map', action="store")
parser.add_argument('-v', help='variable', dest ='variable', action="store")
parser.add_argument('-out', help='out file', dest='outf', action="store", default="s.nc")
parser.add_argument('--time_rec', help='time rec', dest ='time_rec', action="store", default="ocean_time")
parser.add_argument('--time', help='time counter', dest ='time', action="store", type=int, default=0)
parser.add_argument('--vert', help='vertical coordinate number', dest ='vert', action="store", type=int, default=-1)
parser.add_argument('--xzoom', help='zoom along x(?) direction, range is defined in percents', dest ='xzoom', action="store",  default='0:100')
parser.add_argument('--yzoom', help='zoom along y(?) direction, range is defined in percents', dest ='yzoom', action="store",  default='0:100')
parser.add_argument('--var_min', help='minimum value of variable', dest ='var_min', action="store", type=float, default = None)
parser.add_argument('--var_max', help='minimum value of variable', dest ='var_max', action="store", type=float, default = None)
args = parser.parse_args()

if not (args.inf):
    parser.error('please give a file name')
if not os.path.isfile(args.inf):
     print 'file does not exists'
     parser.error('please give a file name')
     

def print_list(data):
    col_width = max(len(i) for i in data) + 2  # padding
    count = 0
    a= []
    for i in data:
        count = count +1
        if count < 6:
            a.append(i)
        else:
            print "".join(j.ljust(col_width) for j in a)
            a = []
            count = 0


def date_time(ot):
     ref = date(1970,01,01)
     ref_time = time(0,0,0)
     refer= datetime.datetime.combine(ref, ref_time)
     if args.time_f =='s':
         return (refer + datetime.timedelta(float(ot)/(3600*24))).strftime("%Y-%m-%d %H:%M:%S")
     else:
         return (refer + datetime.timedelta(float(ot))).strftime("%Y-%m-%d %H:%M:%S")




def unpack(ina):
     outa = zeros(ina.shape)
     outa[:] = ina[:]
     return outa


f = open(args.map, "r")
a = []
for line in f.readlines():
    st = line.split()
    a.append(st)
f.close()

grid_file = "/home/mitya/models/NorROMS/ROMS_src/local/Apps/Barents-4km/arctic4km_grd_bath20_svalbard.nc"
grid = Dataset(grid_file)

nc = Dataset(args.outf, 'w', format='NETCDF3_64BIT')
for i in grid.dimensions.keys():
    print i
    nc.createDimension(i, len(grid.dimensions[i]))

for i in grid.variables.keys():
    print i, grid.variables[i].dimensions
    w = nc.createVariable(i, grid.variables[i].dtype, grid.variables[i].dimensions)
    nc.variables[i][:] = grid.variables[i][:]


nc.close()
grid.close()
sys.exit("here i am")
print f
print f.variables["latitude"]
lat = unpack(f.variables["latitude"])
lon = unpack(f.variables["longitude"])
fyear = unpack(f.variables["fyear"])
print fyear
f.close()

print lat.shape, lon.shape

g = Dataset(args.domain)
#print g
lat_d = unpack(g.variables["lat_rho"])
lon_d = unpack(g.variables["lon_rho"])
g.close()
print lat_d.shape, lon_d.shape
print lat_d[1,1], lon_d[1,1]

def find_nearest(array1,array2, value1, value2):
    array1_res = np.abs(array1-value1)#.argmin()
    array2_res = np.abs(array2-value2)#.argmin()
    array_sum = np.sin(np.radians(array1_res)/2.)*np.sin(np.radians(array1_res)/2.)+np.cos(np.radians(array1))*np.cos(np.radians(value1*np.ones(array1.shape)))*np.sin(np.radians(array2_res)/2.)*np.sin(np.radians(array2_res)/2.)
    idx = array_sum.argmin()
    index = np.unravel_index(idx, array_sum.shape)
    return index

ind = []
ex  = []
file = open("mapping.txt","w")

for index,val in np.ndenumerate(lat_d):
#    print index, val
#    print lat_d[index], lon_d[index] 
    index_n = find_nearest(lat,lon,lat_d[index], lon_d[index]) 
#    print lat[index_n], lon[index_n]
#    print index, index_n
#    ind.append(index)
#    ex.append(index_n)
#    print "______"
    string = str(index[0])+" "+str(index[1])+" "+str(index_n[0])+" "+str(index_n[1])+"\n"
    print string
    file.write(string)
file.close()
    



#lat_lon = zip(lat,lon)
#print size(lat_lon)
#print lat_lon[1]
#for i in lat_lon:
#    print i

#ncvar,ot = extract(args.inf, args.variable, args.time, args.vert)


sys.exit("here I stop")
#fillval = 1e+36
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm


mx = ma.masked_outside(ncvar, -1e+36,1e+36)
mx = mx[int(float(args.yzoom.split(':')[0])*mx.shape[0]/100.):int(float(args.yzoom.split(':')[1])*mx.shape[0]/100.), 
        int(float(args.xzoom.split(':')[0])*mx.shape[1]/100.):int(float(args.xzoom.split(':')[1])*mx.shape[1]/100.)]
cmap=plt.cm.spectral
fig = plt.figure(1)
p=plt.pcolormesh(mx,cmap=cmap);plt.colorbar()     




def onclick(event):
    if event.inaxes != None:
        print event.inaxes
        try:
            dat = str('%.1f' % float(mx[(event.ydata, event.xdata)]))
        except:
            dat = 'nan'
        txt = plt.text(event.xdata, event.ydata, dat , fontsize=8)
        txt.set_bbox(dict(color='white', alpha=0.5, edgecolor='red'))
        plt.plot(event.xdata, event.ydata,'bo', markersize=2)
        fig.canvas.draw()
    else:
        print event.inaxes


fig.canvas.mpl_connect('button_press_event', onclick)


if args.var_min or args.var_max:
    if args.var_min and args.var_max:
        if args.var_min < args.var_max:
            p.set_clim(float(args.var_min), float(args.var_max))
        else:
            print 'incorrect var_min shoud be less than var_max, defaults then'
            pass
    else:
        p.set_clim(args.var_min, args.var_max)
else:
     pass


plt.axis('tight')
try:
    plt.title(args.variable+' '+ date_time(ot[args.time]))
except:
    plt.title(args.variable+' \n no date')
plt.show()
