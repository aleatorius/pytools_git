#!/usr/bin/env python
##!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm
import numpy.ma as ma
import matplotlib.dates as mdates
parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-grid', 
help='input file', 
dest='grid', 
action="store"
)

parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)



args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa



f = Dataset(args.grid)
mask_rho = unpack(f.variables["mask_rho"])
mask_u = unpack(f.variables["mask_u"])
mask_v =unpack(f.variables["mask_v"])
mask_psi =unpack(f.variables["mask_psi"])
bath=unpack(f.variables["h"])
f.close()

#salt=unpack(g.variables["salt"])[-1,2,:,:]
#zeta=unpack(g.variables["zeta"])[-1,:,:]
#temp=unpack(g.variables["temp"])[-1,-1,:,:]
#u=unpack(g.variables["u"])[-1,-1,:,:]
#v=unpack(g.variables["v"])[-1,-1,:,:]
#print "shapes zeta, slat", zeta.shape, salt.shape
e_pos=[]
x_pos=[]
e_out=[]
x_out=[]
fb=5.0
print mask_rho.shape, mask_u.shape, mask_v.shape,mask_psi.shape, "rho,u,v-shaes"
for i in range(0,mask_u.shape[0]):
    for j in range(0,mask_u.shape[1]):
        if mask_u[i,j]-mask_rho[i,j]*mask_rho[i,j+1]!=0.0:
            if fb in [bath[i,j],bath[i,j+1]]:
                if bath[i,j]==fb:
                    if mask_rho[i,j]==1.0:
                        x_pos.append(i)
                        e_pos.append(j)
                else:
                    if mask_rho[i,j+1]==1.0:
                        x_pos.append(i)
                        e_pos.append(j+1)
            else:
                x_out.append(i)
                e_out.append(j)
for i in range(0,mask_v.shape[0]):
    for j in range(0,mask_v.shape[1]):
        if mask_v[i,j]-mask_rho[i,j]*mask_rho[i+1,j]!=0.0:
            if fb in [bath[i,j],bath[i+1,j]]:
                if bath[i,j]==fb:
                    if mask_rho[i,j]==1.0:
                        x_pos.append(i)
                        e_pos.append(j)
                else:
                    if mask_rho[i+1,j]==1.0:
                        x_pos.append(i+1)
                        e_pos.append(j)
            else:
                x_out.append(i)
                e_out.append(j)
                    
for i in range(0,mask_psi.shape[0]):
    for j in range(0,mask_psi.shape[1]):
        if mask_psi[i,j]-mask_rho[i,j]*mask_rho[i+1,j]*mask_rho[i+1,j+1]*mask_rho[i,j+1]!=0.0:
            if fb in [bath[i,j],bath[i+1,j],bath[i+1,j+1],bath[i,j+1]]:
                if  bath[i,j]==fb:
                    if mask_rho[i,j]==1.0:
                        x_pos.append(i)
                        e_pos.append(j)
                elif bath[i+1,j]==fb:
                    if mask_rho[i+1,j]==1.0:
                        x_pos.append(i+1)
                        e_pos.append(j)
                elif bath[i+1,j+1]==fb:
                    if mask_rho[i+1,j+1]==1.0:
                        x_pos.append(i+1)
                        e_pos.append(j+1)
                else:
                    if mask_rho[i,j+1]==1.0:
                        x_pos.append(i)
                        e_pos.append(j+1)
            else:
                x_out.append(i)
                e_out.append(j)
myset=set(zip(x_pos,e_pos))
outset=zip(x_out,e_out)
g = Dataset(args.inf,"r+")
for i in myset:
    g.variables["salt"][0,:,i[0],i[1]]=34.5*np.ones(35)
    g.variables["temp"][0,:,i[0],i[1]]=0.5*np.ones(35)
    g.variables["zeta"][0,i[0],i[1]]=0.0    

xn_pos = []
en_pos = []

var=g.variables["salt"][0,0,:,:]
for i,j in np.ndenumerate(ma.masked_where(mask_rho[0]==0, var)):
    if abs(j)>37.5 and mask_rho[i]==1:
        xn_pos.append(i[0])
        en_pos.append(i[1])
print len(xn_pos)
for i in zip(xn_pos,en_pos):
    g.variables["salt"][0,:,i[0],i[1]]=34.5*np.ones(35)
    g.variables["temp"][0,:,i[0],i[1]]=0.1*np.ones(35)

xn_pos = []
en_pos = []


var=g.variables["zeta"][0,:,:]
for i,j in np.ndenumerate(ma.masked_where(mask_rho[0]==0, var)):
    if abs(j)>2. and mask_rho[i]==1:
        xn_pos.append(i[0])
        en_pos.append(i[1])
print len(xn_pos)
for i in zip(xn_pos,en_pos):
    g.variables["zeta"][0,i[0],i[1]]=0.0

g.close()
exit()
var_masked=ma.masked_where(mask_rho==0, g.variables["salt"][0,0,:,:])
fig = plt.figure(figsize=(12,8))
ax = fig.add_subplot(111)
palette = plt.cm.plasma
p=ax.pcolormesh(var_masked, cmap=palette)
#ax.plot(e_pos,x_pos,'gs', markersize=1)
plt.colorbar(p)
plt.show()


