#!/usr/bin/env python 
##!/home/ntnu/mitya/virt_env/virt1/bin/python -B
## Dmitry Shcherbin, 2014.11.10


import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob, os
import numpy.ma as ma
import argparse
import os.path
import datetime
from datetime import date, time
import pprint
from pprint import *
import matplotlib 
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pylab
from pylab import *
from matplotlib import colors, cm
from pylab import savefig

plt.ioff()

parser = argparse.ArgumentParser(description='transect write 0.1')

parser.add_argument('-i', help='input file', dest='inf', action="store")
parser.add_argument('-is', help='segments vertices', dest='segments', action="store", default="segments")
parser.add_argument('-v', help='variable', dest ='variable', action="store")
parser.add_argument('-f', help='time format', dest='time_f', action="store", default="s")
parser.add_argument('--contourf', help='colormesh or contourf', dest='contourf',choices=("yes","no"), action="store", default="no")
parser.add_argument('--npinter', help='numpy interpolation', dest='npinter',choices=("yes","no"), action="store", default="yes")
parser.add_argument('--res', help='resolution, fraction of minimum distance between layers', dest='res', action="store", type=float, default=2)
parser.add_argument('--extras', help='s-layers', dest='extras',choices=('yes', 'no'), action="store", default="no")
parser.add_argument('--interpolate', help='vertical interpolation, off by default', choices=('yes', 'no'), dest='interpolate', action="store", default="no")
parser.add_argument('--time_rec', help='time rec', dest ='time_rec', action="store", default="ocean_time")
parser.add_argument('--time', help='time counter', dest ='time', action="store", type=int, default=0)
parser.add_argument('--vert', help='vertical coordinate number', dest ='vert', action="store", type=int, default=34)
parser.add_argument('--xzoom', help='zoom along x(?) direction, range is defined in percents', dest ='xzoom', action="store",  default='0:100')
parser.add_argument('--yzoom', help='zoom along y(?) direction, range is defined in percents', dest ='yzoom', action="store",  default='0:100')
parser.add_argument('--var_min', help='minimum value of variable', dest ='var_min', action="store", type=float, default = None)
parser.add_argument('--var_max', help='minimum value of variable', dest ='var_max', action="store", type=float, default = None)
parser.add_argument('--depth_min', help='min depth, positive value', dest ='depth_min', action="store", type=float, default = None)
parser.add_argument('--depth_max', help='max depth, positive value', dest ='depth_max', action="store", type=float, default = None)
parser.add_argument('-o', help='output dir', dest='dir', action="store", default='frames')
parser.add_argument('--title', help='insert title', dest ='title', action="store", default = '')

args = parser.parse_args()

if not (args.inf):
    parser.error('please give a file name')
if not os.path.isfile(args.inf):
     print 'file does not exists'
     parser.error('please give a file name')
     

if ':' not in list(args.xzoom):
     parser.error('provide zoom in i:k format!')
else:
     for i in [0,1]:
          if 0 <= float(args.xzoom.split(':')[i])<= 100:
               pass
          else:
               parser.error('whole range 0:100')

if ':' not in list(args.yzoom):
     parser.error('provide zoom in i:k format!')
else:
     for i in [0,1]:
          if 0 <= float(args.yzoom.split(':')[i])<= 100:
               pass
          else:
               parser.error('wrong xzoom, whole range 0:100')


if float(args.yzoom.split(':')[0])>= float(args.yzoom.split(':')[1]):
     parser.error('in zoom range i:k i<k')
if float(args.xzoom.split(':')[0])>= float(args.xzoom.split(':')[1]):
     parser.error('in zoom range i:k i<k')

def print_list(data):
    col_width = max(len(i) for i in data) + 2  # padding
    count = 0
    a= []
    for i in data:
        count = count +1
        if count < 6:
            a.append(i)
        else:
            print "".join(j.ljust(col_width) for j in a)
            a = []
            count = 0


def date_time(ot):
     ref = datetime.date(1948,01,01)
     ref_time = datetime.time(0,0,0)
     refer= datetime.datetime.combine(ref, ref_time)
     if args.time_f =='s':
         return (refer + datetime.timedelta(float(ot)/(3600*24))).strftime("%Y-%m-%d %H:%M:%S")
     else:
         return (refer + datetime.timedelta(float(ot))).strftime("%Y-%m-%d %H:%M:%S")

def date_time_out(ot):
     ref = datetime.date(1948,01,01)
     ref_time = datetime.time(0,0,0)
     refer= datetime.datetime.combine(ref, ref_time)
     if args.time_f =='s':
         return (refer + datetime.timedelta(float(ot)/(3600*24))).strftime("%Y-%m-%d")
     else:
         return (refer + datetime.timedelta(float(ot))).strftime("%Y-%m-%d")


if not (args.variable):
    f = Dataset(args.inf)
    print_list(f.variables.keys())
    f.close()
    sys.exit()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa

def extract_lat_lon(inf):
    f = inf
    try:
        lat = unpack(f.variables["lat_rho"])
        lon = unpack(f.variables["lon_rho"])
    except:
        pass
    try:
        lat = unpack(f.variables["lat"])
        lon = unpack(f.variables["lon"])
    except:
        pass
    return lat, lon

def extract_vert(inf, inv):
     f = inf
     if str(inv) in f.variables.keys():
         print 'variable', inv, 'exists'
         ncvar = unpack(f.variables[inv])
         print 'the grid dimensions of requested variable:', ncvar.shape

     else:
         print 'provided variable doesnt exists, choose from the list:\n'
         print_list(f.variables.keys())          
         f.close()
         sys.exit()
     return ncvar

def extract(inf, variable, time, vert):
     f = inf
     if str(variable) in f.variables.keys():
         print 'variable', variable, 'exists'
         ncvar = unpack(f.variables[variable][time,:])
         print 'and its dimension equals:', size(ncvar.shape)+1
         if size(ncvar.shape) == 3:
             ncvar = unpack(f.variables[variable][time,vert,:])
         if size(ncvar.shape) == 1:
             print 'possibly there is no time dimension, so it is shown as it is'
             ncvar = unpack(f.variables[variable])
         else:
             pass
         print 'the grid dimensions of requested variable:', ncvar.shape

     else:
         print 'provided variable doesnt exists, choose from the list:\n'
         print_list(f.variables.keys())          
         f.close()
         sys.exit()
     ot = unpack(f.variables[args.time_rec])
     return ncvar, ot



def extract_vertical(inf, variable, time):
     f = inf
     if str(variable) in f.variables.keys():
         #print '\n In the file', inf
         print 'variable', variable, 'exists'
         ncvar = unpack(f.variables[variable][time,:])
         # print 'and its dimension equals:', size(ncvar.shape)+1
         # if size(ncvar.shape) == 3:
         #      ncvar = unpack(f.variables[variable][time,vert,:])
         # if size(ncvar.shape) == 1:
         #      print 'possibly there is no time dimension, so it is shown as it is'
         #      ncvar = unpack(f.variables[variable])
         # else:
         #      pass
         # print 'the grid dimensions of requested variable:', ncvar.shape

     else:
         print 'provided variable doesnt exists, choose from the list:\n'
         print_list(f.variables.keys())          
         f.close()
         sys.exit()
     ot = unpack(f.variables[args.time_rec])
     return ncvar, ot


def extract_allrec(inf, variable, vert):
     f = inf
     if str(variable) in f.variables.keys():
         print 'variable', variable, 'exists'
         ncvar = unpack(f.variables[variable])
         print 'and its dimension equals:', size(ncvar.shape)+1
         if size(ncvar.shape) == 4:
             ncvar = unpack(f.variables[variable][:,vert,:])
         if size(ncvar.shape) == 3:
             print 'possibly there is no time dimension, so it is shown as it is'
             pass
         else:
             pass
         print 'the grid dimensions of requested variable:', ncvar.shape

     else:
         print 'provided variable doesnt exists, choose from the list:\n'
         print_list(f.variables.keys())          
         f.close()
         sys.exit()
     ot = unpack(f.variables[args.time_rec])
     return ncvar, ot


def extract_vertical_allrec(inf, variable):
     f = inf
     if str(variable) in f.variables.keys():
         #print '\n In the file', inf
         print 'variable', variable, 'exists'
         ncvar = unpack(f.variables[variable])
     else:
         print 'provided variable doesnt exists, choose from the list:\n'
         print_list(f.variables.keys())          
         f.close()
         sys.exit()
     ot = unpack(f.variables[args.time_rec])
     return ncvar, ot





#taken from here: Trond Kristiansen https://github.com/trondkr/romstools/tree/master/VolumeFlux
def z_r(zeta,h,s_rho, hc, Cs_r,N, Vtransform):
   # z_r = zeros(int(N),h.shape())


    z_r=[]
    if Vtransform == 2 or Vtransform == 4:
        for k in range(N):
            z0 = (hc * s_rho[k] + h*Cs_r[k])/(hc + h)
            z_r.append(zeta + (zeta + h)*z0)
    elif Vtransform == 1:
        for k in range(N):
            z0 = hc * s_rho[k] + (h - hc) * Cs_r[k]
            z_r.append(z0 + zeta * (1.0 + z0/h))
        
    return z_r


def z_w(zeta,h,s_w, hc, Cs_w,Np, Vtransform):
    print zeta.shape
    dim=[int(Np)]
    for i in zeta.shape:
        dim.append(i)
    z_w =np.zeros(dim)
    slc = [slice(None)] * (len(z_w.shape)-1)
        
    if Vtransform == 2 or Vtransform == 4:
        for k in range(Np):
            slc_0=[k]
            slc_0+=slc
            print z_w[slc_0].shape, slc_0, len(slc_0)
            z0 = (hc * s_w[k] + h*Cs_w[k])/(hc + h)
            z_w[slc_0]=zeta + (zeta + h)*z0
    elif Vtransform == 1:
        for k in range(Np):
            slc_0=[k]
            slc_0+=slc
            z0 = hc * s_w[k] + (h - hc) * Cs_w[k]
            print z_w[slc_0].shape
            z_w[slc_0]=z0 + zeta*(1.0 + z0/h)
    return z_w



#extract data from netcdf
f= Dataset(args.inf)


meta_trans_all, meta_time = extract_vertical_allrec(f, args.variable)
ze =  extract_allrec(f, "zeta", args.vert)[0]
mask_rho =  extract(f, "mask_rho", args.time, args.vert)[0]
Cs_r = extract_vert(f, 'Cs_r')
s_rho = extract_vert(f, 's_rho')
Cs_w = extract_vert(f, 'Cs_w')
s_w = extract_vert(f, 's_w')
Vtransform = extract_vert(f, 'Vtransform')
#Vstretching = extract_vert(args.inf, 'Vstretching')
N = len(s_rho)
Np = len(s_w)
print Np, "Np", N, "N"
#Tcline = extract_vert(args.inf, 'Tcline')
#theta_s = extract_vert(args.inf, 'theta_s')
#theta_b = extract_vert(args.inf, 'theta_b')
hc= extract_vert(f, 'hc')
#print Vtransform, Vstretching, N,Tcline, hc, theta_s, theta_b
h_m =  extract(f, "h", args.time, args.vert)[0]
h = ma.masked_outside(h_m, -1e+36,1e+36)
lat, lon = extract_lat_lon(f)
cmap=plt.cm.spectral

for t,k in enumerate(meta_time):
    fillval = 1e+36
    #masked data
    mx_trans = ma.masked_outside(meta_trans_all[t,:], -1e+36,1e+36)
    print mx_trans.shape, "mxtra"
    zeta = ma.masked_outside(ze[t,:], -1e+36,1e+36)
    print "mx_trans", mx_trans.shape
    
    #    z_w((j[1],j[0]),zeta, h, s_w,hc, Cs_w,Np, Vtransform)
    slc=[slice(None)]*len(zeta.shape)
    slc_1=[slice(0,-1,1)]+slc                                                                                                                                                                      
    slc_2=[slice(1,None,1)]+slc 
    z=z_w(zeta, h, s_w,hc, Cs_w,Np,Vtransform)
       
    depth_1=z[slc_1]
    depth_2=z[slc_2]
    width=depth_2-depth_1
    ave=np.sum(mx_trans*width, axis=0)
    average=ave/(h+zeta)
    print ave.shape
    
    
    

    fig_mesh_np,ax_mesh_np = plt.subplots(figsize=(10, 10))
    #ax_mesh_np.set_yticks(y_tick)
    #ax_mesh_np.set_yticklabels(y_label)

    #ax_mesh_np.set_xticks(x_tick)
    #ax_mesh_np.set_xticklabels(x_label)
    #ax_mesh_np.xaxis.grid(True)
    #ax_mesh_np.yaxis.grid(True)
    #plt.xticks(rotation=30)
    ax_mesh_np = plt.gca()
    #ax_mesh_np.invert_yaxis()
    mesh_masked = ma.masked_outside(average, -1e+36,1e+36)
    if args.contourf=="yes":
        pm=plt.contourf(mesh_masked, cmap=cmap);plt.colorbar()           
    else:
        #pm=plt.pcolormesh(mesh_masked, cmap=cmap);plt.colorbar()
        pm=plt.pcolormesh(mesh_masked, cmap=cmap);plt.colorbar()
    
    plt.title(args.title +'\n'+args.variable+' '+ date_time(k))
    plt.axis('tight')



    if args.var_min or args.var_max:
         if args.var_min and args.var_max:
              if args.var_min < args.var_max:
                   pm.set_clim(float(args.var_min), float(args.var_max))
              else:
                   print 'incorrect var_min shoud be less than var_max, defaults then'
                   pass
         else:
              pm.set_clim(args.var_min, args.var_max)
    else:
         pass
    plt.axis('tight')
    directory ='./'+str(args.dir)+'/'
    if os.path.isdir(directory)== False:
        os.makedirs(directory)
        print 'directory does not exist, so it is just created now', directory
    else:
        print 'directory ', directory, ' exists '
        
    print args.inf.split('/')
    pylab.savefig(directory+args.variable+'_'+date_time_out(k)+'.png')
#    rec = t
#    if rec in range(0,10):
#     
#    else:
#        pylab.savefig(directory+args.variable+'_'+args.inf.split('/')[-1].replace('.nc','_'+str(rec)+'.png'))
#    rec = rec +1
#    print rec



f.close()    
#plt.show()

sys.exit()




