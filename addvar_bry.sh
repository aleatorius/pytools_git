#!/bin/bash
set -x
WORK=/global/work/mitya
pathRun=${WORK}/metno/Clim_20
filein=test_bry.nc
nobioin=test_bry_noextra.nc
if [ -f $nobioin ]; then
    cp -f $filein $nobioin
else
    cp $filein $nobioin
fi

OLDIFS=$IFS
IFS=','
for i in NH4,0.0 PO4,0.1  ; do
    set $i
    echo $1 and $2
    for j in west east south north; do
	ncap -O -h -s "${1}_$j=(salt_${j}*0.+$2)" ${pathRun}/$nobioin -o ${pathRun}/$nobioin 
    done
done

for i in SdeN,0.18 SdeP,0.024  ; do
    set $i
    echo $1 and $2
    for j in west east south north; do
	ncap -O -h -s "${1}_${j}=(detritus_${j}*$2)" ${pathRun}/$nobioin -o ${pathRun}/$nobioin 
    done
done

for i in phyn,0.18 phyp,0.024  ; do
    set $i
    echo $1 and $2
    for j in west east south north; do
	ncap -O -h -s "${1}_${j}=(phyt_${j}*$2)" ${pathRun}/$nobioin -o ${pathRun}/$nobioin 
    done
done

for i in zopn,0.18 zopp,0.024  ; do
    set $i
    echo $1 and $2
    for j in west east south north; do
	ncap -O -h -s "${1}_${j}=(zoop_${j}*$2)" ${pathRun}/$nobioin -o ${pathRun}/$nobioin 
    done
done


IFS=$OLDIFS
#
	
#done
mv ${pathRun}/$nobioin ${pathRun}/test_bry_with_extra.nc

set +x
exit

