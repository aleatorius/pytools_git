#!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm
import numpy.ma as ma
import matplotlib.dates as mdates
parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)
parser.add_argument(
'-v', 
help='variable', 
dest='var', 
action="store"
)


args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa


files = [args.inf]


for i in files:
    print i
    f = Dataset(i,'r')
    try:
        var = unpack(f.variables[args.var])[0,:]
        print var.shape, "var shape, here"
    except:
        print "no var ", args.var

    nc = Dataset("mask.nc", 'w', format='NETCDF3_64BIT')
    nc.createDimension('y', var.shape[0])
    nc.createDimension('x',var.shape[1])
    nc.createVariable('var_mask',np.short,('y','x'))

     

    e_pos=[]
    x_pos=[]


    for i,j in np.ndenumerate(var):
        if j < -25:
            print i,j
            x_pos.append(i[0])
            e_pos.append(i[1])
            print var[i[0],i[1]],j
            nc.variables["var_mask"][i[0],i[1]]=1
        else:
            pass
            nc.variables["var_mask"][i[0],i[1]]=0
    print len(x_pos)


nc.close()
exit()
