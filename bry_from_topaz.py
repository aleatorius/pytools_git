#!/home/mitya/testenv/bin/python -B                                                                                   
import os
import sys
from datetime import datetime

import numpy as np
#import matplotlib.pyplot as plt
from netCDF4 import *
import numpy.ma as ma
#import netcdftime
#import time as tm
#from calendar import monthrange
#import datetime 
#from datetime import date, time
import argparse



parser = argparse.ArgumentParser(description='transect write 0.1')

parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)

parser.add_argument(
'-o', 
help='output file', 
dest='outf', 
action="store"
)

args = parser.parse_args()


#vertical coordinates parameters


f = Dataset(args.inf)
nc = Dataset(args.inf+"_bry", 'w', format='NETCDF3_64BIT')

for i in f.dimensions.keys():
    print len(f.dimensions[i])
    nc.createDimension(i, ( len(f.dimensions[i]) if not f.dimensions[i].isunlimited() else None))   

for name, variable in f.variables.items():
#        if name not in toexclude:
    if len(variable.dimensions) < 3:
 
    # copy variable attributes all at once via dictionary
        try:
            nc[name].setncatts(f[name].__dict__)
        except:
            pass
        nc.createVariable(name, variable.datatype, variable.dimensions)
        nc[name][:] = f[name][:]
        print name
#        print  nc[name][:]

    else:
        print name,variable.dimensions,"oops", len(variable.dimensions), variable.dimensions[:-1]
        for i in ["_W_bry","_E_bry"]:
            nc.createVariable(name+i, variable.datatype, variable.dimensions[:-1])
            try:
                nc[name+i].setncatts(f[name].__dict__)
            except:
                pass
            slc = [slice(None)] * ( len(variable.dimensions) -1)
            if i == "_W_bry":
                slc +=[0]
                print slc
                nc[name+i][:] = f[name][slc]
            else:
                slc +=[-1]
                print slc
                nc[name+i][:] = f[name][slc]
        for i in ["_S_bry","_N_bry"]:
            nc.createVariable(name+i, variable.datatype, variable.dimensions[:-2]+ variable.dimensions[-1:])
            try:
                nc[name+i].setncatts(f[name].__dict__)
            except:
                pass
            slc = [slice(None)] * ( len(variable.dimensions) -2)
            if i == "_S_bry":
                slc +=[0]
                slc +=[slice(None)]
                print slc
                nc[name+i][:] = f[name][slc]
            else:
                slc +=[-1]
                slc +=[slice(None)]
                print slc
                nc[name+i][:] = f[name][slc]

nc.close()
f.close()
exit()
