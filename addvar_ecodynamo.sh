#!/bin/bash
set -x
WORK=/global/work/mitya
pathRun=${WORK}/run/Arctic-20km
filein=test.nc
nobioin=test_noextra.nc
if [ -f $nobioin ]; then
    cp -f $filein $nobioin
else
    cp $filein $nobioin
fi

OLDIFS=$IFS
IFS=','
for i in NH4,0.0 PO4,0.1  ; do
    set $i
    echo $1 and $2
    ncap -O -h -s "$1=(salt*0.+$2)" ${pathRun}/$nobioin -o ${pathRun}/$nobioin 
done

for i in SdetritusN,0.18 SdetritusP,0.024  ; do
    set $i
    echo $1 and $2
    ncap -O -h -s "$1=(detritus*$2)" ${pathRun}/$nobioin -o ${pathRun}/$nobioin 
done

for i in Phy_N,0.18 Phy_P,0.024  ; do
    set $i
    echo $1 and $2
    ncap -O -h -s "$1=(phytoplankton*$2)" ${pathRun}/$nobioin -o ${pathRun}/$nobioin 
done

for i in zooplanktonN,0.18 zooplanktonP,0.024  ; do
    set $i
    echo $1 and $2
    ncap -O -h -s "$1=(zooplankton*$2)" ${pathRun}/$nobioin -o ${pathRun}/$nobioin 
done


IFS=$OLDIFS
#
	
#done
mv ${pathRun}/$nobioin ${pathRun}/test_with_extra.nc

set +x
exit

