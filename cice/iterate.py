#!/usr/bin/env python 
# -*- coding: utf-8 -*-
##!/home/ntnu/mitya/virt_env/virt1/bin/python -B
## Dmitry Shcherbin, 2019.12.04
##with navigation buttons, 2015.01.05
import numpy as np
from numpy import *
import sys, re, glob, os
import numpy.ma as ma
import argparse
import os.path


file_list_1 = sorted(glob.glob("/cluster/shared/arcticfjord/output_data/s800/S800_fixed_2014/iceh.*.nc"))


if len(file_list_1)==0:
    print "list is empty"
    exit()
else:
    pass


for i in file_list_1:
    print i, i.split('.')[-2].replace('-',''), i.split('.')[-2].split('-')[0] 
    date=i.split('.')[-2].replace('-','')
    string_1="python ciceframes_save.py  -o fhocn -v fhocn_ai_d -i "+i+" -of vvel_"+date
    print string_1
    os.system(string_1)
   # string_1="python ciceframes_simple.py --var_min -0.3 --var_max 0.3 --xzoom 20:60 --yzoom 95:100 -o gap -v vvel_d -i "+i+" -of vvel_"+date
    #print string_1
    #os.system(string_1)
    #os.system("convert "+"gap/hi_"+date+"_00.png"+" gap/vvel_"+date+"_00.png" +" +append "+ "gap/joined_"+date+".png" )

