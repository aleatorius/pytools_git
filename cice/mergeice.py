#!/usr/bin/env python 
# -*- coding: utf-8 -*-
##!/home/ntnu/mitya/virt_env/virt1/bin/python -B
## Dmitry Shcherbin, 2019.12.04
##with navigation buttons, 2015.01.05
import numpy as np
from numpy import *
import sys, re, glob, os
import numpy.ma as ma
import argparse
import os.path


file_list_1 = sorted(glob.glob("/cluster/shared/arcticfjord/output_data/s800/S800_fromini_2014/iceh.*-*-*.nc"))


if len(file_list_1)==0:
    print "list is empty"
    exit()
else:
    pass

files=[]
counter=0
for i in file_list_1:
#    if counter==100:
#        print files
#        exit()
#    else:
#        pass
    print i, i.split('.')[-2].replace('-',''), i.split('.')[-2].split('-')[0] 
    date=i.split('.')[-2].replace('-','')
    string_1="python ciceframes_simple.py -o ice -v aice_d -i "+i+" -of ciceout_"+date
    print string_1
    os.system(string_1)
    file=glob.glob("/cluster/shared/arcticfjord/cmems_"+i.split('.')[-2].split('-')[0]+"/S800/"+"*"+i.split('.')[-2].replace('-','')+"*" ) 
    if len(file)==0:
        print "oopsydoopsy"
        if len(files)==0:
            pass
        else:
            string = "cp "+files[-1]+" "+"ice/cmemsout_"+date+"_00.png"
            print string
            os.system("cp "+files[-1]+" "+"ice/cmemsout_"+date+"_00.png")
    else:
        if len(file)==1:
            files.append("ice/cmemsout_"+date+"_00.png")
            string_2="python cmemsframes_simple.py -o ice -v ice_concentration -i " + file[0]+ " -of cmemsout_"+date
            print string_2
            os.system(string_2)
            print glob.glob(files[-1])
    
        else:
            print "too many files"
    os.system("convert "+"ice/cmemsout_"+date+"_00.png"+" ice/ciceout_"+date+"_00.png" +" +append "+ "ice/joined_"+date+".png" )

    counter=counter+1
print files
exit()

    
#    print file_list1[i+start1], file_list2[i+start2]
#    if i in range(0,10):
#        counter="000"+str(i)
#    elif i in range(10,100):
#        counter="00"+str(i)
#    elif i in range(100,1000):
#        counter="0"+str(i)
#    else:
#        counter=str(i)
#    os.system("convert "+file_list1[i+start1]+" "+file_list2[i+start2]+" +append "+ "a4_a20/salt_a4_a20_"+counter+".jpg" )
