#!/usr/bin/env python 
# -*- coding: utf-8 -*-
##!/home/ntnu/mitya/virt_env/virt1/bin/python -B
## Dmitry Shcherbin, 2019.12.04
##with navigation buttons, 2015.01.05
import numpy as np
from numpy import *
import sys, re, glob, os
import numpy.ma as ma
import argparse
import os.path
parser = argparse.ArgumentParser(description='frames generator, extracts snapshots from netcdf file')
parser.add_argument('-day', help='day', dest='day', action="store", default='01')
args = parser.parse_args()

file_list_1 = sorted(glob.glob("/cluster/shared/arcticfjord/s800_reduced/ocean_avg_2015-*-"+args.day+".nc"))


if len(file_list_1)==0:
    print "list is empty"
    exit()
else:
    pass


for i in file_list_1:
    print i, i.split('.')[-2].split("_")[-1]
    string=glob.glob("/cluster/shared/arcticfjord/a4_host_roms/*"+i.split('.')[-2].split("_")[-1] +"*.nc")
    print string
    print os.path.exists(string[0])
    date=i.split('.')[-2].replace('-','')
    string_1="python roms_nested.py --htitle A4 --gtitle S800 --xzoom 0:100 --yzoom 0:100 --var_max 36 --var_min 31 -o fixed_salt_temp -v salt -ig "+i+ " -ih "+string[0]
    print string_1
    os.system(string_1)
    string_1="python roms_nested.py --htitle A4 --gtitle S800 --xzoom 0:100 --yzoom 0:100 --var_max 10 --var_min -2 -o fixed_salt_temp -v temp -ig "+i+ " -ih "+string[0]
    print string_1
    os.system(string_1)
   # string_1="python ciceframes_simple.py --var_min -0.3 --var_max 0.3 --xzoom 20:60 --yzoom 95:100 -o gap -v vvel_d -i "+i+" -of vvel_"+date
    #print string_1
    #os.system(string_1)
    #os.system("convert "+"gap/hi_"+date+"_00.png"+" gap/vvel_"+date+"_00.png" +" +append "+ "gap/joined_"+date+".png" )

