#!/usr/bin/env python
##!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm
import numpy.ma as ma
import matplotlib.dates as mdates
parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-grid1', 
help='input file', 
dest='ingrid1', 
action="store",
default="/cluster/projects/nn9300k/metroms_input/cice.grid.nc"
)
parser.add_argument(
'-grid1_mask', 
help='input file', 
dest='ingrid1_mask', 
action="store",
default="/cluster/projects/nn9300k/metroms_input/cice.kmt.nc"
)

parser.add_argument(
'--correct', 
help='correct grid or not', 
dest='correct', 
action="store",
)
parser.add_argument(
'-grid2', 
help='input file', 
dest='ingrid2', 
action="store",
default="/cluster/projects/nn9300k/metroms_input/svalbard_800m_grid_uvpsi.nc"
)


args = parser.parse_args()

print args.correct
if args.correct:
    print "oh yes"
else:
    print "oh no!"
    exit()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa



f = Dataset(args.ingrid1)
h = Dataset(args.ingrid1_mask)
g = Dataset(args.ingrid2)

lat_rho = unpack(g.variables["lat_rho"])
lon_rho = unpack(g.variables["lon_rho"])
var = unpack(g.variables["h"])

tlat = unpack(f.variables["tlat"])
tlon = unpack(f.variables["tlon"])
mask_rho=unpack(g.variables["mask_rho"])
kmt=unpack(h.variables["kmt"])
g.close()
f.close()
h.close()

e_pos=[]
x_pos=[]
fails=[]
for i,j in np.ndenumerate(mask_rho):
    if  mask_rho[i]-kmt[i]!=0:
#        print i,mask_rho[i]-kmt[i], mask_rho[i],kmt[i]
        x_pos.append(i[0])
        e_pos.append(i[1])
        fails.append(i)
print len(x_pos)

if len(x_pos)!=0:
    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    palette = plt.cm.plasma
    p=ax.pcolormesh(var, cmap=palette)
    #p=ax.pcolormesh(ma.masked_outside(bath,-1e+36,1e+36), cmap=palette)
    #p=ax.pcolormesh(ma.masked_where(mask_rho[0]==0,bath), cmap=palette)
    plt.colorbar(p)
    ax.plot(e_pos,x_pos,'rs', markersize=1)
    plt.show()

if args.correct and len(fails)!=0:
    from os import system
    system("cp -f "+args.ingrid1_mask+" "+args.ingrid1_mask+"_corrected")
    h = Dataset(args.ingrid1_mask+"_corrected")
    for i in fails:
        print i, mask_rho[i], h.variables["kmt"][i]
        h.variables["kmt"][i[0],i[1]]=mask_rho[i]
    h.close()
            


exit()

