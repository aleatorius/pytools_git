#!/usr/bin/env python 
# -*- coding: utf-8 -*-
##!/home/ntnu/mitya/virt_env/virt1/bin/python -B
## Dmitry Shcherbin, 2019.12.04
##with navigation buttons, 2015.01.05
import numpy as np
from numpy import *
import sys, re, glob, os
import numpy.ma as ma
import argparse
import os.path


file_list_1 = sorted(glob.glob("/cluster/shared/arcticfjord/output_data/s800/S800_fixed_2014/iceh.2014-*-*.nc"))


if len(file_list_1)==0:
    print "list is empty"
    exit()
else:
    pass


for i in file_list_1:
    print i, i.split('.')[-2].replace('-',''), i.split('.')[-2].split('-')[0] 
    print i, i.split('.')[-2]
    string=glob.glob("/cluster/shared/arcticfjord/a4_host_cice_ja/iceh."+i.split('.')[-2]+"*.nc")
    print string
    print os.path.exists(string[0])
    date=i.split('.')[-2].replace('-','')
    string_1="python ciceframes_simple_a4_s800.py --xzoom 0:100 --yzoom 0:100 --var_max 2 -o fixed_hs -v hi_d -ig "+i+ " -ih "+string[0]+" --htitle A4 --gtitle S800 "
    print string_1
    os.system(string_1)
    string_1="python ciceframes_simple_a4_s800.py --xzoom 0:100 --yzoom 0:100 --var_max 0.5 -o fixed_hs -v hs_d -ig "+i+ " -ih "+string[0]+" --htitle A4 --gtitle S800 "
    print string_1
    os.system(string_1)
   # string_1="python ciceframes_simple.py --var_min -0.3 --var_max 0.3 --xzoom 20:60 --yzoom 95:100 -o gap -v vvel_d -i "+i+" -of vvel_"+date
    #print string_1
    #os.system(string_1)
    #os.system("convert "+"gap/hi_"+date+"_00.png"+" gap/vvel_"+date+"_00.png" +" +append "+ "gap/joined_"+date+".png" )

