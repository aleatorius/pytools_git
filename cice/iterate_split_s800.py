import numpy as np
from numpy import *
import sys, re, glob, os
import numpy.ma as ma
import argparse
import os.path
import datetime  
from datetime import date, time
from netCDF4 import *
file_list_1 = sorted(glob.glob("/cluster/shared/arcticfjord/output_data/s800/S800_fixed_2014/ocean_avg_*.nc"))

def date_time_list(file):
    ref = date(1948,01,01)
    ref_time = time(0,0,0)   
    refer= datetime.datetime.combine(ref, ref_time)   
    f = Dataset(file)    
    times=f.variables["ocean_time"][:]
    f.close()
    output=[]
    
    for i in times:
        output.append((refer+datetime.timedelta(float(i)/(3600*24))).strftime("%Y-%m-%d"))

    return output
    

if len(file_list_1)==0:
    print "list is empty"
    exit()
else:
    pass


for i in file_list_1:
    count = 0
    for day in  date_time_list(i):
        print day
        string = "ncks -O -C -v salt,temp,ocean_time -d s_rho,34 -d ocean_time,"+str(count)+" "+i+" /cluster/work/users/mitya/s800_reduced/ocean_avg_"+day+".nc"
        print string
        count=count+1
        os.system(string)
#    string = os.system("~/pytools_git/ncdate_list "+i)
#    print string



exit() 
#    string=glob.glob("/cluster/home/mitya/a4_host/iceh."+i.split('.')[-2]+"*.nc")#
#    print string
#    print os.path.exists(string[0])
#    date=i.split('.')[-2].replace('-','')
#    string_1="python ciceframes_simple_a4_s800.py --xzoom 0:100 --yzoom 0:100 --var_max 0.5 -o fixed_hs -v hs_d -ig "+i+ " -ih "+string[0]
#    print string_1
#    os.system(string_1)
   # string_1="python ciceframes_simple.py --var_min -0.3 --var_max 0.3 --xzoom 20:60 --yzoom 95:100 -o gap -v vvel_d -i "+i+" -of vvel_"+date
    #print string_1
    #os.system(string_1)
    #os.system("convert "+"gap/hi_"+date+"_00.png"+" gap/vvel_"+date+"_00.png" +" +append "+ "gap/joined_"+date+".png" )

