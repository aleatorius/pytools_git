#!/bin/bash
#SBATCH  --qos=preproc
##SBATCH --partition=normal
#SBATCH --time=00-00:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-31
                                                                                                                     
path2Fimex=/nird/home/${USER}/bin/fimex

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1

source /cluster/home/mitya/pytools_git/cice/modules_fram_python.sh

set -x


outdir=ja_frames_fhocn_01
if [ ! -d "$outdir" ]; then
    mkdir -p $outdir
fi

if (( $SLURM_ARRAY_TASK_ID < 10 )); then
    echo $SLURM_ARRAY_TASK_ID
    echo "less"
    files=/cluster/shared/arcticfjord/output_data/s800/S800_fixed_2014/iceh.*-*-0$SLURM_ARRAY_TASK_ID*.nc
else
    files=/cluster/shared/arcticfjord/output_data/s800/S800_fixed_2014/iceh.*-*-$SLURM_ARRAY_TASK_ID*.nc
fi
for i in $files
do 
    if  [ -f "$i" ]; then
	python /cluster/home/mitya/pytools_git/cice/ciceframes_save.py -i $i -v fhocn_ai_d -o $outdir --var_max 50 --var_min -300
    fi
    
done



exit 0


