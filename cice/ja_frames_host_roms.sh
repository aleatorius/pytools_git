#!/bin/bash
#SBATCH  --qos=preproc
##SBATCH --partition=normal
#SBATCH --time=00-00:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-31
                                                                                                                     

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1

source /cluster/home/mitya/pytools_git/cice/modules_fram_python.sh

set -x


outdir=ja_frames_fhocn_01
if [ ! -d "$outdir" ]; then
    mkdir -p $outdir
fi

if (( $SLURM_ARRAY_TASK_ID < 10 )); then
    echo $SLURM_ARRAY_TASK_ID
    echo "less"
    padding=0$SLURM_ARRAY_TASK_ID
else
    padding=$SLURM_ARRAY_TASK_ID
fi
python /cluster/home/mitya/pytools_git/cice/iterate_host_roms_salt_temp.py -day $padding

    




exit 0


