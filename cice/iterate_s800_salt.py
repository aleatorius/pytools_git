#!/usr/bin/env python 
# -*- coding: utf-8 -*-
##!/home/ntnu/mitya/virt_env/virt1/bin/python -B
## Dmitry Shcherbin, 2019.12.04
##with navigation buttons, 2015.01.05
import numpy as np
from numpy import *
import sys, re, glob, os
import numpy.ma as ma
import argparse
import os.path


file_list_1 = sorted(glob.glob("/cluster/shared/arcticfjord/output_data/s800/S800_fixed_2014/ocean_avg_*.nc"))


if len(file_list_1)==0:
    print "list is empty"
    exit()
else:
    pass


for i in file_list_1:
    string_1="python ~/pytools_git/pytrans_read_save_s800.py -is ~/pytools_git/segments --xzoom 0:100 --yzoom 0:100 --depth_max 800 --var_min -2 --var_max 8 -v temp -i "+i
    print string_1
#    os.system(string_1)
   # string_1="python ciceframes_simple.py --var_min -0.3 --var_max 0.3 --xzoom 20:60 --yzoom 95:100 -o gap -v vvel_d -i "+i+" -of vvel_"+date
    #print string_1
    os.system(string_1)
    #os.system("convert "+"gap/hi_"+date+"_00.png"+" gap/vvel_"+date+"_00.png" +" +append "+ "gap/joined_"+date+".png" )

