#!/bin/bash
#SBATCH  --qos=preproc
##SBATCH --partition=normal
#SBATCH --time=00-00:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-31
                                                                                                                     
path2Fimex=/nird/home/${USER}/bin/fimex

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1

source /cluster/home/mitya/pytools_git/cice/modules_fram_python.sh

set -x




if (( $SLURM_ARRAY_TASK_ID < 10 )); then
    echo $SLURM_ARRAY_TASK_ID
    echo "less"
    day=0$SLURM_ARRAY_TASK_ID
else
    day=$SLURM_ARRAY_TASK_ID
fi


python iterate_host_fhocn_day.py --day $day


exit 0


