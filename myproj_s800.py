#!/home/mitya/testenv/bin/python -B                                                                                                           
'''myproj.py x,y, d(i)'''
import numpy as np
from numpy import *
import pyproj
from pyproj import Proj
import sys, re, glob
from os import system
a = sys.argv
if len(a) != 4: print 'not a correct input'; print __doc__; sys.exit()
x = float(sys.argv[1])
y = float(sys.argv[2])
tr = sys.argv[3]
if str(tr) not in ('i','d'): print 'not a string i (for the inverse proj-transformation) or d (for a direct one)'; print __doc__; sys.exit()
try: float(x), float(y)
except ValueError: print 'not a number'; print __doc__; sys.exit()
#try: float(y)
#except ValueError: print 'not a number'; print __doc__; sys.exit()

a = np.array([x,y])


p = Proj("+proj=stere +lon_0=12 +lat_0=79.5 +k=0.933013 +R=6370000 +x_0=681347 +y_0=859641 +no_defs")
def pro(ar,s):
    if str(s)=='d':
        return np.array(p(ar[0],ar[1]))
    if str(s) == 'i':
        ar = ar*3000.
        return np.array(p(ar[0],ar[1], inverse=True))
    else:
        print 'please specify d or i'
        return


print (pro(a, tr)[0], pro(a, tr)[1])
