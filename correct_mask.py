#!/usr/bin/env python
##!/home/mitya/testenv/bin/python -B
import os
import time as tm
import numpy as np
from numpy import *
from netCDF4 import Dataset
import argparse

parser = argparse.ArgumentParser(description='it corrects u,v,psi masks given correct rho mask')

parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)


args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa



f = Dataset(args.inf, "r+")
mask_rho = unpack(f.variables["mask_rho"])
mask_u = unpack(f.variables["mask_u"])
mask_v =unpack(f.variables["mask_v"])
mask_psi =unpack(f.variables["mask_psi"])
bath=unpack(f.variables["h"])
dump=[]

print mask_rho.shape, mask_u.shape, mask_v.shape,mask_psi.shape, "rho-,u-,v-,psi-mask shapes"
for i in range(0,mask_u.shape[0]):
    for j in range(0,mask_u.shape[1]):
        if mask_u[i,j]-mask_rho[i,j]*mask_rho[i,j+1]!=0.0:
            print mask_u[i,j]-mask_rho[i,j]*mask_rho[i,j+1],i,j,"u", bath[i,j]
            f.variables["mask_u"][i,j]=mask_rho[i,j]*mask_rho[i,j+1]
            dump.append(i)
for i in range(0,mask_v.shape[0]):
    for j in range(0,mask_v.shape[1]):
        if mask_v[i,j]-mask_rho[i,j]*mask_rho[i+1,j]!=0.0:
            print mask_v[i,j]-mask_rho[i,j]*mask_rho[i+1,j],i,j,"v", bath[i,j]
            f.variables["mask_v"][i,j]=mask_rho[i,j]*mask_rho[i+1,j]
            dump.append(i)
for i in range(0,mask_psi.shape[0]):
    for j in range(0,mask_psi.shape[1]):
        if mask_psi[i,j]-mask_rho[i,j]*mask_rho[i+1,j]*mask_rho[i+1,j+1]*mask_rho[i,j+1]!=0.0:
            print mask_psi[i,j]-mask_rho[i,j]*mask_rho[i+1,j]*mask_rho[i+1,j+1]*mask_rho[i,j+1],i,j,"psi", bath[i,j]
            f.variables["mask_psi"][i,j]=mask_rho[i,j]*mask_rho[i+1,j]*mask_rho[i+1,j+1]*mask_rho[i,j+1]
            dump.append(i)
print "there is ",len(dump)," cells corrected"
#string=str(f.getncattr("history"))
#print string
#string=string+'\n Modified by '+str(os.path.basename(__file__))+' '+str(tm.strftime("%c"))
#print string
#f.setncattr("history",string)
f.close()
