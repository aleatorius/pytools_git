#!/bin/bash
#SBATCH  --qos=preproc
##SBATCH --partition=normal
#SBATCH --time=00-00:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-9
                                                                                                                     


datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1

source /cluster/home/mitya/pytools_git/cice/modules_fram_python.sh

set -x


outdir=ja_frames
if [ ! -d "$outdir" ]; then
    mkdir -p $outdir
fi
files=/cluster/shared/arcticfjord/output_data/s800/S800_fixed_2014/ocean_avg_05${SLURM_ARRAY_TASK_ID}*.nc

for i in $files
do 
    if  [ -f "$i" ]; then
	python /cluster/home/mitya/pytools_git/pytrans_read_save_s800.py -i $i -is ~/pytools_git/segments_svalbard -v temp --depth_max 300 --var_max 8 --var_min -2 -o $outdir
	python /cluster/home/mitya/pytools_git/pytrans_read_save_s800.py -i $i -is ~/pytools_git/segments_svalbard -v salt --depth_max 300 --var_max 35 --var_min 32 -o $outdir
    fi
    
done



exit 0


