#!/bin/bash

####roms_s800 with 3000m resolyutuion wrf with rotated to true north_east vectors to 800m resolution, requirs template.nc with coordinates

#module load intel/13.0 
#module load netCDF/4.2.1.1-intel-13.0  
#module load Boost/1.52.0-intel-13.0-OpenMPI-1.6.2
source ~/pytools_git/fimex_wtf/modules_fram_fimex.sh
#cd /global/work/mitya/metno/ERA-Interim_convert
set -x
#if [ -z "$1" ]
#  then
#    echo "No argument supplied"
#    exit
#fi

grid=/cluster/shared/arcticfjord/input_data/grids/cice.grid_s4k.nc_cor_eta_u
infile=/cluster/shared/arcticfjord/input_data/s2k/iced.2014-05-10-00000.nc
echo $infile
infilestrip=$(basename -- "$infile")
homedir=/cluser/home/mitya
pathTools=/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
outdir=/cluster/work/users/mitya/

interpolation=coord_nearestneighbor




tempfile=$outdir${infilestrip%.*}_t.nc

rm -f $tempfile 

cp -f $infile $tempfile
ncrename  --bfr_sz=4194304 -d nj,eta_t -d ni,xi_t $tempfile
ncks --bfr_sz=4194304 -A -v eta_t,xi_t $grid $tempfile
ncks  --bfr_sz=4194304 -A -v projection_stere /cluster/shared/arcticfjord/input_data/grids/cice.kmt_s2k.nc $tempfile
ncatted  --bfr_sz=4194304 -a grid_mapping,,a,c,"projection_stere" $tempfile
ncatted  --bfr_sz=4194304 -a standard_name,xi_t,o,c,"projection_x_coordinate" $tempfile 
ncatted  --bfr_sz=4194304 -a units,xi_t,o,c,"m" $tempfile 
ncatted  --bfr_sz=4194304 -a standard_name,eta_t,o,c,"projection_y_coordinate" $tempfile 
ncatted  --bfr_sz=4194304 -a units,eta_t,o,c,"m" $tempfile 
echo $tempfile


fimex --input.file ${tempfile} --input.printCS


xvals="2012000,2014000,...,4790000" 
yvals="688000,690000,...,2486000"

outfile=${outdir}${infilestrip%.*}_s2k.nc
echo $outfile

fimex --input.file=${tempfile}  --interpolate.projString="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=$interpolation


#ncrename -v lon,tlon -v lat,tlat $outfile
ncrename  --bfr_sz=4194304 -d eta_t,nj -d xi_t,ni $outfile
#rm -f $tempfile 

#cp -f $outfile cice.grid.nc
