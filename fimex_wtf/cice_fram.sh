#!/bin/bash

####roms_s800 with 3000m resolyutuion wrf with rotated to true north_east vectors to 800m resolution, requirs template.nc with coordinates


#source /cluster/home/$USER/pytools_git/fimex_wtf/modules_fram_fimex.sh

set -x
if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi

infile=$1
echo $infile
infilestrip=$(basename -- "$infile")
homedir=/cluster/home/mitya
pathTools=/cluster/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
outdir=$PWD/



xvals="0,800,...,1440800"
yvals="0,800,...,1080800"


outfile=${infile%.*}_800_out.nc
echo $outfile
    fimex --input.file=${infile}  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=bilinear  
