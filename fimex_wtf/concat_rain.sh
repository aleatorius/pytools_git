#!/bin/bash

string=""

for i in {0..25}; do (( i= $i * 113 )); string="$string wrf_*prec_2007_${i}_*nc" ; done

echo $string

ncrcat --bfr_sz=419430 $string wrf_3km_svalbard_prec_2007_800.nc
