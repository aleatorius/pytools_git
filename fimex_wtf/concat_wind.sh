#!/bin/bash

string=""

for i in {0..25}; do (( i= $i * 113 )); string="$string  wrf_3km_svalbard_2007_${i}_*_800.nc" ; done

echo $string

ncrcat --bfr_sz=419430 $string wrf_3km_svalbard_2007_800.nc
