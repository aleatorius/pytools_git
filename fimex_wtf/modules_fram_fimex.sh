#!/bin/bash

# Stallo


module purge
module load StdEnv
#module load intel/2018a
#module load OpenMPI/2.1.2-iccifort-2018.1.163-GCC-6.4.0-2.28
#module load iomkl/2018a
#module load intel/2018a
#module load netCDF-Fortran/4.4.4-intel-2018a-HDF5-1.8.19
#export I_MPI_F90=ifort
module load netCDF/4.4.1.1-intel-2017a-HDF5-1.8.18
module load Boost/1.63.0-intel-2017a-Python-2.7.13
module load PROJ/4.9.3-intel-2017a
module load UDUNITS/2.2.24-intel-2017a
module load NCO/4.6.6-intel-2017a

#module load netCDF/4.4.1.1-iomkl-2018a-HDF5-1.8.19 
#module load netCDF-Fortran/4.4.4-iomkl-2018a-HDF5-1.8.19
#module load netCDF/4.4.1.1-intel-2018a-HDF5-1.8.19
#module load netCDF-Fortran/4.4.4-intel-2018a-HDF5-1.8.19
#module load HDF5/1.8.19-intel-2018a
#module load imkl/2018.1.163-iimpi-2018a
#module load GDAL/2.2.3-intel-2018a-Python-2.7.14-HDF5-1.8.19
#module load Python/2.7.14-intel-2018a
#module load netcdf4-python/1.3.1-intel-2018a-Python-2.7.14
#module load Git/2.8.1

#module load openmpi/1.6.2
#module load hdf5/1.8.9
#module load netcdf/4.2.1.1
#module load python/2.7.3






#module load netcdf/4.1.3
#module load intelcomp mpt netcdf python/2.7.2



#module load mpt/2.10 intelcomp/15.0.1 netcdf/4.3.2 hdf5/1.8.14-mpi
#module load intelcomp/15.0.1
#module load netcdf/4.3.2
#module load hdf5/1.8.14-mpi
