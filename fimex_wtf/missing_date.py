#!/usr/bin/env python
##!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)



args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = np.zeros(ina.shape)
        outa[:] = ina[:]
    return outa


reg_int=0.125

f = Dataset(args.inf)
time = unpack(f.variables["time"])
print time.shape
f.close()
indexes=[]
fails=[]
for index,val in np.ndenumerate(time):
    indexes.append(index)
    if len(indexes)>1:
        if time[indexes[-1]]-time[indexes[-2]]!=reg_int:
            fails.append(int(indexes[-2][0]))
#vars=["rain"]
vars=["Tair", "Qair","Pair","Uwind","Vwind"]
print len(fails)

cuts=[]
interpolated=[]
if len(fails)==0:
    exit()
else:
    pass

for i in fails:
    print time[i],i, (time[i+1]-time[i])/reg_int
    fileout=args.inf+"_"+str(i)
    print fileout
    cuts.append(fileout)

    system("ncks --bfr_sz=4194304 -O -d time,"+str(int(i))+","+str(int(i)+1)+" "+args.inf + " "+fileout)
    file=[]
    for j in range(1,int((time[i+1]-time[i])/reg_int)):
        print fileout+"_"+str(j)
        dtt=j*reg_int/(time[i+1]-time[i])
        print dtt
        string="ncap2 --bfr_sz=4194304 -O -s 'time(0)=time(0)+"+str(j*reg_int)+" ' "
        for v in vars:
            print v
            string=string+"-s '"+v+"(0,:,:)="+v+"(0,:,:)+("+v+"(1,:,:)-"+v+"(0,:,:))*"+str(dtt)+"' "
        print string
        string =string+fileout+" "+fileout+"_"+str(j)
        system(string)
        string = "ncap2 --bfr_sz=4194304 -O -s 'difftime=time(1)-time(0)' "
        for v in vars:
            print v,"sec"
            string=string+"-s 'diff"+v+"="+v+"(1,:,:)-"+v+"(0,:,:)' "
        string = string + fileout+"_"+str(j)+" "+fileout+"_"+str(j)
        system(string)
        string = "ncks --bfr_sz=4194304  -O -d time,0,0 "+fileout+"_"+str(j)+" "+fileout+"_"+str(j)
        system(string)
        print string
        file.append(str(fileout+"_"+str(j)))
    string="ncrcat --bfr_sz=4194304 -h -O "
    for f in file:
        string=string+str(f)+" "

    string = string+fileout+"_inter"
    interpolated.append(fileout+"_inter")

    system(string)
    for f in file:
        system("rm -f "+f)

print cuts
for cut in cuts:
    system("rm -f "+cut)

print interpolated


#slicing in good periods
good_cuts=[]
for i in range(len(fails)+1):
    if i == 0:
        string = "ncks --bfr_sz=4194304 -O -d time,0,"+str(fails[i])+" "+args.inf+" "+args.inf+"_cut_"+str(i)
    elif i ==len(fails):
        string = "ncks --bfr_sz=4194304 -O -d time,"+str(fails[i-1]+1)+","+" "+args.inf+" "+args.inf+"_cut_"+str(i)
    else:
        string = "ncks  --bfr_sz=4194304 -O -d time,"+str(fails[i-1]+1)+","+str(fails[i])+" "+args.inf+" "+args.inf+"_cut_"+str(i)
    print string
    system(string)
    good_cuts.append(args.inf+"_cut_"+str(i))
            
print good_cuts

string = "ncrcat --bfr_sz=4194304 -h -O "
for i in range(0,len(good_cuts)-1):
    string = string + " "+good_cuts[i]+" "+ interpolated[i]

string = string + " "+good_cuts[-1]+" "+args.inf+"_withoutgaps"
print string
system(string)
for f in good_cuts:
    system("rm -f "+f)
for f in interpolated:
    system("rm -f "+f)
