#!/bin/bash

####roms_s800 with 3000m resolyutuion wrf with rotated to true north_east vectors to 800m resolution, requirs template.nc with coordinates

#module load intel/13.0 
#module load netCDF/4.2.1.1-intel-13.0  
#module load Boost/1.52.0-intel-13.0-OpenMPI-1.6.2
source ~/pytools_git/fimex_wtf/modules_fram_fimex.sh
#cd /global/work/mitya/metno/ERA-Interim_convert
set -x
if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi

infile=$1
echo $infile
infilestrip=$(basename -- "$infile")
homedir=/cluser/home/mitya
pathTools=/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
outdir=/cluster/work/users/mitya/



tempfile=$outdir${infilestrip%.*}_temp.nc
cp $infile $tempfile
echo $tempfile

ncks -A -v ni,nj,grid_mapping ~/pytools_git/fimex_wtf/template_a4.nc  ${tempfile} 
ncatted -a grid_mapping,,a,c,grid_mapping ${tempfile}



fimex --input.file ${tempfile} --input.printCS


xvals="2012000,2016000,...,4796000"
yvals="688000,692000,...,2504000"


outfile=${outdir}${infilestrip%.*}_svalbard.nc
echo $outfile

fimex --input.file=${tempfile}  --interpolate.projString="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=bilinear  
