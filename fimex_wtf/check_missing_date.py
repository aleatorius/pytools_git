#!/usr/bin/env python
##!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)



args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = np.zeros(ina.shape)
        outa[:] = ina[:]
    return outa


reg_int=0.125

f = Dataset(args.inf)
time = unpack(f.variables["time"])
print time.shape
f.close()
indexes=[]
fails=[]
for index,val in np.ndenumerate(time):
    indexes.append(index)
    if len(indexes)>1:
        if time[indexes[-1]]-time[indexes[-2]]!=reg_int:
            fails.append(int(indexes[-2][0]))
vars=["rain"]
#vars=["Tair", "Qair","Pair","Uwind","Vwind"]
print len(fails)

