#!/bin/bash

####roms_s800 with 3000m resolyutuion wrf with rotated to true north_east vectors to 800m resolution, requirs template.nc with coordinates

#module load intel/13.0 
#module load netCDF/4.2.1.1-intel-13.0  
#module load Boost/1.52.0-intel-13.0-OpenMPI-1.6.2
#module purge
#module load Boost/1.63.0-intel-2017a-Python-2.7.13
#module load netCDF/4.4.1.1-intel-2017a
#module load NCO/4.2.2
source $HOME/pytools_git/fimex_wtf/modules_fram_fimex.sh
#cd /global/work/mitya/metno/ERA-Interim_convert
set -x
if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi

infile=$1
echo $infile
infilestrip=$(basename -- "$infile")
homedir=$HOME
pathTools=~/bin
path2Fimex=${homedir}/bin/fimex
outdir=/cluster/work/users/mitya/
template=~/pytools_git/fimex_wtf/template.nc
echo $template

tempfile=$outdir${infilestrip%.*}_temp.nc
cp $infile $tempfile
echo $tempfile
ncks --bfr_sz=4194304 -A $template $tempfile
ncatted --bfr_sz=4194304 -a grid_mapping,,a,c,"projection_stere" $tempfile 
fimex --input.file ${tempfile} --input.printCS



#int grid_mapping ;
#grid_mapping:long_name = "grid mapping" ;
#grid_mapping:straight_vertical_longitude_from_pole = 100 ;
#grid_mapping:false_easting = 1174400 ;
#grid_mapping:standard_parallel = 60. ;
#grid_mapping:semi_major_axis = 6378137. ;
#grid_mapping:false_northing = 61600 ;
#grid_mapping:grid_mapping_name = "polar_stereographic" ;
#grid_mapping:inverse_flattening = 298.257223563 ;
#grid_mapping:latitude_of_projection_origin = 90. ;
#grid_mapping:ellipsoid = "WGS84" ;
#grid_mapping:dx = 160 ;
#grid_mapping:proj4string = "+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1174400 +y_0=61600 +lon_0=100" ;



#xvals="0,160,...,64160"
#yvals="0,160,...,96160"


xvals="0,800,...,1440800"
yvals="0,800,...,1080800"


outfile=${outdir}${infilestrip%.*}_800_out.nc
echo $outfile
#    ${path2Fimex} --input.file=${tempfile}  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1174400 +y_0=61600 +lon_0=100"  --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=bilinear  

    ${path2Fimex} --input.file=${tempfile}  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=bilinear  

#--output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml
