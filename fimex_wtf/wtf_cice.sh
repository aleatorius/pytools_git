#!/bin/bash

####roms_s800 with 3000m resolyutuion wrf with rotated to true north_east vectors to 800m resolution, requirs template.nc with coordinates

#module load intel/13.0 
#module load netCDF/4.2.1.1-intel-13.0  
#module load Boost/1.52.0-intel-13.0-OpenMPI-1.6.2
module purge
module load Boost/1.63.0-intel-2017a-Python-2.7.13
module load netCDF/4.4.1.1-intel-2017a
module load NCO/4.2.2
#cd /global/work/mitya/metno/ERA-Interim_convert
set -x
if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi

infile=$1
echo $infile
infilestrip=$(basename -- "$infile")
homedir=/home/mitya
pathTools=/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
outdir=/global/work/mitya/
template=/home/mitya/template.nc
echo $template

tempfile=$outdir${infilestrip%.*}_temp.nc
cp $infile $tempfile
echo $tempfile

START_TIME=$SECONDS
ncks -A $template $tempfile
ELAPSED_TIME=$(($SECONDS - $START_TIME))

echo $ELAPSED_TIME


ncatted -a grid_mapping,,a,c,"projection_stere" $tempfile 
fimex --input.file ${tempfile} --input.printCS


xvals="0,800,...,1440800"
yvals="0,800,...,1080800"


outfile=${outdir}${infilestrip%.*}_800_out.nc
echo $outfile
    fimex --input.file=${tempfile}  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=bilinear  --output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml


#rotate vector
ncrename -d y,eta_rho -d x,xi_rho -v x,xi_rho -v y,eta_rho $outfile

angle=/home/mitya/pytools_git/fimex_wtf/angle.nc
cp ${outfile} ${outfile}_unrotated
ncks -A $angle $outfile
ncap -O -s "Uwind_e=(Uwind*cos(angle)+Vwind*sin(angle));Vwind_e=(Vwind*cos(angle)-Uwind*sin(angle))" -v $outfile ${outfile}_wind
ncrename -v Uwind_e,Uwind -v Vwind_e,Vwind ${outfile}_wind
ncks -A -v Uwind,Vwind ${outfile}_wind ${outfile}
rm ${outfile}_wind