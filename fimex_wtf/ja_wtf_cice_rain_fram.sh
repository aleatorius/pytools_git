#!/bin/bash
#SBATCH --qos=preproc
##SBATCH --nodes=1 --ntasks-per-node=1 --qos=devel
##SBATCH --partition=normal
#SBATCH --time=00-00:59:00
#SBATCH --job-name=S800
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9081k ##nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-26
                                                                                                                     
path2Fimex=/cluster/home/${USER}/bin/fimex

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1

source /cluster/home/${USER}/pytools_git/fimex_wtf/modules_fram_fimex.sh

set -x

filein=/cluster/work/users/mitya/wtf/wrf_3km_svalbard_prec_2010.nc_withoutgaps

chunk=113

## chunk*number of array should give a number of records exactly

((range1=($SLURM_ARRAY_TASK_ID - 1) * $chunk))
((range2=$SLURM_ARRAY_TASK_ID  * $chunk - 1))
echo $range1
echo $range2

infilestrip=$(basename -- "$filein")
outfile=${infilestrip%.*}_${range1}_${range2}.nc


SCRATCH_DIRECTORY=/cluster/work/users/${USER}/job-array/${SLURM_JOBID}

mkdir -p ${SCRATCH_DIRECTORY}

cd ${SCRATCH_DIRECTORY}

ncks  --bfr_sz=4194304 -d time,$range1,$range2 $filein $outfile
$path2Fimex --input.file $outfile --input.printCS 

##lets process
cp /cluster/home/${USER}/pytools_git/fimex_wtf/template.nc ${SCRATCH_DIRECTORY}
template=template.nc

START_TIME=$SECONDS
ncks --bfr_sz=4194304 -A $template $outfile
ELAPSED_TIME=$(($SECONDS - $START_TIME))

ncatted --bfr_sz=4194304 -a grid_mapping,,a,c,"projection_stere" $outfile 
$path2Fimex --input.file ${outfile} --input.printCS

outdir=/cluster/work/users/${USER}/s800_processed/
xvals="0,800,...,1440800"
yvals="0,800,...,1080800"


#outfile=${outdir}${infilestrip%.*}_800_out.nc
outfile_new=${infilestrip%.*}_${range1}_${range2}_800.nc
#echo $outfile
if $path2Fimex --input.file=${outfile}  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile_new --interpolate.method=bilinear  ; then
    echo "success"
else
    echo "failure"
    mv $outfile ${outfile_new}_err
    outfile_new=${outfile_new}_err
fi

if [ ! -d "$outdir" ]; then
    mkdir -p $outdir
fi
mv ${outfile_new}  ${outdir}.

#cd ${SLURM_SUBMIT_DIR}
#rm -rf ${SCRATCH_DIRECTORY}

exit 0


