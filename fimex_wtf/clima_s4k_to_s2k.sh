#!/bin/bash

####roms_s800 with 3000m resolyutuion wrf with rotated to true north_east vectors to 800m resolution, requirs template.nc with coordinates

#module load intel/13.0 
#module load netCDF/4.2.1.1-intel-13.0  
#module load Boost/1.52.0-intel-13.0-OpenMPI-1.6.2
source ~/pytools_git/fimex_wtf/modules_fram_fimex.sh
#cd /global/work/mitya/metno/ERA-Interim_convert
set -x
#if [ -z "$1" ]
#  then
#    echo "No argument supplied"
#    exit
#fi

infile=/cluster/shared/arcticfjord/input_data/s2k/start_201405.nc
grid=/cluster/shared/arcticfjord/input_data/grids/arctic4km_grd_svalbard_withpsi.nc

echo $infile
infilestrip=$(basename -- "$infile")
homedir=/cluser/home/mitya
pathTools=/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
outdir=/cluster/work/users/mitya/
interpolation=coord_nearestneighbor

rho=1
v=1
u=1

if [ $rho -eq 1 ]; then

tempfile=$outdir${infilestrip%.*}_rho.nc

rm -f $tempfile 

ncks --bfr_sz=4194304 -A -v eta_rho,xi_rho,lat,lon,salt,temp,zeta,clim_time,projection_stere $infile $tempfile
#ncatted -a grid_mapping,,a,c,grid_mapping $tempfile
echo $tempfile

fimex --input.file ${tempfile} --input.printCS


#xvals="2012000,2014000,...,4792000" 
#yvals="688000,690000,...,2500000"
xvals="2012000,2014000,...,4790000" 
yvals="688000,690000,...,2486000"




outfile=${outdir}${infilestrip%.*}_s2k_rho.nc
echo $outfile

fimex --input.file=${tempfile}  --interpolate.projString="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=$interpolation


rm -f $tempfile 

ncrename  --bfr_sz=4194304 -v lat,lat_rho -v lon,lon_rho $outfile 
ncatted   --bfr_sz=4194304 -a coordinates,zeta,o,c,"lon_rho lat_rho" $outfile
ncatted   --bfr_sz=4194304 -a coordinates,salt,o,c,"lon_rho lat_rho" $outfile
ncatted   --bfr_sz=4194304 -a coordinates,temp,o,c,"lon_rho lat_rho" $outfile
ncatted   --bfr_sz=4194304 -a long_name,lat_rho,o,c,"latitude of RHO-points" $outfile 
ncatted   --bfr_sz=4194304 -a long_name,lon_rho,o,c,"longitude of RHO-points" $outfile 

#cp -f $outfile grid.nc

fi


if [ $u -eq 1 ]; then

tempfile=$outdir${infilestrip%.*}_u.nc

rm -f $tempfile 

ncks --bfr_sz=4194304 -A -v u,ubar,projection_stere $infile $tempfile
ncks  --bfr_sz=4194304 -A -v xi_u,eta_u $grid $tempfile
#ncatted -a grid_mapping,,a,c,grid_mapping $tempfile
echo $tempfile

fimex --input.file ${tempfile} --input.printCS


xvals="2013000,2015000,...,4789000" 
yvals="688000,690000,...,2486000"



outfile=${outdir}${infilestrip%.*}_s2k_u.nc
echo $outfile

fimex --input.file=${tempfile}  --interpolate.projString="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=$interpolation

rm -f $tempfile 
ncrename  --bfr_sz=4194304 -v lat,lat_u -v lon,lon_u $outfile
ncatted   --bfr_sz=4194304 -a coordinates,u,o,c,"lon_u lat_u" $outfile 
ncatted   --bfr_sz=4194304 -a coordinates,ubar,o,c,"lon_u lat_u" $outfile 
ncatted   --bfr_sz=4194304 -a long_name,lat_u,o,c,"latitude of U-points" $outfile 
ncatted   --bfr_sz=4194304 -a long_name,lon_u,o,c,"longitude of U-points" $outfile 




#ncks -A $outfile  grid.nc
fi



if [ $v -eq 1 ]; then
tempfile=$outdir${infilestrip%.*}_v.nc

rm -f $tempfile 

ncks --bfr_sz=4194304 -A -v v,vbar,projection_stere $infile $tempfile
ncks -A -v xi_v,eta_v $grid $tempfile
#ncatted -a grid_mapping,,a,c,grid_mapping $tempfile
echo $tempfile

fimex --input.file ${tempfile} --input.printCS


xvals="2012000,2014000,...,4790000" 
yvals="689000,691000,...,2485000"


outfile=${outdir}${infilestrip%.*}_s2k_v.nc
echo $outfile

fimex --input.file=${tempfile}  --interpolate.projString="+proj=stere +R=6371000.0 +lat_0=90 +lat_ts=60.0 +x_0=4180000.0 +y_0=2570000.0 +lon_0=58.0" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=$interpolation


rm -f $tempfile 


ncrename  --bfr_sz=4194304 -v lat,lat_v -v lon,lon_v $outfile
ncatted   --bfr_sz=4194304 -a coordinates,v,o,c,"lon_v lat_v" $outfile 
ncatted   --bfr_sz=4194304 -a coordinates,vbar,o,c,"lon_v lat_v" $outfile 
ncatted   --bfr_sz=4194304 -a long_name,lat_v,o,c,"latitude of V-points" $outfile 
ncatted   --bfr_sz=4194304 -a long_name,lon_v,o,c,"longitude of V-points" $outfile 


#ncks -A $outfile  grid.nc


fi



