#!/bin/bash

####roms_s800 with 3000m resolyutuion wrf with rotated to true north_east vectors to 800m resolution, requirs template.nc with coordinates

set -x

source $HOME/pytools_git/fimex_wtf/modules_stallo.sh


#if [ -z "$1" ]
#  then
#    echo "No argument supplied"
#    exit
#fi
function process_file(){
infile=$1
echo $infile
infilestrip=$(basename -- "$infile")
homedir=/cluster/home/mitya
pathTools=/cluster/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
outdir=$PWD/

ncks -O -C -v fsens_ai_d,flat_ai_d,flwdn_d,aice_d,flwup_ai_d,fswabs_ai_d,fswthru_ai_d,fhocn_ai_d,time $infile ${infilestrip}
ncks -A -v ni,nj,grid_mapping ~/pytools_git/fimex_wtf/template_a4.nc  ${infilestrip} 
ncatted -a grid_mapping,,a,c,grid_mapping ${infilestrip}



xvals="0,800,...,1440800"
yvals="0,800,...,1080800"




outfile=/global/work/mitya/a4_host/${infilestrip%.*}_800_out.nc
echo $outfile
    fimex --input.file=${infilestrip}  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --interpolate.yAxisV=${yvals} --output.file=$outfile --interpolate.method=bilinear  
rm -f ${infilestrip}
}


months="03"


for mm in $months
do
    files=/global/work/apn/A4_modelruns/A4_nudging/cice/iceh.2015-$mm-*.nc
    for i in $files; do process_file $i; done
done

exit 1

days="22"
for mm in $months
do
    for day in $days
    do
    files=/global/work/apn/A4_modelruns/A4_nudging/cice/iceh.2015-$mm-${day}*nc
    for i in $files; do process_file $i; done
    done
done






