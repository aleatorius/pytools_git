#!/usr/bin/env python
##!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date

parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-inf', 
help='input file', 
dest='infile', 
action="store",
default="/cluster/projects/nn9300k/metroms_input/cice.grid.nc"
)



parser.add_argument('--index', nargs='+', type=int)
args = parser.parse_args()
my_tuple = tuple(args.index)





def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa


g = Dataset(args.infile)
lat_rho = unpack(g.variables["XLAT"])
lon_rho = unpack(g.variables["XLONG"])
print lon_rho[my_tuple],lat_rho[my_tuple] 
