#!/bin/bash   
set -x 
if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi


infile=$1
echo $infile
infilestrip=$(basename -- "$infile")


outdir=/cluster/work/users/mitya/
outfile=${outdir}${infilestrip%.*}_sanitized.nc
string=""
for i in {0..251}
do
    ncks -O -d Time,$i,$i $infile ${outfile}_$i
    string="$string ${outfile}_$i"
    ncap2 -O -s "var_tmp=aicen; where (aicen < 0.0001) var_tmp=0.0001; aicen=var_tmp; tmp_ice=vicen/aicen; tmp_snow=vsnon/aicen; tmp_vicen=vicen; tmp_vsnon=vsnon; where( tmp_ice == 0) tmp_vicen=0.0000001;  where (tmp_snow==0) tmp_vsnon=0.000000001; vicen=tmp_vicen; vsnon=tmp_vsnon; test_ice=vicen/aicen; test_snow=vsnon/aicen" ${outfile}_$i  ${outfile}_$i
    ncks -O -x -v tmp_vicen,tmp_vsnon,var_tmp,tmp_ice,tmp_snow ${outfile}_$i ${outfile}_$i 
done

ncrcat $string $outfile

set +x
