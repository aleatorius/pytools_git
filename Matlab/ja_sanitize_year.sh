#!/bin/bash
##SBATCH --nodes=1 --ntasks-per-node=1 --qos=devel
##SBATCH --partition=normal
#SBATCH --time=00-00:59:00
#SBATCH --job-name=array
##SBATCH --mem-per-cpu=1000M
#SBATCH --account=nn9300k
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR
#SBATCH --array=1-365
##SBATCH --qos=preproc                                                                                                         

datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>${PWD}/run.log_slurm_${datstamp}_${SLURM_ARRAY_TASK_ID} 2>&1


set -x 


year="2011"

infile=/cluster/work/users/mitya/topaz/a4_cice/${year}/processed_filtered.nc


if [ -f /cluster/work/users/${USER}/job_cice_bry/${year}/bry.nc_$(( SLURM_ARRAY_TASK_ID ))  ]; then
    echo "alredy done"
    exit 0
fi

infilestrip=$(basename -- "$infile")

SCRATCH_DIRECTORY=/cluster/work/users/${USER}/job_cice_bry/${year}     #${SLURM_JOBID}



if [ ! -d "${SCRATCH_DIRECTORY}" ]; then
    mkdir -p ${SCRATCH_DIRECTORY}
fi


SCRATCH_DIRECTORY=/cluster/work/users/${USER}/job_cice_bry/${year}/$(( SLURM_ARRAY_TASK_ID ))     #${SLURM_JOBID}

if [ ! -d "${SCRATCH_DIRECTORY}" ]; then
    mkdir -p ${SCRATCH_DIRECTORY}
fi

#outdir=/cluster/work/users/mitya/
#outfile=${outdir}${infilestrip%.*}_sanitized.nc

cd ${SCRATCH_DIRECTORY}
source $HOME/pytools_git/clima_from_topaz/modules_fram_fimex.sh
if [ ! -f "${SCRATCH_DIRECTORY}/slice.nc" ]; then
    ncks --bfr_sz=4194304 -d Time,$(( SLURM_ARRAY_TASK_ID - 1 )) $infile ${SCRATCH_DIRECTORY}/slice.nc 
fi


ncap2 -O -s "*var_tmp=aicen; where (aicen < 0.0001) var_tmp=0.0001; aicen=var_tmp; *tmp_ice=vicen/aicen; *tmp_snow=vsnon/aicen; *tmp_vicen=vicen; *tmp_vsnon=vsnon; where( tmp_ice == 0) tmp_vicen=0.0000001;  where (tmp_snow==0) tmp_vsnon=0.000000001; vicen=tmp_vicen; vsnon=tmp_vsnon; test_ice=vicen/aicen; test_snow=vsnon/aicen" slice.nc bry.nc  
#ncks -O -x -v tmp_vicen,tmp_vsnon,var_tmp,tmp_ice,tmp_snow slice.nc bry.nc
source ~/.bashrc




python ~/pytools_git/bry_from_topaz.py -i bry.nc
module purge
module load NCO/4.6.6-intel-2017a

ncrename -O -d nj,eta_t -d ni,xi_t -d Time,days bry.nc bry.nc
mv bry.nc_bry ../bry.nc_$(( SLURM_ARRAY_TASK_ID ))
echo "success"
#ncrcat $string $outfile


set +x
