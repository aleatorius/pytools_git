#!/bin/bash   
#SBATCH -J sanitize
#SBATCH --account nn9300k
#SBATCH --partition=bigmem
#SBATCH --nodes=1 --ntasks-per-node=1 --cpus-per-task=1
#SBATCH --mem-per-cpu=64G
#SBATCH --time=00-10:00:00
#SBATCH -o slurm.%j.out # STDOUT
#SBATCH -e slurm.%j.err # STDERR

##RUNDIR=/cluster/home/mitya/Matlab/matlabmfiles
module purge
module load NCO/4.6.6-intel-2017a
set -x 
datstamp=`date +%Y_%m_%d_%H_%M`
exec 1>log_${datstamp} 2>&1


infile=/cluster/shared/arcticfjord/Annettes_files/filtered/processed_2015_filtered.nc
echo $infile
infilestrip=$(basename -- "$infile")


outdir=/cluster/work/users/mitya/
outfile=${outdir}${infilestrip%.*}_sanitized.nc

for i in {0..251}
do
    ncks -O -d Time,$i,$i $infile ${outfile}_$i
    string="$string ${outfile}_$i"
    ncap2 -O -s "var_tmp=aicen; where (aicen < 0.0001) var_tmp=0.0001; aicen=var_tmp; tmp_ice=vicen/aicen; tmp_snow=vsnon/aicen; tmp_vicen=vicen; tmp_vsnon=vsnon; where( tmp_ice == 0) tmp_vicen=0.0000001;  where (tmp_snow==0) tmp_vsnon=0.000000001; vicen=tmp_vicen; vsnon=tmp_vsnon; test_ice=vicen/aicen; test_snow=vsnon/aicen" ${outfile}_$i  ${outfile}_$i
    ncks -O -x -v tmp_vicen,tmp_vsnon,var_tmp,tmp_ice,tmp_snow ${outfile}_$i ${outfile}_$i 
done

ncrcat $string $outfile


set +x
