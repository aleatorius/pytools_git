addpath('/cluster/home/mitya/Matlab/matlabmfiles/timefunctions/');
Year=2014;
Month=8;
Day=1;
NumberOfFiles=31;  % 1 August- 31 December
IceCategories = [0.0 0.64 1.39 2.47 4.57];
[aa,NumberOfIceClasses] = size(IceCategories);
IceLayers = 7;
SnowLayers = 1; 
DeltaT = 24;
FreezingPointOfSeawater = -1.86; %Arrigo et al. (1993) - Journal of Geophysical Research 98: 6929-6946
a = 0.407;
b=0.573; 
Smax = 3.2;
Prefix = '/cluster/shared/arcticfjord/Annettes_files/filtered/dump';
OutFile = '/cluster/shared/arcticfjord/Annettes_files/filtered/August2014_modified_02.nc';
AtmFile = '/cluster/shared/arcticfjord/tair_2014_filtered_800_out.nc'
Tair = ncread(AtmFile,'Tair');
Tair = Tair - 273.15; %Kelvin to Celsius
addpath('/cluster/home/mitya/Matlab/matlabmfiles/timefunctions/');

cont = 849; % 4 records per day in AN_2014_unlim.nc times 212 days (1 Jan 31 Jul) + the first record in August


for t = 1:NumberOfFiles
   t 
   if t == 1
      secs(t) = date2unixsecs(Year,Month,Day);
   else
      secs(t) = secs(t-1) + DeltaT * 3600;
   end
   [AYear, AMonth, ADay] = unixsecs2date(secs(t));
   YearStr = int2str(AYear);
   MonthStr = int2str(AMonth);
   if AMonth < 10
      MonthStr = strcat('0',MonthStr);
   end
   DayStr = int2str(ADay);
   if ADay < 10
      DayStr = strcat('0',DayStr);
   end   
   
   MyTair = Tair(:,:,cont:cont+3);
   DailyAirTemp = mean(MyTair,3); %Takes averages along the third dimension (time)
   cont = cont + 4;
   
   FileName = strcat(Prefix,YearStr,MonthStr,DayStr,'.nc')
   aice_d = ncread(FileName,'fice');
   max_aicen = max(max(aice_d))
   hi_d   = ncread(FileName,'hice'); % area normalized
   max_hi_d = max(max(hi_d))
   hsnow   = ncread(FileName,'hsnow'); % area normalized
   Tsfc_d   = 	min(DailyAirTemp,-0.00001); %I am assuming that Tscf = air temp because we have no TOPAZ output for Tsfc
   
   [m,n] = size(aice_d);
   

   %Tsfc_d   = ncread(FileName,'Tsfc_d');
   iage_d = ncread(FileName,'fy_age');  %This is just FYI age not ice agae in general
   for i = 1:m
       for j = 1:n
            for k = 1:NumberOfIceClasses
                if hi_d(i,j) < IceCategories(1)
                    Category(i,j) = 1;
                elseif hi_d(i,j) >= IceCategories(1) & hi_d(i,j) < IceCategories(2) 
                    Category(i,j) = 2;
                elseif hi_d(i,j) >= IceCategories(2) & hi_d(i,j) < IceCategories(3) 
                    Category(i,j) = 3;    
                elseif hi_d(i,j) >= IceCategories(3) & hi_d(i,j) < IceCategories(4) 
                    Category(i,j) = 4;   
                else Category(i,j) = 5;
                end    
            end
       end    
   end
   aicen_d = zeros(m,n,NumberOfIceClasses)+0.0001;
   vicen_d = zeros(m,n,NumberOfIceClasses);
   vsnon_d = zeros(m,n,NumberOfIceClasses);
   Tinz_d  = zeros(m,n,IceLayers,NumberOfIceClasses);
   Sinz_d  = zeros(m,n,IceLayers,NumberOfIceClasses);
   Tsnz_d  = zeros(m,n,SnowLayers,NumberOfIceClasses);
   alvl_d = zeros(m,n,NumberOfIceClasses);
   vlvl_d = zeros(m,n,NumberOfIceClasses);
   
  for i = 1:m
       for j = 1:n
          aicen_d(i,j,Category(i,j)) = aice_d(i,j); 
          %if aicen_d(i,j,Category(i,j)) > 10e-10
          if aicen_d(i,j,Category(i,j)) > 10e-3 & hi_d(i,j) > 0.01    
             vicen_d(i,j,Category(i,j)) = hi_d(i,j) * aice_d(i,j);
             vsnon_d(i,j,Category(i,j)) = hsnow(i,j) * aice_d(i,j);
             alvl_d(i,j,Category(i,j)) = 1.0;
             vlvl_d(i,j,Category(i,j)) = 1.0;
             Tsnz_d(i,j,1,Category(i,j)) = Tsfc_d(i,j) + (FreezingPointOfSeawater-Tsfc_d(i,j))/(hsnow(i,j)+hi_d(i,j)) * hsnow(i,j) * 0.5;    
             for k = 1:IceLayers
               Depth =  hsnow(i,j) + k * hi_d(i,j)/IceLayers - 0.5 * hi_d(i,j)/IceLayers;  
               Tinz_d(i,j,k,Category(i,j)) = Tsfc_d(i,j) + (FreezingPointOfSeawater-Tsfc_d(i,j))/(hsnow(i,j)+hi_d(i,j)) * Depth;
               z = (k-1/2) / IceLayers;
               if hi_d(i,j) > 1.5 %Assume MYI
                   Sinz_d(i,j,k,Category(i,j)) = 0.5*Smax*(1-cos(pi*z^(a/(z+b)))); 
               else %Assume FYI
                   Sinz_d(i,j,k,Category(i,j)) = 19.539*z^2-19.93*z+8.913;  
               end
            end  
          end
       end
   end    

   Max_aicen_d = max(max(max(aicen_d)))
   %[m,n,IceLayers,NumberOfIceClasses] = size(Tinz_d);
   [m,n,SnowLayers,NumberOfIceClasses] = size(Tsnz_d);
   'File creation'
   cd /cluster/shared/arcticfjord/Annettes_files/filtered;
   if t == 1   
      pwd
      nccreate(OutFile,'Domain_lines','Datatype','int16','Dimensions',{'ni',m},'Format','netcdf4');
      nccreate(OutFile,'Domain_columns','Datatype','int16','Dimensions',{'nj',n},'Format','netcdf4');
      nccreate(OutFile,'Ice_layers','Datatype','int16','Dimensions',{'nkice',IceLayers},'Format','netcdf4');
      nccreate(OutFile,'Snow_layers','Datatype','int16','Dimensions',{'nksnow',SnowLayers},'Format','netcdf4');
      nccreate(OutFile,'NCAT','Datatype','int16','Dimensions',{'ncat',NumberOfIceClasses},'Format','netcdf4');
      nccreate(OutFile,'Time','Datatype','int16','Dimensions',{'Time',NumberOfFiles},'Format','netcdf4');  
      nccreate(OutFile,'TLON','Datatype','single','Dimensions',{'ni',m,'nj',n},'Format','netcdf4'); 
      ncwrite(OutFile,'Domain_lines',[0:m-1]);
      ncwrite(OutFile,'Domain_columns',[0:n-1]);
      ncwrite(OutFile,'Ice_layers',[0:IceLayers-1]);
      ncwrite(OutFile,'NCAT',[0:NumberOfIceClasses-1]);
      ncwrite(OutFile,'Time',[0:NumberOfFiles-1]); 
      TLON = ncread(FileName,'lon');
      ncwrite(OutFile,'TLON',TLON); 
      nccreate(OutFile,'TLAT','Datatype','single','Dimensions',{'ni',m,'nj',n},'Format','netcdf4'); 
      TLAT = ncread(FileName,'lat');
      ncwrite(OutFile,'TLAT',TLAT);
      nccreate(OutFile,'ULON','Datatype','single','Dimensions',{'ni',m,'nj',n},'Format','netcdf4'); 
      ULON = ncread(FileName,'lon');
      ncwrite(OutFile,'ULON',ULON);
      nccreate(OutFile,'ULAT','Datatype','single','Dimensions',{'ni',m,'nj',n},'Format','netcdf4'); 
      ULAT = ncread(FileName,'lat');
      ncwrite(OutFile,'ULAT',ULAT);
      nccreate(OutFile,'Tsfc','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4');   
      nccreate(OutFile,'iage','Datatype','double','Dimensions',{'ni',m,'nj',n,'Time',NumberOfFiles},'Format','netcdf4');   
      nccreate(OutFile,'aicen','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4');
      nccreate(OutFile,'vicen','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4');
      nccreate(OutFile,'vsnon','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4');
      nccreate(OutFile,'Tinz','Datatype','double','Dimensions',{'ni',m,'nj',n,'nkice',IceLayers,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4')
      nccreate(OutFile,'Sinz','Datatype','double','Dimensions',{'ni',m,'nj',n,'nkice',IceLayers,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4');
      nccreate(OutFile,'Tsnz','Datatype','double','Dimensions',{'ni',m,'nj',n,'nksnow',SnowLayers,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4');
      nccreate(OutFile,'alvln','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4');   
      nccreate(OutFile,'vlvln','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4'); 
      nccreate(OutFile,'apondn','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4'); 
      nccreate(OutFile,'hpondn','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4'); 
      nccreate(OutFile,'ipondn','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4'); 
      nccreate(OutFile,'hbrine','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4'); 
      nccreate(OutFile,'fbrine','Datatype','double','Dimensions',{'ni',m,'nj',n,'ncat',NumberOfIceClasses,'Time',NumberOfFiles},'Format','netcdf4'); 
   end
   NumberOfIceClasses
   for types = 1:NumberOfIceClasses
      Tsfc(:,:,types) = Tsfc_d(:,:);
      alvln(:,:,types) = alvl_d(:,:,types);
      vlvln(:,:,types) = vlvl_d(:,:,types);
   end   
   iage(:,:) = iage_d(:,:,1);
   aicen(:,:,:) = aicen_d(:,:,:);
   vicen(:,:,:) = vicen_d(:,:,:);
   vsnon(:,:,:) = vsnon_d(:,:,:);
   Tinz(:,:,1:IceLayers,1:NumberOfIceClasses) = Tinz_d;
   Sinz(:,:,1:IceLayers,1:NumberOfIceClasses) = Sinz_d;
   Tsnz(:,:,1:SnowLayers,1:NumberOfIceClasses) = Tsnz_d; 
   apondn = zeros(m,n,NumberOfIceClasses);
   hpondn = zeros(m,n,NumberOfIceClasses);
   ipondn = zeros(m,n,NumberOfIceClasses);
   fbrine = zeros(m,n,NumberOfIceClasses);
   hbrine = zeros(m,n,NumberOfIceClasses);
     
   ncwrite(OutFile,'Tsfc',Tsfc,[1 1 1 t]); 
   ncwrite(OutFile,'iage',iage,[1 1 t]); 
   ncwrite(OutFile,'aicen',aicen,[1 1 1 t]);
   ncwrite(OutFile,'vicen',vicen,[1 1 1 t]);
   ncwrite(OutFile,'vsnon',vsnon,[1 1 1 t]);
   ncwrite(OutFile,'Tinz',Tinz,[1 1 1 1 t]); 
   ncwrite(OutFile,'Sinz',Sinz,[1 1 1 1 t]); 
   ncwrite(OutFile,'Tsnz',Tsnz,[1 1 1 1 t]);
   ncwrite(OutFile,'alvln',alvln,[1 1 1 t]); 
   ncwrite(OutFile,'vlvln',vlvln,[1 1 1 t]); 
   ncwrite(OutFile,'apondn',apondn,[1 1 1 t]); 
   ncwrite(OutFile,'hpondn',hpondn,[1 1 1 t]);
   ncwrite(OutFile,'ipondn',ipondn,[1 1 1 t]); 
   ncwrite(OutFile,'fbrine',fbrine,[1 1 1 t]); 
   ncwrite(OutFile,'hbrine',hbrine,[1 1 1 t]);
end %t
               
              
