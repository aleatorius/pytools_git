%  This function find the (x,y) coordinates of the model grid given
%  a (lon,lat) point.

global IPRINT
IPRINT=0;

Gname='/n2/arango/NJB/Jul99/Data/njb1_grid_a.nc';

lon(1)=-(74.0+12.7/60.0);      % N-line starting point
lat(1)=39.0+32.9/60.0;
lon(2)=-(74.0+ 2.0/60.0);      % N-line ending point
lat(2)=39.0+25.9/60.0;

lon(3)=-(74.0+14.7/60.0);      % A-line starting point
lat(3)=39.0+27.7/60.0;
lon(4)=-(74.0+ 5.2/60.0);      % A-line ending point
lat(4)=39.0+20.6/60.0;

lon(5)=-(74.0+21.0/60.0);      % S-line starting point
lat(5)=39.0+23.9/60.0;
lon(6)=-(74.0+10.4/60.0);      % Sline ending point
lat(6)=39.0+16.9/60.0;

%---------------------------------------------------------------------
%  Read in grid coordinates.
%---------------------------------------------------------------------

rlon=nc_read(Gname,'lon_rho'); rlon=rlon';
rlat=nc_read(Gname,'lat_rho'); rlat=rlat';

xr=nc_read(Gname,'x_rho');  xr=xr';
yr=nc_read(Gname,'y_rho');  yr=yr';

np=length(lon);

for n=1:np,
  [i,j]=ijgrid(lon(n),lat(n),rlon,rlat);
  I(n)=i;
  J(n)=j;
  x(n)=xr(j,i);
  y(n)=yr(j,i);
  disp(['Point ',num2str(n,'%2.2i'), ...
        ': I = ',num2str(i,'%3.3i'),', J = ',num2str(j,'%3.3i'), ...
        ', Lon = ',num2str(lon(n),'%8.4f'), ...
        ', Lat = ',num2str(lat(n),'%8.4f')]);
  disp(['                              x = ',num2str(x(n))]);
  disp(['                              y = ',num2str(y(n))]);
end,

