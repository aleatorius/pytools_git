%  Driver script for creating DX visulaization file.

CREATE=1;

Xname ='/n0/arango/NJB/Jul98/Run24/dx_his_24b.nc';
Hname1='/n0/arango/NJB/Jul98/Run24/njb1_his_24_01.nc';
Hname2='/n0/arango/NJB/Jul98/Run24/njb1_his_24_02.nc';
Gname ='/n0/arango/NJB/Jul98/njb1_grid_a.nc';

bounds.Istr=1;
bounds.Iend=50;
bounds.Jstr=50;
bounds.Jend=150;
bounds.Dstr=11008;
bounds.Dend=11017;

%bounds.Dstr=-Inf;
%bounds.Dend=Inf;

%-----------------------------------------------------------------------
%  Set swiches of variables to define:
%-----------------------------------------------------------------------

define.rgrid2=1;       % 2D grid positions at RHO-points.
define.rgrid3=1;       % 3D grid positions at RHO-points.
define.wgrid3=1;       % 3D grid positions at W-points.
define.h     =1;       % bathymetry.
define.rlon  =1;       % longitude at rho-points.
define.rlat  =1;       % latitude at rho-points.
define.mask  =1;       % land/sea at rho-points.
define.angle =1;       % rotation angle at rho-points.

define.zeta  =1;       % free-surface.
define.v2d   =1;       % 2D momentum.
define.v3d   =1;       % 3D momentum.
define.temp  =1;       % temperature.
define.salt  =1;       % salinity.
define.rho   =1;       % density anomaly.
define.AKv   =1;       % vertical viscosity.
define.AKt   =1;       % vertical diffusion of temperature.
define.AKs   =1;       % vertical diffusion of salinity.
define.Hsbl  =1;       % depth of surface boundary layer.

%-----------------------------------------------------------------------
%  Create DX visualization file.
%-----------------------------------------------------------------------

if (CREATE),
  [got,Vname,status]=c_ncdx(Xname,Hname1,Gname,define,bounds);
end,

%-----------------------------------------------------------------------
%  Write out visualization data.
%-----------------------------------------------------------------------

[status]=w_ncdx(Xname,Hname1,define,got,Vname,bounds);
[status]=w_ncdx(Xname,Hname2,define,got,Vname,bounds);
