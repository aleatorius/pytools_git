function [status]=w_ncdx(Xname,Hname,got,define,Vname,bounds);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% [status]=w_ncdx(Xname,Hname,got,define,Vname,bounds)                      %
%                                                                           %
% This function writes out data IBM DX NetCDF file.                         %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Xname       DX NetCDF file name to create (string).                    %
%    Hname       History NetCDF file name (string).                         %
%    got         Switches indicating defined variables.                     %
%    define      Switches indicating which variables to define              %
%                (0: no, 1: yes), structure array.                          %
%    Vname       Names of defined variables (structure array).              %
%                     **.zeta   => free-surface.                            %
%                     **.v2d    => 2D momentum.                             %
%                     **.v3d    => 3D momentum.                             %
%                     **.temp   => temperature.                             %
%                     **.salt   => salinity.                                %
%                     **.rho    => density anomaly.                         %
%                     **.AKv    => vertical viscosity.                      %
%                     **.AKt    => vertical diffusion of temperature.       %
%                     **.AKs    => vertical diffusion of salinity.          %
%                     **.Hsbl   => depth of surface boundary layer.         %
%    bounds      Grid bounds to process (structure array):                  %
%                  bounds.Istr  => starting I-index.                        %
%                  bounds.Iend  => ending I-index.                          %
%                  bounds.Jstr  => starting J-index.                        %
%                  bounds.Jend  => ending J-index.                          %
%                  bounds.Dstr  => starting day.                            %
%                  bounds.Dend  => ending day.                              %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    status      Error flag.                                                %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------
%  Check size of unlimited time dimension
%----------------------------------------------------------------------------

%  Number of records in input file.

[dnames,dsizes]=nc_dim(Hname);
ndims=length(dsizes);

for n=1:ndims,
  name=deblank(dnames(n,:));
  switch name
    case 'time'
      nrec=dsizes(n);
    case 'xi_rho'
      Lp=dsizes(n);
    case 'eta_rho'
      Mp=dsizes(n);
    case 's_rho'
      Nr=dsizes(n);
    case 's_w'
      Nw=dsizes(n);
    case 'tracer'
      NT=dsizes(n);
  end,
end,

L=Lp-1;
Lm=L-1;
M=Mp-1;
Mm=M-1;

%  Starting time index in output file.

[dnames,dsizes]=nc_dim(Xname);
ndims=length(dsizes);

for n=1:ndims,
  name=deblank(dnames(n,:));
  if (strcmp(name,'time')),
    Tstr=dsizes(n);
  end,
end,

%----------------------------------------------------------------------------
%  Set extraction I-, J-indices.
%----------------------------------------------------------------------------

if (nargin > 4),
 Isr=bounds.Istr+1;
 Ier=bounds.Iend+1;
 Jsr=bounds.Jstr+1;
 Jer=bounds.Jend+1;
else,
 Isr=2;
 Ier=L;
 Jsr=2;
 Jer=M;
end,

%----------------------------------------------------------------------------
%  Write out variables.
%----------------------------------------------------------------------------

Trec=Tstr;

for n=1:nrec,

% time=nc_read(Hname,'scrum_time',n);
  time=nc_read(Hname,'ocean_time',n);
  day=time/86400;

  if ( (day >= bounds.Dstr) & ( day <= bounds.Dend) ),
  
    Trec=Trec+1;

    gtime=gregorian(day+2440000);
    yy=gtime(1);
    MM=gtime(2);
    dd=gtime(3);
    hh=gtime(4);
    mm=gtime(5);
    ss=gtime(6);
    hour=hh+mm/60.0+ss/3600.0;

    disp(['Input record: ',num2str(n,'%3.3i'), ...
          ', Output record: ',num2str(Trec,'%3.3i'), ...
          ', yy:mm:dd:hh = ',num2str(yy),':', ...
          num2str(MM,'%2.2i'),':',num2str(dd,'%2.2i'),':', ...
          sprintf('%5.2f',hour)]);

% Time.

    [status]=nc_write(Xname,Vname.time,time,Trec);
    if (status ~= 0),
      disp(['W_NCDX - Error while writing: ',Vname.time]);
      return
    end,

% Free-surface.

    if (got.zeta & define.zeta),
      Finp=nc_read(Hname,Vname.zeta,n);
      Fout=Finp(Isr:Ier,Jsr:Jer);
      [status]=nc_write(Xname,Vname.zeta,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.zeta]);
        return
      end,
      clear Finp Fout
    end,

% 2D momentum.

    if (got.v2d & define.v2d),
      Finp=nc_read(Hname,Vname.ubar,n);
      u=0.5.*(Finp(1:Lm,1:Mp)+Finp(2:L,1:Mp));
      Finp=nc_read(Hname,Vname.vbar,n);
      v=0.5.*(Finp(1:Lp,1:Mm)+Finp(1:Lp,2:M));
      Fout(1,:,:)=u(Isr-1:Ier-1,Jsr:Jer);
      Fout(2,:,:)=v(Isr:Ier,Jsr-1:Jer-1);
      [status]=nc_write(Xname,Vname.v2d,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.v2d]);
        return
      end,
      clear Finp Fout u v 
    end,

% 3D momentum.

    if (got.v3d & define.v3d),
      Finp=nc_read(Hname,Vname.u,n);
      u=0.5.*(Finp(1:Lm,1:Mp,:)+Finp(2:L,1:Mp,:));
      Finp=nc_read(Hname,Vname.v,n);
      v=0.5.*(Finp(1:Lp,1:Mm,:)+Finp(1:Lp,2:M,:));
      if (Nr == Nw),
        w=nc_read(Hname,Vname.w,n);
        Finp=zeros([Lp Mp Nr+1]);
        Finp(:,:,2:Nr+1)=w;
        w=0.5.*(Finp(:,:,1:Nr)+Finp(:,:,2:Nr+1));
      else,
        Finp=nc_read(Hname,Vname.w,n);
        w=0.5.*(Finp(:,:,1:Nw-1)+Finp(:,:,2:Nw));
      end,     
      Fout(1,:,:,:)=u(Isr-1:Ier-1,Jsr:Jer,:);
      Fout(2,:,:,:)=v(Isr:Ier,Jsr-1:Jer-1,:);
      Fout(3,:,:,:)=w(Isr:Ier,Jsr:Jer,:);
      [status]=nc_write(Xname,Vname.v3d,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.v3d]);
        return
      end,
      clear Finp Fout u v w
    end,
 
% Temperature.

    if (got.temp & define.temp),
      Finp=nc_read(Hname,Vname.temp,n);
      Fout=Finp(Isr:Ier,Jsr:Jer,:);
      [status]=nc_write(Xname,Vname.temp,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.temp]);
        return
      end,
      clear Finp Fout
    end,

% Salinity.
 
    if (got.salt & define.salt),
      Finp=nc_read(Hname,Vname.salt,n);
      Fout=Finp(Isr:Ier,Jsr:Jer,:);
      [status]=nc_write(Xname,Vname.salt,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.salt]);
        return
      end,
      clear Finp Fout
    end,

% Density anomaly.

    if (got.rho & define.rho),
      Finp=nc_read(Hname,Vname.rho,n);
      Fout=Finp(Isr:Ier,Jsr:Jer,:);
      [status]=nc_write(Xname,Vname.rho,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.rho]);
        return
      end,
      clear Finp Fout
    end,

% Vertical viscosity.
 
    if (got.AKv & define.AKv),
      Finp=nc_read(Hname,Vname.AKv,n);
      Fout=Finp(Isr:Ier,Jsr:Jer,:);
      [status]=nc_write(Xname,Vname.AKv,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.AKv]);
        return
      end,
      clear Finp Fout
    end,

% Vertical diffusion of temperature.

    if (got.AKt & define.AKt),
      Finp=nc_read(Hname,Vname.AKt,n);
      Fout=Finp(Isr:Ier,Jsr:Jer,:);
      [status]=nc_write(Xname,Vname.AKt,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.AKt]);
        return
      end,
      clear Finp Fout
    end,

% Vertical diffusion of salinity.

    if (got.AKs & define.AKs),
      Finp=nc_read(Hname,Vname.AKs,n);
      Fout=Finp(Isr:Ier,Jsr:Jer,:);
      [status]=nc_write(Xname,Vname.AKs,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.AKs]);
        return
      end,
      clear Finp Fout
    end,

% Depth of surface boundary layer.

    if (got.Hsbl & define.Hsbl),
      Finp=nc_read(Hname,Vname.Hsbl,n);
      Fout=Finp(Isr:Ier,Jsr:Jer);
      [status]=nc_write(Xname,Vname.Hsbl,Fout,Trec);
      if (status ~= 0),
        disp(['W_NCDX - Error while writing: ',Vname.Hsbl]);
        return
      end,
      clear Finp Fout
    end,

  end,

end,

return
