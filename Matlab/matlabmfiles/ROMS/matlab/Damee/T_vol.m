%  This function computes volume integrated temperature and salinity
%  in the specified region.

global IPRINT


IPRINT=0;
IPLOT=1;
IPLOT2=0;
damee='g7_05';
interpolate=1;

Llon=-97;
Rlon=-10;
Blat=9;
Tlat=47;
delta=0.5;

RangeLab=[' Llon= ',num2str(Llon), ...
         ', Rlon= ',num2str(Rlon), ...
         ', Blat= ',num2str(Blat), ...
         ', Tlat= ',num2str(Tlat)];

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Tindex=1:1:61;
    mgrid='Damee #4: Run 04,';
  case 'g4_06'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 06,';
  case 'g4_07'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 07,';
  case 'g4_10.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 10.01,';
  case 'g4_13.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 13.01,';
  case 'g4_13.2'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 13.02,';
  case 'g4_14'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 14,';
  case 'g4_15'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 15,';
  case 'g4_16'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    Tindex=1:1:60;
    mgrid='Damee #4: Run 16,';
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 17,';
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 19_01,';
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 20,';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 22,';
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    Tindex=1:1:60;
    mgrid='Damee #4: Run 23,';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 24,';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    gname='/n0/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/coamps/grid4/junk1.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 26,';
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 27,';
  case 'g5_01'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/d15/arango/scrum/Damee/grid5/damee5_avg_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #5: Run 01,';
  case 'g6_01'
    gname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    fname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 01,';
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 05,';
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/coamps/grid6/damee6_avg_06.nc';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 06,';
  case 'g7_01'
    gname='/d15/arango/scrum/Damee/grid7/damee7_grid_a.nc';
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    Tindex=1:1:48;
    mgrid='Damee #7: Run 01,';
  case 'g7_05'
    gname='damee7_grid_b.nc';
    fname='/coamps/grid4/damee7_ts_05.nc';
    Tindex=1:1:120;
    mgrid='Damee #7: Run 05,';
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    Tindex=1:1:36;
    mgrid='Damee #9: Run 01,';
end,

%--------------------------------------------------------------------------
% Compute temperature and salinity volume integral.
%--------------------------------------------------------------------------

if (interpolate),
  [Tvol]=vol_int(gname,fname,'temp',Tindex,Llon,Rlon,Blat,Tlat,delta);
  [Svol]=vol_int(gname,fname,'salt',Tindex,Llon,Rlon,Blat,Tlat,delta);
else,
  [Tvol]=vol_int(gname,fname,'temp',Tindex);
  [Svol]=vol_int(gname,fname,'salt',Tindex);
end,

switch ( damee ),
  case 'g4_10.1'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_10a=Tvol; S4_10a=Svol;
  case 'g4_13.1'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_13a=Tvol; S4_13a=Svol;
  case 'g4_13.2'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_13b=Tvol; S4_13b=Svol;
  case 'g4_14'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_14=Tvol; S4_14=Svol;
  case 'g4_15'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_15=Tvol; S4_15=Svol;
  case 'g4_16'
    Tvol.time=Tvol.time./86400./360;
    Svol.time=Svol.time./86400./360;
    T4_16=Tvol; S4_16=Svol;
  case 'g4_17'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_17=Tvol; S4_17=Svol;
  case 'g4_19.1'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_19a=Tvol; S4_19a=Svol;
  case 'g4_22.1'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_22a=Tvol; S4_22a=Svol;
  case 'g4_23'
    Tvol.time=Tvol.time./86400./360;
    Svol.time=Svol.time./86400./360;
    T4_23=Tvol; S4_23=Svol;
  case 'g4_24'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_24=Tvol; S4_24=Svol;
  case 'g4_25'
    Tvol.time=(Tvol.time-Tvol.time(1))./86400./360;
    Svol.time=(Svol.time-Svol.time(1))./86400./360;
    T4_25=Tvol; S4_25=Svol;
  case 'g4_26'
    Tvol.time=Tvol.time./86400./360;
    Svol.time=Svol.time./86400./360;
    T4_26=Tvol; S4_26=Svol;
  case 'g6_05'
    Tvol.time=Tvol.time./86400./360;
    Svol.time=Svol.time./86400./360;
    T6_05=Tvol; S6_05=Svol;
  case 'g6_06'
    Tvol.time=Tvol.time./86400./360;
    Svol.time=Svol.time./86400./360;
    T6_06=Tvol; S6_05=Svol;
  case 'g7_05'
    Tvol.time=Tvol.time./86400./360;
    Svol.time=Svol.time./86400./360;
    T7_05=Tvol; S7_05=Svol;
end,

%--------------------------------------------------------------------------
%  Plot time evolution of volume temperature and salinity.
%--------------------------------------------------------------------------

if ( IPLOT ),

  figure;
  plot(Tvol.time,Tvol.field,'k');
  grid on;
  set(gca,'xlim',[-Inf Inf]);
  title(strcat(mgrid,'  Volume Averaged Temperature'));
  if (interpolate),
    xlabel({['time (years)'],[RangeLab]});
  else,
    xlabel('time (years)');
  end,
  ylabel('Temperature');
  print -dps Tvol.ps

  figure;
  plot(Svol.time,Svol.field,'k');
  grid on;
  set(gca,'xlim',[-Inf Inf]);
  title(strcat(mgrid,'  Volume Averaged Salinity'));
  if (interpolate),
    xlabel({['time (years)'],[RangeLab]});
  else,
    xlabel('time (years)');
  end,
  ylabel('Salinity');
  print -dps -append Tvol.ps

end,


if ( IPLOT2 ),

  figure;
  plot(T4_19a.time,T4_19a.field, ...
       T4_22a.time,T4_22a.field, ...
       T4_23.time,T4_23.field, ...
       T4_24.time,T4_24.field);
  legend ('r19','r22','r23','r24',2);
  grid on;
  set(gca,'xlim',[-Inf Inf]);
  title('Volume Averaged Temperature');
  if (interpolate),
    xlabel({['time (years)'],[RangeLab]});
  else,
    xlabel('time (years)');
  end,
  ylabel('Temperature');
  print -dpsc Tvol.ps

  figure;
  plot(S4_19a.time,S4_19a.field, ...
       S4_22a.time,S4_22a.field, ...
       S4_23.time,S4_23.field, ...
       S4_24.time,S4_24.field);
  legend ('r19','r22','r23','r24',2);
  grid on;
  set(gca,'xlim',[-Inf Inf]);
  title('Volume Averaged Salinity');
  xlabel({['time (years)'],[RangeLab]});
  ylabel('Salinity');
  print -dpsc -append Tvol.ps

end,
