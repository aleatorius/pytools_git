% This script builds  seasonal averages for the last three years of
% simulation and write it out into a NetCDF file.

global IPRINT

IPRINT=0;
damee='g4_17';

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e0/arango/Damee/grid4/damee4_yavg_04.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_06'
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e0/arango/Damee/grid4/damee4_yavg_06.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_07'
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e2/arango/Damee/grid4/damee4_yavg_07.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_10.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_10_01.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_13.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_13_01.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_13.2'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_13_02.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_14'
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_14.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_15'
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_15.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_16'
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_16.nc';
    feb=25:12:60;
    may=28:12:60;
    aug=31:12:60;
    nov=34:12:60;
  case 'g4_17'
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_17.nc';
    feb=73:12:104;
    may=76:12:104;
    aug=79:12:104;
    nov=70:12:104;
  case 'g4_19.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_19_01.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_20'
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_20.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_22.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_22_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_22_01.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g4_23'
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_23.nc';
    feb=25:12:60;
    may=28:12:60;
    aug=31:12:60;
    nov=34:12:60;
  case 'g4_24'
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_24.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g5_01'
    fname='/d15/arango/scrum/Damee/grid5/damee5_avg_01.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    oname='/e0/arango/Damee/grid5/damee5_yavg_01.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g6_01'
    fname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='/e0/arango/Damee/grid5/damee6_yavg_01.nc';
    feb=85:12:120;
    may=88:12:120;
    aug=91:12:120;
    nov=94:12:120;
  case 'g7_01'
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    oname='/e2/arango/Damee/grid7/damee7_yavg_01.nc';
    feb=13:12:48;
    may=16:12:48;
    aug=19:12:48;
    nov=12:12:48;
  case 'g7_04'
    fname='/comop/arango/Damee/grid7/damee7_avg_04_04.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    oname='/comop/arango/Damee/grid7/damee7_yavg_04_04.nc';
    feb=12:12:34;
    may=3:12:34;
    aug=6:12:34;
    nov=9:12:34;
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    oname='/e2/arango/Damee/grid9/damee9_yavg_01.nc';
    feb= 1:12:36;
    may= 4:12:36;
    aug= 7:12:36;
    nov=10:12:36;
end,

%--------------------------------------------------------------------------
%  Compute averages.
%--------------------------------------------------------------------------

time=[0 45 135 225 315];
time=time.*86400.0;
status=nc_write(oname,'scrum_time',time);
disp(['time: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'zeta',feb);
status=nc_write(oname,'zeta',Favg,2);
disp(['zeta - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'zeta',may);
status=nc_write(oname,'zeta',Favg,3);
disp(['zeta - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'zeta',aug);
status=nc_write(oname,'zeta',Favg,4);
disp(['zeta - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'zeta',nov);
status=nc_write(oname,'zeta',Favg,5);
disp(['zeta - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'ubar',feb);
status=nc_write(oname,'ubar',Favg,2);
disp(['ubar - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'ubar',may);
status=nc_write(oname,'ubar',Favg,3);
disp(['ubar - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'ubar',aug);
status=nc_write(oname,'ubar',Favg,4);
disp(['ubar - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'ubar',nov);
status=nc_write(oname,'ubar',Favg,5);
disp(['ubar - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'vbar',feb);
status=nc_write(oname,'vbar',Favg,2);
disp(['vbar - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'vbar',may);
status=nc_write(oname,'vbar',Favg,3);
disp(['vbar - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'vbar',aug);
status=nc_write(oname,'vbar',Favg,4);
disp(['vbar - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'vbar',nov);
status=nc_write(oname,'vbar',Favg,5);
disp(['vbar - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'u',feb);
status=nc_write(oname,'u',Favg,2);
disp(['u    - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'u',may);
status=nc_write(oname,'u',Favg,3);
disp(['u    - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'u',aug);
status=nc_write(oname,'u',Favg,4);
disp(['u    - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'u',nov);
status=nc_write(oname,'u',Favg,5);
disp(['u    - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'v',feb);
status=nc_write(oname,'v',Favg,2);
disp(['v    - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'v',may);
status=nc_write(oname,'v',Favg,3);
disp(['v    - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'v',aug);
status=nc_write(oname,'v',Favg,4);
disp(['v    - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'v',nov);
status=nc_write(oname,'v',Favg,5);
disp(['v    - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'omega',feb);
status=nc_write(oname,'omega',Favg,2);
disp(['omeg - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'omega',may);
status=nc_write(oname,'omega',Favg,3);
disp(['omeg - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'omega',aug);
status=nc_write(oname,'omega',Favg,4);
disp(['omeg - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'omega',nov);
status=nc_write(oname,'omega',Favg,5);
disp(['omeg - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'temp',feb);
status=nc_write(oname,'temp',Favg,2);
disp(['temp - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'temp',may);
status=nc_write(oname,'temp',Favg,3);
disp(['temp - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'temp',aug);
status=nc_write(oname,'temp',Favg,4);
disp(['temp - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'temp',nov);
status=nc_write(oname,'temp',Favg,5);
disp(['temp - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'salt',feb);
status=nc_write(oname,'salt',Favg,2);
disp(['salt - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'salt',may);
status=nc_write(oname,'salt',Favg,3);
disp(['salt - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'salt',aug);
status=nc_write(oname,'salt',Favg,4);
disp(['salt - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'salt',nov);
status=nc_write(oname,'salt',Favg,5);
disp(['salt - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'rho',feb);
status=nc_write(oname,'rho',Favg,2);
disp(['rho - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'rho',may);
status=nc_write(oname,'rho',Favg,3);
disp(['rho - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'rho',aug);
status=nc_write(oname,'rho',Favg,4);
disp(['rho - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'rho',nov);
status=nc_write(oname,'rho',Favg,5);
disp(['rho - nov: ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'Hsbl',feb);
status=nc_write(oname,'hbl',Favg,2);
disp(['hbl - feb: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'Hsbl',may);
status=nc_write(oname,'hbl',Favg,3);
disp(['hbl - may: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'Hsbl',aug);
status=nc_write(oname,'hbl',Favg,4);
disp(['hbl - aug: ',num2str(status)]);
[X,Y,Z,Favg]=average(gname,fname,'Hsbl',nov);
status=nc_write(oname,'hbl',Favg,5);
disp(['hbl - nov: ',num2str(status)]);



