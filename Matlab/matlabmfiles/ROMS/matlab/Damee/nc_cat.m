% This script concatenates NetCDF files.

global IPRINT

IPRINT=0;
damee=9;

switch ( damee ),
  case 7
    fname='/e0/arango/Damee/grid7/damee7_avg_01a.nc';
    oname='/e0/arango/Damee/grid5/damee7_yavg_01.nc';
    Tindex=85:1:120;
  case 9
    fname='/e2/arango/Damee/grid9/damee9_avg_01d.nc';
    oname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    nstart=24;
    Tindex=1:1:12;
end,

%--------------------------------------------------------------------------
%  Read in data and append to output NetCDF file
%--------------------------------------------------------------------------

nrec=length(Tindex);

for n=1:nrec,
  Trec=Tindex(n);
  nout=nstart+n;

  [F]=nc_read(fname,'scrum_time',Trec);
  status=nc_write(oname,'scrum_time',F,nout);
  disp(['time - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'zeta',Trec);
  status=nc_write(oname,'zeta',F,nout);
  disp(['zeta - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'ubar',Trec);
  status=nc_write(oname,'ubar',F,nout);
  disp(['ubar - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'vbar',Trec);
  status=nc_write(oname,'vbar',F,nout);
  disp(['vbar - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'u',Trec);
  status=nc_write(oname,'u',F,nout);
  disp(['u    - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'v',Trec);
  status=nc_write(oname,'v',F,nout);
  disp(['v    - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'omega',Trec);
  status=nc_write(oname,'omega',F,nout);
  disp(['omeg - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'temp',Trec);
  status=nc_write(oname,'temp',F,nout);
  disp(['temp - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'salt',Trec);
  status=nc_write(oname,'salt',F,nout);
  disp(['salt - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

  [F]=nc_read(fname,'rho',Trec);
  status=nc_write(oname,'rho',F,nout);
  disp(['rho  - Records: ',num2str(Trec),' ',num2str(nout), ...
        ' status = ',num2str(status)]);

end,
