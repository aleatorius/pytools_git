function [x,y,f]=NA_secdif(gname,fname,iname,vname,tindex,xsec,ysec,ds,iflag);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [x,y,f]=NA_section(gname,fname,vname,xsec,ysec,ds,iflag)         %
%                                                                           %
% This script extracts a cross-section from a NetCDF file.  Then, it        %
% subtract provided "initial" conditions.  If the more than one time        %
% record is requested, it computes the time average over all records.       %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%                  If no grid file, put field file name instead.            %
%    fname       Field NetCDF file name (character string).                 %
%    iname       Initial field NetCDF file name (character string).         %
%    vname       NetCDF variable name to process (character string).        %
%    tindex      Time records to process (integer; scalar or vector).       %
%                  If tindex is a vector, the data will averaged            %
%                  over all requested time records.                         %
%    xsec        Starting and Ending section X-positions (vector).          %
%    ysec        Starting and Ending section Y-positions (vector).          %
%    ds          Section horizontal grid spacing (real).                    %
%    iflag       Interpolation flag (integer):                              %
%                  iflag=0  => interpolate to model depths.                 %
%                  iflag=1  => interpolate to standard depths.              %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    x           Slice X-positions (matrix).                                %
%    y           Slice Y-positions (matrix).                                %
%    f           Field section (array).                                     %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Deactivate printing information switch from "nc_read".
 
global IPRINT
 
IPRINT=0;

method='linear';
meridional=0;
transverse=0;
zonal=0;

if (nargin < 7),
  iflag=0;
end,

%----------------------------------------------------------------------------
% Set standard depths to interpolate.
%----------------------------------------------------------------------------

z=[    0   -10   -20   -30   -50   -75  -100  -125  -150  -200  -250 ...
    -300  -400  -500  -600  -700  -800  -900 -1000 -1100 -1200 -1300 ...
   -1400 -1500 -1750 -2000 -2250 -2500 -2750 -3000 -3250 -3500 -3750 ...
   -4000 -4250 -4500 -4750 -5000 -5250];

Kz=length(z);
   
%----------------------------------------------------------------------------
% Determine positions and Land/Sea masking variable names.
%----------------------------------------------------------------------------

[dnames,dsizes,igrid]=nc_vinfo(fname,vname);

if (~isempty(findstr(dnames(1,:),'time'))),
  if ((length(dsizes)-1) < 3),
    error(['section - cannot vertically interpolate: ',vname]);
  end
end,

switch ( igrid ),
  case 1
    Xname='lon_rho';
    Yname='lat_rho';
    Mname='mask_rho';
  case 2
    Xname='lon_psi';
    Yname='lat_psi';
    Mname='mask_psi';
  case 3
    Xname='lon_u';
    Yname='lat_u';
    Mname='mask_u';
  case 4
    Xname='lon_v';
    Yname='lat_v';
    Mname='mask_v';
  case 5
    Xname='lon_rho';
    Yname='lat_rho';
    Mname='mask_rho';
end,
 
%----------------------------------------------------------------------------
% Read in variable positions and Land/Sea mask.
%----------------------------------------------------------------------------
 
X=nc_read(gname,Xname); X=X';
Y=nc_read(gname,Yname); Y=Y';
 
mask=nc_read(gname,Mname); mask=mask';
 
%----------------------------------------------------------------------------
% Compute grid depths.
%----------------------------------------------------------------------------
 
Z=depths(fname,gname,igrid,1,0);
[Jm Im Km]=size(Z);

if (iflag),
  Z(:,:,Km)=zeros([Jm Im]);
end,

%----------------------------------------------------------------------------
% Read in initial field.
%----------------------------------------------------------------------------

Fini=nc_read(iname,vname,1);

%----------------------------------------------------------------------------
% Read in field(s).  If apropriate, average over number of requested
% records.
%----------------------------------------------------------------------------

nrec=length(tindex);

if (nrec > 1),
  i=0;
  F=zeros([Im Jm Km]);
  for n=1:nrec
    i=i+1;
    Trec=tindex(n);
    a=nc_read(fname,vname,Trec);
    F=F+a;
  end,
  F=F./i;
else,
  F=nc_read(fname,vname,tindex);
end,

% Subtract initial conditions.

F=F-Fini;
for k=1:Km, a=F(:,:,k); b(:,:,k)=a'; end, F=b; clear a b

% Apply Land/Sea mask to average fields.

msk=mask(:,:,ones([1 Km]));
Mind=find(msk < 1.0);
F(Mind)=NaN;

%--------------------------------------------------------------------------
% Extract section at model depths.
%--------------------------------------------------------------------------

if (length(xsec) == 2),
  x1=xsec(1);
  x2=xsec(2);
  if (x1 == x2), meridional=1; end,
else
  transverse=1;
end,

if (length(ysec) == 2),
  y1=ysec(1);
  y2=ysec(2);
  if (y1 == y2), zonal=1; end,
else
  transverse=1;
end,

if (meridional),

  s=y1:ds:y2;
  Xi=x1;
  Yi=s';
  Lm=length(Yi);
  x=Yi(:,ones([1 Km]));

  for k=1:Km,
    Zi(:,k)=interp2(X,Y,Z(:,:,k),Xi,Yi,method);
    Fi(:,k)=interp2(X,Y,F(:,:,k),Xi,Yi,method);
  end,

elseif (zonal),

  s=x1:ds:x2;
  Xi=s';
  Yi=y1;
  Lm=length(Xi);
  x=Xi(:,ones([1 Km]));

  for k=1:Km,
    Zi(:,k)=interp2(X,Y,Z(:,:,k),Xi,Yi,method)';
    Fi(:,k)=interp2(X,Y,F(:,:,k),Xi,Yi,method)';
  end,

elseif (transverse),

  Xi=xsec;
  Yi=ysec;
  Lm=length(Xi);
  x=Xi(:,ones([1 Km]));

  for k=1:Km,
    for i=1:Lm,
      Zi(i,k)=interp2(X,Y,Z(:,:,k),Xi(i),Yi(i),method);
      Fi(i,k)=interp2(X,Y,F(:,:,k),Xi(i),Yi(i),method);
    end,
  end,

end,

if (~iflag),
  y=Zi;
  f=Fi;
  return
end,

%--------------------------------------------------------------------------
%  If applicable, vertically interpolate to standard levels.
%--------------------------------------------------------------------------

if (iflag),
  if (meridional | zonal),
    s=s';
    x=s(:,ones([1 Kz]));
  elseif (transverse),
    x=Xi(:,ones([1 Kz]));
  end,
  y=z(ones([1 Lm]),:);
  for i=1:Lm;
    f(i,:)=interp1(Zi(i,:),Fi(i,:),z,method);
  end,
end,

return
