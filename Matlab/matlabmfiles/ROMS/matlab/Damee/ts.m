%  This function plots TS-diagrams of climatology and simulations.

global IPRINT

IPRINT=0;
IPLOT=1;
ICLIMA=0;
damee='g4_17';

Rmin=20; Rmax=30;

%Tmin=-2; Tmax=2; Smin=34; Smax=35; Rval=Rmin:(Rmax-Rmin)/50:Rmax;
%Tmin=0; Tmax=20; Smin=32; Smax=37; Rval=Rmin:(Rmax-Rmin)/25:Rmax;
Tmin=6; Tmax=12; Smin=35; Smax=37; Rval=Rmin:(Rmax-Rmin)/50:Rmax;

switch ( damee ),
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_17.nc';
    mgrid='Damee #4: Run 17,';
    Tindex=2:3:12;
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_19_01.nc';
    mgrid='Damee #4: Run 19_01,';
    Tindex=1:1:5;
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_20.nc';
    mgrid='Damee #4: Run 20,';
    Tindex=1:1:5;
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_22.nc';
    mgrid='Damee #4: Run 22,';
    Tindex=1:1:5;
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_23.nc';
    mgrid='Damee #4: Run 23,';
    Tindex=2:3:12;
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_24.nc';
    mgrid='Damee #4: Run 24,';
    Tindex=2:3:12;
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_25.nc';
    mgrid='Damee #4: Run 25,';
    Tindex=2:3:12;
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_26.nc';
    mgrid='Damee #4: Run 26,';
    Tindex=2:3:12;
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_27.nc';
    mgrid='Damee #4: Run 27,';
    Tindex=2:3:12;
  case 'g5_03'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_yavg_03.nc';
    mgrid='Damee #5: Run 03,';
    Tindex=2:3:12;
  case 'g6_03'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    fname='/comop/arango/Damee/grid6/damee6_yavg_03.nc';
    mgrid='Damee #6: Run 03,';
    Tindex=2:3:12;
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_yavg_05.nc';
    Tindex=2:3:12;
    mgrid='Damee #7: Run 05,';
end,

cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';

%-----------------------------------------------------------------------
% Plot TS-diagrams for Levitus Climatology.
%-----------------------------------------------------------------------

if (ICLIMA),

  Cindex=[2 5 8 11];
  nrec=length(Cindex);

  for n=1:nrec,

    Trec=Cindex(n);

    if (Trec == 1),
      Title='Levitus January Climatology (1994)';
    elseif (Trec == 2),
      Title='Levitus February Climatology (1994)';
    elseif (Trec == 3),
      Title='Levitus March Climatology (1994)';
    elseif (Trec == 4),
      Title='Levitus April Climatology (1994)';
    elseif (Trec == 5),
      Title='Levitus May Climatology (1994)';
    elseif (Trec == 6),
      Title='Levitus June Climatology (1994)';
    elseif (Trec == 7),
      Title='Levitus July Climatology (1994)';
    elseif (Trec == 8),
      Title='Levitus August Climatology (1994)';
    elseif (Trec == 9),
      Title='Levitus September Climatology (1994)';
    elseif (Trec == 10),
      Title='Levitus October Climatology (1994)';
    elseif (Trec == 11),
      Title='Levitus November Climatology (1994)';
    elseif (Trec == 12),
      Title='Levitus December Climatology (1994)';
    end,

    figure;
    F=ts_diag(gname,cname,Trec,Smin,Smax,Tmin,Tmax,Rval);
    title([Title]);

    if (n == 1),
      print -zbuffer -dps ts_Lclm.ps
    else,
      print -zbuffer -dps -append ts_Lclm.ps
    end,

  end,
end,

%-----------------------------------------------------------------------
% Plot TS-diagrams simulation
%-----------------------------------------------------------------------

nrec=length(Tindex);

for n=1:nrec,

  Trec=Tindex(n);

  if (Trec == 1),
    Title=' January Average';
  elseif (Trec == 2),
    Title=' February Average';
  elseif (Trec == 3),
    Title='March Average';
  elseif (Trec == 4),
    Title=' April Average';
  elseif (Trec == 5),
    Title=' May Average';
  elseif (Trec == 6),
    Title=' June Average';
  elseif (Trec == 7),
    Title=' July Average';
  elseif (Trec == 8),
    Title=' August Average';
  elseif (Trec == 9),
    Title=' September Average';
  elseif (Trec == 10),
    Title=' October Average';
  elseif (Trec == 11),
    Title=' November Average';
  elseif (Trec == 12),
    Title=' December Average';
  end,

  figure;
  F=ts_diag(gname,fname,Trec,Smin,Smax,Tmin,Tmax,Rval);
  title(strcat(mgrid,'  ',Title));

  if (n == 1),
    print -zbuffer -dps ts_diag.ps
  else,
    print -zbuffer -dps -append ts_diag.ps
  end,

end,
