%  This function computes area integrated temperature and salinity
%  profiles in the specified region.

%  Read in area profile from matlab file.

load area.mat

%  Set data according to paper experiments

z1=area24.Z; t1=area24.T; s1=area24.S; 
  z1c=area24.Zlev; t1c=area24.Tlev; s1c=area24.Slev;
z2=area22.Z; t2=area22.T; s2=area22.S;
  z2c=area22.Zlev; t2c=area22.Tlev; s2c=area22.Slev;
z3=area26.Z; t3=area26.T; s3=area26.S;
  z3c=area26.Zlev; t3c=area26.Tlev; s3c=area26.Slev;
z4=area25.Z; t4=area25.T; s4=area25.S;
  z4c=area25.Zlev; t4c=area25.Tlev; s4c=area25.Slev;
z5=area02.Z; t5=area02.T; s5=area02.S;
  z5c=area02.Zlev; t5c=area02.Tlev; s5c=area02.Slev;
z6=area05.Z; t6=area05.T; s6=area05.S;
  z6c=area05.Zlev; t6c=area05.Tlev; s6c=area05.Slev;
z7=area17.Z; t7=area17.T; s7=area17.S;
  z7c=area17.Zlev; t7c=area17.Tlev; s7c=area17.Slev;
z8=area27.Z; t8=area27.T; s8=area27.S;
  z8c=area27.Zlev; t8c=area27.Tlev; s8c=area27.Slev;

clear area*

%  Plot composite area averaged profiles.

figure;

subplot(1,2,1);
plot(t1,z1,'r-', ...
     t2,z2,'c-', ...
     t3,z3,'g-', ...
     t4,z4,'b-', ...
     t5,z5,'k-', ...
     t6,z6,'m-', ...
     t7,z7,'y-');
set(gcf,'Units','Normalized',...
        'Position',[0.2 0.1 0.6 0.8],...
        'PaperUnits','Normalized',...
        'PaperPosition',[0 0 1 1]);
grid on;
legend('E1','E2','E3','E4','E5','E6','E7',4);
hold on;
h=plot(t1c,z1c,'k--'); set(h,'LineWidth',[2]);
set(gca,'Ylim',[-1000 0]);
title('Temperature');
ylabel('depth (m)');

subplot(1,2,2);
plot(s1,z1,'r-', ...
     s2,z2,'c-', ...
     s3,z3,'g-', ...
     s4,z4,'b-', ...
     s5,z5,'k-', ...
     s6,z6,'m-', ...
     s7,z7,'y-');
grid on;
hold on;
h=plot(s1c,z1c,'k--'); set(h,'LineWidth',[2]);
set(gca,'Ylim',[-1000 0]);
title('Salinity');
ylabel('depth (m)');

print -dpsc ts_area1.ps

figure;

subplot(1,2,1);
plot(t1,z1,'r-', ...
     t2,z2,'c-', ...
     t3,z3,'g-', ...
     t4,z4,'b-', ...
     t5,z5,'k-', ...
     t6,z6,'m-', ...
     t7,z7,'y-');
set(gcf,'Units','Normalized',...
        'Position',[0.2 0.1 0.6 0.8],...
        'PaperUnits','Normalized',...
        'PaperPosition',[0 0 1 1]);
grid on;
legend('E1','E2','E3','E4','E5','E6','E7',4);
hold on;
h=plot(t1c,z1c,'k--'); set(h,'LineWidth',[2]);
set(gca,'Ylim',[-5000 -1000]);
title('Temperature');
ylabel('depth (m)');

subplot(1,2,2);
plot(s1,z1,'r-', ...
     s2,z2,'c-', ...
     s3,z3,'g-', ...
     s4,z4,'b-', ...
     s5,z5,'k-', ...
     s6,z6,'m-', ...
     s7,z7,'y-');
grid on;
hold on;
h=plot(s1c,z1c,'k--'); set(h,'LineWidth',[2]);
set(gca,'Ylim',[-5000 -1000]);
title('Salinity');

print -dpsc -append ts_area1.ps

%------------------------------------------------------------------------
%  Plot only E1 and E5
%------------------------------------------------------------------------

figure;

subplot(1,2,1);
plot(t1,z1,'k-', ...
     t6,z6,'g-', ...
     t1c,z1c,'r-');
grid on;
legend('E1','E6','Lev',4);
set(gca,'Ylim',[-1000 0]);
title('Temperature');
ylabel('depth (m)');

%figure

subplot(1,2,2);
plot(s1,z1,'k-', ...
     s6,z6,'g-', ...
     s1c,z1c,'r-');
grid on;
set(gca,'Ylim',[-1000 0]);
title('Salinity');
ylabel('depth (m)');

print -dpsc ts_area2.ps

figure;

subplot(1,2,1);
plot(t1,z1,'k-', ...
     t6,z6,'g-', ...
     t1c,z1c,'r-');
grid on;
legend('E1','E6','Lev',4);
set(gca,'Ylim',[-5000 -1000]);
title('Temperature');
ylabel('depth (m)');

%figure

subplot(1,2,2);
plot(s1,z1,'k-', ...
     s6,z6,'g-', ...
     s1c,z1c,'r-');
grid on;
set(gca,'Ylim',[-5000 -1000]);
title('Salinity');

print -dpsc -append ts_area2.ps
