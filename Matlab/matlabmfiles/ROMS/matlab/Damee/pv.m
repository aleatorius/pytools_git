%  This function computes potential density and potential vorticity
%  sections averaged over specified region.

global IPRINT

IPRINT=0;
IPLOT=1;
ICOLOR=1;
damee='g4_28';

zref=0;

Llon=-32; Rlon=-28; Blat=10; Tlat=55;
%Llon=-57; Rlon=-53; Blat=10; Tlat=44;

delta=0.5;

Vcnt=[-0.1 0.1 2.0 3.0 4.0 5.0 6.0 7.0 8.0 10.0 20.0 40.0 100.0];

Vscl=1.0e+11;
cmin=0; cmax=10;
ncpv=13; dark=0.3; light=1.0; step=(dark-light)/(ncpv-1);
grey=dark:-step:light;

if ( ICOLOR ),
  Cmap='default';
else,
  Cmap=[grey' grey' grey'];
end,

ncrho=20; Rmin=24; Rmax=28;
Rval=Rmin:(Rmax-Rmin)/ncrho:Rmax;
%Rval=[24.0 24.5 24.6 25.0 25.5 25.7 26.0 26.5 27.0];

switch ( damee ),
  case 'clima'
    fname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Levitus,';
    Nstr=1; Nend=12;
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 04,';
    Nstr=0; Nend=12;
  case 'g4_06'
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 06,';
    Nstr=0; Nend=12;
  case 'g4_07'
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 07,';
    Nstr=0; Nend=12;
  case 'g4_10.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 10.01,';
    Nstr=0; Nend=12;
  case 'g4_13.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 13.01,';
    Nstr=0; Nend=12;
  case 'g4_13.2'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 13.02,';
    Nstr=0; Nend=12;
  case 'g4_14'
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 14,';
    Nstr=0; Nend=12;
  case 'g4_15'
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 15,';
    Nstr=0; Nend=12;
  case 'g4_16'
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Damee #4: Run 16,';
    Nstr=0; Nend=12;
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    mgrid='Damee #4: Run 17 ';
    Nstr=0; Nend=12;
  case 'g4_18'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_18.nc';
    mgrid='Damee #4: Run 18,';
    Nstr=0; Nend=12;
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    mgrid='Damee #4: Run 19_01,';
    Nstr=0; Nend=12;
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    mgrid='Damee #4: Run 20,';
    Nstr=0; Nend=12;
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    mgrid='Damee #4: Run 22,';
    Nstr=0; Nend=12;
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    mgrid='Damee #4: Run 23,';
    Nstr=0; Nend=12;
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    mgrid='Damee #4: Run 24,';
    Nstr=0; Nend=12;
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
%   fname='/comop/arango/Damee/grid4/damee4_ann_25.nc';
    mgrid='Damee #4: Run 25,';
    Nstr=0; Nend=12;
%   Nstr=0; Nend=0;
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    mgrid='Damee #4: Run 26,';
    Nstr=0; Nend=12;
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    mgrid='Damee #4: Run 27,';
    Nstr=0; Nend=12;
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    mgrid='Damee #4: Run 28,';
    Nstr=0; Nend=12;
  case 'g5_03'
    fname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    mgrid='Damee #5: Run 03,';
    Nstr=0; Nend=12;
    Llon=-32; Rlon=-28; Blat=10; Tlat=49;
  case 'g5_04'
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    mgrid='Damee #5: Run 04,';
    Nstr=0; Nend=12;
    Llon=-32; Rlon=-28; Blat=10; Tlat=49;
  case 'g6_03'
    fname='/comop/arango/Damee/grid6/damee6_avg_03.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    mgrid='Damee #6: Run 03,';
    Nstr=0; Nend=12;
    Llon=-32; Rlon=-28; Blat=10; Tlat=49;
  case 'g6_04'
    fname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    mgrid='Damee #6: Run 04,';
    Nstr=0; Nend=12;
    Llon=-32; Rlon=-28; Blat=10; Tlat=49;
  case 'g7_01'
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc/damee7_grid_a.nc';
    mgrid='Damee #7: Run 01,';
    Nstr=0; Nend=12;
  case 'g7_04'
    fname='/comop/arango/Damee/grid4/damee7_avg_04_04.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    mgrid='Damee #7: Run 04,';
    Nstr=0; Nend=12;
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    mgrid='Damee #7: Run 05,';
    Nstr=0; Nend=12;
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    mgrid='Damee #9: Run 01,';
    Nstr=0; Nend=12;
end,

%--------------------------------------------------------------------------
% Compute potential density and voticity sections.
%--------------------------------------------------------------------------

for n=Nstr:Nend,
  switch (n),
    case 0
      if (damee(1:5) == 'g4_04'),
        Tindex=26:1:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=25:1:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=13:1:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 1:1:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 1:1:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 1:1:36;
      else,
        Tindex=85:1:120;
%       Tindex=1;
      end,
      Text=' Annual';
    case 1
      if (damee(1:5) == 'g4_04'),
        Tindex=36:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=36:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=24:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex=10:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex=12:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex=12:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=96:12:120;
      end,
      Text=' January';
    case 2
      if (damee(1:5) == 'g4_04'),
        Tindex=37:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=25:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=13:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex=11:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 1:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 1:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=85:12:120;
      end,
      Text=' February';
    case 3
      if (damee(1:5) == 'g4_04'),
        Tindex=38:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=26:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=14:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex=12:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 2:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 2:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=86:12:120;
      end,
      Text=' March';
    case 4
      if (damee(1:5) == 'g4_04'),
        Tindex=39:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=27:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=15:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 1:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 3:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 3:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=87:12:120;
      end,
      Text=' April';
    case 5
      if (damee(1:5) == 'g4_04'),
        Tindex=40:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=28:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=16:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 2:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 4:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 4:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=88:12:120;
      end,
      Text=' May';
    case 6
      if (damee(1:5) == 'g4_04'),
        Tindex=41:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=29:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=17:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 3:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 5:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 5:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=89:12:120;
      end,
        Text=' June';
    case 7
      if (damee(1:5) == 'g4_04'),
        Tindex=42:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=30:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=18:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 4:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 6:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 6:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=90:12:120;
      end,
      Text=' July';
    case 8
      if (damee(1:5) == 'g4_04'),
        Tindex=43:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=31:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=19:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 5:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 7:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 7:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=91:12:120;
      end,
      Text=' August';
    case 9
      if (damee(1:5) == 'g4_04'),
        Tindex=44:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=32:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=20:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 6:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 8:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 8:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=92:12:120;
      end,
      Text=' September';
    case 10
      if (damee(1:5) == 'g4_04'),
        Tindex=45:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
       Tindex=33:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=21:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 7:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 9:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 9:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=93:12:120;
      end,
      Text=' October';
    case 11
      if (damee(1:5) == 'g4_04'),
        Tindex=46:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=34:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=22:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 8:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex=10:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex=10:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=94:12:120;
      end,
        Text=' November';
    case 12
      if (damee(1:5) == 'g4_04'),
        Tindex=47:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=35:12:60;
      elseif (damee(1:5) == 'g7_01')
        Tindex=23:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 9:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex=11:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex=11:12:36;
      elseif (damee(1:5) == 'clima')
        Tindex=n;
      else,
        Tindex=95:12:120;
      end,
      Text=' December';
  end,

  [Dat]=pvor(gname,fname,Tindex,zref,Llon,Rlon,Blat,Tlat,delta);

%--------------------------------------------------------------------------
%  Plot sections.
%--------------------------------------------------------------------------

  if ( IPLOT ),
   
%   PV=abs(-Vscl.*Dat.pv);
    PV=-Vscl.*Dat.pv;

    figure;

  % pcolor(Dat.ypv,Dat.zpv,PV); shading interp; 
    [c1,h1]=contourf(Dat.ypv,Dat.zpv,PV,Vcnt); shading flat;
    caxis([cmin cmax]);
    colormap(Cmap);
    colorbar;

    set(gca,'Ylim',[-1500 0],'Ytick',[-1500:100:0]);

    hold on;
    [c,h2]=contour(Dat.yrho,Dat.zrho,Dat.rho-1000,Rval); clabel(c,h2);
    set(h2,'edgecolor','k');
    box on;

    title(strcat(mgrid,Text,...
                 ' Potential Vorticity (10^{-11}  m^{-1} s^{-1})'));
    RangeLab=['   PVmin= ',num2str(min(min(PV))), ...
             ',   PVmax= ',num2str(max(max(PV))), ...
             ',   Dmin= ',num2str(min(min(Dat.rho-1000))), ...
             ',   Dmax= ',num2str(max(max(Dat.rho-1000)))];
    xlabel({['Latitude'],[RangeLab]});

    if ( ICOLOR ),
      if (n == 1),
        print -dpsc pv.ps
      else,
        print -dpsc -append pv.ps
      end,
    else
      if (n == 1),
        print -dps pv.ps
      else,
        print -dps -append pv.ps
      end,
    end,

  end,
end,

