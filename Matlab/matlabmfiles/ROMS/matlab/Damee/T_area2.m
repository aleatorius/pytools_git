%  This function computes area integrated temperature and salinity
%  profiles in the specified region.

global IPRINT


IPRINT=0;
IPLOT=1;
IPLOT2=0;
damee='g4_25';
interpolate=0;

Llon=-97;
Rlon=-10;
Blat=9;
Tlat=47;
delta=0.5;

RangeLab=[' Llon= ',num2str(Llon), ...
         ', Rlon= ',num2str(Rlon), ...
         ', Blat= ',num2str(Blat), ...
         ', Tlat= ',num2str(Tlat)];

switch ( damee ),
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_19_01.nc';
    Tindex=1;
    mgrid='Damee #4: Run 19_01 ';
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_20.nc';
    Tindex=1;
    mgrid='Damee #4: Run 20 ';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_22_01.nc';
    Tindex=1;
    mgrid='Damee #4: Run 22_01 ';
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_23.nc';
    Tindex=1;
    mgrid='Damee #4: Run 23 ';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_24.nc';
    Tindex=1:1:12;
    mgrid='Damee #4: Run 24 ';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_25.nc';
    Tindex=1:1:12;
    mgrid='Damee #4: Run 25 ';
end,

cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
Nrec=length(Tindex);

%--------------------------------------------------------------------------
%  Compute area averaged temperature and salinity profiles.
%--------------------------------------------------------------------------

for n=1:Nrec,
  Trec=Tindex(n);
  if (interpolate),
    [z,t,s]=tsavg(gname,fname,Trec,Llon,Rlon,Blat,Tlat);
    Z(:,n)=z';
    T(:,n)=t';
    S(:,n)=s';
  else,
    [z,t,s]=tsavg(gname,fname,Trec);
    Z(:,n)=z';
    T(:,n)=t';
    S(:,n)=s';
  end,
  disp(['Processing record: ',num2str(n,'%3.3i')]);
end,

%--------------------------------------------------------------------------
%  Plot area averaged temperature and salinity profiles.
%--------------------------------------------------------------------------

