function [z,T,S]=tsavg(gname,fname,tindex,Llon,Rlon,Blat,Tlat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [z,T,S]=tsavg(gname,fname,tindex,Llon,Rlon,Blat,Tlat)            %
%                                                                           %
% This function computes temperature and salinity vertical profiles         %
% averaged over specified area.                                             %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%    fname       Field NetCDF file name (character string).                 %
%    tindex      Time records to process (integer; scalar or vector).       %
%                  If tindex is a vector, the data will averaged            %
%                  over all requested time records.                         %
%    Llon        Left edge longitude (degrees, negative EAST).              %
%    Rlon        Right edge longitude (degrees, negative EAST).             %
%    Blat        Bottom edge latitude (degrees, negative SOUTH).            %
%    Tlat        Top edge latitude (degrees, negative SOUTH).               %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    z           depth (vector; meters).                                    %
%    T           temperature profile (vector; Celcius)                      %
%    S           salinity profile (vector; PSU)                             %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='linear';

%----------------------------------------------------------------------------
% Set standard levels.
%----------------------------------------------------------------------------

z=[   -5   -10   -20   -30   -50   -75  -100  -125  -150  -200  -250 ...
    -300  -400  -500  -600  -700  -800  -900 -1000 -1100 -1200 -1300 ...
   -1400 -1500 -1750 -2000 -2250 -2500 -2750 -3000 -3250 -3500 -3750 ...
   -4000 -4250 -4500 -4750 -5000];

Kz=length(z);

%----------------------------------------------------------------------------
% Compute depths at RHO-points.
%----------------------------------------------------------------------------

Zr=depths(fname,gname,1,0,0);

[Lp,Mp,N]=size(Zr);

%----------------------------------------------------------------------------
%  Select indices of area to process.
%----------------------------------------------------------------------------

rlon=nc_read(gname,'lon_rho');
rlat=nc_read(gname,'lat_rho');

if (nargin > 3)
  Istr=min(find(rlon(:,1) >= Llon));
  Iend=max(find(rlon(:,1) <= Rlon));
  Jstr=min(find(rlat(1,:) >= Blat));
  Jend=max(find(rlat(1,:) <= Tlat));
else,
  Istr=1;
  Iend=Lp;
  Jstr=1;
  Jend=Mp;
end,

%----------------------------------------------------------------------------
% Read in curvilinear metric factor and compute area.
%----------------------------------------------------------------------------

pm=nc_read(gname,'pm');
pn=nc_read(gname,'pn');
rmask=nc_read(gname,'mask_rho');

dxdy=(1./pm).*(1./pn).*rmask;

clear pm pn

%----------------------------------------------------------------------------
% Read in temperature and salinity and compute its time average.
%----------------------------------------------------------------------------

t=zeros([Lp Mp N]);
s=zeros([Lp Mp N]);

nrec=length(tindex);

if (nrec > 1),
  i=0;
  for n=1:nrec,
    i=i+1;
    Trec=tindex(n);
    a=nc_read(fname,'temp',Trec);
    t=t+a;
    a=nc_read(fname,'salt',Trec);
    s=s+a;
  end,
  t=t./i;
  s=s./i;
  clear a
else,
  t=nc_read(fname,'temp',tindex);
  s=nc_read(fname,'salt',tindex);
end,
  
%----------------------------------------------------------------------------
% Interpolate to standard vertical levels.
%----------------------------------------------------------------------------

jj=0;
for j=Jstr:Jend,
  ii=0;
  jj=jj+1;
  for i=Istr:Iend,
    ii=ii+1;
    if (rmask(i,j) > 0),
      Zwrk=reshape(Zr(i,j,:),1,N);
      Twrk=reshape(t(i,j,:),1,N);
      Swrk=reshape(s(i,j,:),1,N);
      Ti(ii,jj,:)=interp1(Zwrk,Twrk,z,method);
      Si(ii,jj,:)=interp1(Zwrk,Swrk,z,method);
      DxDy(ii,jj)=dxdy(i,j);
    end,
  end,
end,

clear Tz Sz t s

%----------------------------------------------------------------------------
% Compute area averages.
%----------------------------------------------------------------------------

[Im Jm Km]=size(Ti);

for k=1:Km,
  T(k)=NaN;
  S(k)=NaN;
  Tsum=0;
  Ssum=0;
  Asum=0;
  for j=1:Jm,
    for i=1:Im,
      if (~isnan(Ti(i,j,k))),
        Tsum=Tsum+Ti(i,j,k)*DxDy(i,j);
        Ssum=Ssum+Si(i,j,k)*DxDy(i,j);
        Asum=Asum+DxDy(i,j);
      end,
    end,
  end,
  if (Asum > 0),
    T(k)=Tsum/Asum;
    S(k)=Ssum/Asum;
  end,
end,

return
