function [status]=wrt_psi(fname,psi,time,lon,lat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function status=wrt_psi(fname,psi,time)                                   %
% function status=wrt_psi(fname,psi,time,lon,lat)                           %
%                                                                           %
% This writes transport streamfunction data into a NetCDF. It the number    %
% of arguments is less than three, it appends data to an existing NetCDF    %
% and increments the time record (unlimited) dimension by one. Otherwise,   %
% it creates and defines a new NetCDF file.                                 %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    fname       output NetCDF file name (character string).                %
%    psi         Transport Streamfunction (real matrix; Sv).                %
%    time        Time (seconds).                                            %
%    lon         Longitude (real matrix).                                   %
%    lat         Latitude (real matrix).                                    %
%                                                                           %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).               %
%          nc_write                                                         %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Supress all error messages from NetCDF.

[ncopts]=mexcdf('setopts',0);

%  Define NetCDF parameters.

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncchar]=mexcdf('parameter','nc_char');
[ncfloat]=mexcdf('parameter','nc_float');
[ncunlim]=mexcdf('parameter','nc_unlimited');

%  Set variable dimensions.

[L M]=size(psi);

%----------------------------------------------------------------------------
%  If appending, inquire current time record index.
%----------------------------------------------------------------------------

tindex=1;

if (nargin < 4),

  [ncid]=mexcdf('ncopen',fname,'nc_write');
  if (ncid == -1),
    error(['WRT_SSH: ncopen - unable to open file: ', fname])
    return
  end

% Inquire about unlimmited dimension.
 
  [ndims,nvars,natts,recdim,status]=mexcdf('ncinquire',ncid);
  if (status == -1),
    error(['NC_WRITE: ncinquire - cannot inquire file: ',fname])
  end,
  [name,dsize,status]=mexcdf('ncdiminq',ncid,recdim);
  if (status == -1),
    error(['NC_READ: ncdiminq - unable to inquire about unlimited ',...
           'dimension.'])
  end

  tindex=dsize+1;

end,

%----------------------------------------------------------------------------
%  Create and define output NetCDF.
%----------------------------------------------------------------------------

if (tindex == 1),

%  Create NetCDF file.

  [ncid]=mexcdf('nccreate',fname,'nc_write');
  if (ncid == -1),
    error(['WRT_PSI: ncopen - unable to open file: ', fname])
    return
  end

%  Define dimensions.
 
  [xrdim]=mexcdf('ncdimdef',ncid,'xi_psi',L);
  if (xrdim == -1),
    error(['WRT_PSI: ncdimdef - unable to define dimension: lon_rho.'])
  end,
 
  [yrdim]=mexcdf('ncdimdef',ncid,'eta_psi',M);
  if (yrdim == -1),
    error(['WRT_PSI: ncdimdef - unable to define dimension: lat_rho.'])
  end,
 
  [tdim]=mexcdf('ncdimdef',ncid,'time',ncunlim);
  if (tdim == -1),
    error(['WRT_PSI: ncdimdef - unable to define unlimited dimension: ',...
          ' psi_time.'])
  end,

%  Define global attribute(s).

  type='SCRUM history file';
  history=['Transport Streamfunction data, ', date_stamp];

  lenstr=max(size(type));
  [status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lenstr,type);
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to global attribure: history.'])
    return
  end
 
  lenstr=max(size(history));
  [status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lenstr,history);
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to global attribure: history.'])
    return
  end

%  Define PSI time.

  [varid]=mexcdf('ncvardef',ncid,'model_time',ncdouble,1,tdim);
  if (varid == -1),
    error(['WRT_PSI: ncvardef - unable to define variable: model_time.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,25,...
                  'time since initialization');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'psi_time:long_name.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,6,...
                  'second');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'psi_time:units.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'field',ncchar,20,...
                  'time, scalar, series');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'psi_time:field.'])
  end

%  Define longitude and latitude.

  [varid]=mexcdf('ncvardef',ncid,'lon_psi',ncfloat,2,[yrdim xrdim]);
  if (varid == -1),
    error(['WRT_PSI: ncvardef - unable to define variable: lon.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,23,...
                  'longitude of PSI-points');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'PSI:long_name.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,11,...
          'degree_east');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'PSI:units.'])
  end

  [varid]=mexcdf('ncvardef',ncid,'lat_psi',ncfloat,2,[yrdim xrdim]);
  if (varid == -1),
    error(['WRT_PSI: ncvardef - unable to define variable: lat.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,22,...
                  'latitude of PSI-points');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'PSI:long_name.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,12,...
          'degree_north');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'PSI:units.'])
  end

%  Define PSI.

  [varid]=mexcdf('ncvardef',ncid,'psi',ncfloat,3,[tdim yrdim xrdim]);
  if (varid == -1),
    error(['WRT_PSI: ncvardef - unable to define variable: psi.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,24,...
                  'transport streamfunction');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'PSI:long_name.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,9,'Sverdrups');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'PSI:units.'])
  end
  [status]=mexcdf('ncattput',ncid,varid,'field',ncchar,19,...
                  'psi, scalar, series');
  if (status == -1),
    error(['WRT_PSI: ncattput - unable to define attribute: ',...
           'PSI:field.'])
  end

%  Leave definition mode and close NetCDF file.

   [status]=mexcdf('ncendef',ncid);
   if (status == -1),
     error(['WRT_PSI: ncendef - unable to leave definition mode.'])
   end

%----------------------------------------------------------------------------
%  Write out longitude and latitude.
%----------------------------------------------------------------------------

  [status]=nc_write(fname,'lon_psi',lon);
  [status]=nc_write(fname,'lat_psi',lat);

end

%============================================================================
%  Write out or append PSI data into NetCDF file.
%============================================================================

[status]=nc_write(fname,'model_time',time,tindex);
[status]=nc_write(fname,'psi',psi,tindex);

return
