%  Computes mean and variability of SST and SSH averaged over the last
%  3 year of simulation.

global IPRINT

IPRINT=0;
IWRITE=0;
INCWRITE=0;
IPLOT=1;
COLOR=1;
PCOLOR=1;
damee='g4_27';

interpolate=0;
%method='linear';
method='cubic';

spval=900;

stride=[1 1 1 1];
order=1;
missv=2;
spv=0;

%  Set color palette.

nc=20;
 
Tmin=-10;
Tmax=30;
Tint=(Tmax-Tmin)/nc;
Tval=Tmin:Tint:Tmax;
Tlab=[4 8 12 16 20 24 28];
TVint=10/nc;
TVval=0:TVint:10;
TVlab=[2 4 6 8 10];

Hmin=-1.0;
Hmax=1.0;
Hint=(Hmax-Hmin)/nc;
Hval=Hmin:Hint:Hmax;
Hlab=[-0.8 -0.4 -0.2 0.0 0.2 0.4];
HVint=1/nc;
HVval=0:HVint:1;
HVlab=[0.1 0.2 0.3 0.4 0.5];

if ( COLOR ),
  Cmap='default';
else,
  dark=0.2;
  light=0.9;
  step=(dark-light)/(nc-1);
  grey=light:step:dark;
  Cmap=[grey' grey' grey'];
end,

% Set plotting map range.

LonMin=-100.5;
LonMax=15.5;
LatMin=-35.5;
LatMax=65.5;

% Set IO files.

switch (damee), 
  case 'g4_04'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    Tname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    Oname1='sstssh4_04.avg';
    Oname2='/comop/arango/Damee/grid4/damee4_yavg_04.nc';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 04,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_06'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/e1/arango/Damee/grid4/damee4_2dflds_06.nc';
    Tname='/e2/arango/Damee/grid4/damee4_temp_06.nc';
    Oname1='sstssh4_06.avg';
    Oname2='/comop/arango/Damee/grid4/damee4_yavg_06.nc';
    Tindex=1:1:360;
    mgrid='Damee #4: Run 06,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    Tname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    Oname1='sstssh4_17.avg';
    Oname2='/comop/arango/Damee/grid4/damee4_yavg_17.nc';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 17,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_22'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    Tname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    Oname1='sstssh4_22.avg';
    Oname2='/comop/arango/Damee/grid4/damee4_yavg_22.nc';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 22,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    Tname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    Oname1='sstssh4_24.avg';
    Oname2='/comop/arango/Damee/grid4/damee4_yavg_24.nc';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 24,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    Tname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    Oname1='sstssh4_25.avg';
    Oname2='/comop/arango/Damee/grid4/damee4_yavg_25.nc';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 25,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    Tname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    Oname1='sstssh4_26.avg';
    Oname2='/comop/arango/Damee/grid4/damee4_yavg_26.nc';
    Tindex=25:1:60;
    mgrid='Damee #4: Run 26,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_27'
    gname='/n0/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/coamps/grid4/damee4_avg_27_02.nc';
    Tname='/coamps/grid4/damee4_avg_27_02.nc';
    Oname1='sstssh4_27.avg';
%   Oname2='/coamps/grid4/damee4_ann_27_02.nc'; 
%   Tindex=85:1:120;
    Tindex=109:1:120;
    Oname2='/coamps/grid4/damee4_y10_27_02.nc'; 
    mgrid='Damee #4: Run 27,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Zname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    Tname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    Oname1='sstssh4_28.avg';
    Oname2='/comop/arango/Damee/grid4/damee4_ann_28.nc';
    Tindex=68:1:103;
    mgrid='Damee #4: Run 28,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g5_03'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    Zname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    Tname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    Oname1='sstssh5_03.avg';
    Oname2='/comop/arango/Damee/grid5/damee5_yavg_03.nc';
    Tindex=85:1:120;
    mgrid='Damee #5, Run 03,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    Zname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    Tname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    Oname1='sstssh5_04.avg';
    Oname2='/comop/arango/Damee/grid5/damee5_yavg_04.nc';
    Tindex=85:1:120;
    mgrid='Damee #5, Run 04,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g6_03'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    Zname='/comop/arango/Damee/grid6/damee6_avg_03.nc';
    Tname='/comop/arango/Damee/grid6/damee6_avg_03.nc';
    Oname1='sstssh6_03.avg';
    Oname2='/comop/arango/Damee/grid6/damee6_yavg_03.nc';
    Tindex=85:1:120;
    mgrid='Damee #6, Run 03,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g6_04'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    Zname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    Tname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    Oname1='sstssh6_04.avg';
    Oname2='/comop/arango/Damee/grid6/damee6_ann_04.nc';
    Tindex=85:1:120;
    mgrid='Damee #6, Run 04,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    Zname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    Tname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    Oname1='sstssh6_05.avg';
    Oname2='/n2/arango/Damee/grid6/damee6_ann_05.nc';
    Tindex=85:1:120;
    mgrid='Damee #6, Run 05,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    Zname='/coamps/grid6/damee6_avg_06.nc';
    Tname='/coamps/grid6/damee6_avg_06.nc';
    Oname1='sstssh6_06.avg';
    Oname2='/coamps/grid6/damee6_ann_06.nc';
    Tindex=85:1:120;
    mgrid='Damee #6, Run 06,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    Zname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    Tname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    Oname1='sstssh7_05.avg';
    Oname2='/comop/arango/Damee/grid7/damee7_yavg_05.nc';
    Tindex=1:1:36;
    mgrid='Damee #7: Run 05,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
end,

cst_file='/export/home/arango/ocean/plot/Data/coastdb.dat';

nrec=length(Tindex);

istr=findstr(Tname,'.nc');
if (~isempty(istr)),
  tfile=Tname(1:istr-1);
end,

%--------------------------------------------------------------------------
% Read in longitude and latitude positions and Land/Sea mask.
%--------------------------------------------------------------------------

rlon=nc_read(gname,'lon_rho');
rlat=nc_read(gname,'lat_rho');
rmask=nc_read(gname,'mask_rho');

[Lp,Mp]=size(rlon);

[dnames,dsizes]=nc_dim(Tname);
ndims=length(dsizes);
for n=1:ndims,
  name=deblank(dnames(n,:));
  switch name,
    case 's_rho'
      Ninp=dsizes(n);
  end,
end,

rlon=rlon';
rlat=rlat';
rmask=rmask';

%--------------------------------------------------------------------------
% Read in SST and SSH from years 8-10 and compute averages.
%--------------------------------------------------------------------------

% Compute time mean.

SSTavg=zeros([Lp Mp]);
SSHavg=zeros([Lp Mp]);

i=0;

for n=1:nrec;
  i=i+1;
  Trec=Tindex(n);
  start=[Trec Ninp -1 -1];
  count=[Trec Ninp -1 -1];
  f=getcdf_batch(tfile,'temp',start,count,stride,order,missv,spv);
  SSTavg=SSTavg+f;
  f=nc_read(Zname,'zeta',Trec);
  SSHavg=SSHavg+f;
end,

SSTavg=SSTavg./i;
SSHavg=SSHavg./i;

% Compute time variance.

SSTvar=zeros([Lp Mp]);
SSHvar=zeros([Lp Mp]);

i=0;

for n=1:nrec;
  i=i+1;
  Trec=Tindex(n);
  start=[Trec Ninp -1 -1];
  count=[Trec Ninp -1 -1];
  f=getcdf_batch(tfile,'temp',start,count,stride,order,missv,spv);
  v=f-SSTavg;
  SSTvar=SSTvar+v.*v;
  f=nc_read(Zname,'zeta',Trec);
  v=f-SSHavg;
  SSHvar=SSHvar+v.*v;
end,

%SSTvar=sqrt(SSTvar./i);
%SSHvar=sqrt(SSHvar./i);

SSTvar=SSTvar./i;
SSHvar=SSHvar./i;

%--------------------------------------------------------------------------
%  Write out data to NetCDF file.
%--------------------------------------------------------------------------

if (INCWRITE),
  SSTvar2=sqrt(SSTvar);
  SSHvar2=sqrt(SSHvar);
  [status]=wrt_sstssh(Oname2,SSTavg,SSTvar2,SSHavg,SSHvar2);
end,

SSTavg=SSTavg';
SSTvar=SSTvar';
SSHavg=SSHavg';
SSHvar=SSHvar';

%--------------------------------------------------------------------------
% Interpolate data to requested grid.
%--------------------------------------------------------------------------

if (interpolate),
 
  x=-98.0:0.5:10.0; x=x';
  y=9.0:0.5:47.0;

  Lout=length(x);
  Mout=length(y);
  Xout=x(:,ones([1 Mout]));
  Yout=y(ones([1 Lout]),:);

% Apply Land/Sea mask to average fields.

  mask=reshape(rmask,1,prod(size(rmask)));
  Mind=find(mask == 0.0);

  SSTavg=reshape(SSTavg,1,prod(size(SSTavg))); SSTavg(Mind)=[];
  SSTvar=reshape(SSTvar,1,prod(size(SSTvar))); SSTvar(Mind)=[];
  SSHavg=reshape(SSHavg,1,prod(size(SSHavg))); SSHavg(Mind)=[];
  SSHvar=reshape(SSHvar,1,prod(size(SSHvar))); SSHvar(Mind)=[];

  lon=reshape(rlon,1,prod(size(rlon))); lon(Mind)=[];
  lat=reshape(rlat,1,prod(size(rlat))); lat(Mind)=[];

% Interpolate

  SSTavgout=griddata(lon,lat,SSTavg,Xout,Yout,method);
  SSTvarout=griddata(lon,lat,SSTvar,Xout,Yout,method);
  SSHavgout=griddata(lon,lat,SSHavg,Xout,Yout,method);
  SSHvarout=griddata(lon,lat,SSHvar,Xout,Yout,method);

  mask=interp2(rlon,rlat,rmask,Xout,Yout,method);
  Mind=find(mask < 0.5);

  SSTavgout(Mind)=NaN;
  SSTvarout(Mind)=NaN;
  SSHavgout(Mind)=NaN;   
  SSHvarout(Mind)=NaN;   

%--------------------------------------------------------------------------
%  Write out data to ASCII file
%--------------------------------------------------------------------------

  if ( IWRITE ),

    fid=fopen(Oname1,'w');
    if (fid < 0),
      error(['Cannot open ' Oname1 '.'])
    end

    for j=1:Mout, 
      for i=1:Lout, 
        if (~isnan(SSTavgout(i,j))), 
          fprintf(fid,'%7.1f %6.1f %7.3f %7.3f %7.3f %7.3f\n', ... 
                  Xout(i,j),Yout(i,j),SSHavgout(i,j),SSHvarout(i,j), ... 
                  SSTavgout(i,j),SSTvarout(i,j)); 
        end, 
      end, 
    end,

    fclose(fid);

  end,
end,

%--------------------------------------------------------------------------
% Plot data.
%--------------------------------------------------------------------------

if (IPLOT),

  m_proj('Mercator','long',[LonMin LonMax],'lat',[LatMin LatMax]);

  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=spval);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  [Xcst,Ycst]=m_ll2xy(cstlon,cstlat,'point');

  [X,Y]=m_ll2xy(rlon,rlat,'point');
  if (interpolate),
    [XOUT,YOUT]=m_ll2xy(Xout,Yout,'point');
  end,
  [Xmin,Ymin]=m_ll2xy(LonMin,LatMin);
  [Xmax,Ymax]=m_ll2xy(LonMax,LatMax);
  
  figure; 
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1]);
  fmin=min(min(SSHavg)); fmax=max(max(SSHavg));
  if (PCOLOR),
    if (interpolate),
      pcolor(XOUT,YOUT,SSHavgout); shading interp;
    else,
      pcolor(X,Y,SSHavg); shading interp;
    end,
  else,
    [c,h]=contourf(X,Y,SSHavg,Hval); clabel(c,h,Hlab);
    [c,h]=contourf(X,Y,SSHavg,Hval); clabel(c,h,Hlab);
    colormap(Cmap);
  end,
  if (COLOR), colorbar; end,
  hold on;
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  draw_cst(Xcst,Ycst,'k');
  m_grid;
  title(strcat(mgrid,' Mean Sea surface Height (m)'));
  xlabel(['Min = ',num2str(fmin),'  Max = ',num2str(fmax), ...
         '  CI = ',num2str(Hint)]);
  print -dpsc sstssh.ps
    
  figure;
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1]);
  fmin=min(min(SSHvar)); fmax=max(max(SSHvar));
  if (PCOLOR),
    if (interpolate),
      pcolor(XOUT,YOUT,SSHvarout); shading interp;
    else,
      pcolor(X,Y,SSHvar); shading interp;
    end,
  else,
    [c,h]=contourf(X,Y,SSHvar,HVval); clabel(c,h,HVlab);
    colormap(Cmap);
  end,
  if (COLOR), colorbar; end,
  hold on;
  draw_cst(Xcst,Ycst,'k');
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  m_grid;
  title(strcat(mgrid,' Sea surface Height Variability (m)'));
  xlabel(['Min = ',num2str(fmin),'  Max = ',num2str(fmax), ...
          '  CI = ',num2str(HVint)]);

  print -dpsc -append sstssh.ps

  figure;
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1]);
  fmin=min(min(SSTavg)); fmax=max(max(SSTavg));
  if (PCOLOR),
    if (interpolate),
      pcolor(XOUT,YOUT,SSTavgout); shading interp;
    else,
      pcolor(X,Y,SSTavg); shading interp;
    end,
  else,
    [c,h]=contourf(X,Y,SSTavg,Tval); clabel(c,h,Tlab);
    colormap(Cmap);
  end,
  if (COLOR), colorbar; end,
  hold on;
  draw_cst(Xcst,Ycst,'k');
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  m_grid;
  title(strcat(mgrid,' Mean Sea Surface Temperature (C)'));
  xlabel(['Min = ',num2str(fmin),'  Max = ',num2str(fmax), ...
          '  CI = ',num2str(Tint)]);

  print -dpsc -append sstssh.ps
    
  figure;
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1]);
  fmin=min(min(SSTvar)); fmax=max(max(SSTvar));
  if (PCOLOR),
    if (interpolate),
      pcolor(XOUT,YOUT,SSTvarout); shading interp;
    else,
      pcolor(X,Y,SSTvar); shading interp;
    end,
  else,
    [c,h]=contourf(X,Y,SSTvar,TVval); clabel(c,h,TVlab);
    colormap(Cmap);
  end,
  if (COLOR), colorbar; end,
  hold on;
  draw_cst(Xcst,Ycst,'k');
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  m_grid;
  title(strcat(mgrid,' Sea Surface Temperature Variability (C)'));
  xlabel(['Min = ',num2str(fmin),'  Max = ',num2str(fmax), ...
          '  CI = ',num2str(TVint)]);

  print -dpsc -append sstssh.ps

end,
