      program sec_ziv
c
c  This program converts section data to Ziv format.
c
      implicit none
      integer npts
      parameter (npts=100000)
c
      integer im, inp, km, out, stdinp, stdout
      real spval
      real s(npts), t(npts), wrk(npts), x(npts), y(npts)
      character*60 Iname, Oname
      parameter (inp=1, out=2, spval=99.0, stdinp=5, stdout=6)
c
      write(stdout,10)
  10  format(/,' Enter input  file name: ',$)
      read(stdinp,'(a)') Iname
      write(stdout,20)
  20  format(' Enter output file name: ',$)
      read(stdinp,'(a)') Oname
c
      open (inp,file=Iname,form='formatted',status='old')
      open (out,file=Oname,form='formatted',status='unknown')
c
c  Read in number of points to process.
c
      read(inp,*) im, km
c
c  Process data.
c
      call convert (inp,out,x,y,s,t,wrk,im,km,spval)
      stop
      end
      subroutine convert (inp,out,x,y,s,t,wrk,im,km,spval)
c
c  Writes data in Ziv format.
c
      implicit none
      integer i, im, inp, k, km, nz, out
      real spval
      real s(im,km), t(im,km), wrk(km), x(im), y(im)
c
c  Read in input data.
c
      read(inp,*) (x(i),i=1,im)
      read(inp,*) (y(i),i=1,im)
      read(inp,*) ((t(i,k),i=1,im),k=1,km)
      read(inp,*) ((s(i,k),i=1,im),k=1,km)
      close(inp)
c
c  Write out data.
c
      do i=1,im
        nz=0
        do k=1,km
          if (t(i,k).ne.spval) nz=nz+1
        enddo
        if (nz.ne.0) then
          write(out,10) x(i), y(i), nz
  10      format(f7.1,f6.1,i3)
          write(out,20) (t(i,k), s(i,k), k=1,nz)
  20      format(78f7.3)
        endif
      enddo
      close(out)
      return
      end
