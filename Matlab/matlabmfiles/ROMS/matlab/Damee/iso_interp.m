function [Zval]=iso_interp(F,Z,Fval,isearch);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [Zval]=iso_interp(F,Z,Fval,isearch)                              %
%                                                                           %
% This function finds the depths of requested field values via linear       %
% interpolation.  The input field can be non-monotonic.                     %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    F           Field values as function of depth (vector).                %
%    Z           Depths (vector; negative, deep to shallow).                %
%    Fval        Field values to interpolate (scalar or vector).            %
%    isearch     Search direction (interpolates the first occurrence of     %
%                                  requested value):                        %
%                  isearch => 0, bottom to surface.                         %
%                  isearch => 1, surface to bottom.                         %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Zval        Depth of requested values.                                 %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Set default search direction.

if (nargin < 4),
 isearch=0;
end,

% Dertemine number of points.

N=length(F);
Ni=length(Fval);

% Initialize requested depths.

Zval=ones(size(Fval)).*NaN;

%----------------------------------------------------------------------------
%  Compute depths of requested values via interpolation.
%----------------------------------------------------------------------------

if (isearch),
  F=fliplr(F);
  Z=fliplr(Z);
end,

for i=1:Ni,
  for k=1:N-1,
    if ((F(k) >= Fval(i)) & (Fval(i) > F(k+1))),
      delta=1.0/(F(k)-F(k+1));
      fac1=(Fval(i)-F(k+1))*delta;
      fac2=(F(k)-Fval(i))*delta;
      Zval(i)=fac1*Z(k)+fac2*Z(k+1);
      break;
    elseif ((F(k) < Fval(i)) & (Fval(i) <= F(k+1))),
      delta=1.0/(F(k+1)-F(k));
      fac1=(F(k+1)-Fval(i))*delta;
      fac2=(Fval(i)-F(k))*delta;
      Zval(i)=fac1*Z(k)+fac2*Z(k+1);
      break;
    end,
  end,
end,

return
