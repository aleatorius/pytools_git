function [t,s]=check(fname,Im,Km);

t(1:Im,1:Km)=NaN;
s(1:Im,1:Km)=NaN;

fid=fopen(fname,'r');
if (fid < 0),
  error(['Cannot open ' fname '.'])
end

i=0;
while feof(fid) == 0
  [f,count]=fscanf(fid,'%g %g %d',3);
  if (~isempty(f)),
    nz=round(f(3:3:count));
    [f,count]=fscanf(fid,'%g',2*nz);
    i=i+1;
    t(i,1:nz)=f(1:2:count)';
    s(i,1:nz)=f(2:2:count)';
  end,
end,

