%  This function computes meridional heat transport.

global IPRINT

IPRINT=0;
IWRITE=1;
IPLOT=1;
IPLOT2=0;
COLOR=1;
damee='g6_06';
method='linear';

%  Set color palette.

if (damee(1:2) == 'g9' | damee(1:2) == 'g7'),
  Pmin=-2;
  Pmax=5;
  nc=14;
  Cval=Pmin:(Pmax-Pmin)/nc:Pmax;
  Lval=[0 0.5 1.0 2.0];
else,
  Pmin=0;
  Pmax=2;
  nc=20;
  Cval=Pmin:(Pmax-Pmin)/nc:Pmax;
  Lval=[0.4 0.8 1.2];
end,

if ( COLOR ),
  cmap='default';
else,
  dark=0.2;
  light=0.9;
  step=(dark-light)/(nc-1);
  grey=light:step:dark;
  Cmap=[grey' grey' grey'];
end,

xb = [11 24 36 48];
yb = [1.39 1.08 1.01 .65];
eb = [.25 .25 .25 .25];

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mht4_04.dat';
    Tindex=1:1:61;
    mgrid='Damee #4: Run 04,';
    y=-20:0.5:60; y=y';
  case 'g4_06'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    oname='mht4_06.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 06,';
    y=-20:0.5:60; y=y';
  case 'g4_07'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    oname='mht4_07.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 07,';
    y=-20:0.5:60; y=y';
  case 'g4_10.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    oname='mht4_10_01.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 10.01,';
    y=-20:0.5:60; y=y';
  case 'g4_13.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    oname='mht4_13_01.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 13.01,';
    y=-20:0.5:60; y=y';
  case 'g4_13.2'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    oname='mht4_13_02.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 13.02,';
    y=-20:0.5:60; y=y';
  case 'g4_14'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    oname='mht4_14.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 14,';
    y=-20:0.5:60; y=y';
  case 'g4_15'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    oname='mht4_15.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 15,';
    y=-20:0.5:60; y=y';
  case 'g4_16'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    oname='mht4_16.dat';
    Tindex=1:1:60;
    mgrid='Damee #4: Run 16,';
    y=-20:0.5:60; y=y';
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    oname='mht4_17.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 17,';
    y=-20:0.5:60; y=y';
  case 'g4_18'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_18.nc';
    oname='mht4_18.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 18,';
    y=-20:0.5:60; y=y';
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    oname='mht4_19_01.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 19_01,';
    y=-20:0.5:60; y=y';
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    oname='mht4_20.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 20,';
    y=-20:0.5:60; y=y';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    oname='mht4_22_01.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 22,';
    y=-20:0.5:60; y=y';
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    oname='mht4_23.dat';
    Tindex=1:1:60;
    mgrid='Damee #4: Run 23,';
    y=-20:0.5:60; y=y';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    oname='mht4_24.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 24,';
    y=-20:0.5:60; y=y';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    oname='mht4_24.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 25,';
    y=-20:0.5:60; y=y';
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    oname='mht4_26.dat';
    Tindex=1:1:60;
    mgrid='Damee #4: Run 26,'; 
    y=-20:0.5:60; y=y';
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    oname='mht4_27.dat';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 27,';
    y=-20:0.5:60; y=y';
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    oname='mht4_28.dat';
    Tindex=1:1:103;
    mgrid='Damee #4: Run 28,';
    y=-20:0.5:60; y=y';
  case 'g5_03'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    oname='mht5_03.dat';
    Tindex=1:1:120;
    mgrid='Damee #5: Run 03,';
    y=9:0.5:47; y=y';
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    oname='mht5_04.dat';
    Tindex=1:1:120;
    mgrid='Damee #5: Run 04,';
    y=9:0.5:47; y=y';
  case 'g6_03'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    fname='/comop/arango/Damee/grid6/damee6_avg_03.nc';
    oname='mht6_01.dat';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 03,';
    y=9:0.5:47; y=y';
  case 'g6_04'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    fname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    oname='mht6_04.dat';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 04,';
    y=9:0.5:47; y=y';
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    oname='mht6_05.dat';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 05,';
    y=9:0.5:47; y=y';
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/coamps/grid6/damee6_avg_06.nc';
    oname='mht6_06.dat';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 06,';
    y=9:0.5:47; y=y';
  case 'g7_01'
    gname='/d15/arango/scrum/Damee/grid7/damee7_grid_a.nc';
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    oname='mht7_01.dat';
    Tindex=1:1:48;
    mgrid='Damee #7: Run 01,';
    y=-20:0.5:60; y=y';
  case 'g7_04'
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee7_avg_04_04.nc';
    oname='mht7_04.dat';
    Tindex=1:1:34;
    mgrid='Damee #7: Run 04,';
    y=-20:0.5:60; y=y';
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    cname='/comop/arango/benchmark/data/damee7_Lclm_b.nc';
    Tindex=1:1:36;
    mgrid='Damee #7: Run 05,';
    y=-20:0.5:60; y=y';
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    oname='mht9_01.dat';
    Tindex=1:1:36;
    mgrid='Damee #9: Run 01,';
    y=-20:0.5:60; y=y';
end,

nrec=length(Tindex);

%--------------------------------------------------------------------------
% Compute meridional heat transport.
%--------------------------------------------------------------------------

for n=1:nrec,
  Trec=Tindex(n);
  [day,lat,f]=heat(gname,fname,Trec);
  time(n)=day;
  H(:,n)=f';
end,

%--------------------------------------------------------------------------
% Interpolate to specified rewuested DAMEE latitudes.
%--------------------------------------------------------------------------

[Jm,Nm]=size(H);

for n=1:nrec,
  wrk=reshape(H(:,n),1,Jm);
  Hint(:,n)=interp1(lat,wrk,y,method);
end,

[Jm,Nm]=size(Hint);

Ti=time(ones([1 Jm]),:)./360;
Xi=y(:,ones([1 Nm]));

%--------------------------------------------------------------------------
% Get requested DAMEE time averages.
%--------------------------------------------------------------------------

for n=1:13,
  switch (n)
    case 1      % Annual
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,26:1:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,25:1:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,68:1:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,13:1:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,1:1:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,1:1:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,1:1:36)')';
      else,
        Hout(:,n)=mean(Hint(:,85:1:120)')';
      end,
    case 2      % January
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,36:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,36:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,72:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,24:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,10:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,12:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,12:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,96:12:120)')';
      end,
    case 3      % February
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,37:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,25:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,73:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,13:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,11:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,1:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,1:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,85:12:120)')';
      end,
    case 4      % March
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,27:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,26:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,74:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,14:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,11:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,2:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,2:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,86:12:120)')';
      end,
    case 5      % April
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,27:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,27:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,75:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,15:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,1:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,3:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,3:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,87:12:120)')';
      end,
    case 6      % May
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,28:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,28:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,76:12:103)')';
      elseif (damee(1:5) == 'g4_23')
        Hout(:,n)=mean(Hint(:,26:1:60)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,16:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,2:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,4:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,4:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,88:12:120)')';
      end,
    case 7      % June
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,29:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,29:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,77:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,17:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,3:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,5:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,5:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,89:12:120)')';
      end,
    case 8      % July
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,30:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,30:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,78:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,18:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,4:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,6:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,6:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,90:12:120)')';
      end,
    case 9      % August
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,31:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,31:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,79:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,19:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,5:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,7:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,7:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,91:12:120)')';
      end,
    case 10     % September
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,32:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,32:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,68:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,20:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,6:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,8:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,8:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,92:12:120)')';
      end,
    case 11     % October
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,33:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,33:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,69:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,21:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,7:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,9:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,9:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,93:12:120)')';
      end,
    case 12     % November
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,34:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,34:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,70:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,22:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,8:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,10:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,10:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,94:12:120)')';
      end,
    case 13     % December
      if (damee(1:5) == 'g4_04'),
        Hout(:,n)=mean(Hint(:,35:12:61)')';
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Hout(:,n)=mean(Hint(:,35:12:60)')';
      elseif (damee(1:5) == 'g4_28')
        Hout(:,n)=mean(Hint(:,71:12:103)')';
      elseif (damee(1:5) == 'g7_01')
        Hout(:,n)=mean(Hint(:,23:12:48)')';
      elseif (damee(1:5) == 'g7_04')
        Hout(:,n)=mean(Hint(:,9:12:34)')';
      elseif (damee(1:5) == 'g7_05')
        Hout(:,n)=mean(Hint(:,11:12:36)')';
      elseif (damee(1:5) == 'g9_01')
        Hout(:,n)=mean(Hint(:,11:12:36)')';
      else,
        Hout(:,n)=mean(Hint(:,95:12:120)')';
      end,
  end,
end,

[Jm,Nm]=size(Hout);

Tout=time(ones([1 Jm]),:)./360;
Xout=y(:,ones([1 Nm]));

save mht.mat Hout Xout Hint Ti Xi

%--------------------------------------------------------------------------
%  Plot meridional heat transport.
%--------------------------------------------------------------------------

if ( IPLOT ),

  figure;
  Hmin=min(min(Hint)); Hmax=max(max(Hint));
  [c,h]=contourf(Xi,Ti,Hint,20); colorbar;
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0.2 0.1 0.6 0.8]);
  title(strcat(mgrid,'Meridional Heat transport (PW)'));
  xlabel({['Latitude'],['Min = ',num2str(Hmin),'  Max = ',num2str(Hmax)]});
  ylabel('Time  (years)');
  print -dpsc mht.ps

  figure;
  plot(Xout(:,1),Hout(:,1));
  grid on; hold on;
  errorbar(xb,yb,eb,'s');
  title(strcat(mgrid,' Annual Mean Meridional Heat transport (PW)'));
  xlabel('Latitude'); ylabel('PectaWatts');
  set(gca,'Xlim',[-Inf Inf],'Ylim',[0 2]);
  print -dpsc -append mht.ps

  figure;
  plot(Xout(:,2:13),Hout(:,2:13));
  grid on; hold on;
  errorbar(xb,yb,eb,'s');
  title(strcat(mgrid,' Monthly Mean Meridional Heat transport (PW)'));
  xlabel('Latitude'); ylabel('PectaWatts');
  set(gca,'Xlim',[-Inf Inf],'Ylim',[0 2]);
  print -dpsc -append mht.ps

end,

if ( IPLOT2 ),

  figure;
  plot(Xout(:,1),H19(:,1),Xout(:,1),H22(:,1), ...
       Xout(:,1),H23(:,1),Xout(:,1),H24(:,1));
  legend('r19','r22','r23','r24',1)
  grid on; hold on;
  errorbar(xb,yb,eb,'s');
  title(strcat(mgrid,' Annual Mean Meridional Heat transport (PW)'));
  xlabel('Latitude'); ylabel('PectaWatts');
  set(gca,'Xlim',[-Inf Inf],'Ylim',[0.4 1.6]);

  figure;
  plot(Xout(:,2),H19(:,2),Xout(:,2),H22(:,2), ...
       Xout(:,2),H23(:,2),Xout(:,2),H24(:,2));
  legend('r19','r22','r23','r24',1)
  grid on; hold on;
  errorbar(xb,yb,eb,'s');
  title(strcat(mgrid,' January Mean Meridional Heat transport (PW)'));
  xlabel('Latitude'); ylabel('PectaWatts');
  set(gca,'Xlim',[-Inf Inf],'Ylim',[0.4 1.6]);

  figure;
  plot(Xout(:,9),H19(:,9),Xout(:,9),H22(:,9), ...
       Xout(:,9),H23(:,9),Xout(:,9),H24(:,9));
  legend('r19','r22','r23','r24',1)
  grid on; hold on;
  errorbar(xb,yb,eb,'s');
  title(strcat(mgrid,' August Mean Meridional Heat transport (PW)'));
  xlabel('Latitude'); ylabel('PectaWatts');
  set(gca,'Xlim',[-Inf Inf],'Ylim',[0.4 1.6]);

end,

%--------------------------------------------------------------------------
%  Write out meridional heat transport.
%--------------------------------------------------------------------------

if ( IWRITE ),
  fid=fopen(oname,'w');
  if (fid < 0);
    error(['Cannot create ' oname '.']);
  end,

  [Im,Nm]=size(Hout);
  fprintf(fid,'%i %i\n',Im,Nm);
  fprintf(fid,'%6.1f',y);
  fprintf(fid,'\n');
  fprintf(fid,'%6.3f',Hout);
  fprintf(fid,'\n');

  fclose(fid);
end,
