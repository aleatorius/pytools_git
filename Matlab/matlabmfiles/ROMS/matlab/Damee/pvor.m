function [Dat]=pvor(gname,fname,tindex,zref,Llon,Rlon,Blat,Tlat,delta);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [Dat]=pvor(gname,fname,tindex,zref,Llon,Rlon,Blat,Tlat,delta)    %
%                                                                           %
% This function computes potential density and potential vorticity sections %
% averaged over specified horizontal region.                                %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%                  If no grid file, put field file name instead.            %
%    fname       Field NetCDF file name (character string).                 %
%    tindex      Time records to process (integer; scalar or vector).       %
%                  If tindex is a vector, the data will averaged over       %
%                  requested time records.                                  %
%    zref        Potential density reference depth (m).
%    Llon        Left corner longitude (East values are negative).          %
%    Rlon        Right corner longitude (East values are negative).         %
%    Blat        Bottom corner latitude (South values are negative).        %
%    Tlat        Top corner latitude (South values are negative).           %
%    delta       Horizontal grid spacing to interpolate (degrees).          %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Dat         Potential vorticity data (structure array).                %
%                  Dat.rho0:  reference density (kg/m3).                    %
%                  Dat.xref:  reference depth (m).                          %
%                  Dat.pv:    potential vorticity (1/m/s).                  %
%                  Dat.zpv:   depths of potential vorticity (m).            %
%                  Dat.ypv:   latitudes of potential vorticity (degrees).   %
%                  Dat.rho:   potential density (kg/m3).                    %
%                  Dat.zrho:  depths of potential density (m)  .            %
%                  Dat.yrho:  latitudes of potential density (degrees).     %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

method='linear';

%----------------------------------------------------------------------------
% Read in grid data.
%----------------------------------------------------------------------------

% Compute vertical grid.  Assume zero free-surface and constant in time.

z_r=depths(fname,gname,1,0,0);

[Lp,Mp,N]=size(z_r);
L=Lp-1;
M=Mp-1;
Nm=N-1;
Np=N+1;

wrk=depths(fname,gname,5,0,0);
z_w=wrk(:,:,2:N);

clear wrk

% Read in Coriolis parameter.

f=nc_read(gname,'f');

% Read in horizontal positions.

lon=nc_read(gname,'lon_rho');
lat=nc_read(gname,'lat_rho');

% Read in Land/Sea mask.

mask=nc_read(gname,'mask_rho');

%----------------------------------------------------------------------------
% Read in temperature and salinity and average over number of requested
% records.  Then, compute density referenced to specified surface.
%----------------------------------------------------------------------------

nrec=length(tindex);

S=zeros([Lp Mp N]);
T=zeros([Lp Mp N]);

i=0;
for n=1:nrec,
  i=i+1;
  Trec=tindex(n);
  a=nc_read(fname,'temp',Trec);
  T=T+a;
  a=nc_read(fname,'salt',Trec);
  S=S+a;
end,
T=T./i;
S=S./i;

% Compute potential density.

Z=abs(z_r);
Zref=ones(size(T)).*abs(zref);

rho=sw_pden(S,T,Z,Zref);

clear a S T Z Zref

%----------------------------------------------------------------------------
%  Compute potential vorticity (1/m/s).
%----------------------------------------------------------------------------

rho0=1026.5;
Dat.rho0=rho0;
Dat.zref=zref;

for k=1:Nm,
  pv(:,:,k)=mask(:,:).*f(:,:).*(rho(:,:,k+1)-rho(:,:,k))./ ...
                               (z_r(:,:,k+1)-z_r(:,:,k));
end,
pv=pv./rho0;

%----------------------------------------------------------------------------
% Average potential density and potential vorticity over requested area.
%----------------------------------------------------------------------------

% Set grid to interpolate horizontally.

x=Llon:delta:Rlon; x=x';
Im=length(x);
y=Blat:delta:Tlat;
Jm=length(y);

Xi=x(:,ones([1 Jm]));
Yi=y(ones([1 Im]),:);

for k=1:Nm,
  F=reshape(pv(:,:,k),Lp,Mp);
  Fint(:,:,k)=interp2(lon',lat',F',Xi,Yi,method);
  F=reshape(z_w(:,:,k),Lp,Mp);
  Zint(:,:,k)=interp2(lon',lat',F',Xi,Yi,method);
end,

%  Average along x-direction.

y=y';

Dat.pv=reshape(mean(Fint,1),Jm,Nm);
Dat.zpv=reshape(mean(Zint,1),Jm,Nm);
Dat.ypv=y(:,ones([1 Nm]));

for k=1:N,
  F=reshape(rho(:,:,k),Lp,Mp);
  Fint(:,:,k)=interp2(lon',lat',F',Xi,Yi,method);
  F=reshape(z_r(:,:,k),Lp,Mp);
  Zint(:,:,k)=interp2(lon',lat',F',Xi,Yi,method);
end,

Dat.rho=reshape(mean(Fint,1),Jm,N);
Dat.zrho=reshape(mean(Zint,1),Jm,N);
Dat.yrho=y(:,ones([1 N]));

return
