% This script computes the transport through the Florida Straits.
%
% To append new data to "fst_all.mat" file use, for instant:
%
% T4_28=T; x4_28=x; save fst_all.mat T4_28 x4_28 -APPEND
%

IPLOT=1;
IWRITE=1;
damee='g6_05';

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    oname='fst4_04.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 04,';
  case 'g4_06'
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    oname='fst4_06.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 06,';
  case 'g4_07'
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    oname='fst4_07.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 07,';
  case 'g4_10.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    oname='fst4_10_01.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 10.01,';
  case 'g4_13.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    oname='fst4_13_01.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 13.01,';
  case 'g4_13.2'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    oname='fst4_13_02.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 13.02,';
  case 'g4_14'
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    oname='fst4_14.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 14,';
  case 'g4_15'
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    oname='fst4_15.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 15,';
  case 'g4_16'
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    oname='fst4_16.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 16,';
  case 'g4_17'
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    oname='fst4_17.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 17,';
  case 'g4_18'
    fname='/e2/arango/Damee/grid4/damee4_avg_18.nc';
    oname='fst4_18.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 18,';
  case 'g4_19.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    oname='fst4_19_01.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 19_01,';
  case 'g4_20'
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    oname='fst4_20.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 20,';
  case 'g4_22.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    oname='fst4_22_01.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 22,';
  case 'g4_23'
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    oname='fst4_23.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 23,';
  case 'g4_24'
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    oname='fst4_24.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 24 ';
  case 'g4_25'
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    oname='fst4_25.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    oname='fst4_26.dat';
    Jo=66; Istr=22; Iend=23;
    mgrid='Damee #4: Run 26,';
  case 'g4_27'
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    oname='fst4_27.dat';
    Jo=66; Istr=22; Iend=98;
    mgrid='Damee #4: Run 27,';
  case 'g4_28'
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    oname='fst4_28.dat';
    Jo=66; Istr=22; Iend=98;
    mgrid='Damee #4: Run 28,';
  case 'g5_03'
    fname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    oname='fst5_03.dat';
    Jo=25; Istr=22; Iend=23;
    mgrid='Damee #5: Run 03,';
  case 'g5_04'
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    oname='fst5_04.dat';
    Jo=25; Istr=22; Iend=23;
    mgrid='Damee #5: Run 04,';
  case 'g6_03'
    fname='/comop/arango/Damee/grid6/damee6_avg_03.nc';
    oname='fst6_03.dat';
    Jo=42; Istr=36; Iend=38;
    mgrid='Damee #6: Run 03,';
  case 'g6_04'
    fname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    oname='fst6_04.dat';
    Jo=42; Istr=36; Iend=38;
    mgrid='Damee #6: Run 04,';
  case 'g6_05'
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    oname='fst6_05.dat';
    Jo=42; Istr=36; Iend=38;
    mgrid='Damee #6: Run 05,';
  case 'g6_06'
    fname='/coamps/grid6/damee6_avg_06.nc';
    oname='fst6_06.dat';
    Jo=42; Istr=36; Iend=38;
    mgrid='Damee #6: Run 06,';
  case 'g7_01'
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    oname='fst7_01.dat';
    Jo=130; Istr=42; Iend=45;
    mgrid='Damee #7: Run 01,';
  case 'g7_05'
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    oname='fst7_04.dat';
    Jo=130; Istr=42; Iend=45;
    mgrid='Damee #7: Run 05,';
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    oname='fst9_01.dat';
    mgrid='Damee #9: Run 01,';
end,

%--------------------------------------------------------------------------
% Compute transport through Florida Straits
%--------------------------------------------------------------------------

[time,T]=FS_trans(fname,Jo,Istr,Iend);

%--------------------------------------------------------------------------  
%  Plot.
%--------------------------------------------------------------------------

if ( IPLOT ),
  figure
  x=time-15/360;
  plot(x,T);
  title(strcat(mgrid,' Transport Through the Florida Strait'));
  xlabel('Year');
  ylabel('Sverdrups');
  grid on;
  set(gca,'Xlim',[-Inf Inf]);

  figure
  plot(x,T);
  title(strcat(mgrid,' Transport Through the Florida Strait'));
  xlabel('Year');
  ylabel('Sverdrups');
  grid on;
  set(gca,'Xlim',[7 10],'Ylim',[10 45]);
  print -dpsc FStrans.ps
end,

%--------------------------------------------------------------------------  
%  Write out data.
%--------------------------------------------------------------------------

if ( IWRITE ),
  fid=fopen(oname,'w');
  if (fid < 0),
    error(['Cannot create ' oname4 '.'])
  end,
  month=1:1:36;
  trans=T(85:120);
  for n=1:length(month),
    fprintf(fid,'%4i %7.3f\n',month(n),trans(n));
  end,
  fclose(fid);
end,

