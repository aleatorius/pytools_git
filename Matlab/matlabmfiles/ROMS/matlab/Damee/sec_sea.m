%  Computes Damee seasonal cross-sections of temperature and salinity
%  averaged over the last three years of simulation.

global IPRINT

IPRINT=0;
IWRITE=1;
IPLOT=1;
damee='g6_06';

iflag=0;
if (IWRITE),
  iflag=1;
end,

ds=0.5;
nc=20;
cmap='default';

switch ( damee ),
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #4: Run 17,';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #4: Run 22,';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #4: Run 24,';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    mgrid='Damee #4: Run 26,'; 
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #4: Run 27,';
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #4: Run 28,';
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #5: Run 04,';
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #6: Run 05,';
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/coamps/grid6/damee6_avg_06.nc';
    spr=[86 87 88  98  99 100 110 111 112];
    sum=[89 90 91 101 102 103 113 114 115];
    aut=[92 93 94 104 105 106 116 117 118];
    win=[95 96 97 107 108 109 119 120  85];
    mgrid='Damee #6: Run 06,';
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    feb=49;
    may=52;
    aug=55;
    nov=58;
    mgrid='Damee #7: Run 05,';
end,

%--------------------------------------------------------------------------
% Extract sections along 55W.
%--------------------------------------------------------------------------

xsec=[-55.0 -55.0];
ysec=[10.0 47.0];

[x,z,Twin]=NA_sec(gname,fname,'temp',win,xsec,ysec,ds,iflag);
[x,z,Tspr]=NA_sec(gname,fname,'temp',spr,xsec,ysec,ds,iflag);
[x,z,Tsum]=NA_sec(gname,fname,'temp',sum,xsec,ysec,ds,iflag);
[x,z,Taut]=NA_sec(gname,fname,'temp',aut,xsec,ysec,ds,iflag);

[x,z,Swin]=NA_sec(gname,fname,'salt',win,xsec,ysec,ds,iflag);
[x,z,Sspr]=NA_sec(gname,fname,'salt',spr,xsec,ysec,ds,iflag);
[x,z,Ssum]=NA_sec(gname,fname,'salt',sum,xsec,ysec,ds,iflag);
[x,z,Saut]=NA_sec(gname,fname,'salt',aut,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Twin,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: Winter, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_sea55w.ps

  figure,
  contourf(x,z,Tspr,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: Spring, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea55w.ps

  figure,
  contourf(x,z,Tsum,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: Summer, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea55w.ps

  figure,
  contourf(x,z,Taut,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: Autumn, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea55w.ps

  figure,
  contourf(x,z,Swin,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: Winter, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea55w.ps

  figure,
  contourf(x,z,Sspr,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: Spring, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea55w.ps

  figure,
  contourf(x,z,Ssum,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: Summer, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea55w.ps

  figure,
  contourf(x,z,Saut,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: Autumn, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea55w.ps
end,

if (IWRITE),
  [Im,Km]=size(Twin);
  Xout=xsec(1)*ones([1 Im]);
  Yout=x(:,1);  
  sec_frmt(Xout,Yout,Twin,Swin,'ts55w.win');
  sec_frmt(Xout,Yout,Tspr,Sspr,'ts55w.spr');
  sec_frmt(Xout,Yout,Tsum,Ssum,'ts55w.sum');
  sec_frmt(Xout,Yout,Taut,Saut,'ts55w.aut');
end,
  

%--------------------------------------------------------------------------
% Extract sections from Cape Hateras to Bermuda.
%--------------------------------------------------------------------------

xsec=-76.0:0.5:-65.0; xsec=xsec';
ysec=[35.0 35.0 35.0 35.0 34.5 34.5 34.5 34.5 34.5 34.0 34.0 34.0 34.0 ...
      33.5 33.5 33.5 33.5 33.5 33.0 33.0 33.0 33.0 32.5];

[x,z,Twin]=NA_sec(gname,fname,'temp',win,xsec,ysec,ds,iflag);
[x,z,Tspr]=NA_sec(gname,fname,'temp',spr,xsec,ysec,ds,iflag);
[x,z,Tsum]=NA_sec(gname,fname,'temp',sum,xsec,ysec,ds,iflag);
[x,z,Taut]=NA_sec(gname,fname,'temp',aut,xsec,ysec,ds,iflag);

[x,z,Swin]=NA_sec(gname,fname,'salt',win,xsec,ysec,ds,iflag);
[x,z,Sspr]=NA_sec(gname,fname,'salt',spr,xsec,ysec,ds,iflag);
[x,z,Ssum]=NA_sec(gname,fname,'salt',sum,xsec,ysec,ds,iflag);
[x,z,Saut]=NA_sec(gname,fname,'salt',aut,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Twin,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: Winter, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_seaCHB.ps

  figure,
  contourf(x,z,Tspr,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: Spring, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_seaCHB.ps

  figure,
  contourf(x,z,Tsum,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: Summer, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_seaCHB.ps

  figure,
  contourf(x,z,Taut,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: Autumn, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_seaCHB.ps

  figure,
  contourf(x,z,Swin,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: Winter, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_seaCHB.ps

  figure,
  contourf(x,z,Sspr,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: Spring, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_seaCHB.ps

  figure,
  contourf(x,z,Ssum,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: Summer, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_seaCHB.ps

  figure,
  contourf(x,z,Saut,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: Autumn, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_seaCHB.ps
end,

if (IWRITE),
  Xout=xsec;
  Yout=ysec;
  sec_frmt(Xout,Yout,Twin,Swin,'tsCHB.win');
  sec_frmt(Xout,Yout,Tspr,Sspr,'tsCHB.spr');
  sec_frmt(Xout,Yout,Tsum,Ssum,'tsCHB.sum');
  sec_frmt(Xout,Yout,Taut,Saut,'tsCHB.aut');
end,

%--------------------------------------------------------------------------
% Extract sections along 27N.
%--------------------------------------------------------------------------

xsec=[-80.0 -10.0];
ysec=[27.0 27.0];

[x,z,Twin]=NA_sec(gname,fname,'temp',win,xsec,ysec,ds,iflag);
[x,z,Tspr]=NA_sec(gname,fname,'temp',spr,xsec,ysec,ds,iflag);
[x,z,Tsum]=NA_sec(gname,fname,'temp',sum,xsec,ysec,ds,iflag);
[x,z,Taut]=NA_sec(gname,fname,'temp',aut,xsec,ysec,ds,iflag);

[x,z,Swin]=NA_sec(gname,fname,'salt',win,xsec,ysec,ds,iflag);
[x,z,Sspr]=NA_sec(gname,fname,'salt',spr,xsec,ysec,ds,iflag);
[x,z,Ssum]=NA_sec(gname,fname,'salt',sum,xsec,ysec,ds,iflag);
[x,z,Saut]=NA_sec(gname,fname,'salt',aut,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Twin,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: Winter, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_sea27n.ps

  figure,
  contourf(x,z,Tspr,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: Spring, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea27n.ps

  figure,
  contourf(x,z,Tsum,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: Summer, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea27n.ps

  figure,
  contourf(x,z,Taut,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: Autumn, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea27n.ps

  figure,
  contourf(x,z,Swin,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: Winter, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea27n.ps

  figure,
  contourf(x,z,Sspr,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: Spring, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea27n.ps

  figure,
  contourf(x,z,Ssum,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: Summer, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea27n.ps

  figure,
  contourf(x,z,Saut,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: Autumn, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea27n.ps
end,

if (IWRITE),
  [Im,Km]=size(Twin);
  Xout=x(:,1);
  Yout=xsec(1).*ones([1 Im]);  
  sec_frmt(Xout,Yout,Twin,Swin,'ts27n.win');
  sec_frmt(Xout,Yout,Tspr,Sspr,'ts27n.spr');
  sec_frmt(Xout,Yout,Tsum,Ssum,'ts27n.sum');
  sec_frmt(Xout,Yout,Taut,Saut,'ts27n.aut');
end,

%--------------------------------------------------------------------------
% Extract sections along 35N.
%--------------------------------------------------------------------------

xsec=[-80.0 -5.0];
ysec=[35.0 35.0];

[x,z,Twin]=NA_sec(gname,fname,'temp',win,xsec,ysec,ds,iflag);
[x,z,Tspr]=NA_sec(gname,fname,'temp',spr,xsec,ysec,ds,iflag);
[x,z,Tsum]=NA_sec(gname,fname,'temp',sum,xsec,ysec,ds,iflag);
[x,z,Taut]=NA_sec(gname,fname,'temp',aut,xsec,ysec,ds,iflag);

[x,z,Swin]=NA_sec(gname,fname,'salt',win,xsec,ysec,ds,iflag);
[x,z,Sspr]=NA_sec(gname,fname,'salt',spr,xsec,ysec,ds,iflag);
[x,z,Ssum]=NA_sec(gname,fname,'salt',sum,xsec,ysec,ds,iflag);
[x,z,Saut]=NA_sec(gname,fname,'salt',aut,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Twin,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: Winter, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_sea35n.ps

  figure,
  contourf(x,z,Tspr,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: Spring, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea35n.ps

  figure,
  contourf(x,z,Tsum,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: Summer, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea35n.ps

  figure,
  contourf(x,z,Taut,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: Autumn, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea35n.ps

  figure,
  contourf(x,z,Swin,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: Winter, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea35n.ps

  figure,
  contourf(x,z,Sspr,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: Spring, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea35n.ps

  figure,
  contourf(x,z,Ssum,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: Summer, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea35n.ps

  figure,
  contourf(x,z,Saut,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: Autumn, Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_sea35n.ps
end,

if (IWRITE),
  [Im,Km]=size(Twin);
  Xout=x(:,1);
  Yout=xsec(1).*ones([1 Im]);  
  sec_frmt(Xout,Yout,Twin,Swin,'ts35n.win');
  sec_frmt(Xout,Yout,Tspr,Sspr,'ts35n.spr');
  sec_frmt(Xout,Yout,Tsum,Ssum,'ts35n.sum');
  sec_frmt(Xout,Yout,Taut,Saut,'ts35n.aut');
end,
