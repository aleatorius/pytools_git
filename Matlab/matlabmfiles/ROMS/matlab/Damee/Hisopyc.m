function [F]=Hisopyc(gname,fname,isoval,Tindex,Llon,Rlon,Blat,Tlat,delta);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [F]=Hisopyc(gname,fname,isoval,Tindex,Llon,Rlon,Blat,Tlat,delta) %
%                                                                           %
% This function computes the depths of requested isopycnal(s) and then      %
% extracts requested section for a Hovmuller diagram.                       %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%    fname       Field NetCDF file name (character string).                 %
%    isoval      Isopycnal value to process (kg/m3).                        %
%    Tindex      Time records to process (integer).                         %
%    Llon        Left corner longitude (East values are negative).          %
%    Rlon        Right corner longitude (East values are negative).         %
%    Blat        Bottom corner latitude (South values are negative).        %
%    Tlat        Top corner latitude (South values are negative).           %
%    delta       Horizontal grid spacing to interpolate (degrees).          %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    F           Structure array:                                           %
%                  F.x  =>   Longitude or Latitude.                         %
%                  F.y  =>   Time.                                          %
%                  F.z  =>   Isopycnal(s) depths (depth,isoval,time).       %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

method='linear';

%----------------------------------------------------------------------------
%  Set equation of state expansion coefficients.
%----------------------------------------------------------------------------

A00=+19092.56D0;  A01=+209.8925D0;   A02=-3.041638D0;   A03=-1.852732D-3;
A04=-1.361629D-5; B00=+104.4077D0;   B01=-6.500517D0;   B02=+0.1553190D0;
B03=+2.326469D-4; D00=-5.587545D0;   D01=+0.7390729D0;  D02=-1.909078D-2;
E00=+4.721788D-1; E01=+1.028859D-2;  E02=-2.512549D-4;  E03=-5.939910D-7;
F00=-1.571896D-2; F01=-2.598241D-4;  F02=+7.267926D-6;  G00=+2.042967D-3;
G01=+1.045941D-5; G02=-5.782165D-10; G03=+1.296821D-7;  H00=-2.595994D-7;
H01=-1.248266D-9; H02=-3.508914D-9;  Q00=+999.842594D0; Q01=+6.793952D-2;
Q02=-9.095290D-3; Q03=+1.001685D-4;  Q04=-1.120083D-6;  Q05=+6.536332D-9;
U00=+0.824493D0;  U01=-4.08990D-3;   U02=+7.64380D-5;   U03=-8.24670D-7;
U04=+5.38750D-9;  V00=-5.72466D-3;   V01=+1.02270D-4;   V02=-1.65460D-6;
W00=+4.8314D-4; 

%--------------------------------------------------------------------------
%  Get longitude and latitude grid positions.
%--------------------------------------------------------------------------

rlon=nc_read(gname,'lon_rho');
rlat=nc_read(gname,'lat_rho');

rmask=nc_read(gname,'mask_rho');

%--------------------------------------------------------------------------
%  Set depths at RHO-points
%--------------------------------------------------------------------------

Zr=depths(fname,gname,1,0,0);

[Lp,Mp,N]=size(Zr);

%--------------------------------------------------------------------------
%  Set section coordinates.
%--------------------------------------------------------------------------

if (Llon == Rlon),
  Yi=Blat:delta:Tlat;  Yi=Yi';
  Xi=ones(size(Yi))*Llon;
  F.x=Yi;
elseif (Blat == Tlat),
  Xi=Llon:delta:Rlon;  Xi=Xi';
  Yi=ones(size(Xi))*Blat;
  F.x=Xi;
end,

%--------------------------------------------------------------------------
%  Inquire name of time variable.
%--------------------------------------------------------------------------

[vname,nvars]=nc_vname(fname);

for n=1:nvars,
  name=vname(n,:);
  if (~isempty(findstr(name,'_time'))),
    tname=vname(n,:);
    break;
  end,
end,

%---------------------------------------------------------------------------
% Compute depth of Isopycnal(s).
%---------------------------------------------------------------------------

Nrec=length(Tindex);
Niso=length(isoval);

for n=1:Nrec,

  Trec=Tindex(n);
  
  disp(['Processing time record: ', num2str(n,'%3.3i')]);

% Read in temperature and salinity.

  T=nc_read(fname,'temp',Trec);
  S=nc_read(fname,'salt',Trec);

% Read in time.

  F.y(n)=nc_read(fname,tname,Trec);

% Compute Sigma-t referenced to the surface.

  sqrtS=sqrt(S);

  sig_t = Q00 + Q01.*T + Q02.*T.^2 + Q03.*T.^3 + Q04.*T.^4 + Q05.*T.^5 + ...
          U00.*S + U01.*S.*T + U02.*S.*T.^2 + U03.*S.*T.^3 + U04.*S.*T.^4 + ...
          V00.*S.*sqrtS + V01.*S.*sqrtS.*T + V02.*S.*sqrtS.*T.^2 + ...
          W00.*S.^2;

  sig_t=sig_t-1000;

  clear T S sqrtS

% Compute depths of requested isopycnals.

  Ziso=ones([Lp Mp Niso]).*NaN;

  for j=1:Mp,
    for i=1:Lp,
      if (rmask(i,j) == 1),
        z=reshape(Zr(i,j,:),1,N);
        f=reshape(sig_t(i,j,:),1,N);
        Ziso(i,j,:)=iso_interp(f,z,isoval,0);
      end,
    end,
  end,

  clear f z sig_t

% Extract requested section via interpolation.

  for m=1:Niso,
    z=Ziso(:,:,m);
    f=interp2(rlon',rlat',z',Xi,Yi,method);
    F.z(:,m,n)=f;
  end,

  clear Ziso f z

end,

return
