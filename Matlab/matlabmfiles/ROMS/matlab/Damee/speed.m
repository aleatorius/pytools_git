function [plon,plat,Vmag]=speed(fname,gname,level,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [plon,plat,Vmag]=speed(fname,gname,level,tindex);                %
%                                                                           %
% This function the velocity magnitude for the requested depth level and    %
% time record.                                                              %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%    fname       Field NetCDF file name (character string).                 %
%    level       Depth level (integer).                                     %
%    tindex      Time records to process (integer; scalar or vector).       %
%                  If tindex is a vector, the data will averaged            %
%                  over all requested time records.                         %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    plon        Longitude (matrix).                                        %
%    plat        Latitude (matrix).                                         %
%    Vmag        Velocity magnitude (matrix).                               %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Deactivate printing information switch from "nc_read".
 
global IPRINT
 
IPRINT=0;

%---------------------------------------------------------------------------
%  Read in locations at PSI-points
%---------------------------------------------------------------------------

[dnames,dsizes,igrid]=nc_vinfo(fname,'u');

nrec=dsizes(1);
N=dsizes(2);
Mp=dsizes(3);
L=dsizes(4);
Lp=L+1;
M=Mp-1;

if (level < 0 | level > N),
  error(['SPEED: unavailable level = ',num2str(level)]);
end,

plon=nc_read(gname,'lon_psi');
plat=nc_read(gname,'lat_psi');

%---------------------------------------------------------------------------
%  Read in surface velocity.
%---------------------------------------------------------------------------

istr=findstr(fname,'.nc');
if (~isempty(istr)),
  file=fname(1:istr-1);
end,

stride=[1 1 1 1];
order=1;
missv=2;
spval=0;

nrec=length(tindex);

if (nrec > 1),
  i=0;
  u=zeros([L Mp]);
  v=zeros([Lp M]);
  for m=1:nrec,
    i=i+1;
    trec=tindex(m);
    start=[trec N -1 -1];
    count=[trec N -1 -1];
    F=getcdf_batch(file,'u',start,count,stride,order,missv,spval);
    u=u+F;
    F=getcdf_batch(file,'v',start,count,stride,order,missv,spval);
    v=v+F;
  end,
  u=u./i;
  v=v./i;
else,
  start=[tindex N -1 -1];
  count=[tindex N -1 -1];
  u=getcdf_batch(file,'u',start,count,stride,order,missv,spval);
  v=getcdf_batch(file,'v',start,count,stride,order,missv,spval);
end,

%---------------------------------------------------------------------------
%  Compute surface speed at PSI-points.
%---------------------------------------------------------------------------

Vmag=sqrt((0.5.*(u(:,1:M)+u(:,2:Mp))).^2 + ...
          (0.5.*(v(1:L,:)+v(2:Lp,:))).^2);

return
