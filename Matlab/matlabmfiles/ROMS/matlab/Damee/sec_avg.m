%  Computes Damee cross-section of temperature and salinity averaged
%  over the last three years of simulation.

global IPRINT

IPRINT=0;
IWRITE=1;
IPLOT=1;
damee=6;
iflag=0;
if (IWRITE),
  iflag=1;
end,

ds=0.5;
nc=20;
cmap='default';

switch ( damee ),
  case 3
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Tindex=25:1:60;
    grid='Damee #4a: ';
  case 4
    fname='/e1/arango/Damee/grid4/damee4_avg_06.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid4/damee4_levfeb_a.nc';
    Tindex=85:1:120;
    grid='Damee #4b: ';
  case 5
    fname='/d15/arango/scrum/Damee/grid5/damee5_avg_01.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid5/damee5_levfeb_a.nc';
    Tindex=85:1:120;
    grid='Damee #5: ';
  case 6
    fname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    iname='/d15/arango/scrum/Damee/grid6/damee6_levfeb_a.nc';
    Tindex=85:1:120;
    grid='Damee #6: ';
  case 7
    gname='/d15/arango/scrum/Damee/grid7/damee7_grid_a.nc';
    fname='/e0/arango/Damee/grid7/damee7_avg_01a.nc';
    Tindex=1:1:36;
    grid='Damee #7: ';
end,

%--------------------------------------------------------------------------
% Extract sections along 55W.
%--------------------------------------------------------------------------

xsec=[-55.0 -55.0];
ysec=[10.0 47.0];

[x,z,Tavg]=NA_sec(gname,fname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Savg]=NA_sec(gname,fname,'salt',Tindex,xsec,ysec,ds,iflag);
[x,z,Tdif]=NA_secdif(gname,fname,iname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Sdif]=NA_secdif(gname,fname,iname,'salt',Tindex,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tavg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Temperature along 55W: Years 7-10 Average'));
  xlabel('Latitude'); ylabel('Depth');

  figure,
  contourf(x,z,Tdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Temperature along 55W: Initial - Average'));
  xlabel('Latitude'); ylabel('Depth');

  figure,
  contourf(x,z,Savg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Salinity along 55W: Years 7-10 Average'));
  xlabel('Latitude'); ylabel('Depth');

  figure,
  contourf(x,z,Sdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Salinity along 55W: Initial - Average'));
  xlabel('Latitude'); ylabel('Depth');
end,

if (IWRITE),
  [Im,Km]=size(Tavg);
  Xout=xsec(1)*ones([1 Im]);
  Yout=x(:,1);  
  sec_frmt(Xout,Yout,Tavg,Savg,'ts55w.avg');
  sec_frmt(Xout,Yout,Tdif,Sdif,'ts55w.dif');
end,

%--------------------------------------------------------------------------
% Extract sections from Cape Hateras to Bermuda.
%--------------------------------------------------------------------------

xsec=-76.0:0.5:-65.0; xsec=xsec';
ysec=[35.0 35.0 35.0 35.0 34.5 34.5 34.5 34.5 34.5 34.0 34.0 34.0 34.0 ...
      33.5 33.5 33.5 33.5 33.5 33.0 33.0 33.0 33.0 32.5];

[x,z,Tavg]=NA_sec(gname,fname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Savg]=NA_sec(gname,fname,'salt',Tindex,xsec,ysec,ds,iflag);
[x,z,Tdif]=NA_secdif(gname,fname,iname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Sdif]=NA_secdif(gname,fname,iname,'salt',Tindex,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tavg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Temperature Cape Hatteras/Bermuda: Years 7-10 Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Tdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Temperature Cape Hatteras/Bermuda: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Savg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Salinity Cape Hatteras/Bermuda: Years 7-10 Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Sdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Salinity Cape Hatteras/Bermuda: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
end,

if (IWRITE),
  Xout=xsec;
  Yout=ysec;
  sec_frmt(Xout,Yout,Tavg,Savg,'tsCHB.avg');
  sec_frmt(Xout,Yout,Tdif,Sdif,'tsCHB.dif');
end,

%--------------------------------------------------------------------------
% Extract sections along 27N.
%--------------------------------------------------------------------------

xsec=[-80.0 -10.0];
ysec=[27.0 27.0];

[x,z,Tavg]=NA_sec(gname,fname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Savg]=NA_sec(gname,fname,'salt',Tindex,xsec,ysec,ds,iflag);
[x,z,Tdif]=NA_secdif(gname,fname,iname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Sdif]=NA_secdif(gname,fname,iname,'salt',Tindex,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tavg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Temperature along 27N: Years 7-10 Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Tdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Temperature along 27N: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Savg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Salinity along 27N: Years 7-10 Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Sdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Salinity along 27N: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
end,

if (IWRITE),
  [Im,Km]=size(Tavg);
  Xout=x(:,1);
  Yout=xsec(1).*ones([1 Im]);  
  sec_frmt(Xout,Yout,Tavg,Savg,'ts27n.avg');
  sec_frmt(Xout,Yout,Tdif,Sdif,'ts27n.dif');
end,

%--------------------------------------------------------------------------
% Extract sections along 35N.
%--------------------------------------------------------------------------

xsec=[-80.0 -5.0];
ysec=[35.0 35.0];

[x,z,Tavg]=NA_sec(gname,fname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Savg]=NA_sec(gname,fname,'salt',Tindex,xsec,ysec,ds,iflag);
[x,z,Tdif]=NA_secdif(gname,fname,iname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Sdif]=NA_secdif(gname,fname,iname,'salt',Tindex,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tavg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Temperature along 35N: Years 7-10 Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Tdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Temperature along 35N: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Savg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Salinity along 35N: Years 7-10 Average'));
  xlabel('Longitude'); ylabel('Depth');

  figure,
  contourf(x,z,Sdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(grid,'Salinity along 35N: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
end,

if (IWRITE),
  [Im,Km]=size(Tavg);
  Xout=x(:,1);
  Yout=xsec(1).*ones([1 Im]);  
  sec_frmt(Xout,Yout,Tavg,Savg,'ts35n.avg');
  sec_frmt(Xout,Yout,Tdif,Sdif,'ts35n.dif');
end,
