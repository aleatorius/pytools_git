function [X,Y,Z,Favg]=average(gname,fname,vname,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [X,Y,Z,Favg]=average(gname,fname,vname,tindex);                  %
%                                                                           %
% This script computes the time average fields over the specified time      %
% records.                                                                  %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%    fname       Field NetCDF file name (character string).                 %
%    vname       NetCDF variable name to process (character string).        %
%    tindex      Time records to process (integer; vector).                 %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    X           X-positions (matrix).                                      %
%    Y           Y-positions (matrix).                                      %
%    Z           Z-positions (array).                                       %
%    Favg        Averaged Field (array).                                    %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Deactivate printing information switch from "nc_read".
 
global IPRINT
 
IPRINT=0;

%----------------------------------------------------------------------------
% Determine positions and Land/Sea masking variable names.
%----------------------------------------------------------------------------

[dnames,dsizes,igrid]=nc_vinfo(fname,vname);

switch ( igrid ),
  case 1
    Xname='lon_rho';
    Yname='lat_rho';
  case 2
    Xname='lon_psi';
    Yname='lat_psi';
  case 3
    Xname='lon_u';
    Yname='lat_u';
  case 4
    Xname='lon_v';
    Yname='lat_v';
  case 5
    Xname='lon_rho';
    Yname='lat_rho';
end,
 
%----------------------------------------------------------------------------
% Read in variable positions.
%----------------------------------------------------------------------------
 
X=nc_read(gname,Xname);
Y=nc_read(gname,Yname);
 
%----------------------------------------------------------------------------
% Compute grid depths.
%----------------------------------------------------------------------------
 
Z=depths(fname,gname,igrid,0,0);
[Im Jm Km]=size(Z);

Z(:,:,Km)=zeros([Im Jm]);

%----------------------------------------------------------------------------
% Read in field(s) and average over number of requested records.
%----------------------------------------------------------------------------

nrec=length(tindex);

if (length(dsizes) == 4),
  Favg=zeros([Im Jm Km]);
elseif (length(dsizes)== 3),
  Favg=zeros([Im Jm]);
end,

i=0;
for n=1:nrec
  i=i+1;
  Trec=tindex(n);
  a=nc_read(fname,vname,Trec);
  Favg=Favg+a;
end,
Favg=Favg./i;

return
