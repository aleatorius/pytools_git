function [lon,time,T]=NS_trans(Fname,Tindex,Jo,Istr,Iend);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [lon,time,T]=NS_trans(Fname,Tindex,Jo,Istr,Iend);                %
%                                                                           %
% This function computes time-series of cumulative North-South transport    %
% at latitude index Jo between longitude indices (Istr,Iend).               %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Fname       Data NetCDF file name (character string).                  %
%    Tindex      time-indice(s) to process (scalar or vector).              %
%    Jo          V-point latitude index to process.                         %
%    Istr        Starting longitude index.                                  %
%    Jstr        Ending longitude index.                                    %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    lon         longitude (vector)                                         %
%    time        time (vector; years).                                      %
%    T           Transport (Sv) as function of (lon,time).                  %
%                                                                           %
% North Atlantic Grids:  Transport at 27N                                   %
%                                                                           %
%    Damee #4    Jo=66, Istr=22, Iend=98                                    %
%    Damee #5    Jo=25, Istr=22, Iend=98                                    %
%    Damee #6    Jo=42, Istr=36, Iend=                                      %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

% Read in metrics and total depth.

pm=nc_read(Fname,'pm');
h=nc_read(Fname,'h');

rlon=nc_read(Fname,'lon_rho');
lon=rlon(Istr:Iend,Jo);

% Read in model time (year)

[vname,nvars]=nc_vname(Fname);
for n=1:nvars,
  name=deblank(vname(n,:));
  switch name
    case 'scrum_time'
      Vname.time=name;
    case 'ocean_time'
      Vname.time=name;
  end,
end,

time_sec=nc_read(Fname,Vname.time);
time_day=time_sec./86400;
time_ann=time_day./360;

% Compute transport (Sv).

[Ntime]=length(Tindex);
scale=1.0e-6;

for n=1:Ntime,
  rec=Tindex(n);
  vbar=nc_read(Fname,'vbar',rec);
  zeta=nc_read(Fname,'zeta',rec);
  time(n)=time_ann(rec);
  Tsum=0;
  m=0;
  for i=Istr:Iend,
    dx=0.5*(pm(i,Jo)+pm(i+1,Jo));
    D=h(i,Jo)+zeta(i,Jo);
    Tsum=Tsum+D*vbar(i,Jo)/dx;
    m=m+1;
    T(m,n)=Tsum;
  end,
end,

T=scale.*T;

return

