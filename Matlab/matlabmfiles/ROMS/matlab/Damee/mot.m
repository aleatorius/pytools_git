%  This function computes zonally averages meridional overturning
%  streamfunction.

global IPRINT

IPRINT=0;
IWRITE=1;
IPLOT=1;
COLOR=1;
damee='g6_05';
method='linear';

%  Set color palette.

Pmin=-20;
Pmax=20;
nc=20;
Cval=Pmin:(Pmax-Pmin)/nc:Pmax;
Lval=[-2 0 2 10];

if ( COLOR ),
  Cmap='default';
else,
  dark=0.2;
  light=0.9;
  step=(dark-light)/(nc-1);
  grey=light:step:dark;
  Cmap=[grey' grey' grey'];
end,

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_04.dat';
    mgrid='Damee #4: Run 04,';
  case 'g4_06'
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_06.dat';
    mgrid='Damee #4: Run 06,';
  case 'g4_07'
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_07.dat';
    mgrid='Damee #4: Run 07,';
  case 'g4_10.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_10_01.dat';
    mgrid='Damee #4: Run 10.01,';
  case 'g4_13.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_13_01.dat';
    mgrid='Damee #4: Run 13.01,';
  case 'g4_13.2'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_13_02.dat';
    mgrid='Damee #4: Run 13.02,';
  case 'g4_14'
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_14.dat';
    mgrid='Damee #4: Run 14,';
  case 'g4_15'
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_15.dat';
    mgrid='Damee #4: Run 15,';
  case 'g4_16'
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='mot4_16.dat';
    mgrid='Damee #4: Run 16,';
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    mgrid='Damee #4: Run 17,';
  case 'g4_18'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_18.nc';
    mgrid='Damee #4: Run 18,';
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    mgrid='Damee #4: Run 19_01,';
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    mgrid='Damee #4: Run 20,';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    mgrid='Damee #4: Run 22,';
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    mgrid='Damee #4: Run 23,';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    mgrid='Damee #4: Run 24,';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    mgrid='Damee #4: Run 26,';
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    mgrid='Damee #4: Run 27,';
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    mgrid='Damee #4: Run 28,';
  case 'g5_03'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    oname='mot5_03.dat';
    mgrid='Damee #5: Run 03,';
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    oname='mot5_04.dat';
    mgrid='Damee #5: Run 04,';
  case 'g6_03'
    fname='/comop/arango/Damee/grid6/damee6_avg_03.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='mot6_03.dat';
    mgrid='Damee #6: Run 03,';
  case 'g6_04'
    fname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='mot6_04.dat';
    mgrid='Damee #6: Run 04,';
  case 'g6_05'
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    oname='mot6_05.dat';
    mgrid='Damee #6: Run 05,';
  case 'g6_06'
    fname='/coamps/grid6/damee6_avg_06.nc';
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    oname='mot6_06.dat';
    mgrid='Damee #6: Run 06,';
  case 'g7_01'
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc/damee7_grid_a.nc';
    oname='mot7_01.dat';
    mgrid='Damee #7: Run 01,';
  case 'g7_04'
    fname='/comop/arango/Damee/grid4/damee7_avg_04_04.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    oname='mot7_04.dat';
    mgrid='Damee #7: Run 04,';
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    oname='mot7_04.dat';
    mgrid='Damee #7: Run 05,';
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    oname='mot9_01.dat';
    mgrid='Damee #9: Run 01,';
end,

if (IWRITE),
  fid=fopen(oname,'w');
  if (fid < 0),
    error(['Cannot create ' oname '.'])
  end,
end,

%----------------------------------------------------------------------------
% Compute zonally averaged meridional overturning streamfunction.
%----------------------------------------------------------------------------

for n=1:13,

  switch (n),
    case 1
      if (damee(1:5) == 'g4_04'),
        Tindex=26:1:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=25:1:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=68:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=13:1:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex=1:1:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex=1:1:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex=1:1:36;
      else,
        Tindex=85:1:120;
      end,
      Text=' Annual Meridional Overturning Streamfunction (Sv)';
    case 2
      if (damee(1:5) == 'g4_04'),
        Tindex=36:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=36:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=72:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=24:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex=10:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex=12:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex=12:12:36;
      else,
        Tindex=96:12:120;
      end,
      Text=' January Meridional Overturning Streamfunction (Sv)';
    case 3
      if (damee(1:5) == 'g4_04'),
        Tindex=37:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=25:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=73:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=13:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex=11:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 1:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 1:12:36;
      else,
        Tindex=85:12:120;
      end,
      Text=' February Meridional Overturning Streamfunction (Sv)';
    case 4
      if (damee(1:5) == 'g4_04'),
        Tindex=38:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=26:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=74:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=14:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex=12:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 2:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 2:12:36;
      else,
        Tindex=86:12:120;
      end,
      Text=' March Meridional Overturning Streamfunction (Sv)';
    case 5
      if (damee(1:5) == 'g4_04'),
        Tindex=39:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=27:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=75:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=15:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 1:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 3:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 3:12:36;
      else,
        Tindex=87:12:120;
      end,
      Text=' April Meridional Overturning Streamfunction (Sv)';
    case 6
      if (damee(1:5) == 'g4_04'),
        Tindex=40:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=28:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=76:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=16:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex=2:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 4:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 4:12:36;
      else,
        Tindex=88:12:120;
      end,
      Text=' May Meridional Overturning Streamfunction (Sv)';
    case 7
      if (damee(1:5) == 'g4_04'),
        Tindex=41:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=29:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=77:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=17:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 3:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 5:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 5:12:36;
      else,
        Tindex=89:12:120;
      end,
      Text=' June Meridional Overturning Streamfunction (Sv)';
    case 8
      if (damee(1:5) == 'g4_04'),
        Tindex=42:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=30:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=78:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=18:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 4:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 6:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 6:12:36;
      else,
        Tindex=90:12:120;
      end,
      Text=' July Meridional Overturning Streamfunction (Sv)';
    case 9
      if (damee(1:5) == 'g4_04'),
        Tindex=43:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=31:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=79:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=19:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 5:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 7:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 7:12:36;
      else,
        Tindex=91:12:120;
      end,
      Text=' August Meridional Overturning Streamfunction (Sv)';
    case 10
      if (damee(1:5) == 'g4_04'),
        Tindex=44:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=32:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=68:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=20:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 6:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 8:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 8:12:36;
      else,
        Tindex=92:12:120;
      end,
      Text=' September Meridional Overturning Streamfunction (Sv)';
    case 11
      if (damee(1:5) == 'g4_04'),
        Tindex=45:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=33:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=69:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=21:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 7:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex= 9:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex= 9:12:36;
      else,
        Tindex=93:12:120;
      end,
      Text=' October Meridional Overturning Streamfunction (Sv)';
    case 12
      if (damee(1:5) == 'g4_04'),
        Tindex=46:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=34:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=70:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=22:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 8:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex=10:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex=10:12:36;
      else,
        Tindex=94:12:120;
      end,
      Text=' November Meridional Overturning Streamfunction (Sv)';
    case 13
      if (damee(1:5) == 'g4_04'),
        Tindex=47:12:61;
      elseif (damee(1:5) == 'g4_16' | ...
              damee(1:5) == 'g4_23' | ...
              damee(1:5) == 'g4_26')
        Tindex=35:12:60;
      elseif (damee(1:5) == 'g4_28')
        Tindex=71:1:103;
      elseif (damee(1:5) == 'g7_01')
        Tindex=23:12:48;
      elseif (damee(1:5) == 'g7_04')
        Tindex= 9:12:34;
      elseif (damee(1:5) == 'g7_05')
        Tindex=11:12:36;
      elseif (damee(1:5) == 'g9_01')
        Tindex=11:12:36;
      else,
        Tindex=95:12:120;
      end,
      Text=' December Meridional Overturning Streamfunction (Sv)';
  end,

  [lat,z,P]=overturn(gname,fname,Tindex);

  [Jm,Km]=size(P);
  Y=lat(:,ones([1 Km]));
  Z=z(ones([1 Jm]),:);

%----------------------------------------------------------------------------
% Interpolate to requested DAMEE grid.
%----------------------------------------------------------------------------

  y=9:0.5:47; y=y';
% y=-25:0.5:60; y=y';
  Im=length(y);
  Yi=y(:,ones([1 Km]));
  Zi=z(ones([1 Im]),:);

  Pi=interp2(Y',Z',P',Yi,Zi,method);

%----------------------------------------------------------------------------
% Plot meridional overturning streamfunction.
%----------------------------------------------------------------------------

  if ( IPLOT ),
    figure(1);
    pmin=min(min(P)); pmax=max(max(P));
    [c,h]=contourf(Y,Z,P,Cval); clabel(c,h,Lval);
    colormap('default'); colorbar;
    title(strcat(mgrid,Text));
    xlabel(['Latitude     Tmin = ',num2str(pmin),'  Tmax = ',num2str(pmax)]);

    figure(2);
    pmin=min(min(Pi)); pmax=max(max(Pi));
    [c,h]=contourf(Yi,Zi,Pi,Cval); clabel(c,h,Lval);
    colormap(Cmap);

    title(strcat(mgrid,Text));
    xlabel(['Latitude,    Tmin = ',num2str(pmin),'  Tmax = ',num2str(pmax)]);
    if (COLOR),
      colorbar;
      if (n == 1),
        print -dpsc mot.ps
      else,
        print -dpsc -append mot.ps
      end,
    else,
      if (n == 1),
        print -dps mot.ps
      else,
        print -dps -append mot.ps
      end,
    end,
  end,

%----------------------------------------------------------------------------
% Write out data to intermediate section format.
%----------------------------------------------------------------------------

  if (IWRITE),
    fprintf(fid,'%i %i %i\n',n-1,Im,Km);
    fprintf(fid,'%7.1f',y);
    fprintf(fid,'\n');
    fprintf(fid,'%8.1f',z);
    fprintf(fid,'\n');
    fprintf(fid,'%7.3f',Pi);
    fprintf(fid,'\n');
  end,
end,

if (IWRITE),
  fclose(fid);
end,
