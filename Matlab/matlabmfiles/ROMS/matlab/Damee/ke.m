%  This function computes volume integrated kinetic energy in the
%  specified region.

global IPRINT

IPRINT=0;
IPLOT=1;
IPLOT2=0;
damee='g4_26';
interpolate=1;

if (interpolate),
  Llon=-97;
  Rlon=-10;
  Blat=9;
  Tlat=47;
  delta=0.5;
end,

RangeLab=[' Llon= ',num2str(Llon), ...
         ', Rlon= ',num2str(Rlon), ...
         ', Blat= ',num2str(Blat), ...
         ', Tlat= ',num2str(Tlat)];

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    Tindex=1:1:61;
    mgrid='Damee #4: Run 04,';
  case 'g4_06'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 06,';
  case 'g4_07'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 07,';
  case 'g4_10.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 10.01,';
  case 'g4_13.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 13.01,';
  case 'g4_13.2'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 13.02,';
  case 'g4_14'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 14,';
  case 'g4_15'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 15,';
  case 'g4_16'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    Tindex=1:1:60;
    mgrid='Damee #4: Run 16,';
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    Tindex=1:1:60;
    mgrid='Damee #4: Run 23,';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 24,';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    Tindex=1:1:60;
    mgrid='Damee #4: Run 26,';
  case 'g5_01'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/d15/arango/scrum/Damee/grid5/damee5_avg_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #5: Run 01,';
  case 'g6_01'
    gname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    fname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 01,';
  case 'g7_01'
    gname='/d15/arango/scrum/Damee/grid7/damee7_grid_a.nc';
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    Tindex=1:1:48;
    mgrid='Damee #7: Run 01,';
  case 'g7_04'
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee7_avg_04_04.nc';
    Tindex=1:1:34;
    mgrid='Damee #7: Run 04,';
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    Tindex=1:1:36;
    mgrid='Damee #7: Run 05,';
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    Tindex=1:1:36;
    mgrid='Damee #9: Run 01,';
end,

%--------------------------------------------------------------------------
% Compute volume kinetic energy (J/m2).
%--------------------------------------------------------------------------

if (interpolate),
  [KE]=vol_ke(gname,fname,Tindex,Llon,Rlon,Blat,Tlat,delta);
else,
  [KE]=vol_ke(gname,fname,Tindex);
end,

switch ( damee ),
  case ('g4_16' | 'g4_23' | 'g4_26')
    KE.time=KE.time./86400./360;
  otherwise
    KE.time=(KE.time-KE.time(1))./86400./360;
end,

%--------------------------------------------------------------------------
%  Plot time evolution of kinetic energy.
%--------------------------------------------------------------------------

if ( IPLOT ),

  figure;
  scale=0.01;
  plot(KE.time,KE.field.*scale,'k');
  grid on;
  set(gca,'xlim',[-Inf Inf]);
  title(strcat(mgrid,'  Bulk Kinetic Energy'));
  if (interpolate),
    xlabel({['time (years)'],[RangeLab]});
  else,
    xlabel('time (years)');
  end,
  ylabel('KE  (10^2  J/m^2)');

end,

if ( IPLOT2 ),

  figure;
  plot(KE4d.time,KE4d.field.*scale, ...
       KE4e.time,KE4e.field.*scale, ...
       KE4g.time,KE4g.field.*scale, ...
       KE4h.time,KE4h.field.*scale, ...
       KE4i.time,KE4i.field.*scale);
  legend ('a','b','c','d','e',2);
  grid on;
  set(gca,'xlim',[-Inf Inf]);
  title('Volume Averaged Kinetic Energy');
  if (interpolate),
    xlabel({['time (years)'],[RangeLab]});
  else,
    xlabel('time (years)');
  end,
  ylabel('KE  (10^2  J/m^2)');
  print -dpsc ke_vol.ps

end,
