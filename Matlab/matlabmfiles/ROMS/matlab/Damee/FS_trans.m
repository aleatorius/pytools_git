function [time,T]=FS_trans(fname,Jo,Istr,Iend);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [time,T]=FS_trans(fname,Jo,Istr,Iend);                           %
%                                                                           %
% This function computes the transport through the Florida Straits at       %
% 27N.                                                                      %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    dname       Data NetCDF file name (character string).                  %
%    Jo          Neareast V-point index to latitude 27N.                    %
%    Istr        Starting longitude index.                                  %
%    Jstr        Ending longitude index.                                    %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    time        time (vector; years).                                      %
%    T           Florida Transport (Sv).                                    %
%                                                                           %
% North Atlantic Grids:                                                     %
%                                                                           %
%    Damee #4    Jo=66, Istr=22, Iend=23                                    %
%    Damee #5    Jo=25, Istr=22, Iend=23                                    %
%    Damee #6    Jo=42, Istr=36, Iend=38                                    %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

% Read in metrics and total depth.

pm=nc_read(fname,'pm');
h=nc_read(fname,'h');

% Read in model time.

[vname,nvars]=nc_vname(fname);
for n=1:nvars,
  name=deblank(vname(n,:));
  switch name
    case 'scrum_time'
      Vname.time=name;
    case 'ocean_time'
      Vname.time=name;
  end,
end,

time_sec=nc_read(fname,Vname.time);
time=time_sec./86400;
time=time./360;
[Ntime]=length(time);

% Compute transport (Sv).

scale=1.0e-6;

for n=1:Ntime,
  vbar=nc_read(fname,'vbar',n);
  zeta=nc_read(fname,'zeta',n);
  sum=0;
  for i=Istr:Iend,
    dx=0.5*(pm(i,Jo)+pm(i+1,Jo));
    D=h(i,Jo)+zeta(i,Jo);
    sum=sum+D*vbar(i,Jo)/dx;
  end,
  T(n)=sum;
end,

T=scale.*T;

return

