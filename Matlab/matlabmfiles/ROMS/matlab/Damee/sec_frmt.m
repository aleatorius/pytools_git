function sec_frmt (x,y,T,S,fname)

%  This function saves data on an intemediate format which latter will be
%  translated to Ziv's section format.

spval=99.0;

fid=fopen(fname,'w');
if (fid < 0),
  error(['Cannot create ' fname '.'])
end,

%  Remove NaN from data and replace by "spval".

[Lm,Km]=size(T);

ind=find(isnan(T));
T(ind)=spval;
ind=find(isnan(S));
S(ind)=spval;

% Write out data.

fprintf(fid,'%i %i\n',Lm,Km);
fprintf(fid,'%7.1f',x);
fprintf(fid,'\n');
fprintf(fid,'%7.1f',y);
fprintf(fid,'\n');
fprintf(fid,'%7.3f',T);
fprintf(fid,'\n');
fprintf(fid,'%7.3f',S);
fprintf(fid,'\n');

fclose(fid);

return



