%  This function plot composite meridional heat transport for all
%  DAMEE paper experiments.

%  Read in heat transport data.

load mht24.mat     % E1  Grid 4, Run 24
load mht22.mat     % E2  Grid 4, Run 22
load mht26.mat     % E3  Grid 4, Run 26
load mht25.mat     % E4  Grid 4, Run 25
load mht03.mat     % E5  Grid 6, Run 03
load mht05.mat     % E6  Grid 7, Run 05
load mht17.mat     % E7  Grid 4, Run 17   2nd-order Centered
load mht28.mat     % E8  Grid 4, Run 28   4th-order centered
load mht27.mat     % E9  Grid 4, Run 27
load mht02.mat     % E10 Grid 5, Run 03

%  Extract annual mean.

x1=X24(:,1); y1=H24(:,1);
x2=X22(:,1); y2=H22(:,1);
x3=X26(:,1); y3=H26(:,1);
x4=X25(:,1); y4=H25(:,1);
x5=X03(:,1); y5=H03(:,1);
x6=X05(:,1); y6=H05(:,1);
x7=X17(:,1); y7=H17(:,1);
x8=X28(:,1); y8=H28(:,1);

x9=X27(:,1); y9=H27(:,1);
x10=X02(:,1); y10=H02(:,1);

%  Set error bars.

xb = [11 24 36 48];
yb = [1.39 1.08 1.01 .65];
eb = [.25 .25 .25 .25];

%  Set default color order.

Corder=[1.00 0.00 0.00; ...  % red
        0.00 1.00 1.00; ...  % cyan
        0.00 1.00 0.00; ...  % green
        0.00 0.00 1.00; ...  % blue
        0.00 0.00 0.00; ...  % black
        1.00 0.00 1.00; ...  % magenta
        1.00 0.75 0.00; ...  % orange
        1.00 0.50 0.25];     % pink

set(0,'DefaultAxesColorOrder',Corder);

%  Plot meridional heat transport.

figure;
plot(x1,y1, ...
     x2,y2, ...
     x3,y3, ...
     x4,y4, ...
     x8,y8, ...
     x6,y6, ...
     x7,y7, ...
     x8,y8);
legend('E1','E2','E3','E4','E5','E6','E7','E8',1)
grid on; hold on;
errorbar(xb,yb,eb,'ks');
xlabel('Latitude'); ylabel('Heat Transport (PW)');
set(gca,'Xlim',[9 49],'Ylim',[0.3 1.6]);

print -dpsc mht_all.ps
