% This script builds annual average for COADS data.
% and write it out into a NetCDF file

global IPRINT

gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
fname='/e2/arango/Damee/grid4/damee4_coads.nc';
oname='/e2/arango/Damee/grid4/damee4_coads_ann.nc';

Tindex=1:1:12;


%--------------------------------------------------------------------------
%  Compute averages.
%--------------------------------------------------------------------------

F=nc_read(fname,'sustr'); Favg=mean(F,3);
status=nc_write(oname,'sustr',Favg,1);
disp(['sustr : ',num2str(status)]);

F=nc_read(fname,'svstr'); Favg=mean(F,3);
status=nc_write(oname,'svstr',Favg,1);
disp(['svstr : ',num2str(status)]);

F=nc_read(fname,'shflux'); Favg=mean(F,3);
status=nc_write(oname,'shflux',Favg,1);
disp(['shflux : ',num2str(status)]);

F=nc_read(fname,'swflux'); Favg=mean(F,3);
status=nc_write(oname,'swflux',Favg,1);
disp(['swflux : ',num2str(status)]);

F=nc_read(fname,'SST'); Favg=mean(F,3);
status=nc_write(oname,'SST',Favg,1);
disp(['SST : ',num2str(status)]);

F=nc_read(fname,'dQdSST'); Favg=mean(F,3);
status=nc_write(oname,'dQdSST',Favg,1);
disp(['dQdSST : ',num2str(status)]);

F=nc_read(fname,'swrad'); Favg=mean(F,3);
status=nc_write(oname,'swrad',Favg,1);
disp(['swrad : ',num2str(status)]);

status=nc_write(oname,'sms_time',0,1);
disp(['sms_time : ',num2str(status)]);

status=nc_write(oname,'shf_time',0,1);
disp(['shf_time : ',num2str(status)]);

status=nc_write(oname,'swf_time',0,1);
disp(['swf_time : ',num2str(status)]);

status=nc_write(oname,'srf_time',0,1);
disp(['srf_time : ',num2str(status)]);

status=nc_write(oname,'sst_time',0,1);
disp(['sst_time : ',num2str(status)]);
