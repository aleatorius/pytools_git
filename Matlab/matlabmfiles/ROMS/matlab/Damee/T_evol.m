%  This function computes area integrated temperature and salinity,
%  at standard levels, and in the specified region.

global IPRINT

IPRINT=0;
IPLOT=1;
damee='g4_17';
interpolate=0;

Llon=-97;
Rlon=-10;
Blat=9;
Tlat=47;
delta=0.5;

RangeLab=[' Llon= ',num2str(Llon), ...
         ', Rlon= ',num2str(Rlon), ...
         ', Blat= ',num2str(Blat), ...
         ', Tlat= ',num2str(Tlat)];

switch ( damee ),
  case 'clima'
    gname='/e2/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Levitus,';
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 17,';
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 19_01,';
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 20,';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 22_01,';
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 23,';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 24,';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 25,';
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    Tindex=1:1:120;
    mgrid='Damee #4: Run 27,';
end,

%--------------------------------------------------------------------------
%  Compute area averaged temperature and salinity profiles.
%--------------------------------------------------------------------------

if (interpolate),
  [F]=T_evolution(gname,fname,Tindex,Llon,Rlon,Blat,Tlat);
else,
  [F]=T_evolution(gname,fname,Tindex);
end,

save T_evol.mat

%--------------------------------------------------------------------------
%  Plot time evolution of temperature and salinity.
%--------------------------------------------------------------------------

if (IPLOT),

  [Im Jm]=size(F.temp);

  dt=(F.time(2)-F.time(1))/2;
  time=(F.time-dt)./86400./360;

  X=time(ones([1 Im]),:); X=X';
  Y=F.z(:,ones([1 Jm])); Y=Y';
  T=F.temp; T=T';
  S=F.salt; S=S';

  [Im Jm]=size(T);

  figure;
  plot3(X,Y,T);
  grid on;
  xlabel('Time (years)');
  ylabel('Depth (m)');
  zlabel('Temperature (C)');
  print -dpsc T_evol.ps

  figure;
  plot3(X,Y,S);
  grid on;
  xlabel('Time (years)');
  ylabel('Depth (m)');
  zlabel('Salinity (PSU)');
  print -dpsc -append T_evol.ps

  figure;
  for n=1:Jm,
    Tval=T(:,n)-mean(T(:,n))-n/2;
    plot(X(:,n),Tval);
    if (n == 1), hold on; end,
    Tlab(n)=Tval(1);
    zlab(n)=Y(1,n);
  end,
  set(gca,'Ylim',[-19 1]);
  set(gca,'Xtick',[0:1:10],...
          'Ytick',fliplr(Tlab),...
          'Yticklabel',fliplr(zlab));
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0.2 0.1 0.6 0.8]);
  grid on;
  xlabel('Time  (years)')
  ylabel('Depth (m)')
  title('Area averaged Temperature');
  print -dpsc -append T_evol.ps

  figure;
  for n=1:Jm,
    Sval=S(:,n)-mean(S(:,n))-n/2;
    plot(X(:,n),Sval);
    if (n == 1), hold on; end,
    Slab(n)=Sval(1);
    zlab(n)=Y(1,n);
  end,
  set(gca,'Ylim',[-19 0]); 
  set(gca,'Xtick',[0:1:10],...
          'Ytick',fliplr(Slab),...
          'Yticklabel',fliplr(zlab));
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0.2 0.1 0.6 0.8]);
  grid on;
  xlabel('Time  (years)')
  ylabel('Depth (m)')
  title('Area averaged Salinity');
  print -dpsc -append T_evol.ps

end,
