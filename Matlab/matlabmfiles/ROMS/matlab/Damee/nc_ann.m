% This script builds averages for the last three years of simulation
% and write it out into a NetCDF file

global IPRINT

IPRINT=0;
damee='g4_27';

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e0/arango/Damee/grid4/damee4_yavg_04.nc';
    Tindex=85:1:120;
  case 'g4_06'
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e0/arango/Damee/grid4/damee4_yavg_06.nc';
    Tindex=85:1:120;
  case 'g4_07'
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e2/arango/Damee/grid4/damee4_yavg_07.nc';
    Tindex=85:1:120;
  case 'g4_10.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_10_01.nc';
    Tindex=85:1:120;
  case 'g4_13.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_13_01.nc';
    Tindex=85:1:120;
  case 'g4_13.2'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_13_02.nc';
    Tindex=85:1:120;
  case 'g4_14'
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_14.nc';
    Tindex=85:1:120;
  case 'g4_15'
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_15.nc';
    Tindex=85:1:120;
  case 'g4_16'
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_16.nc';
    Tindex=25:1:60;
  case 'g4_17'
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
%   oname='/comop/arango/Damee/grid4/damee4_yavg_17.nc';
    oname='/comop/arango/Damee/grid4/damee4_ann_17.nc';
    Tindex=85:1:120;
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_19_01.nc';
    Tindex=85:1:120;
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_20.nc';
    Tindex=85:1:120;
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_22_01.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_22_01.nc';
    Tindex=85:1:120;
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_23.nc';
    Tindex=25:1:60;
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_24.nc';
    Tindex=85:1:120;
  case 'g4_27'
    gname='/n0/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/coamps/grid4/damee4_avg_27_02.nc';
    oname='/coamps/grid4/damee4_ann_27_02.nc';
    Tindex=85:1:120;
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    oname='/comop/arango/Damee/grid4/damee4_ann_28.nc';
    Tindex=68:1:103;
  case 'g5_01'
    fname='/d15/arango/scrum/Damee/grid5/damee5_avg_01.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    oname='/e0/arango/Damee/grid5/damee5_yavg_01.nc';
    Tindex=85:1:120;
  case 'g5_04'
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    oname='/comop/arango/Damee/grid5/damee5_ann_04.nc';
    Tindex=85:1:120;
  case 'g6_01'
    fname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='/e0/arango/Damee/grid6/damee6_yavg_01.nc';
    Tindex=85:1:120;
  case 'g6_04'
    fname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='/comop/arango/Damee/grid6/damee6_ann_04.nc';
    Tindex=85:1:120;
  case 'g6_05'
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    oname='/n2/arango/Damee/grid6/damee6_ann_05.nc';
    Tindex=85:1:120;
  case 'g6_06'
    fname='/coamps/grid6/damee6_avg_06.nc';
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    oname='/coamps/grid6/damee6_ann_06.nc';
    Tindex=85:1:120;
  case 'g7_01'
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    oname='/e2/arango/Damee/grid7/damee7_yavg_01.nc';
    Tindex=13:1:48;
  case 'g7_04'
    fname='/comop/arango/Damee/grid7/damee7_avg_04_04.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    oname='/comop/arango/Damee/grid7/damee7_yavg_04_04.nc';
    Tindex=1:1:34;
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    oname='/e2/arango/Damee/grid9/damee9_yavg_01.nc';
    Tindex=1:1:36;
end,

%--------------------------------------------------------------------------
%  Compute averages.
%--------------------------------------------------------------------------

[X,Y,Z,Favg]=average(gname,fname,'zeta',Tindex);
status=nc_write(oname,'zeta',Favg,1);
disp(['zeta : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'ubar',Tindex);
status=nc_write(oname,'ubar',Favg,1);
disp(['ubar : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'vbar',Tindex);
status=nc_write(oname,'vbar',Favg,1);
disp(['vbar : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'u',Tindex);
status=nc_write(oname,'u',Favg,1);
disp(['u    : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'v',Tindex);
status=nc_write(oname,'v',Favg,1);
disp(['v    : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'omega',Tindex);
status=nc_write(oname,'omega',Favg,1);
disp(['omeg : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'temp',Tindex);
status=nc_write(oname,'temp',Favg,1);
disp(['temp : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'salt',Tindex);
status=nc_write(oname,'salt',Favg,1);
disp(['salt : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'rho',Tindex);
status=nc_write(oname,'rho',Favg,1);
disp(['rho  : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'hbl',Tindex);
status=nc_write(oname,'hbl',Favg,1);
disp(['hbl  : ',num2str(status)]);

