function plt_frmt1 (fname)

IPLOT=1;
spval=99.0;

% Set standard depths to interpolate.

z=[    0   -10   -20   -30   -50   -75  -100  -125  -150  -200  -250 ...
    -300  -400  -500  -600  -700  -800  -900 -1000 -1100 -1200 -1300 ...
   -1400 -1500 -1750 -2000 -2250 -2500 -2750 -3000 -3250 -3500 -3750 ...
   -4000 -4250 -4500 -4750 -5000 -5250];

Kz=length(z);

% Read in data.

fid=fopen(fname,'r');
if (fid < 0),
  error(['Cannot open ' fname '.'])
end,

[f,count]=fscanf(fid,'%i %i',2);
Im=round(f(1));
Km=round(f(2));
[x,count]=fscanf(fid,'%g',Im);
[y,count]=fscanf(fid,'%g',Im);
[T,count]=fscanf(fid,'%g',[Im,Km]);
[S,count]=fscanf(fid,'%g',[Im,Km]);

fclose(fid);

% Plot fields.

if (IPLOT),

  X=x(:,ones([1 Km]));
  Y=y(:,ones([1 Km]));
  Z=z(ones([1 Im]),:);

  ind=find(T == spval);
  T(ind)=NaN;
  ind=find(S == spval);
  S(ind)=NaN;

  if (min(x) ~= max(x)),
    figure;
    contourf(X,Z,T,20); colorbar; grid on;
    title(['Temperature: ',fname]);
    xlabel('Longitude'); ylabel('Depth');

    figure;
    contourf(X,Z,S,20); colorbar; grid on;
    title(['Salinity: ',fname]);
    xlabel('Longitude'); ylabel('Depth');
  else
    figure;
    contourf(Y,Z,T,20); colorbar; grid on;
    title(['Temperature: ',fname]);
    xlabel('Latitude'); ylabel('Depth');

    figure;
    contourf(Y,Z,S,20); colorbar; grid on;
    title(['Salinity: ',fname]);
    xlabel('Latitude'); ylabel('Depth');
  end,
end,

return
