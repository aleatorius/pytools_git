%  Computes Damee get cross-sections of temperature and salinity at
%  selected average month of Year 10.

global IPRINT

IPRINT=0;
IWRITE=1;
IPLOT=1;
damee='g6_06';

iflag=0;
if (IWRITE),
  iflag=1;
end,

ds=0.5;
nc=20;
cmap='default';

switch ( damee ),
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #4: Run 17,';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #4: Run 22,';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #4: Run 24,';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    mgrid='Damee #4: Run 26,'; 
    feb=49;
    may=52;
    aug=55;
    nov=58;
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #4: Run 27,';
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #4: Run 28,';
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #5: Run 04,';
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #6: Run 05,';
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/coamps/grid6/damee6_avg_06.nc';
    feb=109;
    may=112;
    aug=115;
    nov=118;
    mgrid='Damee #6: Run 06,';
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    feb=49;
    may=52;
    aug=55;
    nov=58;
    mgrid='Damee #7: Run 05,';
end,

%--------------------------------------------------------------------------
% Extract sections along 55W.
%--------------------------------------------------------------------------

xsec=[-55.0 -55.0];
ysec=[10.0 47.0];

[x,z,Tfeb]=NA_sec(gname,fname,'temp',feb,xsec,ysec,ds,iflag);
[x,z,Tmay]=NA_sec(gname,fname,'temp',may,xsec,ysec,ds,iflag);
[x,z,Taug]=NA_sec(gname,fname,'temp',aug,xsec,ysec,ds,iflag);
[x,z,Tnov]=NA_sec(gname,fname,'temp',nov,xsec,ysec,ds,iflag);

[x,z,Sfeb]=NA_sec(gname,fname,'salt',feb,xsec,ysec,ds,iflag);
[x,z,Smay]=NA_sec(gname,fname,'salt',may,xsec,ysec,ds,iflag);
[x,z,Saug]=NA_sec(gname,fname,'salt',aug,xsec,ysec,ds,iflag);
[x,z,Snov]=NA_sec(gname,fname,'salt',nov,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tfeb,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: February, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth55w.ps

  figure,
  contourf(x,z,Tmay,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: May, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth55w.ps

  figure,
  contourf(x,z,Taug,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: August, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth55w.ps

  figure,
  contourf(x,z,Tnov,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: November, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth55w.ps

  figure,
  contourf(x,z,Sfeb,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: February, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth55w.ps

  figure,
  contourf(x,z,Smay,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: May, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth55w.ps

  figure,
  contourf(x,z,Saug,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: August, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth55w.ps

  figure,
  contourf(x,z,Snov,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: November, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth55w.ps
end,

if (IWRITE),
  [Im,Km]=size(Tfeb);
  Xout=xsec(1)*ones([1 Im]);
  Yout=x(:,1);  
  sec_frmt(Xout,Yout,Tfeb,Sfeb,'ts55w.feb');
  sec_frmt(Xout,Yout,Tmay,Smay,'ts55w.may');
  sec_frmt(Xout,Yout,Taug,Saug,'ts55w.aug');
  sec_frmt(Xout,Yout,Tnov,Snov,'ts55w.nov');
end,

%--------------------------------------------------------------------------
% Extract sections from Cape Hateras to Bermuda.
%--------------------------------------------------------------------------

xsec=-76.0:0.5:-65.0; xsec=xsec';
ysec=[35.0 35.0 35.0 35.0 34.5 34.5 34.5 34.5 34.5 34.0 34.0 34.0 34.0 ...
      33.5 33.5 33.5 33.5 33.5 33.0 33.0 33.0 33.0 32.5];

[x,z,Tfeb]=NA_sec(gname,fname,'temp',feb,xsec,ysec,ds,iflag);
[x,z,Tmay]=NA_sec(gname,fname,'temp',may,xsec,ysec,ds,iflag);
[x,z,Taug]=NA_sec(gname,fname,'temp',aug,xsec,ysec,ds,iflag);
[x,z,Tnov]=NA_sec(gname,fname,'temp',nov,xsec,ysec,ds,iflag);

[x,z,Sfeb]=NA_sec(gname,fname,'salt',feb,xsec,ysec,ds,iflag);
[x,z,Smay]=NA_sec(gname,fname,'salt',may,xsec,ysec,ds,iflag);
[x,z,Saug]=NA_sec(gname,fname,'salt',aug,xsec,ysec,ds,iflag);
[x,z,Snov]=NA_sec(gname,fname,'salt',nov,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tfeb,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: February, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mthCHB.ps

  figure,
  contourf(x,z,Tmay,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: May, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mthCHB.ps

  figure,
  contourf(x,z,Taug,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: August, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mthCHB.ps

  figure,
  contourf(x,z,Tnov,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: November, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mthCHB.ps

  figure,
  contourf(x,z,Sfeb,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: February, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mthCHB.ps

  figure,
  contourf(x,z,Smay,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: May, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mthCHB.ps

  figure,
  contourf(x,z,Saug,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: August, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mthCHB.ps

  figure,
  contourf(x,z,Snov,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: November, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mthCHB.ps
end,

if (IWRITE),
  Xout=xsec;
  Yout=ysec;
  sec_frmt(Xout,Yout,Tfeb,Sfeb,'tsCHB.feb');
  sec_frmt(Xout,Yout,Tmay,Smay,'tsCHB.may');
  sec_frmt(Xout,Yout,Taug,Saug,'tsCHB.aug');
  sec_frmt(Xout,Yout,Tnov,Snov,'tsCHB.nov');
end,

%--------------------------------------------------------------------------
% Extract sections along 27N.
%--------------------------------------------------------------------------

xsec=[-80.0 -10.0];
ysec=[27.0 27.0];

[x,z,Tfeb]=NA_sec(gname,fname,'temp',feb,xsec,ysec,ds,iflag);
[x,z,Tmay]=NA_sec(gname,fname,'temp',may,xsec,ysec,ds,iflag);
[x,z,Taug]=NA_sec(gname,fname,'temp',aug,xsec,ysec,ds,iflag);
[x,z,Tnov]=NA_sec(gname,fname,'temp',nov,xsec,ysec,ds,iflag);

[x,z,Sfeb]=NA_sec(gname,fname,'salt',feb,xsec,ysec,ds,iflag);
[x,z,Smay]=NA_sec(gname,fname,'salt',may,xsec,ysec,ds,iflag);
[x,z,Saug]=NA_sec(gname,fname,'salt',aug,xsec,ysec,ds,iflag);
[x,z,Snov]=NA_sec(gname,fname,'salt',nov,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tfeb,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: February, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth27n.ps

  figure,
  contourf(x,z,Tmay,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: May, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth27n.ps

  figure,
  contourf(x,z,Taug,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: August, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth27n.ps

  figure,
  contourf(x,z,Tnov,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: November, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth27n.ps

  figure,
  contourf(x,z,Sfeb,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: February, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth27n.ps

  figure,
  contourf(x,z,Smay,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: May, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth27n.ps

  figure,
  contourf(x,z,Saug,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: August, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth27n.ps

  figure,
  contourf(x,z,Snov,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: November, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth27n.ps
end,

if (IWRITE),
  [Im,Km]=size(Tfeb);
  Xout=x(:,1);
  Yout=xsec(1).*ones([1 Im]);  
  sec_frmt(Xout,Yout,Tfeb,Sfeb,'ts27n.feb');
  sec_frmt(Xout,Yout,Tmay,Smay,'ts27n.may');
  sec_frmt(Xout,Yout,Taug,Saug,'ts27n.aug');
  sec_frmt(Xout,Yout,Tnov,Snov,'ts27n.nov');
end,

%--------------------------------------------------------------------------
% Extract sections along 35N.
%--------------------------------------------------------------------------

xsec=[-80.0 -5.0];
ysec=[35.0 35.0];

[x,z,Tfeb]=NA_sec(gname,fname,'temp',feb,xsec,ysec,ds,iflag);
[x,z,Tmay]=NA_sec(gname,fname,'temp',may,xsec,ysec,ds,iflag);
[x,z,Taug]=NA_sec(gname,fname,'temp',aug,xsec,ysec,ds,iflag);
[x,z,Tnov]=NA_sec(gname,fname,'temp',nov,xsec,ysec,ds,iflag);

[x,z,Sfeb]=NA_sec(gname,fname,'salt',feb,xsec,ysec,ds,iflag);
[x,z,Smay]=NA_sec(gname,fname,'salt',may,xsec,ysec,ds,iflag);
[x,z,Saug]=NA_sec(gname,fname,'salt',aug,xsec,ysec,ds,iflag);
[x,z,Snov]=NA_sec(gname,fname,'salt',nov,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tfeb,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: February, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth35n.ps

  figure,
  contourf(x,z,Tmay,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: May, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth35n.ps

  figure,
  contourf(x,z,Taug,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: August, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth35n.ps

  figure,
  contourf(x,z,Tnov,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: November, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_mth35n.ps

  figure,
  contourf(x,z,Sfeb,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: February, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth35n.ps

  figure,
  contourf(x,z,Smay,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: May, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth35n.ps

  figure,
  contourf(x,z,Saug,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: August, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth35n.ps

  figure,
  contourf(x,z,Snov,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: November, Year 10'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_mth35n.ps
end,

if (IWRITE),
  [Im,Km]=size(Tfeb);
  Xout=x(:,1);
  Yout=xsec(1).*ones([1 Im]);  
  sec_frmt(Xout,Yout,Tfeb,Sfeb,'ts35n.feb');
  sec_frmt(Xout,Yout,Tmay,Smay,'ts35n.may');
  sec_frmt(Xout,Yout,Taug,Saug,'ts35n.aug');
  sec_frmt(Xout,Yout,Tnov,Snov,'ts35n.nov');
end,
