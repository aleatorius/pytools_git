%  This function plots composite Kinetic for all DAMEE paper experiments.

%  Read in Florida Strait transport data.

load ke.mat

%  Set data according to paper experiments.

x1=D24.time; y1=0.01.*D24.ke.*D24.vol./D24.area; s1=35.5;
x2=D22.time; y2=0.01.*D22.ke.*D22.vol./D22.area; s2=35.5;
x3=D26.time; y3=0.01.*D26.ke.*D26.vol./D26.area; s3=35.5;
x4=D25.time; y4=0.01.*D25.ke.*D25.vol./D25.area; s4=35.5;
x5=D03.time; y5=0.01.*D03.ke.*D03.vol./D03.area; s5=35.5;
x6=D05.time; y6=0.01.*D05.ke.*D05.vol./D05.area; s6=35.5;
x7=D27.time; y7=0.01.*D27.ke.*D27.vol./D27.area; s7=1.0;
x8=D02.time; y8=0.01.*D02.ke.*D02.vol./D02.area; s8=1.0;
x9=D17.time; y9=0.01.*D17.ke.*D17.vol./D17.area; s9=1.0;

%  Plot Kinetic Energy.

figure;
plot(x1,s1.*y1,'r-', ...
     x2,s2.*y2,'c-', ...
     x3,s3.*y3,'g-', ...
     x7,s7.*y7,'b-', ...
     x8,s8.*y8,'k-', ...
     x6,s6.*y6,'m-', ...
     x9,s9.*y9,'y-');
h=legend('E1','E2','E3','E4','E5','E6','E7',4);
grid on; hold on;
xlabel('Year'); ylabel('Kinetic Energy (10^2  J/m^2)');
set(gca,'Xlim',[0 10],'Ylim',[0 70]);
print -dpsc ke_all.ps

figure;
plot(x1,s1.*y1,'r-', ...
     x2,s2.*y2,'c-', ...
     x3,s3.*y3,'g-', ...
     x7,s7.*y7,'b-', ...
     x8,s8.*y8,'k-', ...
     x6,s6.*y6,'m-', ...
     x9,s9.*y9,'y-');
legend('E1','E2','E3','E4','E5','E6','E7',4);
grid on; hold on;
xlabel('Year'); ylabel('Kinetic Energy (10^2  J/m^2)');
set(gca,'Xlim',[0 20],'Ylim',[0 70]);
print -dpsc -append ke_all.ps
