function plt_path (fname)

IPLOT=1;
spval=900.0;
spv=99.0;

cst_file='/d1/arango/scrum3.0/plot/Data/us_cst.dat';

m_proj('Mercator');
 
[cstlon,cstlat]=rcoastline(cst_file);
[icst]=find(cstlon>=spval);
cstlon(icst)=NaN;
cstlat(icst)=NaN;

figure;
set(gca,'xlim',[-80 -50],'ylim',[30 50]);
hold on;
grid on;
draw_cst(cstlon,cstlat,'k');

% Read in data.

fid=fopen(fname,'r');
if (fid < 0),
  error(['Cannot open ' fname '.'])
end,

while feof(fid) == 0
  [f,count]=fscanf(fid,'%i %i',2);
  month=round(f(1));
  npts=round(f(2));
  [lon,count]=fscanf(fid,'%g',npts);
  [lat,count]=fscanf(fid,'%g',npts);
  ind=find(lon == spv);
  lon(ind)=NaN;
  lat(ind)=NaN;
  plot(lon,lat);
end,

return
