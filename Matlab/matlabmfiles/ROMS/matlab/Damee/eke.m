%  Computes mean eddy kinetic energy averaged over the last
%  3 year of simulation.

global IPRINT

IPRINT=0;
IWRITE=1;
IPLOT=1;
interpolate=1;
method='linear';
damee='g6_05';

% Set IO files.

switch ( damee ),
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    Oname='eke4_17.dat';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 17,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    Oname='eke4_22_01.dat';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 22,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    Oname='eke4_24.dat';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 24,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    Oname='eke4_24.dat';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 25,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    Oname='eke4_26.dat';
    Tindex=25:1:60;
    mgrid='Damee #4: Run 26,'; 
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    Oname='eke4_27.dat';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 27,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    Oname='eke4_28.dat';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 28,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    Oname='eke5_04.dat';
    Tindex=85:1:120;
    mgrid='Damee #5: Run 04,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    Oname='eke6_05.dat';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 05,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/coamps/grid6/damee6_avg_06.nc';
    Oname='eke6_06.dat';
    Tindex=1:1:120;
    mgrid='Damee #6: Run 06,';
    LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;
  case 'g7_05'
    gname='/e1/arango/Damee/grid7/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    Oname='eke7_05.dat';
    Tindex=1:1:36;
    mgrid='Damee #7: Run 05,';
    LonMin=-100; LonMax=15; LatMin=-35;  LatMax=65;
end,

cst_file='/home/arango/ocean/plot/Data/coastdb.dat';
spval=900;

Nrec=length(Tindex);

%--------------------------------------------------------------------------
% Read in longitude and latitude positions and Land/Sea mask.
%--------------------------------------------------------------------------

plon=nc_read(gname,'lon_psi');
plat=nc_read(gname,'lat_psi');
pmask=nc_read(gname,'mask_psi');

[L,M]=size(plon);
Lp=L+1;
Mp=M+1;
Ninp=20;

plon=plon';
plat=plat';
pmask=pmask';
Mind=find(pmask == 0.5);

%--------------------------------------------------------------------------
% Read in U and V from years 7-10 and compute averages.
%--------------------------------------------------------------------------

% Compute time mean.

Uavg=zeros([L  Mp Ninp]);
Vavg=zeros([Lp M  Ninp]);

i=0;
for n=1:Nrec;
  i=i+1;
  Trec=Tindex(n);
  f=nc_read(fname,'u',Trec);
  Uavg=Uavg+f;
  f=nc_read(fname,'v',Trec);
  Vavg=Vavg+f;
end,

Uavg=Uavg./i;
Vavg=Vavg./i;

% Compute kinetic energy.

E0000=0.125.*((Uavg(1:L,1:M,Ninp)+Uavg(1:L,2:Mp,Ninp)).* ...
              (Uavg(1:L,1:M,Ninp)+Uavg(1:L,2:Mp,Ninp)) + ...
              (Vavg(1:L,1:M,Ninp)+Vavg(2:Lp,1:M,Ninp)).* ...
              (Vavg(1:L,1:M,Ninp)+Vavg(2:Lp,1:M,Ninp)));
E0000=E0000';
E0000(Mind)=0;

Zinp=depths(fname,gname,3,0,0);
for j=1:Mp,
  for i=1:L,
    Zwrk=reshape(Zinp(i,j,:),1,Ninp);
    Uwrk=reshape(Uavg(i,j,:),1,Ninp);
    U0700(i,j)=interp1(Zwrk,Uwrk,-700.0);
    U4000(i,j)=interp1(Zwrk,Uwrk,-4000.0);
  end,
end,

Zinp=depths(fname,gname,4,0,0);
for j=1:M,
  for i=1:Lp,
    Zwrk=reshape(Zinp(i,j,:),1,Ninp);
    Vwrk=reshape(Vavg(i,j,:),1,Ninp);
    V0700(i,j)=interp1(Zwrk,Vwrk,-700.0);
    V4000(i,j)=interp1(Zwrk,Vwrk,-4000.0);
  end,
end,

E0700=0.125.*((U0700(1:L,1:M)+U0700(1:L,2:Mp)).* ...
              (U0700(1:L,1:M)+U0700(1:L,2:Mp)) + ...
              (V0700(1:L,1:M)+V0700(2:Lp,1:M)).* ...
              (V0700(1:L,1:M)+V0700(2:Lp,1:M)));
E0700=E0700';
E0700(Mind)=0;

E4000=0.125.*((U4000(1:L,1:M)+U4000(1:L,2:Mp)).* ...
              (U4000(1:L,1:M)+U4000(1:L,2:Mp)) + ...
              (V4000(1:L,1:M)+V4000(2:Lp,1:M)).* ...
              (V4000(1:L,1:M)+V4000(2:Lp,1:M)));
E4000=E4000';
E4000(Mind)=0;

%--------------------------------------------------------------------------
% Interpolate data to requested grid.
%--------------------------------------------------------------------------

if (interpolate),

  x=-98.0:0.5:10.0; x=x';
  y=9.0:0.5:47.0;

  LonMin=-100; LonMax=15; LatMin=9;  LatMax=47;

  Lout=length(x);
  Mout=length(y);
  Xout=x(:,ones([1 Mout]));
  Yout=y(ones([1 Lout]),:);

% Apply Land/Sea mask to average fields.

  mask=reshape(pmask,1,prod(size(pmask)));
  Mind=find(mask == 0.0);

  E0000=reshape(E0000,1,prod(size(E0000))); E0000(Mind)=[];
  E0700=reshape(E0700,1,prod(size(E0700))); E0700(Mind)=[];
  E4000=reshape(E4000,1,prod(size(E4000))); E4000(Mind)=[];

  lon=reshape(plon,1,prod(size(plon))); lon(Mind)=[];
  lat=reshape(plat,1,prod(size(plat))); lat(Mind)=[];

% Interpolate

  KE0000=griddata(lon,lat,E0000,Xout,Yout,method);
  KE0700=griddata(lon,lat,E0700,Xout,Yout,method);
  KE4000=griddata(lon,lat,E4000,Xout,Yout,method);

  mask=interp2(plon,plat,pmask,Xout,Yout,method);
  Mind=find(mask < 0.5);

  KE0000(Mind)=NaN;
  KE0700(Mind)=NaN;
  KE4000(Mind)=NaN;  

%--------------------------------------------------------------------------
% Write out data to an ASCII file.
%--------------------------------------------------------------------------

  if (IWRITE),

    fid=fopen(Oname,'w');
    if (fid < 0),
      error(['Cannot open ' Oname '.'])
    end

    for j=1:Mout,
      for i=1:Lout,
        if (~isnan(KE0000(i,j)) & ~isnan(KE0700(i,j)) & ~isnan(KE4000(i,j))),
%         fprintf(fid,'%7.1f %6.1f %7.3f %7.3f %7.3f\n', ...
          fprintf(fid,'%7.1f %6.1f %12.4e %12.4e %12.4e\n', ...
                  Xout(i,j),Yout(i,j),KE0000(i,j),KE0700(i,j), ...
                  KE4000(i,j));
        end,
      end,
    end

    fclose(fid);

  end,

end,

%--------------------------------------------------------------------------
% Plot data.
%--------------------------------------------------------------------------

if (IPLOT),

  m_proj('Mercator','long',[LonMin LonMax],'lat',[LatMin LatMax]);

  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=spval);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  [Xcst,Ycst]=m_ll2xy(cstlon,cstlat,'point');

  if (interpolate),
    [XOUT,YOUT]=m_ll2xy(Xout,Yout,'point');
  else
    [X,Y]=m_ll2xy(plon,plat,'point');
  end,
  [Xmin,Ymin]=m_ll2xy(LonMin,LatMin);
  [Xmax,Ymax]=m_ll2xy(LonMax,LatMax);

  figure;
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1]);
  if (interpolate),
    fmin=min(min(KE0000)); fmax=max(max(KE0000));
    pcolor(XOUT,YOUT,KE0000); shading interp; colorbar;
  else,
    fmin=min(min(E0000)); fmax=max(max(E0000));
    pcolor(X,Y,E0000); shading interp; colorbar;
  end,
  hold on;
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  draw_cst(Xcst,Ycst,'k');
  m_grid;
  title(strcat(mgrid,' Surface Kinetic Energy'));
  xlabel(['Min = ',num2str(fmin),'  Max = ',num2str(fmax)]);


  figure;
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1]);
  if (interpolate),
    fmin=min(min(KE0700)); fmax=max(max(KE0700));
    pcolor(XOUT,YOUT,KE0700); shading interp; colorbar;
  else,
    fmin=min(min(E0700)); fmax=max(max(E0700));
    pcolor(X,Y,E0700); shading interp; colorbar;
  end,
  hold on;
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  draw_cst(Xcst,Ycst,'k');
  m_grid;
  title(strcat(mgrid,' Surface Kinetic Energy at 700m'));
  xlabel(['Min = ',num2str(fmin),'  Max = ',num2str(fmax)]);

  figure;
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1]);
  if (interpolate),
    fmin=min(min(KE4000)); fmax=max(max(KE4000));
    pcolor(XOUT,YOUT,KE4000); shading interp; colorbar;
  else,
    fmin=min(min(E4000)); fmax=max(max(E4000));
    pcolor(X,Y,E4000); shading interp; colorbar;
  end,
  hold on;
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  draw_cst(Xcst,Ycst,'k');
  m_grid;
  title(strcat(mgrid,' Surface Kinetic Energy at 4000m'));
  xlabel(['Min = ',num2str(fmin),'  Max = ',num2str(fmax)]);
end,
