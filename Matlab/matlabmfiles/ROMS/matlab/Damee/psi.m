%  Driver for processing transport streamfunction data NetCDF file.

global IPRINT

IPRINT=0;
IWRITE=0;
IPLOT=1;
damee='4b';

switch ( damee ),
  case '4a'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='grid4a/psi_4a.dat';
    oname='/e2/arango/Damee/grid4/damee4_psi_04.nc';
    mgrid='Damee #4a: ';
  case '4b'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='grid4b/psi_4b.dat';
    oname='/e2/arango/Damee/grid4/damee4_psi_06.nc';
    mgrid='Damee #4b: ';
  case '4c'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='grid4c/psi_4c.dat';
    oname='/e2/arango/Damee/grid4/damee4_psi_07.nc';
    mgrid='Damee #4c: ';
  case '5'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='grid5/psi_5.dat';
    oname='/e2/arango/Damee/grid5/damee5_psi_01.nc';
    mgrid='Damee #5: ';
end,

geosat='/e1/arango/Damee/matlab/Data/gsaxis.geosat';
topex='/e1/arango/Damee/matlab/Data/gsaxis.topex';

cst_file='/d1/arango/scrum3.0/plot/Data/us_cst.dat';

%--------------------------------------------------------------------------
%  Read in coast line data.
%--------------------------------------------------------------------------

if ( IPLOT ),
  m_proj('Mercator');

  spval=900.0;
  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=spval);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
end,

%-------------------------------------------------------------------------
%  Read in Gulf stream axis from GEOSAT.
%-------------------------------------------------------------------------

if ( IPLOT ),

  fid=fopen(geosat,'r');
  if (fid < 0),
    error(['Cannot open ' fname '.'])
  end,
 
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);

  [f count]=fscanf(fid,'%g %g %g %g %g %g %g %g %g %g',[10 inf]);

  Xgeo_nth=f(1:10:count);
  Ygeo_nth=f(2:10:count);
  Xgeo_nsd=f(3:10:count);
  Ygeo_nsd=f(4:10:count);

  Xgeo_avg=f(5:10:count);
  Ygeo_avg=f(6:10:count);

  Xgeo_sth=f(7:10:count);
  Ygeo_sth=f(8:10:count);
  Xgeo_ssd=f(9:10:count);
  Ygeo_ssd=f(10:10:count);

  fclose(fid);

end,

%-------------------------------------------------------------------------
%  Read in Gulf stream axis from TOPEX.
%-------------------------------------------------------------------------

if ( IPLOT ),

  fid=fopen(topex,'r');
  if (fid < 0),
    error(['Cannot open ' fname '.'])
  end,
 
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);
  line=fgetl(fid);

  [f count]=fscanf(fid,'%g %g %g %g %g %g %g %g %g %g',[10 inf]);

  Xpex_nth=f(1:10:count);
  Ypex_nth=f(2:10:count);
  Xpex_nsd=f(3:10:count);
  Ypex_nsd=f(4:10:count);

  Xpex_avg=f(5:10:count);
  Ypex_avg=f(6:10:count);

  Xpex_sth=f(7:10:count);
  Ypex_sth=f(8:10:count);
  Xpex_ssd=f(9:10:count);
  Ypex_ssd=f(10:10:count);

  fclose(fid);

end,

%--------------------------------------------------------------------------
%  Read in positions data.
%--------------------------------------------------------------------------

plon=nc_read(gname,'lon_psi');
plat=nc_read(gname,'lat_psi');
pmask=nc_read(gname,'mask_psi');
msk=find(pmask < 1);

%--------------------------------------------------------------------------
%  Read in ASCII data and write into a NetCDF file.
%--------------------------------------------------------------------------

if ( IWRITE ),

  fid=fopen(fname,'r');
  if (fid < 0);
    error(['Cannot open ' oname '.']);
  end,

  i=0;
  while feof(fid) == 0
    [f,count]=fscanf(fid,'%i %i %i\n',3);
    if (~isempty(f)),
      trec=f(1);
      L=round(f(2));
      M=round(f(3));
      [f,count]=fscanf(fid,'%g',[L M]);
    
      disp(['Processing time record: ',num2str(trec)]);
      i=i+1;
      time=(trec*30.0+15.0)*86400.0;
      if (i == 1),
        [status]=wrt_psi(oname,f,time,plon,plat);
      else
        [status]=wrt_psi(oname,f,time);
      end,
    end,
  end,

  fclose(fid);

end,

%--------------------------------------------------------------------------
%  Read transport streamfunction from NetCDF file.
%--------------------------------------------------------------------------
 
P=nc_read(oname,'psi');

%--------------------------------------------------------------------------
%  Plot transport streamfunction.
%--------------------------------------------------------------------------

if ( IPLOT ),

  pavg=mean(P,3);
% pavg(msk)=NaN;

  figure;
  pcolor(plon,plat,pavg); shading interp; colorbar;
% contourf(plon,plat,pavg,20); colormap(gray); colorbar;
  grid on;
  hold on;
  draw_cst(cstlon,cstlat,'k');
  title(strcat(mgrid,' Annual Transport Streamfunction (Sv)'));
  xlabel('Longitude'); ylabel('Latitude');
  set(gca,'xlim',[-80 -50],'ylim',[30 50]);

% plot(Xgeo_nth,Ygeo_nth,'k--',Xgeo_avg,Ygeo_avg,'k',Xgeo_sth,Ygeo_sth,'k--');
% text(-79,48.5,'GEOSAT');
  han=plot(Xpex_nth,Ypex_nth,Xpex_avg,Ypex_avg,Xpex_sth,Ypex_sth);
  set(han,'color','k','linewidth',1)
  text(-79,48.5,'TOPEX');

end,


