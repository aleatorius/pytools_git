%  This function computes the volume between density layers.

global IPRINT

IPRINT=0;
IPLOT=1;
COLOR=0;
damee='g4_24';

isoval=[24.70 25.28 25.77 26.18 25.52 26.80 27.03 27.22 ...
        27.38 27.52 27.64 27.74 27.82 27.88 27.92];

switch ( damee ),
  case 'clima'
    fname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Levitus  ';
    Tindex=[2 5 8 11];
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_19_01.nc';
    mgrid='Damee #4: Run 19_01 ';
    Tindex=1:1:5;
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_20.nc';
    mgrid='Damee #4: Run 20 ';
    Tindex=1:1:5;
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_yavg_22_01.nc';
    mgrid='Damee #4: Run 22_01 ';
    Tindex=1:1:5;
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_23.nc';
    mgrid='Damee #4: Run 23 ';
    Tindex=1:1:5;
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_24.nc';
    mgrid='Damee #4: Run 24 ';
    Tindex=1;
end,

Nrec=length(Tindex);
Nlayers=length(isoval);

%--------------------------------------------------------------------------
% Compute depths of isopycnic surfaces.
%--------------------------------------------------------------------------

for n=1:Nrec,

  Trec=Tindex(n);
  Ziso=z_isopycnal(gname,fname,isoval,Trec);

end,

%--------------------------------------------------------------------------
% Compute tickness of density layers.
%--------------------------------------------------------------------------

for n=1:Nlayers,

end