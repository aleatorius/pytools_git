%  Computes Damee cross-section of temperature and salinity averaged
%  over the last three years of simulation.

global IPRINT

IPRINT=0;
IWRITE=1;
IPLOT=1;
damee='g6_06';

iflag=0;
if (IWRITE),
  iflag=1;
end,

ds=0.5;
nc=20;
cmap='default';

switch ( damee ),
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid4/damee4_levfeb_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_ann_17.nc';
    Tindex=1;
    mgrid='Damee #4: Run 17,';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid4/damee4_levfeb_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_ann_22.nc';
    Tindex=1;
    mgrid='Damee #4: Run 22,';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid4/damee4_levfeb_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_ann_24.nc';
    oname='mht4_24.dat';
    Tindex=1;
    mgrid='Damee #4: Run 24,';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid4/damee4_levfeb_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_ann_25.nc';
    oname='mht4_24.dat';
    Tindex=1;
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid4/damee4_levfeb_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_ann_26.nc';
    Tindex=1;
    mgrid='Damee #4: Run 26,'; 
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid4/damee4_levfeb_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_ann_27b.nc';
    Tindex=1;
    mgrid='Damee #4: Run 27,';
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid4/damee4_levfeb_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_ann_28.nc';
    oname='mht4_28.dat';
    Tindex=1;
    mgrid='Damee #4: Run 28,';
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    iname='/d15/arango/scrum/Damee/grid5/damee5_levfeb_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_ann_04.nc';
    Tindex=1;
    mgrid='Damee #5: Run 04,';
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    iname='/n2/arango/Damee/grid6/damee6_levfeb_c.nc';
    fname='/n2/arango/Damee/grid6/damee6_ann_05.nc';
    Tindex=1;
    mgrid='Damee #6: Run 05,';
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    iname='/n2/arango/Damee/grid6/damee6_levfeb_c.nc';
    fname='/coamps/grid6/damee6_ann_06.nc';
    Tindex=1;
    mgrid='Damee #6: Run 06,';
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    iname='/d15/arango/scrum/Damee/grid7/damee6_levfeb_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_ann__05.nc';
    Tindex=1;
    mgrid='Damee #7: Run 05,';
end,

%--------------------------------------------------------------------------
% Extract sections along 55W.
%--------------------------------------------------------------------------

xsec=[-55.0 -55.0];
ysec=[10.0 47.0];

[x,z,Tavg]=NA_sec(gname,fname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Savg]=NA_sec(gname,fname,'salt',Tindex,xsec,ysec,ds,iflag);
[x,z,Tdif]=NA_secdif(gname,fname,iname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Sdif]=NA_secdif(gname,fname,iname,'salt',Tindex,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tavg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc TS_ann55w.ps

  figure,
  contourf(x,z,Tdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 55W: Initial - Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_ann55w.ps

  figure,
  contourf(x,z,Savg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: Years 8-10 Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_ann55w.ps

  figure,
  contourf(x,z,Sdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 55W: Initial - Average'));
  xlabel('Latitude'); ylabel('Depth');
  print -dpsc -append TS_ann55w.ps
end,

if (IWRITE),
  [Im,Km]=size(Tavg);
  Xout=xsec(1)*ones([1 Im]);
  Yout=x(:,1);  
  sec_frmt(Xout,Yout,Tavg,Savg,'ts55w.avg');
  sec_frmt(Xout,Yout,Tdif,Sdif,'ts55w.dif');
end,

%--------------------------------------------------------------------------
% Extract sections from Cape Hateras to Bermuda.
%--------------------------------------------------------------------------

xsec=-76.0:0.5:-65.0; xsec=xsec';
ysec=[35.0 35.0 35.0 35.0 34.5 34.5 34.5 34.5 34.5 34.0 34.0 34.0 34.0 ...
      33.5 33.5 33.5 33.5 33.5 33.0 33.0 33.0 33.0 32.5];

[x,z,Tavg]=NA_sec(gname,fname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Savg]=NA_sec(gname,fname,'salt',Tindex,xsec,ysec,ds,iflag);
[x,z,Tdif]=NA_secdif(gname,fname,iname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Sdif]=NA_secdif(gname,fname,iname,'salt',Tindex,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tavg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: Years 8-10 Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc TS_annCHB.ps

  figure,
  contourf(x,z,Tdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature Cape Hatteras/Bermuda: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_annCHB.ps

  figure,
  contourf(x,z,Savg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: Years 8-10 Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_annCHB.ps

  figure,
  contourf(x,z,Sdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity Cape Hatteras/Bermuda: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_annCHB.ps
end,

if (IWRITE),
  Xout=xsec;
  Yout=ysec;
  sec_frmt(Xout,Yout,Tavg,Savg,'tsCHB.avg');
  sec_frmt(Xout,Yout,Tdif,Sdif,'tsCHB.dif');
end,

%--------------------------------------------------------------------------
% Extract sections along 27N.
%--------------------------------------------------------------------------

xsec=[-80.0 -10.0];
ysec=[27.0 27.0];

[x,z,Tavg]=NA_sec(gname,fname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Savg]=NA_sec(gname,fname,'salt',Tindex,xsec,ysec,ds,iflag);
[x,z,Tdif]=NA_secdif(gname,fname,iname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Sdif]=NA_secdif(gname,fname,iname,'salt',Tindex,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tavg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: Years 8-10 Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc TS_ann27n.ps

  figure,
  contourf(x,z,Tdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 27N: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_ann27n.ps

  figure,
  contourf(x,z,Savg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: Years 8-10 Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_ann27n.ps

  figure,
  contourf(x,z,Sdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 27N: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_ann27n.ps
end,

if (IWRITE),
  [Im,Km]=size(Tavg);
  Xout=x(:,1);
  Yout=xsec(1).*ones([1 Im]);  
  sec_frmt(Xout,Yout,Tavg,Savg,'ts27n.avg');
  sec_frmt(Xout,Yout,Tdif,Sdif,'ts27n.dif');
end,

%--------------------------------------------------------------------------
% Extract sections along 35N.
%--------------------------------------------------------------------------

xsec=[-80.0 -5.0];
ysec=[35.0 35.0];

[x,z,Tavg]=NA_sec(gname,fname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Savg]=NA_sec(gname,fname,'salt',Tindex,xsec,ysec,ds,iflag);
[x,z,Tdif]=NA_secdif(gname,fname,iname,'temp',Tindex,xsec,ysec,ds,iflag);
[x,z,Sdif]=NA_secdif(gname,fname,iname,'salt',Tindex,xsec,ysec,ds,iflag);

if (IPLOT),
  figure,
  contourf(x,z,Tavg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: Years 8-10 Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc TS_ann35n.ps

  figure,
  contourf(x,z,Tdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Temperature along 35N: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_ann35n.ps

  figure,
  contourf(x,z,Savg,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: Years 8-10 Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_ann35n.ps

  figure,
  contourf(x,z,Sdif,nc); colormap(cmap); colorbar;
  grid on;
  title(strcat(mgrid,'Salinity along 35N: Initial - Average'));
  xlabel('Longitude'); ylabel('Depth');
  print -dpsc -append TS_ann35n.ps
end,

if (IWRITE),
  [Im,Km]=size(Tavg);
  Xout=x(:,1);
  Yout=xsec(1).*ones([1 Im]);  
  sec_frmt(Xout,Yout,Tavg,Savg,'ts35n.avg');
  sec_frmt(Xout,Yout,Tdif,Sdif,'ts35n.dif');
end,
