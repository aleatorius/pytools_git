function [time,lat,H]=heat(gname,fname,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [time,lat,H]=MheatT(gname,fname,tindex)                          %
%                                                                           %
% This function computes meridional heat transport.                         %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%                  If no grid file, put field file name instead.            %
%    fname       Field NetCDF file name (character string).                 %
%    tindex      Time record to process (integer).                          %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    time        Time (scalar; days).                                       %
%    lat         Latitude (vector).                                         %
%    H           Meridional heat transport (vector, pectaWatts).            %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Deactivate printing information switch from "nc_read".
 
global IPRINT
 
IPRINT=0;

%----------------------------------------------------------------------------
%  Read in curvilinear metric factor.
%----------------------------------------------------------------------------

pm=nc_read(gname,'pm');
vlat=nc_read(gname,'lat_v');
lat=vlat(1,:);
lat=lat';

%----------------------------------------------------------------------------
%  Compute vertical metric.
%----------------------------------------------------------------------------

z_w=depths(fname,gname,5,0,tindex);
 
[Lp,Mp,Np]=size(z_w);

M=Mp-1;
N=Np-1;

ds=1/N;
ods=N;

Hz=(z_w(:,:,2:Np)-z_w(:,:,1:N)).*ods;

%----------------------------------------------------------------------------
%  Read in temperature and meridional velocity.
%----------------------------------------------------------------------------

[vname,nvars]=nc_vname(fname);
for n=1:nvars,
  name=deblank(vname(n,:));
  switch name
    case 'scrum_time'
      Vname.time=name;
    case 'ocean_time'
      Vname.time=name;
  end,
end,

T=nc_read(fname,'temp',tindex);
V=nc_read(fname,'v',tindex);

time=nc_read(fname,Vname.time,tindex);
time=time/86400;

%----------------------------------------------------------------------------
% Compute Meridional heat transport (pectaWatts).
%----------------------------------------------------------------------------

% Compute meridional flux.

dx=pm(:,:,ones([1 N]));
F=0.5.*(Hz(:,1:M,:)+Hz(:,2:Mp,:)).*(T(:,1:M,:)+T(:,2:Mp,:)).*V./ ...
       (dx(:,1:M,:)+dx(:,2:Mp,:));

% Integrate vertically.

Fy=sum(F.*ds,3);

% Integrate horizontally.

H=sum(Fy,1);

%----------------------------------------------------------------------------
% Scale to pectaWatts (1.0E+15  Watts).
%----------------------------------------------------------------------------

rhoCp=4.06e+6;              % Joules/kg/Celsius
scale=1.0e-15*rhoCp;

H=scale.*H;

return






