%  This script computes the position of the Gulf Stream in terms of
%  the position of the surface maximum velocity.

IWRITE=0;
IPLOT=1;
damee='g4_25';
method='linear';

lon_min=-75.0;
lon_max=-58.0;
lat_min=33.0;
lat_max=50.0;
spval=900.0;
spv=99.0;

%  Set plotting Map longitudes and Latitudes

LonMin=-80;
LonMax=-50;
LatMin=30;
LatMax=50;

switch ( damee ),
  case 'g4_04'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    Oname='GS_core4_04.avg';
    Tindex=25:1:60;
    TopLat=42;
    mgrid='Damee #4: Run 04';
  case 'g4_06'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
%   fname='/e1/arango/Damee/grid4/damee4_uv_06.nc';
%   Tindex=1:1:360;
    Oname='GS_core4_06.avg';
    Tindex=85:1:120;
    TopLat=42;
    mgrid='Damee #4: Run 06';
  case 'g4_07'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    Oname='GS_core4_07.avg';
    Tindex=85:1:120;
    TopLat=42;
    mgrid='Damee #4: Run 07';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    Oname='GS_core4_24.avg';
    Tindex=85:1:120;
    TopLat=42;
    mgrid='Damee #4: Run 24';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    Oname='GS_core4_25.avg';
    Tindex=85:1:120;
    TopLat=42;
    mgrid='Damee #4: Run 25';
  case 'g5_01'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/d15/arango/scrum/Damee/grid5/damee5_avg_01.nc';
    Oname='GS_core5_01.avg';
    Tindex=85:1:120;
    TopLat=40;
    mgrid='Damee #5: Run 01';
  case 'g6_01'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    Oname='GS_core6_01.avg';
    Tindex=85:1:120;
    TopLat=42;
    mgrid='Damee #6: Run 01';
  case 'g7_01'
    gname='/d15/arango/scrum/Damee/grid7/damee7_grid_a.nc';
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    Oname='GS_core7.avg';
    Tindex=13:1:36;
    TopLat=42;
    mgrid='Damee #7: Run 01';
  case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    Oname='GS_core9.avg';
    Tindex=1:1:36;
    TopLat=42;
    mgrid='Damee #9: Run 01';
end,

geosat='/e1/arango/Damee/matlab/Data/gsaxis.geosat';
topex='/e1/arango/Damee/matlab/Data/gsaxis.topex';

cst_file='/d1/arango/scrum3.0/plot/Data/us_cst.dat';
Text=' Gulf Stream North Wall (Surface Velocity Core)';


if (IPLOT),
  m_proj('Mercator');
 
  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=spval);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  [Xcst,Ycst]=m_ll2xy(cstlon,cstlat,'point');

  [Xmin,Ymin]=m_ll2xy(LonMin,LatMin);
  [Xmax,Ymax]=m_ll2xy(LonMax,LatMax);

  figure(1);
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  hold on;
  draw_cst(Xcst,Ycst,'k');
  title(strcat(mgrid,Text));

  figure(2);
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  hold on;
  draw_cst(Xcst,Ycst,'k');
  title(strcat(mgrid,Text));
end,

%-------------------------------------------------------------------------
%  Read in Gulf stream axis from GEOSAT.
%-------------------------------------------------------------------------

fid=fopen(geosat,'r');
if (fid < 0),
  error(['Cannot open ' fname '.'])
end
 
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);

[f count]=fscanf(fid,'%g %g %g %g %g %g %g %g %g %g',[10 inf]);

Xgeo_nth=f(1:10:count);
Ygeo_nth=f(2:10:count);
Xgeo_nsd=f(3:10:count);
Ygeo_nsd=f(4:10:count);

Xgeo_avg=f(5:10:count);
Ygeo_avg=f(6:10:count);

Xgeo_sth=f(7:10:count);
Ygeo_sth=f(8:10:count);
Xgeo_ssd=f(9:10:count);
Ygeo_ssd=f(10:10:count);

fclose(fid);

%-------------------------------------------------------------------------
%  Read in Gulf stream axis from TOPEX.
%-------------------------------------------------------------------------

fid=fopen(topex,'r');
if (fid < 0),
  error(['Cannot open ' fname '.'])
end
 
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);

[f count]=fscanf(fid,'%g %g %g %g %g %g %g %g %g %g',[10 inf]);

Xpex_nth=f(1:10:count);
Ypex_nth=f(2:10:count);
Xpex_nsd=f(3:10:count);
Ypex_nsd=f(4:10:count);

Xpex_avg=f(5:10:count);
Ypex_avg=f(6:10:count);

Xpex_sth=f(7:10:count);
Ypex_sth=f(8:10:count);
Xpex_ssd=f(9:10:count);
Ypex_ssd=f(10:10:count);

fclose(fid);

%-------------------------------------------------------------------------
%  Compute average surface velocity magnitude.
%-------------------------------------------------------------------------

level=20;
month=0;
[plon,plat,Vmag]=speed(fname,gname,level,Tindex);

%-------------------------------------------------------------------------
%  Extract maximum velocity positions.
%-------------------------------------------------------------------------

%  Interpolate to Gulf Stream domain.

x=-80:0.5:-50; x=x';
y=30:0.5:TopLat;
Xmv=x';

Lout=length(x);
Mout=length(y);
Xout=x(:,ones([1 Mout]));
Yout=y(ones([1 Lout]),:);

Vout=interp2(plon',plat',Vmag',Xout,Yout,method);

%  Extract positions of maximum velocity.

[Vmax,ind]=max(Vout,[],2);
Ymv=y(ind)';

%  Plot data.

if ( IPLOT ),
  figure(1);
  plot(Xmv,Ymv,'kx');
  plot(Xgeo_nth,Ygeo_nth,Xgeo_avg,Ygeo_avg,Xgeo_sth,Ygeo_sth);
  text(-79,48.5,'GEOSAT');
  print -dpsc GS_core.ps

  figure(2);
  plot(Xmv,Ymv,'kx');
  plot(Xpex_nth,Ypex_nth,Xpex_avg,Ypex_avg,Xpex_sth,Ypex_sth);
  text(-79,48.5,'TOPEX');
  print -dpsc -append GS_core.ps

  figure(3);
  pcolor(Xout,Yout,Vout); shading interp; colorbar;
end,

if (IWRITE),
  fid=fopen(Oname,'w');
  if (fid < 0);
    error(['Cannot create ' Oname '.']);
  end,

  lon=Xmv; lat=Ymv;
  msk=find(lon<lon_min | lon>lon_max); lon(msk)=[]; lat(msk)=[];

  fprintf(fid,'%i %i\n',month,length(lon));
  fprintf(fid,'%9.4f ',lon);
  fprintf(fid,'\n');
  fprintf(fid,'%9.4f ',lat);
  fprintf(fid,'\n');
  fclose(fid);
end,

