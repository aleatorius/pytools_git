function [depth]=iso_depth(X,Z,F,isoval)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [Yiso]=iso_depth(X,Y,F,isoval);                                  %
%                                                                           %
% This function extract the position of requested isoline from a 2D field.  %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    X           X-positions of field (matrix).                             %
%    Y           Y-positions of field (matrix).                             %
%    F           Field to process (matrix).                                 %
%    isoval      Isoline value to process (real scalar).                    %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    Xiso        X-positions of isoline (vector).                           %
%    Yiso        Y-positions of isoline (vector).                           %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='linear';

[Im,Km]=size(F);

for i=1:Im,
  Fwrk=reshape(F(i,:),1,Km);
  Zwrk=reshape(Z(i,:),1,Km);
  depth(i)=interp1(Fwrk,Zwrk,isoval,method);
end   

return


