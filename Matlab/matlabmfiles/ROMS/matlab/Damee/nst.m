% This script computes time-series of cumulative North-South transport.

IPLOT=1;
IWRITE=1;
damee='g6_06';

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    oname='nst4_04.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 04,';
  case 'g4_06'
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    oname='nst4_06.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 06,';
  case 'g4_07'
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    oname='nst4_07.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 07,';
  case 'g4_10.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    oname='nst4_10_01.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 10.01,';
  case 'g4_13.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    oname='nst4_13_01.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 13.01,';
  case 'g4_13.2'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    oname='nst4_13_02.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 13.02,';
  case 'g4_14'
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    oname='nst4_14.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 14,';
  case 'g4_15'
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    oname='nst4_15.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 15,';
  case 'g4_16'
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    oname='nst4_16.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 16,';
  case 'g4_17'
    fname='/e0/arango/Damee/grid4/damee4_avg_17.nc';
    oname='nst4_17.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 17,';
  case 'g4_18'
    fname='/e2/arango/Damee/grid4/damee4_avg_18.nc';
    oname='nst4_18.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 18,';
  case 'g4_19.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    oname='nst4_19_01.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 19_01,';
  case 'g4_20'
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    oname='nst4_20.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 20,';
  case 'g4_22.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    oname='nst4_22_01.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 22,';
  case 'g4_23'
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    oname='nst4_23.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 23,';
  case 'g4_24'
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    oname='nst4_24.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 24 ';
  case 'g4_25'
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    oname='nst4_25.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    oname='nst4_26.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 26,';
  case 'g4_27'
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    oname='nst4_27.dat';
    Jo=66; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #4: Run 27,';
  case 'g5_03'
    fname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    oname='nst5_03.dat';
    Jo=25; Istr=22; Iend=98;
    Tindex=85:1:120;
    mgrid='Damee #5: Run 03,';
  case 'g6_05'
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    oname='nst6_05.dat';
    Jo=42; Istr=36; Iend=169;
    Tindex=85:1:120;
    mgrid='Damee #6: Run 06,';
  case 'g6_06'
    fname='/coamps/grid6/damee6_avg_06.nc';
    oname='nst6_06.dat';
    Jo=42; Istr=36; Iend=169;
    Tindex=85:1:120;
    mgrid='Damee #6: Run 06,';
end,

%--------------------------------------------------------------------------
% Compute transport along 27N.
%--------------------------------------------------------------------------

[lon,time,T]=NS_trans(fname,Tindex,Jo,Istr,Iend);

%--------------------------------------------------------------------------
% Average transport by month.
%--------------------------------------------------------------------------

Tmonth(:, 1)=mean(T(:,12:12:36),2);
Tmonth(:, 2)=mean(T(:, 1:12:36),2);
Tmonth(:, 3)=mean(T(:, 2:12:36),2);
Tmonth(:, 4)=mean(T(:, 3:12:36),2);
Tmonth(:, 5)=mean(T(:, 4:12:36),2);
Tmonth(:, 6)=mean(T(:, 5:12:36),2);
Tmonth(:, 7)=mean(T(:, 6:12:36),2);
Tmonth(:, 8)=mean(T(:, 7:12:36),2);
Tmonth(:, 9)=mean(T(:, 8:12:36),2);
Tmonth(:,10)=mean(T(:, 9:12:36),2);
Tmonth(:,11)=mean(T(:,10:12:36),2);
Tmonth(:,12)=mean(T(:,11:12:36),2);

%--------------------------------------------------------------------------  
%  Plot.
%--------------------------------------------------------------------------

if ( IPLOT ),
  figure
  plot(lon,Tmonth);
  title(strcat(mgrid,' Monthly Barotropic Transport along 27N'));
  xlabel('Longitude');
  ylabel('Sverdrups');
  grid on;
  set(gca,'Xlim',[-Inf Inf]);
  print -dpsc NS_trans.ps

  figure
  plot(lon,T);
  title(strcat(mgrid,' Barotropic Transport along 27N'));
  xlabel('Longitude');
  ylabel('Sverdrups');
  grid on;
  set(gca,'Xlim',[-Inf Inf]);
  print -dpsc -append NS_trans.ps

  figure
  [Im Jm]=size(T);
  X=lon(:,ones([1 Jm]));
  Y=time(ones([1 Im]),:);
  pcolor(X,Y,T); shading interp; colorbar;
  title(strcat(mgrid,' Barotropic Transport (Sv) along 27N'));
  xlabel('Longitude');
  ylabel('Year');
  print -dpsc -append NS_trans.ps
end,

%--------------------------------------------------------------------------  
%  Write out data.
%--------------------------------------------------------------------------

if ( IWRITE ),
  fid=fopen(oname,'w');
  if (fid < 0),
    error(['Cannot create ' oname4 '.'])
  end,
  [Im Jm]=size(Tmonth);
  month=1:1:12;
  for j=1:Jm,
    for i=1:Im,
      fprintf(fid,'%4i %8.4f %7.3f\n',month(j),lon(i),Tmonth(i,j));
    end,
  end,
  fclose(fid);
end,

