%  This function computes depth of an isopycnal along a specified
%  section and then plot Hovmuller diagram.

global IPRINT

IPRINT=0;
IPLOT=1;
COLOR=0;
damee='g4_17';

method='linear';

Llon=-30;
Rlon=-30;
Blat=-20;
Tlat=60;
delta=0.5;

if (Llon == Rlon),
  meridional=1;
elseif (Blat == Tlat),
  meridional=0;
end,

isoval=[25.0 25.5 25.7 26.0 26.5 27.0];

switch ( damee ),
  case 'clima'
    fname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    mgrid='Levitus,';
    Tindex=[2 5 8 11];
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    mgrid='Damee #4: Run 17,';
    Tindex=1:1:120;
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    mgrid='Damee #4: Run 19_01,';
    Tindex=1:1:120;
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    mgrid='Damee #4: Run 20,';
    Tindex=1:1:120;
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_22_01.nc';
    mgrid='Damee #4: Run 22_01,';
    Tindex=1:1:120;
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23.nc';
    mgrid='Damee #4: Run 23,';
    Tindex=1:1:120;
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    mgrid='Damee #4: Run 24,';
    Tindex=1:1:120;
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    mgrid='Damee #4: Run 25,';
    Tindex=1:1:120;
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    mgrid='Damee #4: Run 26,';
    Tindex=1:1:60;
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    mgrid='Damee #4: Run 27,';
    Tindex=1:1:120;
end,

%--------------------------------------------------------------------------
% Compute depths of isopycnic surfaces.
%--------------------------------------------------------------------------

[F]=Hisopyc(gname,fname,isoval,Tindex,Llon,Rlon,Blat,Tlat,delta);

save ziso.mat F

%--------------------------------------------------------------------------
% Draw Hovmuller diagram.
%--------------------------------------------------------------------------

if ( IPLOT ), 

  Im=length(F.x);
  Jm=length(F.y);
  Niso=length(isoval);

  dt=(F.y(2)-F.y(1))/2;
  time=(F.y-dt)./86400./360;

  X=F.x(:,ones([1 Jm]));
  Y=time(ones([1 Im]),:);

  for n=1:Niso,

    Ziso=reshape(F.z(:,n,:),Im,Jm);

    figure;
    [c,h]=contourf(X,Y,Ziso,20); colorbar;
    grid on;
    box on;
    if (meridional),
      xlabel('Latitude');
      title(['Depth (m)  of ',num2str(isoval(n)), '  kg/m^3  Isopycnal ' ...
             'along ',num2str(abs(Llon)),'W']);
    else,
      xlabel('Longitude');
      title(['Depth (m)  of ',num2str(isoval(n)), '  kg/m^3  Isopycnal ' ...
             'along ',num2str(abs(Blat)),'N']);
    end,
    ylabel('Time (years)');

    if (n == 1),
      print -dpsc ziso.ps
    else,
      print -dpsc -append ziso.ps
    end,

  end,

end,
