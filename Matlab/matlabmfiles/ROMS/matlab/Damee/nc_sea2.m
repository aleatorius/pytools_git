% This script builds  seasonal averages for the last three years of
% simulation and write it out into a NetCDF file.

global IPRINT

IPRINT=0;
damee='g6_06';

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e0/arango/Damee/grid4/damee4_yavg_04.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_06'
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e0/arango/Damee/grid4/damee4_yavg_06.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_07'
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e2/arango/Damee/grid4/damee4_yavg_07.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_10.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_10_01.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_13.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_13_01.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_13.2'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_13_02.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_14'
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_14.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_15'
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_15.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_16'
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_16.nc';
    jan=36:12:120;
    feb=25:12:120;
    mar=26:12:120;
    apr=27:12:120;
    may=28:12:120;
    jun=29:12:120;
    jul=30:12:120;
    aug=31:12:120;
    sep=32:12:120;
    oct=33:12:120;
    nov=34:12:120;
    dec=35:12:120;
  case 'g4_17'
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_17.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_19.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_19_01.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_20'
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_20.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_22'
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_22.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_23'
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_23.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_24'
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_24.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_25'
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_25.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_26'
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_26.nc';
    jan=12:12:60;
    feb= 1:12:60;
    mar= 2:12:60;
    apr= 3:12:60;
    may= 4:12:60;
    jun= 5:12:60;
    jul= 6:12:60;
    aug= 7:12:60;
    sep= 8:12:60;
    oct= 9:12:60;
    nov=10:12:60;
    dec=11:12:60;
  case 'g4_27'
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_27.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g4_28'
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_yavg_28.nc';
    jan=72:12:103;
    feb=73:12:103;
    mar=74:12:103;
    apr=75:12:103;
    may=76:12:103;
    jun=77:12:103;
    jul=78:12:103;
    aug=79:12:103;
    sep=68:12:103;
    oct=69:12:103;
    nov=70:12:103;
    dec=71:12:103;
  case 'g5_03'
    fname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    oname='/comop/arango/Damee/grid5/damee5_yavg_03.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g5_04'
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    oname='/comop/arango/Damee/grid5/damee5_yavg_04.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g6_03'
    fname='/comop/arango/Damee/grid6/damee6_avg_03.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='/comop/arango/Damee/grid6/damee6_yavg_03.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g6_04'
    fname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='/comop/arango/Damee/grid6/damee6_yavg_04.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g6_05'
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    oname='/n2/arango/Damee/grid6/damee6_yavg_05.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g6_06'
    fname='/coamps/grid6/damee6_avg_06.nc';
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    oname='/coamps/grid6/damee6_yavg_06.nc';
    jan=96:12:120;
    feb=85:12:120;
    mar=86:12:120;
    apr=87:12:120;
    may=88:12:120;
    jun=89:12:120;
    jul=90:12:120;
    aug=91:12:120;
    sep=92:12:120;
    oct=93:12:120;
    nov=94:12:120;
    dec=95:12:120;
  case 'g7_05'
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    oname='/comop/arango/Damee/grid7/damee7_yavg_05.nc';
    jan=12:12:36;
    feb= 1:12:36;
    mar= 2:12:36;
    apr= 3:12:36;
    may= 4:12:36;
    jun= 5:12:36;
    jul= 6:12:36;
    aug= 7:12:36;
    sep= 8:12:36;
    oct= 9:12:36;
    nov=10:12:36;
    dec=11:12:36;
end,

month( 1,:)=jan;
month( 2,:)=feb;
month( 3,:)=mar;
month( 4,:)=apr;
month( 5,:)=may;
month( 6,:)=jun;
month( 7,:)=jul;
month( 8,:)=aug;
month( 9,:)=sep;
month(10,:)=oct;
month(11,:)=nov;
month(12,:)=dec;

time=15:30:360;
time=time.*86400.0;

%--------------------------------------------------------------------------
%  Inquire name of variables in input NetCDF.
%--------------------------------------------------------------------------

[vname,nvars]=nc_vname(fname);

got_time=0;
got_zeta=0;
got_ubar=0;
got_vbar=0;
got_temp=0;
got_salt=0;
got_rho=0;
got_u=0;
got_v=0;
got_w=0;
got_omega=0;
got_hbl=0;
got_AKv=0;
got_AKt=0;
got_AKs=0;

for n=1:nvars,
  name=deblank(vname(n,:));
  switch (name),
    case 'scrum_time'
      got_time=1;
    case 'ocean_time'
      got_time=1;
    case 'zeta'
      got_zeta=1;
    case 'ubar'
      got_ubar=1;
    case 'vbar'
      got_vbar=1;
    case 'temp'
      got_temp=1;
    case 'salt'
      got_salt=1;
    case 'rho'
      got_rho=1;
    case 'u'
      got_u=1;
    case 'v'
      got_v=1;
    case 'w'
      got_w=1;
    case 'omega'
      got_omega=1;
    case 'Hsbl'
      got_hbl=1;
    case 'AKv'
      got_AKv=1;
    case 'AKt'
      got_AKt=1;
    case 'AKs'
      got_AKs=1;
  end,
end,

%--------------------------------------------------------------------------
%  Compute averages.
%--------------------------------------------------------------------------

for n=1:12,

  Trec=month(n,:);

  if (got_time),
    status=nc_write(oname,'scrum_time',time(n),n);
    disp(['time - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_zeta),
    [X,Y,Z,Favg]=average(gname,fname,'zeta',Trec);
    status=nc_write(oname,'zeta',Favg,n);
    disp(['zeta - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_ubar),
    [X,Y,Z,Favg]=average(gname,fname,'ubar',Trec);
    status=nc_write(oname,'ubar',Favg,n);
    disp(['ubar - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_vbar),
    [X,Y,Z,Favg]=average(gname,fname,'vbar',Trec);
    status=nc_write(oname,'vbar',Favg,n);
    disp(['vbar - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_u),
    [X,Y,Z,Favg]=average(gname,fname,'u',Trec);
    status=nc_write(oname,'u',Favg,n);
    disp(['u    - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_v),
    [X,Y,Z,Favg]=average(gname,fname,'v',Trec);
    status=nc_write(oname,'v',Favg,n);
    disp(['v    - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_w),
    [X,Y,Z,Favg]=average(gname,fname,'w',Trec);
    status=nc_write(oname,'w',Favg,n);
    disp(['w    - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_omega),
    [X,Y,Z,Favg]=average(gname,fname,'omega',Trec);
    status=nc_write(oname,'omega',Favg,n);
    disp(['omeg - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_temp),
    [X,Y,Z,Favg]=average(gname,fname,'temp',Trec);
    status=nc_write(oname,'temp',Favg,n);
    disp(['temp - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_salt),
    [X,Y,Z,Favg]=average(gname,fname,'salt',Trec);
    status=nc_write(oname,'salt',Favg,n);
    disp(['salt - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_rho),
    [X,Y,Z,Favg]=average(gname,fname,'rho',Trec);
    status=nc_write(oname,'rho',Favg,n);
    disp(['rho  - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_hbl),
    [X,Y,Z,Favg]=average(gname,fname,'Hsbl',Trec);
    status=nc_write(oname,'hbl',Favg,n);
    disp(['hbl  - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_AKv),
    [X,Y,Z,Favg]=average(gname,fname,'AKv',Trec);
    status=nc_write(oname,'AKv',Favg,n);
    disp(['AKv  - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_AKt),
    [X,Y,Z,Favg]=average(gname,fname,'AKt',Trec);
    status=nc_write(oname,'AKt',Favg,n);
    disp(['AKt  - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

  if (got_AKs),
    [X,Y,Z,Favg]=average(gname,fname,'AKs',Trec);
    status=nc_write(oname,'AKs',Favg,n);
    disp(['AKs  - ',num2str(n,'%2.2i'),': ',num2str(status)]);
  end,

end,
