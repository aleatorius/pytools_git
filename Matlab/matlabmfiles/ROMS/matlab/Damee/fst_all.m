%  This function plots composite Florida Strait transport for all
%  DAMEE paper experiments.

%  Read in Florida Strait transport data.

load fst.mat

%  Set data according to paper experiments.

x1=x24; y1=T24; clear x24 T24
x2=x22; y2=T22; clear x22 T22
x3=x26; y3=T26; clear x26 T26
x4=x25; y4=T25; clear x25 T25
x5=x03; y5=T03; clear x03 T03
x6=x05; y6=T05; clear x05 T05
x7=x17; y7=T17; clear x17 T17
x8=x27; y8=T27; clear x27 T27
x9=x02; y9=T02; clear x02 T02

%  Set Cable Data (Sverdrups) at 27N (March 1982 - April 1996) from
%  Dr. J.C. Larsen.

Cmin=[27.31 28.01 25.99 29.47 28.74 27.96 ...
      30.92 31.58 30.11 28.03 26.27 28.33];
Cavg=[30.81 31.37 30.91 32.37 32.84 33.26 ...
      33.87 34.05 33.24 32.08 30.76 30.85];
Cmax=[33.24 34.57 36.60 37.49 35.86 37.79 ...
      37.65 36.50 36.72 36.58 33.41 34.39];

%  Replicate Cable data for 10 years.

x=0:10/119:10; x=x';
Tmin=[Cmin Cmin Cmin Cmin Cmin Cmin Cmin Cmin Cmin Cmin];
Tavg=[Cavg Cavg Cavg Cavg Cavg Cavg Cavg Cavg Cavg Cavg];
Tmax=[Cmax Cmax Cmax Cmax Cmax Cmax Cmax Cmax Cmax Cmax];

%  Plot Florida Straits transport.

figure;
plot(x1,y1,'r-', ...
     x2,y2,'c-', ...
     x3,y3,'g-', ...
     x4,y4,'b-', ...
     x8,y8,'k-', ...
     x6,y6,'m-', ...
     x7,y7,'y-');
legend('E1','E2','E3','E4','E5','E6','E7',3);
grid on; hold on;
xlabel('Year'); ylabel('Florida Straits Transport (Sv)');
set(gca,'Xlim',[7 10],'Ylim',[10 50]);
h=plot(x,Tmin,'k--',x,Tavg,'k--',x,Tmax,'k--');
set(h,'LineWidth',[2]);

