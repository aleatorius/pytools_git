function [lat,z,phi]=overturn(gname,fname,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [lat,z,phi]=overturn(gname,fname,Tstr,Tend)                      %
%                                                                           %
% This function computes zonally averaged meridional overturning            %
% streamfunction, time averaged over specified records.                     %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%                  If no grid file, put field file name instead.            %
%    fname       Field NetCDF file name (character string).                 %
%    tindex      Time records to process (integer; scalar or vector).       %
%                  If tindex is a vector, the data will averaged            %
%                  over all requested time records.                         %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    lat         Latitude (vector; degrees North).                          %
%    z           depth (vector; meters).                                    %
%    phi         Meridional overturning streamfunction (matrix; Sv).        %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='linear';

%----------------------------------------------------------------------------
% Set standard levels.
%----------------------------------------------------------------------------

z=[    0   -10   -20   -30   -50   -75  -100  -125  -150  -200  -250 ...
    -300  -400  -500  -600  -700  -800  -900 -1000 -1100 -1200 -1300 ...
   -1400 -1500 -1750 -2000 -2250 -2500 -2750 -3000 -3250 -3500 -3750 ...
   -4000 -4250 -4500 -4750 -5000 -5250];

Kz=length(z);

%----------------------------------------------------------------------------
% Read in curvilinear metric factor.
%----------------------------------------------------------------------------

pm=nc_read(gname,'pm');

vlat=nc_read(gname,'lat_v');
lat=vlat(1,:);
lat=lat';

clear vlat

%----------------------------------------------------------------------------
% Compute depths at W-points.
%----------------------------------------------------------------------------

z_w=depths(fname,gname,5,0,0);

[Lp,Mp,Np]=size(z_w);

M=Mp-1;
N=Np-1;
Nm=N-1;

ds=1/N;
ods=N;

Hz=(z_w(1:Lp,1:Mp,2:Np)-z_w(1:Lp,1:Mp,1:N)).*ods;

%----------------------------------------------------------------------------
% Read in meridional velocity and compute time average.
%----------------------------------------------------------------------------

v=zeros([Lp M N]);

nrec=length(tindex);

if (nrec > 1),
  i=0;
  for n=1:nrec,
    i=i+1;
    Trec=tindex(n);
    a=nc_read(fname,'v',Trec);
    v=v+a;
  end,
  v=v./i;
  clear a
else,
  v=nc_read(fname,'v',tindex);
end,
  
%----------------------------------------------------------------------------
% Compute meridional overturning streamfunction (SV).
%----------------------------------------------------------------------------

%  Compute meridional flux; scale to Sverdrups.

scale=1.0e-6;
dx=pm(:,:,ones([1 N]));

Fv=-scale.*(Hz(:,1:M,:)+Hz(:,2:Mp,:)).*v(:,1:M,:).*ds./ ...
           (dx(:,1:M,:)+dx(:,2:Mp,:));

% Integrate vertically.

dFvdz(:,:,1)=zeros([Lp M]);
for k=2:Np,
  dFvdz(:,:,k)=dFvdz(:,:,k-1)+Fv(:,:,k-1);
end;

% Interpolate to standard vertical levels.

zwv=0.5.*(z_w(:,1:M,:)+z_w(:,2:Mp,:));
for j=1:M,
  for i=1:Lp,
    Z=reshape(zwv(i,j,:),1,Np);
    V=reshape(dFvdz(i,j,:),1,Np);
    dFvdzL(i,j,:)=interp1(Z,V,z,method);
  end,
end,
dFvdzL(:,:,1 )=zeros([Lp M]);
dFvdzL(:,:,Kz)=zeros([Lp M]);

ind=find(isnan(dFvdzL));
dFvdzL(ind)=0.0;

% Integrate horizontally.

a=sum(dFvdzL,1);
phi=reshape(a,M,Kz);

return
