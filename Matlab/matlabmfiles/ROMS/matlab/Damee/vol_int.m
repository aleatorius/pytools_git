function [Fvol]=vol_int(gname,fname,vname,tindex,Llon,Rlon,Blat,Tlat,delta);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [Fvol]=vol_int(gname,fname,vname,tindex,Llon,Rlon,Blat,Tlat,     %
%                         delta)                                            %
%                                                                           %
% This function computes the volume integral values for requested variable  %
% for the entire domain or specified region.                                %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%                  If no grid file, put field file name instead.            %
%    fname       Field NetCDF file name (character string).                 %
%    vname       Variable name (character string).                          %
%    tindex      Time records to process (integer; scalar or vector).       %
%                  If tindex is a vector, the data will integrated          %
%                  over requested time records.                             %
%    Llon        Left corner longitude (East values are negative).          %
%    Rlon        Right corner longitude (East values are negative).         %
%    Blat        Bottom corner latitude (South values are negative).        %
%    Tlat        Top corner latitude (South values are negative).           %
%    delta       Horizontal grid spacing to interpolate (degrees).          %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Fvol        Volume integrated data (structure array).                  %
%                  Fvol.field:   field                                      %
%                  Fvol.time:    time                                       %
%                  Fvol.volume:  total volume (m3).                         %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

method='linear';

% Activate interpolation switch

if (nargin > 4),
  interpolate=1;
else,
  interpolate=0;
end,

%----------------------------------------------------------------------------
% Determine positions and Land/Sea masking variable names.
%----------------------------------------------------------------------------

[dnames,dsizes,igrid]=nc_vinfo(fname,vname);

switch ( igrid ),
  case 1
    Xname='lon_rho';
    Yname='lat_rho';
    Mname='mask_rho';
  case 2
    Xname='lon_psi';
    Yname='lat_psi';
    Mname='mask_psi';
  case 3
    Xname='lon_u';
    Yname='lat_u';
    Mname='mask_u';
  case 4
    Xname='lon_v';
    Yname='lat_v';
    Mname='mask_v';
  case 5
    Xname='lon_rho';
    Yname='lat_rho';
    Mname='mask_rho';
end,

[varname,nvars]=nc_vname(fname);
for n=1:nvars,
  name=deblank(varname(n,:));
  switch name
    case 'scrum_time'
      Vname.time=name;
    case 'ocean_time'
      Vname.time=name;
  end,
end,

%----------------------------------------------------------------------------
% Read in grid data.
%----------------------------------------------------------------------------

% Compute vertical coordinate metric.  Assume zero free-surface and
% constant in time.

z_w=depths(fname,gname,5,0,0);

[Lp,Mp,Np]=size(z_w);
L=Lp-1;
M=Mp-1;
N=Np-1;
Nm=N-1;

Hz=(z_w(1:Lp,1:Mp,2:Np)-z_w(1:Lp,1:Mp,1:N));

clear z_w

% Read in horizontal curvilinear metrics.

pm=nc_read(gname,'pm');
pn=nc_read(gname,'pn');

% Read in horizontal positions.

lon=nc_read(gname,Xname);
lat=nc_read(gname,Yname);

% Read in Land/Sea mask.

msk=nc_read(gname,Mname);
mask=msk(:,:,ones([1 N]));

clear msk

% Compute grid spacing.

switch ( igrid ),
  case 1
    dx=1./pm(:,:,ones([1 N]));
    dy=1./pn(:,:,ones([1 N]));
    dz=Hz;
  case 2
    gs=0.25.*(pm(1:L,1:M)+pm(2:Lp,1:M)+pm(1:L,2:Mp)+pm(2:Lp,2:Mp));
    dx=1./gs(:,:,ones([1 N]));
    gs=0.25.*(pn(1:L,1:M)+pn(2:Lp,1:M)+pn(1:L,2:Mp)+pn(2:Lp,2:Mp));
    dy=1./gs(:,:,ones([1 N]));
    dz=0.25.*(Hz(1:L,1:M,:)+Hz(2:Lp,1:M,:)+Hz(1:L,2:Mp,:)+Hz(2:Lp,2:Mp,:));
  case 3
    gs=0.5.*(pm(1:L,1:Mp)+pm(2:Lp,1:Mp));
    dx=1./gs(:,:,ones([1 N]));
    gs=0.5.*(pn(1:L,1:Mp)+pn(2:Lp,1:Mp));
    dy=1./gs(:,:,ones([1 N]));
    dz=0.5.*(Hz(1:L,1:Mp,:)+Hz(2:Lp,1:Mp,:));
  case 4
    gs=0.5.*(pm(1:Lp,1:M)+pm(1:Lp,2:Mp));
    dx=1./gs(:,:,ones([1 N]));
    gs=0.5.*(pn(1:Lp,1:M)+pn(1:Lp,2:Mp));
    dy=1./gs(:,:,ones([1 N]));
    gs=0.5.*(Hz(1:Lp,1:M,:)+Hz(1:Lp,2:Mp,:));
end,

dxdydz=dx.*dy.*dz.*mask;

% Set grid to interpolate horizontally.

if (interpolate),
  x=Llon:delta:Rlon; x=x';
  Im=length(x);
  y=Blat:delta:Tlat;
  Jm=length(y);

  Xi=x(:,ones([1 Jm]));
  Yi=y(ones([1 Im]),:);
end,

% Get total volume for the analyzed area.

if (interpolate),
  F2d=sum(dxdydz,3);
  Fint=interp2(lon',lat',F2d',Xi,Yi,method);
  Fvol.volume=(sum(sum(Fint)));
  clear F2d Fint
else
  Fvol.volume=sum(sum(sum(dxdydz)));
end,

clear dx dy dz gs Hz mask pm pn

%----------------------------------------------------------------------------
% Volume integrate requested variable.
%----------------------------------------------------------------------------

nrec=length(tindex);

if (nrec > 0),

  i=0;

  for n=1:nrec,

    i=i+1;
    Trec=tindex(n);

%  Read in variable and multipy by volume.

    F3d=nc_read(fname,vname,Trec);
    F3d=F3d.*dxdydz;

%  Integrate vertically and interpolate to requested area, if any.

    if (interpolate),
      F=sum(F3d,3);
      F2d=interp2(lon',lat',F',Xi,Yi,method);
      clear F
    else,
      F2d=sum(F3d,3);
    end,

%  Integrate horizontally.

    Fvol.field(i)=sum(sum(F2d))/Fvol.volume;
    Fvol.time(i)=nc_read(fname,Vname.time,Trec);

  end,
end,

return
