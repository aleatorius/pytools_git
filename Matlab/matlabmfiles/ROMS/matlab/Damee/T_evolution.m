function [F]=T_evolution(gname,fname,Tindex,Llon,Rlon,Blat,Tlat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [F]=T_evolution(gname,fname,Tindex,Llon,Rlon,Blat,Tlat);         %
%                                                                           %
% This function computes temperature and salinity time evolution at         %
% standard depths averaged over specified area.                             %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%    fname       Field NetCDF file name (character string).                 %
%    Tindex      Time records to process (integer; vector).                 %
%    Llon        Left edge longitude (degrees, negative EAST).              %
%    Rlon        Right edge longitude (degrees, negative EAST).             %
%    Blat        Bottom edge latitude (degrees, negative SOUTH).            %
%    Tlat        Top edge latitude (degrees, negative SOUTH).               %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    F           Structure array:                                           %
%                  F.z    =>  standard depths.                              %
%                  F.time =>  time.                                         %
%                  F.temp =>  temperature.                                  %
%                  F.salt =>  salinity.                                     %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='linear';

%----------------------------------------------------------------------------
% Set standard levels.
%----------------------------------------------------------------------------

z=[  -20   -30   -50   -75  -100  -125  -150  -200  -250  -300  -400 ...
    -500  -600  -700  -800  -900 -1000 -1100 -1200 -1300 -1400 -1500 ...
   -1750 -2000 -2250 -2500 -2750 -3000 -3250 -3500 -3750 -4000 -4250 ...
   -4500 -4750 -5000];

F.z=z';
Kz=length(z);

%--------------------------------------------------------------------------
%  Get longitude and latitude grid positions.
%--------------------------------------------------------------------------

rlon=nc_read(gname,'lon_rho');
rlat=nc_read(gname,'lat_rho');

rmask=nc_read(gname,'mask_rho');

%--------------------------------------------------------------------------
%  Set depths at RHO-points
%--------------------------------------------------------------------------

Zr=depths(fname,gname,1,0,0);

[Lp,Mp,N]=size(Zr);

%----------------------------------------------------------------------------
%  Select indices of area to process.
%----------------------------------------------------------------------------

if (nargin > 3)
  Istr=min(find(rlon(:,1) >= Llon));
  Iend=max(find(rlon(:,1) <= Rlon));
  Jstr=min(find(rlat(1,:) >= Blat));
  Jend=max(find(rlat(1,:) <= Tlat));
else,
  Istr=1;
  Iend=Lp;
  Jstr=1;
  Jend=Mp;
end,

%--------------------------------------------------------------------------
%  Inquire name of time variable.
%--------------------------------------------------------------------------

[vname,nvars]=nc_vname(fname);

for n=1:nvars,
  name=vname(n,:);
  if (~isempty(findstr(name,'_time'))),
    tname=vname(n,:);
    break;
  end,
end,

%----------------------------------------------------------------------------
% Read in curvilinear metric factor and compute area.
%----------------------------------------------------------------------------

pm=nc_read(gname,'pm');
pn=nc_read(gname,'pn');

dxdy=(1./pm).*(1./pn).*rmask;

clear pm pn

%----------------------------------------------------------------------------
%  Compute temperature and salinity area averages at each standard depth.
%----------------------------------------------------------------------------

Nrec=length(Tindex);

for n=1:Nrec,
  
  Trec=Tindex(n);

  disp(['Processing time record: ', num2str(n,'%3.3i')]);

% Read in temperature and salinity.

  T=nc_read(fname,'temp',Trec);
  S=nc_read(fname,'salt',Trec);

% Read in time.

  F.time(n)=nc_read(fname,tname,Trec);

% Interpolate to standard vertical levels.

  jj=0;
  for j=Jstr:Jend,
    ii=0;
    jj=jj+1;
    for i=Istr:Iend,
      ii=ii+1;
      if (rmask(i,j) > 0),
        Zwrk=reshape(Zr(i,j,:),1,N);
        Twrk=reshape(T(i,j,:),1,N);
        Swrk=reshape(S(i,j,:),1,N);
        Ti(ii,jj,:)=interp1(Zwrk,Twrk,z,method);
        Si(ii,jj,:)=interp1(Zwrk,Swrk,z,method);
        DxDy(ii,jj)=dxdy(i,j);
      end,
    end,
  end,

  clear S T Swrk Twrk Zwrk

% Compute area averages.

  [Im Jm Km]=size(Ti);

  for k=1:Km,
    F.temp(k,n)=NaN;
    F.salt(k,n)=NaN;
    Tsum=0;
    Ssum=0;
    Asum=0;
    for j=1:Jm,
      for i=1:Im,
        if (~isnan(Ti(i,j,k))),
          Tsum=Tsum+Ti(i,j,k)*DxDy(i,j);
          Ssum=Ssum+Si(i,j,k)*DxDy(i,j);
          Asum=Asum+DxDy(i,j);
        end,
      end,
    end,
    if (Asum > 0),
      F.temp(k,n)=Tsum/Asum;
      F.salt(k,n)=Ssum/Asum;
    end,
  end,

  clear Si Ti DxDy

end,

return
