% This script builds averages for year 10 of simulation and writes it
% out into a NetCDF file

global IPRINT

IPRINT=0;
damee='g4_27';

switch ( damee ),
  case 'g4_04'
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e0/arango/Damee/grid4/damee4_y10_04.nc';
    Tindex=109:1:120;
  case 'g4_06'
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e0/arango/Damee/grid4/damee4_y10_06.nc';
    Tindex=109:1:120;
  case 'g4_07'
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/e2/arango/Damee/grid4/damee4_y10_07.nc';
    Tindex=109:1:120;
  case 'g4_10.1'
    fname='/comop/arango/Damee/grid4/damee4_avg_10_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_10_01.nc';
    Tindex=109:1:120;
  case 'g4_13.1'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_01.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_13_01.nc';
    Tindex=109:1:120;
  case 'g4_13.2'
    fname='/e2/arango/Damee/grid4/damee4_avg_13_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_13_02.nc';
    Tindex=109:1:120;
  case 'g4_14'
    fname='/comop/arango/Damee/grid4/damee4_avg_14.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_14.nc';
    Tindex=109:1:120;
  case 'g4_15'
    fname='/comop/arango/Damee/grid4/damee4_avg_15.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_15.nc';
    Tindex=109:1:120;
  case 'g4_16'
    fname='/e0/arango/Damee/grid4/damee4_avg_16_02.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_16.nc';
    Tindex=49:1:60;
  case 'g4_17'
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
%   oname='/comop/arango/Damee/grid4/damee4_y10_17.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_17.nc';
    Tindex=109:1:120;
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_19_01.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_19_01.nc';
    Tindex=109:1:120;
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_20.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_20.nc';
    Tindex=109:1:120;
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_22_01.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_22_01.nc';
    Tindex=109:1:120;
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_23_02.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_23.nc';
    Tindex=49:1:60;
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_24.nc';
    Tindex=109:1:120;
  case 'g4_27'
    gname='/n0/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/coamps/grid4/damee4_avg_27_02.nc';
    oname='/coamps/grid4/damee4_y10_27_02.nc';
    Tindex=109:1:120;
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    oname='/comop/arango/Damee/grid4/damee4_y10_28.nc';
    Tindex=109:1:120;
  case 'g5_01'
    fname='/d15/arango/scrum/Damee/grid5/damee5_avg_01.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    oname='/e0/arango/Damee/grid5/damee5_y10_01.nc';
    Tindex=109:1:120;
  case 'g5_04'
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    oname='/comop/arango/Damee/grid5/damee5_y10_04.nc';
    Tindex=109:1:120;
  case 'g6_01'
    fname='/e1/arango/Damee/grid6/damee6_avg_01.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='/e0/arango/Damee/grid6/damee6_y10_01.nc';
    Tindex=109:1:120;
  case 'g6_04'
    fname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    oname='/comop/arango/Damee/grid6/damee6_y10_04.nc';
    Tindex=109:1:120;
  case 'g6_05'
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    oname='/n2/arango/Damee/grid6/damee6_y10_05.nc';
    Tindex=109:1:120;
  case 'g6_06'
    fname='/coamps/grid6/damee6_avg_06.nc';
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    oname='/coamps/grid6/damee6_y10_06.nc';
    Tindex=109:1:120;
  case 'g7_05'
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    gname='/e1/arango/Damee/grid7/damee7_grid_a.nc';
    oname='/comop/arango/Damee/grid7/damee7_y10_05.nc';
    Tindex=25:1:35;
end,

%--------------------------------------------------------------------------
%  Compute averages.
%--------------------------------------------------------------------------

[X,Y,Z,Favg]=average(gname,fname,'zeta',Tindex);
status=nc_write(oname,'zeta',Favg,1);
disp(['zeta : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'ubar',Tindex);
status=nc_write(oname,'ubar',Favg,1);
disp(['ubar : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'vbar',Tindex);
status=nc_write(oname,'vbar',Favg,1);
disp(['vbar : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'u',Tindex);
status=nc_write(oname,'u',Favg,1);
disp(['u    : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'v',Tindex);
status=nc_write(oname,'v',Favg,1);
disp(['v    : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'omega',Tindex);
status=nc_write(oname,'omega',Favg,1);
disp(['omeg : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'temp',Tindex);
status=nc_write(oname,'temp',Favg,1);
disp(['temp : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'salt',Tindex);
status=nc_write(oname,'salt',Favg,1);
disp(['salt : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'rho',Tindex);
status=nc_write(oname,'rho',Favg,1);
disp(['rho  : ',num2str(status)]);

[X,Y,Z,Favg]=average(gname,fname,'hbl',Tindex);
status=nc_write(oname,'hbl',Favg,1);
disp(['hbl  : ',num2str(status)]);

