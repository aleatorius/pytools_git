function [KE]=vol_ke(gname,fname,tindex,Llon,Rlon,Blat,Tlat,delta);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [KE]=vol_ke(gname,fname,tindex,Llon,Rlon,Blat,Tlat,delta)        %
%                                                                           %
% This function computes the volume integral Kinetic energy for the         %
% entire domain or specified region.                                        %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    gname       Grid NetCDF file name (character string).                  %
%                  If no grid file, put field file name instead.            %
%    fname       Field NetCDF file name (character string).                 %
%    tindex      Time records to process (integer; scalar or vector).       %
%                  If tindex is a vector, the data will integrated          %
%                  over requested time records.                             %
%    Llon        Left corner longitude (East values are negative).          %
%    Rlon        Right corner longitude (East values are negative).         %
%    Blat        Bottom corner latitude (South values are negative).        %
%    Tlat        Top corner latitude (South values are negative).           %
%    delta       Horizontal grid spacing to interpolate (degrees).          %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    KE          Volume kinetic energy data (structure array).              %
%                  KE.field:   kinetic energy (Joules/m2)                   %
%                  KE.time:    time                                         %
%                  KE.area:    total area (m2)                              %
%                  KE.volume:  total volume (m3)                            %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

method='linear';

% Activate interpolation switch.

if (nargin > 4),
  interpolate=1;
else,
  interpolate=0;
end,

% Get name of time variable.

[vname,nvars]=nc_vname(fname);
for n=1:nvars,
  name=deblank(vname(n,:));
  switch name
    case 'scrum_time'
      Vname.time=name;
    case 'ocean_time'
      Vname.time=name;
  end,
end,

%----------------------------------------------------------------------------
% Read in grid data.
%----------------------------------------------------------------------------

% Compute vertical coordinate metric.  Assume zero free-surface and
% constant in time.

z_w=depths(fname,gname,5,0,0);

[Lp,Mp,Np]=size(z_w);
L=Lp-1;
M=Mp-1;
N=Np-1;
Nm=N-1;

Hz=(z_w(1:Lp,1:Mp,2:Np)-z_w(1:Lp,1:Mp,1:N));

clear z_w

% Read in horizontal curvilinear metrics.

pm=nc_read(gname,'pm');
pn=nc_read(gname,'pn');

% Read in horizontal positions.

lon=nc_read(gname,'lon_psi');
lat=nc_read(gname,'lat_psi');

% Read in Land/Sea mask.

msk=nc_read(gname,'mask_psi');
mask=msk(:,:,ones([1 N]));

% Compute grid spacing at PSI-points.

gx=0.25.*(pm(1:L,1:M)+pm(2:Lp,1:M)+pm(1:L,2:Mp)+pm(2:Lp,2:Mp));
dx=1./gx(:,:,ones([1 N]));
gy=0.25.*(pn(1:L,1:M)+pn(2:Lp,1:M)+pn(1:L,2:Mp)+pn(2:Lp,2:Mp));
dy=1./gy(:,:,ones([1 N]));
dz=0.25.*(Hz(1:L,1:M,:)+Hz(2:Lp,1:M,:)+Hz(1:L,2:Mp,:)+Hz(2:Lp,2:Mp,:));

dxdy=(1./gx).*(1./gy).*msk;
dxdydz=dx.*dy.*dz.*mask;

clear gx gy msk

% Set grid to interpolate horizontally.

if (interpolate),
  x=Llon:delta:Rlon; x=x';
  Im=length(x);
  y=Blat:delta:Tlat;
  Jm=length(y);

  Xi=x(:,ones([1 Jm]));
  Yi=y(ones([1 Im]),:);
end,

% Get total volume for the analyzed area.

if (interpolate),
  F2d=sum(dxdydz,3);
  Fint=interp2(lon',lat',F2d',Xi,Yi,method);
  KE.volume=(sum(sum(Fint)));
  clear F2d Fint
  Fint=interp2(lon',lat',dxdy',Xi,Yi,method);
  KE.area=sum(sum(Fint));
  clear Fint
else
  KE.volume=sum(sum(sum(dxdydz)));
  KE.area=sum(sum(dxdy));
end,

clear dx dxdy dy dz Hz mask pm pn

%----------------------------------------------------------------------------
% Volume integrate requested variable.
%----------------------------------------------------------------------------

nrec=length(tindex);

if (nrec > 0),

  i=0;

  for n=1:nrec,

    i=i+1;
    Trec=tindex(n);

%  Read in velocities and density

    u=nc_read(fname,'u',Trec);
    v=nc_read(fname,'v',Trec);
    rho=nc_read(fname,'rho',Trec);
    rho=rho+1000;

%  Compute kinetic energy at PSI-points (J/m2).

    ke=0.25.*(rho(1:L,1:M ,:)+rho(2:Lp,1:M ,:)+ ...
              rho(1:L,2:Mp,:)+rho(2:Lp,2:Mp,:)).* ...
       0.25.*((u(1:L,1:M,:)+u(1:L ,2:Mp,:)).* ...
              (u(1:L,1:M,:)+u(1:L ,2:Mp,:))+ ...
              (v(1:L,1:M,:)+v(2:Lp,1:M ,:)).* ...
              (v(1:L,1:M,:)+v(2:Lp,1:M ,:)));
 
    ke=ke.*dxdydz;

%  Integrate vertically and interpolate to requested area, if any.

    if (interpolate),
      F=sum(ke,3);
      F2d=interp2(lon',lat',F',Xi,Yi,method);
      clear F
    else,
      F2d=sum(ke,3);
    end,

%  Integrate horizontally and divide by area so units are J/m2.

    KE.field(i)=sum(sum(F2d))*0.5/KE.area;
    KE.time(i)=nc_read(fname,Vname.time,Trec);

  end,
end,

return
