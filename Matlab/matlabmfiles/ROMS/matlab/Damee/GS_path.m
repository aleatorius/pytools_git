%  This scripts computes the position of the Gulf Stream north wall
%  which is defined as the 15C istherm position at 200 meters.

IWRITE=1;
IPLOT=1;
TOPEX=1;
GEOSAT=0;
damee='g6_05';

vname='temp';
isoval=15.0;
depth=-200.0;
lon_min=-75.0;
lon_max=-58.0;
lat_min=33.0;
lat_max=50.0;
spval=900.0;
spv=99.0;

geosat='/export/home/arango/ocean/matlab/Damee/Data/gsaxis.geosat';
topex='/export/home/arango/ocean/matlab/Damee/Data/gsaxis.topex';
cst_file='/export/home/arango/ocean/plot/Data/us_cst.dat';

%  Set plotting Map longitudes and Latitudes

LonMin=-80;
LonMax=-50;
LatMin=30;
LatMax=50;

switch ( damee ),
  case 'g4_04'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_04.nc';
    Oname='GS_path4_04.avg';
    Tindex=25:1:60;
    mgrid='Damee #4: Run 04,';
  case 'g4_06'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e0/arango/Damee/grid4/damee4_avg_06.nc';
    Oname='GS_path4_06.avg';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 06,';
  case 'g4_07'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_07.nc';
    Oname='GS_path4_07.avg';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 07,';
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e1/arango/Damee/grid4/damee4_avg_17.nc';
    Oname='GS_path4_17.avg';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 17,';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_22_01.nc';
    Oname='GS_path4_22.avg';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 22,';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_24.nc';
    Oname='GS_path4_24.avg';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 24,';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_25.nc';
    Oname='GS_path4_25.avg';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 25,';
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_26_02.nc';
    Oname='GS_path4_26.avg';
    Tindex=25:1:60;
    mgrid='Damee #4: Run 26,';
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/e2/arango/Damee/grid4/damee4_avg_27_01.nc';
    Oname='GS_path4_27.avg';
    Tindex=85:1:120;
    mgrid='Damee #4: Run 27,';
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_avg_28.nc';
    Oname='GS_path4_28.avg';
    Tindex=68:1:103;
    mgrid='Damee #4: Run 28,';
  case 'g5_03'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_03.nc';
    Oname='GS_path5_03.avg';
    Tindex=85:1:120;
    mgrid='Damee #5: Run 03,';
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_avg_04.nc';
    Oname='GS_path5_04.avg';
    Tindex=85:1:120;
    mgrid='Damee #5: Run 04,';
  case 'g6_03'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    fname='/comop/arango/Damee/grid6/damee6_avg_03.nc';
    Oname='GS_path6_03.avg';
    Tindex=85:1:120;
    mgrid='Damee #6: Run 03,';
  case 'g6_04'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    fname='/comop/arango/Damee/grid6/damee6_avg_04.nc';
    Oname='GS_path6_04.avg';
    Tindex=85:1:120;
    mgrid='Damee #6: Run 04,';
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/n2/arango/Damee/grid6/damee6_avg_05.nc';
    Oname='GS_path6_05.avg';
    Tindex=85:1:120;
    mgrid='Damee #6: Run 05,';
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/coamps/grid6/damee6_avg_06.nc';
    Oname='GS_path6_06.avg';
    Tindex=85:1:120;
    mgrid='Damee #6: Run 06,';
  case 'g7_01'
    gname='/d15/arango/scrum/Damee/grid7/damee7_grid_a.nc';
    fname='/e2/arango/Damee/grid7/damee7_avg_01c.nc';
    Oname='GS_path7_01.avg';
    Tindex=13:1:36;
    mgrid='Damee #7: Run 01 ';
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_avg_05_y8-10.nc';
    cname='/comop/arango/benchmark/data/damee7_Lclm_b.nc';
    Tindex=1:1:36;
    mgrid='Damee #7: Run 05,';
   case 'g9_01'
    fname='/e2/arango/Damee/grid9/damee9_avg_01e.nc';
    gname='/d15/arango/scrum/Damee/grid9/damee9_grid_a.nc';
    Oname='GS_path9_01.avg';
    Tindex=1:1:36;
    mgrid='Damee #9: Run 01,';
end,

Text=' Gulf Stream North Wall (15C Isotherm at 200m)';

%-------------------------------------------------------------------------
%  Read in Gulf stream axis from GEOSAT.
%-------------------------------------------------------------------------

fid=fopen(geosat,'r');
if (fid < 0),
  error(['Cannot open ' geosat '.'])
end
 
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);

[f count]=fscanf(fid,'%g %g %g %g %g %g %g %g %g %g',[10 inf]);

Xgeo_nth=f(1:10:count);
Ygeo_nth=f(2:10:count);
Xgeo_nsd=f(3:10:count);
Ygeo_nsd=f(4:10:count);

Xgeo_avg=f(5:10:count);
Ygeo_avg=f(6:10:count);

Xgeo_sth=f(7:10:count);
Ygeo_sth=f(8:10:count);
Xgeo_ssd=f(9:10:count);
Ygeo_ssd=f(10:10:count);

fclose(fid);

%-------------------------------------------------------------------------
%  Read in Gulf stream axis from TOPEX.
%-------------------------------------------------------------------------

fid=fopen(topex,'r');
if (fid < 0),
  error(['Cannot open ' topex '.'])
end
 
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);
line=fgetl(fid);

[f count]=fscanf(fid,'%g %g %g %g %g %g %g %g %g %g',[10 inf]);

Xpex_nth=f(1:10:count);
Ypex_nth=f(2:10:count);
Xpex_nsd=f(3:10:count);
Ypex_nsd=f(4:10:count);

Xpex_avg=f(5:10:count);
Ypex_avg=f(6:10:count);

Xpex_sth=f(7:10:count);
Ypex_sth=f(8:10:count);
Xpex_ssd=f(9:10:count);
Ypex_ssd=f(10:10:count);

fclose(fid);

%-------------------------------------------------------------------------
%  Create output ASCII file.
%-------------------------------------------------------------------------

if (IWRITE),
  fid=fopen(Oname,'w');
  if (fid < 0);
    error(['Cannot create ' Oname '.']);
  end,
end,

%-------------------------------------------------------------------------
%  Initialize Mercator Map.
%-------------------------------------------------------------------------

if (IPLOT),
  m_proj('Mercator','long',[LonMin LonMax],'lat',[LatMin LatMax]);
 
  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=spval);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  [Xcst,Ycst]=m_ll2xy(cstlon,cstlat,'point');

  [Xmin,Ymin]=m_ll2xy(LonMin,LatMin);
  [Xmax,Ymax]=m_ll2xy(LonMax,LatMax);

  figure(1);
  set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
          'Xtick',[],'Ytick',[]);
  hold on;
  draw_cst(Xcst,Ycst,'k');
  title(strcat(mgrid,Text));
  m_grid('xtick',[LonMin+2:2:LonMax],'ytick',[LatMin:2:LatMax]);
  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1]);
end,

%-------------------------------------------------------------------------
%  Extract the Gulf Stream Northern Wall from each time record.
%-------------------------------------------------------------------------

month=0;
nrec=length(Tindex);

for n=1:nrec;

  time=Tindex(n);
  disp(['Processing time record: ',num2str(time)]);
  figure(2); 
  [X,Y,x,y,t]=iso_lines(fname,vname,time,isoval,depth);

% Plot Northern wall.

  if (IPLOT),
    figure(1);
    [Xpath,Ypath]=m_ll2xy(X,Y,'point');
    plot(Xpath,Ypath,'r');
  end,

% Write Northern wall to output file.

  if (IWRITE),
    month=month+1;
    lon=X; lat=Y;
    ind=find(lat < lat_min | lat > lat_max); lon(ind)=[]; lat(ind)=[];
    ind=find(lon < lon_min | lon > lon_max); lon(ind)=[]; lat(ind)=[];
    ind=find(isnan(lon)); lon(ind)=spv; lat(ind)=spv;
    fprintf(fid,'%i %i\n',month,length(lon));
    fprintf(fid,'%9.4f ',lon);
    fprintf(fid,'\n');
    fprintf(fid,'%9.4f ',lat);
    fprintf(fid,'\n');
  end,
end,

%  Draw TOPEX or GEOSAT mean paths.

if (IPLOT),
  figure(1);
  if (TOPEX),
    [X,Y]=m_ll2xy(Xpex_nth,Ypex_nth,'point');
    plot(X,Y,'k');
    [X,Y]=m_ll2xy(Xpex_avg,Ypex_avg,'point');
    plot(X,Y,'k');
    [X,Y]=m_ll2xy(Xpex_sth,Ypex_sth,'point');
    plot(X,Y,'k');
  end
  if (GEOSAT),
    [X,Y]=m_ll2xy(Xgeo_nth,Ygeo_nth,'point');
    plot(X,Y,'k');
    [X,Y]=m_ll2xy(Xgeo_avg,Ygeo_avg,'point');
    plot(X,Y,'k');
    [X,Y]=m_ll2xy(Xgeo_sth,Ygeo_sth,'point');
    plot(X,Y,'k');
  end
  print -dpsc gs_path.ps
end

if (IWRITE),
  fclose(fid);
end,
