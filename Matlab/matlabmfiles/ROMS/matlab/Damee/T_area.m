%  This function computes area integrated temperature and salinity
%  profiles in the specified region.

global IPRINT


IPRINT=0;
IPLOT=1;
IPLOT2=0;
damee='g6_06';
interpolate=1;

Llon=-97;
Rlon=-10;
Blat=9;
Tlat=47;
delta=0.5;

RangeLab=[' Llon= ',num2str(Llon), ...
         ', Rlon= ',num2str(Rlon), ...
         ', Blat= ',num2str(Blat), ...
         ', Tlat= ',num2str(Tlat)];

switch ( damee ),
  case 'g4_17'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_17.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Damee #4: Run 17';
  case 'g4_19.1'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_19_01.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1;
    mgrid='Damee #4: Run 19_01';
  case 'g4_20'
    gname='/d15/arango/scrum/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_20.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1;
    mgrid='Damee #4: Run 20';
  case 'g4_22.1'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_22.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1;
    mgrid='Damee #4: Run 22';
  case 'g4_23'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_23.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1;
    mgrid='Damee #4: Run 23';
  case 'g4_24'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_24.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Damee #4: Run 24';
  case 'g4_25'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_25.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Damee #4: Run 25';
  case 'g4_26'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_26.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Damee #4: Run 26';
  case 'g4_27'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_27.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Damee #4: Run 27';
  case 'g4_28'
    gname='/e1/arango/Damee/grid4/damee4_grid_a.nc';
    fname='/comop/arango/Damee/grid4/damee4_yavg_28.nc';
    cname='/e2/arango/Damee/grid4/damee4_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Damee #4: Run 28';
  case 'g5_03'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_yavg_03.nc';
    cname='/d15/arango/scrum/Damee/grid5/damee5_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Damee #5: Run 03,';
  case 'g5_04'
    gname='/d15/arango/scrum/Damee/grid5/damee5_grid_a.nc';
    fname='/comop/arango/Damee/grid5/damee5_yavg_04.nc';
    cname='/d15/arango/scrum/Damee/grid5/damee5_Lclm_a.nc';
    Tindex=1:1:12;
    mgrid='Damee #5: Run 04,';
  case 'g6_03'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    fname='/comop/arango/Damee/grid6/damee6_yavg_03.nc';
    cname='/d15/arango/scrum/Damee/grid6/damee6_Lclm_b.nc';
    Tindex=1:1:12;
    mgrid='Damee #6: Run 03';
  case 'g6_04'
    gname='/d15/arango/scrum/Damee/grid6/damee6_grid_b.nc';
    fname='/comop/arango/Damee/grid6/damee6_yavg_04.nc';
    cname='/d15/arango/scrum/Damee/grid6/damee6_Lclm_b.nc';
    Tindex=1:1:12;
    mgrid='Damee #6: Run 04';
  case 'g6_05'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/n2/arango/Damee/grid6/damee6_yavg_05.nc';
    cname='/n2/arango/Damee/grid6/damee6_Lclm_c.nc';
    Tindex=1:1:12;
    mgrid='Damee #6: Run 05';
  case 'g6_06'
    gname='/n2/arango/Damee/grid6/damee6_grid_c.nc';
    fname='/coamps/grid6/damee6_yavg_06.nc';
    cname='/n2/arango/Damee/grid6/damee6_Lclm_c.nc';
    Tindex=1:1:12;
    mgrid='Damee #6: Run 06';
  case 'g7_05'
    gname='/comop/arango/benchmark/data/damee7_grid_b.nc';
    fname='/comop/arango/Damee/grid7/damee7_yavg_05.nc';
    cname='/comop/arango/benchmark/data/damee7_Lclm_b.nc';
    Tindex=1:1:12;
   mgrid='Damee #7: Run 05';
end,


%--------------------------------------------------------------------------
%  Compute area averaged temperature and salinity profiles.
%--------------------------------------------------------------------------

if (interpolate),
  [Z,T,S]=tsavg(gname,fname,Tindex,Llon,Rlon,Blat,Tlat);
  [Zlev,Tlev,Slev]=tsavg(gname,cname,[1:1:12],Llon,Rlon,Blat,Tlat);
else,
  [Z,T,S]=tsavg(gname,fname,Tindex);
  [Zlev,Tlev,Slev]=tsavg(gname,cname,[1:1:12]);
end,

%--------------------------------------------------------------------------
%  Plot area averaged temperature and salinity profiles.
%--------------------------------------------------------------------------

if (IPLOT),

  figure;

  subplot(1,2,1);
  plot(T,Z,'k',Tlev,Zlev,'r');
  grid on;
  set(gca,'Ylim',[-1000 0]);
  title('Temperature');
  if (interpolate),
    xlabel({[mgrid],[RangeLab]});
  else
    xlabel([mgrid]);
  end,
  ylabel('depth (m)');

  subplot(1,2,2);
  plot(S,Z,'k',Slev,Zlev,'r');
  grid on;
  set(gca,'Ylim',[-1000 0]);
  title('Salinity');
  if (interpolate),
    xlabel({[mgrid],[RangeLab]});
  else
    xlabel([mgrid]);
  end,
  ylabel('depth (m)');

  print -dpsc ts_area.ps

  figure;

  subplot(1,2,1);
  plot(T,Z,'k',Tlev,Zlev,'r');
  grid on;
  set(gca,'Ylim',[-5000 -1000]);
  title('Temperature');
  if (interpolate),
    xlabel({[mgrid],[RangeLab]});
  else
    xlabel([mgrid]);
  end,
  ylabel('depth (m)');

  subplot(1,2,2);
  plot(S,Z,'k',Slev,Zlev,'r');
  grid on;
  set(gca,'Ylim',[-5000 -1000]);
  title('Salinity');
  if (interpolate),
    xlabel({[mgrid],[RangeLab]});
  else
    xlabel([mgrid]);
  end,

  print -dpsc -append ts_area.ps

end,
