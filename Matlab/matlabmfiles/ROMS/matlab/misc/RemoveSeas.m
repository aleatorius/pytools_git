function field2 = RemoveSeas(field,month,varargin);  

if nargin > 2
  n = varargin{1}
else
n=length(size(field));
end
field2=field*nan;

if n == 1
   for imon =1:12
      in=find(month == imon);
	fieldm(imon) = meanNaN(field(in),1);
   end
   
   for it = 1 : length(field)
      imon = month(it);
	field2(it) = field(it) - fieldm(imon);
   end
end


if n == 2
   for imon =1:12
      in=find(month == imon);
	fieldm(:,imon) = meanNaN(field(:,in),2);
   end
   
   for it = 1 : size(field,2)
      imon = month(it);
	field2(:,it) = field(:,it) - fieldm(:,imon);
   end
end

if n == 3
   for imon =1:12
      in=find(month == imon);
	fieldm(:,:,imon) = meanNaN(field(:,:,in),3);
   end
   
   for it = 1 : size(field,3)
      imon = month(it);
	field2(:,:,it) = field(:,:,it) - fieldm(:,:,imon);
   end
end
