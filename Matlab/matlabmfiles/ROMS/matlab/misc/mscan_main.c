/*
 * MATLAB Compiler: 2.1
 * Date: Mon Oct  1 13:36:46 2001
 * Arguments: "-B" "macro_default" "-O" "all" "-O" "fold_scalar_mxarrays:on"
 * "-O" "fold_non_scalar_mxarrays:on" "-O" "optimize_integer_for_loops:on" "-O"
 * "array_indexing:on" "-O" "optimize_conditionals:on" "-m" "-W" "main" "-L"
 * "C" "-t" "-T" "link:exe" "-h" "libmmfile.mlib" "mscan.m" 
 */

#ifndef MLF_V2
#define MLF_V2 1
#endif

#include "libmatlb.h"
#include "mscan.h"
#include "libmmfile.h"

static mexFunctionTableEntry function_table[1]
  = { { "mscan", mlxMscan, -2, 0, &_local_function_table_mscan } };

static _mexInitTermTableEntry init_term_table[2]
  = { { libmmfileInitialize, libmmfileTerminate },
      { InitializeModule_mscan, TerminateModule_mscan } };

static _mex_information _main_info
  = { 1, 1, function_table, 0, NULL, 0, NULL, 2, init_term_table };

/*
 * The function "main" is a Compiler-generated main wrapper, suitable for
 * building a stand-alone application.  It calls a library function to perform
 * initialization, call the main function, and perform library termination.
 */
int main(int argc, const char * * argv) {
    return mclMain(argc, argv, mlxMscan, 0, &_main_info);
}
