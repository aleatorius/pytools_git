/*
 * MATLAB Compiler: 2.1
 * Date: Mon Oct  1 13:36:46 2001
 * Arguments: "-B" "macro_default" "-O" "all" "-O" "fold_scalar_mxarrays:on"
 * "-O" "fold_non_scalar_mxarrays:on" "-O" "optimize_integer_for_loops:on" "-O"
 * "array_indexing:on" "-O" "optimize_conditionals:on" "-m" "-W" "main" "-L"
 * "C" "-t" "-T" "link:exe" "-h" "libmmfile.mlib" "mscan.m" 
 */

#ifndef MLF_V2
#define MLF_V2 1
#endif

#ifndef __mscan_h
#define __mscan_h 1

#ifdef __cplusplus
extern "C" {
#endif

#include "libmatlb.h"

extern void InitializeModule_mscan(void);
extern void TerminateModule_mscan(void);
extern _mexLocalFunctionTable _local_function_table_mscan;

extern void mlfMscan(mxArray * name, ...);
extern void mlxMscan(int nlhs, mxArray * plhs[], int nrhs, mxArray * prhs[]);

#ifdef __cplusplus
}
#endif

#endif
