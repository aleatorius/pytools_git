/*
 * MATLAB Compiler: 2.1
 * Date: Mon Oct  1 13:36:46 2001
 * Arguments: "-B" "macro_default" "-O" "all" "-O" "fold_scalar_mxarrays:on"
 * "-O" "fold_non_scalar_mxarrays:on" "-O" "optimize_integer_for_loops:on" "-O"
 * "array_indexing:on" "-O" "optimize_conditionals:on" "-m" "-W" "main" "-L"
 * "C" "-t" "-T" "link:exe" "-h" "libmmfile.mlib" "mscan.m" 
 */
#include "mscan.h"
#include "libmatlbm.h"
#include "libmmfile.h"

static mxChar _array1_[128] = { 'R', 'u', 'n', '-', 't', 'i', 'm', 'e', ' ',
                                'E', 'r', 'r', 'o', 'r', ':', ' ', 'F', 'i',
                                'l', 'e', ':', ' ', 'm', 's', 'c', 'a', 'n',
                                ' ', 'L', 'i', 'n', 'e', ':', ' ', '1', ' ',
                                'C', 'o', 'l', 'u', 'm', 'n', ':', ' ', '1',
                                ' ', 'T', 'h', 'e', ' ', 'f', 'u', 'n', 'c',
                                't', 'i', 'o', 'n', ' ', '"', 'm', 's', 'c',
                                'a', 'n', '"', ' ', 'w', 'a', 's', ' ', 'c',
                                'a', 'l', 'l', 'e', 'd', ' ', 'w', 'i', 't',
                                'h', ' ', 'm', 'o', 'r', 'e', ' ', 't', 'h',
                                'a', 'n', ' ', 't', 'h', 'e', ' ', 'd', 'e',
                                'c', 'l', 'a', 'r', 'e', 'd', ' ', 'n', 'u',
                                'm', 'b', 'e', 'r', ' ', 'o', 'f', ' ', 'o',
                                'u', 't', 'p', 'u', 't', 's', ' ', '(', '0',
                                ')', '.' };
static mxArray * _mxarray0_;

static mxChar _array3_[152] = { 'R', 'u', 'n', '-', 't', 'i', 'm', 'e', ' ',
                                'E', 'r', 'r', 'o', 'r', ':', ' ', 'F', 'i',
                                'l', 'e', ':', ' ', 'm', 's', 'c', 'a', 'n',
                                '/', 'f', 'i', 'n', 'd', 'r', 'e', 'v', 'e',
                                'n', 'd', ' ', 'L', 'i', 'n', 'e', ':', ' ',
                                '1', '8', '8', ' ', 'C', 'o', 'l', 'u', 'm',
                                'n', ':', ' ', '1', ' ', 'T', 'h', 'e', ' ',
                                'f', 'u', 'n', 'c', 't', 'i', 'o', 'n', ' ',
                                '"', 'm', 's', 'c', 'a', 'n', '/', 'f', 'i',
                                'n', 'd', 'r', 'e', 'v', 'e', 'n', 'd', '"',
                                ' ', 'w', 'a', 's', ' ', 'c', 'a', 'l', 'l',
                                'e', 'd', ' ', 'w', 'i', 't', 'h', ' ', 'm',
                                'o', 'r', 'e', ' ', 't', 'h', 'a', 'n', ' ',
                                't', 'h', 'e', ' ', 'd', 'e', 'c', 'l', 'a',
                                'r', 'e', 'd', ' ', 'n', 'u', 'm', 'b', 'e',
                                'r', ' ', 'o', 'f', ' ', 'o', 'u', 't', 'p',
                                'u', 't', 's', ' ', '(', '1', ')', '.' };
static mxArray * _mxarray2_;

static mxChar _array5_[151] = { 'R', 'u', 'n', '-', 't', 'i', 'm', 'e', ' ',
                                'E', 'r', 'r', 'o', 'r', ':', ' ', 'F', 'i',
                                'l', 'e', ':', ' ', 'm', 's', 'c', 'a', 'n',
                                '/', 'f', 'i', 'n', 'd', 'r', 'e', 'v', 'e',
                                'n', 'd', ' ', 'L', 'i', 'n', 'e', ':', ' ',
                                '1', '8', '8', ' ', 'C', 'o', 'l', 'u', 'm',
                                'n', ':', ' ', '1', ' ', 'T', 'h', 'e', ' ',
                                'f', 'u', 'n', 'c', 't', 'i', 'o', 'n', ' ',
                                '"', 'm', 's', 'c', 'a', 'n', '/', 'f', 'i',
                                'n', 'd', 'r', 'e', 'v', 'e', 'n', 'd', '"',
                                ' ', 'w', 'a', 's', ' ', 'c', 'a', 'l', 'l',
                                'e', 'd', ' ', 'w', 'i', 't', 'h', ' ', 'm',
                                'o', 'r', 'e', ' ', 't', 'h', 'a', 'n', ' ',
                                't', 'h', 'e', ' ', 'd', 'e', 'c', 'l', 'a',
                                'r', 'e', 'd', ' ', 'n', 'u', 'm', 'b', 'e',
                                'r', ' ', 'o', 'f', ' ', 'i', 'n', 'p', 'u',
                                't', 's', ' ', '(', '1', ')', '.' };
static mxArray * _mxarray4_;

static mxChar _array7_[154] = { 'R', 'u', 'n', '-', 't', 'i', 'm', 'e', ' ',
                                'E', 'r', 'r', 'o', 'r', ':', ' ', 'F', 'i',
                                'l', 'e', ':', ' ', 'm', 's', 'c', 'a', 'n',
                                '/', 'p', 'r', '_', 's', 't', 'r', '_', 't',
                                'r', 'i', 'm', ' ', 'L', 'i', 'n', 'e', ':',
                                ' ', '2', '0', '5', ' ', 'C', 'o', 'l', 'u',
                                'm', 'n', ':', ' ', '1', ' ', 'T', 'h', 'e',
                                ' ', 'f', 'u', 'n', 'c', 't', 'i', 'o', 'n',
                                ' ', '"', 'm', 's', 'c', 'a', 'n', '/', 'p',
                                'r', '_', 's', 't', 'r', '_', 't', 'r', 'i',
                                'm', '"', ' ', 'w', 'a', 's', ' ', 'c', 'a',
                                'l', 'l', 'e', 'd', ' ', 'w', 'i', 't', 'h',
                                ' ', 'm', 'o', 'r', 'e', ' ', 't', 'h', 'a',
                                'n', ' ', 't', 'h', 'e', ' ', 'd', 'e', 'c',
                                'l', 'a', 'r', 'e', 'd', ' ', 'n', 'u', 'm',
                                'b', 'e', 'r', ' ', 'o', 'f', ' ', 'o', 'u',
                                't', 'p', 'u', 't', 's', ' ', '(', '1', ')',
                                '.' };
static mxArray * _mxarray6_;

static mxChar _array9_[153] = { 'R', 'u', 'n', '-', 't', 'i', 'm', 'e', ' ',
                                'E', 'r', 'r', 'o', 'r', ':', ' ', 'F', 'i',
                                'l', 'e', ':', ' ', 'm', 's', 'c', 'a', 'n',
                                '/', 'p', 'r', '_', 's', 't', 'r', '_', 't',
                                'r', 'i', 'm', ' ', 'L', 'i', 'n', 'e', ':',
                                ' ', '2', '0', '5', ' ', 'C', 'o', 'l', 'u',
                                'm', 'n', ':', ' ', '1', ' ', 'T', 'h', 'e',
                                ' ', 'f', 'u', 'n', 'c', 't', 'i', 'o', 'n',
                                ' ', '"', 'm', 's', 'c', 'a', 'n', '/', 'p',
                                'r', '_', 's', 't', 'r', '_', 't', 'r', 'i',
                                'm', '"', ' ', 'w', 'a', 's', ' ', 'c', 'a',
                                'l', 'l', 'e', 'd', ' ', 'w', 'i', 't', 'h',
                                ' ', 'm', 'o', 'r', 'e', ' ', 't', 'h', 'a',
                                'n', ' ', 't', 'h', 'e', ' ', 'd', 'e', 'c',
                                'l', 'a', 'r', 'e', 'd', ' ', 'n', 'u', 'm',
                                'b', 'e', 'r', ' ', 'o', 'f', ' ', 'i', 'n',
                                'p', 'u', 't', 's', ' ', '(', '1', ')', '.' };
static mxArray * _mxarray8_;
static mxArray * _mxarray10_;
static mxArray * _mxarray11_;
static mxArray * _mxarray12_;
static mxArray * _mxarray13_;

static mxChar _array15_[1] = { 'R' };
static mxArray * _mxarray14_;

static mxChar _array17_[2] = { 'N', 'I' };
static mxArray * _mxarray16_;

static mxChar _array19_[1] = { 'y' };
static mxArray * _mxarray18_;

static mxChar _array21_[30] = { 'T', 'h', 'e', 'n', ' ', 'y', 'o', 'u',
                                ' ', 'b', 'e', 't', 't', 'e', 'r', ' ',
                                'd', 'o', ' ', 't', 'h', 'a', 't', ' ',
                                'f', 'i', 'r', 's', 't', '!' };
static mxArray * _mxarray20_;

static mxChar _array23_[27] = { 'P', 'r', 'o', 'c', 'e', 's', 's', 'i', 'n',
                                'g', ' ', 'f', 'o', 'l', 'l', 'o', 'w', 'i',
                                'n', 'g', ' ', 'f', 'i', 'l', 'e', 's', ':' };
static mxArray * _mxarray22_;

static mxChar _array25_[2] = { 'm', '.' };
static mxArray * _mxarray24_;

static mxChar _array27_[19] = { 'N', 'o', 't', ' ', 'm', 'a', 't',
                                'l', 'a', 'b', ' ', '*', '.', 'm',
                                ' ', 'f', 'i', 'l', 'e' };
static mxArray * _mxarray26_;

static mxChar _array29_[1] = { '.' };
static mxArray * _mxarray28_;

static mxChar _array31_[2] = { 'i', 'f' };
static mxArray * _mxarray30_;

static mxChar _array33_[4] = { 'e', 'l', 's', 'e' };
static mxArray * _mxarray32_;

static mxChar _array35_[6] = { 'e', 'l', 's', 'e', 'i', 'f' };
static mxArray * _mxarray34_;

static mxChar _array37_[8] = { 'f', 'u', 'n', 'c', 't', 'i', 'o', 'n' };
static mxArray * _mxarray36_;

static mxChar _array39_[3] = { 'f', 'o', 'r' };
static mxArray * _mxarray38_;

static mxChar _array41_[6] = { 's', 'w', 'i', 't', 'c', 'h' };
static mxArray * _mxarray40_;

static mxChar _array43_[9] = { 'o', 't', 'h', 'e', 'r', 'w', 'i', 's', 'e' };
static mxArray * _mxarray42_;

static mxChar _array45_[5] = { 'w', 'h', 'i', 'l', 'e' };
static mxArray * _mxarray44_;

static mxChar _array47_[3] = { 't', 'r', 'y' };
static mxArray * _mxarray46_;

static mxChar _array49_[5] = { 'c', 'a', 't', 'c', 'h' };
static mxArray * _mxarray48_;

static mxChar _array51_[3] = { 'e', 'n', 'd' };
static mxArray * _mxarray50_;

static mxChar _array53_[3] = { 'd', 'n', 'e' };
static mxArray * _mxarray52_;

static double _array55_[255] = { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0,
                                 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0,
                                 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0,
                                 26.0, 27.0, 28.0, 29.0, 30.0, 31.0, 32.0, 33.0,
                                 34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0, 41.0,
                                 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0,
                                 50.0, 51.0, 52.0, 53.0, 54.0, 55.0, 56.0, 57.0,
                                 58.0, 59.0, 60.0, 61.0, 62.0, 63.0, 64.0, 65.0,
                                 66.0, 67.0, 68.0, 69.0, 70.0, 71.0, 72.0, 73.0,
                                 74.0, 75.0, 76.0, 77.0, 78.0, 79.0, 80.0, 81.0,
                                 82.0, 83.0, 84.0, 85.0, 86.0, 87.0, 88.0, 89.0,
                                 90.0, 91.0, 92.0, 93.0, 94.0, 95.0, 96.0, 97.0,
                                 98.0, 99.0, 100.0, 101.0, 102.0, 103.0, 104.0,
                                 105.0, 106.0, 107.0, 108.0, 109.0, 110.0,
                                 111.0, 112.0, 113.0, 114.0, 115.0, 116.0,
                                 117.0, 118.0, 119.0, 120.0, 121.0, 122.0,
                                 123.0, 124.0, 125.0, 126.0, 127.0, 128.0,
                                 129.0, 130.0, 131.0, 132.0, 133.0, 134.0,
                                 135.0, 136.0, 137.0, 138.0, 139.0, 140.0,
                                 141.0, 142.0, 143.0, 144.0, 145.0, 146.0,
                                 147.0, 148.0, 149.0, 150.0, 151.0, 152.0,
                                 153.0, 154.0, 155.0, 156.0, 157.0, 158.0,
                                 159.0, 160.0, 161.0, 162.0, 163.0, 164.0,
                                 165.0, 166.0, 167.0, 168.0, 169.0, 170.0,
                                 171.0, 172.0, 173.0, 174.0, 175.0, 176.0,
                                 177.0, 178.0, 179.0, 180.0, 181.0, 182.0,
                                 183.0, 184.0, 185.0, 186.0, 187.0, 188.0,
                                 189.0, 190.0, 191.0, 192.0, 193.0, 194.0,
                                 195.0, 196.0, 197.0, 198.0, 199.0, 200.0,
                                 201.0, 202.0, 203.0, 204.0, 205.0, 206.0,
                                 207.0, 208.0, 209.0, 210.0, 211.0, 212.0,
                                 213.0, 214.0, 215.0, 216.0, 217.0, 218.0,
                                 219.0, 220.0, 221.0, 222.0, 223.0, 224.0,
                                 225.0, 226.0, 227.0, 228.0, 229.0, 230.0,
                                 231.0, 232.0, 233.0, 234.0, 235.0, 236.0,
                                 237.0, 238.0, 239.0, 240.0, 241.0, 242.0,
                                 243.0, 244.0, 245.0, 246.0, 247.0, 248.0,
                                 249.0, 250.0, 251.0, 252.0, 253.0, 254.0,
                                 255.0 };
static mxArray * _mxarray54_;

static mxChar _array57_[1] = { ' ' };
static mxArray * _mxarray56_;

static mxChar _array59_[1] = { 'r' };
static mxArray * _mxarray58_;

static mxChar _array61_[1] = { 'w' };
static mxArray * _mxarray60_;

static mxChar _array63_[2] = { 0x005c, 'n' };
static mxArray * _mxarray62_;

static mxChar _array65_[3] = { ' ', '(', ')' };
static mxArray * _mxarray64_;

static mxChar _array67_[5] = { 'e', 'x', 'a', 'c', 't' };
static mxArray * _mxarray66_;

static mxChar _array69_[1] = { '%' };
static mxArray * _mxarray68_;

static mxChar _array71_[4] = { '%', 's', 0x005c, 'n' };
static mxArray * _mxarray70_;

static mxChar _array73_[6] = { '%', 's', '%', 's', 0x005c, 'n' };
static mxArray * _mxarray72_;

static mxChar _array75_[4] = { 'c', 'a', 's', 'e' };
static mxArray * _mxarray74_;

static mxChar _array77_[2] = { '%', '%' };
static mxArray * _mxarray76_;

static mxChar _array79_[3] = { '.', '.', '.' };
static mxArray * _mxarray78_;
static mxArray * _mxarray80_;
static mxArray * _mxarray81_;

static mxChar _array83_[3] = { ' ', ',', ';' };
static mxArray * _mxarray82_;
static mxArray * _mxarray84_;
static mxArray * _mxarray85_;

void InitializeModule_mscan(void) {
    _mxarray0_ = mclInitializeString(128, _array1_);
    _mxarray2_ = mclInitializeString(152, _array3_);
    _mxarray4_ = mclInitializeString(151, _array5_);
    _mxarray6_ = mclInitializeString(154, _array7_);
    _mxarray8_ = mclInitializeString(153, _array9_);
    _mxarray10_ = mclInitializeDouble(0.0);
    _mxarray11_ = mclInitializeDouble(1.0);
    _mxarray12_ = mclInitializeDouble(2.0);
    _mxarray13_ = mclInitializeDoubleVector(0, 0, (double *)NULL);
    _mxarray14_ = mclInitializeString(1, _array15_);
    _mxarray16_ = mclInitializeString(2, _array17_);
    _mxarray18_ = mclInitializeString(1, _array19_);
    _mxarray20_ = mclInitializeString(30, _array21_);
    _mxarray22_ = mclInitializeString(27, _array23_);
    _mxarray24_ = mclInitializeString(2, _array25_);
    _mxarray26_ = mclInitializeString(19, _array27_);
    _mxarray28_ = mclInitializeString(1, _array29_);
    _mxarray30_ = mclInitializeString(2, _array31_);
    _mxarray32_ = mclInitializeString(4, _array33_);
    _mxarray34_ = mclInitializeString(6, _array35_);
    _mxarray36_ = mclInitializeString(8, _array37_);
    _mxarray38_ = mclInitializeString(3, _array39_);
    _mxarray40_ = mclInitializeString(6, _array41_);
    _mxarray42_ = mclInitializeString(9, _array43_);
    _mxarray44_ = mclInitializeString(5, _array45_);
    _mxarray46_ = mclInitializeString(3, _array47_);
    _mxarray48_ = mclInitializeString(5, _array49_);
    _mxarray50_ = mclInitializeString(3, _array51_);
    _mxarray52_ = mclInitializeString(3, _array53_);
    _mxarray54_ = mclInitializeDoubleVector(1, 255, _array55_);
    _mxarray56_ = mclInitializeString(1, _array57_);
    _mxarray58_ = mclInitializeString(1, _array59_);
    _mxarray60_ = mclInitializeString(1, _array61_);
    _mxarray62_ = mclInitializeString(2, _array63_);
    _mxarray64_ = mclInitializeString(3, _array65_);
    _mxarray66_ = mclInitializeString(5, _array67_);
    _mxarray68_ = mclInitializeString(1, _array69_);
    _mxarray70_ = mclInitializeString(4, _array71_);
    _mxarray72_ = mclInitializeString(6, _array73_);
    _mxarray74_ = mclInitializeString(4, _array75_);
    _mxarray76_ = mclInitializeString(2, _array77_);
    _mxarray78_ = mclInitializeString(3, _array79_);
    _mxarray80_ = mclInitializeDouble(3.0);
    _mxarray81_ = mclInitializeDouble(1.5);
    _mxarray82_ = mclInitializeString(3, _array83_);
    _mxarray84_ = mclInitializeDouble(39.0);
    _mxarray85_ = mclInitializeDouble(-1.0);
}

void TerminateModule_mscan(void) {
    mxDestroyArray(_mxarray85_);
    mxDestroyArray(_mxarray84_);
    mxDestroyArray(_mxarray82_);
    mxDestroyArray(_mxarray81_);
    mxDestroyArray(_mxarray80_);
    mxDestroyArray(_mxarray78_);
    mxDestroyArray(_mxarray76_);
    mxDestroyArray(_mxarray74_);
    mxDestroyArray(_mxarray72_);
    mxDestroyArray(_mxarray70_);
    mxDestroyArray(_mxarray68_);
    mxDestroyArray(_mxarray66_);
    mxDestroyArray(_mxarray64_);
    mxDestroyArray(_mxarray62_);
    mxDestroyArray(_mxarray60_);
    mxDestroyArray(_mxarray58_);
    mxDestroyArray(_mxarray56_);
    mxDestroyArray(_mxarray54_);
    mxDestroyArray(_mxarray52_);
    mxDestroyArray(_mxarray50_);
    mxDestroyArray(_mxarray48_);
    mxDestroyArray(_mxarray46_);
    mxDestroyArray(_mxarray44_);
    mxDestroyArray(_mxarray42_);
    mxDestroyArray(_mxarray40_);
    mxDestroyArray(_mxarray38_);
    mxDestroyArray(_mxarray36_);
    mxDestroyArray(_mxarray34_);
    mxDestroyArray(_mxarray32_);
    mxDestroyArray(_mxarray30_);
    mxDestroyArray(_mxarray28_);
    mxDestroyArray(_mxarray26_);
    mxDestroyArray(_mxarray24_);
    mxDestroyArray(_mxarray22_);
    mxDestroyArray(_mxarray20_);
    mxDestroyArray(_mxarray18_);
    mxDestroyArray(_mxarray16_);
    mxDestroyArray(_mxarray14_);
    mxDestroyArray(_mxarray13_);
    mxDestroyArray(_mxarray12_);
    mxDestroyArray(_mxarray11_);
    mxDestroyArray(_mxarray10_);
    mxDestroyArray(_mxarray8_);
    mxDestroyArray(_mxarray6_);
    mxDestroyArray(_mxarray4_);
    mxDestroyArray(_mxarray2_);
    mxDestroyArray(_mxarray0_);
}

static mxArray * mlfMscan_findrevend(mxArray * str);
static void mlxMscan_findrevend(int nlhs,
                                mxArray * plhs[],
                                int nrhs,
                                mxArray * prhs[]);
static mxArray * mlfMscan_pr_str_trim(mxArray * str);
static void mlxMscan_pr_str_trim(int nlhs,
                                 mxArray * plhs[],
                                 int nrhs,
                                 mxArray * prhs[]);
static void Mmscan(mxArray * name, mxArray * varargin);
static mxArray * Mmscan_findrevend(int nargout_, mxArray * str);
static mxArray * Mmscan_pr_str_trim(int nargout_, mxArray * str);

static mexFunctionTableEntry local_function_table_[2]
  = { { "findrevend", mlxMscan_findrevend, 1, 1, NULL },
      { "pr_str_trim", mlxMscan_pr_str_trim, 1, 1, NULL } };

_mexLocalFunctionTable _local_function_table_mscan
  = { 2, local_function_table_ };

/*
 * The function "mlfMscan" contains the normal interface for the "mscan"
 * M-function from file "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines
 * 1-188). This function processes any input arguments and passes them to the
 * implementation version of the function, appearing above.
 */
void mlfMscan(mxArray * name, ...) {
    mxArray * varargin = NULL;
    mlfVarargin(&varargin, name, 0);
    mlfEnterNewContext(0, -2, name, varargin);
    Mmscan(name, varargin);
    mlfRestorePreviousContext(0, 1, name);
    mxDestroyArray(varargin);
}

/*
 * The function "mlxMscan" contains the feval interface for the "mscan"
 * M-function from file "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines
 * 1-188). The feval function calls the implementation version of mscan through
 * this function. This function processes any input arguments and passes them
 * to the implementation version of the function, appearing above.
 */
void mlxMscan(int nlhs, mxArray * plhs[], int nrhs, mxArray * prhs[]) {
    mxArray * mprhs[2];
    int i;
    if (nlhs > 0) {
        mlfError(_mxarray0_);
    }
    for (i = 0; i < 1 && i < nrhs; ++i) {
        mprhs[i] = prhs[i];
    }
    for (; i < 1; ++i) {
        mprhs[i] = NULL;
    }
    mlfEnterNewContext(0, 1, mprhs[0]);
    mprhs[1] = NULL;
    mlfAssign(&mprhs[1], mclCreateVararginCell(nrhs - 1, prhs + 1));
    Mmscan(mprhs[0], mprhs[1]);
    mlfRestorePreviousContext(0, 1, mprhs[0]);
    mxDestroyArray(mprhs[1]);
}

/*
 * The function "mlfMscan_findrevend" contains the normal interface for the
 * "mscan/findrevend" M-function from file
 * "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines 188-205). This function
 * processes any input arguments and passes them to the implementation version
 * of the function, appearing above.
 */
static mxArray * mlfMscan_findrevend(mxArray * str) {
    int nargout = 1;
    mxArray * ret = mclGetUninitializedArray();
    mlfEnterNewContext(0, 1, str);
    ret = Mmscan_findrevend(nargout, str);
    mlfRestorePreviousContext(0, 1, str);
    return mlfReturnValue(ret);
}

/*
 * The function "mlxMscan_findrevend" contains the feval interface for the
 * "mscan/findrevend" M-function from file
 * "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines 188-205). The feval
 * function calls the implementation version of mscan/findrevend through this
 * function. This function processes any input arguments and passes them to the
 * implementation version of the function, appearing above.
 */
static void mlxMscan_findrevend(int nlhs,
                                mxArray * plhs[],
                                int nrhs,
                                mxArray * prhs[]) {
    mxArray * mprhs[1];
    mxArray * mplhs[1];
    int i;
    if (nlhs > 1) {
        mlfError(_mxarray2_);
    }
    if (nrhs > 1) {
        mlfError(_mxarray4_);
    }
    for (i = 0; i < 1; ++i) {
        mplhs[i] = mclGetUninitializedArray();
    }
    for (i = 0; i < 1 && i < nrhs; ++i) {
        mprhs[i] = prhs[i];
    }
    for (; i < 1; ++i) {
        mprhs[i] = NULL;
    }
    mlfEnterNewContext(0, 1, mprhs[0]);
    mplhs[0] = Mmscan_findrevend(nlhs, mprhs[0]);
    mlfRestorePreviousContext(0, 1, mprhs[0]);
    plhs[0] = mplhs[0];
}

/*
 * The function "mlfMscan_pr_str_trim" contains the normal interface for the
 * "mscan/pr_str_trim" M-function from file
 * "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines 205-216). This function
 * processes any input arguments and passes them to the implementation version
 * of the function, appearing above.
 */
static mxArray * mlfMscan_pr_str_trim(mxArray * str) {
    int nargout = 1;
    mxArray * ret = mclGetUninitializedArray();
    mlfEnterNewContext(0, 1, str);
    ret = Mmscan_pr_str_trim(nargout, str);
    mlfRestorePreviousContext(0, 1, str);
    return mlfReturnValue(ret);
}

/*
 * The function "mlxMscan_pr_str_trim" contains the feval interface for the
 * "mscan/pr_str_trim" M-function from file
 * "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines 205-216). The feval
 * function calls the implementation version of mscan/pr_str_trim through this
 * function. This function processes any input arguments and passes them to the
 * implementation version of the function, appearing above.
 */
static void mlxMscan_pr_str_trim(int nlhs,
                                 mxArray * plhs[],
                                 int nrhs,
                                 mxArray * prhs[]) {
    mxArray * mprhs[1];
    mxArray * mplhs[1];
    int i;
    if (nlhs > 1) {
        mlfError(_mxarray6_);
    }
    if (nrhs > 1) {
        mlfError(_mxarray8_);
    }
    for (i = 0; i < 1; ++i) {
        mplhs[i] = mclGetUninitializedArray();
    }
    for (i = 0; i < 1 && i < nrhs; ++i) {
        mprhs[i] = prhs[i];
    }
    for (; i < 1; ++i) {
        mprhs[i] = NULL;
    }
    mlfEnterNewContext(0, 1, mprhs[0]);
    mplhs[0] = Mmscan_pr_str_trim(nlhs, mprhs[0]);
    mlfRestorePreviousContext(0, 1, mprhs[0]);
    plhs[0] = mplhs[0];
}

/*
 * The function "Mmscan" is the implementation version of the "mscan"
 * M-function from file "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines
 * 1-188). It contains the actual compiled code for that M-function. It is a
 * static function and must only be called from one of the interface functions,
 * appearing below.
 */
/*
 * function mscan(name,varargin)
 */
static void Mmscan(mxArray * name, mxArray * varargin) {
    mexLocalFunctionTable save_local_function_table_ = mclSetCurrentLocalFunctionTable(
                                                         &_local_function_table_mscan);
    mxArray * y = mclGetUninitializedArray();
    mxArray * x = mclGetUninitializedArray();
    mxArray * filebuff = mclGetUninitializedArray();
    mxArray * buff = mclGetUninitializedArray();
    mxArray * line = mclGetUninitializedArray();
    mxArray * file = mclGetUninitializedArray();
    mxArray * names = mclGetUninitializedArray();
    mxArray * asciiconv = mclGetUninitializedArray();
    mxArray * end_op_at_end = mclGetUninitializedArray();
    mxArray * stop_op = mclGetUninitializedArray();
    mxArray * start_op = mclGetUninitializedArray();
    mxArray * dirnames = mclGetUninitializedArray();
    mxArray * extname = mclGetUninitializedArray();
    mxArray * filename = mclGetUninitializedArray();
    mxArray * dirname = mclGetUninitializedArray();
    mxArray * ans = mclGetUninitializedArray();
    mxArray * r = mclGetUninitializedArray();
    mxArray * a = mclGetUninitializedArray();
    mxArray * lastwasfun = mclGetUninitializedArray();
    mxArray * level = mclGetUninitializedArray();
    mxArray * casesub = mclGetUninitializedArray();
    mxArray * tbsize = mclGetUninitializedArray();
    mxArray * inter = mclGetUninitializedArray();
    mxArray * subdir = mclGetUninitializedArray();
    mclCopyArray(&name);
    mclCopyArray(&varargin);
    /*
     * %   MSCAN     Version 1.0 Copyright (C) Peter Rydesäter 1998-12-13 -
     * %
     * %             This makes your m-files look better and helps
     * %             you find non matching for,if,while,try,else
     * %             ,end . . .
     * %
     * %             Tested with matlab 5.x 
     * %
     * % Syntax: 
     * %
     * %   mscan name option
     * %   mscan(name,option)
     * %
     * %             name is filename with/without path and can also
     * %             hold wildcard (*) to work on multiple files. If an
     * %             optional string argument with an 'r' in is added
     * %             it will work on ALL sub directories.
     * %
     * %             Updates m-files, replaces tabs with spaces and
     * %             indents for block structures. Also fixes end of
     * %             line:       windows (^M ) <--> unix
     * %             It works with wildcards and can
     * %
     * %             Use with care. Backup your m-files first!
     * %
     * % 
     * %
     * % Example 1:
     * %             mscan /home/mrxx/matlab/*.m  r
     * %         OR
     * %             mscan('/home/mrxx/matlab/*.m','r')
     * %
     * %         Converts all *.m files in the path and sub
     * %         derectories.( Optional r.)
     * % 
     * % Example 2:
     * %             mscan('test*.m');
     * %         OR
     * %             mscan test*.m
     * %
     * %         Converts all m-files beginning with 'test' in
     * %         current dir. No sub directories.
     * %
     * %
     * %  This program is free software; you can redistribute it and/or
     * %  modify it under the terms of the GNU General Public License
     * %  as published by the Free Software Foundation; either version 2
     * %  of the License, or (at your option) any later version.
     * %
     * %  This program is distributed in the hope that it will be useful,
     * %  but WITHOUT ANY WARRANTY; without even the implied warranty of
     * %  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     * %  GNU General Public License for more details.
     * 
     * 
     * %  You should have received a copy of the GNU General Public License
     * %  along with this program; if not, write to the Free Software
     * %  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
     * %  02111-1307, USA.
     * %
     * %  Please send me an e-mail if you use this: 
     * %  Peter.Rydesater@ite.mh.se
     * %  Mitthögskolan
     * %  Östersund
     * %  Sweden
     * %  
     * 
     * subdir=0;
     */
    mlfAssign(&subdir, _mxarray10_);
    /*
     * inter=1;
     */
    mlfAssign(&inter, _mxarray11_);
    /*
     * tbsize=2;
     */
    mlfAssign(&tbsize, _mxarray12_);
    /*
     * casesub=2;
     */
    mlfAssign(&casesub, _mxarray12_);
    /*
     * level=0;
     */
    mlfAssign(&level, _mxarray10_);
    /*
     * lastwasfun=0;
     */
    mlfAssign(&lastwasfun, _mxarray10_);
    /*
     * 
     * for a=1:length(varargin),
     */
    {
        int v_ = mclForIntStart(1);
        int e_ = mclForIntEnd(
                   mlfScalar(mclLengthInt(mclVa(varargin, "varargin"))));
        if (v_ > e_) {
            mlfAssign(&a, _mxarray13_);
        } else {
            /*
             * if strcmp(upper(varargin{a}),'R'),
             * subdir=1;
             * elseif strcmp(upper(varargin{a}),'NI'),
             * inter=0;
             * end
             * end
             */
            for (; ; ) {
                if (mlfTobool(
                      mclVe(
                        mlfStrcmp(
                          mclVe(
                            mclFeval(
                              mclValueVarargout(),
                              mlxUpper,
                              mclVe(
                                mlfIndexRef(
                                  mclVsa(varargin, "varargin"),
                                  "{?}",
                                  mlfScalar(v_))),
                              NULL)),
                          _mxarray14_)))) {
                    mlfAssign(&subdir, _mxarray11_);
                } else if (mlfTobool(
                             mclVe(
                               mlfStrcmp(
                                 mclVe(
                                   mclFeval(
                                     mclValueVarargout(),
                                     mlxUpper,
                                     mclVe(
                                       mlfIndexRef(
                                         mclVsa(varargin, "varargin"),
                                         "{?}",
                                         mlfScalar(v_))),
                                     NULL)),
                                 _mxarray16_)))) {
                    mlfAssign(&inter, _mxarray10_);
                }
                if (v_ == e_) {
                    break;
                }
                ++v_;
            }
            mlfAssign(&a, mlfScalar(v_));
        }
    }
    /*
     * 
     * if inter,
     */
    if (mlfTobool(mclVv(inter, "inter"))) {
        /*
         * %r=input('Have you backed up your *.m files or some outher safety rutine? (y/n)','s');
         * r='y'; 
         */
        mlfAssign(&r, _mxarray18_);
        /*
         * if strncmp(r,'y',1)==0,
         */
        if (mclEqBool(
              mclVe(mlfStrncmp(mclVv(r, "r"), _mxarray18_, _mxarray11_)),
              _mxarray10_)) {
            /*
             * disp('Then you better do that first!');
             */
            mlfDisp(_mxarray20_);
            /*
             * return;
             */
            goto return_;
        /*
         * end
         */
        }
        /*
         * 
         * disp('Processing following files:');
         */
        mlfDisp(_mxarray22_);
        /*
         * 
         * 
         * if strncmp(findrevend(name),'m.',2)==0,
         */
        if (mclEqBool(
              mclVe(
                mlfStrncmp(
                  mclVe(mlfMscan_findrevend(mclVa(name, "name"))),
                  _mxarray24_,
                  _mxarray12_)),
              _mxarray10_)) {
            /*
             * disp('Not matlab *.m file');
             */
            mlfDisp(_mxarray26_);
            /*
             * return;
             */
            goto return_;
        /*
         * end
         */
        }
    /*
     * end
     */
    }
    /*
     * 
     * if subdir,
     */
    if (mlfTobool(mclVv(subdir, "subdir"))) {
        /*
         * [dirname,filename,extname]=fileparts(name);
         */
        mlfAssign(
          &dirname,
          mlfFileparts(&filename, &extname, NULL, mclVa(name, "name")));
        /*
         * dirnames=dir(dirname);
         */
        mlfAssign(&dirnames, mlfNDir(1, mclVv(dirname, "dirname")));
        /*
         * for a=1:length(dirnames),
         */
        {
            int v_ = mclForIntStart(1);
            int e_ = mclForIntEnd(
                       mlfScalar(mclLengthInt(mclVv(dirnames, "dirnames"))));
            if (v_ > e_) {
                mlfAssign(&a, _mxarray13_);
            } else {
                /*
                 * if strncmp(dirnames(a).name,'.',1),        % Not .* directorys
                 * elseif dirnames(a).isdir,                  % Recursive call if it's a directory
                 * mscan(fullfile(dirnames(a).name,[filename extname]),'R','NI');
                 * end
                 * end
                 */
                for (; ; ) {
                    if (mlfTobool(
                          mclVe(
                            mclFeval(
                              mclValueVarargout(),
                              mlxStrncmp,
                              mclVe(
                                mlfIndexRef(
                                  mclVsv(dirnames, "dirnames"),
                                  "(?).name",
                                  mlfScalar(v_))),
                              _mxarray28_,
                              _mxarray11_,
                              NULL)))) {
                    } else if (mlfTobool(
                                 mclVe(
                                   mlfIndexRef(
                                     mclVsv(dirnames, "dirnames"),
                                     "(?).isdir",
                                     mlfScalar(v_))))) {
                        mlfMscan(
                          mclVe(
                            mlfFullfile(
                              mclVe(
                                mlfIndexRef(
                                  mclVsv(dirnames, "dirnames"),
                                  "(?).name",
                                  mlfScalar(v_))),
                              mlfHorzcat(
                                mclVv(filename, "filename"),
                                mclVv(extname, "extname"),
                                NULL),
                              NULL)),
                          _mxarray14_, _mxarray16_, NULL);
                    }
                    if (v_ == e_) {
                        break;
                    }
                    ++v_;
                }
                mlfAssign(&a, mlfScalar(v_));
            }
        }
    /*
     * end
     */
    }
    /*
     * 
     * % Operatiors for start of move in...
     * start_op=strvcat('if','else','elseif','function','for',...
     */
    mlfAssign(
      &start_op,
      mlfStrvcat(
        _mxarray30_,
        _mxarray32_,
        _mxarray34_,
        _mxarray36_,
        _mxarray38_,
        _mxarray40_,
        _mxarray42_,
        _mxarray44_,
        _mxarray46_,
        _mxarray48_,
        NULL));
    /*
     * 'switch','otherwise','while','try','catch');
     * 
     * % Operation for end of move in...
     * stop_op=strvcat('end','else','elseif','otherwise','catch');
     */
    mlfAssign(
      &stop_op,
      mlfStrvcat(
        _mxarray50_, _mxarray32_, _mxarray34_, _mxarray42_, _mxarray48_, NULL));
    /*
     * 
     * % Reversed ordered letters for operators ending move in in the end of line.
     * end_op_at_end=strvcat('dne');
     */
    mlfAssign(&end_op_at_end, mlfStrvcat(_mxarray52_, NULL));
    /*
     * 
     * % Converts a tab to a space
     * asciiconv=char([1:255]);
     */
    mlfAssign(&asciiconv, mlfChar(_mxarray54_, NULL));
    /*
     * asciiconv(9)=' ';
     */
    mclIntArrayAssign1(&asciiconv, _mxarray56_, 9);
    /*
     * 
     * names=dir(name);
     */
    mlfAssign(&names, mlfNDir(1, mclVa(name, "name")));
    /*
     * 
     * for a=1:length(names),
     */
    {
        int v_ = mclForIntStart(1);
        int e_ = mclForIntEnd(mlfScalar(mclLengthInt(mclVv(names, "names"))));
        if (v_ > e_) {
            mlfAssign(&a, _mxarray13_);
        } else {
            /*
             * disp(names(a).name);
             * file=fopen(names(a).name,'r');
             * line=1;
             * while(1),
             * buff=fgetl(file);
             * if ~isstr(buff), break, end %here is a coment at the end
             * filebuff{line}=pr_str_trim(asciiconv(double(buff)));
             * line=line+1;
             * end
             * fclose(file);
             * 
             * level=0;
             * file=fopen(names(a).name,'w');
             * for line=1:length(filebuff),
             * if length(filebuff)<=line & length(filebuff{line})==0,
             * fprintf(file,'\n');
             * else
             * [x,y]=strtok(filebuff{line},' ()');
             * x=lower(pr_str_trim(x));
             * if strcmp('function',x),
             * level=0;
             * lastwasfun=1;
             * end
             * if strmatch(x,stop_op,'exact') & level>0 & length(x)>0,
             * level=level-1;
             * end
             * if strncmp(filebuff{line},'%%',2) | ...
             * (strncmp(filebuff{line},'%',1) & lastwasfun)
             * fprintf(file,'%s\n',filebuff{line});
             * else
             * fprintf(file,'%s%s\n',blanks(level*tbsize-casesub*strcmp('case',x)),filebuff{line});
             * if strcmp('function',x)
             * lastwasfun=1;
             * elseif length(x)==0
             * lastwasfun=lastwasfun; % Do nothing empty line.
             * else
             * lastwasfun=0;
             * end
             * end
             * if strmatch(x,start_op,'exact') & length(x)>0,
             * level=level+1;
             * end
             * x=findrevend(filebuff{line});
             * if strncmp(x,'...',3),
             * fprintf(file,blanks(ceil(tbsize*1.5)));
             * end
             * [x,y]=strtok(x,' ,;');
             * if strmatch(x,end_op_at_end,'exact') & level>0 & length(x)>0 & length(deblank(y))>2,
             * level=level-1;
             * end
             * end
             * end
             * clear line
             * clear filebuff
             * fclose(file);
             * end
             */
            for (; ; ) {
                mclFeval(
                  mclAnsVarargout(),
                  mlxDisp,
                  mclVe(
                    mlfIndexRef(
                      mclVsv(names, "names"), "(?).name", mlfScalar(v_))),
                  NULL);
                mlfAssign(
                  &file,
                  mclFeval(
                    mclValueVarargout(),
                    mlxFopen,
                    mclVe(
                      mlfIndexRef(
                        mclVsv(names, "names"), "(?).name", mlfScalar(v_))),
                    _mxarray58_,
                    NULL));
                mlfAssign(&line, _mxarray11_);
                while (mlfTobool(_mxarray11_)) {
                    mlfAssign(&buff, mlfFgetl(mclVv(file, "file")));
                    if (mclNotBool(mclVe(mlfIsstr(mclVv(buff, "buff"))))) {
                        break;
                    }
                    mlfIndexAssign(
                      &filebuff,
                      "{?}",
                      mclVsv(line, "line"),
                      mlfMscan_pr_str_trim(
                        mclVe(
                          mclArrayRef1(
                            mclVsv(asciiconv, "asciiconv"),
                            mlfDouble(mclVv(buff, "buff"))))));
                    mlfAssign(&line, mclPlus(mclVv(line, "line"), _mxarray11_));
                }
                mclAssignAns(&ans, mlfFclose(mclVv(file, "file")));
                mlfAssign(&level, _mxarray10_);
                mlfAssign(
                  &file,
                  mclFeval(
                    mclValueVarargout(),
                    mlxFopen,
                    mclVe(
                      mlfIndexRef(
                        mclVsv(names, "names"), "(?).name", mlfScalar(v_))),
                    _mxarray60_,
                    NULL));
                {
                    int v_0 = mclForIntStart(1);
                    int e_0 = mclForIntEnd(
                                mlfScalar(
                                  mclLengthInt(mclVv(filebuff, "filebuff"))));
                    if (v_0 > e_0) {
                        mlfAssign(&line, _mxarray13_);
                    } else {
                        for (; ; ) {
                            mxArray * a_ = mclInitialize(
                                             mclBoolToArray(
                                               mclLengthInt(
                                                 mclVv(filebuff, "filebuff"))
                                               <= v_0));
                            if (mlfTobool(a_)
                                && mlfTobool(
                                     mclAnd(
                                       a_,
                                       mclEq(
                                         mclVe(
                                           mclFeval(
                                             mclValueVarargout(),
                                             mlxLength,
                                             mclVe(
                                               mlfIndexRef(
                                                 mclVsv(filebuff, "filebuff"),
                                                 "{?}",
                                                 mlfScalar(v_0))),
                                             NULL)),
                                         _mxarray10_)))) {
                                mxDestroyArray(a_);
                                mclAssignAns(
                                  &ans,
                                  mlfNFprintf(
                                    0, mclVv(file, "file"), _mxarray62_, NULL));
                            } else {
                                mxDestroyArray(a_);
                                mclFeval(
                                  mlfVarargout(&x, &y, NULL),
                                  mlxStrtok,
                                  mclVe(
                                    mlfIndexRef(
                                      mclVsv(filebuff, "filebuff"),
                                      "{?}",
                                      mlfScalar(v_0))),
                                  _mxarray64_,
                                  NULL);
                                mlfAssign(
                                  &x,
                                  mlfLower(
                                    mclVe(
                                      mlfMscan_pr_str_trim(mclVv(x, "x")))));
                                if (mlfTobool(
                                      mclVe(
                                        mlfStrcmp(
                                          _mxarray36_, mclVv(x, "x"))))) {
                                    mlfAssign(&level, _mxarray10_);
                                    mlfAssign(&lastwasfun, _mxarray11_);
                                }
                                {
                                    mxArray * a_0 = mclInitialize(
                                                      mclVe(
                                                        mlfStrmatch(
                                                          mclVv(x, "x"),
                                                          mclVv(
                                                            stop_op, "stop_op"),
                                                          _mxarray66_)));
                                    if (mlfTobool(a_0)) {
                                        mlfAssign(
                                          &a_0,
                                          mclAnd(
                                            a_0,
                                            mclGt(
                                              mclVv(level, "level"),
                                              _mxarray10_)));
                                    } else {
                                        mlfAssign(&a_0, mlfScalar(0));
                                    }
                                    if (mlfTobool(a_0)
                                        && mlfTobool(
                                             mclAnd(
                                               a_0,
                                               mclBoolToArray(
                                                 mclLengthInt(mclVv(x, "x"))
                                                 > 0)))) {
                                        mxDestroyArray(a_0);
                                        mlfAssign(
                                          &level,
                                          mclMinus(
                                            mclVv(level, "level"),
                                            _mxarray11_));
                                    } else {
                                        mxDestroyArray(a_0);
                                    }
                                }
                                {
                                    mxArray * a_1 = mclInitialize(
                                                      mclVe(
                                                        mclFeval(
                                                          mclValueVarargout(),
                                                          mlxStrncmp,
                                                          mclVe(
                                                            mlfIndexRef(
                                                              mclVsv(
                                                                filebuff,
                                                                "filebuff"),
                                                              "{?}",
                                                              mlfScalar(v_0))),
                                                          _mxarray76_,
                                                          _mxarray12_,
                                                          NULL)));
                                    if (mlfTobool(a_1)) {
                                    } else {
                                        mxArray * b_ = mclInitialize(
                                                         mclVe(
                                                           mclFeval(
                                                             mclValueVarargout(),
                                                             mlxStrncmp,
                                                             mclVe(
                                                               mlfIndexRef(
                                                                 mclVsv(
                                                                   filebuff,
                                                                   "filebuff"),
                                                                 "{?}",
                                                                 mlfScalar(
                                                                   v_0))),
                                                             _mxarray68_,
                                                             _mxarray11_,
                                                             NULL)));
                                        if (mlfTobool(b_)) {
                                            mlfAssign(
                                              &b_,
                                              mclAnd(
                                                b_,
                                                mclVv(
                                                  lastwasfun, "lastwasfun")));
                                        } else {
                                            mlfAssign(&b_, mlfScalar(0));
                                        }
                                        {
                                            unsigned char c_0 = mlfTobool(
                                                                  mclOr(
                                                                    a_1, b_));
                                            mxDestroyArray(b_);
                                            if (c_0) {
                                            } else {
                                                goto l0_;
                                            }
                                        }
                                    }
                                    mxDestroyArray(a_1);
                                    mclAssignAns(
                                      &ans,
                                      mlfNFprintf(
                                        0,
                                        mclVv(file, "file"),
                                        _mxarray70_,
                                        mclVe(
                                          mlfIndexRef(
                                            mclVsv(filebuff, "filebuff"),
                                            "{?}",
                                            mlfScalar(v_0))),
                                        NULL));
                                    goto done_;
                                    l0_:
                                    mxDestroyArray(a_1);
                                    mclAssignAns(
                                      &ans,
                                      mlfNFprintf(
                                        0,
                                        mclVv(file, "file"),
                                        _mxarray72_,
                                        mclVe(
                                          mlfBlanks(
                                            mclMinus(
                                              mclMtimes(
                                                mclVv(level, "level"),
                                                mclVv(tbsize, "tbsize")),
                                              mclMtimes(
                                                mclVv(casesub, "casesub"),
                                                mclVe(
                                                  mlfStrcmp(
                                                    _mxarray74_,
                                                    mclVv(x, "x"))))))),
                                        mclVe(
                                          mlfIndexRef(
                                            mclVsv(filebuff, "filebuff"),
                                            "{?}",
                                            mlfScalar(v_0))),
                                        NULL));
                                    if (mlfTobool(
                                          mclVe(
                                            mlfStrcmp(
                                              _mxarray36_, mclVv(x, "x"))))) {
                                        mlfAssign(&lastwasfun, _mxarray11_);
                                    } else if (mclLengthInt(mclVv(x, "x"))
                                               == 0) {
                                        mlfAssign(
                                          &lastwasfun,
                                          mclVsv(lastwasfun, "lastwasfun"));
                                    } else {
                                        mlfAssign(&lastwasfun, _mxarray10_);
                                    }
                                    done_:;
                                }
                                {
                                    mxArray * a_2 = mclInitialize(
                                                      mclVe(
                                                        mlfStrmatch(
                                                          mclVv(x, "x"),
                                                          mclVv(
                                                            start_op,
                                                            "start_op"),
                                                          _mxarray66_)));
                                    if (mlfTobool(a_2)
                                        && mlfTobool(
                                             mclAnd(
                                               a_2,
                                               mclBoolToArray(
                                                 mclLengthInt(mclVv(x, "x"))
                                                 > 0)))) {
                                        mxDestroyArray(a_2);
                                        mlfAssign(
                                          &level,
                                          mclPlus(
                                            mclVv(level, "level"),
                                            _mxarray11_));
                                    } else {
                                        mxDestroyArray(a_2);
                                    }
                                }
                                mlfAssign(
                                  &x,
                                  mclFeval(
                                    mclValueVarargout(),
                                    mlxMscan_findrevend,
                                    mclVe(
                                      mlfIndexRef(
                                        mclVsv(filebuff, "filebuff"),
                                        "{?}",
                                        mlfScalar(v_0))),
                                    NULL));
                                if (mlfTobool(
                                      mclVe(
                                        mlfStrncmp(
                                          mclVv(x, "x"),
                                          _mxarray78_,
                                          _mxarray80_)))) {
                                    mclAssignAns(
                                      &ans,
                                      mlfNFprintf(
                                        0,
                                        mclVv(file, "file"),
                                        mclVe(
                                          mlfBlanks(
                                            mclVe(
                                              mlfCeil(
                                                mclMtimes(
                                                  mclVv(tbsize, "tbsize"),
                                                  _mxarray81_))))),
                                        NULL));
                                }
                                mlfAssign(
                                  &x,
                                  mlfNStrtok(
                                    2, &y, mclVv(x, "x"), _mxarray82_));
                                {
                                    mxArray * a_3 = mclInitialize(
                                                      mclVe(
                                                        mlfStrmatch(
                                                          mclVv(x, "x"),
                                                          mclVv(
                                                            end_op_at_end,
                                                            "end_op_at_end"),
                                                          _mxarray66_)));
                                    if (mlfTobool(a_3)) {
                                        mlfAssign(
                                          &a_3,
                                          mclAnd(
                                            a_3,
                                            mclGt(
                                              mclVv(level, "level"),
                                              _mxarray10_)));
                                    } else {
                                        mlfAssign(&a_3, mlfScalar(0));
                                    }
                                    if (mlfTobool(a_3)) {
                                        mlfAssign(
                                          &a_3,
                                          mclAnd(
                                            a_3,
                                            mclBoolToArray(
                                              mclLengthInt(mclVv(x, "x"))
                                              > 0)));
                                    } else {
                                        mlfAssign(&a_3, mlfScalar(0));
                                    }
                                    if (mlfTobool(a_3)
                                        && mlfTobool(
                                             mclAnd(
                                               a_3,
                                               mclBoolToArray(
                                                 mclLengthInt(
                                                   mclVe(
                                                     mlfDeblank(
                                                       mclVv(y, "y"))))
                                                 > 2)))) {
                                        mxDestroyArray(a_3);
                                        mlfAssign(
                                          &level,
                                          mclMinus(
                                            mclVv(level, "level"),
                                            _mxarray11_));
                                    } else {
                                        mxDestroyArray(a_3);
                                    }
                                }
                            }
                            if (v_0 == e_0) {
                                break;
                            }
                            ++v_0;
                        }
                        mlfAssign(&line, mlfScalar(v_0));
                    }
                }
                mlfClear(&line, NULL);
                mlfClear(&filebuff, NULL);
                mclAssignAns(&ans, mlfFclose(mclVv(file, "file")));
                if (v_ == e_) {
                    break;
                }
                ++v_;
            }
            mlfAssign(&a, mlfScalar(v_));
        }
    }
    /*
     * return;
     * 
     * 
     */
    return_:
    mxDestroyArray(subdir);
    mxDestroyArray(inter);
    mxDestroyArray(tbsize);
    mxDestroyArray(casesub);
    mxDestroyArray(level);
    mxDestroyArray(lastwasfun);
    mxDestroyArray(a);
    mxDestroyArray(r);
    mxDestroyArray(ans);
    mxDestroyArray(dirname);
    mxDestroyArray(filename);
    mxDestroyArray(extname);
    mxDestroyArray(dirnames);
    mxDestroyArray(start_op);
    mxDestroyArray(stop_op);
    mxDestroyArray(end_op_at_end);
    mxDestroyArray(asciiconv);
    mxDestroyArray(names);
    mxDestroyArray(file);
    mxDestroyArray(line);
    mxDestroyArray(buff);
    mxDestroyArray(filebuff);
    mxDestroyArray(x);
    mxDestroyArray(y);
    mxDestroyArray(varargin);
    mxDestroyArray(name);
    mclSetCurrentLocalFunctionTable(save_local_function_table_);
}

/*
 * The function "Mmscan_findrevend" is the implementation version of the
 * "mscan/findrevend" M-function from file
 * "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines 188-205). It contains the
 * actual compiled code for that M-function. It is a static function and must
 * only be called from one of the interface functions, appearing below.
 */
/*
 * function ret=findrevend(str)
 */
static mxArray * Mmscan_findrevend(int nargout_, mxArray * str) {
    mexLocalFunctionTable save_local_function_table_ = mclSetCurrentLocalFunctionTable(
                                                         &_local_function_table_mscan);
    mxArray * ret = mclGetUninitializedArray();
    mxArray * a = mclGetUninitializedArray();
    mxArray * isstr = mclGetUninitializedArray();
    mxArray * ans = mclGetUninitializedArray();
    mclCopyArray(&str);
    /*
     * % Picks out end of line before comment in reverse order with blanks
     * % in beginning and end removed.
     * 
     * deblank(str);
     */
    mclAssignAns(&ans, mlfDeblank(mclVa(str, "str")));
    /*
     * isstr=0;
     */
    mlfAssign(&isstr, _mxarray10_);
    /*
     * for a=1:length(str),
     */
    {
        int v_ = mclForIntStart(1);
        int e_ = mclForIntEnd(mlfScalar(mclLengthInt(mclVa(str, "str"))));
        if (v_ > e_) {
            mlfAssign(&a, _mxarray13_);
        } else {
            /*
             * if str(a)==char(39),
             * isstr= ~isstr;
             * elseif str(a)=='%' & isstr==0,
             * a=a-1;
             * break;
             * end
             * end
             */
            for (; ; ) {
                mlfAssign(&a, mlfScalar(v_));
                if (mclEqBool(
                      mclVe(mclArrayRef1(mclVsa(str, "str"), mclVsv(a, "a"))),
                      mclVe(mlfChar(_mxarray84_, NULL)))) {
                    mlfAssign(&isstr, mclNot(mclVv(isstr, "isstr")));
                } else {
                    mxArray * a_ = mclInitialize(
                                     mclEq(
                                       mclVe(
                                         mclArrayRef1(
                                           mclVsa(str, "str"), mclVsv(a, "a"))),
                                       _mxarray68_));
                    if (mlfTobool(a_)
                        && mlfTobool(
                             mclAnd(
                               a_,
                               mclEq(mclVv(isstr, "isstr"), _mxarray10_)))) {
                        mxDestroyArray(a_);
                        mlfAssign(&a, mclMinus(mclVv(a, "a"), _mxarray11_));
                        break;
                    } else {
                        mxDestroyArray(a_);
                    }
                }
                if (v_ == e_) {
                    break;
                }
                ++v_;
            }
        }
    }
    /*
     * ret=deblank(str(a:-1:1));
     */
    mlfAssign(
      &ret,
      mlfDeblank(
        mclVe(
          mclArrayRef1(
            mclVsa(str, "str"),
            mlfColon(mclVv(a, "a"), _mxarray85_, _mxarray11_)))));
    mclValidateOutput(ret, 1, nargout_, "ret", "mscan/findrevend");
    mxDestroyArray(ans);
    mxDestroyArray(isstr);
    mxDestroyArray(a);
    mxDestroyArray(str);
    mclSetCurrentLocalFunctionTable(save_local_function_table_);
    return ret;
    /*
     * return;
     * 
     */
}

/*
 * The function "Mmscan_pr_str_trim" is the implementation version of the
 * "mscan/pr_str_trim" M-function from file
 * "/tmp_mnt/d2/emanuele/matlib/misc/mscan.m" (lines 205-216). It contains the
 * actual compiled code for that M-function. It is a static function and must
 * only be called from one of the interface functions, appearing below.
 */
/*
 * function ret=pr_str_trim(str)
 */
static mxArray * Mmscan_pr_str_trim(int nargout_, mxArray * str) {
    mexLocalFunctionTable save_local_function_table_ = mclSetCurrentLocalFunctionTable(
                                                         &_local_function_table_mscan);
    mxArray * ret = mclGetUninitializedArray();
    mclCopyArray(&str);
    /*
     * 
     * % ret=pr_str_trim(str)  Removes blanks in both beginninge and end of line.
     * %
     * % Peter Rydesäter 99-04-06
     * %
     * str=deblank(str(end:-1:1));
     */
    mlfAssign(
      &str,
      mlfDeblank(
        mclVe(
          mclArrayRef1(
            mclVsa(str, "str"),
            mlfColon(
              mlfEnd(mclVa(str, "str"), _mxarray11_, _mxarray11_),
              _mxarray85_,
              _mxarray11_)))));
    /*
     * ret=deblank(str(end:-1:1));
     */
    mlfAssign(
      &ret,
      mlfDeblank(
        mclVe(
          mclArrayRef1(
            mclVsa(str, "str"),
            mlfColon(
              mlfEnd(mclVa(str, "str"), _mxarray11_, _mxarray11_),
              _mxarray85_,
              _mxarray11_)))));
    mclValidateOutput(ret, 1, nargout_, "ret", "mscan/pr_str_trim");
    mxDestroyArray(str);
    mclSetCurrentLocalFunctionTable(save_local_function_table_);
    return ret;
    /*
     * return; 
     * 
     * 
     */
}
