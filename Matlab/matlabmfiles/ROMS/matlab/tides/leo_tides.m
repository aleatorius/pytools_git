function [Tide]=leo_tides(adcirc);

%  This script extract tidal information from ADCIRC at LEO Metric
%  stations.

%---------------------------------------------------------------------
%  Load ADCIRC data.
%---------------------------------------------------------------------

load(adcirc);

Ntide=length(periods);
 
%---------------------------------------------------------------------
%  Set LEO stations location.
%---------------------------------------------------------------------

STA(1,1:6)='Node A';
XstrD(1)=74.0; XstrM(1)=15.72; YstrD(1)=39.0; YstrM(1)=27.69;

STA(2,1:6)='Node B';
XstrD(2)=74.0; XstrM(2)=14.75; YstrD(2)=39.0; YstrM(2)=27.41;

STA(3,1:6)='COOL 2';
XstrD(3)=74.0; XstrM(3)=13.7;  YstrD(3)=39.0; YstrM(3)=26.3;

STA(4,1:6)='COOL 3';
XstrD(4)=74.0; XstrM(4)=11.6;  YstrD(4)=39.0; YstrM(4)=24.9;

STA(5,1:6)='COOL 4';
XstrD(5)=74.0; XstrM(5)=9.48;  YstrD(5)=39.0; YstrM(5)=23.5;

STA(6,1:6)='COOL 5';
XstrD(6)=74.0; XstrM(6)=7.4;   YstrD(6)=39.0; YstrM(6)=22.08;

STA(7,1:6)='COOL 6';
XstrD(7)=74.0; XstrM(7)=5.06;  YstrD(7)=39.0; YstrM(7)=20.55;

Nsta=length(XstrD);

for n=1:Nsta,
  slon(n)=-abs(XstrD(n)+XstrM(n)/60.0);
  slat(n)=YstrD(n)+YstrM(n)/60.0;
  disp([' Station ',STA(n,:),': ', ...
  ' Slon = ',num2str(slon(n),'%8.4f'),' Slat = ',num2str(slat(n),'%7.4f')]);
end,

slon=slon';
slat=slat';

Tide.sta=STA;
Tide.lon=slon;
Tide.lat=slat;

%---------------------------------------------------------------------
%  Interpolate tide data to application grid.
%---------------------------------------------------------------------
%
%  Interpolate elevation and velocity from ADCIRC model data using
%  "triterp", linear-interpolation from triangulated grid.

for k=1:Ntide,

  ei(:,k)=triterp(tri,lon,lat,elev(:,k),slon,slat);
  ui(:,k)=triterp(tri,lon,lat,u(:,k),slon,slat);
  vi(:,k)=triterp(tri,lon,lat,v(:,k),slon,slat);
  
end,

%---------------------------------------------------------------------
%  Extract tide-induced mean elevation and velocity components.
%---------------------------------------------------------------------

Tide.Emean=squeeze(abs(ei(:,1)));
Tide.Amean=squeeze(angle(ei(:,1))).*180/pi;
Tide.Umean=squeeze(ui(:,1));
Tide.Vmean=squeeze(vi(:,1));

%---------------------------------------------------------------------
%  Extract other tidal contituents (2:Ntide).
%---------------------------------------------------------------------

Tide.period=periods(2:Ntide);
Tide.component=['O1'; 'K1'; 'N2'; 'M2'; 'S2'; 'M4'; 'M6'];

Tide.Ephase=angle(ei(:,2:Ntide)).*180./pi;
Tide.Eamp  =abs(ei(:,2:Ntide));

%---------------------------------------------------------------------
%  Convert tidal current amplitude and phase lag parameters to tidal
%  current ellipse parameters: Major axis, ellipticity, inclination,
%  and phase.  Use "tidal_ellipse" (Zhigang Xu) package.
%---------------------------------------------------------------------

ui_pha=angle(ui(:,2:Ntide)).*180./pi;
ui_amp=abs(ui(:,2:Ntide));

vi_pha=angle(vi(:,2:Ntide)).*180./pi;
vi_amp=abs(vi(:,2:Ntide));

[major,eccentricity,inclination,phase]=ap2ep(ui_amp,ui_pha,vi_amp,vi_pha);

Tide.Cmax=major;
Tide.Cmin=major.*eccentricity;
Tide.Cangle=inclination;
Tide.Cphase=phase;

return


