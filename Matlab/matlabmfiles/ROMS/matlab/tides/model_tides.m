function [ssh,ubar,vbar]=model_tides(Tide,time);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [ssh,ubar,vbar]=model_tides(Tide,time);                          %
%                                                                           %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Tide       Tidal data (structure array).                               %
%                 Tide.period => tidal.period (hours).                      %
%                 Tide.Eamp   => tidal elevation amplitude (m).             %
%                 Tide.Ephase => tidal elevation phase angle (degrees).     %
%                 Tide.Cphase => tidal current phase angle (degrees).       %
%                 Tide.Cangle => tidal current inclination angle (degrees). %
%                 Tide.Cmax   => maximum tidal current (m/s).               %
%                 Tide.Cmin   => minimum tidal current (m/s).               %
%    time       Model time to compute net tidal forcing (sec).              %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    ssh        Sea surface elevation (m).                                  %
%    ubar       Barotropic tidal u-velocity (m/s).                          %
%    vbar       Barotropic tidal v-velocity (m/s).                          %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Nsta]=length(Tide.lon);
[Ntide]=length(Tide.period);

ssh=zeros([Nsta 1]);
ubar=zeros([Nsta 1]);
vbar=zeros([Nsta 1]);

deg2rad=pi/180;

for itide=1:Ntide,

  if (Tide.period(itide) > 0),
 
    period=Tide.period(itide)*3600;
    omega=2.0*pi*time/period;

    Eamp=Tide.Eamp(:,itide);
    Ephase=Tide.Ephase(:,itide).*deg2rad;
    ssh=ssh+Eamp.*cos(omega-Ephase);

    Cangle=Tide.Cangle(:,itide).*deg2rad;
    Cphase=Tide.Cphase(:,itide).*deg2rad;
    Cmin=Tide.Cmin(:,itide);
    Cmax=Tide.Cmax(:,itide);
    ubar=ubar+Cmax.*cos(Cangle).*cos(omega-Cphase) ...
	     -Cmin.*sin(Cangle).*sin(omega-Cphase);
    vbar=vbar+Cmax.*sin(Cangle).*cos(omega-Cphase) ...
	     -Cmin.*sin(Cangle).*cos(omega-Cphase);

  end,
end,       

return
