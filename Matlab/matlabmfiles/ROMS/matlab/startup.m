function startup

% startup -- User script configuration for Matlab.  It can set default
%            paths, define Handle Graphics defaults, or predefine
%            variables in your workspace.

global IPRINT
IPRINT=0;

format long g

my_root='/cluster/home/pduarte/matlabmfiles/ROMS';

path(path, fullfile(my_root, 'matlab', 'Damee', ''))
path(path, fullfile(my_root, 'matlab', 'coastlines', ''))
path(path, fullfile(my_root, 'matlab', 'dx', ''))
path(path, fullfile(my_root, 'matlab', 'floats', ''))
path(path, fullfile(my_root, 'matlab', 'forcing', ''))
path(path, fullfile(my_root, 'matlab', 'grib', ''))
path(path, fullfile(my_root, 'matlab', 'hydro', ''))
path(path, fullfile(my_root, 'matlab', 'm_map', ''))
path(path, fullfile(my_root, 'matlab', 'presto', ''))
path(path, fullfile(my_root, 'matlab', 'rivers', ''))
path(path, fullfile(my_root, 'matlab', 'rmask', ''))
path(path, fullfile(my_root, 'matlab', 'rnt', ''))
path(path, fullfile(my_root, 'matlab', 'rnc', ''))
path(path, fullfile(my_root, 'matlab', 'misc', ''))
path(path, fullfile(my_root, 'matlab', 'seagrid', ''))
path(path, fullfile(my_root, 'matlab', 'seawater', ''))
path(path, fullfile(my_root, 'matlab', 'tides', ''))
path(path, fullfile(my_root, 'matlab', 'tidal_ellipse', ''))
path(path, fullfile(my_root, 'matlab', 'tools', ''))
path(path, fullfile(my_root, 'matlab', 'tri', ''))

% NetCDF Toolbox.

path(path, fullfile(my_root, 'matlab', 'netcdf', ''))
path(path, fullfile(my_root, 'matlab', 'netcdf', 'nctype', ''))
path(path, fullfile(my_root, 'matlab', 'netcdf', 'ncutility', ''))
path(path, fullfile(my_root, 'matlab', 'netcdf', 'ncfiles', ''))
