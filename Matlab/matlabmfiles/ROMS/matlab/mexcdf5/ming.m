function a=ming(b);
g=find(b(:)~=NaN);
a=min(b(g));
