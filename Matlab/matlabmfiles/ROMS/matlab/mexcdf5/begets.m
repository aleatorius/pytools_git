function msg = begets(fcn,nin,a,b,c,d,e,f,g,h,i,j)

% BEGETS Message showing the result of a function.
%  BEGETS(FCN,NIN,A,B,...) creates a message that
%   shows the function FCN with its input and
%   output values.  The number of input arguments
%   is NIN.  The argument list A,B,... is organized
%   into NIN input values, followed immediately by
%   the output values.  Thus, begets('sqrt', 1, 4, 2)
%   results in the message "sqrt(4) ==> 2".

% See also: MAT2STR.

% Copyright (C) 1991 Charles R. Denham, Zydeco.

if nargin < 1
   help begets
   disp(' Some examples:')
   x = (1:4).';
   begets('mean', 1, x, mean(x));
   x = (2:4).^2;
   begets('sqrt', 1, x, sqrt(x));
   x = [1 2; 3 4]; [m, n] = size(x);
   begets('size', 1, x, [m n]);
   begets('size', 1, x, m, n);
   x = [1 2; 2 1]; [v, d] = eig(x);
   begets('eig', 1, x, v, d)
   begets('1/0', 0, inf)
   begets('inf.*0', 0, inf.*0)
   x = abs('hello');
   begets('setstr', 1, x, setstr(x))
   x = 'hello';
   begets('abs', 1, x, abs(x))
   return
end

% FCN(...) Input argument list.

s = '';
s = [fcn];
if nin > 0, s = [s '(']; end
arg = 'a';
for ii = 1:nin;
   s = [s mat2str(eval(arg))];
   if ii < nin, s = [s ', ']; end
   arg = arg + 1;
end
if nin > 0, s = [s ')']; end

% [...] Output argument list.

t = '';
nout = nargin - nin - 2;
if nout > 1, t = ['[']; end
for ii = 1:nout
   t = [t mat2str(eval(arg))];
   if ii < nout, t = [t ', ']; end
   arg = arg + 1;
end
if nout > 1, t = [t ']']; end

% Message.

% u = [t ' = ' s];
u = [s ' ==> ' t];

if nargout > 0, msg = u; else, disp([' ' u]); end
