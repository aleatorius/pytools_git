%     Copyright (C), May 11 1992, J.V. Mansbridge,
%     Commonwealth Scientific and Industrial Research Organisation
%     Revision $Revision: 1.4 $
%     Author   $Author: mansbrid $
%     Date     $Date: 1996/01/05 04:52:24 $
%     RCSfile  $RCSfile: manual.tex,v $
% @(#)
% 
%\documentstyle{article}
\documentstyle[a4wide,11pt]{article}
\title{ {\Huge \bf Using MATLAB to read netCDF files\\}
	\vspace{2cm} 
        {\LARGE User Note 12\\ $Revision: 1.4 $ \\}
        \vspace{2cm}
      }

\author{\LARGE Jim Mansbridge\\ \\ 
\LARGE CSIRO Division of Oceanography\\ \\ \\}

\vspace{2cm} 
      
\date{\LARGE January 5 1995}

\begin{document}
\maketitle
\newpage

\vspace{10cm} 
\begin{flushleft}

Copyright (C) 1992 by J.V. Mansbridge,
Commonwealth Scientific and Industrial Research Organisation\\

\vspace{1cm} 

$Revision: 1.4 $\\

\vspace{1cm} 

Acknowledgements\\

\vspace{0.5cm} 

This document was written by Jim Mansbridge for the Oceanography
Division of the Commonwealth Scientific and Industrial Research
Organisation, Australia.  The m-files that it describes were written
by Jim Mansbridge with valuable suggestions from Peter McIntosh of
CSIRO and Rich Signell of the U.S. Geological Survey.  The C code,
mexcdf.c and mexcdf.h, was written by Charles R. Denham of ZYDECO.
Thanks also to Phil Morgan, John Wilkin, Bill Hart and Peter Campbell
of CSIRO for their help at various stages.

\end{flushleft}

\newpage

\pagenumbering{arabic}
\section{Introduction}

Many data sets are now stored in the Network Common Data Form (netCDF)
and it is expected that its use will increase.  At present users may
handle these files by writing their own programs or by employing
user-friendly software.

For some tasks, such as creating or modifying netCDF files, it is
necessary to write Fortran or C programs which link with netCDF
library programs.  If you need to do this then you should read the
netCDF User's Guide.  Hard copies of this exist, but the easiest way to
view the guide is by using the `info' facility in emacs.

Fortunately, most people will not need to write specialist programs
because they will only want to extract data from netCDF files.  This
user note describes the software which enables data in a netCDF file
to be copied into the MATLAB workspace.  Then the user can employ MATLAB
to analyse the data.

There are at two main advantages to netCDF

\begin{itemize}
\item 	Scientific data can be created, accessed and shared in a
        machine independent fashion.
\item	A netCDF data file can be self describing.
\end{itemize}

The machine independence of netCDF files means that they may be
created on one machine, transferred to another and then read and,
if necessary, modifed on the second machine; all that is necessary is
that the netCDF software be installed on both machines.  For example,
I have been regularly creating
netCDF files on the NCAR Cray, ftp'ing them to a sparc network
and then extracting data from the files using MATLAB on an SGI.
Even though the machines have very different architectures I have
not had any problems with the data transference.

Because the netCDF files are self-describing important information
about the data such as units, version number and longitude-latitude
grid description can be included in the file --- the file can be
self-contained.  Apart from avoiding confusion (are the Semtner-Chervin
velocites really in cm/sec?) this enables the creation of generic
software for extracting the data.



\section{Software summary}

There are 6 matlab functions which the user might regularly employ in
connection with netCDF files.  These are

\begin{itemize}
\item 	{\em whatcdf} which lists the netCDF files that are in the current
                 directory and the common data area,
\item	{\em inqcdf} which is an interactive function that allows the user
                to find out details
                about the netCDF file (e.g. dimensions of data arrays,
                units),
\item	{\em getcdf} which is an interactive function that allows the
	        user to import
                data into the MATLAB workspace and
\item	{\em getcdf\_batch} which performs the same task as getcdf but
		does so non-interactively.  Users can therefore write
		their own MATLAB script and function files.
\item	{\em attcdf} which allows the user to import netCDF file
                attributes into the MATLAB workspace.
\item	{\em get\_time} which returns time information for a netCDF
                file that meets COARDS standards.
\end{itemize}

A brief description of these functions can be found by using the
matlab help facility.  A full description of each function is given in
the next 6 sections of this user's guide.  This is done by reproducing
an actual MATLAB session.

Additionally, it is possible to make direct, lower level, calls to
netCDF library routines via the function mexcdf.  This function is
more difficult to use and it is not expected that many users will need
to make calls to it.  A brief description of mexcdf is given after the
descriptions of the 6 higher level functions.


\section{whatcdf}

After having entered the appropriate directory and started the MATLAB
session then we could enter {\em whatcdf} at the prompt and see

\vspace{0.2cm}
\begin{verbatim}
>> whatcdf
 
-----  current directory netCDF files  -----
foo    		testjm 
 
-----  current directory compressed netCDF files  -----
uvd             xz2375N  
 
-----  common data set of netCDF files  -----
bottomley_sst           fsu_pacific             levitus_seasonal    
cac_sst                 gordon_atlas            nodc_tasman_ctd    
esbensen                gordon_grid             oberhuber    
etopo5                  hastenrath_indian       sadler    
fram_year6              hellerman               trenberth_mean    
franklin_tasman_ctd     indo_temp_sal           wyrtki    
fsu_indian              levitus_mean    	      
 
>> 
\end{verbatim}
\vspace{0.2cm}


This tells us that the current directory has 2 files of the form
foo.nc, testjm.nc.  (Note that this function follows the MATLAB
convention of not listing the suffixes of the files --- netCDF files
always have a .nc or .cdf suffix.)  As well, there are 2 compressed
files ---- uvd.nc.Z and xz2375N.nc.Z.  Both the {\em inqcdf} and
{\em getcdf} functions are able to uncompress these files.  The third set
of files are available for everyone to read from any directory because
they are of general interest.

\section{inqcdf}

Having used {\em whatcdf} to find the names of the netCDF files
accessible from the current directory we can use {\em inqcdf} to
inquire about a particular file --- in this case uvd(.nc).

\vspace{0.3cm}
\begin{verbatim}
>> inqcdf('uvd')
                
 uvd.nc is compressed.  Do you want it to be uncompressed?  (y/n) [y] y
\end{verbatim}
\vspace{0.3cm}

I have answered {\em y} to the question.
Simply hitting the carriage return would have been
equivalent to {\em y} because that is the default as indicated by the
square brackets.  After uncompressing the file MATLAB writes the
following to the screen:

\vspace{0.3cm}
\begin{verbatim}
                ---  Global attributes  ---
source: Semtner & Chervin SLU 414 11/23/88
title: 5 year ensemble stats - southern hemisphere, all depths
history: initial cdf file created by J.V.Mansbridge
 
The 3 dimensions are  1) lon2 = 722  2) lat2 = 151  3) depth2 = 20.
None of the dimensions is unlimited
 
----- Get further information about the following variables -----
 
  -1) None of them (no further information)
   0) All of the variables
   1) s-u              2) s-v              3) eke
   4) c-uv             5) lat2             6) depth2
   7) lon2          
 

  Select a menu number: 1
\end{verbatim}
\vspace{0.3cm}

This tells us that the netCDF file has 3 global attributes, named 
{\em source}, {\em title} and {\em history}.  In this case the values
of all three
global attributes (which are printed out) are character strings;
the attributes could take any of the data types supported by netCDF
(integer, real, etc).  The names of the global attributes are standard
ones so that generic software can look for them and use their values.

We are then told that the variables can have any of 3 dimensions;
the names and sizes of these are given.  In netCDF one of the dimensions may be
unlimited.  This could happen, for example, if we were storing data
from an ongoing series of measurements --- there might be no limit to
the number of measurements.  In the present case the dimensions of longitude,
latitude and depth are all limited.

Next, the program tells us the names of the 7 variables in the netCDF
file and offers to give us more information about any or all of them.
To find out about the first variable, s-u, I enter
{\em 1}.  This produces:


%\pagebreak
\vspace{0.2cm}
\begin{verbatim}
 
   ---  Information about s-u(lat2 depth2 lon2 )  ---
 
*long_name: sd of northwards velocity, 5 year annual mean
*units: cm/sec                          *_FillValue: 1e+36  
*missing_value: 1e+36
 
----- Get further information about the following variables -----
 
  -1) None of them (no further information)
   0) All of the variables
   1) s-u              2) s-v              3) eke
   4) c-uv             5) lat2             6) depth2
   7) lon2          
 

 Select a menu number: 5
\end{verbatim}
\vspace{0.3cm}

This tells us that the variable s-u has 3 dimensions, named 
{\em lat2}, {\em depth2} and {\em lon2}, and 4 local
attributes.  The first local attribute, {\em long\_name}, gives us
information about what data are contained in the variable.  {\em
units} tells us that the velocity is in cm/sec.
{\em \_FillValue} is the default value written to each point in the
data array when the netCDF file is created.  In this example the same
number is used for {\em missing\_value} which indicates land in
oceaographic observations.


We are then given the opportunity to examine another variable.
I choose to find out about the fifth variable, lat2, and so enter
{\em 5}.  This produces:


\vspace{0.2cm}
\begin{verbatim}
 
   ---  Information about lat2(lat2 )  ---
 
*long_name: latitude_type_2             *units: degrees_northeast
*_FillValue: 1e+36                      *missing_value: 1e+36  
*valid_range: -90  90                   
 
----- Get further information about the following variables -----
 
  -1) None of them (no further information)
   0) All of the variables
   1) s-u              2) s-v              3) eke
   4) c-uv             5) lat2             6) depth2
   7) lon2          
 

 Select a menu number: -1
\end{verbatim}
%\pagebreak
\vspace{0.3cm}

The variable lat2 has dimension lat2.  This clumsy expression comes about
because it is possible to use the same name for a dimension and a
variable.  The dimension lat2 refers to the number 151 --- there are
151 grid points along a latitude circle at half degree intervals.
The fact that there is a variable with the
same name implies that the variable tells us the latitude of the grid
points.  Thus, if we want to know the latitude of s-u at the
third latitudinal grid point we simply find out the value of lat2(3)
--- in this case -73.5.  When a data array such as s-u is read into
the MATLAB workspace it may also be useful to read in the one
dimensional arrays lat2, lon2 and depth2 so that the position of each
grid point can be properly determined.  These arrays can also be used
to label the axes on graphs.

Finally, {\em inqcdf} is exited by choosing {\em -1} in the menu.  The next
step is to read some data from a variable.



\section{getcdf}

The following session shows how to use {\em getcdf} to read some data
into the MATLAB workspace.  It is not necessary to read in all of the
data stored under a given variable name, and indeeed the MATLAB
software can only handle scalars, vectors and matrices.  For a
variable that is stored
with n dimensions then the user must give two n-dimensional corner
points which then specify the `hyperslab' to be returned.  These would
correspond to the bottom left hand corner and the top right hand
corner in a 2-space.  The data retrieval begins as follows:

\vspace{0.2cm}
\begin{verbatim}
>> su = getcdf('uvd');
 
----- Choose the variable -----
 
   1) s-u              2) s-v              3) eke
   4) c-uv             5) lat2             6) depth2
   7) lon2          
 

 Select a menu number: 1
\end{verbatim}
\vspace{0.3cm}

The first line above specifies that data from the netCDF file is going
to be read into the MATLAB variable su.  The trailing semi-colon stops
the data from being written to the screen.

I select variable {\em 1}, s-u, and this produces the following:

\vspace{0.2cm}
%\pagebreak
\begin{verbatim}
 
s-u has 3 dimensions
   1)  lat2 : Length 151 : Elements -74.5 -74 -73.5 ... -0.5 0 0.5
   2)  depth2 : Length 20 : Elements 1250 3750 6250 ... 4.125e+05 4.6e+05 5e+05
   3)  lon2 : Length 722 : Elements 20.5 21 21.5 ... 380 380.5 381

----- What sort of hyperslab do you want? -----
 
      1) a scalar
      2) a vector
      3) a matrix
 

 Select a menu number: 3
\end{verbatim}
\vspace{0.3cm}

This tells us that s-u has 3 dimensions, it gives their sizes, and
it lists the first 3 and last 3 values of the vectors corresponding to
those dimensions.  Thus, for example, we see that the first longitude point
is at 20.5 degrees.

The data can be returned as either a scalar, a one-dimensional array (a
vector), or a two-dimensional array (a matrix).  The standard version
of matlab cannot handle higher dimensional data structures.

I select menu item {\em 3}, to return a matrix, and this produces
the following:

\vspace{0.2cm}
\begin{verbatim}
                        
 Should the hyperslab vary with ... lat2? (y/n) [y] 
                                                    
 Should the hyperslab vary with ... depth2? (y/n) [y] n
 
\end{verbatim}
\vspace{0.3cm}

I have decided that I want to get a longitude-latitude data slice at a
constant depth.  Hence, in answer to the first question I simply hit
the carriage return because I want the hyperslab of returned values
(in this case two dimensional array) to vary with latitude.  It is
not going to vary with depth and so I answer {\em n} to the second
question.  Since the only other dimension is latitude it follows
logically that the hyperslab must vary with this dimension; hence
there is no need for the program to ask about latitude.

The program now asks the next set of questions:

\vspace{0.2cm}
%\pagebreak
\begin{verbatim}
 
 1)  lat2 : Length 151 : Elements -74.5 -74 -73.5 ... -0.5 0 0.5

    lat2 : Starting index (between 1 and 151)  (cr for all indices)  
 
 2)  depth2 : Length 20 : Elements 1250 3750 6250 ... 4.125e+05 4.6e+05 5e+05

    depth2 : Index (between 1 and 20)  1
 
 3)  lon2 : Length 722 : Elements 20.5 21 21.5 ... 380 380.5 381

    lon2 : Starting index (between 1 and 722)  (cr for all indices)  2

          finishing index (between 2 and 722)  64

          stride length (cr for 1)  3
\end{verbatim}
\vspace{0.3cm}

For the first dimension I simply hit the carriage return to get the
default, which is the entire range of lat2, i.e., 151 elements going
from -74.5 degrees to 0.5 degrees.

The second dimension is the depth and I choose index 1, i.e., 1250 cm.

For the third dimension, lon2, I want only a subset of the available
data points.  I choose to start at index 2 and finish at index 64 with a
stride of 3.  The stride means that I will get every third data point;
hence the indices in the lon2 direction will be 2, 5, 8, \ldots, 56, 59,
62.

The next question is:

\vspace{0.2cm}
\begin{verbatim}
 
----- In which order do you want the indicies? -----
 
      1) s-u(lon2,lat2)
      2) s-u(lat2,lon2)
 

 Select a menu number: 2
\end{verbatim}
\vspace{0.3cm}

Here I choose menu item {\em 2} so that the returned matrix will have
latitude as its row dimension and longitude as its column dimension.
This will be consistent with the normal way maps are drawn with
latitude in the y direction and longitude in the x direction.

There will then be a delay while all of the data are read.  Then a
further question may or may not appear.  It is:

\vspace{0.2cm}
%\pagebreak
\begin{verbatim}
 
----- s-u contains missing values:  Choose an action -----
 
      1) Leave the missing value unchanged
      2) Replace the missing value with NaN
      3) Replace the missing value with a new value
 

 Select a menu number: 3
                        
    Type in your new missing value marker [0]  1000
>>                                             
\end{verbatim}
\vspace{0.3cm}

The above question will only appear if at least one of the retrieved
values is a missing value indicator (in this case the missing\_value
and \_FillValue are both
1e+36).  (For a description of missing values see sections 2.3.1 and
7.13 of the netCDF User's Guide; getcdf recognises the attributes
\_FillValue, missing\_value, valid\_range, valid\_min and valid\_max.)

The first option leaves the missing
value indicators unchanged.  You may want to do this because, for
example, after
reading inqcdf you know that there are different indicators for land
and ice and so can use matlab to find out where these regions are.
The second option is to replace the missing value indicators
with NaN, the IEEE Not-a-Number indicator.  Some MATLAB functions can
use this value in very convenient ways (the graphics functions in
particular).  Alternatively, you may replace the missing
value indicator with a number that suits you.  For example, if land
points in a velocity field were replaced with zeros then flux
calculations
could be done safely without having to worry about where the land is.

After I have chosen to replace each missing value with 1000
then control returns to the usual matlab interface and the matrix
su contains the desired data.  Note that the size of the matrix will
be $151 \times 21$ because I have retrieved all points in the lat2
direction but only every third point from 2 to 64 in the lon2 direction.



\section{getcdf\_batch}


{\em getcdf\_batch} achieves the same thing as {\em getcdf} but is
non-interactive.  It can therefore be used in script and function
files.  The syntax is then

\vspace{0.2cm}

\begin{flushleft}

val = {\em getcdf\_batch}(filename, varid, corner, end\_point,
           stride, order, change\_miss, new\_miss);

\end{flushleft}

\vspace{0.2cm}

The MATLAB variable val will be passed a scalar, a vector or a matrix
of values.  If the hyperslab specified by corner and end\_point has
more than 2 dimensions then the values are returned as a vector since
matlab cannot store such high order arrays.

The variables to be passed to the function correspond to the answers
which would be given if {\em getcdf} had been run.  For the example
with {\em getcdf} in the previous section the call would be:

\vspace{0.2cm}
\begin{verbatim}

us = getcdf_batch('uvd', 1, [1 1 2], [151 1 64], [1 1 3], 2, 3, 1000);

\end{verbatim}
\vspace{0.2cm}

{\em NOTE:} The variables {\em change\_miss} and {\em new\_miss}
appear in the argument list even if there are no missing values
and, consequently, {\em getcdf} would not have asked any questions
about them.

To make things easier you may use the name of the variable instead of
its menu number.  As well you may use a negative integer for a
starting index to indicate that you want all of the values in a given
direction.  An example of this would be:

\vspace{0.2cm}
\begin{verbatim}

us = getcdf_batch('uvd', 'u-s', [-1 1 2], [-1 1 64], [1 1 3], 2, 3, 1000);

\end{verbatim}
\vspace{0.2cm}

Actually, it is only necesssary to pass the first two arguments to
getcdf\_batch as below: 

\vspace{0.2cm}
\begin{verbatim}

us = getcdf_batch('uvd', 'u-s');

\end{verbatim}
\vspace{0.2cm}

The default is to return every element of the variable with missing
values replaced by NaNs.  If the variable has 2 dimensions then the
dimension which appears earlier in the corner vector will be the
column index in the returned matrix.  If the variable has 3 or more
dimensions then the data will be returned in a single vector.  The
storage will be as in the C convention (the opposite of the fortran
one), i.e., the first index will be the slowest varying, through to
the last index being the fastest varying.  It is then up to the user
to sort out the data using, for example, matlab's reshape command.

It is also allowable for {\em getcdf\_batch} to be passed only the
first four input arguments, in which case defaults are used for the
other four.

In more detail the meanings of the function arguments are:

\begin{itemize}
\item 	{\em filename} is a character string containing the NetCDF
	file name without the .nc or .cdf suffix.
\item 	{\em varid} may be an integer or a string.  If it is an
        integer then it must be the menu number of the n dimensional
        variable as used by a call to {\em getcdf} or {\em inqcdf}.  If
        it is a string then it should be the name of the variable.
\item 	{\em corner} is a matlab length n vector giving one corner of the
	`hyperslab' of data.  This will be the corner with lowest
	index values (the bottom left-hand corner in a 2-space).
	The corners refer to the dimensions in
	the same order that these dimensions are listed in the
	relevant questions in {\em getcdf} and in the
	{\em inqcdf} description of the variable.  A negative
	element will cause all values in that direction to be
	returned.  (This duplicates hitting carriage return when asked
	for the starting index in {\em getcdf}.)
\item 	{\em end\_point} is a matlab length n vector giving a second
	corner of the
	`hyperslab' of data.  This will be the corner with highest
	index values (the top right-hand corner in a 2-space).  
	The corners refer to the dimensions in
	the same order that these dimensions are listed in the
	relevant questions in {\em getcdf} and in the
	{\em inqcdf} description of the variable.
\item 	{\em stride} is a vector of length n specifying the interval
        between 
        accessed values of the hyperslab in each of the n dimensions.  A
        value of 1 accesses adjacent values in the given dimension; a
        value of 2 accesses every other value; and so on.
\item 	{\em order}.  This is used when a vector or matrix is returned.
        For a matrix it
        specifies the order of the two dimensions in that matrix.
        If {\em order} is 1 then the dimension which appears earlier in
        the corner vector will be the column index in the returned matrix.
        If {\em order} is 2 then it will be the other way around.  If
        a vector is to be returned then setting {\em order} = 1
        causes it to be a row vector and
        {\em order} = 2 causes it to be a column vector.
\item 	{\em change\_miss} is used to specify what action should be
	taken with missing data values.  As in {\em getcdf}, if 
	{\em change\_miss} = 1 then missing values will not be changed.
  	If {\em change\_miss} = 2 then missing values will be changed
	to NaN and if {\em change\_miss} = 3 then missing values will
	be changed to {\em new\_miss}.
\item 	{\em new\_miss} is the value given to missing data if 
	{\em change\_miss} = 3.
\end{itemize}

\section{attcdf}

attcdf returns selected attributes of the a netCDF file.  The syntax
is

\vspace{0.2cm}
\begin{flushleft}
 
[a0, a1, ..., a16] = {\em attcdf}(file, var\_name, att\_name)

\end{flushleft}
\vspace{0.2cm}

We can get the same information that inqcdf gave in the earlier
example as follows:

\vspace{0.2cm}
\begin{verbatim}
>> att = attcdf('uvd', 's-u', 'missing_value')

att =

   1.0000e+36

>>
\end{verbatim}
\vspace{0.3cm}

If we leave off the third argument than all of the attributes of the
variable are returned:

\vspace{0.2cm}
\begin{verbatim}
>> [a0, a1, a2, a3] = attcdf('uvd', 's-u')    

a0 =

sd of eastwards velocity, 5 year annual mean


a1 =

cm/sec


a2 =

   1.0000e+36


a3 =

   1.0000e+36

>> 
\end{verbatim}
\vspace{0.3cm}

In more detail the meanings of the terms in the above statement are:

\begin{itemize}
\item 	{\em file} is the name of a netCDF file but without the .cdf
        or .nc extent.
\item 	{\em var\_name} is a string containing the name of the
        variable whose attribute is required.  A global attribute may
        be specified by passing the string 'global'.
\item 	{\em att\_name} is a string containing the name of the
        attribute that is required.  If att\_name is not specified
        then it is assumed that the user wants all of the attributes.

\item 	{\em [a0, a1, ..., a16]} contains the returned values.  If
        att\_name is specified then its value is
        returned in a0.  If att\_name is not specified then the first 17
        attributes of var\_name are returned in a0 to a16.  As well, if
        only 1 argument is passed to attcdf then the first 17
        global attributes are returned in a0 to a16.

\end{itemize}
 
Finally, as with the other functions, if the file is in compressed
form then the user will be given an option for automatic uncompression.

\section{get\_time}
get\_time returns time information for a file that meets COARDS
standards.  In this case time is calculated relative to a base date
which is specified in the units attribute of the time-like
variable.  We can see the format employed in the standard by using
attcdf to extract this attribute in an example.

\vspace{0.2cm}
\begin{verbatim}
>> base_string = attcdf('sst_cac_95', 'time')

base_string =

days since 1980-1-1 12:00:0.0

>> 
\end{verbatim}
\vspace{0.3cm}

The general form for using get\_time is
 
\vspace{0.2cm}
\begin{flushleft}
 
[gregorian\_time, julian\_time, gregorian\_base, julian\_base] =
  {\em get\_time}(file, time\_var);

\end{flushleft}
\vspace{0.2cm}

For the example file we get the following.

\vspace{0.2cm}
\begin{verbatim}

>> gregorian_time = get_time('sst_cac_95')

gregorian_time =

        1995           1           4          12           0           0
        1995           1          11          12           0           0
        1995           1          18          12           0           0
        1995           1          25          12           0           0
        1995           2           1          12           0           0

>>
 
\end{verbatim}
\vspace{0.2cm}

The resultant matrix has a date in each row - successive dates are a
week apart.

In more detail the meanings of the terms in the above statement are:

\begin{itemize}
\item 	{\em file} is the name of a netCDF file but without the .cdf
        or .nc extent.
\item 	{\em time\_var} is the name of the time-like variable in the
        netCDF file.  If this argument is missing then it is assumed
        that the variable name is 'time'.
\item 	{\em gregorian\_time} is an Mx6 matrix where the rows refer to
        the M times
        specified in var\_time in the netCDF file.  The columns
        are the year, month, day, hour, minute and second in that order.
\item 	{\em julian\_time} is an M vector giving the julian times
        specified in var\_time in the netCDF file.  Note that
        var\_time actually contains the julian time relative to a
        base time.
\item 	{\em gregorian\_base} is a 6-vector giving the year, month,
        day, hour, minute,
        second of the base time as specified in the 'units' attribute of
        var\_time.
\item 	{\em julian\_base} is the julian time of the base time.

\end{itemize}

\section{mexcdf}

The six functions discussed earlier are all matlab m-files designed
to make it easy to copy data from netCDF files to the matlab
workspace.  They actually work by making calls to a matlab mex-file
called mexcdf which links the netCDF library functions into matlab.
If you need to use matlab to do other things with netCDF files, for
example, create or edit them, then this can be done by calling mexcdf.
(Note, however, that it will usually be more computationally efficient,
and often easier, to do these things by writing Fortran or C programs
as was intended by the developers of netCDF.)

In general, for every C-Language NetCDF function that is called from
within a C program there is a corresponding mexcdf call of the general
form

\begin{flushleft}

[out1, out2, ...] = mexcdf('{\em operation}', in1, in2, ...)

\vspace{0.2cm}

Here {\em operation} will be the name of the corresponding C function.
For example, the matlab statement

\vspace{0.2cm}

[name, datatype, ndims, [dim], natts, status] = mexcdf('ncvarinq', cdfid, varid);

\vspace{0.2cm}

corresponds to the C function call

\vspace{0.2cm}

status = int ncvarinq(int cdfid, int varid, char* name,  nc\_type*
datatype, int* ndims, int~dim[], int* natts);

\end{flushleft}

The mexcdf function differs from the usual netCDF fuction calls in a
number of ways however.  While most of these differences, listed
below, are extensions some are restrictions.

\begin{itemize}
\item  	Dimensions and variables are accessible by id number or name.
\item	Attributes are accessible by id number or name.
\item	Parameters are accessible by id number or name.
\item	A prepended "nc" is not necessary for operation names.
\item	A prepended "NC\_" is not necessary for specifying parameters.
\item	Parameter names are not case-sensitive.
\item	The required lengths in hyperslabs default to actual lengths
	if -1 is used. 
\item	The parameter AUTOSCALE enables automatic rescaling by
	scale\_factor and add\_offset (the default is no rescaling).
\item	SETOPTS can be called to set NetCDF options; NC\_FATAL is
	disabled.
\item	ERR can be called to get the error-code of the most recent
	operation.
\item	PARAMETER can be called to access parameters by name.
\item	In generalised hyperslab access calls NULL is sent instead of
	imap.
\end{itemize}

For a more detailed description of the mexcdf syntax and capabilities
type 'help mexcdf' from within a matlab session.

(End of document)

\end{document}








