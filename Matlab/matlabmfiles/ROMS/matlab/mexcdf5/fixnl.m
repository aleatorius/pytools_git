function fixnl(theInputFile, theOutputFile)

% fixnl -- Fix newline characters.
%  fixnl(theInputFile, theOutputFile) repairs the
%   newline characters in the text file named theInputFile
%   and places the result in theOutputFile.  If no input
%   file is given, the get/put file dialogs are used.

% Copyright (C) 1996 Dr. Charles R. Denham, ZYDECO.

if nargin < 1
   help fixnl
   theInputFilename = 0;
   [theInputFilename, thePathname] = uigetfile('*.m');
   if ~any(theInputFilename), return, end
   theInputFile = [thePathname theInputFilename];
end

fp = fopen(theInputFile, 'r');
s = setstr(fread(fp).');
fclose(fp);

if nargin < 1
   theOutputFile = 0;
   [theOutputFilename, thePathname] = uiputfile(theInputFilename);
   if ~any(theOutputFilename), return, end
   theOutputFile = [thePathname theOutputFilename];
end

CR = setstr(13);   % Carriage-return.
LF = setstr(10);   % Line-feed.
CRLF = [CR LF];    % PC and Vax style.

c = computer;
if findstr(computer, 'MAC')           % Macintosh.
   NL = CR;
  elseif findstr(computer, 'PCWIN')   % PC.
   NL = CRLF;
  elseif findstr(computer, 'VMS')     % Vax VMS.
   NL = CRLF;
  else                                % Unix.
   NL = LF;
end

s = strrep(s, CRLF, CR);
s = strrep(s, LF, CR);
s = strrep(s, CR, NL);

fp = fopen([theOutputFile '.tmp'], 'w');
fwrite(fp, s);
fclose(fp);
