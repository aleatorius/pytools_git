function [w,jd] = getvel(cdf,uvar,vvar,start,stop)
%function [w,jd] = getvel(cdf,uvar,vvar,[start],[stop])
% get velocity from EPIC style time series file
if(nargin<5),
 [u,jd]=getts(cdf,uvar);
 [v,jd]=getts(cdf,vvar);
else
 [u,jd]=getts(cdf,uvar,start,stop);
 [v,jd]=getts(cdf,vvar,start,stop);
end
w=u(:)+sqrt(-1)*v(:);
