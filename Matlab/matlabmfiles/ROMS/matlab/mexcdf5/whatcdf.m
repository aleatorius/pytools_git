function whatcdf

%--------------------------------------------------------------------
%     Copyright (C) J. V. Mansbridge, CSIRO, january 24 1992
%     Revision $Revision: 1.2 $
%
% DESCRIPTION:
% whatcdf lists all of the netCDF files in the current directory but without
% their .cdf or .nc suffices; compressed files are listed also.  Lastly, all
% files in the common data set are given except for any named zzmore (This
% allows zzmore to be a directory pointing to 'hidden' netCDF files).
% 
% INPUT:
% none
%
% OUTPUT:
% messages to the user's terminal
%
% EXAMPLE:
% Simply type whatcdf at the matlab prompt.
%
% CALLER:   general purpose
% CALLEE:   Unix ls, sed
%
% AUTHOR:   J. V. Mansbridge, CSIRO
%---------------------------------------------------------------------

%     Copyright (C), J.V. Mansbridge, 
%     Commonwealth Scientific and Industrial Research Organisation
%     Revision $Revision: 1.2 $
%     Author   $Author: mansbrid $
%     Date     $Date: 93/06/22 11:33:29 $
%     RCSfile  $RCSfile: whatcdf.m,v $
% @(#)whatcdf.m   1.3   92/05/18
% 
%--------------------------------------------------------------------

disp(' ')
disp('-----  current directory netCDF files  -----')
!ls -C *.cdf *.nc | sed -e 's/\.cdf/    /g' | sed -e 's/\.nc/   /g'
disp(' ')
disp('-----  current directory compressed netCDF files  -----')
!ls -C *.cdf.Z *.nc.Z | sed -e 's/\.cdf\.Z/      /g' | sed -e 's/\.nc\.Z/     /g'
disp(' ')
disp('-----  common data set of netCDF files  -----')
path_name = pos_data_cdf;
command = [ '!ls -C "' path_name '" | sed -e ''s/\.cdf/    /g''' ...
    ' | sed -e ''s/\.nc/   /g'' | sed -e ''s/zzmore/      /g''' ];
eval(command)
disp(' ')

