function inqcdf(file)

%--------------------------------------------------------------------
%     Copyright (C) J. V. Mansbridge, CSIRO, january 24 1992
%     Revision $Revision: 1.7 $
%
%  function inqcdf(file)
%
% DESCRIPTION:
%  inqcdf('file') is an interactive function that returns information
%  about the NetCDF file 'file.cdf' or 'file.nc'.  If the file is in
%  compressed form then the user will be given an option for automatic
%  uncompression.
% 
% INPUT:
%  file is the name of a netCDF file but without the .cdf or .nc extent.
%
% OUTPUT:
%  information is written to the user's terminal.
%
% CALLER:   general purpose
% CALLEE:   check_cdf.m, mexcdf.mex
%
% AUTHOR:   J. V. Mansbridge, CSIRO
%---------------------------------------------------------------------

%     Copyright (C), J.V. Mansbridge, 
%     Commonwealth Scientific and Industrial Research Organisation
%     Revision $Revision: 1.7 $
%     Author   $Author: mansbrid $
%     Date     $Date: 1994/02/16 00:17:31 $
%     RCSfile  $RCSfile: inqcdf.m,v $
% @(#)inqcdf.m   1.5   92/05/26
% 
% Note that the netcdf functions are accessed by reference to the mex
% function mexcdf.
%--------------------------------------------------------------------

% Check the number of arguments.

if nargin < 1
   help inqcdf
   return
end

% Do some initialisation.

blank = abs(' ');

% Check that the file is accessible.  If it is then its full name will
% be stored in the variable cdf.  The file may have the extent .cdf or
% .nc and be in the current directory or the common data set (whose
% path can be found by a call to pos_data_cdf.m.  If a compressed form
% of the file is in the current directory then the user is prompted to
% uncompress it.  If, after all this, the netcdf file is not accessible
% then the m file is exited with an error message.

ilim = 2;
for i = 1:ilim

  if i == 1
    cdf = [ file '.cdf' ];
  elseif i == 2
    cdf = [ file '.nc' ];
  end

  err = check_cdf(cdf);

  if err == 0
    break;
  elseif err == 1
    if i == ilim
      error([ file ' could not be found' ])
    end
  elseif err == 2
    path_name = pos_data_cdf;
    cdf = [ path_name cdf ];
    break;
  elseif err == 3
    err1 = uncompress_cdf(cdf);
    if err1 == 0
      break;
    elseif err1 == 1
      disp([ 'exiting because you chose not to uncompress ' cdf ])
      return;
    elseif err1 == 2
      error([ 'exiting because ' cdf ' could not be uncompressed' ])
    end
  end
end

% Open the netcdf file.
  
[cdfid, rcode] = mexcdf('ncopen', cdf, 'nowrite');

% don't print out netcdf warning messages

mexcdf('setopts',0);

if rcode == -1
  error([ 'mexcdf: ncopen: rcode = ' int2str(rcode) ])
end

% Collect information about the cdf file.

[ndims, nvars, ngatts, recdim, rcode] =  mexcdf('ncinquire', cdfid);
if rcode == -1
   error([ 'mexcdf: ncinquire: rcode = ' int2str(rcode) ])
end

% Find and print out the global attributes of the cdf file.

if ngatts > 0
   disp('                ---  Global attributes  ---')
   for i = 0:ngatts-1
     [attnam, rcode] = mexcdf('attname', cdfid, 'global', i);
     [attype, attlen, rcode] = mexcdf('ncattinq', cdfid, 'global', attnam);
     [values, rcode] = mexcdf('ncattget', cdfid, 'global', attnam);
%keyboard   
      if attype == 2
         s = values;
      elseif attype == 3 | attype == 4
         s = [];
         for i = 1:length(values)
            s = [ s int2str(values(i)) '  ' ];
         end
      elseif attype == 5 | attype == 6
         s = [];
         for i = 1:length(values)
            s = [ s num2str(values(i)) '  ' ];
         end
      end
      s = [ attnam ': ' s ];
      disp(s)
   end
else
   disp('   ---  There are no Global attributes  ---')
end

% Get and print out information about the dimensions.

disp(' ')
s = [ 'The ' int2str(ndims) ' dimensions are' ];
for i = 0:ndims-1
  [dimnam, dimsiz, rcode] = mexcdf('ncdiminq', cdfid, i);
  s = [ s '  ' int2str(i+1) ') ' dimnam ' = ' int2str(dimsiz) ];
end
s = [ s '.'];

disp(s)
if recdim <= 0
   disp('None of the dimensions is unlimited')
else
   [dimnam, dimsiz, rcode] = mexcdf('ncdiminq', cdfid, recdim);
   s = [ dimnam ' is unlimited in length'];
   disp(s)
end

% Print out the names of all of the variables so that the user may
% choose to 1) finish the inquiry, 2) print out information about all
% variables or 3) print out information about only one of them.

infinite = 1;
while infinite
   k = -2;
   while k <-1 | k > nvars
      disp(' ')
      s = [ '----- Get further information about the following variables -----'];
      disp(s)
      disp(' ')
      s = [ '  -1) None of them (no further information)' ];
      disp(s)
      s = [ '   0) All of the variables' ];
      disp(s)
      for i = 0:3:nvars-1
         stri = int2str(i+1);
         if length(stri) == 1
            stri = [ ' ' stri];
         end
         [varnam, vartyp, nvdims, vdims, nvatts, rcode] = ...
         mexcdf('ncvarinq', cdfid, i);
         s = [ '  ' stri ') ' varnam ];
         addit = 26 - length(s);
         for j =1:addit
            s = [ s ' '];
         end
   
         if i < nvars-1
            stri = int2str(i+2);
            if length(stri) == 1
               stri = [ ' ' stri];
            end
            [varnam, vartyp, nvdims, vdims, nvatts, rcode] = ...
            mexcdf('ncvarinq', cdfid, i+1);
            s = [ s '  ' stri ') ' varnam ];
            addit = 52 - length(s);
            for j =1:addit
               s = [ s ' '];
            end
         end 
   
         if i < nvars - 2
            stri = int2str(i+3);
            if length(stri) == 1
               stri = [ ' ' stri];
            end
            [varnam, vartyp, nvdims, vdims, nvatts, rcode] = ...
            mexcdf('ncvarinq', cdfid, i+2);
            s = [ s '  ' stri ') ' varnam ];
         end 
         disp(s)
      end
      disp(' ')
      s = [ 'Select a menu number: '];
      k = input(s);
   end
   
% Get and print out information about as many variables as necessary.
% If k == - 1 close the netcdf file and return.
   
   if k == -1
      [rcode] = mexcdf('ncclose', cdfid);
      if rcode == -1
	error(['** ERROR ** ncclose: rcode = ' num2str(rcode)])
      end
      return
   elseif k == 0
      klow = 0;
      kup = nvars - 1;
   else
      klow = k - 1;
      kup = k - 1;
   end
   
   if nvars > 0
      for k = klow:kup
         [varnam, vartyp, nvdims, vdims, nvatts, rcode] = ...
                  mexcdf('ncvarinq', cdfid, k); 
   
% Write out a message containing the dimensions of the variable.

         s = [ '   ---  Information about ' varnam '(' ];
         for j = 1:nvdims
	   [dimnam, dimsiz, rcode] = mexcdf('ncdiminq', cdfid, vdims(j));
	   s = [ s dimnam ' ' ];
         end
         s = [ s ')  ---' ];
         disp(' ')
         disp(s)
   
% Find and print out the attributes of the variable.
   
         if nvatts > 0
            disp(' ')
            s = [ '   ---  ' varnam ' attributes  ---' ];
            left_side = 1;
	    for j = 0:nvatts-1
	       [attnam, rcode] = mexcdf('ncattname', cdfid, k, j); 
	       [attype, attlen, rcode] = mexcdf('ncattinq', cdfid, ...
		        k, attnam); 
	       [values, rcode] = mexcdf('ncattget', cdfid, k, attnam);
   
               if attype == 2
                  s = values;
               elseif attype == 3 | attype == 4
                  s = [];
                  for ii = 1:length(values)
                     s = [ s int2str(values(ii)) '  ' ];
                  end
               elseif attype == 5 | attype == 6
                  s = [];
                  for ii = 1:length(values)
                     s = [ s num2str(values(ii)) '  ' ]; 
                  end
               end
   
% Go through convolutions to try to fit information about two attributes onto
% one line.
   
               le_att = length(attnam);
               le_s = length(s);
               le_sum = le_att + le_s;
               st = [ '*' attnam ': ' s ];
               if left_side == 1
                  if le_sum > 37
                     disp(st)
                  else
                     n_blanks = 37 - le_sum;
                     if n_blanks > 1
                        for ii = 1:n_blanks
                           st = [ st ' ' ];
                        end
                     end
                     temp = st;
                     left_side = 0;
                  end
               else
                  if le_sum > 37
                     disp(temp)
                  else
                     st = [ temp st ];
                  end
                  disp(st)
                  left_side = 1;
               end
            end
            if left_side == 0
               disp(temp)
            end
         else
            s = [ '*  ' varnam ' has no attributes' ];
            disp(s)
         end
      end
   else
      disp(' ')
      disp('   ---  There are no variables  ---')
   end
end
