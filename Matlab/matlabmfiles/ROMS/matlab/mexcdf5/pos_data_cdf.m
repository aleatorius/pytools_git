function path_name = pos_data_cdf() 

%--------------------------------------------------------------------
%     Copyright (C) J. V. Mansbridge, CSIRO, april 15 1992
%     Revision $Revision: 1.2 $
%
% DESCRIPTION:
% pos_data_cdf returns the path to the common data set directory.
% This is the directory containing netcdf files accessible to all
% users.
% 
% INPUT:
% none
%
% OUTPUT:
% path_name: the path to the common data set directory.
%
% EXAMPLE:
% Simply type path_name at the matlab prompt.
%
% CALLER:   check_cdf.m, getcdf.m, getcdf_batch.m, inqcdf.m, whatcdf.m
% CALLEE:   None
%
% AUTHOR:   J. V. Mansbridge, CSIRO
%---------------------------------------------------------------------

%     Copyright (C), J.V. Mansbridge, 
%     Commonwealth Scientific and Industrial Research Organisation
%     Revision $Revision: 1.2 $
%     Author   $Author: mansbrid $
%     Date     $Date: 1994/02/16 00:17:31 $
%     RCSfile  $RCSfile: pos_data_cdf.m,v $
% @(#)pos_data_cdf.m   1.1   92/04/16
% 
%--------------------------------------------------------------------

%path_name = [ ];
path_name = [ '/usr/local/netcdf-data/' ];
