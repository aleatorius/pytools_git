function [dx,xmin,xmax]=deltax(x,ndiv);
%[dx,xmin,xmax]=deltax(x,ndiv);
%  function for figuring out even divisions, 1,2,5,10 etc.
				  
dxarray=[1 2 5 10];
xmin=ming(x(:));
xmax=maxg(x(:));
dx=(xmax-xmin)/ndiv;
dxpower=10^floor(log(dx)/log(10));
a=near(dxarray,dx/dxpower);
dx=dxpower*dxarray(a(1));
xmin=floor(xmin/dx)*dx;
xmax=ceil(xmax/dx)*dx;
