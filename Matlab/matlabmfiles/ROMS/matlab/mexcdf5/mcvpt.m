function values = mcvpt(cdf, var, corner,count,values)

% MCVPT Get a NetCDF hyperslab.
%  MCVPT('CDF','VAR',[COORD],[COUNT],VAL) returns the hyperslab 
%   from NetCDF file 'CDF, variable 'VAR', starting at corner 
%   [COORD] and containing [COUNT] elements along the edges.  
%   The starting index of an array is 0 (C-Language convention).
%
% Example:
%    value = mcvpt('foo.cdf', 'x', [0 0], [2 3],[1 2 3; 2 3 4])

% Show usage if too few arguments.
%
if nargin~=3 & nargin~=5,
   help mcvpt;
   return;
end
%
% Open netCDF file
%
[cdfid,rcode ]=mexcdf('open',cdf,'WRITE');
%
% Suppress warning messages from netCDF
[rcode]=mexcdf('setopts',0);
%
% Get variable id
%
[varid]=mexcdf('varid',cdfid,var);
[var_name,var_type,nvdims,var_dim,natts]=mexcdf('varinq',cdfid,varid);
attstring = fill_attnames(cdfid, varid, natts);
%
% Automagically put all the data if slab is not specified
%
if nargin==3,
  for n=1:nvdims,
    dimid=var_dim(n);
    [dim_name,dim_size,rcode]=mexcdf('diminq',cdfid,dimid);
    corner(n)=0;
    count(n)=dim_size;
  end
end
%
% put slab
% 
values=values.';  % want fastest varying dimension to be # of rows
mexcdf('varput',cdfid,varid,corner,count,values,1);
mexcdf('close',cdfid);
