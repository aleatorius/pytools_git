function ans=last(x);
% function ans=last(x);
% finds last value of x
ans=x(length(x));
