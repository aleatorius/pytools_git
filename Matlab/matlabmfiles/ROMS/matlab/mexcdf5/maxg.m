function a=maxg(b);
g=find(b(:)~=NaN);
a=max(b(g));
