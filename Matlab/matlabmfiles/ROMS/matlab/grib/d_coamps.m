%  This script creates/writes/appends COAMPS NetCDF data from GRIB
%  flat files.

ICREATE=0;
IWRITE=1;
WRT_JPEG=0;
IPLOT=0;

dir='/coamps/mel/Jul99/coamps_flat/';
OutName='/coamps/mel/Jul99/netcdf/coamps_jul99.nc';

time.year=1999;
time.month=7;

%  The starting and ending days is by 0.5 intervals since the data
%  is generated every 12 hours.

day_str=30.5;
day_end=31.5;
tau=0;

im=191;
jm=176;

%-----------------------------------------------------------------------
%  Create COAMPS NetCDF file.
%-----------------------------------------------------------------------

%  Set structure arrays: define (variables to define), grided
%  (point or grided data), Tname (name of time variable), and
%  Vname (name of forcing variable).  Switches are: 0=no, 1=yes.

define.time =1;             % time.
Vname.time  ='time';

define.wind =1;             % surface wind components
Vname.suw   ='Uwind';
Vname.svw   ='Vwind';

define.pair =1;             % surface air pressure
Vname.pair  ='Pair';

define.tair =1;             % surface air temperature
Vname.tair  ='Tair';

define.qair =1;             % surface relative humidity
Vname.qair  ='humidity';

define.rain =1;             % rain fall rate
Vname.rain  ='rain';

if (ICREATE),
  [status]=c_coamps(OutName,define,Vname);
  disp(['Created COAMPS NetCDF file: ',OutName]);
end,

%-----------------------------------------------------------------------
%  Plot COAMPS data from flat GRIB files.
%-----------------------------------------------------------------------

if ( IPLOT );

%  Read in coastline data.

  cst_file=['/home/arango/ocean/plot/Data/us_cst.dat'];
  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=900);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  clear icst;

%  Read in and plot COAMPS data.  Create one postscript file for
%  each time record.

  for day=day_str:0.5:day_end,
    time.day=floor(day);
    time.hour=rem(day,1)*24;
    time.Jday=julian(time.year,time.month,time.day,time.hour)-2440000;
    for tau=0:6:48;
      [u,v,t,p,rh,q]=r_coamps(dir,im,jm,time,tau);
      if (got),
        if (ICREATE & IWRITE),
          figure(1);
        else,
          figure;
        end,
        clf;
        [h]=p_grib_coamps(time,tau,u,v,p,t,rh,q,cstlon,cstlat); 
        hold off;
        if (WRT_JPEG),
          eval(['print -djpeg90 coamps_',num2str(time.month,'%2.2i'),...
          num2str(time.day,'%2.2i'),num2str(time.hour,'%2.2i'),...
          num2str(tau,'%2.2i'),'.jpg']);
        end,
      end,
    end,
  end,
end,

%-----------------------------------------------------------------------
%  Write out COAMPS data into NetCDF file.
%-----------------------------------------------------------------------

if ( IWRITE );

  for day=day_str:0.5:day_end,
    time.day=floor(day);
    time.hour=rem(day,1)*24;
    time.Jday=julian(time.year,time.month,time.day,time.hour)-2440000;
    tindex=(day*2)+(time.hour/12)-1;
    disp(['Time - yy:mm:day:hour = ', ...
          num2str(time.year),':', ...
          num2str(time.month,'%2.2i'),':', ...
          num2str(time.day,'%2.2i'),':', ...
          sprintf('%5.2f',time.hour)]);
    status=w_coamps(dir,time,define,Vname,OutName,tindex);
  end,

end,
