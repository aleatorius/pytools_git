function [h]=fill_noraps(Julday,tau,u,v,p,t,rh,q,cstlon,cstlat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [h]=fill_noraps(Julday,tau,u,v,p,t,rh,q,cstlon,cstlat)     %
%                                                                      %
%  This function plots color-filled NORAPS data.                                    %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     Julday    Modified Julian day.                                   %
%     tau       Forecast time (hours).                                 %
%     u         East-West wind component (m/s).                        %
%     v         North-West wind component (m/s).                       %
%     t         Air temperature at 10m (C).                            %
%     s         Surface pressure (mb).                                 %
%     rh        Relative humidity (%).                                 %
%     q         precipitation (cm).                                    %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     h         figure handle.                                         %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Set switch to draw coastlines.

ICOAST=1;
if (nargin < 9),
  ICOAST=0;
end,

%-----------------------------------------------------------------------
%  Set date stamp.
%-----------------------------------------------------------------------

wkday=['Sun. ';'Mon. ';'Tue. ';'Wed. ';'Thu. ';'Fri. ';'Sat. '];
lwkday=[4,4,4,4,4,4,4];
Month=['Jan ';'Feb ';'Mar ';'Apr ';'May ';'Jun ';'Jul ';'Aug ';...
       'Sep ';'Oct ';'Nov ';'Dec '];
lmonth=[3,3,3,3,3,3,3,3,3,3,3,3];
ampm=[' AM';' PM'];

%  Convert Julian day to Gregorian day.

gtime=gregorian(Julday+2440000.0);

year  =gtime(1);
month =gtime(2);
day   =gtime(3);
hour  =gtime(4);
minute=gtime(5);
second=gtime(6);
iwday=day_code(month,day,year);

%  Set date stamp.

stamp1=['Issued: ',wkday(iwday,1:lwkday(iwday)),' ',Month(month,1:lmonth(month)),...
        ' ',num2str(day),', ',num2str(year),' - ',num2str(hour),' GMT.'];

gtime=gregorian(Julday+tau/24.0+2440000.0);

year  =gtime(1);
month =gtime(2);
day   =gtime(3);
hour  =gtime(4);
minute=gtime(5);
second=gtime(6);
iwday=day_code(month,day,year);
stamp2=['NORAPS, Valid: ',wkday(iwday,1:lwkday(iwday)),' ',Month(month,1:lmonth(month)),...
        ' ',num2str(day),', ',num2str(hour),' GMT.'];

%-----------------------------------------------------------------------
%  Set NORAPS grid.
%-----------------------------------------------------------------------

[im,jm]=size(p);

lon_min=-124.0;
lon_max=-52;
del_lon=(lon_max-lon_min)/(im-1);

lat_min=5.0;
lat_max=51.0;
del_lat=(lat_max-lat_min)/(jm-1);

lon=lon_min:del_lon:lon_max; lon=lon';
lat=lat_min:del_lat:lat_max;

Lon=repmat(lon,[1 jm]);
Lat=repmat(lat,[im 1]);

%-----------------------------------------------------------------------
%  Set range of data to plot.
%-----------------------------------------------------------------------

LonMin=-76.5;
LonMax=-70.5;

LatMin=36.0;
LatMax=42.0;

LonRange=find((lon >= LonMin) & (lon <= LonMax));
LatRange=find((lat >= LatMin) & (lat <= LatMax));

X=Lon(LonRange,LatRange);
Y=Lat(LonRange,LatRange);

%-----------------------------------------------------------------------
%  Plot surface pressure and winds.
%-----------------------------------------------------------------------

m_proj('Mercator');
hold off;

subplot(2,2,1);

plev=990:0.5:1040;
plab=990:2:1040;
F=p(LonRange,LatRange);

Ptext=[];
if (~isnan(F)),
 [c,h]=contourf(X,Y,F,20);
 set(h,'edgecolor','k');
 colormap('jet');
 colorbar;
% H=clabel(c,h,plab);
 Ptext='Surface Pressure (mb)';
end,
hold on;
grid on;

Fx=u(LonRange,LatRange);
Fy=v(LonRange,LatRange);
F=sqrt(Fx.*Fx + Fy.*Fy);

Ttext=[];
RangeLab=[];
if (~isnan(F)),
  quiver(X,Y,Fx,Fy,'w');
  set(h,'edgecolor','k');
  TauMin=min(min(F));
  TauMax=max(max(F));
  RangeLab=[' Min= ',num2str(TauMin),', Max= ',num2str(TauMax)];
  if (isempty(Ptext)),
    Ttext='Winds (m/s) at 10m';
  else,
    Ttext=' and Wind Speed (m/s) at 10m';
  end,
end,

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

title(stamp1);
if (~isempty(RangeLab)),
  xlabel({[Ptext Ttext],[RangeLab]});
else,
  xlabel([Ptext Ttext]);
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

%-----------------------------------------------------------------------
%  Plot air temperature.
%-----------------------------------------------------------------------

subplot(2,2,2);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

tlev=0:1:40;
tlab=0:1:40;
F=t(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [c,h]=contourf(X,Y,F,20);
  set(h,'edgecolor','k');
  colormap('jet');
  colorbar;
%  H=clabel(c,h,tlab);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

title(stamp2);
if (~isempty(RangeLab)),
  xlabel({'Temperature at 2m (C)',[RangeLab]});
else,
  xlabel('Temperature at 2m (C)');
end,
ylabel('Latitude');

set(gca,'nextplot','replace')


%-----------------------------------------------------------------------
%  Plot relative humidity.
%-----------------------------------------------------------------------

subplot(2,2,3);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

rhlev=10:10:100;
rhlab=10:10:100;
F=rh(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [c,h]=contourf(X,Y,F,20);
  set(h,'edgecolor','k');
  colormap('jet');
  colorbar;
%  H=clabel(c,h,rhlab);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

if (~isempty(RangeLab)),
  xlabel({'Relative Humidity (%)',[RangeLab]});
else,
  xlabel('Relative Humidity (%)');
end,
ylabel('Latitude');

set(gca,'nextplot','replace')

%-----------------------------------------------------------------------
%  Plot precipitation.
%-----------------------------------------------------------------------

subplot(2,2,4);

qlev=0:0.02:10;
qlab=0:0.5:5;
F=q(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [c,h]=contour(X,Y,F,20);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

if (~isempty(RangeLab)),
  xlabel({'12-hour precipitation (cm)',[RangeLab]});
else,
  xlabel('12-hour precipitation (cm)');
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

return
