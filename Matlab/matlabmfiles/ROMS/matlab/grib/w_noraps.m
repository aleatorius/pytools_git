function [status]=w_noraps(dir,year,month,day,hour,fname,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=w_noraps(dir,year,month,day,hour,fname,tindex)     %
%                                                                      %
% This reads in NORAPS GRIB fields and writes them into specified      %
% NetCDF file at the stipulated time record.                           %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    dir       Flat GRIB file directory (string).                      %
%    year      Year to process (integer).                              %
%    month     Month to process (integer).                             %
%    day       Day to process (integer).                               %
%    hour      Hour to process (integer).                              %
%    fname     NetCDF file name (string).                              %
%    tindex    NetCDF time record to write (integer, optional).        %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status    Error flag (integer).                                   %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_dim, read_grib, nc_write                                 %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Set NORAPS dimension parameters.
%-----------------------------------------------------------------------

Im=145;
Jm=93;
Nm=9;

delta=6;
spval=-999.0;

%-----------------------------------------------------------------------
%  Read in GRIB flat files.
%-----------------------------------------------------------------------

for fcast=1:Nm
  tau=delta*(fcast-1);
  [u,v,t,p,h,q]=r_noraps(dir,Im,Jm,year,month,day,hour,tau);
  ind=find(isnan(u));
  if (~isempty(ind)), u(ind)=spval; end,
  ind=find(isnan(v));
  if (~isempty(ind)), v(ind)=spval; end,
  ind=find(isnan(t));
  if (~isempty(ind)), t(ind)=spval; end,
  ind=find(isnan(p));
  if (~isempty(ind)), p(ind)=spval; end,
  ind=find(isnan(h));
  if (~isempty(ind)), h(ind)=spval; end,
  ind=find(isnan(q));
  if (~isempty(ind)), q(ind)=spval; end,
  U(:,:,fcast)=u;
  V(:,:,fcast)=v;
  T(:,:,fcast)=t;
  P(:,:,fcast)=p;
  H(:,:,fcast)=h;
  Q(:,:,fcast)=q;
end,

%-----------------------------------------------------------------------
%  Get Julian Day.
%-----------------------------------------------------------------------

Julday=julian(year,month,day,hour)-2440000;

%-----------------------------------------------------------------------
%  If time record is not provided, inquire NetCDF file for index to
%  append.
%-----------------------------------------------------------------------

if (nargin < 7),
 
  [dnames,dsizes]=nc_dim(fname);
  ndims=length(dsizes);

  for n=1:ndims,
    name=dnames(n,:);
    if (name(1:4) == 'time'),
      tindex=dsizes(n)+1;
    end,
  end,

end,

%-----------------------------------------------------------------------
%  Write out fields into NetCDF file.
%-----------------------------------------------------------------------

status=nc_write(fname,'time',Julday,tindex);
if (status == 0),
  disp(['Wrote  time      at record: ',num2str(tindex)]);
end,

status=nc_write(fname,'uwind',U,tindex);
if (status == 0),
  disp(['Wrote  uwind     at record: ',num2str(tindex)]);
end,

status=nc_write(fname,'vwind',V,tindex);
if (status == 0),
  disp(['Wrote  vwind     at record: ',num2str(tindex)]);
end,

status=nc_write(fname,'Tair',T,tindex);
if (status == 0),
  disp(['Wrote  Tair      at record: ',num2str(tindex)]);
end,

status=nc_write(fname,'Pair',P,tindex);
if (status == 0),
  disp(['Wrote  Pair      at record: ',num2str(tindex)]);
end,

status=nc_write(fname,'humidity',H,tindex);
if (status == 0),
  disp(['Wrote  humidity  at record: ',num2str(tindex)]);
end,

status=nc_write(fname,'precip',Q,tindex);
if (status == 0),
  disp(['Wrote  precip    at record: ',num2str(tindex)]);
end,

return

