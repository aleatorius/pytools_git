function [status]=w_coamps(dir,time,define,Vname,fname,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=w_coamps(dir,time,define,Vname,fname,tindex)       %
%                                                                      %
% This reads in COAMPS GRIB fields and writes them into specified      %
% NetCDF file at the stipulated time record.                           %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    dir       Flat GRIB file directory (string).                      %
%    time      time of data to write (structure array):                %
%                 time.Jday  => Julian day.                            %
%                 time.year  => year of the century.                   %
%                 time.month => month of the year (1-12).              %
%                 time.day   => day of the month.                      %
%                 time.hour  => hour of the day (GMT, 1-24).           %
%    define      Switches indicating which variables to defined        %
%                  (0: no, 1: yes), structure array.                   %
%    Vname       Name of defined forcing NetCDF variables.             %
%    fname     NetCDF file name (string).                              %
%    tindex    NetCDF time record to write (integer, optional).        %
%                                                                      %
%                Variables in structure arrays:                        %
%                                                                      %
%                  ***.wind   => surface wind components.              %
%                  ***.suw    => surface U-wind component.             %
%                  ***.svw    => surface U-wind component.             %
%                  ***.pair   => surface air pressure.                 %
%                  ***.tair   => surface air temperature.              %
%                  ***.qair   => surface relative humidity.            %
%                  ***.rain   => rain fall rate.                       %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status    Error flag (integer).                                   %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_dim, read_grib, nc_write                                 %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Set COAMPS dimension parameters.
%-----------------------------------------------------------------------

Im=191;
Jm=176;
Nm=9;

delta=6;
spval=-999.0;
got=0;

%-----------------------------------------------------------------------
%  Read in GRIB flat files.
%-----------------------------------------------------------------------

for fcast=1:Nm
  tau=delta*(fcast-1);
  [u,v,t,p,h,q,got]=r_coamps(dir,Im,Jm,time,tau);
  ind=find(isnan(u));
  if (~isempty(ind)), u(ind)=spval; end,
  ind=find(isnan(v));
  if (~isempty(ind)), v(ind)=spval; end,
  ind=find(isnan(t));
  if (~isempty(ind)), t(ind)=spval; end,
  ind=find(isnan(p));
  if (~isempty(ind)), p(ind)=spval; end,
  ind=find(isnan(h));
  if (~isempty(ind)), h(ind)=spval; end,
  ind=find(isnan(q));
  if (~isempty(ind)), q(ind)=spval; end,
  U(:,:,fcast)=u;
  V(:,:,fcast)=v;
  T(:,:,fcast)=t;
  P(:,:,fcast)=p;
  H(:,:,fcast)=h;
  Q(:,:,fcast)=q;
end,

%-----------------------------------------------------------------------
%  If time record is not provided, inquire NetCDF file for index to
%  append.
%-----------------------------------------------------------------------

if (nargin < 7),
 
  [dnames,dsizes]=nc_dim(fname);
  ndims=length(dsizes);

  for n=1:ndims,
    name=dnames(n,:);
    if (name(1:4) == 'time'),
      tindex=dsizes(n)+1;
    end,
  end,

end,

%-----------------------------------------------------------------------
%  Write out fields into NetCDF file.
%-----------------------------------------------------------------------

if (got),

  status=nc_write(fname,Vname.time,time.Jday,tindex);

  if (define.wind),
    status=nc_write(fname,Vname.suw,U,tindex);
  end,

  if (define.wind),
    status=nc_write(fname,Vname.svw,V,tindex);
  end,

  if (define.tair),
    status=nc_write(fname,Vname.tair,T,tindex);
  end,

  if (define.pair),
    status=nc_write(fname,Vname.pair,P,tindex);
  end,

  if (define.qair),
    status=nc_write(fname,Vname.qair,H,tindex);
  end,

  if (define.rain),
    status=nc_write(fname,Vname.rain,Q,tindex);
  end,

end,

return

