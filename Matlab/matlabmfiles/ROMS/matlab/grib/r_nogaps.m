function [u,v,p,t,d,rh,q,c,got]=r_nogaps(dir,im,jm,time,tau);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [u,v,p,t,d,r,c,got]=r_nogaps(dir,im,jm,time,tau)            %
%                                                                      %
% This function reads in NOGAPS GRIB files.                            %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    im        Number of colums to read (integer).                     %
%    jm        Number of rows to read (integer).                       %
%    dir       Flat GRIB file directory (string).                      %
%    time      time of data to write (structure array):                %
%                time.Jday  => Julian day.                             %
%                time.year  => year of the century.                    %
%                time.month => month of the year (1-12).               %
%                time.day   => day of the month.                       %
%                time.hour  => hour of the day (GMT, 1-24).            %
%    tau       Forecast length in hours (integer).                     %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    u         East-West wind component (m/s).                         %
%    v         North-West wind component (m/s).                        %
%    p         Surface pressure (mb).                                  %
%    t         Air temperature at 10m (Celsius).                       %
%    d         Dew Point Temperature (Celsius).                        %
%    rh        Relative humidity (%).                                  %
%    q         Accumulated precipitation (mm/12 hour).                 %
%    c         Total cloud cover (%).                                  %
%    got       Counter of fields found.                                %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Build GRIB flat files names.
%-----------------------------------------------------------------------

if (time.year < 100),
  syy=int2str(time.year);
else,
  syy=int2str(time.year-1900);
end,

smm=int2str(time.month);
if (time.month < 10),
  smm=['0' smm];
end,

sdd=int2str(time.day);
if (time.day < 10),
  sdd=['0' sdd];
end,

shh=int2str(time.hour);
if (time.hour < 10),
  shh=['0' shh];
end,

stau=int2str(tau);
if (tau < 10),
  stau=['00' stau];
elseif (tau < 100),
  stau=['0' stau];
end,

u_file=[dir 'FF' syy smm sdd shh stau '033.240.105.00010'];
v_file=[dir 'FF' syy smm sdd shh stau '034.240.105.00010'];
t_file=[dir 'FF' syy smm sdd shh stau '011.240.105.00002'];
p_file=[dir 'FF' syy smm sdd shh stau '002.240.102.00000'];
d_file=[dir 'FF' syy smm sdd shh stau '018.240.100.01000'];
q_file=[dir 'FF' syy smm sdd shh stau '061.240.001.00000'];
c_file=[dir 'FF' syy smm sdd shh stau '071.240.001.00000'];

got=0;

%-----------------------------------------------------------------------
%  Read in wind components at 10 m.
%-----------------------------------------------------------------------

uid=fopen(u_file,'r');
if (uid > 0),
  u=fread(uid,[im,jm],'float');
  fclose(uid);
  got=got+1;
else,
  u=NaN.*(ones([im jm]));
end,

vid=fopen(v_file,'r');
if (vid > 0),
  v=fread(vid,[im,jm],'float');
  fclose(vid);
  got=got+1;
else,
  v=NaN.*(ones([im jm]));
end,

%-----------------------------------------------------------------------
%  Read in air temperature at 2m.  Convert to Celsisus.
%-----------------------------------------------------------------------

GOT.Tair=0;
tid=fopen(t_file,'r');
if (tid > 0),
  t=fread(tid,[im,jm],'float');
  t=t-273.16;
  fclose(tid);
  got=got+1;
  GOT.Tair=1;
else,
  t=NaN.*(ones([im jm]));
end,

%-----------------------------------------------------------------------
%  Read in pressure reduced to mean sea level.
%-----------------------------------------------------------------------

pid=fopen(p_file,'r');
if (pid > 0),
  p=fread(pid,[im,jm],'float');
  p=p./100.0;
  fclose(pid);
  got=got+1;
else,
  p=NaN.*(ones([im jm]));
end,

%-----------------------------------------------------------------------
%  Read in dew-point depression. Convert to dew-point temperature.
%-----------------------------------------------------------------------

GOT.Tdew=0;
did=fopen(d_file,'r');
if (did > 0),
  d=fread(did,[im,jm],'float');
  d=t-d;
  fclose(did);
  got=got+1;
  GOT.Tdew=1;
else,
  d=NaN.*(ones([im jm]));
end,

%-----------------------------------------------------------------------
%  Compute relative humidity.
%-----------------------------------------------------------------------

if (GOT.Tair & GOT.Tdew),
  Es=6.11.*10.^(7.5.*t./(237.7+t));
  E =6.11.*10.^(7.5.*d./(237.7+d));
  rh=(E./Es).*100;
  rh=min(100.0,rh);
end,

%-----------------------------------------------------------------------
%  Read in Accumulated precipitation (mm/12 hour).
%-----------------------------------------------------------------------

rid=fopen(q_file,'r');
if (rid > 0),
  q=fread(rid,[im,jm],'float');
  fclose(rid);
  got=got+1;
else,
  q=zeros([im jm]);
end,

%-----------------------------------------------------------------------
%  Read in total cloud cover (%).
%-----------------------------------------------------------------------

cid=fopen(c_file,'r');
if (cid > 0),
  c=fread(cid,[im,jm],'float');
  c=c*100.0;
  c=min(100.0,c);
  fclose(cid);
  got=got+1;
else,
  c=zeros([im jm]);
end,

return
