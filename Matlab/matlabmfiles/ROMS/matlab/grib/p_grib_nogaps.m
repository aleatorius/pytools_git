function [h]=p_grib_nogaps(time,tau,u,v,p,t,d,rh,q,c,cstlon,cstlat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [h]=plot_nogaps(Julday,Ftime,u,v,p,t,rh,q,cstlon,cstlat)    %
%                                                                      %
% This function plots NOGAPS data.                                     %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    time      time of data to write (structure array):                %
%                time.Jday  => Julian day.                             %
%                time.year  => year of the century.                    %
%                time.month => month of the year (1-12).               %
%                time.day   => day of the month.                       %
%                time.hour  => hour of the day (GMT, 1-24).            %
%     tau       Forecast time (hours).                                 %
%     u         East-West wind component (m/s) at 10m.                 %
%     v         North-West wind component (m/s) at 10m.                %
%     p         Surface pressure (Pa) at MSL.                          %
%     t         Air temperature (C) at 2m.                             %
%     d         Dew-point temperature (C) at 2m.                       %
%     rh        Relative humidity (%).                                 %
%     q         Accumulated precipitation (mm/12 hour).                %
%     c         Total cloud cover (%).                                 %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     h         figure handle.                                         %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Set switch to draw coastlines.

ICOAST=1;
if (nargin < 9),
  ICOAST=0;
end,

%-----------------------------------------------------------------------
%  Set date stamp.
%-----------------------------------------------------------------------

wkday=['Sunday   ';'Monday   ';'Tuesday  ';'Wednesday';'Thursday ';...
       'Friday   ';'Saturday '];
lwkday=[6,6,7,9,8,6,8];
Month=['January  ';'February ';'March    ';'April    ';'May      ';...
       'June     ';'July     ';'August   ';'September';'October  ';...
       'November ';'December '];
lmonth=[7,8,5,5,3,4,4,6,9,7,8,8];
ampm=[' AM';' PM'];

%  Convert Julian day to Gregorian day.

gtime=gregorian(time.Jday+2440000);

year  =gtime(1);
month =gtime(2);
day   =gtime(3);
hour  =gtime(4);
minute=gtime(5);
second=gtime(6);
iwday=day_code(month,day,year);

%  Set date stamp.

stamp1=['Issued: ',wkday(iwday,1:lwkday(iwday)), ...
        ' ', Month(month,1:lmonth(month)), ...
        ' ',num2str(day),', ',num2str(year), ...
        ' - ',num2str(hour),' GMT.'];

gtime=gregorian(time.Jday+2440000+tau/24.0);

year  =gtime(1);
month =gtime(2);
day   =gtime(3);
hour  =gtime(4);
minute=gtime(5);
second=gtime(6);
iwday=day_code(month,day,year);
stamp2=['NOGAPS, Valid: ',wkday(iwday,1:lwkday(iwday)), ...
        ' ',Month(month,1:lmonth(month)), ...
        ' ',num2str(day),', ',num2str(hour),' GMT.'];

%-----------------------------------------------------------------------
%  Set NOGAPS grid.
%-----------------------------------------------------------------------

[im,jm]=size(p);

lon_min=-180.0;
lon_max=180;
del_lon=(lon_max-lon_min)/(im);

lat_min=-90;
lat_max=90;
del_lat=(lat_max-lat_min)/(jm-1);

lonE=0:del_lon:lon_max;
lonW=lon_min+1:del_lon:-1;
lon=[lonE lonW]; lon=lon';

lat=lat_min:del_lat:lat_max;

Lon=repmat(lon,[1 jm]);
Lat=repmat(lat,[im 1]);

%-----------------------------------------------------------------------
%  Set range of data to plot.
%-----------------------------------------------------------------------

LonMin=-76.0;
LonMax=-71.0;

LatMin=36.0;
LatMax=42.0;

LonRange=find((lon >= LonMin) & (lon <= LonMax));
LatRange=find((lat >= LatMin) & (lat <= LatMax));

X=Lon(LonRange,LatRange);
Y=Lat(LonRange,LatRange);

%-----------------------------------------------------------------------
%  Plot surface pressure and wind stress.
%-----------------------------------------------------------------------

m_proj('Mercator');
hold off;

subplot(2,2,1);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

plev=990:0.5:1040;
plab=990:2:1040;
F=p(LonRange,LatRange);

Ptext=[];
if (~isnan(F)),
  [C,h]=contourf(X,Y,F,20);
  set(h,'edgecolor','k');
  colormap('jet');
  colorbar;
% H=clabel(C,h,plab);
  Ptext='Surface Pressure (mb)';
end,
hold on;
grid on;

Fx=u(LonRange,LatRange);
Fy=v(LonRange,LatRange);
F=sqrt(Fx.*Fx + Fy.*Fy);

Ttext=[];
RangeLab=[];
if (~isnan(F)),
  quiver(X,Y,Fx,Fy,'w');
  set(h,'edgecolor','k');
  TauMin=min(min(F));
  TauMax=max(max(F));
  RangeLab=[' Min= ',num2str(TauMin),', Max= ',num2str(TauMax)];
  if (isempty(Ptext)),
    Ttext='Wind Stress (Pa) at 10m';
  else,
    Ttext=' and Wind Stress (Pa) at 10m';
  end,
end,

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

title(stamp1);
if (~isempty(RangeLab)),
  xlabel({[Ptext Ttext],[RangeLab]});
else,
  xlabel([Ptext Ttext]);
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

%-----------------------------------------------------------------------
%  Plot air temperature.
%-----------------------------------------------------------------------

subplot(2,2,2);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

tlev=0:1:40;
tlab=0:1:40;
F=t(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [C,h]=contourf(X,Y,F,20);
  set(h,'edgecolor','k');
  colormap('jet');
  colorbar;
% H=clabel(C,h,tlab);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

title(stamp2);
if (~isempty(RangeLab)),
  xlabel({'Temperature at 2m',[RangeLab]});
else,
  xlabel('Temperature at 2m');
end,
ylabel('Latitude');

set(gca,'nextplot','replace');

%-----------------------------------------------------------------------
%  Plot precipitation.
%-----------------------------------------------------------------------

subplot(2,2,3);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

rlev=0:0.25:100;
rlab=0:1.0:100;
F=r(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [C,h]=contour(X,Y,F,20);
% H=clabel(C,h,rlab);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

if (~isempty(RangeLab)),
  xlabel({'12-hour precipitation (mm)',[RangeLab]});
else,
  xlabel('12-hour precipitation (mm)');
end,
ylabel('Latitude');

set(gca,'nextplot','replace');

%-----------------------------------------------------------------------
%  Plot relative humidity (%).
%-----------------------------------------------------------------------

subplot(2,2,4);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

clev=10:10:100;
clab=10:10:100;
F=rh(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [C,h]=contourf(X,Y,F,20);
  set(h,'edgecolor','k');
  colormap('jet');
  colorbar;
% H=clabel(C,h,rhlab);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

if (~isempty(RangeLab)),
  xlabel({'Relative Humidity (%)',[RangeLab]});
else,
  xlabel('Relative Humidity (%)');
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

return
