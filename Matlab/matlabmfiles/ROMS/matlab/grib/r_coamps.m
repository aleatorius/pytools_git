function [u,v,t,p,rh,q,got]=r_coamps(dir,im,jm,time,tau);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [u,v,t,p,rh,q,got]=r_coamps(dir,im,jm,time,tau)             %
%                                                                      %
% This function reads in COAMPS requested GRIB files.                  %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    im        Number of colums to read (integer).                     %
%    jm        Number of rows to read (integer).                       %
%    dir       Flat GRIB file directory (string).                      %
%    time      time of data to write (structure array):                %
%                time.Jday  => Julian day.                             %
%                time.year  => year of the century.                    %
%                time.month => month of the year (1-12).               %
%                time.day   => day of the month.                       %
%                time.hour  => hour of the day (GMT, 1-24).            %
%    tau       Forecast length in hours (integer).                     %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    u         East-West wind component (m/s).                         %
%    v         North-West wind component (m/s).                        %
%    t         Air temperature at 10m (Celsius).                       %
%    s         Surface pressure (Pa).                                  %
%    rh        Relative humidity (%).                                  %
%    q         Precipitation (cm/hour).                                %
%    got       Counter of fields found.                                %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Build GRIB flat files names.
%-----------------------------------------------------------------------

if (time.year > 1999),
  syy=num2str(time.year-2000,'%2.2i');
else,
  syy=num2str(time.year-1900,'%2.2i');
end,

smm =num2str(time.month,'%2.2i');
sdd =num2str(time.day,'%2.2i');
shh =num2str(time.hour,'%2.2i');	
stau=num2str(tau,'%3.3i');

u_file=[dir 'FF' syy smm sdd shh stau '033.187.105.00010'];
v_file=[dir 'FF' syy smm sdd shh stau '034.187.105.00010'];
t_file=[dir 'FF' syy smm sdd shh stau '011.187.105.00002'];
p_file=[dir 'FF' syy smm sdd shh stau '002.187.102.00000'];
r_file=[dir 'FF' syy smm sdd shh stau '052.187.100.01000'];
q_file=[dir 'FF' syy smm sdd shh stau '061.187.001.00000'];

got=0;

%-----------------------------------------------------------------------
%  Read in wind components (m/s) at 10 m.
%-----------------------------------------------------------------------

uid=fopen(u_file,'r');
if (uid > 0),
  u=fread(uid,[im,jm],'float');
  fclose(uid);
  got=got+1;
else,
  disp(['Not found Uvel: ', u_file]);
  u=NaN.*(ones([im jm]));
end,
clear uid

vid=fopen(v_file,'r');
if (vid > 0),
  v=fread(vid,[im,jm],'float');
  fclose(vid);
  got=got+1;
else,
  disp(['Not found Vvel: ', v_file]);
  v=NaN.*(ones([im jm]));
end,
clear vid

%-----------------------------------------------------------------------
%  Read in surface air temperature (degrees Kelvin).
%-----------------------------------------------------------------------

tid=fopen(t_file,'r');
if (tid > 0),
  t=fread(tid,[im,jm],'float');
  t=t-273.16;
  fclose(tid);
else,
  disp(['Not found Tair: ', t_file]);
  t=NaN.*(ones([im jm]));
end,
clear tid

%-----------------------------------------------------------------------
%  Read in surface pressure (hPa).
%-----------------------------------------------------------------------

pid=fopen(p_file,'r');
if (pid > 0),
  p=fread(pid,[im,jm],'float');
  p=p./100.0;
  fclose(pid);
  got=got+1;
else,
  disp(['Not found Pair: ', p_file]);
  p=NaN.*(ones([im jm]));
end,
clear pid

%-----------------------------------------------------------------------
%  Read in relative humidity (%).
%-----------------------------------------------------------------------

hid=fopen(r_file,'r');
if (hid > 0),
  rh=fread(hid,[im,jm],'float');
  rh=rh*100.0;
  rh=min(100.0,rh);  
  fclose(hid);
  got=got+1;
else,
  disp(['Not found rhum: ', r_file]);
  rh=NaN.*(ones([im jm]));
end,
clear hid

%-----------------------------------------------------------------------
%  Read in precipitation (cm/hour).
%-----------------------------------------------------------------------

qid=fopen(q_file,'r');
if (qid > 0),
  q=fread(qid,[im,jm],'float');
  fclose(qid);
  got=got+1;
else,
  disp(['Not found rain: ', q_file]);
  q=zeros([im jm]);
end,
clear qid

return
