function [status]=w_nogaps(dir,time,define,Vname,fname,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=w_nogaps(dir,time,define,Vname,fname,tindex)       %
%                                                                      %
% This reads in NOGAPS GRIB fields and writes them into specified      %
% NetCDF file at the stipulated time record.                           %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    dir       Flat GRIB file directory (string).                      %
%    time      time of data to write (structure array):                %
%                 time.Jday  => Julian day.                            %
%                 time.year  => year of the century.                   %
%                 time.month => month of the year (1-12).              %
%                 time.day   => day of the month.                      %
%                 time.hour  => hour of the day (GMT, 1-24).           %
%    define      Switches indicating which variables to defined        %
%                  (0: no, 1: yes), structure array.                   %
%    Vname       Name of defined forcing NetCDF variables.             %
%    fname     NetCDF file name (string).                              %
%    tindex    NetCDF time record to write (integer, optional).        %
%                                                                      %
%                Variables in structure arrays:                        %
%                                                                      %
%                  ***.wind   => surface wind components.              %
%                  ***.suw    => surface U-wind component.             %
%                  ***.svw    => surface U-wind component.             %
%                  ***.pair   => surface air pressure.                 %
%                  ***.tair   => surface air temperature.              %
%                  ***.rain   => rain fall rate.                       %
%                  ***.cloud  => total cloud cover.                    %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status    Error flag (integer).                                   %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_dim, read_grib, nc_write                                 %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Set NOGAPS dimension parameters.
%-----------------------------------------------------------------------

Im=360;
Jm=181;
Nm=14;

lon_min=-180.0;
lon_max=180;
del_lon=(lon_max-lon_min)/(Im);

lat_min=-90;
lat_max=90;
del_lat=(lat_max-lat_min)/(Jm-1);

lonE=0:del_lon:lon_max;
lonW=lon_min+1:del_lon:-1;
lon=[lonE lonW]; lon=lon';
lat=lat_min:del_lat:lat_max;

delta=[0 6 12 18 24 30 36 42 48 60 72 84 96 120];

spval=-999.0;
got=0;

%  Set indices to extract.

LonMin=-124.0;
LonMax=-52.0;
LatMin=5.0;
LatMax=51.0;

LonRange=find((lon >= LonMin) & (lon <= LonMax));
LatRange=find((lat >= LatMin) & (lat <= LatMax));

%-----------------------------------------------------------------------
%  Read in GRIB flat files.
%-----------------------------------------------------------------------

for fcast=1:Nm

  tau=delta(fcast);

  [u,v,p,t,d,rh,q,c,got]=r_nogaps(dir,Im,Jm,time,tau);

  ind=find(isnan(u));
  if (~isempty(ind)), u(ind)=spval; end,
  ind=find(isnan(v));
  if (~isempty(ind)), v(ind)=spval; end,
  ind=find(isnan(t));
  if (~isempty(ind)), t(ind)=spval; end,
  ind=find(isnan(p));
  if (~isempty(ind)), p(ind)=spval; end,
  ind=find(isnan(d));
  if (~isempty(ind)), d(ind)=spval; end,
  ind=find(isnan(rh));
  if (~isempty(ind)), rh(ind)=spval; end,
  ind=find(isnan(q));
  if (~isempty(ind)), q(ind)=spval; end,
  ind=find(isnan(c));
  if (~isempty(ind)), c(ind)=spval; end,

  U(:,:,fcast)=u(LonRange,LatRange);
  V(:,:,fcast)=v(LonRange,LatRange);
  T(:,:,fcast)=t(LonRange,LatRange);
  P(:,:,fcast)=p(LonRange,LatRange);
  D(:,:,fcast)=d(LonRange,LatRange);
  R(:,:,fcast)=rh(LonRange,LatRange);
  Q(:,:,fcast)=q(LonRange,LatRange);
  C(:,:,fcast)=c(LonRange,LatRange);

end,

%-----------------------------------------------------------------------
%  If time record is not provided, inquire NetCDF file for index to
%  append.
%-----------------------------------------------------------------------

if (nargin < 7),
 
  [dnames,dsizes]=nc_dim(fname);
  ndims=length(dsizes);

  for n=1:ndims,
    name=dnames(n,:);
    if (name(1:4) == 'time'),
      tindex=dsizes(n)+1;
    end,
  end,

end,

%-----------------------------------------------------------------------
%  Write out fields into NetCDF file.
%-----------------------------------------------------------------------

if (got),

  status=nc_write(fname,Vname.time,time.Jday,tindex);

  if (define.wind)
    status=nc_write(fname,Vname.suw,U,tindex);
    status=nc_write(fname,Vname.svw,V,tindex);
  end,

  if (define.tair),
    status=nc_write(fname,Vname.tair,T,tindex);
  end,

  if (define.pair),
    status=nc_write(fname,Vname.pair,P,tindex);
  end,

  if (define.tdew),
    status=nc_write(fname,Vname.tdew,D,tindex);
  end,

  if (define.qair),
    status=nc_write(fname,Vname.qair,R,tindex);
  end,

  if (define.rain),
    status=nc_write(fname,Vname.rain,Q,tindex);
  end,

  if (define.cloud),
    status=nc_write(fname,Vname.cloud,C,tindex);
  end,

end,

return

