function [status]=c_nogaps(fname,define,Vname)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=c_nogaps(fname,define,Vname)                       %
%                                                                      %
% This function creates a NetCDF to store NOGAPS data.                 %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    fname       NetCDF file name (string).                            %
%    define      Switches indicating which variables to define         %
%                  (0: no, 1: yes), structure array.                   %
%    Vname       Name of defined forcing NetCDF variables.             %
%                                                                      %
%                Variables in structure arrays:                        %
%                                                                      %
%                  ***.time   => time.                                 %
%                  ***.wind   => surface wind components.              %
%                  ***.suw    => surface U-wind component.             %
%                  ***.svw    => surface U-wind component.             %
%                  ***.pair   => surface air pressure.                 %
%                  ***.tair   => surface air temperature.              %
%                  ***.tdew   => surface dew point temperature.        %
%                  ***.rain   => rain fall rate.                       %
%                  ***.cloud  => cloud fraction.                       %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_dim, nc_read, nc_write                                   %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Set NOGAPS dimension parameters.  A new grid is extracted from the
%  global grid.

Im=73;
Jm=47;
Nm=14;

LonMin=-124.0;
LonMax=-52.0;
LatMin=5.0;
LatMax=51.0;

dx=1.0;
dy=1.0;

spval=-999.0;

Vname.rlon='lon';
Vname.rlat='lat';

%-----------------------------------------------------------------------
%  Get some NetCDF parameters.
%-----------------------------------------------------------------------

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');
[ncclob]=mexcdf('parameter','nc_clobber');

% Set floating-point variables type.

vartyp=ncfloat;

%-----------------------------------------------------------------------
%  Create output NetCDF file.
%-----------------------------------------------------------------------

[ncid,status]=mexcdf('nccreate',fname,'nc_clobber');
if (ncid == -1),
  error(['C_NOGAPS: ncopen - unable to create file: ', fname]);
  return
end,

%-----------------------------------------------------------------------
%  Define output file dimensions.
%-----------------------------------------------------------------------

[did.rlon]=mexcdf('ncdimdef',ncid,'lon',Im);
if (did.rlon == -1),
  error(['C_NOGAPS: ncdimdef - unable to define dimension: lon.']);
end,

[did.rlat]=mexcdf('ncdimdef',ncid,'lat',Jm);
if (did.rlat == -1),
  error(['C_NOGAPS: ncdimdef - unable to define dimension: lat.']);
end,

[did.forecast]=mexcdf('ncdimdef',ncid,'forecast',Nm);
if (did.forecast == -1),
  error(['C_NOGAPS: ncdimdef - unable to define dimension: forecast.']);
end,

[did.time]=mexcdf('ncdimdef',ncid,'time',ncunlim);
if (did.time == -1),
  error(['C_NOGAPS: ncdimdef - unable to define dimension: time.']);
end,

%----------------------------------------------------------------------------
%  Create global attribute(s).
%----------------------------------------------------------------------------

type='NOGAPS file';

lenstr=max(size(type));
[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lenstr,type);
if (status == -1),
  error(['C_NOGAPS: ncattput - unable to global attribure: history.']);
  return
end

history=date_stamp;

lenstr=max(size(history));
[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lenstr,history);
if (status == -1),
  error(['C_NOGAPS: ncattput - unable to global attribure: history.']);
  return
end

%-----------------------------------------------------------------------
% Define NetCDF variables.
%-----------------------------------------------------------------------

% Define spherical switch.

Var.name ='spherical';
Var.type =ncchar;
Var.dimid=[];
Var.long ='grid type logical switch';
Var.opt_T='spherical';
Var.opt_F='Cartesian';

[varid,status]=nc_vdef(ncid,Var);
clear Var

% Define forecast intervals.

Var.name ='forecast';
Var.type =ncdouble;
Var.dimid=[did.forecast];
Var.long ='forecast period';
Var.units ='day';

[varid,status]=nc_vdef(ncid,Var);
clear Var

%  Define time.

Var.name =Vname.time;
Var.type =ncdouble;
Var.dimid=[did.time];
Var.long ='forecast time';
Var.units='modified Julian day';
Var.field='time, scalar, series';

[varid,status]=nc_vdef(ncid,Var);
clear Var

%  Define longitude positions at RHO-points.

Var.name =Vname.rlon;
Var.type =vartyp;
Var.dimid=[did.rlat did.rlon];
Var.long ='longitude of RHO-points';
Var.units='degree_east';

[varid,status]=nc_vdef(ncid,Var);
clear Var

%  Define longitude positions at RHO-points.

Var.name =Vname.rlat;
Var.type =vartyp;
Var.dimid=[did.rlat did.rlon];
Var.long ='latitude of RHO-points';
Var.units='degree_north';

[varid,status]=nc_vdef(ncid,Var);
clear Var

%  Surface wind components.

if (define.wind),
  Var.name =Vname.suw;
  Var.type =vartyp;
  Var.dimid=[did.time did.forecast did.rlat did.rlon];
  Var.long ='zonal, surface wind component';
  Var.units='meter second-1';
  Var.field='U-wind, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var

  Var.name =Vname.svw;
  Var.type =vartyp;
  Var.dimid=[did.time did.forecast did.rlat did.rlon];
  Var.long ='meridional, surface wind component';
  Var.units='meter second-1';
  Var.field='V-wind, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air temperature.

if (define.tair),
  Var.name =Vname.tair;
  Var.type =vartyp;
  Var.dimid=[did.time did.forecast did.rlat did.rlon];
  Var.long ='surface air temperature';
  Var.units='Celsius';
  Var.field='Tair, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

if (define.qair),
  Var.name =Vname.qair;
  Var.type =vartyp;
  Var.dimid=[did.time did.forecast did.rlat did.rlon];
  Var.long ='surface air relative humidity';
  Var.units='percent';
  Var.min=0.0;
  Var.max=100.0;
  Var.field='humidity, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air temperature.

if (define.tdew),
  Var.name =Vname.tdew;
  Var.type =vartyp;
  Var.dimid=[did.time did.forecast did.rlat did.rlon];
  Var.long ='dew-point temperature';
  Var.units='Celsius';
  Var.field='Tdew, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air pressure.

if (define.pair),
  Var.name =Vname.pair;
  Var.type =vartyp;
  Var.dimid=[did.time did.forecast did.rlat did.rlon];
  Var.long ='surface air pressure';
  Var.units='milibar';
  Var.field='Pair, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Rain fall rate.

if (define.rain),
  Var.name =Vname.rain;
  Var.type =vartyp;
  Var.dimid=[did.time did.forecast did.rlat did.rlon];
  Var.long ='accumulated precipitation';
  Var.units='milimeter/12 hour';
  Var.field='rain, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Cloud fraction.

if (define.cloud),
  Var.name =Vname.cloud;
  Var.type =vartyp;
  Var.dimid=[did.time did.forecast did.rlat did.rlon];
  Var.long ='total cloud cover';
  Var.units='percentage';
  Var.field='cloud, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%-----------------------------------------------------------------------
%  Leave definition mode.
%-----------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['C_NOGAPS: ncendef - unable to leave definition mode.'])
end

%-----------------------------------------------------------------------
%  Close NetCDF file.
%-----------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['C_NOGAPS: ncclose - unable to close NetCDF file: ', fname])
end

%=======================================================================
%  Write out positions.
%=======================================================================

% Write out spherical switch.

[status]=nc_write(fname,'spherical','T');


% Write out forecast periods.

tau=[0 6 12 18 24 30 36 42 48 60 72 84 96 120];
tau=tau./24.0;
[status]=nc_write(fname,'forecast',tau);

% Write out longitude positions.

lon=LonMin:dx:LonMax; lon=lon';
Lon=repmat(lon,[1 Jm]);
[status]=nc_write(fname,'lon',Lon);
if (status ~= 0),
  error(['C_NOGAPS: nc_write - error while writing lon.'])
end

% Write out latitude positions.

lat=LatMin:dx:LatMax;
Lat=repmat(lat,[Im 1]);
[status]=nc_write(fname,'lat',Lat);
if (status ~= 0),
  error(['C_NOGAPS: nc_write - error while writing lat.'])
end

return
