function [h]=f_ncnoraps(fname,tau,tindex,cstlon,cstlat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [h]=f_ncnoraps(fname,tau,tindex,cstlon,cstlat);            %
%                                                                      %
%  This function plots NORAPS NetCDF data using color fill.            %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     fname     NORAPS NetCDF file name (string).                      %
%     tau       Forecast to extract (integer):                         %
%                 tau=1,  analysis                                     %
%                 tau=2,  6-hour forcast                               %
%                 tau=3,  12-hour forcast                              %
%                 tau=4,  18-hour forcast                              %
%                 tau=5,  24-hour forcast                              %
%                 tau=6,  30-hour forcast                              %
%                 tau=7,  36-hour forcast                              %
%                 tau=8,  42-hour forcast                              %
%                 tau=9,  48-hour forcast                              %
%     tindex    NetCDF time index to read.                             %
%     cstlon    Coastline longitudes, if any.                          %
%     cstlat    Coastline latitudes, if any.                           %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     h         figure handle.                                         %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Turn off printing information from "nc_read".

global IPRINT

IPRINT=0;

%  Set switch to draw coastlines.

ICOAST=1;
if (nargin < 4),
  ICOAST=0;
end,

%  Check forecast and time indices.

[dnames,dsizes]=nc_dim(fname);

ndims=length(dsizes);
for n=1:ndims,
  name=dnames(n,:);
  if (name(1:3) == 'lon'),
    Im=dsizes(n);
  elseif (name(1:3) == 'lat'),
    Jm=dsizes(n);
  elseif (name(1:8) == 'forecast'),
    Nm=dsizes(n);
  elseif (name(1:4) == 'time'),
    Nrec=dsizes(n);
  end,
end,

if ( tau < 1 | tau > Nm),
  error(['P_NORAPS: illegal forecast index, TAU range: 1 - ',...
        num2str(Nm)]);
end,

if ( tindex < 1 | tau > Nrec),
  error(['P_NORAPS: illegal forecast index, TINDEX range: 1 - ',...
        num2str(Nrec)]);
end,

%-----------------------------------------------------------------------
%  Set date stamp.
%-----------------------------------------------------------------------

wkday=['Sunday   ';'Monday   ';'Tuesday  ';'Wednesday';'Thursday ';...
       'Friday   ';'Saturday '];
lwkday=[6,6,7,9,8,6,8];
Month=['January  ';'February ';'March    ';'April    ';'May      ';...
       'June     ';'July     ';'August   ';'September';'October  ';...
       'November ';'December '];
lmonth=[7,8,5,5,3,4,4,6,9,7,8,8];
ampm=[' AM';' PM'];

%  Read in time and then convert Julian day to Gregorian day.

Julday=nc_read(fname,'time',tindex);

gtime=gregorian(Julday+2440000.0);

year  =gtime(1);
month =gtime(2);
day   =gtime(3);
hour  =gtime(4);
minute=gtime(5);
second=gtime(6);
iwday=day_code(month,day,year);

Ftime=6*(tau-1);
gtime=gregorian(Julday+Ftime/24.0+2440000.0);

year2  =gtime(1);
month2 =gtime(2);
day2   =gtime(3);
hour2  =gtime(4);
minute2=gtime(5);
second2=gtime(6);
iwday2=day_code(month2,day2,year2);

%  Set date stamp.

stamp1=['NORAPS Forecast for ',wkday(iwday2,1:lwkday(iwday2)), ...
        ' - ',Month(month2,1:lmonth(month2)),' ',num2str(day2), ...
        ', ',num2str(year2),', ',num2str(hour2),' GMT.'];

stamp2=['Issued ',wkday(iwday,1:lwkday(iwday)),', ', ...
        Month(month,1:lmonth(month)),' ',num2str(day),', ', ...
        num2str(hour),' GMT.'];

%-----------------------------------------------------------------------
%  Read in NORAPS grid.
%-----------------------------------------------------------------------

Lon=nc_read(fname,'lon');
Lat=nc_read(fname,'lat');

%-----------------------------------------------------------------------
%  Read in and extract requested NORAPS data.
%-----------------------------------------------------------------------

spval=-999;

F=nc_read(fname,'Pair',tindex);
p=F(:,:,tau);
ind=find(p == spval);
if (~isempty(ind)), p(ind)=NaN; end,

F=nc_read(fname,'uwind',tindex);
u=F(:,:,tau);
ind=find(u == spval);
if (~isempty(ind)), u(ind)=NaN; end,

F=nc_read(fname,'vwind',tindex);
v=F(:,:,tau);
ind=find(v == spval);
if (~isempty(ind)), v(ind)=NaN; end,

F=nc_read(fname,'Tair',tindex);
t=F(:,:,tau);
ind=find(t == spval);
if (~isempty(ind)), t(ind)=NaN; end,

F=nc_read(fname,'humidity',tindex);
rh=F(:,:,tau);
ind=find(rh == spval);
if (~isempty(ind)), rh(ind)=NaN; end,

F=nc_read(fname,'precip',tindex);
q=F(:,:,tau);
ind=find(q == spval);
if (~isempty(ind)), q(ind)=NaN; end,

%-----------------------------------------------------------------------
%  Set range of data to plot.
%-----------------------------------------------------------------------

LonMin=-76.5;
LonMax=-70.5;

LatMin=36.0;
LatMax=42.0;

lon=Lon(:,1);
lat=Lat(1,:);

LonRange=find((lon >= LonMin) & (lon <= LonMax));
LatRange=find((lat >= LatMin) & (lat <= LatMax));

X=Lon(LonRange,LatRange);
Y=Lat(LonRange,LatRange);

%-----------------------------------------------------------------------
%  Plot surface pressure and winds.
%-----------------------------------------------------------------------

m_proj('Mercator');
hold off;

subplot(2,2,1);

plev=990:0.5:1040;
plab=990:2:1040;
F=p(LonRange,LatRange);

Ptext=[];
if (~isnan(F)),
 [c,h]=contourf(X,Y,F,20);
 set(h,'edgecolor','k');
 colormap('default');
 colorbar;
% H=clabel(c,h,plab);
 Ptext='Surface Pressure (mb)';
end,
hold on;
grid on;

Fx=u(LonRange,LatRange);
Fy=v(LonRange,LatRange);
F=sqrt(Fx.*Fx + Fy.*Fy);

Ttext=[];
RangeLab=[];
if (~isnan(F)),
  quiver(X,Y,Fx,Fy,'w');
  set(h,'edgecolor','k');
  TauMin=min(min(F));
  TauMax=max(max(F));
  RangeLab=[' Min= ',num2str(TauMin),', Max= ',num2str(TauMax)];
  if (isempty(Ptext)),
    Ttext='Winds (m/s) at 10m';
  else,
    Ttext=' and Wind Speed (m/s) at 10m';
  end,
end,

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

title(stamp1);
if (~isempty(RangeLab)),
  xlabel({[Ptext Ttext],[RangeLab]});
else,
  xlabel([Ptext Ttext]);
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

%-----------------------------------------------------------------------
%  Plot air temperature.
%-----------------------------------------------------------------------

subplot(2,2,2);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');
set(gca,'xtick',[LonMin:1:LonMax],'ytick',[LatMin:1:LatMax]);

tlev=0:1:40;
tlab=0:1:40;
F=t(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [c,h]=contourf(X,Y,F,20);
  set(h,'edgecolor','k');
  colormap('default');
  colorbar;
%  H=clabel(c,h,tlab);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

title(stamp2);
if (~isempty(RangeLab)),
  xlabel({'Temperature at 2m (C)',[RangeLab]});
else,
  xlabel('Temperature at 2m (C)');
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')


%-----------------------------------------------------------------------
%  Plot relative humidity.
%-----------------------------------------------------------------------

subplot(2,2,3);

rhlev=10:10:100;
rhlab=10:10:100;
F=rh(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [c,h]=contourf(X,Y,F,20);
  set(h,'edgecolor','k');
  colormap('default');
  colorbar;
% H=clabel(c,h,rhlab);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

if (~isempty(RangeLab)),
  xlabel({'Relative Humidity (%)',[RangeLab]});
else,
  xlabel('Relative Humidity (%)');
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

%-----------------------------------------------------------------------
%  Plot precipitation.
%-----------------------------------------------------------------------

subplot(2,2,4);

qlev=0:0.02:10;
qlab=0:0.5:5;
F=q(LonRange,LatRange);

RangeLab=[];
if (~isnan(F)),
  [c,h]=contour(X,Y,F,20);
  RangeLab=[' Min= ',num2str(min(min(F))),', Max= ',num2str(max(max(F)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

if (~isempty(RangeLab)),
  xlabel({'12-hour precipitation (cm)',[RangeLab]});
else,
  xlabel('12-hour precipitation (cm)');
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

return
