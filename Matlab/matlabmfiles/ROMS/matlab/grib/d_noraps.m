%  This script plots NORAPS data from flat GRIB file or write data
%  into an existing NORAPS NetCDF file.   Use script "c_noraps" to
%  create a new NetCDF file.

IPLOT=1;
IWRITE=0;

dir='/nopp/MEL/Jul98/';
Oname='noraps_jul98.nc';

year=1998;
month=7;
day_str=12;
day_end=12;
tau=0;

im=145;
jm=93;

%-----------------------------------------------------------------------
%  Plot NORAPS data from flat GRIB files.
%-----------------------------------------------------------------------

if ( IPLOT );

%  Read in coastline data.

  cst_file=['/d1/arango/scrum3.0/plot/Data/us_cst.dat'];
  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=900);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  clear icst;

%  Read in and plot NORAPS data.  Create one postscript file for
%  each time record.

  for day=day_str:day_end,
    for hour=12:12:12,
      for tau=0:6:48;
        Julday=julian(year,month,day,hour)-2440000;
        [u,v,t,p,rh,q]=r_noraps(dir,im,jm,year,month,day,hour,tau);
        figure; clf;
        [h]=fill_noraps(Julday,tau,u,v,p,t,rh,q,cstlon,cstlat); 
        hold off;
        eval(['print -djpeg90 noraps_',num2str(month,'%2.2i'),...
        num2str(day,'%2.2i'),num2str(hour,'%2.2i'),...
        num2str(tau,'%2.2i'),'.jpg']);	
      end,
    end,
  end,
end,

%-----------------------------------------------------------------------
%  Write out NORAPS data into NetCDF file.
%-----------------------------------------------------------------------

if ( IWRITE );

  for day=day_str:day_end,
    for hour=0:12:12,
      tindex=(day*2)+(hour/12)-1;
      status=w_noraps(dir,year,month,day,hour,Oname,tindex);
    end,
  end,

end,
