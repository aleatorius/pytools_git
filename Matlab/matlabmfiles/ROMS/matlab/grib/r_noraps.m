function [u,v,t,p,rh,q]=r_noraps(dir,im,jm,year,month,day,hour,tau);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [u,v,t,p,rh,q]=r_noraps(dir,im,jm,year,month,day,hour,tau)  %
%                                                                      %
%  This function reads in NORAPS requested GRIB files.                 %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     im        Number of colums to read (integer).                    %
%     jm        Number of rows to read (integer).                      %
%     dir       Flat GRIB file directory (string).                     %
%     year      Year to process (integer).                             %
%     month     Month to process (integer).                            %
%     day       Day to process (integer).                              %
%     hour      Hour to process (integer).                             %
%     tau       Forecast length in hours (integer).                    %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     u         East-West wind component (m/s).                        %
%     v         North-West wind component (m/s).                       %
%     t         Air temperature at 10m (Celsius).                      %
%     s         Surface pressure (mb).                                 %
%     rh        Relative humidity (%).                                 %
%     q         precipitation (mm).                                    %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Build GRIB flat files names.
%-----------------------------------------------------------------------

if (year < 100),
  syy=int2str(year);
else,
  syy=int2str(year-1900);
end,

smm=int2str(month);
if (month < 10),
  smm=['0' smm];
end,

sdd=int2str(day);
if (day < 10),
  sdd=['0' sdd];
end,

shh=int2str(hour);
if (hour < 10),
  shh=['0' shh];
end,

stau=int2str(tau);
if (tau < 10),
  stau=['00' stau];
elseif (tau < 100),
  stau=['0' stau];
end,

u_file=[dir 'FF' syy smm sdd shh stau '033.245.105.00010'];
v_file=[dir 'FF' syy smm sdd shh stau '034.245.105.00010'];
t_file=[dir 'FF' syy smm sdd shh stau '011.245.105.00002'];
p_file=[dir 'FF' syy smm sdd shh stau '002.245.102.00000'];
r_file=[dir 'FF' syy smm sdd shh stau '052.245.100.01000'];
q_file=[dir 'FF' syy smm sdd shh stau '061.245.001.00000'];

%-----------------------------------------------------------------------
%  Read in wind components at 10 m.
%-----------------------------------------------------------------------

fid=fopen(u_file,'r');
if (fid > 0),
  u=fread(fid,[im,jm],'float');
  fclose(fid);
else,
  u=NaN.*(ones([im jm]));
end,

fid=fopen(v_file,'r');
if (fid > 0),
  v=fread(fid,[im,jm],'float');
  fclose(fid);
else,
  v=NaN.*(ones([im jm]));
end,

%-----------------------------------------------------------------------
%  Read in surface air temperature.  Convert to Celsisus.
%-----------------------------------------------------------------------

fid=fopen(t_file,'r');
if (fid > 0),
  t=fread(fid,[im,jm],'float');
  t=t-273.0;
  fclose(fid);
else,
  t=NaN.*(ones([im jm]));
end,

%-----------------------------------------------------------------------
%  Read in surface pressure.
%-----------------------------------------------------------------------

fid=fopen(p_file,'r');
if (fid > 0),
  p=fread(fid,[im,jm],'float');
  p=p./100.0;
  fclose(fid);
else,
  p=NaN.*(ones([im jm]));
end,

%-----------------------------------------------------------------------
%  Read in relative humidity (%).
%-----------------------------------------------------------------------

fid=fopen(r_file,'r');
if (fid > 0),
  rh=fread(fid,[im,jm],'float');
  rh=rh.*100.0;
  fclose(fid);
else,
  rh=NaN.*(ones([im jm]));
end,

%-----------------------------------------------------------------------
%  Read in precipitation.
%-----------------------------------------------------------------------

fid=fopen(q_file,'r');
if (fid > 0),
  q=fread(fid,[im,jm],'float');
  fclose(fid);
else,
  q=NaN.*(ones([im jm]));
end,

return
