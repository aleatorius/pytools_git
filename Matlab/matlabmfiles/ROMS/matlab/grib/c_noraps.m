function [status]=c_noraps(fname)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=c_noraps(fname)                                    %
%                                                                      %
% This function creates a NetCDF to store NORAPS data.                 %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    fname       NetCDF file name (string).                            %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_dim, nc_read, nc_write                                   %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Set NORAPS dimension parameters.

Im=145;
Jm=93;
Nm=9;

LonMin=-124.0;
LonMax=-52.0;
LatMin=5.0;
LatMax=51.0;

dx=0.5;
dy=0.5;

spval=-999.0;

%-----------------------------------------------------------------------
%  Get some NetCDF parameters.
%-----------------------------------------------------------------------

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');
[ncclob]=mexcdf('parameter','nc_clobber');

% Set floating-point variables type.

vartyp=ncfloat;

%-----------------------------------------------------------------------
%  Create output NetCDF file.
%-----------------------------------------------------------------------

[ncid,status]=mexcdf('nccreate',fname,'nc_noclobber');
if (ncid == -1),
  error(['C_NORAPS: ncopen - unable to create file: ', fname]);
  return
end,

%-----------------------------------------------------------------------
%  Define output file dimensions.
%-----------------------------------------------------------------------

[xdim]=mexcdf('ncdimdef',ncid,'lon',Im);
if (xdim == -1),
  error(['C_NORAPS: ncdimdef - unable to define dimension: lon.']);
end,

[ydim]=mexcdf('ncdimdef',ncid,'lat',Jm);
if (ydim == -1),
  error(['C_NORAPS: ncdimdef - unable to define dimension: lat.']);
end,

[fdim]=mexcdf('ncdimdef',ncid,'forecast',Nm);
if (fdim == -1),
  error(['C_NORAPS: ncdimdef - unable to define dimension: forecast.']);
end,

[tdim]=mexcdf('ncdimdef',ncid,'time',ncunlim);
if (tdim == -1),
  error(['C_NORAPS: ncdimdef - unable to define dimension: time.']);
end,

%----------------------------------------------------------------------------
%  Create global attribute(s).
%----------------------------------------------------------------------------

type='NORAPS file';

lenstr=max(size(type));
[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lenstr,type);
if (status == -1),
  error(['C_NORAPS: ncattput - unable to global attribure: history.']);
  return
end

history=date_stamp;

lenstr=max(size(history));
[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lenstr,history);
if (status == -1),
  error(['C_NORAPS: ncattput - unable to global attribure: history.']);
  return
end

%=======================================================================
%  Define NORAPS variables
%=======================================================================

%-----------------------------------------------------------------------
% Define time coordinate.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'time',ncdouble,1,tdim);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: time.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,13,...
                'forecast time');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'time:long_name.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,19,...
                'modified Julian day');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'time:units.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,20,...
                'time, scalar, series');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'time:field.']);
end,

%-----------------------------------------------------------------------
% Define longitude.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'lon',vartyp,2,[ydim xdim]);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: lon.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,9,...
                'longitude');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'lon:long_name.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,12,...
                'degrees east');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'lon:units']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,17,...
                'longitude, scalar');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'lon:field.']);
end,

%-----------------------------------------------------------------------
% Define latitude.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'lat',vartyp,2,[ydim xdim]);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: lat.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,8,...
                'latitude');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'lat:long_name.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,13,...
                'degrees north');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'lat:units']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,16,...
                'latitude, scalar');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'lat:field.']);
end,

%-----------------------------------------------------------------------
% Define zonal, surface wind component.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'uwind',vartyp,4,[tdim fdim ydim xdim]);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: uwind.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,29,...
                'zonal, surface wind component');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'uwind:long_name.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,14,...
                'meter second-1');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'uwind:units']);
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',ncfloat,1,spval);
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'uwind:_FillValue']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,21,...
                'uwind, scalar, series');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'uwind:field.']);
end,

%-----------------------------------------------------------------------
% Define meridional, surface wind component.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'vwind',vartyp,4,[tdim fdim ydim xdim]);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: vwind.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,34,...
                'meridional, surface wind component');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'vwind:long_name.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,14,...
                'meter second-1');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'vwind:units']);
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',ncfloat,1,spval);
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'vwind:_FillValue']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,21,...
                'vwind, scalar, series');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'v_wind:field.']);
end,

%-----------------------------------------------------------------------
% Define surface air temperature.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'Tair',vartyp,4,[tdim fdim ydim xdim]);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: Tair.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,23,...
                'surface air temperature');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'Tair:long_name.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,7,'Celsius');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'Tair:units']);
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',ncfloat,1,spval);
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'Tair:_FillValue']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,20,...
                'Tair, scalar, series');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'Tair:field.']);
end,

%-----------------------------------------------------------------------
% Define surface air pressure.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'Pair',vartyp,4,[tdim fdim ydim xdim]);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: Pair.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,20,...
                'surface air pressure');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'Pair:long_name.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,8,'milibars');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'Pair:units']);
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',ncfloat,1,spval);
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'Pair:_FillValue']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,20,...
                'Pair, scalar, series');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'Pair:field.']);
end,

%-----------------------------------------------------------------------
% Define relative humidity.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'humidity',vartyp,4,[tdim fdim ydim xdim]);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: humidity.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,17,...
                'relative humidity');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'humidity:long_name.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,10,'percentage');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'humidity:units']);
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',ncfloat,1,spval);
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'humidity:_FillValue']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,24,...
                'humidity, scalar, series');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'humidity:field.']);
end,

%-----------------------------------------------------------------------
% Define precipitation.
%-----------------------------------------------------------------------

[varid]=mexcdf('ncvardef',ncid,'precip',vartyp,4,[tdim fdim ydim xdim]);
if (varid == -1),
  error(['C_NORAPS: ncvardef - unable to define variable: precip.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,21,...
                '12-hour precipitation');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'precip:long_name.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,9,'milimeter');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'precip:units']);
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',ncfloat,1,spval);
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'precip:_FillValue']);
end,

[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,22,...
                'precip, scalar, series');
if (status == -1),
  error(['C_NORAPS: ncattput - unable to define attribute: ',...
         'precip:field.']);
end,

%-----------------------------------------------------------------------
%  Leave definition mode.
%-----------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['C_NORAPS: ncendef - unable to leave definition mode.'])
end

%-----------------------------------------------------------------------
%  Close NetCDF file.
%-----------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['C_NORAPS: ncclose - unable to close NetCDF file: ', fname])
end

%=======================================================================
%  Write out positions.
%=======================================================================

lon=LonMin:dx:LonMax; lon=lon';
Lon=repmat(lon,[1 Jm]);
[status]=nc_write(fname,'lon',Lon);
if (status ~= 0),
  error(['C_NORAPS: nc_write - error while writing lon.'])
end

lat=LatMin:dx:LatMax;
Lat=repmat(lat,[Im 1]);
[status]=nc_write(fname,'lat',Lat);
if (status ~= 0),
  error(['C_NORAPS: nc_write - error while writing lat.'])
end

return





