%  This script extracts boundary data from climatology array.

Cname='/n6/wilkin/roms/nena/in/roms_nena_climsta_4.nc';
Bname='/n0/arango/NENA/roms_nena_bry.nc';

%  Read in and write out data.

time=nc_read(Cname,'ocean_time');
Nrec=length(time);

for n=1:Nrec,

  f=nc_read(Cname,'zeta',n);
  [Im,Jm]=size(f);
  f_east=squeeze(f(Im,:));
  f_south=squeeze(f(:,1));
  f_north=squeeze(f(:,Jm));
  status=nc_write(Bname,'zeta_east',f_east,n);
  status=nc_write(Bname,'zeta_south',f_south,n);
  status=nc_write(Bname,'zeta_north',f_north,n);
  clear f f_east f_south f_north
  
  f=nc_read(Cname,'ubar',n);
  [Im,Jm]=size(f);
  f_east=squeeze(f(Im,:));
  f_south=squeeze(f(:,1));
  f_north=squeeze(f(:,Jm));
  status=nc_write(Bname,'ubar_east',f_east,n);
  status=nc_write(Bname,'ubar_south',f_south,n);
  status=nc_write(Bname,'ubar_north',f_north,n);
  clear f f_east f_south f_north

  f=nc_read(Cname,'vbar',n);
  [Im,Jm]=size(f);
  f_east=squeeze(f(Im,:));
  f_south=squeeze(f(:,1));
  f_north=squeeze(f(:,Jm));
  status=nc_write(Bname,'vbar_east',f_east,n);
  status=nc_write(Bname,'vbar_south',f_south,n);
  status=nc_write(Bname,'vbar_north',f_north,n);
  clear f f_east f_south f_north
  
  f=nc_read(Cname,'u',n);
  [Im,Jm,Km]=size(f);
  f_east=squeeze(f(Im,:,:));
  f_south=squeeze(f(:,1,:));
  f_north=squeeze(f(:,Jm,:));
  status=nc_write(Bname,'u_east',f_east,n);
  status=nc_write(Bname,'u_south',f_south,n);
  status=nc_write(Bname,'u_north',f_north,n);
  clear f f_east f_south f_north

  f=nc_read(Cname,'v',n);
  [Im,Jm,Km]=size(f);
  f_east=squeeze(f(Im,:,:));
  f_south=squeeze(f(:,1,:));
  f_north=squeeze(f(:,Jm,:));
  status=nc_write(Bname,'v_east',f_east,n);
  status=nc_write(Bname,'v_south',f_south,n);
  status=nc_write(Bname,'v_north',f_north,n);
  clear f f_east f_south f_north

  f=nc_read(Cname,'temp',n);
  [Im,Jm,Km]=size(f);
  f_east=squeeze(f(Im,:,:));
  f_south=squeeze(f(:,1,:));
  f_north=squeeze(f(:,Jm,:));
  status=nc_write(Bname,'temp_east',f_east,n);
  status=nc_write(Bname,'temp_south',f_south,n);
  status=nc_write(Bname,'temp_north',f_north,n);
  clear f f_east f_south f_north

  f=nc_read(Cname,'salt',n);
  [Im,Jm,Km]=size(f);
  f_east=squeeze(f(Im,:,:));
  f_south=squeeze(f(:,1,:));
  f_north=squeeze(f(:,Jm,:));
  status=nc_write(Bname,'salt_east',f_east,n);
  status=nc_write(Bname,'salt_south',f_south,n);
  status=nc_write(Bname,'salt_north',f_north,n);
  clear f f_east f_south f_north

end,

