function status=c_radials(Im,Jm,Oname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function status=c_radials(Im,Jm,Oname);                                   %
%                                                                           %
% This function creates a  RADIAL CODAR NetCDF.                             %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Im          Number of points in the radial-direction.                  %
%    Jm          Number of points on the angle-direction.                   %
%    Oname       Output NetCDF file name (string).                          %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

spval=999;

%  Supress all error messages from NetCDF.

[ncopts]=mexcdf('setopts',0);

%  Get some NetCDF parameters.

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');
[ncclob]=mexcdf('parameter','nc_clobber');

%  Set global attribute.

type='DATA file';
history=['Data from CODAR, ', date_stamp];

%  Set some parameters.

vartyp=ncfloat;

%  Create NetCDF file.

[ncid,status]=mexcdf('nccreate',Oname,'nc_clobber');
if (ncid == -1),
  error(['C_RADIALS: ncopen - unable to create file: ', Oname])
  return
end

%----------------------------------------------------------------------------
%  Define dimensions.
%----------------------------------------------------------------------------

[rdim]=mexcdf('ncdimdef',ncid,'radial',Im);
if (rdim == -1),
  error(['C_RADIALS: ncdimdef - unable to define dimension: xi_rho.'])
end,

[adim]=mexcdf('ncdimdef',ncid,'angle',Jm);
if (adim == -1),
  error(['C_RADIALS: ncdimdef - unable to define dimension: eta_rho.'])
end,

[tdim]=mexcdf('ncdimdef',ncid,'time',ncunlim);
if (tdim == -1),
  error(['C_RADIALS: ncdimdef - unable to define unlimited dimension: ',...
         ' time.'])
end,

%----------------------------------------------------------------------------
%  Create global attribute(s).
%----------------------------------------------------------------------------

lenstr=max(size(type));
[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lenstr,type);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to global attribure: history.'])
  return
end

lenstr=max(size(history));
[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lenstr,history);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to global attribure: history.'])
  return
end

%----------------------------------------------------------------------------
%  Define variables.
%----------------------------------------------------------------------------

%  Define time variable.

[varid]=mexcdf('ncvardef',ncid,'time',ncdouble,1,[tdim]);
if (varid == -1),
  error(['C_RADIALS: ncvardef - unable to define variable: time.'])
end,

text='surface momentum time';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'time:long_name.'])
end,

text='Julian day';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'time:units.'])
end,

%  Define angles from the CODAR source.

[varid]=mexcdf('ncvardef',ncid,'angle',vartyp,1,[adim]);
if (varid == -1),
  error(['C_RADIALS: ncvardef - unable to define variable: angle.'])
end,

text='angle from CODAR source';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'angle:long_name.'])
end,

text='degrees, clockwise from true North';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'angle:units.'])
end,

%  Define longitude.

[varid]=mexcdf('ncvardef',ncid,'lon',vartyp,2,[adim,rdim]);
if (varid == -1),
  error(['C_RADIALS: ncvardef - unable to define variable: lon.'])
end,

text='longitude';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'lon:long_name.'])
end,

text='degrees-east';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'lon:units.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',vartyp,1,spval);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
        'lon:_FillValue.']);
end,

%  Define latitude.

[varid]=mexcdf('ncvardef',ncid,'lat',vartyp,2,[adim,rdim]);
if (varid == -1),
  error(['C_RADIALS: ncvardef - unable to define variable: lat.'])
end,

text='latitude';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'lat:long_name.'])
end,

text='degrees-north';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'lat:units.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',vartyp,1,spval);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
        'lat:_FillValue.']);
end,

%  Define CODAR raw radial velocity.

[varid]=mexcdf('ncvardef',ncid,'Vraw',vartyp,3,[tdim adim rdim]);
if (varid == -1),
  error(['C_RADIALS: ncvardef - unable to define variable: Vraw.'])
end,

text='CODAR raw radial velocity';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'Vraw:long_name.'])
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'Vraw:units.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',vartyp,1,spval);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
        'Vraw:_FillValue.']);
end,

%  Define tidal radial velocity.

[varid]=mexcdf('ncvardef',ncid,'Vtide',vartyp,3,[tdim adim rdim]);
if (varid == -1),
  error(['C_RADIALS: ncvardef - unable to define variable: Vtide.'])
end,

text='tides radial velocity';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'Vtide:long_name.'])
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'Vtide:units.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',vartyp,1,spval);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
        'Vtide:_FillValue.']);
end,

%  Define CODAR detided radial velocity.

[varid]=mexcdf('ncvardef',ncid,'Vdetided',vartyp,3,[tdim adim rdim]);
if (varid == -1),
  error(['C_RADIALS: ncvardef - unable to define variable: Vdetided.'])
end,

text='CODAR detided radial velocity';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'Vdetided:long_name.'])
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'Vdetided:units.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',vartyp,1,spval);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
        'Vdetided:_FillValue.']);
end,

%  Define CODAR filtered and detided radial velocity.

[varid]=mexcdf('ncvardef',ncid,'Vradial',vartyp,3,[tdim adim rdim]);
if (varid == -1),
  error(['C_RADIALS: ncvardef - unable to define variable: Vradial.'])
end,

text='CODAR filtered and detided radial velocity';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'Vradial:long_name.'])
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
         'Vradial:units.'])
end,

[status]=mexcdf('ncattput',ncid,varid,'_FillValue',vartyp,1,spval);
if (status == -1),
  error(['C_RADIALS: ncattput - unable to define attribute: ',...
        'Vradial:_FillValue.']);
end,

%----------------------------------------------------------------------------
%  Leave definition mode.
%----------------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['C_RADIALS: ncendef - unable to leave definition mode.'])
end

%----------------------------------------------------------------------------
%  Close NetCDF file.
%----------------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['C_RADIALS: ncclose - unable to close NetCDF file: ', Oname])
end

return
