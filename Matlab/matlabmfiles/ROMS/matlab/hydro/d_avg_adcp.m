%
%  This script find the average of several HDAT files.
%

IPLOT=1;

instr='ADCP';
Hname(1,:)='28jul98.hdat';

npoly=20;

z=0:-0.1:-30;

%-----------------------------------------------------------------------
%  Compute average cast for each survey.
%-----------------------------------------------------------------------

[nfiles,lstr]=size(Hname);

for n=1:nfiles,
  file=Hname(n,:);
  [Ua(n,:),Va(n,:),Uf(n,:),Vf(n,:),Ui,Vi]=avg_adcp(file,z,npoly);    

  if ( IPLOT ),
    figure; plot(Ua(n,:),z,'k-',Uf(n,:),z,'r-');
    grid on;
    title(['U-velocity: ',file]);
    figure; plot(Va(n,:),z,'k-',Vf(n,:),z,'r-');
    grid on;
    title(['V-velocity: ',file]);
  end,
end,

if ( IPLOT ),
  figure;
  plot(Ua,z,Uf,z); grid on
  title([instr,' Average U-velocity']);

  figure;
  plot(Va,z,Vf,z); grid on
  title([instr,' Average V-velocity']);
end,





