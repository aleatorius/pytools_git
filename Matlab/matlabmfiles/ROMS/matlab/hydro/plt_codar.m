function [h]=plt_codar(fname,tindex,cstlon,cstlat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [h]=plt_codar(fname,tindex,cstlon,cstlat)                   %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    fname     CODAR NetCDF file name (string).                        %
%    tindex    Time record to plot (integer).                          %
%    cstlon    Optional, coastline longitude (vector).                 %
%    cstlat    Optional, coastline latitude (vector).                  %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    h         Graphics handle.                                        %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Read in coastline data, if not provided.
%-----------------------------------------------------------------------

if (nargin < 3),
  cst_file='/d1/arango/scrum3.0/plot/Data/usgs_east.dat';
% cst_file='/d1/arango/scrum3.0/plot/Data/us_cst.dat';
 
  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=900);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
end,

%-----------------------------------------------------------------------
%  Draw coastlines.
%-----------------------------------------------------------------------

m_proj('Mercator');

LonMin=-74.5;
LonMax=-73.6;
LatMin=39.1;
LatMax=39.8;

figure;

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin LatMax]);
hold on;
plot([LonMin LonMax],[LatMax LatMax],'k');
plot([LonMax LonMax],[LatMin LatMax],'k');
grid on;
draw_cst(cstlon,cstlat,'k');

%-----------------------------------------------------------------------
%  Read in CODAR positions.
%-----------------------------------------------------------------------

lon=nc_read(fname,'lon');
lat=nc_read(fname,'lat');

%-----------------------------------------------------------------------
%  Read in CODAR velocity components.
%-----------------------------------------------------------------------

u=nc_read(fname,'u',tindex);
v=nc_read(fname,'v',tindex);

ind=find(u > 900);
u(ind)=NaN;
v(ind)=NaN;

%-----------------------------------------------------------------------
%  Plot CODAR vectors.
%-----------------------------------------------------------------------

h=quiver(lon,lat,u,v,2);

return




