fid=fopen(fname,'r');
if (fid < 0),
  error(['Cannot open ' fname '.'])
end

[f count]=fscanf(fid,'%g %g %g %g %g %g %g %g %g %g %g %g %g %g %g %g\n',...
                 [14 inf]);

adcp.hour   =f( 1:14:count);
adcp.lat    =f( 2:14:count);
adcp.lon    =f( 3:14:count);
adcp.range  =f( 4:14:count);
adcp.z      =f( 5:14:count);
adcp.uraw   =f( 6:14:count);
adcp.vraw   =f( 7:14:count);
adcp.utide  =f( 8:14:count);
adcp.vtide  =f( 9:14:count);
adcp.udetide=f(10:14:count);
adcp.vdetide=f(11:14:count);
adcp.ufilter=f(12:14:count);
adcp.vfilter=f(13:14:count);
adcp.julday =f(14:14:count);


adcp.hour   =f(:,1);
adcp.lat    =f(:,2);
adcp.lon    =f(:,3);
adcp.range  =f( 4:14:count);
adcp.z      =f( 5:14:count);
adcp.uraw   =f( 6:14:count);
adcp.vraw   =f( 7:14:count);
adcp.utide  =f( 8:14:count);
adcp.vtide  =f( 9:14:count);
adcp.udetide=f(10:14:count);
adcp.vdetide=f(11:14:count);
adcp.ufilter=f(12:14:count);
adcp.vfilter=f(13:14:count);
adcp.julday =f(14:14:count);
