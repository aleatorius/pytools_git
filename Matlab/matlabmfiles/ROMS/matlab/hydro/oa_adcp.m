function [status]=oa_adcp(Aname,Gname,Oname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function []=oa_adcp(Aname,Gname,Oname);                             %
%                                                                      %
%  This function processes ADCP OAs.                                   %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     Aname     ADCP NetCDF file (string).                             %
%     Gname     GRID NetCDF file (string).                             %
%     Oname     Output assimilation file (string).                     %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     status    Error flag (integer).                                  %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

%-----------------------------------------------------------------------
% Read in model grid positions and angle.
%-----------------------------------------------------------------------

rlon=nc_read(Gname,'lon_rho');
rlat=nc_read(Gname,'lat_rho');

angle=nc_read(Gname,'angle');

[Lp,Mp]=size(rlon);
L=Lp-1;
M=Mp-1;

%-----------------------------------------------------------------------
%  Inquire size of time record dimension in OA file.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(Aname);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:5) == 'level'),
    Nlev=dsizes(n);
  elseif (name(1:4) == 'time'),
    nrec=dsizes(n);
  end,
end,

%-----------------------------------------------------------------------
%  Inquire dimension size in assimilation file.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(Oname);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:5) == 's_rho')
    N=dsizes(n);
  elseif (name(1:8) == 'vel_time'),
    tindex=dsizes(n);
  end,
end,

%-----------------------------------------------------------------------
%  Compute model depths at RHO-points.
%-----------------------------------------------------------------------

Zr=depths(Gname,1,0,0);

Angle=repmat(angle,[1 1 N]);

%-----------------------------------------------------------------------
%  Process OAed ADCP data.
%-----------------------------------------------------------------------

z=nc_read(Aname,'zout');

for n=1:nrec,

%  Read in data.

  Uwrk=nc_read(Aname,'u',n);
  Vwrk=nc_read(Aname,'v',n);
  Verr=nc_read(Aname,'v_err',n);
  Vrms=nc_read(Aname,'v_rms',n); 
  time=nc_read(Aname,'time',n); 

  
%  Over-write surface value with value below.

  ind=find(Uwrk > 1.0e+10);
  Uwrk(ind)=0;
  Vwrk(ind)=0;
  Verr(ind)=1;

  ind=find(Vrms > 1.0e+10);
  Vrms(ind)=1;

  Uwrk(:,:,Nlev)=Uwrk(:,:,Nlev-1);
  Vwrk(:,:,Nlev)=Vwrk(:,:,Nlev-1);
  Verr(:,:,Nlev)=Verr(:,:,Nlev-1);
  Vrms(Nlev)=Vrms(Nlev-1);

%  Interpolate velocity to vertical model grid.

  for j=1:Mp,
    for i=1:Lp,
      uk=reshape(Uwrk(i,j,:),1,Nlev);
      vk=reshape(Vwrk(i,j,:),1,Nlev);
      ek=reshape(Verr(i,j,:),1,Nlev);
      ek=1-ek./Vrms';
      Zi=reshape(Zr(i,j,:),1,N);
      U(i,j,:)=interp1(z,uk,Zi);
      V(i,j,:)=interp1(z,vk,Zi);
      E(i,j,:)=interp1(z,ek,Zi);
    end,
  end,

  ind=find(isnan(U));
  U(ind)=0;
  V(ind)=0;
  E(ind)=0;

%  Rotate surface velocity to model grid.

   Uwrk=U.*cos(Angle)+V.*sin(Angle);
   Vwrk=V.*cos(Angle)-U.*sin(Angle);

%  Average to U- and V-points.

   u=0.5.*(Uwrk(1:L,1:Mp,:)+Uwrk(2:Lp,1:Mp,:));
   v=0.5.*(Vwrk(1:Lp,1:M,:)+Vwrk(1:Lp,2:Mp,:));

%  Write out data into output NetCDF file.

   status=nc_write(Oname,'u',u,tindex+n);
   status=nc_write(Oname,'v',v,tindex+n);
   status=nc_write(Oname,'Evel',E,tindex+n);
   status=nc_write(Oname,'vel_time',time,tindex+n);

   disp(['Processed input record: ',num2str(n,'%3.3i'),...
         '  and output record: ',num2str(tindex+n,'%3.3i')]);

end,

return
