function [header,nhdr,nsta]=read_header(hid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [header,nhdr,nsta]=read_header(hid)                        %
%                                                                      %
%  This function read the header of a hydrography HDAT file.           %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hid     opened input unit (integer).                             %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     header  file header (text matrix).                               %
%     nhdr    number of header entries (integer).                      %
%     nsta    number of stations reported in header (integer).         %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Read header of a hydrography HDAT file.

nhdr=0;
while 1
  txt=fgetl(hid);
  lenstr=length(txt);
  if (strncmp(txt,' stations =',11)),
    nsta=sscanf(txt(12:lenstr),'%i');
  elseif (strncmp(txt,'stations =',10)),
    nsta=sscanf(txt(11:lenstr),'%i');
  end,
  nhdr=nhdr+1;
  header(nhdr,1:lenstr)=txt;
  if (strcmp(txt,'END')), break, end
end,

return
