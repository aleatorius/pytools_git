%  Modify error field of radial NetCDF file to normalized error variance.

global IPRINT

IPRINT=0;
ICREATE=1;
IWRITE=1;

Nname='/comop/arango/NJB/Data/CODAR/oag1_Nradials.nc';
Sname='/comop/arango/NJB/Data/CODAR/oag1_Sradials.nc';
Oname='/comop/arango/NJB/Data/CODAR/oag1_radials.nc';

%----------------------------------------------------------------------------
%  Determine number of records to process.
%----------------------------------------------------------------------------

Ntime=nc_read(Nname,'Vsur_time');
Stime=nc_read(Sname,'Vsur_time');

nrec=min(length(Ntime),length(Stime));

Sangle=nc_read(Sname,'angle1');
Nangle=nc_read(Nname,'angle2');
[Lp,Mp]=size(Nangle);

%----------------------------------------------------------------------------
%  Create output NetCDF file.
%----------------------------------------------------------------------------

if (ICREATE),
  status=c_oaradials(Lp,Mp,Oname);
  status=nc_write(Oname,'angle1',Sangle);
  status=nc_write(Oname,'angle2',Nangle);
end,

%----------------------------------------------------------------------------
%  Write out data.
%----------------------------------------------------------------------------

if (IWRITE),

  for n=1:nrec,

    SVrad=nc_read(Sname,'Vradial1',n);
    SVerr=nc_read(Sname,'Vradial1_err',n);
%   SVrms=nc_read(Sname,'Vradial_rms',n);
%   SVerr=SVerr./SVrms;

    NVrad=nc_read(Nname,'Vradial2',n);
    NVerr=nc_read(Nname,'Vradial2_err',n);
%   NVrms=nc_read(Nname,'Vradial_rms',n);
%   NVerr=NVerr./NVrms;

    s1=nc_write(Oname,'Vsur_time',Ntime(n),n);
    s2=nc_write(Oname,'Vradial1',NVrad,n);
    s3=nc_write(Oname,'Vradial1_err',NVerr,n);
    s4=nc_write(Oname,'Vradial2',SVrad,n);
    s5=nc_write(Oname,'Vradial2_err',SVerr,n);

    disp(['Processed input record: ',num2str(n,'%3.3i'),...
          ' status: ',num2str(s1+s2+s3+s4+s5)]);

  end,
end,
