function [lon,lat,Ztop,Zbot,time,T,S]=get_hydro(Hname,z);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2000 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Hernan G. Arango %%%
%                                                                           %
% function [lon,lat,Ztop,Zbot,time,T,S]=get_hydro(Hname,z);                 %
%                                                                           %
%  This function reads a HDAT file and interpolates Temperature and         %
%  Salinity to specified depths.                                            %
%                                                                           %
%  On Input:                                                                %
%                                                                           %
%     Hname    HDAT file names (string).                                    %
%     z        Depths to interpolate (vector; meters, negative).            %
%                                                                           %
%  On Output:                                                               %
%                                                                           %
%     lon      Longitude of casts.                                          %
%     lat      Latitude of casts.                                           %
%     Ztop     Shallowest depth of available data.                          %
%     Zbot     Deepest depth of available data.                             %
%     time     Time of casts.                                               %
%     T        Temperature casts.                                           %
%     S        Salinity casts.                                              %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='cubic';

%-----------------------------------------------------------------------
%  Read in HDAT file(s).
%-----------------------------------------------------------------------

[nfiles,lstr]=size(Hname);

ic=0;

for m=1:nfiles,

  fname=deblank(Hname(m,:));

  [header,hinfo,htype,hdata]=rhydro(fname);

  [nhvar,nhpts,nhsta]=size(hdata);

%-----------------------------------------------------------------------
%  Interpolate casts to a regular grid.
%-----------------------------------------------------------------------

  for n=1:nhsta,
    npts=hinfo(n,2);
    if (npts > 5),
      ic=ic+1;
      Zdat=squeeze(-hdata(1,1:npts,n));
      Tdat=squeeze(hdata(2,1:npts,n));
      Sdat=squeeze(hdata(3,1:npts,n));
      Ti=interp1(Zdat,Tdat,z,method);
      Si=interp1(Zdat,Sdat,z,method);
      T(:,ic)=Ti';
      S(:,ic)=Si';
      lon(ic)=hinfo(n,4);
      lat(ic)=hinfo(n,5);
      time(ic)=hinfo(n,7);
      Ztop(ic)=Zdat(1);
      Zbot(ic)=Zdat(npts);
    end,
  end,

end,

return
