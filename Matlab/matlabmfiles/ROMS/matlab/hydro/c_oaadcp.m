function status=c_oaadcp(Gname,Oname,Nlev);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function status=c_oaadcp(Gname,Oname,Nlev);                               %
%                                                                           %
% This function creates an output NetCDF.                                   %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Gname       Grid NetCDF file name (string).                            %
%    Oname       Output NetCDF file name (string).                          %
%    Nlev        Number of vertical levels (integer).                       %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Inquire grid dimensions.

[dnames,dsizes]=nc_dim(Gname);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:6) == 'xi_rho'),
    L=dsizes(n);
  elseif (name(1:7) == 'eta_rho'),
    M=dsizes(n);
  end,
end,
Ntime=Inf;

%  Supress all error messages from NetCDF.

[ncopts]=mexcdf('setopts',0);

%  Get some NetCDF parameters.

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');
[ncclob]=mexcdf('parameter','nc_clobber');

%  Set global attribute.

type='DATA file';
history=['Data from OSCR ', date_stamp];

%  Set some parameters.

vartyp=ncfloat;

%  Create NetCDF file.

[ncid,status]=mexcdf('nccreate',Oname,'nc_write');
if (ncid == -1),
  error(['C_OAADCP: ncopen - unable to create file: ', Oname]);
  return
end

%----------------------------------------------------------------------------
%  Define dimensions.
%----------------------------------------------------------------------------

[xrdim]=mexcdf('ncdimdef',ncid,'xi_rho',L);
if (xrdim == -1),
  error(['C_OAADCP: ncdimdef - unable to define dimension: xi_rho.']);
end,

[xudim]=mexcdf('ncdimdef',ncid,'xi_u',L-1);
if (xudim == -1),
  error(['C_OAADCP: ncdimdef - unable to define dimension: xi_u.']);
end,

[xvdim]=mexcdf('ncdimdef',ncid,'xi_v',L);
if (xvdim == -1),
  error(['C_OAADCP: ncdimdef - unable to define dimension: xi_v.']);
end,

[yrdim]=mexcdf('ncdimdef',ncid,'eta_rho',M);
if (yrdim == -1),
  error(['C_OAADCP: ncdimdef - unable to define dimension: eta_rho.']);
end,

[yudim]=mexcdf('ncdimdef',ncid,'eta_u',M);
if (yudim == -1),
  error(['C_OAADCP: ncdimdef - unable to define dimension: eta_u.']);
end,

[yvdim]=mexcdf('ncdimdef',ncid,'eta_v',M-1);
if (yvdim == -1),
  error(['C_OAADCP: ncdimdef - unable to define dimension: eta_v.']);
end,

if (Nlev > 0),
  [srdim]=mexcdf('ncdimdef',ncid,'s_rho',Nlev);
  if (srdim == -1),
    error(['C_OAADCP: ncdimdef - unable to define dimension: s_rho.']);
  end,
end,

if (isinf(Ntime)),
  [tdim]=mexcdf('ncdimdef',ncid,'vel_time',ncunlim);
  if (tdim == -1),
    error(['C_OAADCP: ncdimdef - unable to define unlimited dimension: ',...
           'vel_time.']);
  end,
else
  [tdim]=mexcdf('ncdimdef',ncid,'vel_time',Ntime);
  if (tdim == -1),
    error(['C_OAADCP: ncdimdef - unable to define dimension: vel_time.']);
  end,
end,

%----------------------------------------------------------------------------
%  Create global attribute(s).
%----------------------------------------------------------------------------

lenstr=max(size(type));
[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lenstr,type);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to global attribure: history.']);
  return
end

lenstr=max(size(history));
[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lenstr,history);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to global attribure: history.']);
  return
end

%----------------------------------------------------------------------------
%  Define variables.
%----------------------------------------------------------------------------

%  Define spherical switch.

[varid]=mexcdf('ncvardef',ncid,'spherical',ncchar,0,0);
if (varid == -1),
  error(['C_OAADCP: ncvardef - unable to define variable: spherical.']);
end

[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,24,...
                'grid type logical switch');
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
        'spherical:long_name.']);
end

[status]=mexcdf('ncattput',ncid,varid,'option_T',ncchar,9,'spherical');
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
        'spherical:option_T']);
end

[status]=mexcdf('ncattput',ncid,varid,'option_F',ncchar,9,'Cartesian');
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
        'spherical:option_F']);
end

%  Defined S-coordinate critical depth.

[varid]=mexcdf('ncvardef',ncid,'hc',ncdouble,0,0);
if (varid == -1),
  error(['C_OAADCP: ncvardef - unable to define variable: hc.']);
end,

text='S-coordinate parameter, critical depth';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'hc:long_name.']);
end,

text='meter';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'hc:units.']);
end,

%  Define S-coordinates curves.

[varid]=mexcdf('ncvardef',ncid,'sc_r',ncdouble,1,[srdim]);
if (varid == -1),
  error(['C_OAADCP: ncvardef - unable to define variable: sc_r.']);
end,

text='S-coordinate at RHO-points';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'sc_r:long_name.'])
end,

text='nondimensional';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'sc_r:units.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'valid_min',ncdouble,1,-1.d0);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
        'sc_r:valid_min.']);
end

[status]=mexcdf('ncattput',ncid,varid,'valid_max',ncdouble,1,0.d0);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
        'sc_r:valid_max.']);
end

%  Define S-coordinate stretching curves at RHO-points.

[varid]=mexcdf('ncvardef',ncid,'Cs_r',ncdouble,1,[srdim]);
if (varid == -1),
  error(['C_OAADCP: ncvardef - unable to define variable: Cs_r.']);
end,

text='S-coordinate stretching curves at RHO-points';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'Cs_r:long_name.']);
end,

text='nondimensional';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'Cs_r:units.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'valid_min',ncdouble,1,-1.d0);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
        'Cs_r:valid_min.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'valid_max',ncdouble,1,0.d0);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
        'Cs_r:valid_max.']);
end,

%  Define time variable.

[varid]=mexcdf('ncvardef',ncid,'vel_time',ncdouble,1,[tdim]);
if (varid == -1),
  error(['C_OAADCP: ncvardef - unable to define variable: time.']);
end,

text='horizontal momentum time';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'vel_time:long_name.']);
end,

text='Julian day';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'vel_time:units.']);
end,

[status]=mexcdf('ncattput',ncid,varid,'add_offset',ncdouble,1,...
                2440000.d0);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
        'vel_time:add_offset.']);
end

text='time, scalar, series';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'time:field.']);
end,

%  Define 3D variable #1 at U-points.

[varid]=mexcdf('ncvardef',ncid,'u',vartyp,4,[tdim srdim yudim xudim]);
if (varid == -1),
  error(['C_OAADCP: ncvardef - unable to define variable: u.']);
end,

text='u-momentum component';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'u:long_name.']);
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'u:units.']);
end,

text='u-velocity, scalar, series';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'Usur:field.']);
end,

%  Define 3D variable #1 at V-points.

[varid]=mexcdf('ncvardef',ncid,'v',vartyp,4,[tdim srdim yvdim xvdim]);
if (varid == -1),
  error(['C_OAADCP: ncvardef - unable to define variable: v.']);
end,

text='v-momentum component';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'v:long_name.']);
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'v:units.']);
end,

text='v-velocity, scalar, series';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'v:field.']);
end,

%  Define 3D variable #3 at RHO-points.

[varid]=mexcdf('ncvardef',ncid,'Evel',vartyp,4,[tdim srdim yrdim xrdim]);
if (varid == -1),
  error(['C_OAADCP: ncvardef - unable to define variable: Evel.']);
end,

text='error for horizontal momentum';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'Evel:long_name.']);
end,

text='nondimensional';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'Evel:units.']);
end,

text='Evel, scalar, series';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
if (status == -1),
  error(['C_OAADCP: ncattput - unable to define attribute: ',...
         'Evel:field.']);
end,

%----------------------------------------------------------------------------
%  Leave definition mode.
%----------------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['C_OAADCP: ncendef - unable to leave definition mode.']);
end,

%----------------------------------------------------------------------------
%  Close NetCDF file.
%----------------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['C_OAADCP: ncclose - unable to close NetCDF file: ', Oname]);
end,

%-----------------------------------------------------------------------
%  Write out grid parameters.
%-----------------------------------------------------------------------

hc=nc_read(Gname,'hc');
sc_r=nc_read(Gname,'sc_r');
Cs_r=nc_read(Gname,'Cs_r');

[status]=nc_write(Oname,'spherical','T');
%[status]=nc_write(Oname,'hc',hc);
[status]=nc_write(Oname,'sc_r',sc_r);
[status]=nc_write(Oname,'Cs_r',Cs_r);

return
