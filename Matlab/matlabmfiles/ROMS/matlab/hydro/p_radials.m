cst_file='/d1/arango/scrum3.0/plot/Data/usgs_east.dat';

Nname='/comop/arango/NJB/Data/CODAR/radials_north.nc';
Sname='/comop/arango/NJB/Data/CODAR/radials_south.nc';

spval=999;

LonMin=-74.6;
LonMax=-73.6;
LatMin=39.0;
LatMax=40.0;

Xlabs=LonMin:0.1:LonMax;
Ylabs=LatMin:0.1:LatMax;

% Initialize projection.

m_proj('Mercator','long',[LonMin LonMax],'lat',[LatMin LatMax]);

%---------------------------------------------------------------------
%  Read in coastlines file.
%---------------------------------------------------------------------

[cstlon,cstlat]=rcoastline(cst_file);
[icst]=find(cstlon>=900);
cstlon(icst)=NaN;
cstlat(icst)=NaN;

[Xcst,Ycst]=m_ll2xy(cstlon,cstlat);

%---------------------------------------------------------------------
%  Read in radials data.
%---------------------------------------------------------------------

Nlon=nc_read(Nname,'lon');
ind=find(Nlon == spval);
if (~isempty(ind)), 
  Nlon(ind)=NaN;
end,

Nlat=nc_read(Nname,'lat');
ind=find(Nlat == spval);
if (~isempty(ind)), 
  Nlat(ind)=NaN;
end,

[Nx,Ny]=m_ll2xy(Nlon,Nlat);

Slon=nc_read(Sname,'lon');
ind=find(Slon == spval);
if (~isempty(ind)), 
  Slon(ind)=NaN;
end,

Slat=nc_read(Sname,'lat');
ind=find(Slat == spval);
if (~isempty(ind)), 
  Slat(ind)=NaN;
end,

[Sx,Sy]=m_ll2xy(Slon,Slat);

%---------------------------------------------------------------------
%  Draw map.
%---------------------------------------------------------------------

plot(Nx,Ny,'r-',Sx,Sy,'b');
hold on;
draw_cst(Xcst,Ycst,'k');
m_grid;
set(gcf,'Units','Normalized',...
        'Position',[0.2 0.1 0.6 0.8],...
        'PaperUnits','Normalized',...
        'PaperPosition',[0 0 1 1]);

print -depsc radials
