%  HyCODE sampling data lines.
%

READ_COAST=0;

cst_file='/n0/arango/ocean/plot/Data/usgs_east.dat';
grd_file='/n7/arango/NJB/Jul00/Data/njb1_grid_a.nc';

spval=900.0;
LonMin=-75;
LonMax=-73;
LatMin=39;
LatMax=40;

XstrD(1)=74.0; XstrM(1)=10.2; YstrD(1)=39.0; YstrM(1)=38.2; STA(1,1:2)='N4';
XendD(1)=73.0; XendM(1)=59.5; YendD(1)=39.0; YendM(1)=31.0;
Istr(1)=2; Iend(1)=29; Jstr(1)=113; Jend(1)=113;

XstrD(2)=74.0; XstrM(2)=11.4; YstrD(2)=39.0; YstrM(2)=35.5; STA(2,1:2)='N3';
XendD(2)=74.0; XendM(2)= 0.7; YendD(2)=39.0; YendM(2)=28.4;
Istr(2)=2; Iend(2)=29; Jstr(2)=106; Jend(2)=106;

XstrD(3)=74.0; XstrM(3)=12.8; YstrD(3)=39.0; YstrM(3)=32.8; STA(3,1:2)='N2';
XendD(3)=74.0; XendM(3)= 2.1; YendD(3)=39.0; YendM(3)=25.9; 
Istr(3)=4; Iend(3)=28; Jstr(3)=99; Jend(3)=99;

XstrD(4)=74.0; XstrM(4)=14.7; YstrD(4)=39.0; YstrM(4)=30.8; STA(4,1:2)='N1';
XendD(4)=74.0; XendM(4)= 4.2; YendD(4)=39.0; YendM(4)=23.9;
Istr(4)=4; Iend(4)=29; Jstr(4)=93; Jend(4)=93;

XstrD(5)=74.0; XstrM(5)=15.8; YstrD(5)=39.0; YstrM(5)=27.7; STA(5,1:2)=' A';
XendD(5)=74.0; XendM(5)= 5.2; YendD(5)=39.0; YendM(5)=20.6;
Istr(5)=5; Iend(5)=30; Jstr(5)=86; Jend(5)=86;

XstrD(6)=74.0; XstrM(6)=18.7; YstrD(6)=39.0; YstrM(6)=26.1; STA(6,1:2)='S1';
XendD(6)=74.0; XendM(6)= 8.3; YendD(6)=39.0; YendM(6)=18.8;
Istr(6)=4; Iend(6)=29; Jstr(6)=80; Jend(6)=80;

XstrD(7)=74.0; XstrM(7)=20.8; YstrD(7)=39.0; YstrM(7)=24.0; STA(7,1:2)='S2';
XendD(7)=74.0; XendM(7)=10.4; YendD(7)=39.0; YendM(7)=17.0;
Istr(7)=4; Iend(7)=29; Jstr(7)=74; Jend(7)=74;

XstrD(8)=74.0; XstrM(8)=23.2; YstrD(8)=39.0; YstrM(8)=22.0; STA(8,1:2)='S3';
XendD(8)=74.0; XendM(8)=12.7; YendD(8)=39.0; YendM(8)=15.1;
Istr(8)=4; Iend(8)=27; Jstr(8)=69; Jend(8)=69;

XstrD(9)=74.0; XstrM(9)=25.8; YstrD(9)=39.0; YstrM(9)=20.1; STA(9,1:2)='S4';
XendD(9)=74.0; XendM(9)=15.3; YendD(9)=39.0; YendM(9)=13.2;
Istr(9)=4; Iend(9)=29; Jstr(9)=63; Jend(9)=63;

nsta=length(XstrD);

for n=1:nsta,
  Xstr(n)=-abs(XstrD(n)+XstrM(n)/60.0);
  Xend(n)=-abs(XendD(n)+XendM(n)/60.0);
  Ystr(n)=YstrD(n)+YstrM(n)/60.0;
  Yend(n)=YendD(n)+YendM(n)/60.0;
  disp([' Station ',STA(n,:),': ', ...
  ' Slon = ',num2str(Xstr(n),'%8.4f'),' Slat = ',num2str(Ystr(n),'%7.4f'), ...
  ', Elon = ',num2str(Xend(n),'%8.4f'),' Elat = ',num2str(Yend(n),'%7.4f')]);
end,

%-------------------------------------------------------------------------
%  Read model grid
%-------------------------------------------------------------------------

rlon=nc_read(grd_file,'lon_rho');
rlat=nc_read(grd_file,'lat_rho');

%-------------------------------------------------------------------------
%  Initialize Mercator Map.
%-------------------------------------------------------------------------

if (READ_COAST),
  m_proj('Mercator','long',[LonMin LonMax],'lat',[LatMin LatMax]);

  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=spval);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  [Xcst,Ycst]=m_ll2xy(cstlon,cstlat,'point');

  [Xmin,Ymin]=m_ll2xy(LonMin,LatMin);
  [Xmax,Ymax]=m_ll2xy(LonMax,LatMax);

  [X,Y]=m_ll2xy(rlon,rlat);
end,

%-------------------------------------------------------------------------
%  Draw map.
%-------------------------------------------------------------------------

figure(1)
set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
        'Xtick',[],'Ytick',[]);
hold on;
draw_cst(Xcst,Ycst,'k');
m_grid('xtick',[LonMin:0.2:LonMax],'ytick',[LatMin:0.2:LatMax]);
set(gcf,'Units','Normalized',...
        'Position',[0.2 0.1 0.6 0.8],...
        'PaperUnits','Normalized',...
        'PaperPosition',[0 0 1 1]);

for n=1:nsta,
  x=X(Istr(n):Iend(n),Jstr(n):Jend(n));
  y=Y(Istr(n):Iend(n),Jstr(n):Jend(n));
  [xp(1),yp(1)]=m_ll2xy(Xstr(n),Ystr(n));
  [xp(2),yp(2)]=m_ll2xy(Xend(n),Yend(n));
  plot(xp,yp,'r+-');
  plot(x,y,'b')
end,


