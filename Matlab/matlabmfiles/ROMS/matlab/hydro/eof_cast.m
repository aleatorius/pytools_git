hname='/n2/arango/NJB/Data/fsi/06jul99_02.hdat';
z=0:-0.5:-30;
npoly=20;

%function []=eof_cast(hname,z,nmodes);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [Tavg,Savg,Tfit,Sfit,Ti,Si,Tcoff,Scorr]=avg_cast(hname,    %
%                                                            z,npoly)  %
%                                                                      %
%  This function reads a HDAT file and interpolates Temperature and    %
%  Salinity to specified depths. Then, it computes average cast and    %
%  fit a n-degree polynomial in a least-squares sense.                 %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hname    HDAT file name (string).                                %
%     z        Depths to interpolate (vector; meters, negative).       %
%     nmodes   Number of modes (integer).                              %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='cubic';

%-----------------------------------------------------------------------
%  Read in HDAT file.
%-----------------------------------------------------------------------

[header,hinfo,htype,hdata]=rhydro(hname);

[nhvar,nhpts,nhsta]=size(hdata);

%-----------------------------------------------------------------------
%  Extract CTD data from HDATA array
%-----------------------------------------------------------------------

P=reshape(hdata(1,:,:),nhpts,nhsta);
T=reshape(hdata(2,:,:),nhpts,nhsta);
S=reshape(hdata(3,:,:),nhpts,nhsta);

%-----------------------------------------------------------------------
%  Interpolate casts to a regular grid.
%-----------------------------------------------------------------------

%  Replicate cast data.

Pwrk=-P;
Twrk=T;
Swrk=S;
Xwrk=repmat(1:nhsta,nhpts,1);

%  Remove null data.

ind=find(Swrk == 0);
if (~isempty(ind)),
  Xwrk(ind)=[];
  Pwrk(ind)=[];
  Twrk(ind)=[];
end,

%  Set regular grid and interpolate data.

[Xi,Pi]=meshgrid(1:nhsta,z);
Ti=griddata(Xwrk,Pwrk,Twrk,Xi,Pi,method);

%-----------------------------------------------------------------------
%  Compute covariance function.
%-----------------------------------------------------------------------

Nz=length(z);

for n=1:Nz,
  for m=1:Nz,
    ic=0;
    Tsum=0;
    for i=1:nhsta,
      if (~isnan(Ti(m,i)) & ~isnan(Ti(n,i)))
        ic=ic+1;
        Tsum=Tsum+Ti(m,i)*Ti(n,i);
      end,
    end,
    if (ic > 0),
      Tcov(m,n)=Tsum/ic;
      Z(m)=z(m);
    end,
  end,
end,

%-----------------------------------------------------------------------
%  Compute compute eigenvalues and eigen vectors.
%-----------------------------------------------------------------------

[V,D]=eigs(Tcov);

%-----------------------------------------------------------------------
%  Fit a polynomial for most energetic EOF.
%-----------------------------------------------------------------------

E=-V(:,1);
E=E';

Ecof=polyfit(Z,E,npoly);
Efit=polyval(Ecof,Z);

%-----------------------------------------------------------------------
%  Test SST extension.
%-----------------------------------------------------------------------

sst=max(max(Ti));
Km=length(Z);
for k=1:Km,
  cff=Ecof(1);
  for n=2:npoly+1
    cff=Z(k)*cff+Ecof(n);
  end,
  Text(k)=cff*sst;
end,

figure;
plot(E,Z,'r',Efit,Z,'k+');

figure;
plot(Text,Z);
