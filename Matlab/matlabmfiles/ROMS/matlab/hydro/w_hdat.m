function w_hdat(Hname,Header,Hinfo,T,S,Z);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function w_hdat(Hname,Header,Hinfo,T,S,Z);                           %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     Hname    Average profile file name (string).                     %
%     Header   File header text matrix.                                %
%     Hinfo    Cast header information (matrix).                       %
%                hinfo(nsta,1):   nhvar   - number of variables        %
%                hinfo(nsta,2):   nhpts   - number of data points      %
%                hinfo(nsta,3):   castid  - cast identifier            %
%                hinfo(nsta,4):   hlng    - longitude                  %
%                hinfo(nsta,5):   hlat    - latitude                   %
%                hinfo(nsta,6):   hdpth   - maximum depth              %
%                hinfo(nsta,7):   htime   - time                       %
%                hinfo(nsta,.):   hscl(1:nhvar) - data scales          %
%                hinfo(nsta,..):  hflag   - special flag               %
%     T        Temperature profile(s).                                 %
%     S        Salinity profile(s).                                    %
%     Z        Depths of profile(s).                                   %
%                                                                      %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Create HDAT file.
%-----------------------------------------------------------------------

fout=fopen(hname,'w');
if (fout<0) error(['Cannot create ' hname '.']), end

%-----------------------------------------------------------------------
%  Write out HDAT global Header.
%-----------------------------------------------------------------------

for n=1:nhdr,
  text=Header(n,:);
  lstr=max(size(text));
  fprintf(fout,'%s\n',text(1:lstr));
end

%-----------------------------------------------------------------------
%  Write out data.
%-----------------------------------------------------------------------

[nhobs,npts]=size(T);

for n=1:nhobs,

  nhvar=hinfo(n,1);
  nhpts=hinfo(n,2);
  castid=hinfo(n,3);
  hlng=hinfo(n,4);
  hlat=hinfo(n,5);
  hdpth=hinfo(n,6);
  htime=hinfo(n,7);
  hscle=hinfo(n,8:7+nhvar);
  hflag=hinfo(n,nhvar+8);
  htype='''CTD: z t s''';
  lstr=length(htype);

  frmt1='%2i %5i %7i %9.4f %8.4f %7.1f %10.4f';
  frmt2='%9.2e %8.2e %8.2e\n';
  frmt3='%3i %s\n';
  frmt4='%7i %6i %6i %6i %6i %6i %6i %6i %6i %6i\n';

  fprintf(fout,frmt1,nhvar,nhpts,castid,hlng,hlat,hdpth,htime);
  fprintf(fout,frmt2,hscle(1:nhvar));
  fprintf(fout,frmt3,hflag,htype(1:lstr));

  ihdat=round((abs(Z)./hscle(1)));
  fprintf(fout,frmt4,ihdat);
  fprintf(fout,'\n');

  ihdat=round((T./hscle(2)));
  fprintf(fout,frmt4,ihdat);
  fprintf(fout,'\n');

  ihdat=round((S./hscle(3)));
  fprintf(fout,frmt4,ihdat);
  fprintf(fout,'\n');

end,

fclose(fout);

return
