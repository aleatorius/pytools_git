function [hinfo,Xi,Pi,Ti,Si,P,T,S]=hdat_ctdsec(hname,z);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [hinfo,Xi,Pi,Ti,Si,P,T,S]=hdat_ctdsec(hname,z);            %
%                                                                      %
%  This function reads a HDAT file and interpolates Temperature and    %
%  Salinity to specified depths.                                       %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hname    HDAT file name (string).                                %
%     z        Depths to interpolate (vector; meters, negative).       %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     hinfo    HDAT information (matrix).                              %
%     Xi       interpolating horizontal grid (matrix).                 %
%     Pi       interpolating vertical grid (matrix).                   %
%     Ti       Interpolated Temperature (matrix).                      %
%     Si       Interpolated Salinity (matrix).                         %
%     T        Read Temperature data (matrix).                         %
%     S        Read Salinity data (matrix).                            %
%     P        Read Pressure data (matrix).                            %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='cubic';

%-----------------------------------------------------------------------
%  Read in HDAT file.
%-----------------------------------------------------------------------

[header,hinfo,htype,hdata]=rhydro(hname);

[nhvar,nhpts,nhsta]=size(hdata);

%-----------------------------------------------------------------------
%  Extract CTD data from HDATA array
%-----------------------------------------------------------------------

P=reshape(hdata(1,:,:),nhpts,nhsta);
T=reshape(hdata(2,:,:),nhpts,nhsta);
S=reshape(hdata(3,:,:),nhpts,nhsta);

%-----------------------------------------------------------------------
%  Interpolate casts to a regular grid.
%-----------------------------------------------------------------------

%  Replicate cast data.

Pwrk=-P;
Twrk=T;
Swrk=S;
Xwrk=repmat(1:nhsta,nhpts,1);

%  Remove null data.

ind=find(Swrk == 0);
Xwrk(ind)=[];
Pwrk(ind)=[];
Twrk(ind)=[];
Swrk(ind)=[];

%  Set regular grid and interpolate data.

[Xi,Pi]=meshgrid(1:nhsta,z);
Ti=griddata(Xwrk,Pwrk,Twrk,Xi,Pi,method);
Si=griddata(Xwrk,Pwrk,Swrk,Xi,Pi,method);

return
