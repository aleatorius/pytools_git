%
%  This script generate initial conditions from an average HDAT profile.
%

global IPRINT

IPRINT=0;

July=36;

switch ( July )
  case 9
    Hname='/nc/arango/NJB/Jul01/cycle0/RunA/njb1_his_00a.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_jul09.nc';
    nrec=65;
    time=nc_read(Hname,'ocean_time',nrec);
  case 11
    Hname='/nc/arango/NJB/Jul01/cycle0/RunB/njb1_his_00b.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_jul11.nc';
    nrec=17;
    time=nc_read(Hname,'ocean_time',nrec);
  case 15
    Hname='/nc/arango/NJB/Jul01/cycle1/Run01/njb1_his_01a.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_jul15.nc';
    nrec=33;
    time=nc_read(Hname,'ocean_time',nrec);
  case 18
    Hname='/nc/arango/NJB/Jul01/cycle2/Run03/njb1_his_03c.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_jul18.nc';
    nrec=25;
    time=nc_read(Hname,'ocean_time',nrec);
  case 22
    Hname='/nc/arango/NJB/Jul01/cycle3/Run05/njb1_his_05b.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_jul22.nc';
    nrec=4;
    time=nc_read(Hname,'ocean_time',nrec);
  case 25
    Hname='/nd/arango/NJB/Jul01/cycle4/Run07/njb1_rst_07b.nc';
%   Hname='/nd/arango/NJB/Jul01/cycle4/Run07/njb1_his_07a.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_jul25.nc';
%   nrec=25;
    nrec=2;
    time=nc_read(Hname,'ocean_time',nrec);
  case 29
    Hname='/nd/arango/NJB/Jul01/cycle5/Run10/njb1_his_10.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_jul29.nc';
    nrec=8;
    time=nc_read(Hname,'ocean_time',nrec);
  case 32
    Hname='/nd/arango/NJB/Jul01/cycle6/Run12/njb1_his_12b.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_aug01.nc';
    nrec=25;
    time=nc_read(Hname,'ocean_time',nrec);
  case 36
    Hname='/nd/arango/NJB/Jul01/cycle7/Run14/njb1_his_14b.nc';
    Iname='/n7/arango/NJB/Jul01/Data/njb1_ini_aug05.nc';
    nrec=33;
    time=nc_read(Hname,'ocean_time',nrec);
end,

Gname='/n7/arango/NJB/Jul00/Data/njb1_grid_a.nc';

%-----------------------------------------------------------------------
%  Read and Write out initial conditions from a history file.
%-----------------------------------------------------------------------

status=nc_write(Iname,'ocean_time',time);

F=nc_read(Hname,'zeta',nrec);
status=nc_write(Iname,'zeta',F);

F=nc_read(Hname,'ubar',nrec);
status=nc_write(Iname,'ubar',F);

F=nc_read(Hname,'vbar',nrec);
status=nc_write(Iname,'vbar',F);

F=nc_read(Hname,'u',nrec);
status=nc_write(Iname,'u',F);

F=nc_read(Hname,'v',nrec);
status=nc_write(Iname,'v',F);

F=nc_read(Hname,'temp',nrec);
status=nc_write(Iname,'temp',F);

F=nc_read(Hname,'salt',nrec);
status=nc_write(Iname,'salt',F);
