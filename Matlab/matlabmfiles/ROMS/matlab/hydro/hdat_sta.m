function [lon,lat,nsta,Stime,Etime]=hdat_sta(hydnam);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [lon,lat,nsta,Stime,Etime]=hdat_sta(hydnam)                %
%                                                                      %
%  This function reads in the longitude/latitude location of each      %
%  cast of a hydrography HDAT file.                                    %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hydnam  input hydrography data filename (string).                %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     lon     longitude of each station (vector).                      %
%     lat     latitude of each station (vector).                       %
%     nsta    number of stations (integer).                            %
%     Stime   start time (string).                                     %
%     Etime   end time (string).                                       %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Open hydrography file.

hid=fopen(hydnam,'r');
if (hid < 0),
  error(['Cannot open ' hydnam '.']),
end

%  Read in header.

[header,nhdr,nsta]=read_header(hid);

%  Get starting and ending times.

for n=1:nhdr,
  txt=header(n,:);
  lenstr=length(txt);
  if (strncmp(txt,' str_time =',11) | strncmp(txt,'str_time =',10)),
    Stime=txt;
  end,
  if (strncmp(txt,' end_time =',11) | strncmp(txt,'end_time =',10)),
    Etime=txt;
  end,   
end,

%  Read hydrographic data.

n=0;
while (feof(hid) == 0),
  [hinfo,htype,hdata,err,message]=read_cast(hid);
  if (err == 0),
    n=n+1;
    castid(n)=hinfo(3);
    lon(n)=hinfo(4);
    lat(n)=hinfo(5);
    depth(n)=hinfo(6);
    time(n)=hinfo(7);
  end,
end,
if (n ~= nsta), nsta=n; end,

% Close hydrography file.

fclose(hid);

return

