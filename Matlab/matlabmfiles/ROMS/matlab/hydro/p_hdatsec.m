function [F]=p_hdatsec(hydnam);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [F]=p_hdatsec(hydnam)                                      %
%                                                                      %
%  This routine plots a hydrographic section from a HDAT file.         %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hydnam  input hydrography data filename (string).                %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     F       Section data.                                            %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Read in HDAT file.

[header,hinfo,htype,hdata]=rhydro(hydnam);

%  Extract hydrographic section.

[nhvar,nhpts,nhsta]=size(hdata);

F.z=reshape(hdata(1,:,:),nhpts,nhsta);
F.t=reshape(hdata(2,:,:),nhpts,nhsta);
F.s=reshape(hdata(3,:,:),nhpts,nhsta);

return



