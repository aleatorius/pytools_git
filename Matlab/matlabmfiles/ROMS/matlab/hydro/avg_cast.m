function [Tavg,Savg,Tfit,Sfit,Ti,Si,Tcoff,Scoof]=avg_cast(hname,z,npoly);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [Tavg,Savg,Tfit,Sfit,Ti,Si,Tcoff,Scorr]=avg_cast(hname,    %
%                                                            z,npoly)  %
%                                                                      %
%  This function reads a HDAT file and interpolates Temperature and    %
%  Salinity to specified depths. Then, it computes average cast and    %
%  fit a n-degree polynomial in a least-squares sense.                 %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hname    HDAT file name (string).                                %
%     z        Depths to interpolate (vector; meters, negative).       %
%     npoly    Degree of polynomial to fit (integer).                  %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     Tavg     Average temperature cast.                               %
%     Savg     Average salinity cast.                                  %
%     Tfit     Average, least-squares fitted, temperature cast.        %
%     Sfit     Average, least-squares fitted, salinity cast.           %
%     Ti       Vertically interpolated temperature data.               %
%     Si       Vertically interpolated salinity data.                  %
%     Tcoff    Polynomial expansion coefficients for temperature.      %
%     Scoff    Polynomial expansion coefficients for temperature.      %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='cubic';

%-----------------------------------------------------------------------
%  Read in HDAT file.
%-----------------------------------------------------------------------

[header,hinfo,htype,hdata]=rhydro(hname);

[nhvar,nhpts,nhsta]=size(hdata);

%-----------------------------------------------------------------------
%  Extract CTD data from HDATA array
%-----------------------------------------------------------------------

P=reshape(hdata(1,:,:),nhpts,nhsta);
T=reshape(hdata(2,:,:),nhpts,nhsta);
S=reshape(hdata(3,:,:),nhpts,nhsta);

%-----------------------------------------------------------------------
%  Interpolate casts to a regular grid.
%-----------------------------------------------------------------------

%  Replicate cast data.

Pwrk=-P;
Twrk=T;
Swrk=S;
Xwrk=repmat(1:nhsta,nhpts,1);

%  Remove null data.

ind=find(Swrk == 0);
Xwrk(ind)=[];
Pwrk(ind)=[];
Twrk(ind)=[];
Swrk(ind)=[];

%  Set regular grid and interpolate data.

[Xi,Pi]=meshgrid(1:nhsta,z);
Ti=griddata(Xwrk,Pwrk,Twrk,Xi,Pi,method);
Si=griddata(Xwrk,Pwrk,Swrk,Xi,Pi,method);

%-----------------------------------------------------------------------
%  Compute average cast.
%-----------------------------------------------------------------------

Nz=length(z);
ic=0;
for n=1:Nz,
  Twrk=Ti(n,:);
  Swrk=Si(n,:);
  ind=find(isnan(Twrk));
  Twrk(ind)=[];
  Swrk(ind)=[];
  if (~isempty(Twrk)),
    Tavg(n)=mean(Twrk);
    Savg(n)=mean(Swrk);
    ic=ic+1;
    iz(ic)=n;
  else
    Tavg(n)=NaN;
    Savg(n)=NaN;
  end,
end,

%-----------------------------------------------------------------------
%  Fill Average cast with surface and bottom values.
%-----------------------------------------------------------------------

nsur=iz(1);
nbot=iz(ic);

Tsur=Tavg(nsur);
Ssur=Savg(nsur);

Tbot=Tavg(nbot);
Sbot=Savg(nbot);

Tavg(1:nsur-1)=Tsur;
Savg(1:nsur-1)=Ssur;

Tavg(nbot+1:Nz)=Tbot;
Savg(nbot+1:Nz)=Sbot;

%-----------------------------------------------------------------------
%  Fit an N-degree polynomial in a least-square sense.
%-----------------------------------------------------------------------

Tcoff=polyfit(z,Tavg,npoly);
Tfit=polyval(Tcoff,z);

Scoff=polyfit(z,Savg,npoly);
Sfit=polyval(Scoff,z);

%-----------------------------------------------------------------------
%  Write average profile into a HDAT file.
%-----------------------------------------------------------------------

oname=[hname,'_avg'];
wrt_avg(oname,header,hinfo,Tfit,Sfit,z);

return
