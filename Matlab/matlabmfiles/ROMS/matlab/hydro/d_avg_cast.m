%
%  This script find the average of several HDAT files.
%

IPLOT=1;

%instr='SeaBird';
instr='MiniBat';

switch (instr),
  case 'SeaBird'
  case 'MiniBat'
    Hname( 1,:)='/n2/arango/NJB/Data/fsi/06jul99_01.hdat';
    Hname( 2,:)='/n2/arango/NJB/Data/fsi/06jul99_02.hdat';
    Hname( 3,:)='/n2/arango/NJB/Data/fsi/07jul99_01.hdat';
    Hname( 4,:)='/n2/arango/NJB/Data/fsi/08jul99_02.hdat';
    Hname( 5,:)='/n2/arango/NJB/Data/fsi/08jul99_03.hdat';
    Hname( 6,:)='/n2/arango/NJB/Data/fsi/08jul99_01.hdat';
    Hname( 7,:)='/n2/arango/NJB/Data/fsi/09jul99_01.hdat';
    Hname( 8,:)='/n2/arango/NJB/Data/fsi/09jul99_02.hdat';
    Hname( 9,:)='/n2/arango/NJB/Data/fsi/09jul99_03.hdat';
    Hname(10,:)='/n2/arango/NJB/Data/fsi/12jul99_01.hdat';
    Hname(11,:)='/n2/arango/NJB/Data/fsi/12jul99_02.hdat';
    Hname(12,:)='/n2/arango/NJB/Data/fsi/12jul99_03.hdat';
end,

npoly=20;

z=0:-0.1:-30;

%-----------------------------------------------------------------------
%  Compute average cast for each survey.
%-----------------------------------------------------------------------

[nfiles,lstr]=size(Hname);

for n=1:nfiles,
  file=Hname(n,:);
  [Ta(n,:),Sa(n,:),Tf(n,:),Sf(n,:),Ti,Si,Tc,Sc]=avg_cast(file,z,npoly);    

  if ( IPLOT ),
    figure; plot(Ta(n,:),z,'k-',Tf(n,:),z,'r-');
    set(gca,'Xlim',[6 24]); grid on;
    title(['Temperature: ',file]);
    figure; plot(Sa(n,:),z,'k-',Sf(n,:),z,'r-'); grid on;
    title(['Salinity: ',file]);
    set(gca,'Xlim',[28 32]); grid on;
    print -dps -append avg_cast.ps
  end,
end,

if ( IPLOT ),
  figure;
  plot(Ta,z); grid on
  set(gca,'Xlim',[6 24]); grid on;
  title([instr,' Average Temperature']);

  figure;
  plot(Sa,z); grid on
  set(gca,'Xlim',[28 32]); grid on;
  title([instr,' Average Salinity']);
end,





