function [header,hinfo,htype,hdata]=rhydro(hydnam);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [header,hinfo,htype,z,t,s]=rhydro(hydnam)                  %
%                                                                      %
%  Read and decode file header for hydrography data written in HDAT    %
%  ASCII format.                                                       %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hydnam  input hydrography data filename (string).                %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     header   file header text matrix.                                %
%     hinfo    Cast header information (matrix).                       %
%                hinfo(nsta,1):   nhvar   - number of variables        %
%                hinfo(nsta,2):   nhpts   - number of data points      %
%                hinfo(nsta,3):   castid  - cast identifier            %
%                hinfo(nsta,4):   hlng    - longitude                  %
%                hinfo(nsta,5):   hlat    - latitude                   %
%                hinfo(nsta,6):   hdpth   - maximum depth              %
%                hinfo(nsta,7):   htime   - time                       %
%                hinfo(nsta,.):   hscl(1:nhvar) - data scales          %
%                hinfo(nsta,..):  hflag   - special flag               %
%     htype    hydrography type identification (string matrix).        %
%     hdata    hydrography field data (array).                         %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Open hydrography file.

hid=fopen(hydnam,'r');
if (hid < 0),
  error(['Cannot open ' hydnam '.']),
end

%  Read in header.

[header,nhdr,nsta]=read_header(hid);

% Read hydrographic data.

for n=1:nsta,
  [line,type,data,err,message]=read_cast(hid);
  if (err ~= 0),
    error(['Cannot read cast in ' hydnam '. ' message]),
  end,
  nhvar=line(1);
  nhpts=line(2);
  hinfo(n,1:8+nhvar)=line(1:8+nhvar)';
  lenstr=length(type);
  htype(n,1:lenstr)=type(1:lenstr);
  hdata(1:nhvar,1:nhpts,n)=data(1:nhvar,1:nhpts);
end,

return
