function [status]=oa_codar(Cname,Gname,Oname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function []=oa_codar(Cname,Gname,Oname);                            %
%                                                                      %
%  This function processes CODAR OAs.                                  %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     Cname     CODAR NetCDF file (string).                            %
%     Gname     GRID NetCDF file (string).                             %
%     Oname     Output assimilation file (string).                     %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     status    Error flag (integer).                                  %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

%-----------------------------------------------------------------------
% Read in model grid positions and angle.
%-----------------------------------------------------------------------

rlon=nc_read(Gname,'lon_rho');
rlat=nc_read(Gname,'lat_rho');

angle=nc_read(Gname,'angle');

[Lp,Mp]=size(rlon);
L=Lp-1;
M=Mp-1;

%-----------------------------------------------------------------------
%  Inquire size of time record dimension in OA file.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(Cname);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:4) == 'time'),
    nrec=dsizes(n);
  end,
end,

%-----------------------------------------------------------------------
%  Inquire size of time record dimension in assimilation file.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(Oname);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:9) == 'Vsur_time'),
    tindex=dsizes(n);
  end,
end,

%-----------------------------------------------------------------------
%  Process OAed CODAR data.
%-----------------------------------------------------------------------

for n=1:nrec,

%  Read in data.

  Usur=nc_read(Cname,'Usur',n);
  Vsur=nc_read(Cname,'Vsur',n);
  Verr=nc_read(Cname,'Vsur_err',n);
  Vrms=nc_read(Cname,'Vsur_rms',n); 
  time=nc_read(Cname,'time',n); 

%  Rotate surface velocity to model grid.

   Uwrk=Usur.*cos(angle)+Vsur.*sin(angle);
   Vwrk=Vsur.*cos(angle)-Usur.*sin(angle);

%  Average to U- and V-points.

   u=0.5.*(Uwrk(1:L,1:Mp)+Uwrk(2:Lp,1:Mp));
   v=0.5.*(Vwrk(1:Lp,1:M)+Vwrk(1:Lp,2:Mp));

%  Nondimensionalize the error by the field variance.

   Verr=1-Verr./Vrms;

%  Write out data into output NetCDF file.

   status=nc_write(Oname,'Usur',u,tindex+n);
   status=nc_write(Oname,'Vsur',v,tindex+n);
   status=nc_write(Oname,'Esur',Verr,tindex+n);
   status=nc_write(Oname,'Vsur_time',time,tindex+n);

   disp(['Processed input record: ',num2str(n,'%3.3i'),...
         '  and output record: ',num2str(tindex+n,'%3.3i')]);

end,

return
