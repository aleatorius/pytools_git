function status=c_oaradials(Im,Jm,Oname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function status=c_c_oaradials(Im,Jm,Oname);                               %
%                                                                           %
% This function creates a OA RADIAL CODAR NetCDF.                           %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Im          Number of points in the radial-direction.                  %
%    Jm          Number of points on the angle-direction.                   %
%    Oname       Output NetCDF file name (string).                          %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Supress all error messages from NetCDF.

[ncopts]=mexcdf('setopts',0);

%  Get some NetCDF parameters.

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');
[ncclob]=mexcdf('parameter','nc_clobber');

%  Set global attribute.

type='OA DATA file';
history=['Data from CODAR, ', date_stamp];

%  Set some parameters.

vartyp=ncfloat;

%  Create NetCDF file.

[ncid,status]=mexcdf('nccreate',Oname,'nc_clobber');
if (ncid == -1),
  error(['C_OARADIALS: ncopen - unable to create file: ', Oname])
  return
end

%----------------------------------------------------------------------------
%  Define dimensions.
%----------------------------------------------------------------------------

[xdim]=mexcdf('ncdimdef',ncid,'lon_rho',Im);
if (xdim == -1),
  error(['C_OARADIALS: ncdimdef - unable to define dimension: xi_rho.'])
end,

[ydim]=mexcdf('ncdimdef',ncid,'lat_rho',Jm);
if (ydim == -1),
  error(['C_OARADIALS: ncdimdef - unable to define dimension: eta_rho.'])
end,

[tdim]=mexcdf('ncdimdef',ncid,'Vsur_time',ncunlim);
if (tdim == -1),
  error(['C_OARADIALS: ncdimdef - unable to define unlimited dimension: ',...
         ' time.'])
end,

%----------------------------------------------------------------------------
%  Create global attribute(s).
%----------------------------------------------------------------------------

lenstr=max(size(type));
[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lenstr,type);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to global attribure: history.'])
  return
end

lenstr=max(size(history));
[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lenstr,history);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to global attribure: history.'])
  return
end

%----------------------------------------------------------------------------
%  Define variables.
%----------------------------------------------------------------------------

%  Define time variable.

[varid]=mexcdf('ncvardef',ncid,'Vsur_time',ncdouble,1,[tdim]);
if (varid == -1),
  error(['C_OARADIALS: ncvardef - unable to define variable: time.'])
end,

text='estimate time';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'time:long_name.'])
end,

text='Julian day';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'time:units.'])
end,

%  Define angles from the CODAR source.

[varid]=mexcdf('ncvardef',ncid,'angle1',vartyp,2,[ydim xdim]);
if (varid == -1),
  error(['C_OARADIALS: ncvardef - unable to define variable: angle1.'])
end,

text='angle from CODAR source 1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'angle1:long_name.'])
end,

text='radians, clockwise from true North';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'angle1:units.'])
end,

[varid]=mexcdf('ncvardef',ncid,'angle2',vartyp,2,[ydim xdim]);
if (varid == -1),
  error(['C_OARADIALS: ncvardef - unable to define variable: angle1.'])
end,

text='angle from CODAR source 2';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'angle2:long_name.'])
end,

text='radians, clockwise from true North';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'angle2:units.'])
end,

%  Define OA CODAR radial velocity.

[varid]=mexcdf('ncvardef',ncid,'Vradial1',vartyp,3,[tdim ydim xdim]);
if (varid == -1),
  error(['C_OARADIALS: ncvardef - unable to define variable: Vradial1.'])
end,

text='surface radial velocity from CODAR source 1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'Vradial1:long_name.'])
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'Vradial1:units.'])
end,

[varid]=mexcdf('ncvardef',ncid,'Vradial2',vartyp,3,[tdim ydim xdim]);
if (varid == -1),
  error(['C_OARADIALS: ncvardef - unable to define variable: Vradial2.'])
end,

text='surface radial velocity from CODAR source 2';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'Vradial2:long_name.'])
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'Vradial2:units.'])
end,

%  Define OA error variance.

[varid]=mexcdf('ncvardef',ncid,'Vradial1_err',vartyp,3,[tdim ydim xdim]);
if (varid == -1),
  error(['C_OARADIALS: ncvardef - unable to define variable: Vradial1_err.'])
end,

text='observation error, surface radial velocity from CODAR source 1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'Vradial1_err:long_name.'])
end,

text='nondimensional';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'Vradial1_err:units.'])
end,

[varid]=mexcdf('ncvardef',ncid,'Vradial2_err',vartyp,3,[tdim ydim xdim]);
if (varid == -1),
  error(['C_OARADIALS: ncvardef - unable to define variable: Vradial2_err.'])
end,

text='observation error, surface radial velocity from CODAR source 2';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'Vradial2_err:long_name.'])
end,

text='nondimensional';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['C_OARADIALS: ncattput - unable to define attribute: ',...
         'Vradial2_err:units.'])
end,

%----------------------------------------------------------------------------
%  Leave definition mode.
%----------------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['C_OARADIALS: ncendef - unable to leave definition mode.'])
end

%----------------------------------------------------------------------------
%  Close NetCDF file.
%----------------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['C_OARADIALS: ncclose - unable to close NetCDF file: ', Oname])
end

return
