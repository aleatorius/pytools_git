function status=c_oacodar(Gname,Oname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function status=c_oacodar(Gname,Oname);                                   %
%                                                                           %
% This function creates an output NetCDF.                                   %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Gname       Grid NetCDF file name (string).                            %
%    Oname       Output NetCDF file name (string).                          %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Inquire grid dimensions.

[dnames,dsizes]=nc_dim(Gname);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:6) == 'xi_rho'),
    L=dsizes(n);
  elseif (name(1:7) == 'eta_rho'),
    M=dsizes(n);
  end,
end,
N=0;
Ntime=Inf;

%  Supress all error messages from NetCDF.

[ncopts]=mexcdf('setopts',0);

%  Get some NetCDF parameters.

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');
[ncclob]=mexcdf('parameter','nc_clobber');

%  Set global attribute.

type='DATA file';
history=['Data from OSCR ', date_stamp];

%  Set some parameters.

vartyp=ncfloat;

%  Create NetCDF file.

[ncid,status]=mexcdf('nccreate',Oname,'nc_write');
if (ncid == -1),
  error(['NC_CREATE: ncopen - unable to create file: ', Oname])
  return
end

%----------------------------------------------------------------------------
%  Define dimensions.
%----------------------------------------------------------------------------

[xrdim]=mexcdf('ncdimdef',ncid,'xi_rho',L);
if (xrdim == -1),
  error(['NC_CREATE: ncdimdef - unable to define dimension: xi_rho.'])
end,

[xudim]=mexcdf('ncdimdef',ncid,'xi_u',L-1);
if (xudim == -1),
  error(['NC_CREATE: ncdimdef - unable to define dimension: xi_u.'])
end,

[xvdim]=mexcdf('ncdimdef',ncid,'xi_v',L);
if (xvdim == -1),
  error(['NC_CREATE: ncdimdef - unable to define dimension: xi_v.'])
end,

%[xpdim]=mexcdf('ncdimdef',ncid,'xi_psi',L-1);
%if (xpdim == -1),
%  error(['NC_CREATE: ncdimdef - unable to define dimension: xi_psi.'])
%end,

[yrdim]=mexcdf('ncdimdef',ncid,'eta_rho',M);
if (yrdim == -1),
  error(['NC_CREATE: ncdimdef - unable to define dimension: eta_rho.'])
end,

[yudim]=mexcdf('ncdimdef',ncid,'eta_u',M);
if (yudim == -1),
  error(['NC_CREATE: ncdimdef - unable to define dimension: eta_u.'])
end,

[yvdim]=mexcdf('ncdimdef',ncid,'eta_v',M-1);
if (yvdim == -1),
  error(['NC_CREATE: ncdimdef - unable to define dimension: eta_v.'])
end,

%[ypdim]=mexcdf('ncdimdef',ncid,'eta_psi',M-1);
%if (ypdim == -1),
%  error(['NC_CREATE: ncdimdef - unable to define dimension: eta_psi.'])
%end,

if (N > 0),
  [srdim]=mexcdf('ncdimdef',ncid,'s_rho',N);
  if (srdim == -1),
    error(['NC_CREATE: ncdimdef - unable to define dimension: s_rho.'])
  end,
  [swdim]=mexcdf('ncdimdef',ncid,'s_w',Np);
  if (yudim == -1),
    error(['NC_CREATE: ncdimdef - unable to define dimension: eta_u.'])
  end,
end,

if (isinf(Ntime)),
  [tdim]=mexcdf('ncdimdef',ncid,'Vsur_time',ncunlim);
  if (tdim == -1),
    error(['NC_CREATE: ncdimdef - unable to define unlimited dimension: ',...
           ' time.'])
  end,
else
  [tdim]=mexcdf('ncdimdef',ncid,'Vsur_time',Ntime);
  if (tdim == -1),
    error(['NC_CREATE: ncdimdef - unable to define dimension: time.'])
  end,
end,

%----------------------------------------------------------------------------
%  Create global attribute(s).
%----------------------------------------------------------------------------

lenstr=max(size(type));
[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lenstr,type);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to global attribure: history.'])
  return
end

lenstr=max(size(history));
[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lenstr,history);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to global attribure: history.'])
  return
end

%----------------------------------------------------------------------------
%  Define variables.
%----------------------------------------------------------------------------

%  Define time variable.

[varid]=mexcdf('ncvardef',ncid,'Vsur_time',ncdouble,1,[tdim]);
if (varid == -1),
  error(['NC_CREATE: ncvardef - unable to define variable: time.'])
end,

text='surface momentum time';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'time:long_name.'])
end,

text='Julian day';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'time:units.'])
end,

text='time, scalar, series';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'time:field.'])
end,

%  Define 3D variable #1 at U-points.

[varid]=mexcdf('ncvardef',ncid,'Usur',vartyp,3,[tdim yudim xudim]);
if (varid == -1),
  error(['NC_CREATE: ncvardef - unable to define variable: Usur.'])
end,

text='surface u-momentum component';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Usur:long_name.'])
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Usur:units.'])
end,

text='u-surface, scalar, series';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Usur:field.'])
end,

%  Define 3D variable #1 at V-points.

[varid]=mexcdf('ncvardef',ncid,'Vsur',vartyp,3,[tdim yvdim xvdim]);
if (varid == -1),
  error(['NC_CREATE: ncvardef - unable to define variable: Vsur.'])
end,

text='surface v-momentum component';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Vsur:long_name.'])
end,

text='meter second-1';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Vsur:units.'])
end,

text='v-surface, scalar, series';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Vsur:field.'])
end,

%  Define 3D variable #3 at RHO-points.

[varid]=mexcdf('ncvardef',ncid,'Esur',vartyp,3,[tdim yrdim xrdim]);
if (varid == -1),
  error(['NC_CREATE: ncvardef - unable to define variable: Esur.'])
end,

text='error for surface momentum';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Esur:long_name.'])
end,

text='nondimensional';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Esur:units.'])
end,

text='Esur, scalar, series';
lstr=max(size(text));
[status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
if (status == -1),
  error(['NC_CREATE: ncattput - unable to define attribute: ',...
         'Esur:field.'])
end,

%----------------------------------------------------------------------------
%  Leave definition mode.
%----------------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['NC_CREATE: ncendef - unable to leave definition mode.'])
end

%----------------------------------------------------------------------------
%  Close NetCDF file.
%----------------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['NC_CREATE: ncclose - unable to close NetCDF file: ', Oname])
end

return
