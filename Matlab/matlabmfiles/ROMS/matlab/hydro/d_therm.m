

Tname='/coamps/thermistor/990718.trm';
Hname='/n3/arango/NJB/Jul99/cycle5/Run10/njb1_his_10a.nc';
Gname='/n2/arango/NJB/Jul99/Data/njb1_grid_a.nc';

%---------------------------------------------------------------------
% Compute depths at RHO-points.
%---------------------------------------------------------------------

Zr=depths(Hname,Gname,1,0,0);

[Im,Jm,Km]=size(Zr);

%---------------------------------------------------------------------
% Set horizontal positions.
%---------------------------------------------------------------------

rlon=nc_read(Gname,'lon_rho');
rlat=nc_read(Gname,'lat_rho');

Xr=repmat(rlon,[1 1 Km]);
Yr=repmat(rlat,[1 1 Km]);

%---------------------------------------------------------------------
% Read in thermistor string data.
%---------------------------------------------------------------------

data=r_therm(Tname);

%---------------------------------------------------------------------
% Interpolate to model grid.
%---------------------------------------------------------------------
