function wrt_avg(hname,header,hinfo,Tavg,Savg,Zavg);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function wrt_avg(hname,header,hinfo,Tavg,Savg,Zavg);                 %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hname    Average profile file name (string).                     %
%     header   File header text matrix.                                %
%     hinfo    Cast header information (matrix).                       %
%                hinfo(nsta,1):   nhvar   - number of variables        %
%                hinfo(nsta,2):   nhpts   - number of data points      %
%                hinfo(nsta,3):   castid  - cast identifier            %
%                hinfo(nsta,4):   hlng    - longitude                  %
%                hinfo(nsta,5):   hlat    - latitude                   %
%                hinfo(nsta,6):   hdpth   - maximum depth              %
%                hinfo(nsta,7):   htime   - time                       %
%                hinfo(nsta,.):   hscl(1:nhvar) - data scales          %
%                hinfo(nsta,..):  hflag   - special flag               %
%     Tavg     Average temperature profile (vector).                   %
%     Savg     Average salinity profile (vector).                      %
%     Zavg     Depths of average profile (vector).                     %
%                                                                      %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Compute some parameters for global header.
%-----------------------------------------------------------------------

lon=hinfo(:,4);
lat=hinfo(:,5);
time=hinfo(:,7);

nhdr=0;
nhobs=1;
hlng=mean(lon);
hlat=mean(lat);
htime=mean(time);

hlng_min=hlng;
hlng_max=hlng;
hlat_min=hlat;
hlat_max=hlat;

%-----------------------------------------------------------------------
%  Write out global header.
%-----------------------------------------------------------------------

%  Title.

text=header(1,:);
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%  Number of stations.

text=['stations = ' num2str(nhobs,6)];
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%  Longitude range.

text=['lng_min = ' num2str(hlng_min)];
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);
text=['lng_max = ' num2str(hlng_max)];
lstr=max(size(text));
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%  Latitude range.

text=['lat_min = ' num2str(hlat_min)];
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);
text=['lat_max = ' num2str(hlat_max)];
lstr=max(size(text));
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%  Format.

text='format = ascii, record interleaving';
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%  Instrument of data type.

text='type = CTD, averaged and fitted cast';
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%  Fields and units.

text='fields = depth, temperature, salinity';
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

text='units = meter, Celsius, PSU';
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%  Creation date.

text=['creation_date = ',date_stamp];
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%  End-of-Header.

text='END';
lstr=length(text);
nhdr=nhdr+1;
Header(nhdr,1:lstr)=text(1:lstr);

%-----------------------------------------------------------------------
%  Create HDAT file.
%-----------------------------------------------------------------------

fout=fopen(hname,'w');
if (fout<0) error(['Cannot create ' hname '.']), end

%-----------------------------------------------------------------------
%  Write out HDAT global Header.
%-----------------------------------------------------------------------

for n=1:nhdr,
  text=Header(n,:);
  lstr=max(size(text));
  fprintf(fout,'%s\n',text(1:lstr));
end

%-----------------------------------------------------------------------
%  Write out data.
%-----------------------------------------------------------------------

nhvar=3;
nhpts=length(Zavg);
hdpth=abs(max(Zavg));

hscle=[0.0001 0.0001 0.0001];
hflag=0;
htype='''CTD: z t s''';
lstr=length(htype);
frmt1='%2i %5i %7i %9.4f %8.4f %7.1f %10.4f';
frmt2='%9.2e %8.2e %8.2e\n';
frmt3='%3i %s\n';
frmt4='%7i %6i %6i %6i %6i %6i %6i %6i %6i %6i\n';

castid=1;
fprintf(fout,frmt1,nhvar,nhpts,castid,hlng,hlat,hdpth,htime);
fprintf(fout,frmt2,hscle(1:nhvar));
fprintf(fout,frmt3,hflag,htype(1:lstr));

ihdat=round((abs(Zavg)./hscle(1)));
fprintf(fout,frmt4,ihdat);
fprintf(fout,'\n');

ihdat=round((Tavg./hscle(2)));
fprintf(fout,frmt4,ihdat);
fprintf(fout,'\n');

ihdat=round((Savg./hscle(3)));
fprintf(fout,frmt4,ihdat);
fprintf(fout,'\n');

fclose(fout);

return
