%
%  This script determines the SST vertical extesnion function from
%  temperature average profile
%

IPLOT=1;

Hname='/n2/arango/NJB/Data/fsi/06jul99_02.hdat';

npoly=20;

z=0:-0.1:-30;

%-----------------------------------------------------------------------
%  Compute average cast.
%-----------------------------------------------------------------------

[Ta,Sa,Tf,Sf,Ti,Si,Tc,Sc]=avg_cast(Hname,z,npoly);    

%-----------------------------------------------------------------------
%  Fit an N-degree polynomial in a least-square sense to nondimensional
%  average profile.
%-----------------------------------------------------------------------

Tmin=min(Ta);
Tmax=max(Ta);
Tval=Tmin;
%Tval=0.5*(Tmax-Tmin);
T=(Ta-Tval)./(Tmax-Tmin);
%T=(Ta-mean(Ta))./std(Ta);

Tcoff=polyfit(z,T,npoly);
Tfit=polyval(Tcoff,z);

%-----------------------------------------------------------------------
%  Test SST extension.
%-----------------------------------------------------------------------

sst=Tmax;
Nz=length(z);
for k=1:Nz,
  cff1=Tcoff(1);
  cff2=Tc(1);
  for n=2:21
    cff1=z(k)*cff1+Tcoff(n);
    cff2=z(k)*cff2+Tc(n);
  end,
  Text1(k)=cff1*sst;
  Text2(k)=cff2*sst;
end,
  
if ( IPLOT ),
  figure;
  plot(Ta,z,'k-',Tf,z,'r-');
  set(gca,'Xlim',[6 24]); grid on;
  title('Temperature');

  figure;
  plot(T,z,'k-',Tfit,z,'r-');
  set(gca,'Xlim',[-0.1 1.1]); grid on;
  title('SST Extension Function');

  figure;
  plot(Text1,z,'k',Ta,z,'r-');
% set(gca,'Xlim',[6 24]); grid on;
  title('Extended Temperature');

end,
