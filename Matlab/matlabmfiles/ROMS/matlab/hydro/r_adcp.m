function [adcp]=r_adcp(Fname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [adcp]=r_adcp(Fname);                                       %
%                                                                      %
% This function reads in ADCP data.                                    %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Fname     ADCP filename (string).                                 %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Read in thermistor data.

data=load(Fname);
np=max(size(data));

%  Find maximun number casts and verical levels.

Km=0;
Im=0;
k=0;
i=1;
for n=1:np-1,
  if ( data(n,11) < data(n+1,11) ),
    k=k+1;
  else,
    i=i+1;
    Km=max(Km,k+1);
    k=0;
  end,
end,
Km=max(Km,k+1);
Im=i;

adcp.lon=ones([Im Km]).*NaN;
adcp.lat=ones([Im Km]).*NaN;
adcp.z=ones([Im Km]).*NaN;
adcp.u=ones([Im Km]).*NaN;
adcp.v=ones([Im Km]).*NaN;
adcp.time=ones([Im Km]).*NaN;

%  Load data into arrays of size (x,z,time).

i=1;
k=0;
for n=1:np-1,
  if ( data(n,11) < data(n+1,11) ),
    k=k+1;
    adcp.lon(i,k)=data(n,10);
    adcp.lat(i,k)=data(n,9);
    adcp.z(i,k)=-data(n,11);
    adcp.u(i,k)=data(n,14);
    adcp.v(i,k)=data(n,15);
    adcp.time(i,k)=julian(data(n,1)+1900,data(n,2),data(n,3),12)-2440000;
  else,
    k=k+1;
    adcp.lon(i,k)=data(n,10);
    adcp.lat(i,k)=data(n,9);
    adcp.z(i,k)=-data(n,11);
    adcp.u(i,k)=data(n,14);
    adcp.v(i,k)=data(n,15);
    adcp.time(i,k)=julian(data(n,1)+1900,data(n,2),data(n,3),12)-2440000;
    i=i+1;
    k=0;
  end,
end,
k=k+1;
adcp.lon(i,k)=data(np,10);
adcp.lat(i,k)=data(n,9);
adcp.z(i,k)=-data(np,11);
adcp.u(i,k)=data(np,14);
adcp.v(i,k)=data(np,15);
adcp.time(i,k)=julian(data(n,1)+1900,data(n,2),data(n,3),12)-2440000;

return
