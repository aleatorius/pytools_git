%  This script postprocess ADCP OA file and creates assimilation file.

CREATE=0;
Nlev=25;

Gname='/comop/arango/NJB/grid1/Jul98/njb1_his_08.nc';
%Aname='/comop/arango/NJB/grid1/OAG/ADCP/oag1_28jul98_01.nc';
%Aname='/comop/arango/NJB/grid1/OAG/ADCP/oag1_28jul98_02.nc';
Aname='/comop/arango/NJB/grid1/OAG/ADCP/oag1_28jul98_03.nc';

Oname='/comop/arango/NJB/grid1/Jul98/adcp_ass.nc';

%  Create Output NetCDF file.

if ( CREATE ),
  [status]=c_oaadcp(Gname,Oname,Nlev);
end,

%  Process OA data.

[status]=oa_adcp(Aname,Gname,Oname);