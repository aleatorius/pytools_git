%  This creates radials NetCDF file.

ICREATE=1;
IPLOT=1;
IWRITE=1;

spval=999;
StarJday=11001;
index=120;

Jns=8; Jne=44;
Jss=9; Jse=45;

cst_file='/d1/arango/scrum3.0/plot/Data/usgs_east.dat';

Nname='/comop/arango/NJB/Data/CODAR/radials_north.nc';
Sname='/comop/arango/NJB/Data/CODAR/radials_south.nc';

%---------------------------------------------------------------------
%  Read in coastlines file.
%---------------------------------------------------------------------

if (IPLOT),
  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=900);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;

  m_proj('Mercator');
  LonMin=-74.6;
  LonMax=-73.9;
  LatMin=39.0;
  LatMax=40.0;
end,

%---------------------------------------------------------------------
%  Read in and process Northern site radial ASCII files.
%---------------------------------------------------------------------

%  Read in ASCII file.

load /nopp/CODAR/radial_files/nsite.dat

%  Extract Northern site RADIAL data.

lon=nsite(:,1);
lat=nsite(:,2);
angle1=nsite(:,3);
angle2=nsite(:,9);
time=nsite(:,10);
bin=nsite(:,4);
vraw=nsite(:,5);
vtid=nsite(:,6);
vdet=nsite(:,7);
vfil=nsite(:,8);

ind=find(vraw  == spval); if (~isempty(ind)), vraw(ind)=NaN; end,
ind=find(vtid  == spval); if (~isempty(ind)), vtid(ind)=NaN; end,
ind=find(vdet  == spval); if (~isempty(ind)), vdet(ind)=NaN; end,
ind=find(vfil  == spval); if (~isempty(ind)), vfil(ind)=NaN; end,

clear nsite

%  Load data into polar coordinate arrays (radial,angle,time).

radius_min=min(bin);
radius_max=max(bin);
delta_radius=1.5;
In=1+(radius_max-radius_min);

angle_min=min(angle2);
angle_max=max(angle2);
delta_angle=5;
Jn=1+round((angle_max-angle_min)/delta_angle);

time_min=min(time);
time_max=max(time);
delta_time=1/24;
Kn=1+round((time_max-time_min)/delta_time);

npts=length(time);

NVraw=ones([In,Jn,Kn]).*NaN;
NVtid=ones([In,Jn,Kn]).*NaN;
NVdet=ones([In,Jn,Kn]).*NaN;
NVfil=ones([In,Jn,Kn]).*NaN;
Nlon=ones([In,Jn]).*NaN;
Nlat=ones([In,Jn]).*NaN;
Ntime=ones([1,Kn]).*NaN;

for n=1:npts;
  i=bin(n);
  j=1+round((angle2(n)-angle_min)/delta_angle);
  k=1+round((time(n)-time_min)/delta_time);
  NVraw(i,j,k)=vraw(n)/100;
  NVtid(i,j,k)=vtid(n)/100;
  NVdet(i,j,k)=vdet(n)/100;
  NVfil(i,j,k)=vfil(n)/100;
  Nlon(i,j)=lon(n);
  Nlat(i,j)=lat(n);
  Nangle(j)=angle2(n);
  Ntime(k)=StarJday+(time(n)-time_min);
end,
NKm=k;

ind=find(NVfil == spval);
NVfil(ind)=NaN;

clear angle1 angle2 bin lat lon time vraw vtid vdet vfil

%---------------------------------------------------------------------
%  Read in and process Southern site radial data. I uses the same
%  time as the Northern site.
%---------------------------------------------------------------------

%  Read in ASCII file.

load /nopp/CODAR/radial_files/ssite.dat

%  Extract Southern site RADIAL data.

lon=ssite(:,1);
lat=ssite(:,2);
angle1=ssite(:,3);
angle2=ssite(:,9);
time=ssite(:,10);
bin=ssite(:,4);
vraw=ssite(:,5);
vtid=ssite(:,6);
vdet=ssite(:,7);
vfil=ssite(:,8);

npts=length(lon);

ind=find(vraw  == spval); if (~isempty(ind)), vraw(ind)=NaN; end,
ind=find(vtid  == spval); if (~isempty(ind)), vtid(ind)=NaN; end,
ind=find(vdet  == spval); if (~isempty(ind)), vdet(ind)=NaN; end,
ind=find(vfil  == spval); if (~isempty(ind)), vfil(ind)=NaN; end,

clear ssite

%  Load data into polar coordinate arrays (radius,angle,time).

radius_min=min(bin);
radius_max=max(bin);
delta_radius=1.5;
Is=1+(radius_max-radius_min);

angle_min=min(angle2);
angle_max=max(angle2);
delta_angle=5;
Js=1+round((angle_max-angle_min)/delta_angle);

time_min=min(time);
time_max=max(time);

SVraw=ones([Is,Js,Kn]).*NaN;
SVtid=ones([Is,Js,Kn]).*NaN;
SVdet=ones([Is,Js,Kn]).*NaN;
SVfil=ones([Is,Js,Kn]).*NaN;
Slon=ones([Is,Js]).*NaN;
Slat=ones([Is,Js]).*NaN;
Stime=ones([1,Kn]).*NaN;

for n=1:npts;
  i=bin(n);
  j=1+round((angle2(n)-angle_min)/delta_angle);
  k=1+round((time(n)-time_min)/delta_time);
  SVraw(i,j,k)=vraw(n)/100;
  SVtid(i,j,k)=vtid(n)/100;
  SVdet(i,j,k)=vdet(n)/100;
  SVfil(i,j,k)=vfil(n)/100;
  Slon(i,j)=lon(n);
  Slat(i,j)=lat(n);
  Sangle(j)=angle2(n);
  Stime(k)=StarJday+(time(n)-time_min);
end,
SKm=k;

ind=find(SVfil == spval);
SVfil(ind)=NaN;

clear angle1 angle2 bin lat lon time vraw vtid vdet vfil

%---------------------------------------------------------------------
%  Plot Selected record.
%---------------------------------------------------------------------

if (IPLOT);

  Ntheta=Nangle(ones([1 In]),Jns:Jne).*pi./180;
  Stheta=Sangle(ones([1 Is]),Jss:Jse).*pi./180;

  NVr=-NVraw(:,Jns:Jne,index);
  Nu=NVr.*sin(Ntheta);
  Nv=NVr.*cos(Ntheta);
  
  SVr=-SVraw(:,Jss:Jse,index);
  Su=SVr.*sin(Stheta);
  Sv=SVr.*cos(Stheta);

  figure
  plot(Nlon(:,Jns:Jne),Nlat(:,Jns:Jne),'r');
  hold on; grid on;
  plot(Slon(:,Jss:Jse),Slat(:,Jss:Jse),'b');
  title('CODAR Radials: Northern and Southern Sites');
  set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin LatMax]);
  draw_cst(cstlon,cstlat,'k');

  figure;

  pcolor(Nlon(:,Jns:Jne),Nlat(:,Jns:Jne),NVr); shading interp; colorbar;
  title(['CODAR: Northern Site, Record = ',num2str(index)]);
  hold on; grid on;
  set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin LatMax]);
  draw_cst(cstlon,cstlat,'k');

  figure;

  pcolor(Slon(:,Jss:Jse),Slat(:,Jss:Jse),SVr); shading interp; colorbar;
  title(['CODAR: Southern Site, Record = ',num2str(index)]);
  hold on; grid on;
  set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin LatMax]);
  draw_cst(cstlon,cstlat,'k');

  figure;
  pcolor(Nlon(:,Jns:Jne),Nlat(:,Jns:Jne),NVr); shading interp; colorbar;
  hold on; grid on;
  set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin LatMax]);
  pcolor(Slon(:,Jss:Jse),Slat(:,Jss:Jse),Su); shading interp; colorbar;
  draw_cst(cstlon,cstlat,'k');
  title(['CODAR: Northern and Southern Sites, Record = ',num2str(index)]);

end,

%---------------------------------------------------------------------
%  Create Output NetCDF file.
%---------------------------------------------------------------------

if ( ICREATE ),

  [status]=c_radials(In,Jne-Jns+1,Nname);

  F=Nangle(Jns:Jne);
  [status]=nc_write(Nname,'angle',F);

  F=Nlon(:,Jns:Jne);
  ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
  [status]=nc_write(Nname,'lon',F);

  F=Nlat(:,Jns:Jne);
  ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
  [status]=nc_write(Nname,'lat',F);

  [status]=c_radials(Is,Jse-Jss+1,Sname);

  F=Sangle(Jss:Jse);
  [status]=nc_write(Sname,'angle',F);

  F=Slon(:,Jss:Jse);
  ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
  [status]=nc_write(Sname,'lon',F);

  F=Slat(:,Jss:Jse);
  ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
  [status]=nc_write(Sname,'lat',F);

end,

%---------------------------------------------------------------------
%  Write out data into NetCDF file. Notice that the radials over
%  land are removed and the radial velocity is multiplied by minus
%  one, so east velocities are positive and west velocities are
%  negative.
%---------------------------------------------------------------------

if ( IWRITE ),
 
  k=0;

  for n=1:Kn,

    if ( ~isnan(Ntime(n)) & ~isnan(Stime(n)) )
      
      k=k+1;

      [status]=nc_write(Nname,'time',Ntime(n),k);
      [status]=nc_write(Sname,'time',Stime(n),k);

      F=-NVraw(:,Jns:Jne,n); 
      ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
      [status]=nc_write(Nname,'Vraw',F,k);

      F=-SVraw(:,Jss:Jse,n);
      ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
      [status]=nc_write(Sname,'Vraw',F,k);

      F=-NVtid(:,Jns:Jne,n);
      ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
      [status]=nc_write(Nname,'Vtide',F,k);

      F=-SVtid(:,Jss:Jse,n);
      ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
      [status]=nc_write(Sname,'Vtide',F,k);

      F=-NVdet(:,Jns:Jne,n);
      ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
      [status]=nc_write(Nname,'Vdetided',F,k);

      F=-SVdet(:,Jss:Jse,n);
      ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
      [status]=nc_write(Sname,'Vdetided',F,k);

      F=-NVfil(:,Jns:Jne,n);
      ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
      [status]=nc_write(Nname,'Vradial',F,k);

      F=-SVfil(:,Jss:Jse,n);
      ind=find(isnan(F)); if (~isempty(ind)), F(ind)=spval; end
      [status]=nc_write(Sname,'Vradial',F,k);

    end,
  end,
end,
