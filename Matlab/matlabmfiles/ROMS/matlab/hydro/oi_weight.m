function [W]=oi_weight(OAname,Emod0,cor,vname,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [W]=oi_weight(OAname,Emod0,cor,vname,tindex);              %
%                                                                      %
%  This function computes OI melding weigths for the requested         %
%  OA varaiable.                                                       %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     OAname    OA NetCDF file name (string).                          %
%     Emod0     Initial model error (percentage, 0-100%).              %
%     cor       Correlation between model and observations.            %
%     vname     Variable name to process file (string).                %
%     tindex    Time index to process.                                 %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     W         OI errors and weights (structure array).               %
%                 W.Eobs => normalized observations error variance.    %
%                 W.Erms => observations RMS value.                    %
%                 W.Eobs => normalized model error variance.           %
%                 W.Cobs => observations OI melding coefficient.       %
%                 W.Cmod => model OI melding coefficient.              %
%                 W.Eoi  => expected oi error variance.                %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

%-----------------------------------------------------------------------
%  Read in OA error and RMS
%-----------------------------------------------------------------------

W.Eobs=nc_read(OAname,strcat(vname,'_err'),tindex);
W.Erms=nc_read(OAname,strcat(vname,'_rms'),tindex);

lon=nc_read(OAname,'lon_rho');
lat=nc_read(OAname,'lat_rho');

%  Normalize OA error by its variance.

W.Eobs=W.Eobs./W.Erms;

%-----------------------------------------------------------------------
%  Compute initial model error.
%-----------------------------------------------------------------------

eps=1.0e-8;
Emod0=Emod0/100;

Emin=min(min(W.Eobs));
Emax=max(max(W.Eobs));
ratio=(Emax-W.Eobs)./(Emax-Emin);
mu=ratio.*Emod0;

cff1=1-2.*mu;

fac=(cor.*cff1+sqrt(1+cff1.*cff1.*(cor*cor-1)))./ ...
    max(2-2.*mu,eps);
W.Emod=fac.*fac.*W.Eobs;

clear cff1 fac mu ratio

%-----------------------------------------------------------------------
%  Compute OI melding coefficients.
%-----------------------------------------------------------------------

cff1=cor.*sqrt(W.Eobs.*W.Emod);
cff2=W.Eobs+W.Emod-2.0d0.*cff1;
W.Cobs=(W.Emod-cff1)./max(cff2,eps);
W.Cobs=max(0.d0,min(1.d0,W.Cobs));
W.Cmod=1.d0-W.Cobs;

%-----------------------------------------------------------------------
%  Compute expected error variance.
%-----------------------------------------------------------------------

W.Eoi=W.Emod.*W.Eobs.*(1.0-cor.*cor)./ ...
      (W.Emod + W.Eobs - 2.0.*cor.*sqrt(W.Emod.*W.Eobs));

%-----------------------------------------------------------------------
%  Plot errors and melding coefficients.
%-----------------------------------------------------------------------

figure;

vmin=min(min(W.Eobs));
vmax=max(max(W.Eobs));
pcolor(lon,lat,W.Eobs); shading interp; colorbar;
title('Normalized observations Error Variance');
xlabel(['Min = ',num2str(vmin),'  Max = ',num2str(vmax)]);

figure;

vmin=min(min(W.Emod));
vmax=max(max(W.Emod));
pcolor(lon,lat,W.Emod); shading interp; colorbar;
title('Normalized Model Error Variance');
xlabel(['Min = ',num2str(vmin),'  Max = ',num2str(vmax)]);

figure;

vmin=min(min(W.Cobs));
vmax=max(max(W.Cobs));
pcolor(lon,lat,W.Cobs); shading interp; colorbar;
title('Observations OI melding coefficient');
xlabel(['Min = ',num2str(vmin),'  Max = ',num2str(vmax)]);

figure;

vmin=min(min(W.Cmod));
vmax=max(max(W.Cmod));
pcolor(lon,lat,W.Cmod); shading interp; colorbar;
title('Model OI melding coefficient');
xlabel(['Min = ',num2str(vmin),'  Max = ',num2str(vmax)]);

figure;

vmin=min(min(W.Eoi));
vmax=max(max(W.Eoi));
pcolor(lon,lat,W.Eoi); shading interp; colorbar;
title('OI Expected Error Variance');
xlabel(['Min = ',num2str(vmin),'  Max = ',num2str(vmax)]);

return
