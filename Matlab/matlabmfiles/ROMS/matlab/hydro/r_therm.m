function [therm]=r_therm(Fname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [therm]=r_therm(Fname);                                     %
%                                                                      %
% This function reads in thermistor string data.                       %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Fname     Thermistor string filename (string).                    %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Read in thermistor data.

data=load(Fname);
np=max(size(data));

%  Load data into arrays of size (x,z,time).

Im=max(data(:,7));
Km=max(data(:,8));
Nm=max(data(:,9));

for m=1:np,
  i=data(m,7);
  k=data(m,8);
  n=data(m,9); 
  therm.lon(i,k)=data(m,1);
  therm.lat(i,k)=data(m,2);
  therm.z(i,k)=-data(m,3);
  therm.t(i,k,n)=data(m,4);
  therm.s(i,k,n)=data(m,5);
  therm.time(n)=data(m,6);
end,

%  In July 2000, use the following equation for salinity:
%
%     s = - 0.1957 * t + 35.034
%

therm.s = - 0.1957 .* therm.t + 35.034;

return


