%
%  This script generate initial conditions from an average HDAT profile.
%

IPLOT=1;

Hname='/coamps/fsi/Jul00/hdat/10jul00_01.hdat';
Hname='/coamps/fsi/Jul00/hdat/10jul00_02.hdat';
Iname='/n7/arango/NJB/Jul00/Data/njb1_ini_jul09.nc';
Gname='/n7/arango/NJB/Jul00/Data/njb1_grid_a.nc';

Tini=11735.0;

npoly=20;

z=0:-0.1:-30;
Nz=length(z);

%-----------------------------------------------------------------------
%  Compute average cast for each survey.
%-----------------------------------------------------------------------

[Ta,Sa,Tf,Sf,Ti,Si]=avg_cast(Hname,z,npoly);    

if ( IPLOT ),
  figure; plot(Ta,z,'k-',Tf,z,'r-');
  set(gca,'Xlim',[6 24]); grid on;
  title(['Temperature: ',Hname]);
  figure; plot(Sa,z,'k-',Sf,z,'r-'); grid on;
  title(['Salinity: ',Hname]);
  set(gca,'Xlim',[28 32]); grid on;
end,

%-----------------------------------------------------------------------
%  Compute model depths at RHO-points.
%-----------------------------------------------------------------------

z(Nz+1)=-105;
Tf(Nz+1)=Tf(Nz);
Sf(Nz+1)=Sf(Nz);

Zr=depths(Iname,Gname,1,0,0);
[Lp,Mp,N]=size(Zr);

%-----------------------------------------------------------------------
%  Interpolate initial temperature and salinity from average profile
%  to model grid.
%-----------------------------------------------------------------------

nbot=length(Ta);

Tc=[Ta Tf(nbot)];
Sc=[Sa Sf(nbot)];

for j=1:Mp,
  for i=1:Lp,
    Zi=reshape(Zr(i,j,:),1,N);
    t(i,j,:)=interp1(z,Tc,Zi,'spline');
    s(i,j,:)=interp1(z,Sc,Zi,'spline');
  end,
end,
   
%-----------------------------------------------------------------------
%  Write out Potential temperature and Salinity into initial NetCDF.
%-----------------------------------------------------------------------

status=nc_write(Iname,'ocean_time',Tini*86400,1);

status=nc_write(Iname,'temp',t,1);
status=nc_write(Iname,'salt',s,1);

%-----------------------------------------------------------------------
%  Zero-out free-surface and momentum.
%-----------------------------------------------------------------------

[F]=nc_read(Iname,'zeta',1);
F=zeros(size(F));
status=nc_write(Iname,'zeta',F,1);

[F]=nc_read(Iname,'ubar',1);
F=zeros(size(F));
status=nc_write(Iname,'ubar',F,1);

[F]=nc_read(Iname,'vbar',1);
F=zeros(size(F));
status=nc_write(Iname,'vbar',F,1);

[F]=nc_read(Iname,'u',1);
F=zeros(size(F));
status=nc_write(Iname,'u',F,1);

[F]=nc_read(Iname,'v',1);
F=zeros(size(F));
status=nc_write(Iname,'v',F,1);



