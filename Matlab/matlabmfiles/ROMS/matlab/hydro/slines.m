%  This script plots locations of hydrographic sections.

global IPRINT
IPRINT=0;

READ_COAST=0;

cst_file='/home/arango/ocean/plot/Data/usgs_east.dat';
grd_file='/n2/arango/NJB/Jul99/Data/njb1_grid_a.nc';

spval=900.0;
LonMin=-75;
LonMax=-73;
LatMin=39;
LatMax=40;

Nlon(1)=-(74.0+12.7/60.0);
Nlon(2)=-(74.0+ 2.0/60.0);
Nlat(1)=39.0+32.9/60.0;
Nlat(2)=39.0+25.9/60.0;

Alon(1)=-(74.0+14.7/60.0);
Alon(2)=-(74.0+ 5.2/60.0);
Alat(1)=39.0+27.7/60.0;
Alat(2)=39.0+20.6/60.0;

Slon(1)=-(74.0+21.0/60.0);
Slon(2)=-(74.0+10.4/60.0);
Slat(1)=39.0+23.9/60.0;
Slat(2)=39.0+16.9/60.0;

In(1)=1;
In(2)=35;
Jn(1)=99;
Jn(2)=99;

Ia(1)=1;
Ia(2)=35;
Ja(1)=87;
Ja(2)=87;

Is(1)=1;
Is(2)=35;
Js(1)=74;
Js(2)=74;

%-------------------------------------------------------------------------
%  Read model grid
%-------------------------------------------------------------------------

rlon=nc_read(grd_file,'lon_rho');
rlat=nc_read(grd_file,'lat_rho');

%-------------------------------------------------------------------------
%  Initialize Mercator Map.
%-------------------------------------------------------------------------

if (READ_COAST),
  m_proj('Mercator','long',[LonMin LonMax],'lat',[LatMin LatMax]);

  [cstlon,cstlat]=rcoastline(cst_file);
  [icst]=find(cstlon>=spval);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  [Xcst,Ycst]=m_ll2xy(cstlon,cstlat,'point');

  [Xmin,Ymin]=m_ll2xy(LonMin,LatMin);
  [Xmax,Ymax]=m_ll2xy(LonMax,LatMax);

  [X,Y]=m_ll2xy(rlon,rlat);
end,

[Xn,Yn]=m_ll2xy(Nlon,Nlat);
[Xa,Ya]=m_ll2xy(Alon,Alat);
[Xs,Ys]=m_ll2xy(Slon,Slat);

%-------------------------------------------------------------------------
%  Draw map.
%-------------------------------------------------------------------------

figure(1)
set(gca,'xlim',[Xmin Xmax],'ylim',[Ymin Ymax],...
        'Xtick',[],'Ytick',[]);
hold on;
draw_cst(Xcst,Ycst,'k');
m_grid('xtick',[LonMin:0.2:LonMax],'ytick',[LatMin:0.2:LatMax]);
set(gcf,'Units','Normalized',...
        'Position',[0.2 0.1 0.6 0.8],...
        'PaperUnits','Normalized',...
        'PaperPosition',[0 0 1 1]);

xn=X(In(1):In(2),Jn(1):Jn(2));
yn=Y(In(1):In(2),Jn(1):Jn(2));
plot(Xn,Yn,'r+-');
plot(xn,yn,'r');

xa=X(Ia(1):Ia(2),Ja(1):Ja(2));
ya=Y(Ia(1):Ia(2),Ja(1):Ja(2));
plot(Xa,Ya,'g+-');
plot(xa,ya,'g');

xs=X(Is(1):Is(2),Js(1):Js(2));
ys=Y(Is(1):Is(2),Js(1):Js(2));
plot(Xs,Ys,'b+-');
plot(xs,ys,'b');
