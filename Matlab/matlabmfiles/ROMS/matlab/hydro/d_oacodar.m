%  This script postprocess CODAR OA file and creates assimilation file.

CREATE=0;

Gname='/comop/arango/NJB/grid1/njb1_grid_a.nc';
%Cname='/comop/arango/NJB/grid1/OAG/CODAR/oag1_jul01.nc';
Cname='/comop/arango/NJB/grid1/OAG/CODAR/oag1_jul16.nc';
%Cname='/comop/arango/NJB/grid1/OAG/CODAR/oag1_jul22.nc';
%Cname='/comop/arango/NJB/grid1/OAG/CODAR/oag1_jul24.nc';
%Cname='/comop/arango/NJB/grid1/OAG/CODAR/oag1_jul27.nc';
%Cname='/comop/arango/NJB/grid1/OAG/CODAR/oag1_jul28.nc';
Oname='/comop/arango/NJB/grid1/Jul98/codar_ass.nc';

%  Create Output NetCDF file.

if ( CREATE ),
  [status]=c_oacodar(Gname,Oname);
end,

%  Process OA data.

[status]=oa_codar(Cname,Gname,Oname);