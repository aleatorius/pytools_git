function [T,S]=sst_proj(fname,SST);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [T]=sst_proj(fname,SST)                                     %
%                                                                      %
% This function computes model 3D temperature by projecting IR sea     %
% surface temperature on a hyperbolic profile.  The salinity is        %
% assumed to vary linearly with temperature.                           %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    fname       History or restart NetCDF file name (string).         %
%    SST         Sea surface temperature at model grid (matrix).       %
%    Tcast       Temperature vertical profile (vector).                %
%    Zcast       Depths of vertical profile (vector).                  %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    T           3D model temperature (array).                         %
%    S           3D model salinity (array).                            %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='linear';

%-----------------------------------------------------------------------
%  Compute model depths at RHO-points.
%-----------------------------------------------------------------------

Zr=depths(fname,1,0,0);
[Lp,Mp,N]=size(Zr);

%-----------------------------------------------------------------------
%  Compute temperature. Use hyperbolic Function.
%-----------------------------------------------------------------------

a=SST*1.5/7+9.715;
b=SST*5.5/7-9.7;
c=SST*0.4/7-0.443;
d=SST*8/7-12.85; 

for k=1:N,
  for i=1:Lp,
    T(i,:,k)=a(i,:)+b(i,:).*tanh(c(i,:).*Zr(i,:,k)+d(i,:));
  end,
end,

%-----------------------------------------------------------------------
%  Compute salinity. Use hyperbolic function and  make coefficients in
%  expression to vary linearly with SST.
%-----------------------------------------------------------------------

a=31.35;
b=SST*0.5/7-0.88;
c=SST*0.4/7-0.443;
d=SST*8/7-12.85; 

for k=1:N,
  for i=1:Lp,
    S(i,:,k)=a-b(i,:).*tanh(c(i,:).*Zr(i,:,k)+d(i,:));
  end,
end,

return
