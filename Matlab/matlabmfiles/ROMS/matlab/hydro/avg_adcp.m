function [Uavg,Vavg,Ufit,Vfit,Ui,Vi]=avg_cast(hname,z,npoly);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [Uavg,Vavg,Ufit,Vfit,Ui,Vi]=avg_cast(hname,z,npoly)        %
%                                                                      %
%  This function reads a HDAT file and interpolates ADCP data to       %
%  specified depths.  Then,  it computes average cast and fit  a       %
%  n-degree polynomial in a least-squares sense.                       %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hname    HDAT file name (string).                                %
%     z        Depths to interpolate (vector; meters, negative).       %
%     npoly    Degree of polynomial to fit (integer).                  %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     Uavg     Average U-velocity cast.                                %
%     Vavg     Average V-velocity cast.                                %
%     Ufit     Average, least-squares fitted, U-velocity cast.         %
%     Vfit     Average, least-squares fitted, V-velocity cast.         %
%     Ui       Vertically interpolated U-velocity data.                %
%     Vi       Vertically interpolated V-velocity data.                %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

method='cubic';

%-----------------------------------------------------------------------
%  Read in HDAT file.
%-----------------------------------------------------------------------

[header,hinfo,htype,hdata]=rhydro(hname);

[nhvar,nhpts,nhsta]=size(hdata);

%-----------------------------------------------------------------------
%  Extract CTD data from HDATA array
%-----------------------------------------------------------------------

P=reshape(hdata(1,:,:),nhpts,nhsta);
U=reshape(hdata(2,:,:),nhpts,nhsta);
V=reshape(hdata(3,:,:),nhpts,nhsta);

%-----------------------------------------------------------------------
%  Interpolate casts to a regular grid.
%-----------------------------------------------------------------------

%  Replicate cast data.

Pwrk=-P;
Uwrk=U;
Vwrk=V;
Xwrk=repmat(1:nhsta,nhpts,1);

%  Remove null data.

ind=find(Uwrk == 0);
Xwrk(ind)=[];
Pwrk(ind)=[];
Uwrk(ind)=[];
Vwrk(ind)=[];

%  Set regular grid and interpolate data.

[Xi,Pi]=meshgrid(1:nhsta,z);
Ui=griddata(Xwrk,Pwrk,Uwrk,Xi,Pi,method);
Vi=griddata(Xwrk,Pwrk,Vwrk,Xi,Pi,method);

%-----------------------------------------------------------------------
%  Compute average cast.
%-----------------------------------------------------------------------

Nz=length(z);
ic=0;
for n=1:Nz,
  Uwrk=Ui(n,:);
  Vwrk=Vi(n,:);
  ind=find(isnan(Uwrk));
  Uwrk(ind)=[];
  Vwrk(ind)=[];
  if (~isempty(Uwrk)),
    Uavg(n)=mean(Uwrk);
    Vavg(n)=mean(Vwrk);
    ic=ic+1;
    iz(ic)=n;
  else
    Uavg(n)=NaN;
    Vavg(n)=NaN;
  end,
end,

%-----------------------------------------------------------------------
%  Fill Average cast with surface and bottom values.
%-----------------------------------------------------------------------

nsur=iz(1);
nbot=iz(ic);

Usur=Uavg(nsur);
Vsur=Vavg(nsur);

Ubot=0;
Vbot=0;

Uavg(1:nsur-1)=Usur;
Vavg(1:nsur-1)=Vsur;

Uavg(nbot+1:Nz)=Ubot;
Vavg(nbot+1:Nz)=Vbot;

%-----------------------------------------------------------------------
%  Fit an N-degree polynomial in a least-square sense.
%-----------------------------------------------------------------------

Ucoff=polyfit(z,Uavg,npoly);
Ufit=polyval(Ucoff,z);

Vcoff=polyfit(z,Vavg,npoly);
Vfit=polyval(Vcoff,z);

%-----------------------------------------------------------------------
%  Write average profile into a HDAT file.
%-----------------------------------------------------------------------

oname=[hname,'_avg'];
wrt_avg(oname,header,hinfo,Uavg,Vavg,z);

return
