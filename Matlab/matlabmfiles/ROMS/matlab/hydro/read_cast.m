function [hinfo,htype,hdata,error,message]=read_cast(hid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  function [hinfo,htype,hdata,error,message]=read_cast(hid)           %
%                                                                      %
%  This function reads in cast from a hydrography HDAT file.           %
%                                                                      %
%  On Input:                                                           %
%                                                                      %
%     hid      Opened file unit (integer).                             %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     hinfo    Cast header information (matrix).                       %
%                hinfo(nsta,1):   nhvar   - number of variables        %
%                hinfo(nsta,2):   nhpts   - number of data points      %
%                hinfo(nsta,3):   castid  - cast identifier            %
%                hinfo(nsta,4):   hlng    - longitude                  %
%                hinfo(nsta,5):   hlat    - latitude                   %
%                hinfo(nsta,6):   hdpth   - maximum depth              %
%                hinfo(nsta,7):   htime   - time                       %
%                hinfo(nsta,.):   hscl(1:nhvar) - data scales          %
%                hinfo(nsta,..):  hflag   - special flag               %
%     htype    hydrography type identification (string matrix).        %
%     hdata    hydrography field data (array).                         %
%     error    Error flag (integer).                                   %
%     message  Error message (string).                                 %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
% Read hinfo of cast header.  Check for error.
%-----------------------------------------------------------------------

[hinfo count]=fscanf(hid,'%d %d %d %f %f %f %f',7);
[message error]=ferror(hid);

if (error==0),
  nhvar=hinfo(1);
  nend=7+nhvar;
  [hinfo(8:nend) count]=fscanf(hid,'%e',nhvar);
  [message error]=ferror(hid);
end,

if (error==0),
  nend=8+nhvar;
  [hinfo(nend) count]=fscanf(hid,'%d',1);
  [message error]=ferror(hid);
end,

if (error==0),
  htype=fgets(hid);
  [message error]=ferror(hid);
end,


%-----------------------------------------------------------------------
% If still no errors, read in casts and convert data.
%-----------------------------------------------------------------------

if (error == 0),
  nhpts=hinfo(2);
  for n=1:nhvar,
    [f count]=fscanf(hid,'%d',nhpts);
    [message error]=ferror(hid);
    if (error == 0),
      scle=hinfo(7+n);
      hdata(n,1:nhpts)=f(1:nhpts)'.*scle;
    end,
  end,
end,

return
