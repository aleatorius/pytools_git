function theHandles = tripatch(tri, x, y, z)

% tripatch -- Plot triangles w/r to sense.
%  tripatch(tri, x, y, z) plots the triangles tri,
%   using green for counter-clockwise sense and
%   red for clockwise.  The patch-handles are
%   returned.
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 22-Jul-1999 13:31:09.
% Updated    22-Jul-1999 14:09:59.

red = [1 0 0];
green = [0 1 0];
blue = [0 0 1];
white = red + blue + green;
black = 0*white;

[m, n] = size(tri);

theSense = trisense(tri, x, y);
ccw = find(theSense > 0);
cw = find(theSense < 0);

h = [NaN; NaN];

wasHold = ishold;

if any(cw)
	t = tri(cw, :).';
	if nargin < 4
		h(2) = patch(x(t), y(t), red, 'EdgeColor', black);
	else
		h(2) = patch(x(t), y(t), z(t), red, 'EdgeColor', black);
	end
	hold on
end

if any(ccw)
	t = tri(ccw, :).';
	if nargin < 4
		h(1) = patch(x(t), y(t), green, 'EdgeColor', white);
	else
		h(1) = patch(x(t), y(t), z(t), green, 'EdgeColor', white);
	end
end

if ~isnan(h(2))
	set(h(2), 'EdgeColor', black)
end

if ~isnan(h(1))
	set(h(1), 'EdgeColor', white)
end

if ~wasHold, hold off, end

if nargout > 0, theHandles = h; end
