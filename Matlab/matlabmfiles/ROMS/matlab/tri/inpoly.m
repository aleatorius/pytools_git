function theResult = inpoly(x, y, xi, yi)

% inpoly -- In-polygon function, based on triangles.
%  inpoly(x, y, xi, yi) returns a logical vector that
%   describes whether the (xi, yi) points are inside
%   (TRUE) or outside (FALSE) the polygon given by
%   the (x, y) perimeter.
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 12-Jul-1999 19:51:14.
% Updated    12-Jul-1999 19:51:14.

if nargout > 0, theResult = []; end

if nargin < 4, help(mfilename), return, end

if x(1) == x(end) & y(1) == y(end)
	x(end) = [];
	y(end) = [];
end

[xp, yp, tp] = tripoly(x, y);

t = tsearch(xp, yp, tp, xi, yi);
t(isnan(t)) = 0;

s = (trisense(tp, xp, yp) > 0);

result = zeros(size(t));
result(t ~= 0) = s(t(t ~= 0));

if nargout > 0
	theResult = result;
else
	disp(result)
	assignin('caller', 'ans', result)
end
