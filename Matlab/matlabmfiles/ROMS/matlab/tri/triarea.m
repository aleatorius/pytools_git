function theResult = triarea(tri, x, y)

% triarea -- Signed areas of triangles.
%  triarea(tri, x, y) returns the areas of the
%   triangles tri, whose vertices are (x, y),
%   such as returned by "delaunay".
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 29-Apr-1999 08:20:50.
% Updated    09-Jul-1999 15:33:15.

if nargin < 1, help(mfilename), return, end

t = tri(:, [1 2 3 1]);

x = x(t);
y = y(t);

result = 0.5 .* (x(:, 1:3).*y(:, 2:4) - x(:, 2:4).*y(:, 1:3)) * ones(3, 1);

if nargout > 0
	theResult = result;
else
	disp(result)
end
