function theResult = isconvex(x, y)

% isconvex -- Is polygon (x, y) convex?
%  isconvex(x, y) returns TRUE if the (x, y) polygon
%   is convex (TRUE); otherwise, FALSE.
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 12-Jul-1999 20:35:58.
% Updated    12-Jul-1999 20:35:58.

if nargin < 1, help(mfilename), return, end

if x(1) == x(end) & y(1) == y(end)
	x(end) = [];
	y(end) = [];
end

tri = delaunay(x, y);

h = trihull(tri);

result = (length(h) == length(x));

if nargout > 0
	theResult = result;
else
	disp(result)
	assignin('caller', 'ans', result)
end
