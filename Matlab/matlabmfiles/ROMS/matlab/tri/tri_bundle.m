function tri_bundle
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 09-Jul-1999 15:39:46.
% Updated    27-Jul-1999 08:03:16.

setdef(mfilename)

dst = 'tri_install';
target = '';

delete([dst '.m'])
delete([dst '.p'])

bundle(dst, target)

% Get full bundle name and trim the extension.

dst = which(dst);
f = find(dst == '.');
if any(f), dst(f(end):end) = ''; end

% Bundle the routines.

bundle(dst, 'makedir', target)
bundle(dst, 'newfolder.mac', target, 'binary')
bundle(dst, 'eval', target, 'makedir tri')
bundle(dst, 'eval', target, 'cd tri')

bundle(dst, 'tri_bundle', target)

bundle(dst, 'triage', target)
bundle(dst, 'triarea', target)
bundle(dst, 'triclean', target)
bundle(dst, 'tridemo', target)
bundle(dst, 'trihull', target)
bundle(dst, 'tripatch', target)
bundle(dst, 'tripoly', target)
bundle(dst, 'trisense', target)
bundle(dst, 'trisort', target)
bundle(dst, 'triterp', target)

bundle(dst, 'areaxy', target)
bundle(dst, 'inpoly', target)
bundle(dst, 'isconvex', target)
bundle(dst, 'uniq', target)
bundle(dst, 'zoomsafe', target)

bundle(dst, 'eval', target, 'cd ..')

bundle(dst, 'disp', target, ' ')
bundle(dst, 'disp', target, ' ## Before using, include the "tri"')
bundle(dst, 'disp', target, ' ##  folder in your Matlab path.')
bundle(dst, 'disp', target, ' ##  Then, run "tridemo" a few times.')

% P-coding.

disp([' ## P-Coding ' dst ' ...'])
pcode(dst)
