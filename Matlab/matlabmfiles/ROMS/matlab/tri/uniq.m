function [theResult, indices] = uniq(x)

% uniq -- Delete duplicated rows in matrix.
%  uniq(x) deletes duplicated rows from matrix x.
%   Unlike the Matlab "unique" routine, the present
%   function does not rearrange the order of rows.
%  [y, indices] = unique(x) also returns indices
%   such that y = x(indices, :).
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 05-May-1999 09:59:50.
% Updated    20-Jul-1999 11:04:23.

[m, n] = size(x);
oldm = m;
if oldm == 1, x = x.'; end
[m, n] = size(x);

xout = x;

indices = (1:m).';
for j = n:-1:1
	[ignore, ind] = sort(x(:, j));
	x = x(ind, :);
	indices = indices(ind);
end

d = diff(x);

if n == 1
	f = find(d == 0);
else
	f = find(all(d.' == 0).');
end

if any(f)
	indices(f+1) = [];
	indices = sort(indices);
	xout = xout(indices, :);
end

if oldm == 1, xout = xout.'; end

if nargout > 0
	theResult = xout;
else
	disp(xout)
end
