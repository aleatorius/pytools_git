function tridemo(nPoints)

% tridemo -- Demonstration of "triage".
%  tridemo(nPoints) shows the result of applying
%   "triage" to a non-convex set of triangles,
%   based on nPoints random points (default = 10).
%   The original triangles are displayed in "green";
%   the added ones are in "red"; and circles show
%   the locations of added "nanified" points.
%   Clicking on any triangle will cause the demo
%   to repeat itself with different points.
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 12-Jul-1999 08:13:31.
% Updated    12-Jul-1999 16:52:44.

if nargin < 1, help(mfilename), nPoints = 'demo'; end
if isequal(nPoints, 'demo'), nPoints = 10; end
if ischar(nPoints), nPoints = eval(nPoints); end

overlapping = 1;

while overlapping
	x = rand(nPoints, 1);
	y = rand(nPoints, 1);
	tri = delaunay(x, y);
	
	index = 0;
	while any(index < 1 | index > nPoints)
		remove = ceil(sqrt(nPoints));
		index = fix(nPoints * rand(1, remove)) + 1;
	end
	
	tri(index, :) = [];
	
	s = trisense(tri, x, y);
	tri(s < 0, :) = fliplr(tri(s < 0, :));
	
	[th, overlapping] = trihull(tri);
	segments = sum(isnan(th)) + 1;
end

[to, xo, yo, zo] = triage(tri, x, y);

s = trisense(to, xo, yo);
cw = (s < 0);
ccw = (s > 0);

delete(get(gca, 'Children'))

hold off
if any(cw)
	h = trimesh(to(cw, :), xo, yo, 0*xo);
	set(h, 'FaceColor', [1 0 0])
	set(h, 'ButtonDownFcn', ['tridemo ' int2str(nPoints)])
	f = find(isnan(zo));
	if any(f)
		hold on
		plot(xo(f), yo(f), 'ko')
		hold off
	end
end
hold on
if any(ccw)
	h = trimesh(to(ccw, :), xo, yo, 0*xo);
	set(h, 'FaceColor', [0 1 0])
	set(h, 'ButtonDownFcn', ['tridemo ' int2str(nPoints)])
end
title(['tridemo ' int2str(nPoints)])
hold off
view(2)
zoomsafe
figure(gcf)
drawnow
