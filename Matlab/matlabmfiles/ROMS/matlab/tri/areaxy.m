function theResult = areaxy(x, y)

% areaxy -- Signed-area of polygon (x, y).
%  areaxy(x, y) returns the signed-area of the
%   polygon given by the (x, y) vertices.  The
%   sign is positive for the counter-clockwise
%   sense; negative for clockwise.  (The Matlab
%   "polyarea" result is unsigned.)
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 08-Jul-1999 22:00:38.
% Updated    08-Jul-1999 22:00:38.

if nargin < 1, help(mfilename), return, end

result = 0;

if ~isempty(x) & length(x) == length(y)
	x = [x(:); x(1)];
	y = [y(:); y(1)];
	i = 1:length(x)-1;
	result = 0.5 * sum((x(i) .* y(i+1) - y(i) .* x(i+1)));
end

if nargout > 0
	theResult = result;
else
	disp(result)
	assignin('caller', 'ans', result)
end
