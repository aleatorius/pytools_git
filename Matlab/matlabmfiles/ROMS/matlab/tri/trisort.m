function theResult = trisort(tri)

% trisort -- Sort a triangular tessellation.
%  trisort(tri) sorts the indices of tri,
%   a triangular tessellation such as from
%   the "delaunay" function.  The rows are
%   sorted and then arranged in order of
%   ascending first index.
 
% Copyright (C) 1999 Dr. Charles R. Denham, ZYDECO.
%  All Rights Reserved.
%   Disclosure without explicit written consent from the
%    copyright owner does not constitute publication.
 
% Version of 29-Apr-1999 08:00:44.

if nargin < 1, help(mfilename), return, end

result = sort(tri.').';
for j = 3:-1:1
	[ignore, indices] = sort(result(:, j));
	result = result(indices, :);
end

if nargout > 0
	theResult = result;
else
	disp(result)
end
