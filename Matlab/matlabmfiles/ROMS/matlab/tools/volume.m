function [CellVol,TotalVol]=volume(fname,gname,Tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2004 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [cell,volume]=volume(fname,gname,Tindex)                         %
%                                                                           %
% This function computes grid cell volume.                                  %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    fname       NetCDF data file name (character string).                  %
%    gname       NetCDF grid file name (character string).                  %
%    Tindex      Time index to process (integer).                           %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    CellVol     Grid cell volume (array; cubic meters).                    %
%    TotalVol    Total grid volume (scalar; cubic meters).                  %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Get horizontal grid spacing.

pm=nc_read(gname,'pm');
pn=nc_read(gname,'pn');

% Compute grid thickneses.

zw=depths(fname,gname,5,0,Tindex);
[Lp,Mp,Np]=size(zw);
N=Np-1;
dz=(zw(1:Lp,1:Mp,2:Np)-zw(1:Lp,1:Mp,1:N));

% Compute grid cell volume.

dx=1./pm(:,:,ones([1 N]));
dy=1./pn(:,:,ones([1 N]));

CellVol=dx.*dy.*dz;

TotalVol=sum(sum(sum(CellVol)));

return
