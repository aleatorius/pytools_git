function [A]=avg_obs(Iname,Vname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2004 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [A]=avg_obs(Iname,Vname)                                         %
%                                                                           %
% This function computes the time average of requested variable.            %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Iname       Input NetCDF variable name.                                %
%    Vname       NetCDF variable name.                                      %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    A           Requested time-averaged field.                             %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Inquire number of time records.

[dnames,dsizes,igrid]=nc_vinfo(Iname,Vname);
ndims=length(dsizes);

for n=1:ndims,
  name=deblank(dnames(n,:));
  switch name
    case 'time',
      Nrec=dsizes(n);
  end,
end,

% Read in field and compute time-average.

A=nc_read(Iname,Vname,1);

for n=2:Nrec,
  f=nc_read(Iname,Vname,n);
  A=A+f;
end,

A=A./Nrec;

return
