function [V]=global_var(Iname,Vname,A);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2004 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [V]=var_obs(Iname,Vname,A)                                       %
%                                                                           %
% This function computes the global (time and space) variance of requested  %
% variable.                                                                 %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Iname       Input NetCDF variable name.                                %
%    Vname       NetCDF variable name.                                      %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    V           Requested field variance (squared field units).            %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Inquire number of time records.

[dnames,dsizes,igrid]=nc_vinfo(Iname,Vname);
ndims=length(dsizes);

for n=1:ndims,
  name=deblank(dnames(n,:));
  switch name
    case 'time',
      Nrec=dsizes(n);
  end,
end,

% Read in field and compute time-average.

V=nc_read(Iname,Vname,1);
V=zeros(size(V));

for n=2:Nrec,
  f=nc_read(Iname,Vname,n);
  V=V+(f-A).^2;
end,

V=V(:);

Npts=(Nrec-1)*length(V);
V=sum(V)/(Npts-1);

return
