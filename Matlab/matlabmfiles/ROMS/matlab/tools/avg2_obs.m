function [A2]=avg2_obs(Iname,Vname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2004 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [A2]=avg_obs(Iname,Vname)                                        %
%                                                                           %
% This function computes the time average of requested variable squared     %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    Iname       Input NetCDF variable name.                                %
%    Vname       NetCDF variable name.                                      %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    A2           Requested time-averaged field squared.                    %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Inquire number of time records.

[dnames,dsizes,igrid]=nc_vinfo(Iname,Vname);
ndims=length(dsizes);

for n=1:ndims,
  name=deblank(dnames(n,:));
  switch name
    case 'time',
      Nrec=dsizes(n);
  end,
end,

% Read in field and compute time-average.

A2=nc_read(Iname,Vname,1);
A2=A2.^2;

for n=2:Nrec,
  f=nc_read(Iname,Vname,n);
  A2=A2+f.^2;
end,

A2=A2./Nrec;

return
