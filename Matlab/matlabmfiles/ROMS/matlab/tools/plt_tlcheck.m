function []=plt_tlcheck(FWDprefix,TLMprefix,Iter,Nrec,Vname,Lev)

% []=plt_tlcheck(FWDprefix,TLMprefix,Iter,Nrec,Vname,Lev)


% Read in nonlinear variable, last written record.

Fname=[FWDprefix,'.nc'];
NLV=nc_read(Fname,Vname,Nrec);
if (nargin > 5), 
  NLV=squeeze(NLV(:,:,Lev));
end,

% Read in perturbed nonlinear variable, last written record.

Fname=[FWDprefix,'_',num2str(Iter,'%3.3i'),'.nc'];
nlv=nc_read(Fname,Vname,Nrec);
if (nargin > 5), 
  nlv=squeeze(nlv(:,:,Lev));
end,

% Read in perturbed tangent linear variable, last written record.

Fname=[TLMprefix,'_',num2str(Iter,'%3.3i'),'.nc'];
tlv=nc_read(Fname,Vname,Nrec);
if (nargin > 5), 
  tlv=squeeze(tlv(:,:,Lev));
end,

% Plot variable.

figure(1);

pcolor(tlv'); shading flat; colorbar;
title(['tl\_',Vname,': Iterarion ',num2str(Iter)])

figure(2);
pcolor((nlv-NLV)'); shading flat; colorbar;
title([Vname,'-',Vname,'0: Iterarion ',num2str(Iter)])

figure(3);
pcolor((tlv-(nlv-NLV))'); shading flat; colorbar;
title(['tl\_',Vname,'-(',Vname,'-',Vname,'0): Iterarion ',num2str(Iter)])

return