% This script computes the rms difference between 
% perturbed and unperturbed runs of NLROMS, and
% compares them with rms fields of TLROMS initialized
% with the same perturbation and linearized about
% the unperturbed NLROMS.

% Read unperturbed NLROMS fields.

inpc='../Work1/roms_fwd_1.nc';
inpp='roms_his+pert-10day-6sv-amp60.nc';
inpt='roms_tlm-6sv.nc';

amp=6.e1;

% determine number of record.

time=nc_read(inpc,'ocean_time');
nrec=length(time);

% Process data record by record

for n=1:nrec,
  
  zetac=nc_readmasked(inpc,'zeta',n);
  zetac=zetac(:); ind=isnan(zetac); zetac(ind)=[];
  
  ubarc=nc_readmasked(inpc,'ubar',n);
  ubarc=ubarc(:); ind=isnan(ubarc); ubarc(ind)=[];

  vbarc=nc_readmasked(inpc,'vbar',n);
  vbarc=vbarc(:); ind=isnan(vbarc); vbarc(ind)=[];

  tempC=nc_readmasked(inpc,'temp',n);
  tempc=tempC(:); ind=isnan(tempc); tempc(ind)=[];
  ind=isnan(tempC); tempC(ind)=0;
  
  saltC=nc_readmasked(inpc,'salt',n);
  saltc=saltC(:); ind=isnan(saltc); saltc(ind)=[];
  ind=isnan(saltC); saltC(ind)=0;

  uC=nc_readmasked(inpc,'u');
  uc=uC(:); ind=isnan(uc); uc(ind)=[];
  ind=isnan(uC); uC(ind)=0;
  
  vC=nc_readmasked(inpc,'v');
  vc=vC(:); ind=isnan(vc); vc(ind)=[];
  ind=isnan(vC); vC(ind)=0;
  
% Read perturbed NLROMS fields.

  zetap=nc_readmasked(inpp,'zeta',n);
  zetap=zetap(:); ind=isnan(zetap); zetap(ind)=[];
  
  ubarp=nc_readmasked(inpp,'ubar',n);
  ubarp=ubarp(:); ind=isnan(ubarp); ubarp(ind)=[];

  vbarp=nc_readmasked(inpp,'vbar',n);
  vbarp=vbarp(:); ind=isnan(vbarp); vbarp(ind)=[];

  tempP=nc_readmasked(inpp,'temp',n);
  tempp=tempP(:); ind=isnan(tempp); tempp(ind)=[];
  ind=isnan(tempP); tempP(ind)=0;
  
  saltP=nc_readmasked(inpp,'salt',n);
  saltp=saltP(:); ind=isnan(saltp); saltp(ind)=[];
  ind=isnan(saltP); saltP(ind)=0;

  uP=nc_readmasked(inpp,'u');
  up=uP(:); ind=isnan(up); up(ind)=[];
  ind=isnan(uP); uP(ind)=0;

  vP=nc_readmasked(inpp,'v');
  vp=vP(:); ind=isnan(vp); vp(ind)=[];
  ind=isnan(vP); vP(ind)=0;

% Read perturbed TLROMS fields.

  zetat=nc_readmasked(inpt,'zeta',n);
  zetat=zetat(:).*amp; ind=isnan(zetat); zetat(ind)=[];
  
  ubart=nc_readmasked(inpt,'ubar',n);
  ubart=ubart(:).*amp; ind=isnan(ubart); ubart(ind)=[];

  vbart=nc_readmasked(inpt,'vbar',n);
  vbart=vbart(:).*amp; ind=isnan(vbart); vbart(ind)=[];

  tempT=nc_readmasked(inpt,'temp',n);
  tempt=tempT(:).*amp; ind=isnan(tempt); tempt(ind)=[];
  ind=isnan(tempT); tempT(ind)=0;
  
  [saltT]=nc_readmasked(inpt,'salt',n);
  saltt=saltT(:).*amp; ind=isnan(saltt); saltt(ind)=[];
  ind=isnan(saltT); saltT(ind)=0;

  [uT]=nc_readmasked(inpt,'u');
  ut=uT(:).*amp; ind=isnan(ut); ut(ind)=[];
  ind=isnan(uT); uT(ind)=0;

  [vT]=nc_readmasked(inpt,'v');
  vt=vT(:).*amp; ind=isnan(vt); vt(ind)=[];
  ind=isnan(vT); vT(ind)=0;

% Compute the rms of each perturbation field.
% Scale the TLM fields as necessary - this can
% be done in lieu of rerunning TLROMS using the
% perturbation but with different amplitude.

  rms_zeta(n)=sqrt(mean((zetap-zetac).^2));
  rms_ubar(n)=sqrt(mean((ubarp-ubarc).^2));
  rms_vbar(n)=sqrt(mean((vbarp-vbarc).^2));
  rms_temp(n)=sqrt(mean((tempp-tempc).^2));
  rms_salt(n)=sqrt(mean((saltp-saltc).^2));
  rms_u(n)=sqrt(mean((up-uc).^2));
  rms_v(n)=sqrt(mean((vp-vc).^2));

  rms_zetat(n)=sqrt(mean(zetat.^2));
  rms_ubart(n)=sqrt(mean(ubart.^2));
  rms_vbart(n)=sqrt(mean(vbart.^2));
  rms_tempt(n)=sqrt(mean(tempt.^2));
  rms_saltt(n)=sqrt(mean(saltt.^2));
  rms_ut(n)=sqrt(mean(ut.^2));
  rms_vt(n)=sqrt(mean(vt.^2));

% Compute the pattern correlations.

  mean_zeta(n)=mean(zetap-zetac);
  mean_ubar(n)=mean(ubarp-ubarc);
  mean_vbar(n)=mean(vbarp-vbarc);
  mean_temp(:,n)=squeeze(mean(mean(tempP-tempC)));
  mean_salt(:,n)=squeeze(mean(mean(saltP-saltC)));
  mean_u(:,n)=squeeze(mean(mean(uP-uC)));
  mean_v(:,n)=squeeze(mean(mean(vP-vC)));

  mean_zetat(n)=mean(zetat);
  mean_ubart(n)=mean(ubart);
  mean_vbart(n)=mean(vbart);
  mean_tempt(:,n)=squeeze(mean(mean(tempT)));
  mean_saltt(:,n)=squeeze(mean(mean(saltT)));
  mean_ut(:,n)=squeeze(mean(mean(uT)));
  mean_vt(:,n)=squeeze(mean(mean(vT)));

% zeta

  top=mean((zetap-zetac-mean_zeta(n)).*(zetat-mean_zetat));
  var1=sqrt(mean((zetap-zetac-mean_zeta(n)).^2));
  var2=sqrt(mean((zetat-mean_zetat).^2));
  corz(n)=top/(var1*var2);

% ubar

  top=mean((ubarp-ubarc-mean_ubar(n)).*(ubart-mean_ubart));
  var1=sqrt(mean((ubarp-ubarc-mean_ubar(n)).^2));
  var2=sqrt(mean((ubart-mean_ubart).^2));
  corub(n)=top/(var1*var2);

% vbar

  top=mean((vbarp-vbarc-mean_vbar(n)).*(vbart-mean_vbart));
  var1=sqrt(mean((vbarp-vbarc-mean_vbar(n)).^2));
  var2=sqrt(mean((vbart-mean_vbart).^2));
  corvb(n)=top/(var1*var2);

% temp - average the pattern correlations over all levels.

  nlev=size(mean_tempt,1);
  cort(n)=0;
  for k=1:nlev,
    avg1=mean_temp(k,n);
    avg2=mean_tempt(k,n);
    top=mean(mean((tempP(:,:,k)-tempC(:,:,k)-avg1).*(tempT(:,:,k)-avg2)));
    var1=sqrt(mean(mean((tempP(:,:,k)-tempC(:,:,k)-avg1).^2)));
    var2=sqrt(mean(mean((tempT(:,:,k)-avg2).^2)));
    cort(n)=cort(n)+top/(nlev*var1*var2);
  end,
    
% salt - average the pattern correlations over all levels.

  nlev=size(mean_saltt,1);
  cors(n)=0;
  for k=1:nlev,
    avg1=mean_salt(k,n);
    avg2=mean_saltt(k,n);
    top=mean(mean((saltP(:,:,k)-saltC(:,:,k)-avg1).*(saltT(:,:,k)-avg2)));
    var1=sqrt(mean(mean((saltP(:,:,k)-saltC(:,:,k)-avg1).^2)));
    var2=sqrt(mean(mean((saltT(:,:,k)-avg2).^2)));
    cors(n)=cors(n)+top/(nlev*var1*var2);
  end,

% u - average the pattern correlations over all levels.

  nlev=size(mean_ut,1);
  coru(n)=0;
  for k=1:nlev,
    avg1=mean_u(k,n);
    avg2=mean_ut(k,n);
    top=mean(mean((uP(:,:,k)-uC(:,:,k)-avg1).*(uT(:,:,k)-avg2)));
    var1=sqrt(mean(mean((uP(:,:,k)-uC(:,:,k)-avg1).^2)));
    var2=sqrt(mean(mean((uT(:,:,k)-avg2).^2)));
    coru(n)=coru(n)+top/(nlev*var1*var2);
  end,

% v - average the pattern correlations over all levels.

  nlev=size(mean_vt,1);
  corv(n)=0;
  for k=1:nlev,
    avg1=mean_v(k,n);
    avg2=mean_vt(k,n);
    top=mean(mean((vP(:,:,k)-vC(:,:,k)-avg1).*(vT(:,:,k)-avg2)));
    var1=sqrt(mean(mean((vP(:,:,k)-vC(:,:,k)-avg1).^2)));
    var2=sqrt(mean(mean((vT(:,:,k)-avg2).^2)));
    corv(n)=corv(n)+top/(nlev*var1*var2);
  end,

end,
 

% Plot the results.

time=[1:nrec];
figure
subplot(2,2,1); plot(time,sq(rms_zeta),time,sq(rms_zetat))
legend('zeta NLM','zeta TLM',4)
subplot(2,2,2); plot(time,sq(rms_ubar),time,sq(rms_ubart))
legend('ubar NLM','ubar TLM',4)
subplot(2,2,3); plot(time,sq(rms_vbar),time,sq(rms_vbart))
legend('vbar NLM','vbar TLM',4)

print -depsc fig1.eps

figure
subplot(2,2,1); plot(time,sq(rms_temp),time,sq(rms_tempt))
legend('temp NLM','temp TLM',4)
subplot(2,2,2); plot(time,sq(rms_salt),time,sq(rms_saltt))
legend('salt NLM','salt TLM',4)
subplot(2,2,3); plot(time,sq(rms_u),time,sq(rms_ut))
legend('u NLM','u TLM',4)
subplot(2,2,4); plot(time,sq(rms_v),time,sq(rms_vt))
legend('v NLM','v TLM',4)

print -depsc fig2.eps

figure
subplot(2,2,1); plot(time,corz); axis([0 40 0 1]);
legend('cor zeta',4)
subplot(2,2,2); plot(time,corub); axis([0 40 0 1]);
legend('cor ubar',4)
subplot(2,2,3); plot(time,corvb); axis([0 40 0 1]);
legend('cor vbar',4)

print -depsc fig3.eps

figure
subplot(2,2,1); plot(time,cort); axis([0 40 0 1]);
legend('cor temp',4)
subplot(2,2,2); plot(time,cors); axis([0 40 0 1]);
legend('cor salt',4)
subplot(2,2,3); plot(time,coru); axis([0 40 0 1]);
legend('cor u',4)
subplot(2,2,4); plot(time,corv); axis([0 40 0 1]);
legend('cor v',4)

print -depsc fig4.eps
