function [variance]=error_var(OBSname,AVGname,VARname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2004 Rutgers University.                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           %
% function [variance]=error_var(OBSname,AVGname,VARname)                    %
%                                                                           %
% This function over-writes observation error with global inverse variance. %
%                                                                           %
% On Input:                                                                 %
%                                                                           %
%    OBSname     Observation NetCDF file name.                              %
%    AVGname     Time-average file name.                                    %    
%    VARname     History file name to compute variance.                     %
%                                                                           %
% On Output:                                                                %
%                                                                           %
%    variance    Global variance.                                           %
%                                                                           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Set state variable type indices.

isFsur=1;  got.Fsur=0;
isUbar=2;  got.Ubar=0;
isVbar=3;  got.Vbar=0;
isUvel=4;  got.Uvel=0;
isVvel=5;  got.Vvel=0;
isTemp=6;  got.Temp=0;
isSalt=7;  got.Salt=0;

%  Inquire NetCDF varaible names.

[vname,nvars]=nc_vname(AVGname);
for n=1:nvars,
  name=deblank(vname(n,:));
  switch (name),
    case 'zeta',
      got.Fsur=1;
    case 'ubar',
      got.Ubar=1;
    case 'vbar',
      got.Vbar=1;
    case 'u',
      got.Uvel=1;
    case 'v',
      got.Vvel=1;
    case 'temp',
      got.Temp=1;
    case 'salt',
      got.Salt=1;
  end,
end,

%  Get observation type and error.

type=nc_read(OBSname,'obs_type');
error=nc_read(OBSname,'obs_error');
variance=nc_read(OBSname,'obs_variance');

%  Compute global (time and space) variance for each state variable.

if (got.Fsur),
  A=avg_obs(AVGname,'zeta');
  variance(isFsur)=global_var(VARname,'zeta',A);
  ind=find(type == isFsur);
  if (~isempty(ind)),
    error(ind)=1/variance(isFsur);
  end,
end,

if (got.Ubar),
  A=avg_obs(AVGname,'ubar');
  variance(isUbar)=global_var(VARname,'ubar',A);
  ind=find(type == isUbar);
  if (~isempty(ind)),
    error(ind)=1/variance(isUbar);
  end,
end,

if (got.Vbar),
  A=avg_obs(AVGname,'vbar');
  variance(isVbar)=global_var(VARname,'vbar',A);
  ind=find(type == isVbar);
  if (~isempty(ind)),
    error(ind)=1/variance(isVbar);
  end,
end,

if (got.Uvel),
  A=avg_obs(AVGname,'u');
  variance(isUvel)=global_var(VARname,'u',A);
  ind=find(type == isUvel);
  if (~isempty(ind)),
    error(ind)=1/variance(isUvel);
  end,
end,

if (got.Vvel),
  A=avg_obs(AVGname,'v');
  variance(isVvel)=global_var(VARname,'v',A);
  ind=find(type == isVvel);
  if (~isempty(ind)),
    error(ind)=1/variance(isVvel);
  end,
end,

if (got.Temp),
  A=avg_obs(AVGname,'temp');
  variance(isTemp)=global_var(VARname,'temp',A);
  ind=find(type == isTemp);
  if (~isempty(ind)),
    error(ind)=1/variance(isTemp);
  end,
end,

if (got.Salt),
  A=avg_obs(AVGname,'salt');
  variance(isSalt)=global_var(VARname,'salt',A);
  ind=find(type == isSalt);
  if (~isempty(ind)),
    error(ind)=1/variance(isSalt);
  end,
end,

%  Write variance and new observation error.

status=nc_write(OBSname,'obs_variance',variance)

status=nc_write(OBSname,'obs_error',error)

return
