function vmovie(ncfile,prop,idx,crange,movie_name);
% function VMOVIE (ncfile,prop,xyidx,[crange]);
%
%   ncfile = the NetCDF file to view.
%   prop   = the property to view.
%   xyidx  =  Bounds of cross-section to extract [xmin xmax ymin ymax] 
%             where either xmin=xmax or ymin=ymax
%   crange = the caxis limits ([cmin cmax]).  Defaults to min/max values in
%            the slice.

frame_type='capture';
%frame_type='print';

map = jet(61);
map (11:10:end,:) = 0.8;
colormap(map)          

nc = netcdf(ncfile,'nowrite');
t  = nc{'ocean_time'}(:)./86400;

if (idx(1)~=idx(2) & idx(3)~=idx(4))
  disp(['ERROR:  idx set incorrectly.  (xmin=xmax or ymin=ymax)']);
  return;
end
if (idx(1)==idx(2))
  if (idx(3)<idx(4))
    xidx=idx(1); yidx=[idx(3):idx(4)];
  else
    disp(['ERROR:  idx set incorrectly.  (ymin<ymax)']);
    return;
  end
else 					% nargin==5
  if (idx(1)<idx(2))
    xidx=[idx(1):idx(2)]; yidx=idx(3);
  else
    disp(['ERROR:  idx set incorrectly.  (xmin<xmax)']);
    return;
  end
end


if length(yidx==1),
  x = nc{'x_rho'}(yidx,xidx)./1000;
else
  x = nc{'y_rho'}(yidx,xidx)'./1000;
end


h    =nc{'h'}(yidx,xidx);
sc_r =nc{'sc_r'}(:);
zeta =nc{'zeta'}(1,yidx,xidx);
hc   =nc{'hc'}(:);
Cs_r =nc{'Cs_r'}(:);
for  k = 1:length(sc_r)
    hslice = zeta*(1+sc_r(k))+hc*sc_r(k)+(h - hc)*Cs_r(k);
    z(k,:,:) = hslice;
end

z = squeeze(z);
x = ones(length(sc_r),1)*x(:)';

v = squeeze(nc{prop}(:,:,yidx,xidx));
if nargin < 4,
  crange = [min(min(min(v))) max(max(max(v)))];
end

pcolor(x,z,squeeze(v(1,:,:)));
caxis (crange);
shading interp;
child = get(gca,'children');

hformat(14);
xlabel('Distance (km)');
ylabel('Depth (m)');
prop_long_name=nc{prop}.long_name(:);
titl=[prop_long_name];
titl=[titl ', day=' sprintf('%5.2f',t(1))];
title(titl);
ht = get(gca,'title');

hc = colorbar;
units = nc{prop}.units;
set(get(hc,'ylabel'),'string',units(:));
close(nc);

while 1,
  for tidx = 2:length(t);
    set(child,'cdata',squeeze(v(tidx,:,:)));
    titl=[prop_long_name];
    titl=[titl ', day=' sprintf('%5.2f',t(tidx))];
    set(ht,'string',titl);
    drawnow
   if exist('movie_name')==1,
     if strmatch(frame_type,'print')
        getframe_fli(tidx,fid);   % background using -print
     else
        anim_frame(movie_name,tidx); % foreground using -capture
     end
     disp(sprintf('Wrote Frame %3.3d',tidx));
   end
  end
  if exist('movie_name')==1,
     if strmatch (frame_type,'print');
      fli_end(fid,movie_name);
     else
      anim_make
     end
     return    %don't keep looping if making movie
  end
end

  