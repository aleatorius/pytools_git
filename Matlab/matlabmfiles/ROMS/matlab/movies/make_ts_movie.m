
nx = 1000; ny=500;
movie_file = 'kagom2.flc';
ncfile = 'kagom_avg_02.nc';
temp_lim = [4 18];
salt_lim = [31 33];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dstart = 10892;
offset = datenum('1-May-1996')-10200;   % datenum('datestr')-offset = roms date

ncg = netcdf('kagom_grd.nc');
x = ncg{'x_rho'}(:);
y = ncg{'y_rho'}(:);
angle = ncg{'angle'}(:);
angle = 0.5.*(angle(2:end,2:end)+angle(1:end-1,1:end-1));
mask = ncg{'mask_rho'}(:);
h = ncg{'h'}(:);
close(ncg);

scale = 1e5;
figure;
set(gcf,'pos',[10 10 600 600]);
colordef black 
set(gcf,'color','black');
mask(mask==0)=nan;
set(gcf,'pos',[10 10 nx ny]);

nc = netcdf(ncfile);
ncf = netcdf('kagom_frc.nc');
timef = ncf{'sms_time'}(:) - 10892;
time = nc{'ocean_time'}(:)./86400;

for n = 1:120
  t=time(n) + dstart + offset;
  clf
 n 
  u_romsg = nc{'u'}(n,end,:,:).*scale;
  u_romsg = 0.5.*(u_romsg(2:end,:)+u_romsg(1:end-1,:));
  v_romsg = nc{'v'}(n,end,:,:).*scale;
  v_romsg = 0.5.*(v_romsg(:,2:end)+v_romsg(:,1:end-1));
  [u_roms,v_roms] = rot2d(u_romsg,v_romsg,angle);

  temp = nc{'temp'}(n,end,:,:);
  ax1 = axes('pos',[0.05 0.05 0.4 0.9]);
  pcolor (x,y,temp.*mask);hold on;
  contour (x,y,h,[60 100 200 500],'-w');hold off;
  caxis(temp_lim);
  axis off;
  hold on; shading flat;
%$$$   quiver(x(2:3:end,2:3:end),y(2:3:end,2:3:end),...
%$$$          u_roms(1:3:end,1:3:end).*mask(1:3:end,1:3:end),...
%$$$          v_roms(1:3:end,1:3:end).*mask(1:3:end,1:3:end),0,'-k');
  set(gca,'dataaspectratio',[1 1 1]);
  title ('Gulf of Maine','fontname','times','fontsize',20)

  nstr = min(find(timef>time(n)));
  sustr = ncf{'sustr'}(nstr);
  svstr = ncf{'svstr'}(nstr);
  tau = (sustr + sqrt(-1).*svstr);
  fatarrow(-7.8e6,5.6e6,tau.*1e6,'yellow');

  pclegend(temp_lim',[.4 .15 .03 .2],gcf,'Temp (\circ{}C)');


  salt = nc{'salt'}(n,end,:,:);
  ax2 = axes('pos',[0.55 0.05 0.4 0.9]);
  pcolor (x,y,salt.*mask);hold on;
  contour (x,y,h,[60 100 200 500],'-w');hold off;
  caxis(salt_lim);
  axis off;
  hold on; shading flat;
%$$$   quiver(x(2:3:end,2:3:end),y(2:3:end,2:3:end),...
%$$$          u_roms(1:3:end,1:3:end).*mask(1:3:end,1:3:end),...
%$$$          v_roms(1:3:end,1:3:end).*mask(1:3:end,1:3:end),0,'-k');
  set(gca,'dataaspectratio',[1 1 1]);
  title ([datestr(t,1),' ',datestr(t,16)],'fontname','times','fontsize',20)
  fatarrow(-7.8e6,5.6e6,tau.*1e6,'yellow');

  pclegend(salt_lim',[.9 .15 .03 .2],gcf,'Salt (PSU)');
  
  
  outfile=sprintf('frame%3.3d.ppm',n);
  ppmwrite(outfile);
end
close(nc);

% make movie
eval('!ls frame???.ppm > allppm.list');
eval(['!rm ' movie_file]);
cmd=['!ppm2fli -g ' int2str(nx) 'x' int2str(ny)];
cmd=[cmd  ' -N allppm.list ' movie_file];
eval(cmd)
%eval('!rm *.ppm');
