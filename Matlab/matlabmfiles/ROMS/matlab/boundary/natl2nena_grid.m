% Script to interpolate from the North Atlantic 10 km ROMS (NATL) to the
% Northeast North Atlantic (NENA) coastal domain.
%
% John Wilkin, Jan 2002


% THE GRID AND LAND/SEA MASK

if ~exist('grd_natl');
  grd_natl = roms_get_grid('NATL_grid_1c.nc');
end

grd = roms_get_grid(grd_file);

% find index range so we can trim data from North Atlantic model to 
% make the interpolations tractable
jj = min(find(grd_natl.lon_rho(1,:)>(min(grd.lon_rho(:))-1))):...
    max(find(grd_natl.lon_rho(1,:)<(max(grd.lon_rho(:))+1)));
ii = min(find(grd_natl.lat_rho(:,1)>(min(grd.lat_rho(:))-1))):...
    max(find(grd_natl.lat_rho(:,1)<(max(grd.lat_rho(:))+1)));

% get the NATL data and trim
x = grd_natl.lon_rho(ii,jj);
y = grd_natl.lat_rho(ii,jj);
z = grd_natl.h(ii,jj);
m = grd_natl.mask_rho(ii,jj);

% interpolate natl bathymetry to the new NENA grid
grd.h = griddata(x,y,z,grd.lon_rho,grd.lat_rho,'linear');

% interpolate natl mask to the new NENA grid
% grd.mask_rho = griddata(x,y,m,grd.lon_rho,grd.lat_rho,'linear');
% tp = find( mask2<(1-eps) & grd.mask_rho>0 );

% Everything below here doesn't matter if we clip the land points from the
% input data before using griddata on the history file output. Beware
% warnings from griddata about bad triangulation, they can indeed give bad
% results. 

return

% We can either use this mask2 from the NATL model domain, or grd.mask
% created by seagrid from the coastline file. The seagrid mask will be more
% faithfull when we have higher resolution, but care will be required to
% ensure that the interpolation (especially of boundary values) does not
% force land points from NATL into water values in NENA...

% mask2 = 1 for points that are surrounded by water
% mask2 = 0 for points that are surrounded by land. 
% grd.h has picked up the depthmin value in the land.

% But some special treatment will be required later for NENA grid points
% that fall in between. Model variables in the history file data will have
% been set to zero on land, and if interpolated to the NENA grid they will
% acquire bad values if interpolated between in-water and on-land
% locations. We need to flag the points in this transition zone. We can find 
% them (for rho points) by finding where 0 < mask2 < 1. These could be
% filled usinf 'nearest' interpolation in griddata, after exclusing the land 
% points from the gridding.

% We need to find the points in the NENA land/sea mask, i.e. in grd.mask_rho 
% that are not completely surrounded water in the NATL domain. 

% new grid water points
% water = find(grd.mask_rho==1);

% original grid water points
% water_natl = find(m==1);

% the following points would not get interpolated right using
% griddata('linear') applied to the large domain history file
% notwater = find(mask2<(1-eps));

% find those of the above that are also classed as water in the new 
% grid (because those that are land won't matter)

tp = find( mask2<(1-eps) & grd.mask_rho>0 );
grd.coastal_transition_points = tp;

save('blah','grd')
