function [dataout,time] = roms_his2his(varname,t,grd,grd_his,his_file)
% Interpolate from one ROMS large domain history file to another, smaller, 
% ROMS domain.
%
% e.g. North Atlantic 10 km ROMS (NATL) to
%      Northeast North Atlantic (NENA) coastal domain
%
% [dataout,time] = roms_his2his(varname,grd,his_file,his_grd_file)
%
% Inputs:
%   varname = ROMS variable to interpolate
%   grd = grd structure for ROMS target grid
%   grd_his = grd structure for ROMS large domain grid
%   his_file = large domain history/climatology file
%
% Outputs:
%   dataout = varname on the rho-points grid for the target.
%             u,v are not interpolated to their own grids because in the
%             event the vectors must be rotated to new curvilinear
%             directions this must be done at colocated rho points.
%
% John Wilkin, Jan 2002

% open history netcdf file
nc = netcdf(his_file);

switch varname
  % x,y,m are the large domain coords
  % xn,yn are the target domain coords
  case { 'u','ubar'}
    x = grd_his.lon_u;
    y = grd_his.lat_u;
    m = grd_his.mask_u;
    xn = grd.lon_rho;
    yn = grd.lat_rho;
    mn = grd.mask_rho;
    warning( 'Interpolating u to rho-points to enable ANGLE rotation')
  case { 'v','vbar'}
    x = grd_his.lon_v;
    y = grd_his.lat_v;
    m = grd_his.mask_v;
    xn = grd.lon_rho;
    yn = grd.lat_rho;
    mn = grd.mask_rho;
    warning( 'Interpolating v to rho-points to enable ANGLE rotation')
  otherwise
    x = grd_his.lon_rho;
    y = grd_his.lat_rho;
    m = grd_his.mask_rho;
    xn = grd.lon_rho;
    yn = grd.lat_rho;
    mn = grd.mask_rho;
end

% find index range so we can trim data from large domain model to 
% make the interpolations tractable
%
% >>>> THIS CODE ONLY WORKS FOR NORTH-SOUTH ORIENTED LARGE GRID <<<<<
%
jj = min(find(x(1,:)>(min(xn(:))-1))):max(find(x(1,:)<(max(xn(:))+1)));
ii = min(find(y(:,1)>(min(yn(:))-1))):max(find(y(:,1)<(max(yn(:))+1)));

% trim the coordinates
x = x(ii,jj);
y = y(ii,jj);
m = m(ii,jj);
wet = find(m==1);

% interpolate large domain to small domain grid, s-level by level
if length(size(nc{varname})) == 3  
  % 2-D variable (ubar,vbar,zeta)
  dataout = ones(size(xn));
  % get the history data, trim in space, squeeze in time
  data = nc{varname}(t,ii,jj);
  data = squeeze(data);
  disp([ 'Doing ' varname]);
  datain = data;
  dataout = mn.*griddata(x(wet),y(wet),datain(wet),xn,yn,'linear');
else
  % 3-D variable (u,v,t,s)
  Ns = size(nc{varname},2);
  dataout = ones([Ns size(xn)]);
    % get the history data, trim in space, squeeze in time
  data = nc{varname}(t,:,ii,jj);
  data = squeeze(data);
  for k=1:size(data,1)
    disp([ 'Doing level k = ' int2str(k) ' for ' varname])
    datain = squeeze(data(k,:,:));
    dataout(k,:,:) = mn.*griddata(x(wet),y(wet),datain(wet),xn,yn,'linear');
  end
end

% get the time
time.name = nc{varname}.time(:);
time.units = nc{nc{varname}.time(:)}.units(:);
time.time = nc{nc{varname}.time(:)}(t);
% Huh? The name of the time variable is stored in attribute 'time' of
% the requested ROMS variable (namely varname), and this step extracts
% assigned units and the value of time for requested time index t. 
% Clever eh!

close(nc)
