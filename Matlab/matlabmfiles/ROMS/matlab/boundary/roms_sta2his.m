function [dataout,time] = roms_sta2his(varname,t,grd,grd_his,sta_file)
% intepolate from a BIG ROMS stations file to a NEST ROMS
% history/climatology/assimilation file 
%
% for nesting the Northeast North Atlantic Shelf model inside the North
% Atlantic model
%
% John Wilkin - March 2002

xn = grd.lon_rho;
yn = grd.lat_rho;

x0 = grd_his.lon_rho;
y0 = grd_his.lat_rho;

% open the stations file
nc = netcdf(sta_file);

% get the boundary point information
%dostationfile = 0;
%w = 2;
%[ispos,jspos,lon_sta,lat_sta,lon_nest,lat_nest] = ...
%    roms_bndyzone(grd,grd_his,w);    

w = 2;
ispos = nc{'Ipos'}(:);
jspos = nc{'Jpos'}(:);
lon_sta = diag(grd_his.lon_rho(jspos+1,ispos+1));
lat_sta = diag(grd_his.lat_rho(jspos+1,ispos+1));

% Step through the boundaries in turn.

% On each boundary, select the boundary zone rho points to a depth 
% of w points from the perimeter.

% Use griddata to interpolate the STATIONS output to these points.

% For velocities, apply cirvilinear grid angle rotation, then interpolate 
% (2-point average) the rho-point values of u,v,ubar,vbar back to their
% C-grid locations

% preallocate output
if length(size(nc{varname})) == 2
  % 2-D variable (ubar,vbar,zeta)
  shape = '2D';
  dataout = zeros(size(xn));
else
  % 3-D variable (u,v,t,s)
  shape = '3D';
  Ns = size(nc{varname},3);
  dataout = zeros([Ns size(xn)]);
end

% west
% don't need any west points for NENA grid
lon_bndy = [];
lat_bndy = [];

% south
lon_bndy = grd.lon_rho(1:w,:);
lat_bndy = grd.lat_rho(1:w,:);
data_bndy = zeros(size(lon_bndy));
wet = find(grd.mask_rho(1:w,:)~=0);
switch shape
  case '3D'
    for k=1:Ns
      disp([ 'Doing south boundary level k = ' int2str(k) ' for ' varname])  
      data_sta = nc{varname}(t,:,k);
      data_bndy(wet) = griddata(lon_sta,lat_sta,data_sta,...
	  lon_bndy(wet),lat_bndy(wet),'linear');
      dataout(k,1:w,:) = data_bndy;      
    end
  case '2D'
    disp([ 'Doing south boundary for ' varname])  
    data_sta = nc{varname}(t,:);
    data_bndy(wet) = griddata(lon_sta,lat_sta,data_sta,...
	lon_bndy(wet),lat_bndy(wet),'linear');
    dataout(1:w,:) = data_bndy;      
end

% east
X = size(grd.lon_rho,2)+(1-w:0);
lon_bndy = grd.lon_rho(:,X);
lat_bndy = grd.lat_rho(:,X);
data_bndy = zeros(size(lon_bndy));
wet = find(grd.mask_rho(:,X)~=0);
switch shape
  case '3D'
    for k=1:Ns
      disp([ 'Doing east boundary level k = ' int2str(k) ' for ' varname])
      data_sta = nc{varname}(t,:,k);
      data_bndy(wet) = griddata(lon_sta,lat_sta,data_sta,...
	  lon_bndy(wet),lat_bndy(wet),'linear');
      dataout(k,:,X) = data_bndy;
    end
  case '2D'
    disp([ 'Doing east boundary for ' varname])
    data_sta = nc{varname}(t,:);
    data_bndy(wet) = griddata(lon_sta,lat_sta,data_sta,...
	lon_bndy(wet),lat_bndy(wet),'linear');
    dataout(:,X) = data_bndy;
end

% north
% with w>1 we pick up some water points in the St Lawrence - these could
% be weeded out if we are anxious to reduce the length of the stations
% list. 
Y = size(grd.lon_rho,1)+(1-w:0);
lon_bndy = grd.lon_rho(Y,:);
lat_bndy = grd.lat_rho(Y,:);
data_bndy = zeros(size(lon_bndy));
wet = find(grd.mask_rho(Y,:)~=0);
switch shape
  case '3D'
    for k=1:Ns
      disp([ 'Doing north boundary level k = ' int2str(k) ' for ' varname])
      data_sta = nc{varname}(t,:,k);
      data_bndy(wet) = griddata(lon_sta,lat_sta,data_sta,...
	  lon_bndy(wet),lat_bndy(wet),'linear');
      dataout(k,Y,:) = data_bndy;
    end
  case '2D'
    disp([ 'Doing north boundary for ' varname])
    data_sta = nc{varname}(t,:);
    data_bndy(wet) = griddata(lon_sta,lat_sta,data_sta,...
	lon_bndy(wet),lat_bndy(wet),'linear');
    dataout(Y,:) = data_bndy;
end

% get the time
time.name = nc{varname}.time(:);
time.units = nc{nc{varname}.time(:)}.units(:);
time.time = nc{nc{varname}.time(:)}(t);
% Huh? The name of the time variable is stored in attribute 'time' of
% the requested ROMS variable (namely varname), and this step extracts
% assigned units and the value of time for requested time index t. 
% Clever eh!

close(nc)
