% Interpolate ROMS North Atlantic 10 km ROMS (NATL) to
% Northeast North American continental shelf (NENA) coastal domain
%
% John Wilkin, Jan 2002
%
% ************************
%
% before this script is invoked, the time index 't' must be set. 
% This determines which time record is being read from the input file
%
% This script is designed to be called multiple times from the command
% window in the case that multiple time levesl are to be written to a
% climatology file. e.g.
%
% from_where = 'sta';
% for t=1:12
%   natl2nena
% end

if ~exist('from_where')
  from_where = 'his';  
end
% from_where = 'sta';

if ~exist('outversion')
  outversion = '13';
end

if ~exist('first_time')
  % flag to create the empty out_file on the first time through
  first_time = 1;
end

% NATL output
natldirname  = '/export/home/wilkin/roms/natl/';
his_file     = [natldirname 'natl_his_08.nc'];
sta_file     = [natldirname 'natl_sta_1.nc'];
frc_file     = [natldirname 'NATL_coads_1d.nc'];
his_grd_file = [natldirname 'NATL_grid_1c.nc'];

his_file     = [natldirname 'natl_his_02.nc'];
sta_file     = [natldirname 'natl_sta_12.nc'];
sta_file     = [natldirname 'natl_sta_13.nc'];

switch from_where
  case 'his'
    infile = his_file;
  case 'sta'
    infile = sta_file;
end

if ~exist('t')
  warning('Time index to read from input history/stations file is not set')
  t = input([ 'Enter time index to read from ' infile ' : ']);
end

% Northeast North America (NENA) shelf domain inputs
grd_file = 'roms_nena_grid_2.nc';

if ~exist('grd_his')
  disp([ 'Loading grd ' his_grd_file])
  grd_his = roms_get_grid(his_grd_file);
else
  disp([ 'Using grd ' grd_his.grd_file]) 
end
if ~exist('grd')
  disp([ 'Loading grd ' grd_file])
  grd = roms_get_grid(grd_file);
else
  disp([ 'Using grd ' grd.grd_file]) 
end

% run this to create the stations.in file for roms
if 0
  dostationfile = 1;
  w = 2;
  [ispos,jspos,lon_sta,lat_sta,lon_nest,lat_nest] = ...
      roms_bndyzone(grd,grd_his,w);    
end



% Get the scoord parameters from his_file and pass along
nc = netcdf(his_file);
grd.theta_s = nc{'theta_s'}(:);
grd.theta_b = nc{'theta_b'}(:);
grd.Tcline  = nc{'Tcline'}(:);
grd.hc	    = nc{'hc'}(:);     
grd.sc_r    = nc{'sc_r'}(:);   
grd.sc_w    = nc{'sc_w'}(:);   
grd.Cs_r    = nc{'Cs_r'}(:);   
grd.Cs_w    = nc{'Cs_w'}(:);   
close(nc)

switch from_where
  case 'his'
    [roms.zeta roms.time] = roms_his2his('zeta',t,grd,grd_his,his_file);
    ubar = roms_his2his('ubar',t,grd,grd_his,his_file);
    vbar = roms_his2his('vbar',t,grd,grd_his,his_file);
    u = roms_his2his('u',t,grd,grd_his,his_file);
    v = roms_his2his('v',t,grd,grd_his,his_file);
    roms.temp = roms_his2his('temp',t,grd,grd_his,his_file);
    roms.salt = roms_his2his('salt',t,grd,grd_his,his_file);
  case 'sta'
    [roms.zeta roms.time] = roms_sta2his('zeta',t,grd,grd_his,sta_file);
    ubar = roms_sta2his('ubar',t,grd,grd_his,sta_file);
    vbar = roms_sta2his('vbar',t,grd,grd_his,sta_file);
    [u roms.time] = roms_sta2his('u',t,grd,grd_his,sta_file);
    v = roms_sta2his('v',t,grd,grd_his,sta_file);
    roms.temp = roms_sta2his('temp',t,grd,grd_his,sta_file);
    roms.salt = roms_sta2his('salt',t,grd,grd_his,sta_file);
  otherwise
    error('large domain data must be from a stations or history file')
end
    
if max(abs(grd.angle))>0.01 % the grid is curvilinear or rotated 
  % with respect to north-east, so vectors must be rotated by ANGLE.
  % ANGLE (from seagrid) is in radians and is the angle the xi-eta 
  % grid is rotated anticlockwise from a north-east grid
  %
  % angle:equation = "u(roms)=Real((Ueast+i*Vnorth)*exp(-i*angle)) 
  % and v(roms)=Imag((Ueast+i*Vnorth)*exp(-i*angle))" ;
  eitheta = exp(-sqrt(-1)*grd.angle);  
  % velocity must be averaged to rho-points, rotated, and moved back to u,v
  % points 
  U = (ubar+sqrt(-1)*vbar).*eitheta;
  roms.ubar = av2(real(U.'))';
  roms.vbar = av2(imag(U));
  for k = 1:size(u,1)
    U = (squeeze(u(k,:,:))+sqrt(-1)*squeeze(v(k,:,:))).*eitheta;
    roms.u(k,:,:) = av2(real(U.'))';
    roms.v(k,:,:) = av2(imag(U));
  end
end

% PREPARE TO WRITE THE NETCDF FILE

if first_time
  
  % Metadata for roms initial/climatology/assimilation file
  titlestr = 'Prototype Northeast North American shelf model';
  switch from_where
    case 'his'
      roms.type = [ 'ROMS ASSIMILATION/NUDGING/CLIMATOLOGY file - 3D '];
      out_file = [ 'roms_nena_clim_' outversion '.nc'];
      sourcestr = [ 'North Atlantic ROMS (J. Levin) at approx 10-km ' ...
	    'interpolated from history file ' his_file];
    case 'sta'
      roms.type = [ 'ROMS ASSIMILATION/NUDGING/CLIMATOLOGY file - ' ...
	    'perimeter points only '];
      out_file = [ 'roms_nena_climsta_' outversion '.nc'];
      sourcestr = [ 'North Atlantic ROMS (J. Levin) at approx 10-km ' ...
	    'interpolated from stations file ' sta_file];
  end
  reference_string = ' ';

  time_variable = roms.time.name;
  time_variable_units = roms.time.units;
  dosalt = 1;
  dou = 1;
  doerrs = 0;
  douerrs = 0;
  
  roms.grd_file = grd_file;
  roms.grd = grd;
  roms.details = ' ';
  roms.details = [ 'Interpolation controlled by script ' which(mfilename)];

  % overwrite
  noclobber = 0;
  nc_create_roms_assim

  % index for writing to out_file. this gets incremented on subsequent calls
  nc_tindex = 1; 

  % reset flag so we don't recreate out_file
  first_time = 0;

end % first_time ... out_file (empty of any data) is now created


% ENTER DATA IN THE NEW NETCDF FILE
nc = netcdf(out_file,'write');
nc{roms.time.name}(nc_tindex) = roms.time.time;
nc{'temp'}(nc_tindex,:,:,:) = roms.temp;
if dosalt
  nc{'salt'}(nc_tindex,:,:,:) = roms.salt;
end
if dou
  nc{'u'}(nc_tindex,:,:,:) = roms.u;
  nc{'v'}(nc_tindex,:,:,:) = roms.v;
  nc{'ubar'}(nc_tindex,:,:) = roms.ubar;
  nc{'vbar'}(nc_tindex,:,:) = roms.vbar;
  nc{'zeta'}(nc_tindex,:,:) = roms.zeta;
end    
nc_tindex = nc_tindex+1;
close(nc)
