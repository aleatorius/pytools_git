function [ispos,jspos,lon_rho_sta,lat_rho_sta,lonn,latn] = ...
    roms_bndyzone(grd,grd0,w)
% [ispos,jspos,lon_rho_sta,lat_rho_sta,lonn,latn] = roms_bndyzone(grd,grd0,w)
%
% consider two roms grids:
% grid NEST is wholly enclosed by the domain of grid BIG
%
% Inputs are grid structures as output by function roms_get_grid(grd_file)
% grd = NEST
% grd0 = BIG
% w is the depth of perimeter boundary zone
%
% Use 'global doplot dolabel dostationfile' to set extra options
%
% We wish to set perimeter (open boundary conditions) for NEST from 
% values computed in BIG 
% 
% To do so we will save ROMS output from the points in BIG that are sufficient 
% to allow a straightforwad spatial interpolation to the perimeter points 
% in NEST. We use the STATIONS option in ROMS to get the points we need.
% 
% No assumptions are made about any points in NEST and BIG coinciding, or 
% the coordinates being aligned in any way.
%
% No assumption is made about NEST necessarily being a finer resolution 
% grid than BIG, though this is obviously the more likely application.
%
% A triangle of BIG points will be assumed good enough to interpolate to 
% any NEST point, i.e. we will use delaunay triangulation with linear
% interpolation to go from the output saved using STATIONS to the new grid.
%
% Steps
%
% Find the minimal BIG domain that encloses the NEST domain 
% - Assign ROMS i,j indices to list
% - Exclude land masked BIG points
%
% Delaunay triangulate BIG
%
% Determine the NEST edge points that will require boundary values 
% interpolated from BIG
% - Use tsearch to find the BIG indicies that enclose each NEST perimeter point
% - Exclude NEST perimeter points that are masked
% - Remove duplicated BIG points from list
%
% In transferring velocity data from BIG to NEST (see natl2nena.m) the u,v
% and ubar,vbar data are first averaged to BIG rho-points, then
% interpolated to NEST rho-points, rotated to align the curvilinear
% directions, then averaged back to respective u,v points.  
%
% The ROMS' STATIONS option specifies the indicies of output locations
% in terms of the rho-points grid. u,v output are at the C-grid staggered
% location 1/2 grid point left (for u) and 1/2 grid point down (for v). 
% To obtain boundary zone values for u,v, roms_his2his interpolates
% u_lon_u,lat_u) to lon_rho,lat_rho, rotates by angle defined at _rho
% points, and then averages u,v, back to NEST _u,_v points. 
%
% This require we expand the STATIONS list:
%
% Triangulate lon_u,lat_u and find which points are required to enclose the 
% same target NEST _rho points. Flag the _u and _v points according to 
% their _rho i,j index because the STATIONS option applies to rho index.
% Do the same for lon_v,lat_
% Eliminate dups
% Note: we need to use w>=2 in NEST to get enough NEST grid _rho points 
% for the average step back to NEST u,v to work. u,v are interpolated to NEST 
% _rho first because the rotation of interpolated u,v to local xi,eta
% direction  is calculated at _rho points where angle is defined. 
% The final C-grid u,v locations are those within the surrounding NEST _rho
% selection (with depth determined by parameter w).
%
% Last step: generate STATIONS list in format of ROMS statoins.in file
%
% John Wilkin
% March 2002
%
% ---------------------------------------------------------------------

if nargin < 2
  grd0=roms_get_grid('NATL_grid_1c.nc')
end
if nargin < 1
  grd=roms_get_grid('roms_nena_grid_2.nc')
end
global doplot dolabel dostationfile
if ~exist('doplot'), doplot=0, end
if ~exist('dolabel'), dolabel=0, end
if ~exist('dostationfile'), dostationfile=0, end

% ---------------------------------------------------------------------


% Determine the NEST edge points that will require boundary values 

% optionally specify a buffer zone width
if nargin < 3
  w = 2;
end

% west
% don't need any west points for NENA grid
% w = 3;
lon_west = [];
lat_west = [];

% south
% w = 3;
lon_south = grd.lon_rho(1:w,:);
lat_south = grd.lat_rho(1:w,:);
clip = find(grd.mask_rho(1:w,:)==0);
lon_south(clip) = [];
lat_south(clip) = [];

% east
% w = 3;
X = size(grd.lon_rho,2)+(1-w:0);
lon_east = grd.lon_rho(:,X);
lat_east = grd.lat_rho(:,X);
clip = find(grd.mask_rho(:,X)==0);
lon_east(clip) = [];
lat_east(clip) = [];

% north
% with w>1 we pick up some water points in the St Lawrence - these could
% be weeded out if we are anxious to reduce the length of the stations
% list. 
% w = 3;
Y = size(grd.lon_rho,1)+(1-w:0);
lon_north = grd.lon_rho(Y,:);
lat_north = grd.lat_rho(Y,:);
clip = find(grd.mask_rho(Y,:)==0);
lon_north(clip) = [];
lat_north(clip) = [];

% combine
% don't worry about duplicated points in corners. 
% duplicates get eliminated from the BIG results.
lon_nest = [lon_west(:); lon_south(:); lon_east(:); lon_north(:)];
lat_nest = [lat_west(:); lat_south(:); lat_east(:); lat_north(:)];

% ---------------------------------------------------------------------

% BIG grid information
[Mp,Lp] = size(grd0.lon_rho);

m_big   = grd0.mask_rho(:);
lat_big = grd0.lat_rho(:);
lon_big = grd0.lon_rho(:);
i_big = 0:(Lp-1);
j_big = 0:(Mp-1);
  
% ROMS i,j index 
[i_big,j_big] = meshgrid(i_big,j_big);
i_big = i_big(:);
j_big = j_big(:);
k_big = Mp*i_big+j_big; % 1-D index for removing duplicates later
% undo this with 
% i = floor(k_big(1)/Mp);
% j = k-Mp*i;

% remove land points from consideration
clip = find(m_big==0);
lon_big(clip) = [];
lat_big(clip) = [];
m_big(clip) = [];
i_big(clip) = [];
j_big(clip) = [];
k_big(clip) = [];

% remove points outside the lon/lat range of the NEST
ax_range = [[range(lon_nest)]' range(lat_nest)'];
ax_range = ax_range+0.5*[-1 1 -1 1];
index = findinrange([lon_big lat_big],ax_range);
lon_big = lon_big(index);
lat_big = lat_big(index);
m_big = m_big(index);
i_big = i_big(index);
j_big = j_big(index);
k_big = k_big(index);

% delaunay triangulation
fuzz = 4*sqrt(eps); % default
fuzz = 5*fuzz;
tri = delaunay(lon_big,lat_big,fuzz);

if doplot
  han = trimesh(tri,lon_big,lat_big,zeros(size(lon_big)));
  view(2)
  hold on
  symb = 'bs';
  set(han,'edgecolor',symb(1))
end
  
% find the triangles that enclose the target NEST points
index = tsearch(lon_big,lat_big,tri,lon_nest,lat_nest);
if any(isnan(index))
  error('tsearch found a NEST point outside the convex hull')
end

% retain only the BIG points that comprise these triangles
lon_big = lon_big(tri(index,:));
lat_big = lat_big(tri(index,:));
m_big   = m_big(tri(index,:));
i_big   = i_big(tri(index,:));
j_big   = j_big(tri(index,:));
k_big   = k_big(tri(index,:));

if doplot
  han = plot(lon_nest,lat_nest,'ko');
  set(han,'markersize',10,'markerfacecolor',[0 0.5 0])
  han = plot(lon_big,lat_big,symb);
  set(han,'markersize',10,'markerfacecolor',symb(1))
end

lon_big = lon_big(:);
lat_big = lat_big(:);
i_big = i_big(:);
j_big = j_big(:);
k_big = k_big(:);

% find the duplicated indicies
[k_big,index] = sort(k_big(:));
lon_big       = lon_big(index);
lat_big       = lat_big(index);
i_big         = i_big(index);
j_big         = j_big(index);

clip = find(diff(k_big)==0);
lon_big(clip) = [];
lat_big(clip) = [];
i_big(clip)   = [];
j_big(clip)   = [];
k_big(clip)   = [];

% examine this plot to verify that the rho points selected coincide with the 
% correspondong rho,u,v points selected on each sweep
if doplot
  plot(lon_nest,lat_nest,'k*')
  han = plot(lon_big,lat_big,'k^');
  set(han,'markersize',8,'markerfacecolor','k')
end

if dolabel
  for i=1:length(lon_big)
    lab = int2str(j_big(i));
    lab = int2str(j_big(i));
    text(lon_big(i),lat_big(i),lab)
  end
end

if dostationfile
  % generate STATIONS input file
  disp('preparing to write station.in file')
  fid = fopen('stations.in','w');
  str = ['1   NSTATION'];
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  fprintf(fid,'%d',length(k_big));
  fprintf(fid,'\n');
  
  % insert the intervening stdin cards
  str = '2   idUvel, idVvel, idWvel, idOvel, idUbar, idVbar, idFsur';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '      T       T       T       T       T       T       T';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '3   idTvar(1:NT)    (temperature, salinity, etc.)';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '      T   T   T   T   T   T   T   T   T';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '4   idUsms, idVsms, idUbms, idVbms, idUbws, idVbws';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '      F       F       T       T       F       F';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '5   idAbed, idUbed, idVbed, idUbot, idVbot, idHrip, idHapp, idZnot, idZapp';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '      F      F       F       F       F       F       F       F       F';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '6   idTsur, idLhea, idShea, idLrad, idSrad';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '      F       F       F       F       F';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '7   idDano, idVvis, idTdif, idSdif, idHsbl, idHbbl, idMtke, idMtls';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  str = '      F       F       F       F       F       T       F       F';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  
  % insert the stations list
  str = [ '8   Station Position Indices: (ISPOS(n),JSPOS(n), n=1,NSTATION)'];
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');
  for k=1:length(k_big)
    % adjustment for ROMS rho points 0:Lm,0:Mm has already been made
    fprintf(fid,'%6i %6i     ',i_big(k),j_big(k)); 
    % append the actual lon/lat for information (ROMS does not need this)
    fprintf(fid,'%8.3f   %8.3f',lon_big(k),lat_big(k));
    fprintf(fid,'\n');
  end 
  str = '99 END of input data';
  fprintf(fid,'%s',str);
  fprintf(fid,'\n');  
  fclose(fid);
end

if nargout > 0
  ispos = i_big;
  jspos = j_big;
  lon_rho_sta = lon_big;
  lat_rho_sta = lat_big;
  lonn = lon_nest;
  latn = lat_nest;
end
