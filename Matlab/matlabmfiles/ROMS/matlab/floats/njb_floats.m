%  This script is used to plot floats tracjectories.


%Fname='/nc/arango/NJB/Jul01/cycle1/Run01/njb1_flt_01a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle1/Run02/njb1_flt_02a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle2/Run03/njb1_flt_03c.nc';
%Fname='/nc/arango/NJB/Jul01/cycle2/Run04/njb1_flt_04a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle3/Run05/njb1_flt_05a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle3/Run06/njb1_flt_06a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle4/Run07/njb1_flt_07a.nc';
 Fname='/nc/arango/NJB/Jul01/cycle4/Run08/njb1_flt_08a.nc';

CSTfile='/n0/arango/ocean/plot/Data/NAeast_full.cst';

LonMin=-74.5;
LonMax=-73.7;
LatMin=39.1;
LatMax=39.8;

m_proj('Mercator','long',[LonMin LonMax],'lat',[LatMin LatMax]);

%---------------------------------------------------------------------
%  Read in floats data. 
%---------------------------------------------------------------------

spval=0.9e+35;

time=nc_read(Fname,'ocean_time');
time=time./86400;

lon=nc_read(Fname,'lon');
lat=nc_read(Fname,'lat');
[nf,nt]=size(lon);

ind=find(lon > spval);
if (~isempty(ind)),
  lon(ind)=NaN;
  lat(ind)=NaN;
end,
[X,Y]=m_ll2xy(lon,lat,'point');

X=X';
Y=Y';

%---------------------------------------------------------------------
%  Plot float trajectories.
%---------------------------------------------------------------------

wkday=['Sun. ';'Mon. ';'Tue. ';'Wed. ';'Thu. ';'Fri. ';'Sat. '];
lwkday=[4,4,4,4,4,4,4];
Month=['Jan ';'Feb ';'Mar ';'Apr ';'May ';'Jun ';'Jul ';'Aug ';...
       'Sep ';'Oct ';'Nov ';'Dec '];
lmonth=[3,3,3,3,3,3,3,3,3,3,3,3];

dlon=1/12;
dlat=1/12;

fn=1:nf;
tail_length=50;
inc=5;

for i=1:30:nt

  clf

% Draw map.

  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1], ...
          'color',[0 0 0]);
  m_grid('xtick',[floor(LonMin):dlon:ceil(LonMax)], ...
         'ytick',[floor(LatMin):dlat:ceil(LatMax)], ...
         'fontweight','bold','fontsize',16,'color','k');
  m_gshhs_f('patch',[1 .85 .7],'edgecolor','k');

% Write trajectory time.

  gtime=gregorian(time(i)+2440000.0);
  year=gtime(1); month=gtime(2); day=gtime(3);
  hour=gtime(4); minute=gtime(5); second=gtime(6);

  iwday=day_code(month,day,year);
  Tstring=[wkday(iwday,1:lwkday(iwday)), ', ', ...
           Month(month,1:lmonth(month)), ' ', ...
           num2str(day,'%2.2i'), ', ', ...
           num2str(hour,'%2.2i'), ':', ...
           num2str(minute,'%2.2i'), ' GMT, ', ...
           num2str(year)];
  title(Tstring,'fontweight','bold','fontsize',16,'color','y')
  hold on;

% Draw LEO transects

  m_line([-74.1700 -73.9917],[39.6367 39.5167],'color','k');
  m_line([-74.1900 -74.0117],[39.5917 39.4733],'color','k');
  m_line([-74.2233 -74.0328],[39.5583 39.4328],'color','k');
  m_line([-74.2475 -74.0717],[39.5142 39.3958],'color','k');
  m_line([-74.2633 -74.0867],[39.4617 39.3433],'color','k');
  m_line([-74.3117 -74.1383],[39.4350 39.3150],'color','k');
  m_line([-74.3467 -74.1733],[39.4000 39.2833],'color','k');
  m_line([-74.3867 -74.2117],[39.3667 39.2517],'color','k');
  m_line([-74.4300 -74.2550],[39.3350 39.2200],'color','k');

% Draw trajectories

  trail=max(1,i-tail_length):inc:i;
  han=plot(X(i,fn),Y(i,fn),'bo',X(trail,fn),Y(trail,fn),'r-');
  set(han(1),'markersize',5);
  drawnow;
  hold off

  outfile=sprintf('floats%3.3d.ppm',i);
  ppmwrite(outfile);

% set(gcf,'Visible','Off');
% eval('print -dppm ',outfile);

  disp(['Processed record: ',num2str(i,'%5.5i')]);

end,

%---------------------------------------------------------------------
%  Make FLC movie.
%---------------------------------------------------------------------

nx=655;
ny=700;

movie_file='floats.flc';
eval('!ls floats???.ppm > allppm.list');
eval(['!rm ' movie_file]);
cmd=['!ppm2fli -g ' int2str(nx) 'x' int2str(ny)];
cmd=[cmd  ' -N allppm.list ' movie_file];
eval(cmd)
