function map = redblue(n)
% REDBLUE(N) red-blue colormap with N entries
% John Wilkin

if nargin == 0
  n = size(colormap,1);
end
n = n+1;
n = 2*round(n/2);

b(1:n/2) =  1;
b(n/2+1:n) = 1-linspace(0,1,n/2);

r(1:n/2) = 1-linspace(1,0,n/2);
r(n/2+1:n) = 1;

g(1:n/2) = r(1:n/2);
g(n/2+1:n) = b(n/2+1:n);

map = [r; g; b]';

% remove the duplicated color entry in the middle
map([2 n/2],:,:) = [];
