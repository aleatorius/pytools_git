%  This script is used to plot floats trajectories.


%Fname='/nc/arango/NJB/Jul01/cycle1/Run01/njb1_flt_01a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle1/Run02/njb1_flt_02a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle2/Run03/njb1_flt_03c.nc';
%Fname='/nc/arango/NJB/Jul01/cycle2/Run04/njb1_flt_04a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle3/Run05/njb1_flt_05a.nc';
%Fname='/nc/arango/NJB/Jul01/cycle3/Run06/njb1_flt_06a.nc';
%Fname='/nd/arango/NJB/Jul01/cycle4/Run07/njb1_flt_07b.nc';
%Fname='/nd/arango/NJB/Jul01/cycle4/Run08/njb1_flt_08b.nc';
%Fname='/nd/arango/NJB/Jul01/cycle5/Run09/njb1_flt_09.nc';
%Fname='/nd/arango/NJB/Jul01/cycle5/Run10/njb1_flt_10a.nc';
%Fname='/nd/arango/NJB/Jul01/cycle6/Run11/njb1_flt_11b.nc';
%Fname='/nd/arango/NJB/Jul01/cycle6/Run12/njb1_flt_12a.nc';
%Fname='/nd/arango/NJB/Jul01/cycle7/Run13/njb1_flt_13a.nc';
 Fname='/nd/arango/NJB/Jul01/cycle7/Run14/njb1_flt_14a.nc';

CSTfile='/n0/arango/ocean/plot/Data/NAeast_full.cst';

LonMin=-74.5;
LonMax=-73.7;
LatMin=39.1;
LatMax=39.8;

m_proj('Mercator','long',[LonMin LonMax],'lat',[LatMin LatMax]);

%---------------------------------------------------------------------
%  Read in floats data. 
%---------------------------------------------------------------------

spval=0.9e+35;

time=nc_read(Fname,'ocean_time');
time=time./86400;

lon=nc_read(Fname,'lon');
lat=nc_read(Fname,'lat');
[nf,nt]=size(lon);

PDFF = 1; % to plot data following floats
if PDFF
   % read data following floats
  data = nc_read(Fname,'depth');
  data=data';
end

ind=find(lon > spval);
if (~isempty(ind)),
  lon(ind)=NaN;
  lat(ind)=NaN;
end,
[X,Y]=m_ll2xy(lon,lat,'point');

X=X';
Y=Y';

%---------------------------------------------------------------------
%  Plot float trajectories.
%---------------------------------------------------------------------

wkday=['Sun. ';'Mon. ';'Tue. ';'Wed. ';'Thu. ';'Fri. ';'Sat. '];
lwkday=[4,4,4,4,4,4,4];
Month=['Jan ';'Feb ';'Mar ';'Apr ';'May ';'Jun ';'Jul ';'Aug ';...
       'Sep ';'Oct ';'Nov ';'Dec '];
lmonth=[3,3,3,3,3,3,3,3,3,3,3,3];

dlon=1/12;
dlat=1/12;

fn=1:nf;        % the floats to plot (1:nf plots all floats)
tail_length=48; % length of the tail of the comet track
inc=5;          % only every inc'th point is plotted (to save time) 
inct=30;        % time increment between frames (to save space and time) 

for i=1:inct:nt

  clf

% Draw map.

  set(gcf,'Units','Normalized',...
          'Position',[0.2 0.1 0.6 0.8],...
          'PaperUnits','Normalized',...
          'PaperPosition',[0 0 1 1], ...
          'color',[0 0 0]);
  m_grid('xtick',[floor(LonMin):dlon:ceil(LonMax)], ...
         'ytick',[floor(LatMin):dlat:ceil(LatMax)], ...
         'fontweight','bold','fontsize',16,'color','k');
  m_gshhs_f('patch',[1 .85 .7],'edgecolor','k');

% Write trajectory time.

  gtime=gregorian(time(i)+2440000.0);
  year=gtime(1); month=gtime(2); day=gtime(3);
  hour=gtime(4); minute=gtime(5); second=gtime(6);

  iwday=day_code(month,day,year);
  Tstring=[wkday(iwday,1:lwkday(iwday)), ', ', ...
           Month(month,1:lmonth(month)), ' ', ...
           num2str(day,'%2.2i'), ', ', ...
           num2str(hour,'%2.2i'), ':', ...
           num2str(minute,'%2.2i'), ' GMT, ', ...
           num2str(year)];
  title(Tstring,'fontweight','bold','fontsize',16,'color','y')
  hold on;

% Draw LEO transects

  m_line([-74.1700 -73.9917],[39.6367 39.5167],'color','k');
  m_line([-74.1900 -74.0117],[39.5917 39.4733],'color','k');
  m_line([-74.2233 -74.0328],[39.5583 39.4328],'color','k');
  m_line([-74.2475 -74.0717],[39.5142 39.3958],'color','k');
  m_line([-74.2633 -74.0867],[39.4617 39.3433],'color','k');
  m_line([-74.3117 -74.1383],[39.4350 39.3150],'color','k');
  m_line([-74.3467 -74.1733],[39.4000 39.2833],'color','k');
  m_line([-74.3867 -74.2117],[39.3667 39.2517],'color','k');
  m_line([-74.4300 -74.2550],[39.3350 39.2200],'color','k');

% Draw trajectories

  trail=max(1,i-tail_length):inc:i;

  if PDFF
     % colour the head according to some property of the flow following
     % the floats
     % plot the tail
     han=plot(X(trail,fn),Y(trail,fn),'r-');
     hold on
     % plot the head with filled symbols
     % range of the data 
     zrng = [-10 0]; % for fload depth
     % zrng = something else for other data
     colormap(redblue)
     han=colourplot(X(i,fn),Y(i,fn),data(i,fn),'o',zrng,5,1);
     set(han,'markeredgecolor',0.5*[1 1 1]);
     hancb=colorbar;
     set(hancb,'xcolor','y','ycolor','y',...
     'fontweight','bold','fontsize',16)
  else
     % just plot the trajectories with an open circle for the head  
     han=plot(X(i,fn),Y(i,fn),'bo',X(trail,fn),Y(trail,fn),'r-');
     %        ^^^^ the head ^^^^     ^^^^^^^ the tail ^^^^^^^^
     set(han(1),'markersize',5);
  end
  drawnow;
  hold off

  outfile=sprintf('floats%3.3d.ppm',i);
  ppmwrite(outfile);

% set(gcf,'Visible','Off');
% eval('print -dppm ',outfile);

  disp(['Processed record: ',num2str(i,'%5.5i')]);

end,

%---------------------------------------------------------------------
%  Make FLC movie.
%---------------------------------------------------------------------

nx=655;
ny=700;

movie_file='floats.flc';
eval('!ls floats???.ppm > allppm.list');
eval(['!rm ' movie_file]);
cmd=['!ppm2fli -g ' int2str(nx) 'x' int2str(ny)];
cmd=[cmd  ' -N allppm.list ' movie_file];
eval(cmd)
