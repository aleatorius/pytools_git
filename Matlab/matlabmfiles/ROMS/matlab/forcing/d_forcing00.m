%
%  This scritp process ROMS forcing NetCDF file.
%

ICREATE=0;        % Create a new FORCING NetCDF file
FORECAST=1;       % Add atmospheric forecast records.

MetName='/n7/arango/NJB/Jul00/Data/njb1_rmfs_sum00.nc';
RegName='/coamps/mel/Jul00/netcdf/coamps_jul00.nc';
GloName='/coamps/mel/Jul00/netcdf/nogaps_jul00.nc';

GrdName='/n7/arango/NJB/Jul00/Data/njb1_grid_a.nc';
FrcName='/n7/arango/NJB/Jul00/Data/njb1_coamps_jul00.nc';

%  The starting and ending days is by 0.5 intervals since the data
%  is generated every 12 hours.

day_str=30.5;                  % ready

Istr=1+(day_str-1.0)/0.5;      % starting input record
Ostr=1+(day_str-1.0)/0.25;     % starting output record

%-----------------------------------------------------------------------
%  Set switches and names of variables to process.
%-----------------------------------------------------------------------

%  Set structure arrays: define (variables to define), grided
%  (point or grided data), Tname (name of time variable), and
%  Vname (name of forcing variable).  Switches are: 0=no, 1=yes.

define.wind =1;             % surface wind components
grided.wind =1;
Tname.wind  ='wind_time';
Vname.suw   ='Uwind';
Vname.svw   ='Vwind';

define.sms  =1;             % surface wind stress
grided.sms  =1;
Tname.sms   ='sms_time';
Vname.sus   ='sustr';
Vname.svs   ='svstr';

define.pair =1;             % surface air pressure
grided.pair =1;
Tname.pair  ='pair_time';
Vname.pair  ='Pair';

define.tair =1;             % surface air temperature
grided.tair =1;
Tname.tair  ='tair_time';
Vname.tair  ='Tair';

define.qair =1;             % surface relative humidity
grided.qair =1;
Tname.qair  ='qair_time';
Vname.qair  ='Qair';

define.cloud=0;             % cloud fraction
grided.cloud=0;
Tname.cloud ='cloud_time';
Vname.cloud ='cloud';

define.rain =1;             % rain fall rate
grided.rain =1;
Tname.rain  ='rain_time';
Vname.rain  ='rain';

define.evap =0;             % evaporation rate
grided.evap =0;
Tname.evap  ='evap_time';
Vname.evap  ='evap';

define.srf  =1;             % shortwave radiation flux
grided.srf  =0;
Tname.srf   ='srf_time';
Vname.srf   ='swrad';

define.lrf  =0;             % longwave radiation flux
grided.lrf  =0;
Tname.lrf   ='lrf_time';
Vname.lrf   ='lwrad';

define.lhf  =0;             % latent heat flux
grided.lhf  =0;
Tname.lhf   ='lhf_time';
Vname.lhf   ='latent';

define.shf  =0;             % sensible heat flux
grided.shf  =0;
Tname.shf   ='sen_time';
Vname.shf   ='sensible';

define.nhf  =1;             % surface net heat flux
grided.nhf  =0;
Tname.nhf   ='shf_time';
Vname.nhf   ='shflux';

define.nff  =0;             % surface net freshwater flux
grided.nff  =0;
Tname.nff   ='swf_time';
Vname.nff   ='swflux';

%-----------------------------------------------------------------------
%  Set time record parameters to process.
%-----------------------------------------------------------------------

%  Inquire size of time record in RMFS NetCDF file.

[dnames,dsizes]=nc_dim(MetName);
ndims=length(dsizes);
for n=1:ndims,
  name=dnames(n,:);
  if (name(1:4) == 'time'),
    nhrec=dsizes(n);
    icoare=1;
  elseif (name(1:8) == 'shf_time'),
    nhrec=dsizes(n);
    icoare=0;
  end,
end,
disp(' ');
disp(['Number of records in Met Tower file: ',num2str(nhrec)]);

%  Inquire size of time record in COAMPS NetCDF file.

[dnames,dsizes]=nc_dim(RegName);
ndims=length(dsizes);
for n=1:ndims,
  name=dnames(n,:);
  if (name(1:4) == 'time'),
    coamps_wrec=dsizes(n);
  end,
end,
disp(['Number of records in COAMPS file: ',num2str(coamps_wrec)]);

%  Inquire size of time record in NOGAPS NetCDF file.

[dnames,dsizes]=nc_dim(GloName);
ndims=length(dsizes);
for n=1:ndims,
  name=dnames(n,:);
  if (name(1:4) == 'time'),
    nogaps_wrec=dsizes(n);
  end,
end,
disp(['Number of records in NOGAPS file: ',num2str(nogaps_wrec)]);
disp(' ');

%-----------------------------------------------------------------------
%  Create Forcing NetCDF file.
%-----------------------------------------------------------------------

if ( ICREATE ),
  day_str=1;
  [status]=c_forcing(GrdName,FrcName,nhrec,define,grided,Tname,Vname);
  disp(['Created forcing NetCDF file: ',FrcName]);
  disp(' ');
end,

%-----------------------------------------------------------------------
%  Determine starting time record in output forcing file.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(FrcName);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:4) == 'time'),
    Fstr=dsizes(n);
  end,
end,

disp(['Number of records in FORCING file: ',num2str(Fstr)]);
disp(['         Starting writting record: ',num2str(Ostr)]);
disp(' ');
 
if (day_str == 1),
  Cindex=1:1:coamps_wrec;
else
  Cindex=Istr:1:coamps_wrec;
end,

Tstr=Ostr;

%-----------------------------------------------------------------------
%  If applicable, write out heat flux data from Met Tower NetCDF.
%-----------------------------------------------------------------------

[status]=w_heat(MetName,FrcName,icoare,define,grided,Tname,Vname);

%-----------------------------------------------------------------------
%  Write out forcing data from COAMPS file.
%-----------------------------------------------------------------------

% Write all available analyses and 6-hour forecasts.

Findx=[1 2];
Tindx=Cindex;
[status]=w_forcing(GrdName,RegName,FrcName, ...
                   define,grided,Tname,Vname,Findx,Tindx,Tstr);

% Write out the latest COAMPS forecast: 12-48 hous

if (FORECAST),
  Findx=3:1:9;
  Tindx=Cindex(length(Cindex));
  Tstr=Tstr+length(Cindex)*2;
  [status]=w_forcing(GrdName,RegName,FrcName, ...
                     define,grided,Tname,Vname,Findx,Tindx,Tstr);
end,

%-----------------------------------------------------------------------
%  Write out forecast wind stress from NOGAPS file.
%-----------------------------------------------------------------------

%  Write out latest 60-120 hours forecast.

if (FORECAST),
  if (coamps_wrec == nogaps_wrec),
    Findx=10:1:14;
    Tindx=nogaps_wrec;
    Tstr=Tstr+7;
    [status]=w_forcing(GrdName,GloName,FrcName, ...
                       define,grided,Tname,Vname,Findx,Tindx,Tstr);
  else
    error(['D_FORCING: unable to write NOGAPS forecast for record: ',...
          num2str(nogaps_wrec)]);
  end,
end,

%-----------------------------------------------------------------------
%  Check written surface wind stress.
%-----------------------------------------------------------------------

disp(' ');
disp('Checking Written Wind Stress: ');
disp(' ');

check_stress(FrcName);
 