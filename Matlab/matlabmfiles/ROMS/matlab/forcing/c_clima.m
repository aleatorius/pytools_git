function [status]=c_clima(Gname,Cname,Nlev);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=c_clima(Gname,Cname);                              %
%                                                                      %
% This function creates climatology file.                              %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Gname       GRID NetCDF file name (string).                       %
%    Cname       Climatology NetCDF file name (string).                %
%    Nlev        Number of levels (integer).                           %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_dim, nc_read, nc_write                                   %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Get some NetCDF parameters.
%-----------------------------------------------------------------------

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');

% Set floating-point variables type.

vartyp=ncfloat;

%-----------------------------------------------------------------------
%  Inquire GRID file dimensions.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(Gname);

ndims=length(dsizes);
for n=1:ndims,
  name=dnames(n,:);
  switch (name),
    case 'xi_rho',
      dsiz.xr=dsizes(n);
      dnam.xr='xi_rho';
    case 'xi_u',
      dsiz.xu=dsizes(n);
      dnam.xu='xi_u';
    case 'xi_v',
      dsiz.xv=dsizes(n);
      dnam.xv='xi_v';
    case 'eta_rho',
      dsiz.er=dsizes(n);
      dnam.er='eta_rho';
    case 'eta_u',
      dsiz.eu=dsizes(n);
      dnam.eu='eta_u';
    case 'eta_v',
      dsiz.ev=dsizes(n);
      dnam.ev='eta_v';
  end,
end,

dsiz.sr=Nlev;
dnam.sr='s_rho';

cycle=360.0;

%  Set time dimensions and variable names.

define.tclm=1;
dsiz.tclm=1;
dnam.tclm='tclm_time';
tnam.tclm='tclm_time';
vnam.tclm='temp';

define.sclm=1;
dsiz.sclm=1;
dnam.sclm='sclm_time';
tnam.sclm='sclm_time';
vnam.sclm='salt';

define.ssh=0;
dsiz.ssh=1;
dnam.ssh='ssh_time';
tnam.ssh='ssh_time';
vnam.ssh='SSH';

%-----------------------------------------------------------------------
%  Create output NetCDF file.
%-----------------------------------------------------------------------

[ncid,status]=mexcdf('nccreate',Cname,'nc_write');
if (ncid == -1),
  error(['C_CLIMA: ncopen - unable to create file: ', Cname]);
  return
end,

%-----------------------------------------------------------------------
%  Define dimensions.
%-----------------------------------------------------------------------

[did.xr]=mexcdf('ncdimdef',ncid,dnam.xr,dsiz.xr); 
if (did.xr == -1),
  error(['C_CLIMA: ncdimdef - unable to define dimension: ',dnam.xr]);
end,

[did.er]=mexcdf('ncdimdef',ncid,dnam.er,dsiz.er);
if (did.er == -1),
  error(['C_CLIMA: ncdimdef - unable to define dimension: ',dnam.er]);
end,

[did.sr]=mexcdf('ncdimdef',ncid,dnam.sr,dsiz.sr);
if (did.sr == -1),
  error(['C_CLIMA: ncdimdef - unable to define dimension: ',dnam.sr]);
end,

if (define.tclm),
  [did.tclm]=mexcdf('ncdimdef',ncid,dnam.tclm,dsiz.tclm);
  if (did.tclm == -1),
    error(['C_CLIMA: ncdimdef - unable to define dimension: ',dnam.tclm]);
  end,
end,

if (define.sclm),
  [did.sclm]=mexcdf('ncdimdef',ncid,dnam.sclm,dsiz.sclm);
  if (did.sclm == -1),
    error(['C_CLIMA: ncdimdef - unable to define dimension: ',dnam.sclm]);
  end,
end,

if (define.ssh),
  [did.ssh]=mexcdf('ncdimdef',ncid,dnam.ssh,dsiz.ssh);
  if (did.ssh == -1),
    error(['C_CLIMA: ncdimdef - unable to define dimension: ',dnam.ssh]);
  end,
end,

%-----------------------------------------------------------------------
%  Set dimension IDs for all variables.
%-----------------------------------------------------------------------

if (define.tclm),
  vdid.tclm=[did.tclm did.sr did.er did.xr];
end,
if (define.sclm),
  vdid.sclm=[did.sclm did.sr did.er did.xr];
end,
if (define.ssh),
  vdid.ssh=[did.ssh did.er did.xr];
end,

%-----------------------------------------------------------------------
%  Create global attribute(s).
%-----------------------------------------------------------------------

type='CLIMATOLOGY file';
lstr=max(size(type));

[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lstr,type);
if (status == -1),
  error(['C_CLIMA: ncattput - unable to global attribure: history.']);
  return
end

history=['CLIMATOLOGY file, 1.0, ', date_stamp];
lstr=max(size(history));

[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lstr,history);
if (status == -1),
  error(['C_CLIMA: ncattput - unable to global attribure: history.']);
  return
end

%=======================================================================
%  Define FORCING variables
%=======================================================================

%-----------------------------------------------------------------------
%  Define time coordinate(s).
%-----------------------------------------------------------------------

%  Temperature climatology.

if (define.tclm),
  [varid]=mexcdf('ncvardef',ncid,tnam.tclm,ncdouble,1,did.tclm);
  if (varid == -1),
    error(['C_CLIMA: ncvardef - unable to define variable: ',tnam.tclm]);
  end,

  text='time for temperature climatology';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           tnam.tclm,':long_name.']);
  end,

  text='modified Julian day';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.tclm,':units.']);
  end,

  [status]=mexcdf('ncattput',ncid,varid,'cycle_length',ncdouble,1,...
                  cycle);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.tclm,':cycle_length.']);
  end,

  text=[tnam.tclm, ', scalar, series'];
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.tclm,':field.']);
  end,
end,

%  Salinity climatology.

if (define.sclm),
  [varid]=mexcdf('ncvardef',ncid,tnam.sclm,ncdouble,1,did.sclm);
  if (varid == -1),
    error(['C_CLIMA: ncvardef - unable to define variable: ',tnam.sclm]);
  end,

  text='time for salinity climatology';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           tnam.sclm,':long_name.']);
  end,

  text='modified Julian day';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.sclm,':units.']);
  end,

  [status]=mexcdf('ncattput',ncid,varid,'cycle_length',ncdouble,1,...
                  cycle);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.sclm,':cycle_length.']);
  end,

  text=[tnam.sclm, ', scalar, series'];
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.sclm,':field.']);
  end,
end,

%  Sea surface height time.

if (define.ssh),
  [varid]=mexcdf('ncvardef',ncid,tnam.ssh,ncdouble,1,did.ssh);
  if (varid == -1),
    error(['C_CLIMA: ncvardef - unable to define variable: ',tnam.ssh]);
  end,

  text='time for sea surface height';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           tnam.ssh,':long_name.']);
  end,

  text='modified Julian day';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.ssh,':units.']);
  end,

  [status]=mexcdf('ncattput',ncid,varid,'cycle_length',ncdouble,1,...
                  cycle);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.ssh,':cycle_length.']);
  end,

  text=[tnam.ssh, ', scalar, series'];
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
          tnam.ssh,':field.']);
  end,
end,

%-----------------------------------------------------------------------
%  Define variables.
%-----------------------------------------------------------------------

%  Temperature climatology.

if (define.tclm),
  [varid]=mexcdf('ncvardef',ncid,vnam.tclm,vartyp,length(vdid.tclm),...
                 vdid.tclm);
  if (varid == -1),
    error(['C_CLIMA: ncvardef - unable to define variable: ',vnam.tclm]);
  end,

  text='potential temperature';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.tclm,':long_name.']);
  end,

  text='Celsius';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.tclm,':units.']);
  end,

  text='temperature, scalar, series';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.tclm,':field.']);
  end,
end,

%  Salinity climatology.

if (define.sclm),
  [varid]=mexcdf('ncvardef',ncid,vnam.sclm,vartyp,length(vdid.sclm),...
                 vdid.sclm);
  if (varid == -1),
    error(['C_CLIMA: ncvardef - unable to define variable: ',vnam.sclm]);
  end,

  text='salinity';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.sclm,':long_name.']);
  end,

  text='PSU';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.sclm,':units.']);
  end,

  text='salinity, scalar, series';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.sclm,':field.']);
  end,
end,

%  Sea surface height climatology.

if (define.ssh),
  [varid]=mexcdf('ncvardef',ncid,vnam.ssh,vartyp,length(vdid.ssh),...
                 vdid.ssh);
  if (varid == -1),
    error(['C_CLIMA: ncvardef - unable to define variable: ',vnam.ssh]);
  end,

  text='sea surface height';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'long_name',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.ssh,':long_name.']);
  end,

  text='meter';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'units',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.ssh,':units.']);
  end,

  text='SSH, scalar, series';
  lstr=max(size(text));
  [status]=mexcdf('ncattput',ncid,varid,'field',ncchar,lstr,text);
  if (status == -1),
    error(['C_CLIMA: ncattput - unable to define attribute: ',...
           vnam.ssh,':field.']);
  end,
end,

%-----------------------------------------------------------------------
%  Leave definition mode.
%-----------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['C_CLIMA: ncendef - unable to leave definition mode.'])
end

%-----------------------------------------------------------------------
%  Close NetCDF file.
%-----------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['C_CLIMA: ncclose - unable to close NetCDF file: ', Cname])
end

return





