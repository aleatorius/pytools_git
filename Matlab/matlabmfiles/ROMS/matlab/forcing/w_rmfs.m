function [status]=w_rmfs(Fname,Mname,define,Tname,Vname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2000 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=w_rmfs(Fname,Mname,define,Tname,Vname)             %
%                                                                      %
% This function reads in Meterological Tower NetCDF file and writes    %
% surface wind stress component, surface net heat flux and shortwave   %
% radiation into FORCING NetCDF file.                                  %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Fname       FORCING NetCDF file (string).                         %    
%    Mname       Met Tower COARE NetCDF file name(s) (string matrix).  %
%    define      Switches indicating which variables to define         %
%                  (0: no, 1: yes), structure array.                   %
%    Tname       Name of defined time NetCDF variables.                %
%    Vname       Name of defined forcing NetCDF variables.             %
%                                                                      %
%                Variables in structure arrays:                        %
%                                                                      %
%                  ***.wind  => surface wind components.               %
%                  ***.sms   => surface wind stress.                   %
%                  ***.pair  => surface air pressure.                  %
%                  ***.tair  => surface air temperature.               %
%                  ***.qair  => surface relative humidity.             %
%                  ***.cloud => cloud fraction.                        %
%                  ***.rain  => rain fall rate.                        %
%                  ***.evap  => evaporation rate.                      %
%                  ***.srf   => shortwave radiation flux.              %
%                  ***.lrf   => longwave radiation flux.               %
%                  ***.lhf   => net latent heat flux.                  %
%                  ***.shf   => net sensible heat flux.                %
%                  ***.nhf   => surface net heat flux.                 %
%                  ***.nff   => surface net freshwater flux.           %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status      Error flag (integer).                                 %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_read, wind_stress                                        %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;


%-----------------------------------------------------------------------
% Read in forcing fields.
%-----------------------------------------------------------------------

[Mfiles,lstr]=size(Mname);

if (define.wind),
  time.wind=[];
  data.suw =[];
  data.svw =[];
end,  

if (define.sms),
  time.sms=[];
  data.sus=[];
  data.svs=[];
end,

if (define.tair),
  time.tair=[];
  data.tair=[];
end,

if (define.pair),
  time.pair=[];
  data.pair=[];
end,

if (define.qair),
  time.qair=[];
  data.qair=[];
end,

if (define.srf),
  time.srf=[];
  data.srf=[];
end,

if (define.lrf),
  time.lrf=[];
  data.lrf=[];
end,

if (define.lhf),
  time.lhf=[];
  data.lhf=[];
end,

if (define.shf),
  time.shf=[];
  data.shf=[];
end,

if (define.nhf),
  time.nhf=[];
  data.nhf=[];
end,

for n=1:Mfiles,

  file=Mname(n,:);

  t=nc_read(file,'time');

  if (define.wind),
    u=nc_read(file,Vname.suw);
    v=nc_read(file,Vname.svw);
    time.wind=[time.wind; t];
    data.suw =[data.suw; u];
    data.svw =[data.svw; v];
  end,  

  if (define.sms),
    u=nc_read(file,Vname.sus);
    v=nc_read(file,Vname.svs);
    time.sms=[time.sms; t];
    data.sus=[data.sus; u];
    data.svs=[data.svs; v];
  end,

  if (define.tair),
    f=nc_read(file,Vname.tair);
    time.tair=[time.tair; t];
    data.tair=[data.tair; f];
  end,

  if (define.pair),
    f=nc_read(file,Vname.pair);
    time.pair=[time.pair; t];
    data.pair=[data.pair; f];
  end,

  if (define.qair),
    f=nc_read(file,Vname.qair);
    time.qair=[time.qair; t];
    data.qair=[data.qair; f];
  end,

  if (define.srf),
    f=nc_read(file,Vname.srf);
    time.srf=[time.srf; t];
    data.srf=[data.srf; f];
  end,

  if (define.lrf),
    f=nc_read(file,Vname.lrf);
    time.lrf=[time.lrf; t];
    data.lrf=[data.lrf; f];
  end,

  if (define.lhf),
    f=nc_read(file,Vname.lhf);
    time.lhf=[time.lhf; t];
    data.lhf=[data.lhf; f];
  end,

  if (define.shf),
    f=nc_read(file,Vname.shf);
    time.shf=[time.shf; t];
    data.shf=[data.shf; f];
  end,

end,

time.nhf=time.srf;
data.nhf=data.srf + data.lrf + data.lhf + data.shf;

%-----------------------------------------------------------------------
%  Write out forcing fields.
%-----------------------------------------------------------------------

if (define.wind),
  [status]=nc_write(Fname,Tname.wind,time.wind);
  [status]=nc_write(Fname,Vname.suw ,data.suw);
  [status]=nc_write(Fname,Vname.svw ,data.svw);
end,  

if (define.sms),
  [status]=nc_write(Fname,Tname.sms,time.sms);
  [status]=nc_write(Fname,Vname.sus,data.sus);
  [status]=nc_write(Fname,Vname.svs,data.svs);
end,

if (define.tair),
  [status]=nc_write(Fname,Tname.tair,time.tair);
  [status]=nc_write(Fname,Vname.tair,data.tair);
end,

if (define.pair),
  [status]=nc_write(Fname,Tname.pair,time.pair);
  [status]=nc_write(Fname,Vname.pair,data.pair);
end,

if (define.qair),
  [status]=nc_write(Fname,Tname.qair,time.qair);
  [status]=nc_write(Fname,Vname.qair,data.qair);
end,

if (define.srf),
  [status]=nc_write(Fname,Tname.srf,time.srf);
  [status]=nc_write(Fname,Vname.srf,data.srf);
end,

if (define.lrf),
  [status]=nc_write(Fname,Tname.lrf,time.lrf);
  [status]=nc_write(Fname,Vname.lrf,data.lrf);
end,

if (define.lhf),
  [status]=nc_write(Fname,Tname.lhf,time.lhf);
  [status]=nc_write(Fname,Vname.lhf,data.lhf);
end,

if (define.shf),
  [status]=nc_write(Fname,Tname.shf,time.shf);
  [status]=nc_write(Fname,Vname.shf,data.shf);
end,

if (define.nhf),
  [status]=nc_write(Fname,Tname.nhf,time.nhf);
  [status]=nc_write(Fname,Vname.nhf,data.nhf);
end,

return
