%
%  This scritp process ROMS forcing NetCDF file from RMFS data only.
%

Cname(1,:)='/coamps/met/Jul00/Tuckerton/jun00.coare';
Cname(2,:)='/coamps/met/Jul00/Tuckerton/jul00.coare';

Mname(1,:)='/coamps/met/Jul00/Tuckerton/coare_jun00.nc';
Mname(2,:)='/coamps/met/Jul00/Tuckerton/coare_jul00.nc';

Gname='/n7/arango/NJB/Jul00/Data/njb1_grid_a.nc';
Fname='/n7/arango/NJB/Jul00/Data/njb1_rmfs_sum00.nc';

%-----------------------------------------------------------------------
%  Set switches and names of variables to process.
%-----------------------------------------------------------------------

%  Set structure arrays: define (variables to define), grided
%  (point or grided data), Tname (name of time variable), and
%  Vname (name of forcing variable).  Switches are: 0=no, 1=yes.

define.wind =1;             % surface wind components
grided.wind =0;
Tname.wind  ='wind_time';
Vname.suw   ='Uwind';
Vname.svw   ='Vwind';

define.sms  =1;             % surface wind stress
grided.sms  =0;
Tname.sms   ='sms_time';
Vname.sus   ='sustr';
Vname.svs   ='svstr';

define.pair =1;             % surface air pressure
grided.pair =0;
Tname.pair  ='pair_time';
Vname.pair  ='Pair';

define.tair =1;             % surface air temperature
grided.tair =0;
Tname.tair  ='tair_time';
Vname.tair  ='Tair';

define.qair =1;             % surface relative humidity
grided.qair =0;
Tname.qair  ='qair_time';
Vname.qair  ='Qair';

define.cloud=0;             % cloud fraction
grided.cloud=0;
Tname.cloud ='cloud_time';
Vname.cloud ='cloud';

define.rain =0;             % rain fall rate
grided.rain =0;
Tname.rain  ='rain_time';
Vname.rain  ='rain';

define.evap =0;             % evaporation rate
grided.evap =0;
Tname.evap  ='evap_time';
Vname.evap  ='evap';

define.srf  =1;             % shortwave radiation flux
grided.srf  =0;
Tname.srf   ='srf_time';
Vname.srf   ='swrad';

define.lrf  =1;             % longwave radiation flux
grided.lrf  =0;
Tname.lrf   ='lrf_time';
Vname.lrf   ='lwrad';

define.lhf  =1;             % latent heat flux
grided.lhf  =0;
Tname.lhf   ='lhf_time';
Vname.lhf   ='latent';

define.shf  =1;             % sensible heat flux
grided.shf  =0;
Tname.shf   ='sen_time';
Vname.shf   ='sensible';

define.nhf  =1;             % surface net heat flux
grided.nhf  =0;
Tname.nhf   ='shf_time';
Vname.nhf   ='shflux';

define.nff  =0;             % surface net freshwater flux
grided.nff  =0;
Tname.nff   ='swf_time';
Vname.nff   ='swflux';

%-----------------------------------------------------------------------
%  Create RMFS July file.
%-----------------------------------------------------------------------

[Nfiles,lstr]=size(Cname);

for n=1:Nfiles
  [status]=c_coare(Mname(n,:));
  [met]=avg_coare(Cname(n,:));
  [status]=w_coare(Mname(n,:),met);
end,

%-----------------------------------------------------------------------
%  Inquire size of time record in RMFS NetCDF files.
%-----------------------------------------------------------------------

[Nfiles,lstr]=size(Mname);

nrec=0;

for n=1:Nfiles
  file=deblank(Mname(n,:));
  [dnames,dsizes]=nc_dim(file);
  ndims=length(dsizes);
  for m=1:ndims,
    name=deblank(dnames(m,:));
    switch name
      case 'time'
        nrec=nrec+dsizes(m);
    end,
  end,
end,

%-----------------------------------------------------------------------
%  Create FORCING NetCDF.
%-----------------------------------------------------------------------

[status]=c_forcing(Gname,Fname,nrec,define,grided,Tname,Vname);

%-----------------------------------------------------------------------
%  Write out RMFS data into FORCING NetCDF file.
%-----------------------------------------------------------------------

[status]=w_rmfs(Fname,Mname,define,Tname,Vname);
