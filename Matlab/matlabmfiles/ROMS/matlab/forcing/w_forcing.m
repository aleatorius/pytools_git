function [status]=w_forcing(Gname,Nname,Fname, ...
                            define,grided,Tname,Vname,Findx,Tindx,Rstr);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  [status]=w_forcing(Gname,Nname,Fname, ...                           %
%                     define,grided, Tname,Vname,Findx,Tindx,Rstr)     %
%                                                                      %
% This function reads in forcing data from COAMPS or NOGAPS NetCDF     %
% and interpolates and rotates vectors to model grid.  Then,  it       %
% writes out data into output Forcing NetCDF file.                     %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Gname       GRID NetCDF file name (string).                       %
%    Nname       COAMPS or NOGAPS NetCDF file name (string).           %
%    Fname       FORCING NetCDF file (string).                         %    
%    define      Switches indicating which variables were defined      %
%                  (0: no, 1: yes), structure array.                   %
%    grided      Switches indicating which grid type for each          %
%                  variable (0: point, 1: grided), structure array.    %
%    Tname       Name of defined time NetCDF variables.                %
%    Vname       Name of defined forcing NetCDF variables.             %
%    Findx       Forecast index (integer scalar or vector).            %
%    Tindx       Time record to process from COAMPS/NOGAPS NetCDF      %
%                  file (integer scalar or vector).                    %
%    Rstr        Starting time record in forcing file to write.        %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status      Error flag (integer).                                 %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_read, wind_stress                                        %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

method='linear';

%-----------------------------------------------------------------------
% Read in model grid positions and angle.
%-----------------------------------------------------------------------

rlon=nc_read(Gname,'lon_rho');
rlat=nc_read(Gname,'lat_rho');

angle=nc_read(Gname,'angle');

[Lp,Mp]=size(rlon);
L=Lp-1;
M=Mp-1;

%-----------------------------------------------------------------------
% Read in COAMPS/NOGAPS positions.
%-----------------------------------------------------------------------

Lon=nc_read(Nname,'lon');
Lat=nc_read(Nname,'lat');

[Im Jm]=size(Lon);

delta=[0 6 12 18 24 30 36 42 48 60 72 84 96 120];
delta=delta./24;

% Set input variable names.

got.tair=0;
got.pair=0';
got.qair=0';
got.rain=0';
got.wind=0';

[vname,nvars]=nc_vname(Nname);
for n=1:nvars,
  name=deblank(vname(n,:));
  switch (name),  
    case 'Tair',
      got.tair=1;
      Fvar.tair='Tair';
    case 'Pair',
      got.pair=1;
      Fvar.pair='Pair';
    case 'humidity',
      got.qair=1;
      Fvar.qair='humidity';
    case 'rain',
      got.rain=1;
      Fvar.rain='rain';
    case 'Uwind',
      got.wind=1;
      Fvar.suw ='Uwind';
    case 'Vwind',
      got.wind=1;
      Fvar.svw ='Vwind';
  end,
end,

%-----------------------------------------------------------------------
% Process in COAMPS or NOGAPS wind.
%-----------------------------------------------------------------------

Nrec=length(Tindx);
Nfor=length(Findx);

Tstr=Rstr-1;

disp(' ');
disp(['Input forcing file: ', Nname]);
disp(['      Time records: ', num2str(Tindx)]);
disp(['  Forecast records: ', num2str(Findx)]);
disp(' ');

for n=1:Nrec,

  Trec=Tindx(n);
  time=nc_read(Nname,'time',Trec);

% Read in forcing data.

  if (define.tair & grided.tair & got.tair),
    Tair=nc_read(Nname,Fvar.tair,Trec);
  end,

  if (define.pair & grided.pair & got.pair),
    Pair=nc_read(Nname,Fvar.pair,Trec);
  end,

  if (define.qair & grided.qair & got.qair),
    Qair=nc_read(Nname,Fvar.qair,Trec);
  end,

  if (define.rain & grided.rain & got.rain),
    rain=nc_read(Nname,Fvar.rain,Trec);
  end,

  if (got.wind & (define.wind & grided.wind) | (define.sms & grided.sms)),
    Uair=nc_read(Nname,Fvar.suw,Trec);
    Vair=nc_read(Nname,Fvar.svw,Trec);
  end,

% Interpolate data to model grid for each requested forecast.

  for m=1:Nfor,

    Frec=Findx(m);
    Tstr=Tstr+1;

    if (define.tair & grided.tair & got.tair),
      Finp=reshape(Tair(:,:,Frec),Im,Jm);
      [Fout]=griddata(Lon,Lat,Finp,rlon,rlat);
      [status]=nc_write(Fname,Vname.tair,Fout,Tstr);
      [status]=nc_write(Fname,Tname.tair,time+delta(Frec),Tstr);
    end,

    if (define.pair & grided.pair & got.pair),
      Finp=reshape(Pair(:,:,Frec),Im,Jm);
      [Fout]=griddata(Lon,Lat,Finp,rlon,rlat);
      [status]=nc_write(Fname,Vname.pair,Fout,Tstr);
      [status]=nc_write(Fname,Tname.pair,time+delta(Frec),Tstr);
    end,

    if (define.qair & grided.qair & got.qair),
      Finp=reshape(Qair(:,:,Frec),Im,Jm);
      [Fout]=griddata(Lon,Lat,Finp,rlon,rlat);
      [status]=nc_write(Fname,Vname.qair,Fout,Tstr);
      [status]=nc_write(Fname,Tname.qair,time+delta(Frec),Tstr);
    end,

    if (define.rain & grided.rain & got.rain),
      Finp=reshape(rain(:,:,Frec),Im,Jm);
      [Fout]=griddata(Lon,Lat,Finp,rlon,rlat);
      [status]=nc_write(Fname,Vname.rain,Fout,Tstr);
      [status]=nc_write(Fname,Tname.rain,time+delta(Frec),Tstr);
    end,

    if (define.wind & grided.wind & got.wind),
      Uinp=reshape(Uair(:,:,Frec),Im,Jm);
      Vinp=reshape(Vair(:,:,Frec),Im,Jm);
      [Uout]=griddata(Lon,Lat,Uinp,rlon,rlat);
      [Vout]=griddata(Lon,Lat,Vinp,rlon,rlat);
      Fout=Uout.*cos(angle)+Vout.*sin(angle);
      [status]=nc_write(Fname,Vname.suw,Fout,Tstr);
      Fout=Vout.*cos(angle)-Uout.*sin(angle);
      [status]=nc_write(Fname,Vname.svw,Fout,Tstr);
      [status]=nc_write(Fname,Tname.wind,time+delta(Frec),Tstr);
    end,

    if (define.sms & grided.sms & got.wind),
      [Uinp,Vinp]=wind_stress(Uair(:,:,Frec),Vair(:,:,Frec));
      [Uout]=griddata(Lon,Lat,Uinp,rlon,rlat);
      [Vout]=griddata(Lon,Lat,Vinp,rlon,rlat);
      Frot=Uout.*cos(angle)+Vout.*sin(angle);
      Fout=0.5.*(Frot(1:L,1:Mp)+Frot(2:Lp,1:Mp));
      [status]=nc_write(Fname,Vname.sus,Fout,Tstr);
      Frot=Vout.*cos(angle)-Uout.*sin(angle);
      Fout=0.5.*(Frot(1:Lp,1:M)+Frot(1:Lp,2:Mp));
      [status]=nc_write(Fname,Vname.svs,Fout,Tstr);
      [status]=nc_write(Fname,Tname.sms,time+delta(Frec),Tstr);
    end,   

% Report records of processed data.

    gtime=gregorian(time+delta(Frec)+2440000.0);
    disp(['Processed grid  data for: ', num2str(gtime(1)), ' ',...
          num2str(gtime(2),'%2.2i'), ' ', num2str(gtime(3),'%2.2i'),...
          ' ', num2str(gtime(4),'%2.2i'),...
          ', Delta=',num2str(delta(Frec),'%4.2f'),...
          ', output record > ',num2str(Tstr,'%3.3i')]);

  end,
end,

return
