function [status]=w_heat(Mname,Fname,iflag,define,grided,Tname,Vname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% [status]=w_heat(Mname,Fname,iflag,define,grided,Tname,Vname)         %
%                                                                      %
% This function reads in Meterological Tower NetCDF file and writes    %
% surface net heat flux and shortwave radiation into FORCING NetCDF    %
% file.                                                                %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Mname       Met Tower NetCDF file name (string).                  %
%    Fname       FORCING NetCDF file (string).                         %    
%    iflag       Data origin switch:                                   %
%                  iflag=0,  an existing forcing NetCDF file.          %
%                  iflag=1,  COARE NetCDF file.                        %
%    define      Switches indicating which variables were defined      %
%                  (0: no, 1: yes), structure array.                   %
%    grided      Switches indicating which grid type for each          %
%                  variable (0: point, 1: grided), structure array.    %
%    Tname       Name of defined time NetCDF variables.                %
%    Vname       Name of defined forcing NetCDF variables.             %
%                                                                      %
%                Variables in structure arrays:                        %
%                                                                      %
%                  ***.wind  => surface wind components.               %
%                  ***.sms   => surface wind stress.                   %
%                  ***.pair  => surface air pressure.                  %
%                  ***.tair  => surface air temperature.               %
%                  ***.qair  => surface relative humidity.             %
%                  ***.cloud => cloud fraction.                        %
%                  ***.rain  => rain fall rate.                        %
%                  ***.evap  => evaporation rate.                      %
%                  ***.srf   => shortwave radiation flux.              %
%                  ***.lrf   => longwave radiation flux.               %
%                  ***.lhf   => latent heat flux.                      %
%                  ***.shf   => sensible heat flux.                    %
%                  ***.nhf   => surface net heat flux.                 %
%                  ***.nff   => surface net freshwater flux.           %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status      Error flag (integer).                                 %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_read, wind_stress                                        %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

%-----------------------------------------------------------------------
% Read in heat flux components.
%-----------------------------------------------------------------------

if (iflag == 0),

  heat=nc_read(Mname,Vname.nhf);
  tnhf=nc_read(Mname,Tname.nhf);

  swrad=nc_read(Mname,Vname.srf);
  tsrf =nc_read(Mname,Tname.srf);

elseif (iflag == 1),

  time=nc_read(Mname,'time');

  swrad=nc_read(Mname,'swrad');
  tsrf=time;

  lwrad=nc_read(Mname,'lwrad');
  tlrf=time;

  latent=nc_read(Mname,'latent');
  tlhf=time;

  sensible=nc_read(Mname,'sensible');
  tshf=time;

  heat=sensible+latent+swrad+lwrad;
  tnhf=time;

end,

%-----------------------------------------------------------------------
% Write out heat flux components.
%-----------------------------------------------------------------------

% Surface net heat flux.

if (~grided.nhf & define.nhf),
  [status]=nc_write(Fname,Tname.nhf,tnhf);
  [status]=nc_write(Fname,Vname.nhf,heat);
  disp(['Processed point data for: surface net heat flux.']);
end,

% Shortwave radiation flux.

if (~grided.srf & define.srf),
  [status]=nc_write(Fname,Tname.srf,tsrf);
  [status]=nc_write(Fname,Vname.srf,swrad);
  disp(['Processed point data for: shortwave radiation flux.']);
end,

% Longwave radiation flux.

if (~grided.lrf & define.lrf & iflag),
  [status]=nc_write(Fname,Tname.lrf,tlrf);
  [status]=nc_write(Fname,Vname.lrf,lwrad);
  disp(['Processed point data for: longwave radiation flux.']);
end,

% Latent heat flux.

if (~grided.lhf & define.lhf & iflag),
  [status]=nc_write(Fname,Tname.lhf,tlhf);
  [status]=nc_write(Fname,Vname.lhf,latent);
  disp(['Processed point data for: latent heat flux.']);
end,

% Sensible heat flux.

if (~grided.shf & define.shf & iflag),
  [status]=nc_write(Fname,Tname.shf,tshf);
  [status]=nc_write(Fname,Vname.shf,sensible);
  disp(['Processed point data for: sensible heat flux.']);
end,

disp(' ');

return
