function check_stress(Fname)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  This function checks the wind stress magnitude in FORCING NetCDF    %
%  file.                                                               %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Fname       FORCING NetCDF file (string).                         %    
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

%-----------------------------------------------------------------------
%  Read in time coordinate.
%-----------------------------------------------------------------------

time=nc_read(Fname,'sms_time');
nrec=length(time);

%-----------------------------------------------------------------------
%  Read in wind stress components and report its maximum values.
%-----------------------------------------------------------------------

for n=1:nrec,
  u=nc_read(Fname,'sustr',n);
  v=nc_read(Fname,'svstr',n);
  [L,Mp]=size(u);
  Lp=L+1;
  M=Mp-1;
  Mag=sqrt((0.5.*(u(1:L,1:M)+u(1:L,2:Mp))).^2 + ...
           (0.5.*(v(1:L,1:M)+v(2:Lp,1:M))).^2);
  Mmin=min(min(Mag));
  Mmax=max(max(Mag));
  gtime=gregorian(time(n)+2440000.0);
  if (Mmin < 1),
    disp(['Record: ',num2str(n,'%3.3i'),'  Date: ',...,
         num2str(gtime(1)),' ',num2str(gtime(2),'%2.2i'),' ',...
         num2str(gtime(3),'%2.2i'),' ',num2str(gtime(4),'%2.2i'),...,
         '  Wind Stress Range: ',num2str(Mmin,'%10.3e'),' -  ',...,
         num2str(Mmax,'%10.3e')]);
  else
    disp(['Record: ',num2str(n,'%3.3i'),'  Date: ',...,
         num2str(gtime(1)),' ',num2str(gtime(2),'%2.2i'),' ',...
         num2str(gtime(3),'%2.2i'),' ',num2str(gtime(4),'%2.2i'),...,
         '  Wind Stress Range: ',num2str(Mmin,'%10.3e'),' -  ',...,
         num2str(Mmax,'%10.3e'), ' <==']);
  end,
end,

return




