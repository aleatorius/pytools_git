function [status]=w_stress(Gname,Nname,Fname,forecast,tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1998 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=w_stress(Gname,Nname,Fname,forecast,tindex)        %
%                                                                      %
% This function reads in wind data from NORAPS or NOGAPS NetCDF        %
% and interpolates and rotates to model grid and then it writes        %
% out into output Forcing NetCDF file.                                 %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Gname       GRID NetCDF file name (string).                       %
%    Nname       NORAPS or NOGAPS NetCDF file name (string).           %
%    Fname       FORCING NetCDF file (string).                         %    
%    forecast    Forecast index (integer scalar or vector).            %
%    tindex      Time record to process from NORAPS/NOGAPS NetCDF      %
%                  file (integer scalar or vector).                    %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status      Error flag (integer).                                 %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_read, wind_stress                                        %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

method='linear';

%-----------------------------------------------------------------------
% Read in model grid positions and angle.
%-----------------------------------------------------------------------

rlon=nc_read(Gname,'lon_rho');
rlat=nc_read(Gname,'lat_rho');

angle=nc_read(Gname,'angle');

[Lp,Mp]=size(rlon);
L=Lp-1;
M=Mp-1;

%-----------------------------------------------------------------------
% Read in NORAPS/NOGAPS positions.
%-----------------------------------------------------------------------

Lon=nc_read(Nname,'lon');
Lat=nc_read(Nname,'lat');

delta=[0 6 12 18 24 30 36 42 48 60 72 84 96 120];
delta=delta./24;

[dnames,dsizes]=nc_dim(Fname);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:8) == 'sms_time'),
    Tstr=dsizes(n);
  end,
end,

%-----------------------------------------------------------------------
% Process in NORAPS or NOGAPS wind.
%-----------------------------------------------------------------------

Nrec=length(tindex);
Nfor=length(forecast);

for n=1:Nrec,

% Read in wind data.

  Trec=tindex(n);
  uwind=nc_read(Nname,'uwind',Trec);
  vwind=nc_read(Nname,'vwind',Trec);
  time=nc_read(Nname,'time',Trec);

% Compute wind stress.

  for m=1:Nfor,

    Frec=forecast(m);
    [TauX,TauY]=wind_stress(uwind(:,:,m),vwind(:,:,m),1);

% Interpolate to model grid.

    [Ustr]=interp2(Lon',Lat',TauX',rlon,rlat,method);
    [Vstr]=interp2(Lon',Lat',TauY',rlon,rlat,method);

% Rotate to model grid.

    taux=Ustr.*cos(angle)+Vstr.*sin(angle);
    tauy=Vstr.*cos(angle)-Ustr.*sin(angle);
    
% Average to U- and V-points.

    sustr=0.5.*(taux(1:L,1:Mp)+taux(2:Lp,1:Mp));
    svstr=0.5.*(tauy(1:Lp,1:M)+tauy(1:Lp,2:Mp));

% Write out into FORCING NetCDF file.

    Tstr=Tstr+1;

    [status]=nc_write(Fname,'sustr',sustr,Tstr);
    [status]=nc_write(Fname,'svstr',svstr,Tstr);
    [status]=nc_write(Fname,'sms_time',time+delta(Frec),Tstr);

    gtime=gregorian(time+delta(Frec)+2440000.0);
    disp(['Processed data for: ', num2str(gtime(1)), ' ',...
          num2str(gtime(2),'%2.2i'), ' ', num2str(gtime(3),'%2.2i'),...
          ' ', num2str(gtime(4),'%2.2i'),...
          ', Delta=',num2str(delta(Frec),'%4.2f'),...
          ', output record > ',num2str(Tstr,'%3.3i')]);
  end,
end,

return
