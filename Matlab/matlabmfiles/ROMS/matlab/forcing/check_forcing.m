function [h]=check_forcing(Aname,Oname,Gname,Avar,Ovar,Findx,Aindx,Oindx);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 1999 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  [h]=check_forcing(Aname,Oname,Gname,Avar,Ovar,Findx,Aindx,Oindx)    %
%                                                                      %
%  This function plots ocean and atmospheric FORCING NetCDF files.     %
%  This is to check the processing of atmospheric data into ocean      %
%  grid.                                                               %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Aname       Atmosphere FORCING NetCDF file (string).              %    
%    Oname       Ocean FORCING NetCDF file (string).                   %    
%    Gname       Ocean GRID NetCDF file (string).                      %    
%    Avar        Name of atmosphere variable to plot (string).         %
%    Ovar        Name of ocean variable to plot (string).              %
%    Findx       Forecast index of atmosphere variable.                %
%    Aindx       Time record of atmosphere variable to plot.           %
%    Oindx       Time record of ocean variable to plot.                %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

%-----------------------------------------------------------------------
%  Read in requested variable from atmophere file.
%-----------------------------------------------------------------------

Alon=nc_read(Aname,'lon');
Alat=nc_read(Aname,'lat');

Adat=nc_read(Aname,Avar,Aindx);

%-----------------------------------------------------------------------
%  Read in requested variable from ocean file.
%-----------------------------------------------------------------------

Olon=nc_read(Gname,'lon_rho');
Olat=nc_read(Gname,'lat_rho');

LonMin=min(min(Olon));
LonMax=max(max(Olon));
LatMin=min(min(Olat));
LatMax=max(max(Olat));

Odat=nc_read(Oname,Ovar,Oindx);

%-----------------------------------------------------------------------
%  Plot requested variable.
%-----------------------------------------------------------------------

figure;
pcolor(Olon,Olat,Odat); shading interp; colorbar; grid on;
title(['Ocean File: ',Ovar]);

figure;
pcolor(Alon,Alat,Adat(:,:,Findx)); shading interp; colorbar;
set(gca,'Xlim',[LonMin LonMax],'Ylim',[LatMin LatMax]);
title(['Atmosphere File: ',Avar]);

return
