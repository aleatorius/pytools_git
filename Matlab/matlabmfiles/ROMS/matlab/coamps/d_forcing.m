%
%  This scritp process ROMS forcing NetCDF file from new COAMPS NetCDF
%  (Shouping hindcasts).
%

ICREATE=0;        % Create a new FORCING NetCDF file	

Cname='/coamps/mel/Jul01/netcdf/Fcoamps_jul01.nc';
Gname='/n7/arango/NJB/Jul00/Data/njb1_grid_a.nc';
Fname='/n7/arango/NJB/Jul01/Data/njb1_Fcoamps_jul01.nc';

%  The starting and ending days is by half our intervals.

day_str=32.0;                  % Starting July day - ready

Istr=1+(day_str-1.0)*48;       % starting input record
Ostr=1+(day_str-1.0)*48;       % starting output record

%-----------------------------------------------------------------------
%  Set switches and names of variables to process.
%-----------------------------------------------------------------------

%  Set structure arrays: define (variables to define), grided
%  (point or grided data), Tname (name of time variable), and
%  Vname (name of forcing variable).  Switches are: 0=no, 1=yes.

define.wind =1;             % surface wind components
grided.wind =1;
Tname.wind  ='wind_time';
Vname.suw   ='Uwind';
Vname.svw   ='Vwind';

define.sms  =0;             % surface wind stress
grided.sms  =1;
Tname.sms   ='sms_time';
Vname.sus   ='sustr';
Vname.svs   ='svstr';

define.pair =1;             % surface air pressure
grided.pair =1;
Tname.pair  ='pair_time';
Vname.pair  ='Pair';

define.tair =1;             % surface air temperature
grided.tair =1;
Tname.tair  ='tair_time';
Vname.tair  ='Tair';

define.qair =1;             % surface relative humidity
grided.qair =1;
Tname.qair  ='qair_time';
Vname.qair  ='Qair';

define.cloud=0;             % cloud fraction
grided.cloud=0;
Tname.cloud ='cloud_time';
Vname.cloud ='cloud';

define.rain =1;             % rain fall rate
grided.rain =1;
Tname.rain  ='rain_time';
Vname.rain  ='rain';

define.evap =0;             % evaporation rate
grided.evap =0;
Tname.evap  ='evap_time';
Vname.evap  ='evap';

define.srf  =1;             % shortwave radiation flux
grided.srf  =1;
Tname.srf   ='srf_time';
Vname.srf   ='swrad';

define.lrf  =1;             % longwave radiation flux
grided.lrf  =1;
Tname.lrf   ='lrf_time';
Vname.lrf   ='lwrad';

define.lhf  =0;             % latent heat flux
grided.lhf  =0;
Tname.lhf   ='lhf_time';
Vname.lhf   ='latent';

define.shf  =0;             % sensible heat flux
grided.shf  =0;
Tname.shf   ='sen_time';
Vname.shf   ='sensible';

define.nhf  =0;             % surface net heat flux
grided.nhf  =0;
Tname.nhf   ='shf_time';
Vname.nhf   ='shflux';

define.nff  =0;             % surface net freshwater flux
grided.nff  =0;
Tname.nff   ='swf_time';
Vname.nff   ='swflux';

%-----------------------------------------------------------------------
%  Inquire size of time record in COAMPS NetCDF file.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(Cname);
ndims=length(dsizes);
for n=1:ndims,
  name=dnames(n,:);
  if (name(1:4) == 'time'),
    crec=dsizes(n);
  end,
end,
disp(['Number of records in COAMPS file: ',num2str(crec)]);

%-----------------------------------------------------------------------
%  Create Forcing NetCDF file.
%-----------------------------------------------------------------------

if ( ICREATE ),
  day_str=1;
  nhrec=0;
  [status]=c_forcing(Gname,Fname,nhrec,define,grided,Tname,Vname);
  disp(['Created forcing NetCDF file: ',Fname]);
  disp(' ');
end,

%-----------------------------------------------------------------------
%  Determine starting time record in output forcing file.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(Fname);
ndims=length(dsizes);

for n=1:ndims,
  name=dnames(n,:);
  if (name(1:4) == 'time'),
    Fstr=dsizes(n);
  end,
end,

disp(['Number of records in FORCING file: ',num2str(Fstr)]);
disp(['         Starting writting record: ',num2str(Ostr)]);
disp(' ');

if (day_str == 1),
  Cindex=1:1:crec;
else
  Cindex=Istr:1:crec;
end,

%-----------------------------------------------------------------------
%  Interpolate and write out forcing data from COAMPS input file.
%-----------------------------------------------------------------------

Tindx=Cindex;
[status]=w_forcing(Gname,Cname,Fname,define,grided,Tname,Vname,Tindx);
