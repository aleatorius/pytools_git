function [status]=c_forcing(gname,fname,nrec,define,grided,Tname,Vname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2001 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% [status]=c_forcing(gname,fname,nrec,define,grided,Tname,Vname)       %
%                                                                      %
% This function creates forcing NetCDF file.                           %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    gname       GRID NetCDF file name (string).                       %
%    fname       FORCING NetCDF file name (string).                    %
%    nrec        Size of point data variables (Met Station Data).      %
%    define      Switches indicating which variables to define         %
%                  (0: no, 1: yes), structure array.                   %
%    grided      Switches indicating which grid to define for each     %
%                  variable (0: point, 1: grided), structure array.    %
%    Tname       Name of defined time NetCDF variables.                %
%    Vname       Name of defined forcing NetCDF variables.             %
%                                                                      %
%                Variables in structure arrays:                        %
%                                                                      %
%                  ***.wind  => surface wind components.               %
%                  ***.sms   => surface wind stress.                   %
%                  ***.pair  => surface air pressure.                  %
%                  ***.tair  => surface air temperature.               %
%                  ***.qair  => surface relative humidity.             %
%                  ***.cloud => cloud fraction.                        %
%                  ***.rain  => rain fall rate.                        %
%                  ***.evap  => evaporation rate.                      %
%                  ***.srf   => shortwave radiation flux.              %
%                  ***.lrf   => longwave radiation flux.               %
%                  ***.lhf   => net latent heat flux.                  %
%                  ***.shf   => net sensible heat flux.                %
%                  ***.nhf   => surface net heat flux.                 %
%                  ***.nff   => surface net freshwater flux.           %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_dim, nc_read, nc_write                                   %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Get some NetCDF parameters.
%-----------------------------------------------------------------------

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');

% Set floating-point variables type.

vartyp=ncfloat;
rgrid=0;

%-----------------------------------------------------------------------
%  Inquire GRID file dimensions.
%-----------------------------------------------------------------------

[Dnames,Dsizes]=nc_dim(gname);

ndims=length(Dsizes);
for n=1:ndims,
  name=deblank(Dnames(n,:));
  switch name
    case 'xi_rho',
      Dsize.xr=Dsizes(n);
      Dname.xr='xi_rho';
    case 'xi_u',
      Dsize.xu=Dsizes(n);
      Dname.xu='xi_u';
    case 'xi_v',
      Dsize.xv=Dsizes(n);
      Dname.xv='xi_v';
    case 'eta_rho',
      Dsize.er=Dsizes(n);
      Dname.er='eta_rho';
    case 'eta_u',
      Dsize.eu=Dsizes(n);
      Dname.eu='eta_u';
    case 'eta_v',
      Dsize.ev=Dsizes(n);
      Dname.ev='eta_v';
  end,
end,

%  Set time dimensions for each variable. If the field is grided,
%  the time dimension is set to unlimited.

if (grided.wind),            % surface wind components
  rgrid=1;
else,
  Dname.wind=Tname.wind;
  Dsize.wind=nrec;
end,  

if (grided.sms),             % surface wind stress
  rgrid=1;
else,
  Dname.sms=Tname.sms;
  Dsize.sms=nrec;
end,  

if (grided.pair),            % surface air pressure
  rgrid=1;
else,
  Dname.pair=Tname.pair;
  Dsize.pair=nrec;
end,  

if (grided.tair),            % surface air temperature
  rgrid=1;
else,
  Dname.tair=Tname.tair;
  Dsize.tair=nrec;
end,  

if (grided.qair),            % surface relative humidity
  rgrid=1;
else,
  Dsize.qair=nrec;
end,  

if (grided.cloud),           % cloud fraction
  rgrid=1;
else,
  Dname.cloud=Tname.cloud;
  Dsize.cloud=nrec;
end,  

if (grided.rain),            % rain fall rate
  rgrid=1;
else,
  Dname.rain=Tname.rain;
  Dsize.rain=nrec;
end,  

if (grided.evap),            % evaporation rate
  rgrid=1;
else,
  Dname.rain=Tname.evap;
  Dsize.evap=nrec;
end,  

if (grided.srf),             % shortwave radiation flux
  rgrid=1;
else,
  Dname.srf=Tname.srf;
  Dsize.srf=nrec;
end,  

if (grided.lrf),             % longwave radiation flux
  rgrid=1;
else,
  Dname.lrf=Tname.lrf;
  Dsize.lrf=nrec;
end,  

if (grided.lhf),             % latent heat flux
  rgrid=1;
else,
  Dname.lhf=Tname.lhf;
  Dsize.lhf=nrec;
end,  

if (grided.shf),             % sensible heat flux
  rgrid=1;
else,
  Dname.shf=Tname.shf;
  Dsize.shf=nrec;
end,  

if (grided.nhf),             % surface net heat flux
  rgrid=1;
else,
  Dname.nhf=Tname.nhf;
  Dsize.nhf=nrec;
end,  

if (grided.nff),             % surface net freshwater flux
  rgrid=1;
else,
  Dname.nff=Tname.nff;
  Dsize.nff=nrec;
end,  
    
%-----------------------------------------------------------------------
%  Create output NetCDF file.
%-----------------------------------------------------------------------

[ncid,status]=mexcdf('nccreate',fname,'nc_write');
if (ncid == -1),
  error(['C_FORCING: ncopen - unable to create file: ', fname]);
  return
end,

%-----------------------------------------------------------------------
%  Define dimensions.
%-----------------------------------------------------------------------

if (rgrid),
  [tid]=mexcdf('ncdimdef',ncid,'time',ncunlim);
  if (tid == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',time]);
  end,
  [did.xr]=mexcdf('ncdimdef',ncid,Dname.xr,Dsize.xr); 
  if (did.xr == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.xr]);
  end,
  [did.er]=mexcdf('ncdimdef',ncid,Dname.er,Dsize.er);
  if (did.er == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.er]);
  end,
end,

if (grided.sms),
  [did.xu]=mexcdf('ncdimdef',ncid,Dname.xu,Dsize.xu);
  if (did.xu == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.xu]);
  end,
  [did.eu]=mexcdf('ncdimdef',ncid,Dname.eu,Dsize.eu);
  if (did.eu == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.eu]);
  end,
  [did.xv]=mexcdf('ncdimdef',ncid,Dname.xv,Dsize.xv);
  if (did.xv == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.xv]);
  end,
  [did.ev]=mexcdf('ncdimdef',ncid,Dname.ev,Dsize.ev);
  if (did.ev == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.ev]);
  end,
end,

if (~grided.sms & define.sms),
  [did.wind]=mexcdf('ncdimdef',ncid,Dname.wind,Dsize.sms);
  if (did.sms == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.sms]);
  end,
else,
  did.sms=tid;
end,

if (~grided.wind & define.wind),
  [did.wind]=mexcdf('ncdimdef',ncid,Dname.wind,Dsize.wind);
  if (did.wind == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.wind]);
  end,
else,
  did.wind=tid;
end,

if (~grided.pair & define.pair),
  [did.pair]=mexcdf('ncdimdef',ncid,Dname.pair,Dsize.pair);
  if (did.pair == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.pair]);
  end,
else,
  did.pair=tid;
end,

if (~grided.tair & define.tair),
  [did.tair]=mexcdf('ncdimdef',ncid,Dname.tair,Dsize.tair);
  if (did.tair == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.tair]);
  end,
else,
  did.tair=tid;
end,

if (~define.qair & define.qair),
  [did.qair]=mexcdf('ncdimdef',ncid,Dname.qair,Dsize.qair);
  if (did.qair == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.qair]);
  end,
else,
  did.qair=tid;
end,

if (~grided.cloud & define.cloud),
  [did.cloud]=mexcdf('ncdimdef',ncid,Dname.cloud,Dsize.cloud);
  if (did.cloud == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.cloud]);
  end,
else,
  did.cloud=tid;
end,

if (~grided.rain & define.rain),
  [did.rain]=mexcdf('ncdimdef',ncid,Dname.rain,Dsize.rain);
  if (did.rain == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.rain]);
  end,
else,
  did.rain=tid;
end,

if (~grided.evap & define.evap),
  [did.evap]=mexcdf('ncdimdef',ncid,Dname.evap,Dsize.evap);
  if (did.evap == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.evap]);
  end,
else,
  did.evap=tid;
end,

if (~grided.srf & define.srf),
  [did.srf]=mexcdf('ncdimdef',ncid,Dname.srf,Dsize.srf);
  if (did.srf == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.srf]);
  end,
else,
  did.srf=tid;
end,

if (~grided.lrf & define.lrf),
  [did.lrf]=mexcdf('ncdimdef',ncid,Dname.lrf,Dsize.lrf);
  if (did.lrf == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.lrf]);
  end,
else,
  did.lrf=tid;
end,

if (~grided.lhf & define.lhf),
  [did.lhf]=mexcdf('ncdimdef',ncid,Dname.lhf,Dsize.lhf);
  if (did.lhf == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.lhf]);
  end,
else,
  did.lhf=tid;
end,

if (~grided.shf & define.shf),
  [did.shf]=mexcdf('ncdimdef',ncid,Dname.shf,Dsize.shf);
  if (did.shf == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.shf]);
  end,
else,
  did.shf=tid;
end,

if (~grided.nhf & define.nhf),
  [did.nhf]=mexcdf('ncdimdef',ncid,Dname.nhf,Dsize.nhf);
  if (did.nhf == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.nhf]);
  end,
else,
  did.nhf=tid;
end,

if (~grided.nff & define.nff),
  [did.nff]=mexcdf('ncdimdef',ncid,Dname.nff,Dsize.nff);
  if (did.nff == -1),
    error(['C_FORCING: ncdimdef - unable to define dimension: ',Dname.nff]);
  end,
else,
  did.nff=tid;
end,

%-----------------------------------------------------------------------
%  Set dimension IDs for all variables.
%-----------------------------------------------------------------------

if (define.wind),
  if (grided.wind),
    vdid.suw=[did.wind did.er did.xr];
    vdid.svw=[did.wind did.er did.xr];
  else,
    vdid.suw=[did.wind];
    vdid.svw=[did.wind];
  end,
end,

if (define.sms),
  if (grided.sms),
    vdid.sus=[did.sms did.eu did.xu];
    vdid.svs=[did.sms did.ev did.xv];
  else,
    vdid.sus=[did.sms];
    vdid.svs=[did.sms];
  end,
end,

if (define.pair),
  if (grided.pair),
    vdid.pair=[did.pair did.er did.xr];
  else,
    vdid.pair=[did.pair];
  end,
end,

if (define.tair),
  if (grided.tair),
    vdid.tair=[did.tair did.er did.xr];
  else,
    vdid.tair=[did.tair];
  end,
end,

if (define.qair),
  if (grided.qair),
    vdid.qair=[did.qair did.er did.xr];
  else,
    vdid.qair=[did.qair];
  end,
end,

if (define.cloud),
  if (grided.cloud),
    vdid.cloud=[did.cloud did.er did.xr];
  else,
    vdid.cloud=[did.cloud];
  end,
end,

if (define.rain),
  if (grided.rain),
    vdid.rain=[did.rain did.er did.xr];
  else,
    vdid.rain=[did.rain];
  end,
end,

if (define.evap),
  if (grided.evap),
    vdid.evap=[did.evap did.er did.xr];
  else,
    vdid.evap=[did.evap];
  end,
end,

if (define.srf),
  if (grided.srf),
    vdid.srf=[did.srf did.er did.xr];
  else,
    vdid.srf=[did.srf];
  end,
end,

if (define.lrf),
  if (grided.lrf),
    vdid.lrf=[did.lrf did.er did.xr];
  else,
    vdid.lrf=[did.lrf];
  end,
end,

if (define.lhf),
  if (grided.lhf),
    vdid.lhf=[did.lhf did.er did.xr];
  else,
    vdid.lhf=[did.lhf];
  end,
end,

if (define.shf),
  if (grided.shf),
    vdid.shf=[did.shf did.er did.xr];
  else,
    vdid.shf=[did.shf];
  end,
end,

if (define.nhf),
  if (grided.nhf),
    vdid.nhf=[did.nhf did.er did.xr];
  else,
    vdid.nhf=[did.nhf];
  end,
end,

if (define.nff),
  if (grided.nff),
    vdid.nff=[did.nff did.er did.xr];
  else,
    vdid.nff=[did.nff];
  end,
end,

%-----------------------------------------------------------------------
%  Create global attribute(s).
%-----------------------------------------------------------------------

type='FORCING file';
lstr=max(size(type));

[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lstr,type);
if (status == -1),
  error(['C_FORCING: ncattput - unable to global attribure: history.']);
  return
end

history=['FORCING file, 1.0, ', date_stamp];
lstr=max(size(history));

[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lstr,history);
if (status == -1),
  error(['C_FORCING: ncattput - unable to global attribure: history.']);
  return
end

%-----------------------------------------------------------------------
%  Define time coordinate(s).
%-----------------------------------------------------------------------

%  Surface winds time.

if (define.wind),
  Var.name =Tname.wind;
  Var.type =ncdouble;
  Var.dimid=did.wind;
  Var.long ='surface wind time';
  Var.units='modified Julian day';
  Var.field=[Tname.wind, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface momentum stress time.

if (define.sms),
  Var.name =Tname.sms;
  Var.type =ncdouble;
  Var.dimid=did.sms;
  Var.long ='surface momentum stress time';
  Var.units='modified Julian day';
  Var.field=[Tname.sms, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air pressure time.

if (define.pair),
  Var.name =Tname.pair;
  Var.type =ncdouble;
  Var.dimid=did.pair;
  Var.long ='surface air pressure time';
  Var.units='modified Julian day';
  Var.field=[Tname.pair, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Vare
end,

%  Surface air temperature time.

if (define.tair),
  Var.name =Tname.tair;
  Var.type =ncdouble;
  Var.dimid=did.tair;
  Var.long ='surface air temperature time';
  Var.units='modified Julian day';
  Var.field=[Tname.tair, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air relative humidity time.

if (define.qair),
  Var.name =Tname.qair;
  Var.type =ncdouble;
  Var.dimid=did.qair;
  Var.long ='surface relative humidity time';
  Var.units='modified Julian day';
  Var.field=[Tname.qair, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Cloud fraction time.

if (define.cloud),
  Var.name =Tname.cloud;
  Var.type =ncdouble;
  Var.dimid=did.cloud;
  Var.long ='cloud fraction time';
  Var.units='modified Julian day';
  Var.field=[Tname.cloud, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Rain fall rate time.

if (define.rain),
  Var.name =Tname.rain;
  Var.type =ncdouble;
  Var.dimid=did.rain;
  Var.long ='rain fall rate time';
  Var.units='modified Julian day';
  Var.field=[Tname.rain, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Evaporation rate time.

if (define.evap),
  Var.name =Tname.evap;
  Var.type =ncdouble;
  Var.dimid=did.evap;
  Var.long ='evaporation rate time';
  Var.units='modified Julian day';
  Var.field=[Tname.evap, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Solar shortwave radiation time.

if (define.srf),
  Var.name =Tname.srf;
  Var.type =ncdouble;
  Var.dimid=did.srf;
  Var.long ='solar shortwave radiation time';
  Var.units='modified Julian day';
  Var.field=[Tname.srf, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Net longwave radiation time.

if (define.lrf),
  Var.name =Tname.lrf;
  Var.type =ncdouble;
  Var.dimid=did.lrf;
  Var.long ='net longwave radiation time';
  Var.units='modified Julian day';
  Var.field=[Tname.lrf, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Net latent heat flux time.

if (define.lhf),
  Var.name =Tname.lhf;
  Var.type =ncdouble;
  Var.dimid=did.lhf;
  Var.long ='net latent heat flux time';
  Var.units='modified Julian day';
  Var.field=[Tname.lhf, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Net sensible radiation time.

if (define.shf),
  Var.name =Tname.shf;
  Var.type =ncdouble;
  Var.dimid=did.shf;
  Var.long ='net sensible radiation time';
  Var.units='modified Julian day';
  Var.field=[Tname.shf, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface net heat flux time.

if (define.nhf),
  Var.name =Tname.nhf;
  Var.type =ncdouble;
  Var.dimid=did.nhf;
  Var.long ='surface heat flux time';
  Var.units='modified Julian day';
  Var.field=[Tname.nhf, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface freshwater flux time.

if (define.nff),
  Var.name =Tname.nff;
  Var.type =ncdouble;
  Var.dimid=did.nff;
  Var.long ='surface momentum stress time';
  Var.units='modified Julian day';
  Var.field=[Tname.nff, ', scalar, series'];

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%-----------------------------------------------------------------------
%  Define variables.
%-----------------------------------------------------------------------

%  Surface u-wind component.

if (define.wind),
  Var.name =Vname.suw;
  Var.type =vartyp;
  if (grided.wind),
    Var.dimid=[did.wind did.er did.xr];
  else,
    Var.dimid=[did.wind];
  end,
  Var.long ='surface u-wind component';
  Var.units='meter second-1';
  Var.time =Tname.wind;
  Var.field='u-wind, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface v-wind component.

if (define.wind),
  Var.name =Vname.svw;
  Var.type =vartyp;
  if (grided.wind),
    Var.dimid=[did.wind did.er did.xr];
  else,
    Var.dimid=[did.wind];
  end,
  Var.long ='surface v-wind component';
  Var.units='meter second-1';
  Var.time =Tname.wind;
  Var.field='v-wind, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface u-momentum stress.

if (define.sms),
  Var.name =Vname.sus;
  Var.type =vartyp;
  if (grided.sms),
    Var.dimid=[did.sms did.eu did.xu];
  else,
    Var.dimid=[did.sms];
  end,
  Var.long ='surface u-momentum stress';
  Var.units='Newton meter-2';
  Var.time =Tname.sms;
  Var.field='surface u-mometum stress, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface v-momentum stress.

if (define.sms),
  Var.name =Vname.svs;
  Var.type =vartyp;
  if (grided.sms),
    Var.dimid=[did.sms did.ev did.xv];
  else,
    Var.dimid=[did.sms];
  end,
  Var.long ='surface v-momentum stress';
  Var.units='Newton meter-2';
  Var.time =Tname.sms;
  Var.field='surface v-mometum stress, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air pressure

if (define.pair),
  Var.name =Vname.pair;
  Var.type =vartyp;
  if (grided.pair),
    Var.dimid=[did.pair did.er did.xr];
  else,
    Var.dimid=[did.pair];
  end,
  Var.long ='surface air pressure';
  Var.units='milibar';
  Var.time =Tname.pair;
  Var.field='Pair, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air temperature.

if (define.tair),
  Var.name =Vname.tair;
  Var.type =vartyp;
  if (grided.tair),
    Var.dimid=[did.tair did.er did.xr];
  else,
    Var.dimid=[did.tair];
  end,
  Var.long ='surface air temperature';
  Var.units='Celsius';
  Var.time =Tname.tair;
  Var.field='Tair, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air relative humidity.

if (define.qair),
  Var.name =Vname.qair;
  Var.type =vartyp;
  if (grided.qair),
    Var.dimid=[did.qair did.er did.xr];
  else,
    Var.dimid=[did.qair];
  end,
  Var.long ='surface air relative humidity';
  Var.units='percentage';
  Var.time =Tname.qair;
  Var.field='Qair, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Cloud fraction.

if (define.cloud),
  Var.name =Vname.cloud;
  Var.type =vartyp;
  if (grided.cloud),
    Var.dimid=[did.cloud did.er did.xr];
  else,
    Var.dimid=[did.cloud];
  end,
  Var.long ='cloud fraction';
  Var.units='nondimensional';
  Var.time =Tname.cloud;
  Var.field='cloud, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Rain fall rate.

if (define.rain),
  Var.name =Vname.rain;
  Var.type =vartyp;
  if (grided.rain),
    Var.dimid=[did.rain did.er did.xr];
  else,
    Var.dimid=[did.rain];
  end,
  Var.long ='rain fall rate';
  Var.units='kilogram meter-2 second-1';
  Var.time =Tname.rain;
  Var.field='rain, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Evaporation rate.

if (define.evap),
  Var.name =Vname.evap;
  Var.type =vartyp;
  if (grided.evap),
    Var.dimid=[did.evap did.er did.xr];
  else,
    Var.dimid=[did.evap];
  end,
  Var.long ='evaporation rate';
  Var.units='kilogram meter-2 second-1';
  Var.time =Tname.evap;
  Var.field='evaporation, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Solar shortwave radiation.

if (define.srf),
  Var.name =Vname.srf;
  Var.type =vartyp;
  if (grided.srf),
    Var.dimid=[did.srf did.er did.xr];
  else,
    Var.dimid=[did.srf];
  end,
  Var.long ='solar shortwave radiation';
  Var.units='Watts meter-2';
  Var.plus ='downward flux, heating';
  Var.minus='upward flux, cooling';
  Var.time =Tname.srf;

  Var.field='shortwave radiation, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Net longwave radiation flux.

if (define.lrf),
  Var.name =Vname.lrf;
  Var.type =vartyp;
  if (grided.lrf),
    Var.dimid=[did.lrf did.er did.xr];
  else,
    Var.dimid=[did.lrf];
  end,
  Var.long ='net longwave radiation flux';
  Var.units='Watts meter-2';
  Var.plus ='downward flux, heating';
  Var.minus='upward flux, cooling';
  Var.time =Tname.lrf;
  Var.field='longwave radiation, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Net latent heat flux.

if (define.lhf),
  Var.name =Vname.lhf;
  Var.type =vartyp;
  if (grided.lhf),
    Var.dimid=[did.lhf did.er did.xr];
  else,
    Var.dimid=[did.lhf];
  end,
  Var.long ='net latent heat flux';
  Var.units='Watts meter-2';
  Var.plus ='downward flux, heating';
  Var.minus='upward flux, cooling';
  Var.time =Tname.lhf;
  Var.field='latent heat flux, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Net sensible heat flux.

if (define.shf),
  Var.name =Vname.shf;
  Var.type =vartyp;
  if (grided.shf),
    Var.dimid=[did.shf did.er did.xr];
  else,
    Var.dimid=[did.shf];
  end,
  Var.long ='net sensible heat flux';
  Var.units='Watts meter-2';
  Var.plus ='downward flux, heating';
  Var.minus='upward flux, cooling';
  Var.time =Tname.shf;
  Var.field='sensible heat flux, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface net heat flux.

if (define.nhf),
  Var.name =Vname.nhf;
  Var.type =vartyp;
  if (grided.nhf),
    Var.dimid=[did.nhf did.er did.xr];
  else,
    Var.dimid=[did.nhf];
  end,
  Var.long ='surface heat flux, scalar, series';
  Var.units='Watts meter-2';
  Var.plus ='downward flux, heating';
  Var.minus='upward flux, cooling';
  Var.time =Tname.nhf;
  Var.field='surface heat flux, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface freshwater flux (E-P).

if (define.nff),
  Var.name =Vname.nff;
  Var.type =vartyp;
  if (grided.nff),
    Var.dimid=[did.nff did.er did.xr];
  else,
    Var.dimid=[did.nff];
  end,
  Var.long ='surface freshwater flux (E-P)';
  Var.units='centimeter day-1';
  Var.plus ='net evaporation';
  Var.minus='net precipitation';
  Var.time =Tname.nff;
  Var.field='surface freshwater flux, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%-----------------------------------------------------------------------
%  Leave definition mode.
%-----------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['C_FORCING: ncendef - unable to leave definition mode.'])
end

%-----------------------------------------------------------------------
%  Close NetCDF file.
%-----------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['C_FORCING: ncclose - unable to close NetCDF file: ', fname])
end

return
