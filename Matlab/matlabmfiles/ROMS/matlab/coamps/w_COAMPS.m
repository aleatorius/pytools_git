function [status]=w_COAMPS(F,Fname,define,Vname,Tindex);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2001 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  [status]=w_COAMPS(F,Fname,define,Vname,Tindx)                       %
%                                                                      %
% This function writes forcing data into COAMPS NetCDF file.           %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    F         COAMPS forcing fields (structure array):                %
%                F.u    => East-West wind component (m/s) at 10m.      %
%                F.v    => North-South wind component (m/s) at 10m.    %
%                F.rh   => Relative humidity (%) at 10m.               %
%                F.Tair => Air temperature (Celsius) at 10m.           %
%                F.Pair => Air pressure (mb) at MSL.                   %
%                F.Psfc => Air pressure (mb) at surface terrain level. %
%                F.q    => Total accumulated precipitation (mm).       %
%                F.Lrad => Longwave radiation flux (W/m2) at surface.  %
%                F.Srad => shortwave radiation flux (W/m2) at surface. %
%    Fname     COAMPS NetCDF file (string).                            %    
%    define    Switches indicating which variables were defined        %
%                (0: no, 1: yes), structure array.                     %
%    Vname     Name of defined forcing NetCDF variables.               %
%    Tindex    Time record to process from COAMPS/NOGAPS NetCDF        %
%                file (integer scalar or vector).                      %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status      Error flag (integer).                                 %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

%-----------------------------------------------------------------------
%  If applicable, determine record to write.
%-----------------------------------------------------------------------

if (nargin < 5)
  [dnames,dsizes]=nc_dim(Fname);
  ndims=length(dsizes);
  for n=1:ndims,
    name=deblank(dnames(n,:));
    switch (name),  
      case 'time',
        Tindex=dsizes(n)+1;
    end,
  end,
end,

%-----------------------------------------------------------------------
% Set input variable names.
%-----------------------------------------------------------------------

got.lrad=0;
got.pair=0;
got.psfc=0;
got.qair=0;
got.rain=0;
got.srad=0;
got.tair=0;
got.time=0;
got.suw=0;
got.svw=0;

[vname,nvars]=nc_vname(Fname);
for n=1:nvars,
  name=deblank(vname(n,:));
  switch (name),  
    case {Vname.lrad}
      got.lrad=1;
    case {Vname.pair}
      got.pair=1;
    case {Vname.psfc}
      got.psfc=1;
    case {Vname.qair}
      got.qair=1;
    case {Vname.rain}
      got.rain=1;
    case {Vname.srad}
      got.srad=1;
    case {Vname.tair}
      got.tair=1;
    case {Vname.time}
      got.time=1;
    case {Vname.suw}
      got.suw=1;
    case {Vname.svw}
      got.svw=1;
  end,
end,

%-----------------------------------------------------------------------
%  Write out COAMPS data.
%-----------------------------------------------------------------------

if (define.time & got.time & isfield(F,'time')),
  [status]=nc_write(Fname,Vname.time,F.time,Tindex);
end,

if (define.wind & got.suw & isfield(F,'u')),
  [status]=nc_write(Fname,Vname.suw,F.u,Tindex);
end,

if (define.wind & got.svw & isfield(F,'v')),
  [status]=nc_write(Fname,Vname.svw,F.v,Tindex);
end,

if (define.tair & got.tair & isfield(F,'Tair')),
  [status]=nc_write(Fname,Vname.tair,F.Tair,Tindex);
end,

if (define.pair & got.pair & isfield(F,'Pair')),
  [status]=nc_write(Fname,Vname.pair,F.Pair,Tindex);
end,

if (define.psfc & got.psfc & isfield(F,'Psfc')),
  [status]=nc_write(Fname,Vname.psfc,F.Psfc,Tindex);
end,

if (define.qair & got.qair & isfield(F,'rh')),
  [status]=nc_write(Fname,Vname.qair,F.rh,Tindex);
end,

if (define.rain & got.rain & isfield(F,'q')),
  [status]=nc_write(Fname,Vname.rain,F.q,Tindex);
end,

if (define.srad & got.srad & isfield(F,'Srad')),
  [status]=nc_write(Fname,Vname.srad,F.Srad,Tindex);
end,

if (define.lrad & got.lrad & isfield(F,'Lrad')),
  [status]=nc_write(Fname,Vname.lrad,F.Lrad,Tindex);
end,

return
