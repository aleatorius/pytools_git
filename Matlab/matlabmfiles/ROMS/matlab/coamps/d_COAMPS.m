%  This script creates/writes/appends COAMPS NetCDF data from binary
%  flat files (very similar to GRIB type files).

ICREATE=1;
IWRITE=1;
IPLOT=0;
GRID=3;

dir='/coamps/mel/Jul00/FNMOC/flat/';
OutName='/coamps/mel/Jul00/netcdf/coamps_jul00_new.nc';

CSTfile='/n0/arango/ocean/plot/Data/us_cst.dat';

switch ( GRID )
  case 1
    lon_file='/coamps/mel/Jul00/FNMOC/work/Lon_grid1';
    lat_file='/coamps/mel/Jul00/FNMOC/work/Lat_grid1';
    Im=73;  Jm=73;
  case 2
    lon_file='/coamps/mel/Jul00/FNMOC/work/Lon_grid2';
    lat_file='/coamps/mel/Jul00/FNMOC/work/Lat_grid2';
    Im=115; Jm=115;
  case 3
    lon_file='/coamps/mel/Jul00/FNMOC/work/Lon_grid3';
    lat_file='/coamps/mel/Jul00/FNMOC/work/Lat_grid3';
    Im=145; Jm=145;
end,

time.year=2000;
time.month=7;

%  The starting and ending days is by 0.5 intervals since the data
%  is generated every 12 hours.

day_str=1.0;
day_end=30.0;

%-----------------------------------------------------------------------
%  Read in coastline data.
%-----------------------------------------------------------------------

if ( IPLOT ),
  [cstlon,cstlat]=rcoastline(CSTfile);
  [icst]=find(cstlon>=900);
  cstlon(icst)=NaN;
  cstlat(icst)=NaN;
  clear icst;
end,

%-----------------------------------------------------------------------
%  Read in COAMPS longitude and latitude positions.
%-----------------------------------------------------------------------

fid=fopen(lon_file,'r','s');
if (fid > 0),
  [lon,Ncount]=fread(fid,Inf,'single'); fclose(fid);
  lon(Ncount)=[]; lon(1)=[];
  lon=reshape(lon,Im,Jm)-360;
end,

fid=fopen(lat_file,'r','s');
if (fid > 0),
  [lat,Ncount]=fread(fid,Inf,'single'); fclose(fid);
  lat(Ncount)=[]; lat(1)=[];
  lat=reshape(lat,Im,Jm);
end,

%-----------------------------------------------------------------------
%  If appropriate, create COAMPS NetCDF file.
%-----------------------------------------------------------------------

%  Set structure arrays: define (variables to define), grided
%  (point or grided data), Tname (name of time variable), and
%  Vname (name of forcing variable).  Switches are: 0=no, 1=yes.

define.time =1;             % time.
Vname.time  ='time';

define.wind =1;             % surface wind components
Vname.suw   ='Uwind';
Vname.svw   ='Vwind';

define.pair =1;             % surface air pressure at MSL
Vname.pair  ='Pair';

define.psfc =1;             % surface air pressure at local surface
Vname.psfc  ='Psfc';

define.tair =1;             % surface air temperature
Vname.tair  ='Tair';

define.qair =1;             % surface relative humidity
Vname.qair  ='Qair';

define.rain =1;             % rain fall rate
Vname.rain  ='rain';

define.lrad =1;             % surface longwave radiation
Vname.lrad  ='lwrad';

define.srad =1;             % surface shortwave radiation
Vname.srad  ='swrad';

if (ICREATE),
  disp(['Created COAMPS NetCDF file: ',OutName]);
  [status]=c_COAMPS(lon,lat,OutName,define,Vname);
end,

%-----------------------------------------------------------------------
%  Read in COAMPS data from flat binary files.
%-----------------------------------------------------------------------

if (IWRITE | IPLOT),

%  Inquire size of time record in COAMPS NetCDF file.

  if (IWRITE),
    [dnames,dsizes]=nc_dim(OutName);
    ndims=length(dsizes);
    for n=1:ndims,
      name=deblank(dnames(n,:));
      switch (name),  
        case 'time',
          Tindex=dsizes(n);
      end,
    end,
    disp(' ');
    disp(['Number of records in COAMPS file: ',num2str(Tindex)]);
    disp(' ');
  end,

%  Process all requested flat files.

  for day=day_str:0.5:day_end,
    time.day=floor(day);
    time.hour=rem(day,1)*24;
    time.Jday=julian(time.year,time.month,time.day,0)-2440000;
    for hour=0:0.5:11.5,
      tau=floor(hour)*10000 + rem(hour,1)*60*100;
      [F]=r_COAMPS(dir,GRID,time,tau);
      F.time=time.Jday+(time.hour+hour)/24;

      disp(' ');
      gtime=gregorian(F.time+2440000);
      disp(['Processing COAMPS data for: ' , ...
             num2str(gtime(1),'%4.4i'), ' ', ...
             num2str(gtime(2),'%2.2i'), ' ', ...
             num2str(gtime(3),'%2.2i'), ' ', ...
             num2str(gtime(4),'%2.2i'), ':', ...
             num2str(gtime(5),'%2.2i')]);

      if (IWRITE),
        Tindex=Tindex+1;
        [status]=w_COAMPS(F,OutName,define,Vname,Tindex);
      end,

      if (IPLOT),
        F.lon=lon;
        F.lat=lat;
        figure(1);
        clf;
        [h]=p_COAMPS(F,time,tau,cstlon,cstlat); 
        hold off;
      end,

    end,
  end,

end,




