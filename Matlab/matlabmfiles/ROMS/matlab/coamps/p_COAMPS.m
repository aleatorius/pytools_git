function [h]=p_COAMPS(F,time,tau,cstlon,cstlat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2001 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Hernan G. Arango %%%
%                                                                      %
% function [h]=p_COAMPS(F,time,tau,cstlon,cstlat)                      %
%                                                                      %
% This function plots color-filled COAMPS data.                        %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    F         COAMPS forcing fields (structure array):                %
%                F.u    => East-West wind component (m/s) at 10m.      %
%                F.v    => North-South wind component (m/s) at 10m.    %
%                F.rh   => Relative humidity (%) at 10m.               %
%                F.Tair => Air temperature (Celsius) at 10m.           %
%                F.Pair => Air pressure (mb) at MSL.                   %
%                F.Psfc => Air pressure (mb) at surface terrain level. %
%                F.q    => Total accumulated precipitation (mm).       %
%                F.Lrad => Longwave radiation flux (W/m2) at surface.  %
%                F.Srad => shortwave radiation flux (W/m2) at surface. %
%    time      time of data to write (structure array):                %
%                time.Jday  => Julian day.                             %
%                time.year  => year of the century.                    %
%                time.month => month of the year (1-12).               %
%                time.day   => day of the month.                       %
%                time.hour  => hour of the day (GMT, 1-24).            %
%     tau       Forecast time (hours).                                 %
%                                                                      %
%  On Output:                                                          %
%                                                                      %
%     h         figure handle.                                         %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%  Set switch to draw coastlines.

ICOAST=1;
if (nargin < 4),
  ICOAST=0;
end,

%-----------------------------------------------------------------------
%  Set date stamp.
%-----------------------------------------------------------------------

wkday=['Sun. ';'Mon. ';'Tue. ';'Wed. ';'Thu. ';'Fri. ';'Sat. '];
lwkday=[4,4,4,4,4,4,4];
Month=['Jan ';'Feb ';'Mar ';'Apr ';'May ';'Jun ';'Jul ';'Aug ';...
       'Sep ';'Oct ';'Nov ';'Dec '];
lmonth=[3,3,3,3,3,3,3,3,3,3,3,3];
ampm=[' AM';' PM'];

%  Convert Julian day to Gregorian day.

gtime=gregorian(time.Jday+2440000);

year  =gtime(1);
month =gtime(2);
day   =gtime(3);
hour  =gtime(4);
minute=gtime(5);
second=gtime(6);
iwday=day_code(month,day,year);

%  Set date stamp.

stamp1=['Issued: ',wkday(iwday,1:lwkday(iwday)), ...
        ' ', Month(month,1:lmonth(month)), ...
        ' ',num2str(day),', ',num2str(year), ...
        ' - ',num2str(hour),' GMT.'];

gtime=gregorian(time.Jday+2440000+tau/24.0);

year  =gtime(1);
month =gtime(2);
day   =gtime(3);
hour  =gtime(4);
minute=gtime(5);
second=gtime(6);
iwday=day_code(month,day,year);
stamp2=['COAMPS, Valid: ',wkday(iwday,1:lwkday(iwday)), ...
        ' ',Month(month,1:lmonth(month)), ...
        ' ',num2str(day),', ',num2str(hour),' GMT.'];


%-----------------------------------------------------------------------
%  Set COAMPS grid.
%-----------------------------------------------------------------------

[im,jm]=size(F.lon);

Lon=F.lon;
Lat=F.lat;

%-----------------------------------------------------------------------
%  Set range of data to plot.
%-----------------------------------------------------------------------

LonMin=-76.5;
LonMax=-70.5;

LatMin=36.0;
LatMax=42.0;

LonRange=find((Lon(:,1) >= LonMin) & (Lon(:,1) <= LonMax));
LatRange=find((Lat(1,:) >= LatMin) & (Lat(1,:) <= LatMax));

X=Lon(LonRange,LatRange);
Y=Lat(LonRange,LatRange);

h=[];

%-----------------------------------------------------------------------
%  Plot surface pressure and winds.
%-----------------------------------------------------------------------

m_proj('Mercator');
hold off;

subplot(2,2,1);

plev=990:0.5:1040;
plab=990:2:1040;
f=F.Pair(LonRange,LatRange);

Ptext=[];
if (~isnan(f)),
 [c,h]=contourf(X,Y,f,20);
 set(h,'edgecolor','k');
 colormap('jet');
 colorbar;
% H=clabel(c,h,plab);
 Ptext='Surface Pressure (mb)';
end,
hold on;
grid on;

fx=F.u(LonRange,LatRange);
fy=F.v(LonRange,LatRange);
f=sqrt(fx.*fx + fy.*fy);

Ttext=[];
RangeLab=[];
if (~isnan(f)),
  quiver(X,Y,fx,fy,'w');
  set(h,'edgecolor','k');
  TauMin=min(min(f));
  TauMax=max(max(f));
  RangeLab=[' Min= ',num2str(TauMin),', Max= ',num2str(TauMax)];
  if (isempty(Ptext)),
    Ttext='Winds (m/s) at 10m';
  else,
    Ttext=' and Wind Speed (m/s) at 10m';
  end,
end,

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

title(stamp1);
if (~isempty(RangeLab)),
  xlabel({[Ptext Ttext],[RangeLab]});
else,
  xlabel([Ptext Ttext]);
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

%-----------------------------------------------------------------------
%  Plot air temperature.
%-----------------------------------------------------------------------

subplot(2,2,2);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

tlev=0:1:40;
tlab=0:1:40;
f=F.Tair(LonRange,LatRange);

RangeLab=[];
if (~isnan(f)),
  [c,h]=contourf(X,Y,f,20);
  set(h,'edgecolor','k');
  colormap('jet');
  colorbar;
%  H=clabel(c,h,tlab);
  RangeLab=[' Min= ',num2str(min(min(f))),', Max= ',num2str(max(max(f)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

title(stamp2);
if (~isempty(RangeLab)),
  xlabel({'Temperature at 2m (C)',[RangeLab]});
else,
  xlabel('Temperature at 2m (C)');
end,
ylabel('Latitude');

set(gca,'nextplot','replace')


%-----------------------------------------------------------------------
%  Plot relative humidity.
%-----------------------------------------------------------------------

subplot(2,2,3);
set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

rhlev=10:10:100;
rhlab=10:10:100;
f=F.rh(LonRange,LatRange);

RangeLab=[];
if (~isnan(f)),
  [c,h]=contourf(X,Y,f,20);
  set(h,'edgecolor','k');
  colormap('jet');
  colorbar;
%  H=clabel(c,h,rhlab);
  RangeLab=[' Min= ',num2str(min(min(f))),', Max= ',num2str(max(max(f)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

if (~isempty(RangeLab)),
  xlabel({'Relative Humidity (%)',[RangeLab]});
else,
  xlabel('Relative Humidity (%)');
end,
ylabel('Latitude');

set(gca,'nextplot','replace')

%-----------------------------------------------------------------------
%  Plot precipitation.
%-----------------------------------------------------------------------

subplot(2,2,4);

qlev=0:0.02:10;
qlab=0:0.5:5;
f=F.q(LonRange,LatRange);

RangeLab=[];
if (~isnan(f)),
  [c,h]=contour(X,Y,f,20);
  RangeLab=[' Min= ',num2str(min(min(f))),', Max= ',num2str(max(max(f)))];
end,
hold on;
grid on;

if (ICOAST),
  plot([LonMin LonMax],[LatMax LatMax],'k');
  plot([LonMax LonMax],[LatMin LatMax],'k');
  draw_cst(cstlon,cstlat,'k');
end,

if (~isempty(RangeLab)),
  xlabel({'12-hour precipitation (cm)',[RangeLab]});
else,
  xlabel('12-hour precipitation (cm)');
end,
ylabel('Latitude');

set(gca,'xlim',[LonMin LonMax],'ylim',[LatMin,LatMax],'FontName','Times');

set(gca,'nextplot','replace')

return
