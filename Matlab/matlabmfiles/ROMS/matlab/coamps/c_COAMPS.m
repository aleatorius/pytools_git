function [status]=c_COAMPS(Lon,Lat,Fname,define,Vname);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2001 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
% function [status]=c_COAMPS(Fname,define,Vname);                      %
%                                                                      %
% This function creates a NetCDF to store COAMPS data.                 %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Lon         COAMPS longitude.                                     %
%    Lat         COAMPS latitude.                                      %
%    Fname       NetCDF file name (string).                            %
%    define      Switches indicating which variables to define         %
%                  (0: no, 1: yes), structure array.                   %
%    Vname       Name of defined forcing NetCDF variables.             %
%                                                                      %
%                Variables in structure arrays:                        %
%                                                                      %
%                  ***.time   => time.                                 %
%                  ***.wind   => surface wind components.              %
%                  ***.suw    => surface U-wind component.             %
%                  ***.svw    => surface V-wind component.             %
%                  ***.pair   => surface air pressure.                 %
%                  ***.tair   => surface air temperature.              %
%                  ***.qair   => surface relative humidity.            %
%                  ***.rain   => rain fall rate.                       %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_dim, nc_read, nc_write                                   %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

spval=-999.0;

%-----------------------------------------------------------------------
%  Determine grid size.
%-----------------------------------------------------------------------

[Im,Jm]=size(Lon);

Vname.rlon='lon';
Vname.rlat='lat';

%-----------------------------------------------------------------------
%  Get some NetCDF parameters.
%-----------------------------------------------------------------------

[ncglobal]=mexcdf('parameter','nc_global');
[ncdouble]=mexcdf('parameter','nc_double');
[ncunlim]=mexcdf('parameter','nc_unlimited');
[ncfloat]=mexcdf('parameter','nc_float');
[ncchar]=mexcdf('parameter','nc_char');
[ncclob]=mexcdf('parameter','nc_clobber');

% Set floating-point variables type.

vartyp=ncfloat;

%-----------------------------------------------------------------------
%  Create output NetCDF file.
%-----------------------------------------------------------------------

[ncid,status]=mexcdf('nccreate',Fname,'nc_clobber');
if (ncid == -1),
  error(['C_COAMPS: ncopen - unable to create file: ', Fname]);
  return
end,

%-----------------------------------------------------------------------
%  Define output file dimensions.
%-----------------------------------------------------------------------

[did.rlon]=mexcdf('ncdimdef',ncid,'lon',Im);
if (did.rlon == -1),
  error(['C_COAMPS: ncdimdef - unable to define dimension: lon.']);
end,

[did.rlat]=mexcdf('ncdimdef',ncid,'lat',Jm);
if (did.rlat == -1),
  error(['C_COAMPS: ncdimdef - unable to define dimension: lat.']);
end,

[did.time]=mexcdf('ncdimdef',ncid,'time',ncunlim);
if (did.time == -1),
  error(['C_COAMPS: ncdimdef - unable to define dimension: time.']);
end,

%----------------------------------------------------------------------------
%  Create global attribute(s).
%----------------------------------------------------------------------------

type='COAMPS file';

lenstr=max(size(type));
[status]=mexcdf('ncattput',ncid,ncglobal,'type',ncchar,lenstr,type);
if (status == -1),
  error(['C_COAMPS: ncattput - unable to global attribure: history.']);
  return
end

history=date_stamp;

lenstr=max(size(history));
[status]=mexcdf('ncattput',ncid,ncglobal,'history',ncchar,lenstr,history);
if (status == -1),
  error(['C_COAMPS: ncattput - unable to global attribure: history.']);
  return
end

%-----------------------------------------------------------------------
% Define NetCDF variables.
%-----------------------------------------------------------------------

% Define spherical switch.

Var.name ='spherical';
Var.type =ncchar;
Var.dimid=[];
Var.long ='grid type logical switch';
Var.opt_T='spherical';
Var.opt_F='Cartesian';

[varid,status]=nc_vdef(ncid,Var);
clear Var

%  Define time.

Var.name  =Vname.time;
Var.type  =ncdouble;
Var.dimid =[did.time];
Var.long  ='issue forecast time';
Var.units ='modified Julian day';
Var.offset=2440000.0;
Var.field ='time, scalar, series';

[varid,status]=nc_vdef(ncid,Var);
clear Var

%  Define longitude positions at RHO-points.

Var.name =Vname.rlon;
Var.type =ncdouble;
Var.dimid=[did.rlat did.rlon];
Var.long ='longitude of RHO-points';
Var.units='degree_east';

[varid,status]=nc_vdef(ncid,Var);
clear Var

%  Define longitude positions at RHO-points.

Var.name =Vname.rlat;
Var.type =ncdouble;
Var.dimid=[did.rlat did.rlon];
Var.long ='latitude of RHO-points';
Var.units='degree_north';

[varid,status]=nc_vdef(ncid,Var);
clear Var


%  Surface wind components.

if (define.wind),
  Var.name =Vname.suw;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='zonal, surface wind component';
  Var.units='meter second-1';
  Var.field='U-wind, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var

  Var.name =Vname.svw;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='meridional, surface wind component';
  Var.units='meter second-1';
  Var.field='V-wind, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air temperature.

if (define.tair),
  Var.name =Vname.tair;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='surface air temperature';
  Var.units='Celsius';
  Var.field='Tair, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air pressure at MSL.

if (define.pair),
  Var.name =Vname.pair;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='air pressure at mean sea level';
  Var.units='milibar';
  Var.field='Pair, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air pressure at local surface.

if (define.psfc),
  Var.name =Vname.psfc;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='surface air pressure';
  Var.units='milibar';
  Var.field='Psfc, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface air relative humidity.

if (define.qair),
  Var.name =Vname.qair;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='surface air relative humidity';
  Var.units='percent';
  Var.min=0.0;
  Var.max=100.0;
  Var.field='humidity, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Rain fall rate.

if (define.rain),
  Var.name =Vname.rain;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='moisture flux';
  Var.units='kilogram_water kilogram_air-1 meter second-1';
  Var.field='rain, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface shortwave radiation flux.

if (define.srad),
  Var.name =Vname.srad;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='net surface shortwave radiation flux';
  Var.units='Watts meter-2';
  Var.plus ='downward flux, heating';
  Var.minus='upward flux, cooling';
  Var.field='shortwave radiation, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%  Surface longwave radiation flux.

if (define.lrad),
  Var.name =Vname.lrad;
  Var.type =vartyp;
  Var.dimid=[did.time did.rlat did.rlon];
  Var.long ='net surface longwave radiation flux';
  Var.units='Watts meter-2';
  Var.plus ='downward flux, heating';
  Var.minus='upward flux, cooling';
  Var.field='longwave radiation, scalar, series';

  [varid,status]=nc_vdef(ncid,Var);
  clear Var
end,

%-----------------------------------------------------------------------
%  Leave definition mode.
%-----------------------------------------------------------------------

[status]=mexcdf('ncendef',ncid);
if (status == -1),
  error(['C_COAMPS: ncendef - unable to leave definition mode.'])
end

%-----------------------------------------------------------------------
%  Close NetCDF file.
%-----------------------------------------------------------------------

[status]=mexcdf('ncclose',ncid);
if (status == -1),
  error(['C_COAMPS: ncclose - unable to close NetCDF file: ', Fname])
end

%=======================================================================
%  Write out positions.
%=======================================================================

% Write out spherical switch.

[status]=nc_write(Fname,'spherical','T');

% Write out longitude and latitude positions.

[status]=nc_write(Fname,'lon',Lon);
[status]=nc_write(Fname,'lat',Lat);

return
