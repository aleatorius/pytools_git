function [status]=w_forcing(Gname,Cname,Fname, ...
                            define,grided,Tname,Vname,Tindx);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2001 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%  [status]=w_forcing(Gname,Cname,Fname, ...                           %
%                     define,grided,Tname,Vname,Tindx)                 %
%                                                                      %
% This function reads in forcing data from non-operational COAMPS      %
% NetCDF file and interpolates and rotates vectors to model grid.      %
% Then, it writes out data into output Forcing NetCDF file.            %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    Gname       GRID NetCDF file name (string).                       %
%    Cname       COAMPS NetCDF file name (string).                     %
%    Fname       FORCING NetCDF file (string).                         %    
%    define      Switches indicating which variables were defined      %
%                  (0: no, 1: yes), structure array.                   %
%    grided      Switches indicating which grid type for each          %
%                  variable (0: point, 1: grided), structure array.    %
%    Tname       Name of defined time NetCDF variables.                %
%    Vname       Name of defined forcing NetCDF variables.             %
%    Tindx       Time record to process from RAMS NetCDF file          %
%                  (integer scalar or vector).                         %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    status      Error flag (integer).                                 %
%                                                                      %
% Calls:   MEXCDF (Interface to NetCDF library using Matlab).          %
%          nc_read, wind_stress                                        %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global IPRINT

IPRINT=0;

fid=fopen('Hcoamps.log','w');

%-----------------------------------------------------------------------
% Read in model grid positions and angle.
%-----------------------------------------------------------------------

rlon=nc_read(Gname,'lon_rho');
rlat=nc_read(Gname,'lat_rho');

angle=nc_read(Gname,'angle');

[Lp,Mp]=size(rlon);
L=Lp-1;
M=Mp-1;

%-----------------------------------------------------------------------
% Read in COAMPS positions at RHO-points.
%-----------------------------------------------------------------------

Clon=nc_read(Cname,'lon');
Clat=nc_read(Cname,'lat');

[Im Jm]=size(Clon);

% Set input variable names.

got.tair=0;
got.pair=0;
got.qair=0;
got.rain=0;
got.wind=0;
got.sms=0;
got.srf=0;
got.lrf=0;
got.shf=0;

[vname,nvars]=nc_vname(Cname);
for n=1:nvars,
  name=deblank(vname(n,:));
  switch name
    case 'Tair',
      got.tair=1;
      Fvar.tair='Tair';
    case 'Pair',
      got.pair=1;
      Fvar.pair='Pair';
    case 'Qair',                     % Prefered variable name
      got.qair=1;
      Fvar.qair='Qair';
    case 'humidity',                 % This names was used in the past
      got.qair=1;
      Fvar.qair='humidity';
    case 'rain',
      got.rain=1;
      Fvar.rain='rain';
    case 'Uwind',
      got.wind=1;
      Fvar.suw ='Uwind';
    case 'Vwind',
      got.wind=1;
      Fvar.svw ='Vwind';
    case 'sustr',
      got.sms=1;
      Fvar.sus='sustr';
    case 'svstr',
      got.sms=1;
      Fvar.svs='svstr';
    case 'rain',
      got.rain=1;
      Fvar.rain='rain';
    case 'swrad',
      got.srf=1;
      Fvar.srf='swrad';
    case 'lwrad',
      got.lrf=1;
      Fvar.lrf='lwrad';
    case 'sensible',
      got.shf=1;
      Fvar.shf='sensible';
  end,
end,

%-----------------------------------------------------------------------
% Process RAMS data.
%-----------------------------------------------------------------------

Nrec=length(Tindx);

Tstr=Tindx(1)-1;

for n=1:Nrec,

  Trec=Tindx(n);
  time=nc_read(Cname,'time',Trec);

% Read in forcing data.  Notice that wind data is averaged to
% interior RHO-points.  

  if (define.tair & grided.tair & got.tair),
    Tair=nc_read(Cname,Fvar.tair,Trec);
  end,

  if (define.pair & grided.pair & got.pair),
    Pair=nc_read(Cname,Fvar.pair,Trec);
  end,

  if (define.qair & grided.qair & got.qair),
    Qair=nc_read(Cname,Fvar.qair,Trec);
  end,

  if (define.srf & grided.srf & got.srf),
    Srad=nc_read(Cname,Fvar.srf,Trec);
  end,

  if (define.lrf & grided.lrf & got.lrf),
    Lrad=nc_read(Cname,Fvar.lrf,Trec);
  end,

  if (define.shf & grided.shf & got.shf),
    Qsen=nc_read(Cname,Fvar.shf,Trec);
  end,

  if (define.rain & grided.rain & got.rain),
    rain=nc_read(Cname,Fvar.rain,Trec);
  end,

  if (got.wind & define.wind & grided.wind),
    Uair=nc_read(Cname,Fvar.suw,Trec);
    Vair=nc_read(Cname,Fvar.svw,Trec);
  end,

  if (got.sms & define.sms & grided.sms),
    Ustr=nc_read(Cname,Fvar.sus,Trec);
    Vstr=nc_read(Cname,Fvar.svs,Trec);
  end,

% Interpolate data to model grid.

  Tstr=Tstr+1;

  if (define.tair & grided.tair & got.tair),
    Finp=Tair(:,:);
    [Fout]=griddata(Clon,Clat,Finp,rlon,rlat);
    [status]=nc_write(Fname,Vname.tair,Fout,Tstr);
    [status]=nc_write(Fname,Tname.tair,time,Tstr);
  end,

  if (define.pair & grided.pair & got.pair),
    Finp=Pair(:,:);
    [Fout]=griddata(Clon,Clat,Finp,rlon,rlat);
    [status]=nc_write(Fname,Vname.pair,Fout,Tstr);
    [status]=nc_write(Fname,Tname.pair,time,Tstr);
  end,

  if (define.qair & grided.qair & got.qair),
    Finp=Qair(:,:);
    [Fout]=griddata(Clon,Clat,Finp,rlon,rlat);
    [status]=nc_write(Fname,Vname.qair,Fout,Tstr);
    [status]=nc_write(Fname,Tname.qair,time,Tstr);
  end,

  if (define.srf & grided.srf & got.srf),
    Finp=Srad(:,:);
    [Fout]=griddata(Clon,Clat,Finp,rlon,rlat);
    [status]=nc_write(Fname,Vname.srf,Fout,Tstr);
    [status]=nc_write(Fname,Tname.srf,time,Tstr);
  end,

  if (define.lrf & grided.lrf & got.lrf),
    Finp=Lrad(:,:);
    [Fout]=griddata(Clon,Clat,Finp,rlon,rlat);
    [status]=nc_write(Fname,Vname.lrf,Fout,Tstr);
    [status]=nc_write(Fname,Tname.lrf,time,Tstr);
  end,

  if (define.shf & grided.shf & got.shf),
    Finp=Qsen(:,:);
    [Fout]=griddata(Clon,Clat,Finp,rlon,rlat);
    [status]=nc_write(Fname,Vname.shf,Fout,Tstr);
    [status]=nc_write(Fname,Tname.shf,time,Tstr);
  end,

  if (define.rain & grided.rain & got.rain),
    Finp=rain(:,:);
    [Fout]=griddata(Clon,Clat,Finp,rlon,rlat);
    [status]=nc_write(Fname,Vname.rain,Fout,Tstr);
    [status]=nc_write(Fname,Tname.rain,time,Tstr);
  end,

  if (define.wind & grided.wind & got.wind),
    Uinp=Uair(:,:);
    Vinp=Vair(:,:);
    [Uout]=griddata(Clon,Clat,Uinp,rlon,rlat);
    [Vout]=griddata(Clon,Clat,Vinp,rlon,rlat);
    Fout=Uout.*cos(angle)+Vout.*sin(angle);
    [status]=nc_write(Fname,Vname.suw,Fout,Tstr);
    Fout=Vout.*cos(angle)-Uout.*sin(angle);
    [status]=nc_write(Fname,Vname.svw,Fout,Tstr);
    [status]=nc_write(Fname,Tname.wind,time,Tstr);
  end,

  if (define.sms & grided.sms & got.wind),
    [Uinp,Vinp]=wind_stress(Uair,Vair);
    [Uout]=griddata(Clon,Clat,Uinp,rlon,rlat);
    [Vout]=griddata(Clon,Clat,Vinp,rlon,rlat);
    Frot=Uout.*cos(angle)+Vout.*sin(angle);
    Fout=0.5.*(Frot(1:L,1:Mp)+Frot(2:Lp,1:Mp));
    [status]=nc_write(Fname,Vname.sus,Fout,Tstr);
    Frot=Vout.*cos(angle)-Uout.*sin(angle);
    Fout=0.5.*(Frot(1:Lp,1:M)+Frot(1:Lp,2:Mp));
    [status]=nc_write(Fname,Vname.svs,Fout,Tstr);
    [status]=nc_write(Fname,Tname.sms,time,Tstr);
  end,   

% Report records of processed data.

  gtime=gregorian(time+2440000.0);
  text=['Processed data for: ', num2str(gtime(1)), ' ',...
        num2str(gtime(2),'%2.2i'), ' ', num2str(gtime(3),'%2.2i'),...
        ' ', num2str(gtime(4),'%2.2i'),':',...
        ' ', num2str(gtime(5),'%2.2i'),...
        ', output record > ',num2str(Tstr,'%3.3i')];
  disp(text);
  fprintf(fid,'%s\n',text);
end,

fclose(fid);

return
