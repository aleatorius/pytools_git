function [F]=r_COAMPS(dir,grd,time,tau);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright (c) 2001 Rutgers University.                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Hernan G. Arango %%%
%                                                                      %
% function [F]=r_COAMPS(dir,grd,time,tau)                              %
%                                                                      %
% This function reads in COAMPS requested binary file.                 %
%                                                                      %
% On Input:                                                            %
%                                                                      %
%    dir       Directory for binary files (string).                    %
%    grd       Nexted grid type (1-3).                                 %
%    time      Time of simulation (structure array):                   %
%                time.Jday  => Julian day.                             %
%                time.year  => year of the century.                    %
%                time.month => month of the year (1-12).               %
%                time.day   => day of the month.                       %
%                time.hour  => starting hour of the day (GMT, 1-24).   %
%    tau       Forecast time hhhmmss (integer).                        %
%                                                                      %
% On Output:                                                           %
%                                                                      %
%    F         Forcing fields (structure array):                       %
%                F.u    => East-West wind component (m/s) at 10m.      %
%                F.v    => North-South wind component (m/s) at 10m.    %
%                F.rh   => Relative humidity (%) at 10m.               %
%                F.Tair => Air temperature (Celsius) at 10m.           %
%                F.Pair => Air pressure (mb) at MSL.                   %
%                F.Psfc => Air pressure (mb) at surface terrain level. %
%                F.q    => Total accumulated precipitation (mm).       %
%                F.Lrad => Longwave radiation flux (W/m2) at surface.  %
%                F.Srad => shortwave radiation flux (W/m2) at surface. %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------
%  Determine grid size.
%-----------------------------------------------------------------------

if (grd == 1),
  Im=73;  Jm=73;
elseif (grd == 2),
  Im=115; Jm=115;
elseif (grd == 3),
  Im=145; Jm=145;
end,
  
%-----------------------------------------------------------------------
%  Build COAMPS binary files names.
%-----------------------------------------------------------------------

sgrd=num2str(grd);
syy =int2str(time.year);
smm =num2str(time.month,'%2.2i');
sdd =num2str(time.day,'%2.2i');
shh =num2str(time.hour,'%2.2i');
stau=num2str(tau,'%7.7i');

u_file=[dir 'uuuua' sgrd syy smm sdd shh stau '00010' '00000' 'hsl'];
v_file=[dir 'vvvva' sgrd syy smm sdd shh stau '00010' '00000' 'hsl'];
r_file=[dir 'relha' sgrd syy smm sdd shh stau '00010' '00000' 'hsl'];
t_file=[dir 'tttta' sgrd syy smm sdd shh stau '00010' '00000' 'hsl'];
P_file=[dir 'slpra' sgrd syy smm sdd shh stau '00000' '00000' 'sll'];
p_file=[dir 'tepra' sgrd syy smm sdd shh stau '00000' '00000' 'sfl'];
q_file=[dir 'prcpa' sgrd syy smm sdd shh stau '00000' '00000' 'sfl'];
L_file=[dir 'lrada' sgrd syy smm sdd shh stau '00000' '00000' 'sfl'];
S_file=[dir 'srada' sgrd syy smm sdd shh stau '00000' '00000' 'sfl'];

%-----------------------------------------------------------------------
%  Read in wind components (m/s) at 10 m.
%-----------------------------------------------------------------------

uid=fopen(u_file,'r','s');
if (uid > 0),
  [W,Ncount]=fread(uid,Inf,'single'); fclose(uid);
  W(Ncount)=[]; W(1)=[];
  F.u=reshape(W,Im,Jm);
else,
  disp(['Not found u']);
  F.u=(ones([Im Jm])).*NaN;
end,
clear uid

vid=fopen(v_file,'r','s');
if (vid > 0),
  [W,Ncount]=fread(vid,Inf,'single'); fclose(vid);
  W(Ncount)=[]; W(1)=[];
  F.v=reshape(W,Im,Jm);
else,
  disp(['Not found v']);
  F.v=(ones([Im Jm])).*NaN;
end,
clear vid

%-----------------------------------------------------------------------
%  Read in relative humidity (percent) at 10m.
%-----------------------------------------------------------------------

rid=fopen(r_file,'r','s');
if (rid > 0),
  [W,Ncount]=fread(rid,Inf,'single'); fclose(rid);
  W(Ncount)=[]; W(1)=[];
  W=min(100,W);
  F.rh=reshape(W,Im,Jm);
else,
  disp(['Not found rh']);
  F.rh=(ones([Im Jm])).*NaN;
end,
clear rid

%-----------------------------------------------------------------------
%  Read in surface air temperature (Kelvin), convert to Celsius.
%-----------------------------------------------------------------------

tid=fopen(t_file,'r','s');
if (tid > 0),
  [W,Ncount]=fread(tid,Inf,'single'); fclose(tid);
  W(Ncount)=[]; W(1)=[];
  F.Tair=reshape(W,Im,Jm)-273.16;
else,
  disp(['Not found Tair']);
  F.Tair=(ones([Im Jm])).*NaN;
end,
clear tid

%-----------------------------------------------------------------------
%  Read in air pressure (mb) at mean sea level (MSL).
%-----------------------------------------------------------------------

Pid=fopen(P_file,'r','s');
if (Pid > 0),
  [W,Ncount]=fread(Pid,Inf,'single'); fclose(Pid);
  W(Ncount)=[]; W(1)=[];
  F.Pair=reshape(W,Im,Jm);
else,
  disp(['Not found Pair']);
  F.Pair=(ones([Im Jm])).*NaN;
end,
clear Pid

%-----------------------------------------------------------------------
%  Read in air pressure (mb) at local surface terrain-following level.
%-----------------------------------------------------------------------

pid=fopen(p_file,'r','s');
if (pid > 0),
  [W,Ncount]=fread(pid,Inf,'single'); fclose(pid);
  W(Ncount)=[]; W(1)=[];
  F.Psfc=reshape(W,Im,Jm);
else,
  disp(['Not found Psfc']);
  F.Psfc=(ones([Im Jm])).*NaN;
end,
clear pid

%-----------------------------------------------------------------------
%  Read in accumulated precipitation (mm).
%-----------------------------------------------------------------------

qid=fopen(q_file,'r','s');
if (qid > 0),
  [W,Ncount]=fread(qid,Inf,'single'); fclose(qid);
  W(Ncount)=[]; W(1)=[];
  F.q=reshape(W,Im,Jm);
else,
  disp(['Not found q']);
  F.q=(ones([Im Jm])).*NaN;
end,
clear qid

%-----------------------------------------------------------------------
%  Read in surface shortwave radiation flux (W/m2, positive upward).
%-----------------------------------------------------------------------

Sid=fopen(S_file,'r','s');
if (Sid > 0),
  [W,Ncount]=fread(Sid,Inf,'single'); fclose(Sid);
  W(Ncount)=[]; W(1)=[];
  F.Srad=reshape(W,Im,Jm);
else,
  disp(['Not found Srad']);
  F.Srad=(ones([Im Jm])).*NaN;
end,
clear Sid

%-----------------------------------------------------------------------
%  Read in surface longwave radiation flux (W/m2, positive upward).
%-----------------------------------------------------------------------

Lid=fopen(L_file,'r','s');
if (Lid > 0),
  [W,Ncount]=fread(Lid,Inf,'single'); fclose(Lid);
  W(Ncount)=[]; W(1)=[];
  F.Lrad=reshape(W,Im,Jm);
else,
  disp(['Not found Lrad']);
  F.Lrad=(ones([Im Jm])).*NaN;
end,
clear Lid

return
