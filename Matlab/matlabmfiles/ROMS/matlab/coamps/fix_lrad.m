%  This script fix the first record of the longwave radiation.

OutName='/coamps/mel/Jul00/netcdf/coamps_jul00_new.nc';

%-----------------------------------------------------------------------
%  Check size of time dimension.
%-----------------------------------------------------------------------

[dnames,dsizes]=nc_dim(OutName);
ndims=length(dsizes);
for n=1:ndims,
  name=deblank(dnames(n,:));
  switch (name),  
    case 'time',
      Nrec=dsizes(n);
  end,
end,

%-----------------------------------------------------------------------
%  Fix longwave radiation records.
%-----------------------------------------------------------------------

Lrec=1:24:Nrec;

Lrad=nc_read(OutName,'lwrad',2);
status=nc_write(OutName,'lwrad',Lrad,1);
for i=2:length(Lrec),
  Lrad1=nc_read(OutName,'lwrad',Lrec(i)-1);
  Lrad2=nc_read(OutName,'lwrad',Lrec(i));
  Lrad3=nc_read(OutName,'lwrad',Lrec(i)+1);
  if (min(min(Lrad2)) == 0 & max(max(Lrad2)) == 0),
    Lrad=0.5.*(Lrad1+Lrad3);
    status=nc_write(OutName,'lwrad',Lrad,Lrec(i));
  end,
end,
