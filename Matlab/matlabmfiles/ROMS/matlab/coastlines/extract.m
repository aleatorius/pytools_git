%  This Matlab script extracts coastline data from GSHHS database.

 job='seagrid';            % Prepare coastlines for SeaGrid
%job='ploting';            % Prepare coastlines for NCAR ploting programs

% database='full';          % Full resolution database
 database='high';          % High resolution database
%database='intermediate';  % Intermediate resolution database
%database='low';           % Low resolution database
%database='crude';         % crude resolution database

switch job,
  case 'seagrid'
%   Oname='/home/arango/ocean/matlab/coastlines/cadiz_coast.mat';
%   Oname='/home/arango/ocean/matlab/coastlines/japan_coast.mat';
%   Oname='/n2/arango/Natl/natl_coast.mat';
%   Oname='/n0/arango/Examples/Okhotsk/okhotsk_coast.mat';
%   Oname='/n0/arango/USwest/USwest_coast.mat';
    Oname='eac_coast.mat';
%   Oname='uswest_coast.mat';
%   Oname='norway_coast.mat';
%   Oname='/n2/arango/Med/med_coast.mat';
%   Oname='/home/arango/ocean/matlab/coastlines/uri_Fcoast.mat';
%   Oname='/home/arango/applications/IAS/ias_Hcoast.mat';
  case 'ploting'
%   Oname='/home/arango/ocean/plot/Data/global_high.cst';
%   Oname='/home/arango/ocean/matlab/coastlines/cadiz_coast.dat';
%   Oname='/home/arango/ocean/plot/Data/useast_coast.dat';
%   Oname='/home/arango/ocean/plot/Data/global_inter.cst';
%   Oname='/home/arango/ocean/plot/Data/NAwest_high.cst';
%   Oname='/home/arango/ocean/plot/Data/NAeast_full.cst';
%   Oname='/home/arango/ocean/plot/Data/Med_high.cst';
%   Oname='/home/arango/ocean/plot/Data/eac_high.cst';
    Oname='/home/arango/ocean/plot/Data/ias_high.cst';
end,

 GSHHS_DIR='/home/arango/ocean/GSHHS/Version_1.2/';
%GSHHS_DIR='/home/arango/ocean/GSHHS/Version_1.5/';

switch database,
  case 'full'
    Cname=strcat(GSHHS_DIR,'gshhs_f.b');
    name='gshhs_f.b';
  case 'high'
    Cname=strcat(GSHHS_DIR,'gshhs_h.b');
    name='gshhs_h.b';
  case 'intermediate'
    Cname=strcat(GSHHS_DIR,'gshhs_i.b');
    name='gshhs_i.b';
  case 'low'
    Cname=strcat(GSHHS_DIR,'gshhs_l.b');
    name='gshhs_l.b';
  case 'crude'
    Cname=strcat(GSHHS_DIR,'gshhs_c.b');
    name='gshhs_c.b';
end,

Llon=-15.0;                % Left   corner longitude     % Gulf of Cadiz
Rlon=-5.0;                 % Right  corner longitude
Blat=32.0;                 % Bottom corner latitude
Tlat=44.0;                 % Top    corner latitude

Llon=-180.0;               % Left   corner longitude     % NA West
Rlon=-100.0;               % Right  corner longitude
Blat=15.0;                 % Bottom corner latitude
Tlat=90.0;                 % Top    corner latitude

Llon=-100.0;               % Left   corner longitude     % NA East
Rlon=-50.0;                % Right  corner longitude
Blat=15.0;                 % Bottom corner latitude
Tlat=90.0;                 % Top    corner latitude

Llon=125.0;                % Left   corner longitude     % Sea of Japan
Rlon=145.0;                % Right  corner longitude
Blat=32.0;                 % Bottom corner latitude
Tlat=53.0;                 % Top    corner latitude

Llon=-100.5;               % Left   corner longitude     % North Atlantic
Rlon=  40.5;               % Right  corner longitude  
Blat=-31.5;                % Bottom corner latitude
Tlat= 72.5;                % Top    corner latitude

Llon=-180.0;               % Left   corner longitude     % Global
Rlon= 180.0;               % Right  corner longitude
Blat=-90.0;                % Bottom corner latitude
Tlat= 90.0;                % Top    corner latitude

Llon=-105.0;               % Left   corner longitude     % North Atlantic
Rlon=  45.0;               % Right  corner longitude
Blat=-38.0;                % Bottom corner latitude
Tlat= 75.0;                % Top    corner latitude

Llon=-134.0;              % Left   corner longitude     % US west Coast
Rlon=-118.0;              % Right  corner longitude
Blat=33.0;                % Bottom corner latitude
Tlat=49.0;                % Top    corner latitude

Llon=-10.0;               % Left   corner longitude     % Mediterranean Coast
Rlon=45.0;                % Right  corner longitude
Blat=30.0;                % Bottom corner latitude
Tlat=48.0;                % Top    corner latitude

Llon=100.0;               % Left   corner longitude     % Lake Vostok Coast
Rlon=110.0;               % Right  corner longitude
Blat=-79.0;               % Bottom corner latitude
Tlat=-75.0;               % Top    corner latitude

Llon=-127.0;              % Left   corner longitude     % Oregon coast
Rlon=-121.0;              % Right  corner longitude
Blat=35.0;                % Bottom corner latitude
Tlat=41.0;                % Top    corner latitude

Llon=-127.0;              % Left   corner longitude     % Oregon coast
Rlon=-121.0;              % Right  corner longitude
Blat=35.0;                % Bottom corner latitude
Tlat=41.0;                % Top    corner latitude

Llon=0.0;                 % Left   corner longitude     % Norway coast
Rlon=40.0;                % Right  corner longitude
Blat=58.0;                % Bottom corner latitude
Tlat=76.5;                % Top    corner latitude

Llon=140.0;               % Left   corner longitude     % Easter Australia coast
Rlon=165.0;               % Right  corner longitude
Blat=-50.0;               % Bottom corner latitude
Tlat=-20.0;               % Top    corner latitude

Llon=134.0;               % Left   corner longitude     % Okhostk Sea
Rlon=175.0;               % Right  corner longitude
Blat=43.0;                % Bottom corner latitude
Tlat=66.0;                % Top    corner latitude

Llon=-72.0;               % Left   corner longitude     % Rhode Island
Rlon=-70.5;               % Right  corner longitude
Blat=41.0;                % Bottom corner latitude
Tlat=42.0;                % Top    corner latitude

Llon=-104.0;              % Left   corner longitude     % Intra-America Seas
Rlon=-54.0;               % Right  corner longitude
Blat=-6.0;                % Bottom corner latitude
Tlat=37.0;                % Top    corner latitude

Llon=-134.0;              % Left   corner longitude     % SCB
Rlon=-114.0;              % Right  corner longitude
Blat=28.0;                % Bottom corner latitude
Tlat=40.0;                % Top    corner latitude

Llon=145.5;                % Left   corner longitude     % EAC
Rlon=155.0;                % Right  corner longitude
Blat=-45.0;                % Bottom corner latitude
Tlat=-25.0;                % Top    corner latitude

spval=999.0;              % Special value

%----------------------------------------------------------------------------
%  Extract coastlines from GSHHS database.
%----------------------------------------------------------------------------

disp(['Reading GSHHS database: ',name]);
[Coast]=r_gshhs(Llon,Rlon,Blat,Tlat,Cname);

disp(['Processing read coastline data']);
switch job,
  case 'seagrid'
    [C]=x_gshhs(Llon,Rlon,Blat,Tlat,Coast,'patch');
  case 'ploting'
    [C]=x_gshhs(Llon,Rlon,Blat,Tlat,Coast,'on');
end,

%----------------------------------------------------------------------------
%  Save extrated coastlines.
%----------------------------------------------------------------------------

lon=C.lon;
lat=C.lat;

switch job,
  case 'seagrid'
    save(Oname,'lon','lat');
  case 'ploting'
    x=lon;
    y=lat;
    ind=find(isnan(x));
    if (~isempty(ind)),
      if (length(ind) == length(C.type)),
        x(ind)=C.type;
        y(ind)=spval;

% Cliping of out-of-range values failed. Try original values.
      
      elseif (length(ind) == length(Coast.type)), 
	x(ind)=Coast.type;
        y(ind)=spval;
      else,      
	x(ind)=1;
        y(ind)=spval;
      end,
    end,
    fid=fopen(Oname,'w');
    if (fid ~= -1),
      for i=1:length(x),
        fprintf(fid,'%11.6f  %11.6f\n',y(i),x(i));
      end,
      fclose(fid);
    end,
end,
