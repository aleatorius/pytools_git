function [lon,lat,h]=x_etopo5(Llon,Rlon,Blat,Tlat,Fname);

%-----------------------------------------------------------------------
%  Read in ETOPO5 longitude and latitude.
%-----------------------------------------------------------------------

x=nc_read(Fname,'topo_lon');
y=nc_read(Fname,'topo_lat');

%-----------------------------------------------------------------------
%  Determine indices to extract.
%-----------------------------------------------------------------------

I=find(x >= Llon & x <= Rlon);
X=x(I);
[X,I]=sort(X);

J=find(y >= Blat & y <= Tlat);
Jmin=min(J);
Jmax=max(J);
Y=y(J); Y=Y';

%-----------------------------------------------------------------------
%  Read in bathymetry.
%-----------------------------------------------------------------------

topo=nc_read(Fname,'topo');
h=topo(I,Jmin:Jmax);
[Im,Jm]=size(h);

%-----------------------------------------------------------------------
%  Set coordinates of extracted bathymetry.
%-----------------------------------------------------------------------

lon=repmat(X,[1 Jm]);
lat=repmat(Y,[Im 1]);

return
