%  This script extract bathymetry from database for the specified
%  map coordinates.

job='seagrid';          %  prepare bathymetry for seagrid

database='etopo5';

switch job,
  case 'seagrid'
    Oname='/n0/arango/ocean/matlab/bath/cadiz_bathy.mat';
    Oname='/n0/arango/ocean/matlab/bath/japan_bathy.mat';
    Oname='/n0/arango/Examples/Okhotsk/okhotsk_bath.mat';
    Oname='/n0/arango/USwest/USwest_bath.mat';
    Oname='/n2/arango/Med/med_bath.mat';
    Oname='uswest_bath.mat';
    Oname='/n2/arango/Norway/norway_bath.mat';
end,

switch database,
  case 'etopo5'
    Bname='/n0/arango/ocean/matlab/bath/etopo5.nc';
end,

Llon=-15.0;             % Left   corner longitude
Rlon=-5.0;              % Right  corner longitude
Blat=32.0;              % Bottom corner latitude
Tlat=44.0;              % Top    corner latitude

Llon=125.0;               % Left   corner longitude
Rlon=145.0;                % Right  corner longitude
Blat=32.0;                 % Bottom corner latitude
Tlat=53.0;                 % Top    corner latitude

Llon=134.0;               % Left   corner longitude     % Okhostk Sea
Rlon=175.0;               % Right  corner longitude
Blat=43.0;                % Bottom corner latitude
Tlat=66.0;                % Top    corner latitude

Llon=-134.0;              % Left   corner longitude     % US west Coast
Rlon=-118.0;              % Right  corner longitude
Blat=33.0;                % Bottom corner latitude
Tlat=49.0;                % Top    corner latitude

Llon=-10.0;               % Left   corner longitude     % Mediterranean Coast
Rlon=45.0;                % Right  corner longitude
Blat=30.0;                % Bottom corner latitude
Tlat=48.0;                % Top    corner latitude

Llon=-127.0;              % Left   corner longitude     % US west coast
Rlon=-121.0;              % Right  corner longitude
Blat=35.0;                % Bottom corner latitude
Tlat=41.0;                % Top    corner latitude

Llon=-5.0;                % Left   corner longitude     % Norway
Rlon=40.0;                % Right  corner longitude
Blat=58.0;                % Bottom corner latitude
Tlat=76.5;                % Top    corner latitude

%-----------------------------------------------------------------------
%  Read and extract bathymetry.
%-----------------------------------------------------------------------

switch database,
  case 'etopo5'
    [lon,lat,h]=x_etopo5(Llon,Rlon,Blat,Tlat,Bname);
end,

%-----------------------------------------------------------------------
%  Process extracted data for requested task.
%-----------------------------------------------------------------------

switch job,
  case 'seagrid'
    xbathy=reshape(lon,1,prod(size(lon)))';
    ybathy=reshape(lat,1,prod(size(lat)))';
    zbathy=reshape(h,1,prod(size(h)))';
    zbathy=-zbathy;
    ind=find(zbathy<0);
    if (~isempty(ind)),
      zbathy(ind)=0;
    end;
    save(Oname,'xbathy','ybathy','zbathy');
end,
