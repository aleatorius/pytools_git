function Dominio3D = Dominio2DParaDominio3Dv2(MatrizXYZ, LayerDepth)
% A MatrizXYZ deve ser uma matriz de profundidades em que a 1� coluna representa as linhas, a 2� as colunas e a terceira as profundidades
% Esta matriz deve corresponder a um dominio com um n�mero de linhas = Lines e um n�mero de colunas = Columns
% O utilizador indica o numero de linhas e de colunas e o numero de camadas (Layers)que pretende criar
% O resultado � uma matriz com um numero de linhas = Lines * Columns * Layers e 4 colunas 
% A primeira coluna contem os n�meros das colunas, a segunda os n�meros das linhas e a terceira os n�meros das camadas de cada uma das c�lulas do modelo. 
% A quarta coluna contem as profundidades.

%prompt = {'Lines:','Columns:','Layers:','Layer depth:'};
prompt = {'Lines:','Columns:','Layers:'};
dlg_title = 'Cria�ao de um dominio 3D a partir de um dominio 2D';
num_lines= 1;
%def = {'91','63','5','2'};
def = {'91','63','5'};
answer  = inputdlg(prompt,dlg_title,num_lines,def);
Lines = str2double(answer(1));
Columns = str2double(answer(2));
Layers = str2double(answer(3));
%LayerDepth = str2double(answer(4));
X = MatrizXYZ;
S = 0;
for Layer = 1:Layers
   for Line = 1:Lines
      for Column = 1:Columns
        S = S + 1;
        y(S,1) = Column;
        y(S,2) = Line; %Lines + 1 - Line
        y(S,3) = Layer;
        if Layer == 1
           y(S,4) = min(LayerDepth(Layer),X(S,3));
        else
           index1 = (Line-1)*Columns+Column;
           ztotal = y(index1,4);
           for k = 2:Layer
              index2 = (k-1) * (Lines*Columns) + index1;
              ztotal = ztotal + max(0,y(index2,4));
           end
           if Layer < Layers
              y(S,4) = min(LayerDepth(Layer),X(index1,3)-ztotal);
           else
              y(S,4) = max(0,X(index1,3)-ztotal);
           end
        end
      end
   end
end
S = 0;
for Layer = 1:Layers
    for Line = 1:Lines  
        for Column = 1:Columns
            S = S + 1;
            z(S,1) = y(S,1);
            LineNr = Lines + 1 - Line;
            LayerNr = Layers + 1 - Layer;
            index = ((LineNr - 1) * Columns + Column) + (LayerNr - 1) * Lines * Columns;
            z(S,2) = y(index,2);
            z(S,3) = y(index,3);
            z(S,4) = y(index,4);
        end
    end
end   
Dominio3D = z;