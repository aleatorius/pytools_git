function [ freeboard ] = freeboard(ice_thickness,snow_thickness,ice_temperature,ice_bulk_salinity,water_temperature, water_salinity)
%  For the 
   rhos = 330; %density of snow kg/m3 
   rhoi = density_ice(ice_temperature,ice_bulk_salinity);
   rhow = sw_dens0(water_salinity,water_temperature);
   freeboard = ice_thickness - (rhos * snow_thickness + rhoi * ice_thickness) / rhow; 
end