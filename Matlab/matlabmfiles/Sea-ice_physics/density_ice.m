function [ sea_ice_density ] = density_ice( ice_temperature,ice_bulk_salinity )
%  From: Hunke, E.C., Lipscomb, W.H., Turner, A.K., Jeffery, N., Elliott
%  (2015). CICE: the Los Alamos Sea Ice Model. Documentation and user's
%  manual. Version 5.1. LA-CC_06_012. Los Alamos National Lab. (page 58)
   rhoi = 917; %density of pure ice k/m3 
   S = ice_bulk_salinity;
   Sbr = liquidus_brine_salinity_mush(ice_temperature);
   phi = liquid_fraction(ice_temperature,ice_bulk_salinity);
   rhobr = density_brine(Sbr);
   sea_ice_density = rhoi * (1-phi) + rhobr * phi; 
end

