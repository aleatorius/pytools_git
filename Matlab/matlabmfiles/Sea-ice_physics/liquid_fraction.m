function [phi] = liquid_fraction(Ice_temperature,Ice_bulk_salinity)
% Liquid fraction of mush from mush temperature and bulk salinity
% This function as well as functions called from here and constants used
% were taken from the columnar version of the Los Alamos Sea Ice Model (CICE),
% from file ice_constants_colpkg.F90 and ice_mushy_physics.F90
%   real(kind=dbl_kind), intent(in) :: &
%         zTin, & ! ice layer temperature (C)
%         zSin    ! ice layer bulk salinity (ppt)

%    real(kind=dbl_kind) :: &
%         phi , & ! liquid fraction
%         Sbr     ! brine salinity (ppt)
    puny   = 1.0e-11;
    zTin = Ice_temperature;
    zSin = Ice_bulk_salinity;
    [Sbr] = liquidus_brine_salinity_mush(zTin);
    Sbr = max(Sbr,puny);
    phi = zSin / max(Sbr, zSin);
end

