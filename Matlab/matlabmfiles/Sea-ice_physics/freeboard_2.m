function [ freeboard ] = freeboard_2(ice_thickness,snow_thickness,rhoi,rhow)
%  From: Hunke, E.C., Lipscomb, W.H., Turner, A.K., Jeffery, N., Elliott
%  (2015). CICE: the Los Alamos Sea Ice Model. Documentation and user's
%  manual. Version 5.1. LA-CC_06_012. Los Alamos National Lab. (page 11)
   rhos = 330; %density of snow kg/m3
   freeboard = ice_thickness - (rhos * snow_thickness + rhoi * ice_thickness) / rhow; 
end