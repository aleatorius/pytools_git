function [ enthalpy ] = enthalpy_ice(Ice_temperature,Ice_bulk_salinity)
% Enthalpy of mush from mush temperature and bulk salinity
% This function as well as functions called from here and constants used
% were taken from the columnar version of the Los Alamos Sea Ice Model (CICE),
% from file ice_constants_colpkg.F90 and ice_mushy_physics.F90

%    real(kind=dbl_kind), intent(in) :: &
%         zTin, & ! ice layer temperature (C)
%         zSin    ! ice layer bulk salinity (ppt)

%    real(kind=dbl_kind) :: &
%         zqin    ! ice layer enthalpy (J m-3) 

%    real(kind=dbl_kind) :: &
%         phi     ! ice liquid fraction
   c1        = 1.0;
   cp_ice    = 2106;             % specific heat of fresh ice (J/kg/K)
   cp_ocn    = 4218;             % specific heat of ocn    (J/kg/K)
   rhoi      = 917.0;            % density of ice (kg/m^3)
   rhow      = 1026.0;           % density of seawater (kg/m^3)
   Lsub      = 2.835e6;          % latent heat, sublimation freshwater (J/kg)
   Lvap      = 2.501e6;          % latent heat, vaporization freshwater (J/kg)
   Lfresh    = Lsub-Lvap;        % latent heat of melting of fresh ice (J/kg)

   zTin = Ice_temperature;
   zSin = Ice_bulk_salinity;
    
   [phi] = liquid_fraction(zTin, zSin)
    
   zqin = phi * (cp_ocn * rhow - cp_ice * rhoi) * zTin + rhoi * cp_ice * zTin - (c1 - phi) * rhoi * Lfresh;
   enthalpy = zqin;
end

