function [ rhobr ] = density_brine( brine_salinity )
%  From: Hunke, E.C., Lipscomb, W.H., Turner, A.K., Jeffery, N., Elliott
%  (2015). CICE: the Los Alamos Sea Ice Model. Documentation and user's
%  manual. Version 5.1. LA-CC_06_012. Los Alamos National Lab. (page 58)
   rhobr = 1000.3 + 0.78237 * brine_salinity + 2.8008e-4 * brine_salinity^2 
end