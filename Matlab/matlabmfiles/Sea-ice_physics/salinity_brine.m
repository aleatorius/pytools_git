function [ salinity ] = salinity_brine( ice_temperature )
%  From: Hunke, E.C., Lipscomb, W.H., Turner, A.K., Jeffery, N., Elliott
%  (2015). CICE: the Los Alamos Sea Ice Model. Documentation and user's
%  manual. Version 5.1. LA-CC_06_012. Los Alamos National Lab. (page 58)
   T0 = -7.636;   % Celsius degrees
   a1 = -18.48;   % g kg-1 K-1 
   a2 = -10.3085; % g kg-1 K-1
   b1 = 0;        % g kg-1
   b2 = 62.4;     % g kg-1
   T = ice_temperature; 
   if T >= T0
       l0 = 1;
   else
       l0 = 0;
   end
   J1 = b1 / a1;
   J2 = b2 / a2;
   L1 = (1 + b1 / 1000) / a1;
   L2 = (1 + b2 / 1000) / a2;
   salinity = (T + J1) / (T / 1000 + L1) * l0 + (T + J2) / (T / 1000 + L2) * (1 - l0)
end