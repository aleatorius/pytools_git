function [ enthalpy ] = enthalpy_snow(snow_temperature)
% Enthalpy of snow from temperature 
% This function waa taken from the columnar version of the Los Alamos Sea Ice Model (CICE),
% from file ice_constants_colpkg.F90 and ice_mushy_physics.F90

%    real(kind=dbl_kind), intent(in) :: &
%         zTin, & ! ice layer temperature (C)
%         zSin    ! ice layer bulk salinity (ppt)

%    real(kind=dbl_kind) :: &
%         zqin    ! ice layer enthalpy (J m-3) 

%    real(kind=dbl_kind) :: &
%         phi     ! ice liquid fraction
   rhos      = 392.0;            % density of snow (kg/m^3)
   cp_ice    = 2106;             % specific heat of fresh ice (J/kg/K)
   Lsub      = 2.835e6;          % latent heat, sublimation freshwater (J/kg)
   Lvap      = 2.501e6;          % latent heat, vaporization freshwater (J/kg)
   Lfresh    = Lsub-Lvap;        % latent heat of melting of fresh ice (J/kg)
   
   
   enthalpy = -rhos * (-cp_ice * snow_temperature + Lfresh);