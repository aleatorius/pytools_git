function [Sbr]=liquidus_brine_salinity_mush(Ice_temperature)
% Liquidus relation: equilibrium brine salinity as function of temperature
% based on empirical data from Assur (1958)
%   real(kind=dbl_kind), intent(in) :: &
%         zTin         ! ice layer temperature (C)

%   real(kind=dbl_kind) :: &
%         Sbr          ! ice brine salinity (ppt)

%   real(kind=dbl_kind) :: &
%         t_high   , & ! mask for high temperature liquidus region
%         lsubzero     ! mask for sub-zero temperatures
%   t_high   = merge(c1, c0, (zTin > Tb_liq))  %Choose c1 if zTin > Tb_liq else choose c0
%   lsubzero = merge(c1, c0, (zTin <= c0))     %Choose c1 if zTin > Tb_liq else choose c0
    c0   = 0.0
    c1   = 1.0;
    c1000= 1000.0; 
%   temperature to brine salinity
%  real(kind=dbl_kind), parameter :: &

    az1_liq = -18.48;
    bz1_liq = 0.0;
    az2_liq = -10.3085;
    bz2_liq = 62.4;
    bz1p_liq = bz1_liq / c1000;
    bz2p_liq = bz2_liq / c1000;
    J1_liq = bz1_liq / az1_liq;
    K1_liq = c1 / c1000;
    L1_liq = (c1 + bz1p_liq) / az1_liq;
    J2_liq = bz2_liq  / az2_liq;
    K2_liq = c1 / c1000;
    L2_liq = (c1 + bz2p_liq) / az2_liq;
    Tb_liq = -7.6362968855167352 % temperature of liquidus break
    zTin = Ice_temperature
    if (zTin > Tb_liq)
        t_high = c1;
    else
        t_high = c0;
    end
    if (zTin <= c0)
       lsubzero = c1;
    else
       lsubzero = c0;
    end   
   
    Sbr = ((zTin + J1_liq) / (K1_liq * zTin + L1_liq)) * t_high +  ((zTin + J2_liq) / (K2_liq * zTin + L2_liq)) * (c1 - t_high)

    Sbr = Sbr * lsubzero

end

