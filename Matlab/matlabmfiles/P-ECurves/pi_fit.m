function [ slope pmaxfit inhibit r2 exit ergebnis ] = pi_fit( i, p , fittyp )
%PI_FIT fits a PI-Curve
%   i is irradiance vector
%   p is productivity vector
%   fittype is 0 without photoinhibition and 1 with photoinhibition

% %remove NaN from p and i
l=1;
h=1;
while l<=length(i)
    if isnan(p(l))
        l=l+1;
        
    else
        inew(h)=i(l);
        pnew(h)=p(l);
        l=l+1;
        h=h+1;
    end
end
clear i p;
    i=inew';
    p=pnew';

if fittyp==0
    % Fitting without Photoinhibition
    
   %define the model
     fitfunc=fittype('a*(1-exp((-b*x)/a))');
     
    %fitoptions 
    % fitopts=fitoptions('Lower',[0 0 0]);
      %fitoptions 
    options = fitoptions(fitfunc);
    options.Lower = [0 0]; 
    options.Upper = [100 100]; 
    options.StartPoint= [1 1];
    
   % fitting
    [fitresult gof]=fit(i,p,fitfunc);
    params=coeffvalues(fitresult);
    params(3)=0;
    
elseif fittyp==1
    % Fitting with Photoinhibition

    %define the model
     fitfunc=fittype('a*(1-exp((-b*x)/a))*(exp((-c*x)/a))');
     
    %fitoptions 
    options = fitoptions('Method','NonlinearLeastSquares');
    options.Robust='on';
    options.Lower = [0 0 0]; 
    options.Upper = [Inf Inf Inf]; 
    options.StartPoint= [0.975 0.938 0.0286];
     
   % fitting
    [fitresult gof output]=fit(i,p,fitfunc,options);
    params=coeffvalues(fitresult);
    
end


slope=params(1);
pmaxfit=params(2);
inhibit=params(3);
r2=gof.rsquare;
exit=output.exitflag;
ergebnis=fitresult;



end

