%% Loads Data and fits all PI-Curves given in the table

% reads data from a tab-limited text-file, first column is irradiance
% values, following columns are PI-Curves
data=importdata('picurves.dat');
load('names.mat');
irradiances=data(:,1);
picurves=data(:,2:end);
% irradiances=data(1:end-1,1);
% picurves=data(1:end-1,2:end);

% Find amount of PI-Curves
amount=size(picurves,2);

%Which fittype should be used (0=No photoinhibition, 1= with
%photoinhibition)
fittyp=1;

%Iterate over all PI-Curves

result=zeros(5,amount);

%for k=1:amount
for k=1:amount

    
    PPvalues=picurves(:,k);
    
    [a1 pmaxfit inhibit, r2 exitflag ergebnis]=pi_fit(irradiances,PPvalues,fittyp);
    
    
    
    result(:,k)=[a1 pmaxfit inhibit r2 exitflag];
    
    %Plotting 
    figure;
     title(['PI-Curve: ' char(names(k))]);
     hold on;
     box on;
     scatter(irradiances,PPvalues,'b.');
     %plot(ergebnis);
     plot(min(irradiances):10:max(irradiances),ergebnis(min(irradiances):10:max(irradiances)),'r')
     legend('Data',['a=' num2str(a1) ' b=' num2str(pmaxfit) ' c=' num2str(inhibit) ' R^2=' num2str(r2) ' exitflag=' num2str(exitflag)],'Location','SouthOutside');
    xlabel('Irradiance [\mu E m^{-2} s^{-1}]');
    ylabel('NPP [\mug C \mug Chl^{-1} h^{-1} ]');
    
    saveas(gca,strcat('/home/pduarte/matlabmfiles/P-ECurves/PI',num2str(k)),'fig')  
    saveas(gca,strcat('/home/pduarte/matlabmfiles/P-ECurves/PI',num2str(k)),'jpg')
end

save('/home/pduarte/matlabmfiles/P-ECurves/result.mat','result');

