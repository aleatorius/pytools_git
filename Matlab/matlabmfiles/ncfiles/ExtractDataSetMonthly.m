function [ DataSet] = ExtractDataSet(Prefix,DataSetName,NumberOfFiles)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
index = 0;
for i = 1:NumberOfFiles
    if (i < 10) 
       FileName = strcat(Prefix,'0',int2str(i),'.nc');
    else
       FileName = strcat(Prefix,int2str(i),'.nc'); 
    end   
    info=ncinfo(FileName);
    [a,b] = size(info.Variables);
    for j = 1:b
        [ll,cc] = size(DataSetName);
        if strcmp(DataSetName(1:min(length(info.Variables(j).Name),cc)),info.Variables(j).Name)
            index = index + 1;
            DataSet(:,:,:,:,index) =ncread(FileName, DataSetName);
        end
    end   
end

