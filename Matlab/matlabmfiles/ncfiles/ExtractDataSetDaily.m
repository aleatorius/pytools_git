function [ DataSet] = ExtractDataSetDaily(Prefix,DataSetName,NumberOfFiles,Year,Month,Day)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
secs = date2unixsecs(Year,Month,Day,0,0,0);

index = 0;
for i = 1:NumberOfFiles
    [YEAR, MONTH, DAY, HOUR, MINUTE, SECOND] = unixsecs2date(secs);
    syear = num2str(YEAR);
    smonth = num2str(MONTH);
    if MONTH < 10 smonth = strcat('0',smonth);
    end
    sday = num2str(DAY);
    if DAY < 10 sday = strcat('0',sday);
    end
    FileName = strcat(Prefix,syear,'-',smonth,'-',sday,'.nc');   
    info=ncinfo(FileName);
    [a,b] = size(info.Variables);
    DataSetName = DataSetName(find(~isspace(DataSetName))); %Removes blanks from string
    for j = 1:b
        [ll,cc] = size(DataSetName);
        %if strcmp(DataSetName(1:min(length(info.Variables(j).Name),cc)),info.Variables(j).Name)
         if strcmp(info.Variables(j).Name,DataSetName)   
            index = index + 1;
            
            %DataSet(:,:,:,:,index) =ncread(FileName, DataSetName(1:min(length(info.Variables(j).Name),cc)));
            DataSet(:,:,:,:,index) =ncread(FileName,DataSetName);
        end
    end  
    secs = secs + 3600*24;
end

