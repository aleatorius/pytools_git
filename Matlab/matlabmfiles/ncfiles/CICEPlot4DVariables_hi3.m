function CICEPlot4DVariables_hi3(FilePrefix,DataSetName,NumberOfFiles,Year,Month,Day,Hour,DeltaT,NumberOfIceTypes,NumberOfIceLayers,GraphFilePrefix,FigureTitle)
%  This function is to be used only with ice variables vertically resolved
%  such as Sinz. These variables have dimensions:
%  grid_linesXgrid_columnsXgrid_layers
%  This function reads time series of CICE output from a specific date
%  and for a specific amount of time, determined by the number of files
%  that are to be read, plots the time series in separate m,aps and graphs
%  and saves the plots in the same directory where files are located
%  FilePrefix - Firt characters of CICE history file name, tipically,
%  'iceh.'
%  DataSetName - This is a dataset name
%  NumberOfFiles - Number of history output files to be read. This can be
%  daily files or files or any other type of frequency files
%  Year, Month, Day, Hour - correspond to the date that defines part of the name
%  of the first history file that is to be read.
%  DeltaT - Number of hours between two files
%  Number of ice types - The function will plot a surface for each ice type
%  Number of ice layers - This is necessary because different numner of
%  layers are used for the physical and for the bgc variables
%  GraphOutputPrefix - First part of the name for the graph files. For
%  example 'Sim1', or anything else allowing to distnguish these files 
%  from any others. The second part of the name is determined by the
%  variable name.
%  Figure title - a tring that is placed at the figure top
%  Note - For the time being this function is prepared to read daily files
%  only!!!!!!!!!!!!!!!!!


for i = 1:NumberOfFiles
   if i == 1
      secs(i) = date2unixsecs(Year,Month,Day);
   else
      secs(i) = secs(i-1) + DeltaT*3600;
   end   
   [AYear, AMonth, ADay] = unixsecs2date(secs(i));
   YearStr = int2str(AYear);
   MonthStr = int2str(AMonth);
   if AMonth < 10
      MonthStr = strcat('0',MonthStr);
   end
   DayStr = int2str(ADay);
   if ADay < 10
      DayStr = strcat('0',DayStr);
   end    
   DateStr(i,:) = strcat(DayStr,'-',MonthStr);
end     
XAxisStep = 15; %Days - this is optional
%[DataSet] = DataSetArray
[DataSet] = ExtractDataSetDaily(FilePrefix,DataSetName,NumberOfFiles,Year,Month,Day); 
%size(DataSet)
hi = ExtractDataSetDaily('iceh.','vicen',NumberOfFiles,Year,Month,Day);
aice = ExtractDataSetDaily('iceh.','aicen',NumberOfFiles,Year,Month,Day);
hs = ExtractDataSetDaily('iceh.','hs',NumberOfFiles,Year,Month,Day);
hsnow(1:NumberOfFiles) = hs(3,3,1,1,1:NumberOfFiles);  
for ii = 1:NumberOfIceTypes
    %Data reading for a contour plot*********************************************************************
    y(1:NumberOfIceLayers,1:NumberOfFiles) = DataSet(3,3,1:NumberOfIceLayers,ii,1:NumberOfFiles); 
    yy = zeros(NumberOfIceLayers,NumberOfFiles);
    depth = zeros(NumberOfIceLayers,NumberOfFiles);
    if (max(max(y(:,:))) ~= 0 | min(min(y(:,:))) ~= 0)
        FigureName = strcat(GraphFilePrefix,'_','ice_type',int2str(ii));
        %Contour plot construction*******************************************************************
        figure
        hold on
        for jj = 1:NumberOfFiles
            if ~isnan(aice(3,3,ii,1,jj)) & aice(3,3,ii,1,jj) > 0.01 
            Ice_thickness(jj) = hi(3,3,ii,1,jj)/aice(3,3,ii,1,jj);  
            yy(:,jj) = y(:,jj);
            VerticalDepthIncr = Ice_thickness(jj)/(NumberOfIceLayers-1);
            depth(1,jj) = 0.0;
            for k = 2:NumberOfIceLayers
                depth(k,jj) = depth(k-1,jj) + VerticalDepthIncr;
            end
            %depth(k,jj)
            Input(1:NumberOfIceLayers,1) = depth(1:NumberOfIceLayers,jj);
            Input(1:NumberOfIceLayers,2) = yy(1:NumberOfIceLayers,jj);
            InterpolatedIceData = LinearInterpolationTimeSeries(Input,VerticalDepthIncr/10,'Temp.xls');
            imagesc(jj,InterpolatedIceData(:,1),InterpolatedIceData(:,2));
            %imagesc(jj,depth(:,jj),yy(:,jj))
            end
        end
        colorbar
        ax1 = gca
        set(ax1,'Fontsize',20)
        ylabel(ax1,'Ice thickness (m)')
        title(ax1,FigureTitle)
        set(ax1,'XLim',[1 NumberOfFiles])
        set(ax1,'YLim',[-1 max(max(depth))])
        set(ax1,'XTick', 1:XAxisStep:NumberOfFiles)
        set(ax1,'XTickLabel',DateStr(1:XAxisStep:NumberOfFiles,:))
        set(ax1,'YDir','Reverse')
        hold on
        plot(1:NumberOfFiles,-hsnow,'c','LineWidth',4);
        saveas(gca,strcat(FigureName,'_contour'),'fig')
    end
end


