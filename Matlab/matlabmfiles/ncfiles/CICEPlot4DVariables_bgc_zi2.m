function [DataSet] = CICEPlot4DVariables_bgc_zi2(FilePrefix,DataSet,Ice_thickness,NumberOfFiles,Year,Month,Day,Hour,GraphFilePrefix,DeltaT,NumberOfIceTypes,NumberOfBGCLayers)
%  This function is to be used only with ice variables vertically resolved
%  such as Sinz. These variables have dimensions:
%  grid_linesXgrid_columnsXgrid_layers
%  This function reads time series of CICE output from a specific date
%  and for a specific amount of time, determined by the number of files
%  that are to be read, plots the time series in separate m,aps and graphs
%  and saves the plots in the same directory where files are located
%  FilePrefix - Firt characters of CICE history file name, tipically,
%  'iceh.'
%  DataSetStrArray - This is a character array with n lines, corresponding
%  to the number of variables that are to be read and with t columns,
%  corresponding to the size of the largest variable name
%  NumberOfFiles - Number of history output files to be read. This can be
%  daily files or files or any other type of frequency files
%  Year, Month, Day - correspond to the date that defines part of the name
%  of the first history file that is to be read.
%  GraphOutputPrefix - First part of the name for the graph files. For
%  example 'Sim1', or anything else allowing to distnguish these files 
%  from any others. The second part of the name is determined by the
%  variable name.
%  DeltaT - Number of hours between two files
%  Note - For the time being this function is prepared to read daily files
%  only!!!!!!!!!!!!!!!!!


for i = 1:NumberOfFiles
   if i == 1
      secs(i) = date2unixsecs(Year,Month,Day);
   else
      secs(i) = secs(i-1) + DeltaT*3600;
   end   
   [AYear, AMonth, ADay] = unixsecs2date(secs(i));
   YearStr = int2str(AYear);
   MonthStr = int2str(AMonth);
   if AMonth < 10
      MonthStr = strcat('0',MonthStr);
   end
   DayStr = int2str(ADay);
   if ADay < 10
      DayStr = strcat('0',DayStr);
   end    
   DateStr(i,:) = strcat(DayStr,'-',MonthStr);
end     
XAxisStep = 30; %Days - this is optional
'Reading brine height'
[BrineHeight] = ExtractDataSetDaily(FilePrefix,'hbrine',NumberOfFiles,Year,Month,Day);

'Finished with brine height'     
for ii = 1:NumberOfIceTypes
         ii
         %Data reading for a contour plot*********************************************************************  
         y(1,1:NumberOfFiles) = 0.0;
         y(2:NumberOfBGCLayers,1:NumberOfFiles) = DataSet(3,3,1:NumberOfBGCLayers-1,ii,1:NumberOfFiles);
         yy = zeros(NumberOfBGCLayers,NumberOfFiles);
         %depth = zeros(NumberOfBGCLayers,NumberOfFiles);
         if (max(max(y(:,:))) ~= 0 | min(min(y(:,:))) ~= 0)
            
            %Contour plot construction*******************************************************************
            figure 
            hold on
            for jj = 1:NumberOfFiles  
               if ~isnan(BrineHeight(3,3,1,1,jj)) & BrineHeight(3,3,1,1,jj) > 0.01       
                  VerticalDepthIncr = BrineHeight(3,3,1,1,jj)/(NumberOfBGCLayers-3);
                  VerticalDim = ceil(Ice_thickness(3,3,1,1,jj)/VerticalDepthIncr); 
                  error = Ice_thickness(3,3,1,1,jj)-VerticalDim*VerticalDepthIncr;
                  VerticalDepthIncr=VerticalDepthIncr+error/VerticalDim;
                  yy(:,jj) = y(:,jj);
                  TopLevel = Ice_thickness(3,3,jj) - BrineHeight(3,3,1,1,jj)
                  depth(1,jj) = 0.0;
                  Var(1,jj) = 0.0;
                  index = 3;    
                  for k = 2:VerticalDim  % First two layers are snow          
                     depth(k,jj) = depth(k-1,jj) + VerticalDepthIncr;                
                     if depth(k,jj) < TopLevel 
                        Var(k,jj) = 0.0;
                     else
                        index = index + 1;
                        Var(k,jj) = yy(index,jj); 
                     end    
                  end
                  depth(1:VerticalDim,jj)
                  imagesc(jj,depth(1:VerticalDim,jj),Var(1:VerticalDim,jj));
               end
            end   
            colorbar
            ax1 = gca
            set(ax1,'Fontsize',20)
            ylabel(ax1,'Ice thickness (m)')
            title(ax1,strcat('({\mu}M)'))
            set(ax1,'XLim',[1 NumberOfFiles])
            set(ax1,'YLim',[0 max(max(depth))])
            set(ax1,'XTick', 1:XAxisStep:NumberOfFiles)
            set(ax1,'XTickLabel',DateStr(1:XAxisStep:NumberOfFiles,:))
            set(ax1,'YDir','Reverse')
     
         end
end         


