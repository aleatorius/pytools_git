function [DataSet] = ExtractDataSetTimeStep(Prefix,DataSetName,NumberOfFiles,Year,Month,Day,InitialStep,DT)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%InitialStep
hours = floor(InitialStep/3600)
minuts = floor(InitialStep/60);
seconds = (InitialStep/60 - minuts)*60;
secs = date2unixsecs(Year,Month,Day,hours,minuts,seconds)

index = 0;
Step = InitialStep;
for i = 1:NumberOfFiles
  
    %if rem(Step,86400) == 0
    %   [YEAR, MONTH, DAY, HOUR, MINUTE, SECOND] = unixsecs2date(secs);
    %else   
       [YEAR, MONTH, DAY, HOUR, MINUTE, SECOND] = unixsecs2date(secs);
    %end
    syear = num2str(YEAR);
    smonth = num2str(MONTH);
    if MONTH < 10 smonth = strcat('0',smonth);
    end
    sday = num2str(DAY);
    if DAY < 10 sday = strcat('0',sday);
    end
    %if rem(Step,86400) == 0
    if Step < 10
        StrStep = strcat('0000',num2str(Step));
    end    
    if Step >= 10 & Step < 100   
        StrStep = strcat('000',num2str(Step)); 
    end    
    if Step >= 100 & Step < 1000   
        StrStep = strcat('00',num2str(Step)); 
    end
    if Step >= 1000 & Step < 10000   
        StrStep = strcat('0',num2str(Step)); 
    end
    if Step >= 10000
        StrStep = num2str(Step); 
    end    
       
    FileName = strcat(Prefix,syear,'-',smonth,'-',sday,'-',StrStep,'.nc');
    
    info=ncinfo(FileName);
    [a,b] = size(info.Variables);
    for j = 1:b
        [ll,cc] = size(DataSetName);
        if strcmp(DataSetName(1:min(length(info.Variables(j).Name),cc)),info.Variables(j).Name)
            index = index + 1;
            DataSet(:,:,:,:,index) =ncread(FileName, DataSetName);
        end
    end  
    secs = secs + DT;
    Step = Step + DT;
    if (Step == 86400)
        Step = 0;
    end    
    
end

