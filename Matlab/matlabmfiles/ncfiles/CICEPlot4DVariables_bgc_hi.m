function [DataSet] = CICEPlot4DVariables_bgc_hi(FilePrefix,DataSetStrArray,Ice_thickness,NumberOfFiles,Year,Month,Day,Hour,GraphFilePrefix,DeltaT,NumberOfIceTypes,NumberOfBGCLayers)
%  This function is to be used only with ice variables vertically resolved
%  such as Sinz. These variables have dimensions:
%  grid_linesXgrid_columnsXgrid_layers
%  This function reads time series of CICE output from a specific date
%  and for a specific amount of time, determined by the number of files
%  that are to be read, plots the time series in separate m,aps and graphs
%  and saves the plots in the same directory where files are located
%  FilePrefix - Firt characters of CICE history file name, tipically,
%  'iceh.'
%  DataSetStrArray - This is a character array with n lines, corresponding
%  to the number of variables that are to be read and with t columns,
%  corresponding to the size of the largest variable name
%  NumberOfFiles - Number of history output files to be read. This can be
%  daily files or files or any other type of frequency files
%  Year, Month, Day - correspond to the date that defines part of the name
%  of the first history file that is to be read.
%  GraphOutputPrefix - First part of the name for the graph files. For
%  example 'Sim1', or anything else allowing to distnguish these files 
%  from any others. The second part of the name is determined by the
%  variable name.
%  DeltaT - Number of hours between two files
%  Note - For the time being this function is prepared to read daily files
%  only!!!!!!!!!!!!!!!!!
[a,b]= size(DataSetStrArray);

for i = 1:NumberOfFiles
   if i == 1
      secs(i) = date2unixsecs(Year,Month,Day);
   else
      secs(i) = secs(i-1) + DeltaT*3600;
   end   
   [AYear, AMonth, ADay] = unixsecs2date(secs(i));
   YearStr = int2str(AYear);
   MonthStr = int2str(AMonth);
   if AMonth < 10
      MonthStr = strcat('0',MonthStr);
   end
   DayStr = int2str(ADay);
   if ADay < 10
      DayStr = strcat('0',DayStr);
   end    
   DateStr(i,:) = strcat(DayStr,'-',MonthStr);
end     
XAxisStep = 5; %Days - this is optional
[BrineHeight] = ExtractDataSetDaily(FilePrefix,'hbrine',NumberOfFiles,Year,Month,Day);
for   i = 1:a
     [DataSet] = ExtractDataSetDaily(FilePrefix,DataSetStrArray(i,:,:,:),NumberOfFiles,Year,Month,Day);
     
     for ii = 1:NumberOfIceTypes
         %Data reading for a contour plot*********************************************************************        
         y(1:NumberOfBGCLayers,1:NumberOfFiles) = DataSet(3,3,1:NumberOfBGCLayers,ii,1:NumberOfFiles);
         yy = zeros(NumberOfBGCLayers,NumberOfFiles);
         depth = zeros(NumberOfBGCLayers,NumberOfFiles);
         if (max(max(y(:,:))) ~= 0 | min(min(y(:,:))) ~= 0)
            FigureName = strcat(GraphFilePrefix,'_','ice_type',int2str(ii),DataSetStrArray(i,:),'fig'); 
            %Contour plot construction*******************************************************************
            figure 
            hold on
            for jj = 1:NumberOfFiles
               yy(:,jj) = y(:,jj);
               TopLevel = Ice_thickness(3,3,jj) - BrineHeight(3,3,1,1,jj)
               %depth(2,jj) = TopLevel/2;
               depth(3,jj) = TopLevel;
               for k = 4:NumberOfBGCLayers  % First two layers are snow               
                  depth(k,jj) = depth(k-1,jj) + BrineHeight(3,3,1,1,jj)/(NumberOfBGCLayers-2);
               end
               %imagesc(jj,0.0:BrineHeight(3,3,1,1,jj)/(NumberOfBGCLayers-2):TopLevel,0); 
               %imagesc(jj,0.0:TopLevel,0); 
               hold on
               %imagesc(jj,depth(3:18,jj),yy(3:18,jj));
               
               %imagesc(jj,depth(2:18,jj),yy(2:18,jj));
            end   
            colorbar
            ax1 = gca
            set(ax1,'Fontsize',20)
            ylabel(ax1,'Ice thickness (m)')
            title(ax1,strcat(DataSetStrArray,' ','({\mu}M)'))
            set(ax1,'XLim',[1 NumberOfFiles])
            set(ax1,'YLim',[0 max(max(depth))])
            set(ax1,'XTick', 1:XAxisStep:NumberOfFiles)
            set(ax1,'XTickLabel',DateStr(1:XAxisStep:NumberOfFiles,:))
            set(ax1,'YDir','Reverse')
            saveas(gca,strcat(FigureName,'_contour'),'fig')  
         end
     end         
end

