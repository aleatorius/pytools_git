function CICEPlotWithFreeBoard_v1(Prefix,DataSetName,NumberOfFiles,Year,Month,Day,NumberOfIceLayers,DT,DeltaX,Title,BGC_Variable,hi,aice,hs,snoice,Sinz,Tinz,MyDataSet,Tsnz,sss,sst)
ilayer = 1;
flayer = NumberOfIceLayers;
if BGC_Variable
    hbrine = ExtractDataSetDaily('iceh.','hbrine',NumberOfFiles,Year,Month,Day);
    ilayer = 3;
    flayer = NumberOfIceLayers + 3; 
end
%hi = ExtractDataSetDaily('iceh.','vicen',NumberOfFiles,Year,Month,Day);
%aice = ExtractDataSetDaily('iceh.','aicen',NumberOfFiles,Year,Month,Day);
%hs = ExtractDataSetDaily('iceh.','hs',NumberOfFiles,Year,Month,Day);
%snoice = ExtractDataSetDaily('iceh.','snoice',NumberOfFiles,Year,Month,Day);
%Sinz = ExtractDataSetDaily('iceh.','Sinz',NumberOfFiles,Year,Month,Day);
%Tinz = ExtractDataSetDaily('iceh.','Tinz',NumberOfFiles,Year,Month,Day);
%MyDataSet = ExtractDataSetDaily('iceh.',DataSetName,NumberOfFiles,Year,Month,Day);
%Tsnz = ExtractDataSetDaily('iceh.','Tsnz',NumberOfFiles,Year,Month,Day);
%sss = ExtractDataSetDaily('iceh.','sss',NumberOfFiles,Year,Month,Day);
%sst = ExtractDataSetDaily('iceh.','sst',NumberOfFiles,Year,Month,Day);

% Calculation of ice free board

for jj = 1:NumberOfFiles
   Ice_thickness(jj) = hi(3,3,1,1,jj)/aice(3,3,1,1,jj);
   Snow_thickness(jj) = hs(3,3,1,1,jj)/aice(3,3,1,1,jj); 
   if Snow_thickness(jj) < 0 | Snow_thickness(jj) > 10
     Snow_thickness(jj) = 0;
   end
   Soma = 0.0;  
   for i = 1:NumberOfIceLayers
     Soma = Soma + density_ice(Tinz(3,3,i,1,jj),Sinz(3,3,i,1,jj));        
   end
   MeanIceDensity = Soma / NumberOfIceLayers;
   WaterDensity = sw_dens0(sss(3,3,jj),sst(3,3,jj));
   free_board(jj) = freeboard_2(Ice_thickness(jj),Snow_thickness(jj),MeanIceDensity,WaterDensity);
end

% Estimation of vertical spatial step for ice and snow plottting

Vertical_step_for_ice = min(Ice_thickness)/30.0;
Vertical_step_for_snow = max(min(Snow_thickness)/30.0,0.0001);

%
% Preparation of array for interpolation

Time = date2unixsecs(Year,Month,Day);
index = 0;
for j = 1:NumberOfFiles
   z = 0;
   for i = ilayer:flayer
     z = z + Ice_thickness(j)/(flayer-ilayer+1);
     index = index + 1;
     MyData(index,1) = z;
     MyData(index,2) = Time;
     MyData(index,3) = MyDataSet(3,3,i,1,j);
   end
   Time = Time + 24 * 3600;
end
save('MyData.mat', 'MyData')
load('MyData.mat','MyData');
%
% Interpolation of ice thickness data

[a,b] = size(MyData);
Time = MyData(1,2):3600*24:MyData(a-(flayer-ilayer+1-1),2);
Input(:,1) = Time;
Input(:,2) = Ice_thickness;
InterpolatedIceData = LinearInterpolationTimeSeries(Input,3600,'Temp.xls');
if BGC_Variable
   Input(:,2) = hbrine(3,3,1,1,:)
   InterpolatedBrineHeight = LinearInterpolationTimeSeries(Input,3600,'Temps.xls'); 
end   
%

% Node definition and gridfit call

xnodes = 0:Vertical_step_for_ice:max(InterpolatedIceData(:,2));
ynodes = MyData(1,2):3600:MyData(a-(flayer-ilayer+1-1),2);
[s,t] = size(xnodes);
[e,f] = size(ynodes);
[ice_grid,xgrid,ygrid] = gridfit(MyData(:,1),MyData(:,2),MyData(:,3),xnodes,ynodes,'interp','triangle');
%
% Interpolation of auxiliary variables and creation of snow grid

Input(:,2) = Snow_thickness;
InterpolatedSnowData = LinearInterpolationTimeSeries(Input,3600,'Temps.xls');
Input(:,2) = snoice(3,3,1,1,:);
InterpolatedSnowIceData = LinearInterpolationTimeSeries(Input,3600,'Temps.xls');
Input(:,2) = free_board;
InterpolatedFreeBoard = LinearInterpolationTimeSeries(Input,3600,'Temps.xls');
Input(:,2) = Tsnz(3,3,1,1,:);
for i = 1:NumberOfFiles
   if Input(i,2) > 0.0
      Input(i,2) = 0.0;
   end
end
InterpolatedSnowTemperature = LinearInterpolationTimeSeries(Input,3600,'Temps.xls');
xsnow_nodes = 0:Vertical_step_for_snow:max(InterpolatedSnowData(:,2));
for i = 1:length(ynodes)
   snow_grid(1:length(xsnow_nodes),i) = min(InterpolatedSnowTemperature(i,2),0.0);
end

% Map plots

figure
hold on
XAxisStep = DeltaX*24;
for j = 1:length(ynodes)
   i = 1;
   Depth(i) = -InterpolatedFreeBoard(j,2);
   Found = 0;
   while Depth(i) < InterpolatedIceData(j,2) - InterpolatedFreeBoard(j,2) & i < t
      i = i + 1;
      Depth(i) = Depth(i-1) + Vertical_step_for_ice;
      if BGC_Variable
         if Depth(i) >= InterpolatedIceData(j,2) - InterpolatedBrineHeight(j,2) & Found == 0
            BrineLimitIndex(j) = i;
            Found = 1;
         end
      end
   end
   if ~BGC_Variable
      if Depth(i) > 0
         imagesc(j,Depth(1:i),transpose(ice_grid(j,1:i)))
      end
   else
      if Depth(i) > 0 
         ZeroValues(1:BrineLimitIndex(j),1) = 0.0;
         imagesc(j,Depth(1:BrineLimitIndex(j)),ZeroValues(1:BrineLimitIndex(j)))
         imagesc(j,Depth(BrineLimitIndex(j):i),transpose(ice_grid(j,BrineLimitIndex(j):i)))
      end
   end
   ii = 1;
   Depth_of_snow(ii) = -InterpolatedFreeBoard(j,2);
   while Depth_of_snow(ii) > -InterpolatedSnowData(j,2) - InterpolatedFreeBoard(j,2) & ii < length(xsnow_nodes)
      ii = ii + 1;
      Depth_of_snow(ii) = Depth_of_snow(ii-1) - Vertical_step_for_snow;
   end
   %min(snow_grid(1:ii,j))
   if Depth_of_snow(ii) < 0.0 & min(snow_grid(1:ii,j)) <= 0 & strcmp(DataSetName,'Tinz')
      imagesc(j,Depth_of_snow(1:ii),snow_grid(1:ii,j))
   end
   SnowDepth(j) = Depth_of_snow(ii);
   [AYear, AMonth, ADay] = unixsecs2date(ynodes(j) - 3600 * 24);
   
   YearStr = int2str(AYear);
   MonthStr = int2str(AMonth);
   if AMonth < 10
      MonthStr = strcat('0',MonthStr);
   end
   DayStr = int2str(ADay);
   if ADay < 10
     DayStr = strcat('0',DayStr);
   end
   DateStr(j,:) = strcat(DayStr,'-',MonthStr);
end

ax1 = gca;
set(ax1,'Fontsize',20)
ylabel(ax1,'Ice depth and snow height (m)')
set(ax1,'YLim',[min(-InterpolatedSnowData(:,2)) max(InterpolatedIceData(:,2))])
set(ax1,'YTick', min(-InterpolatedSnowData(:,2)):0.5:max(InterpolatedIceData(:,2)))
set(ax1,'YTickLabel',min(-InterpolatedSnowData(:,2)):0.5:max(InterpolatedIceData(:,2)))
set(ax1,'XTick', 1:XAxisStep:length(ynodes))
set(ax1,'XTickLabel',DateStr(1:XAxisStep:length(DateStr),:))
set(ax1,'XLim',[1 length(ynodes)])
set(ax1,'YDir','Reverse')
colorbar('YTick',[-30 -25 -20 -15 -10 -5 -2],'Fontsize',20)
colormap(jet)
title(Title)
xlabel('Dates in 2015')
hold on
plot(1:length(ynodes),-InterpolatedFreeBoard,'w','LineWidth',4)
sea_level = zeros(length(ynodes));
plot(1:length(ynodes),sea_level,'m','LineWidth',4)
