function [DataSet] = Map2DVariables(Prefix,DataSetName,NumberOfFiles,Year,Month,Day,InitialStep,DT)
%  This function is to be used only with ice variables vertically resolved
%  such as Sinz. These variables have dimensions:
%  grid_linesXgrid_columnsXgrid_layers
%  This function reads time series of CICE output from a specific date
%  and for a specific amount of time, determined by the number of files
%  that are to be read, plots the time series in separate m,aps and graphs
%  and saves the plots in the same directory where files are located
%  FilePrefix - Firt characters of CICE history file name, tipically,
%  'iceh.'
%  DataSetStrArray - This is a character array with n lines, corresponding
%  to the number of variables that are to be read and with t columns,
%  corresponding to the size of the largest variable name
%  NumberOfFiles - Number of history output files to be read. This can be
%  daily files or files or any other type of frequency files
%  Year, Month, Day - correspond to the date that defines part of the name
%  of the first history file that is to be read.
%  GraphOutputPrefix - First part of the name for the graph files. For
%  example 'Sim1', or anything else allowing to distnguish these files 
%  from any others. The second part of the name is determined by the
%  variable name.
%  DeltaT - Number of hours between two files
%  Note - For the time being this function is prepared to read daily files
%  only!!!!!!!!!!!!!!!!!



     [ DataSet] = ExtractDataSetTimeStep(Prefix,DataSetName,NumberOfFiles,Year,Month,Day,InitialStep,DT);
     %Data reading for a contour plot*********************************************************************        
     y(:,:,1:NumberOfFiles) = DataSet(:,:,1,1,1:NumberOfFiles);
     [d1,d2, d3] = size(y);
     for ii = 1:NumberOfFiles
         ii 
         %if (max(max(y(:,:))) ~= 0 | min(min(y(:,:))) ~= 0)
            FigureName = strcat(int2str(ii),'_',DataSetName,DataSetName)
            %Contour plot construction*******************************************************************
            figure     
            x(1:d2,1:d1) = transpose(y(1:d1,1:d2,ii));
            pcolor(1:d1,1:d2,x);
            shading interp
            colorbar
            ax1 = gca
            set(ax1,'Fontsize',20)
            %ylabel(ax1,'Ice layers')
            %title(ax1,strcat('Ice type','-',int2str(ii)))
            %set(ax1,'XLim',[1 NumberOfFiles])
            %set(ax1,'XTick', 1:XAxisStep:NumberOfFiles)
            %set(ax1,'XTickLabel',DateStr(1:XAxisStep:NumberOfFiles,:))
            saveas(gca,strcat(FigureName,'_contour'),'fig') 
            close
            %*****************************************************************************************************
         %end   
     end
     close
end
