function CICEPlot2(DataSet,NumberOfFiles,Year,Month,Day,Hour,GraphFilePrefix,DeltaT)
%  This function is to be used only with ice bulk variables representing
%  average grid cell quantities, such as aice and hi. These variables have dimensions:
%  grid_linesXgrid_columns
%  This function reads time series of CICE output from a specific date
%  and for a specific amount of time, determined by the number of files
%  that are to be read, plots the time series in separate graphs
%  and saves the plots in the same directory where files are located
%  FilePrefix - Firt characters of CICE history file name, tipically,
%  'iceh.'
%  DataSetStrArray - This is a character array with n lines, corresponding
%  to the number of variables that are to be read and with t columns,
%  corresponding to the size of the largest variable name
%  NumberOfFiles - Number of hisdtory output files to be read. This can be
%  daily files or files or any other type of frequency files
%  Year, Month, Day - correspond to the date that defines part of the name
%  of the first history file that is to be read.
%  GraphOutputPrefix - First part of the name for the graph files. For
%  example 'Sim1', or anything else allowing to distnguish these files 
%  from any others. The second part of the name is determined by the
%  variable name.
%  DeltaT - Number of hours between two files
%  Note - For the time being this function is prepared to read daily files
%  only!!!!!!!!!!!!!!!!!


for i = 1:NumberOfFiles
   if i == 1
      secs(i) = date2unixsecs(Year,Month,Day);
   else
      secs(i) = secs(i-1) + DeltaT*3600;
   end   
   [AYear, AMonth, ADay] = unixsecs2date(secs(i));
   YearStr = int2str(AYear);
   MonthStr = int2str(AMonth);
   if AMonth < 10
      MonthStr = strcat('0',MonthStr);
   end
   DayStr = int2str(ADay);
   if ADay < 10
      DayStr = strcat('0',DayStr);
   end    
   DateStr(i,:) = strcat(DayStr,'-',MonthStr);
end     

x(1:NumberOfFiles) = DataSet(1:NumberOfFiles); 
%figure
%hold on
plot(secs(1:NumberOfFiles),x(1:NumberOfFiles)) 
ax1 = gca;
set(ax1,'Fontsize',20)
set(ax1,'XLim',[secs(1) secs(NumberOfFiles)])
%ylabel(ax1,DataSetStrArray(i,:))
XAxisStep = 5; %Days - this is optional
set(ax1,'XTick', secs(1):DeltaT*3600*XAxisStep:secs(NumberOfFiles))
set(ax1,'XTickLabel',DateStr(1:5:NumberOfFiles,:))
FigureName = strcat(GraphFilePrefix,'_','fig');
saveas(gca,FigureName,'fig')



end
