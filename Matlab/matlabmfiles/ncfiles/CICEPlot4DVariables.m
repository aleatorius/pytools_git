function [DataSet] = CICEPlot4DVariables(FilePrefix,DataSetStrArray,NumberOfFiles,Year,Month,Day,Hour,GraphFilePrefix,DeltaT,NumberOfIceTypes,NumberOfIceLayers)
%  This function is to be used only with ice variables vertically resolved
%  such as Sinz. These variables have dimensions:
%  grid_linesXgrid_columnsXgrid_layers
%  This function reads time series of CICE output from a specific date
%  and for a specific amount of time, determined by the number of files
%  that are to be read, plots the time series in separate m,aps and graphs
%  and saves the plots in the same directory where files are located
%  FilePrefix - Firt characters of CICE history file name, tipically,
%  'iceh.'
%  DataSetStrArray - This is a character array with n lines, corresponding
%  to the number of variables that are to be read and with t columns,
%  corresponding to the size of the largest variable name
%  NumberOfFiles - Number of history output files to be read. This can be
%  daily files or files or any other type of frequency files
%  Year, Month, Day - correspond to the date that defines part of the name
%  of the first history file that is to be read.
%  GraphOutputPrefix - First part of the name for the graph files. For
%  example 'Sim1', or anything else allowing to distnguish these files 
%  from any others. The second part of the name is determined by the
%  variable name.
%  DeltaT - Number of hours between two files
%  Note - For the time being this function is prepared to read daily files
%  only!!!!!!!!!!!!!!!!!
[a,b]= size(DataSetStrArray);

for i = 1:NumberOfFiles
   if i == 1
      secs(i) = date2unixsecs(Year,Month,Day);
   else
      secs(i) = secs(i-1) + DeltaT*3600;
   end   
   [AYear, AMonth, ADay] = unixsecs2date(secs(i));
   YearStr = int2str(AYear);
   MonthStr = int2str(AMonth);
   if AMonth < 10
      MonthStr = strcat('0',MonthStr);
   end
   DayStr = int2str(ADay);
   if ADay < 10
      DayStr = strcat('0',DayStr);
   end    
   DateStr(i,:) = strcat(DayStr,'-',MonthStr);
end     
XAxisStep = 5; %Days - this is optional
for   i = 1:a
     [DataSet] = ExtractDataSetDaily(FilePrefix,DataSetStrArray(i,:,:,:),NumberOfFiles,Year,Month,Day);
     for ii = 1:NumberOfIceTypes
         %Data reading for a contour plot*********************************************************************        
         y(NumberOfIceLayers:-1:1,1:NumberOfFiles) = DataSet(3,3,1:NumberOfIceLayers,ii,1:NumberOfFiles);
         
         if (max(max(y(:,:))) ~= 0 | min(min(y(:,:))) ~= 0)
            FigureName = strcat(GraphFilePrefix,'_','ice_type',int2str(ii),DataSetStrArray(i,:),'fig'); 
            %Contour plot construction*******************************************************************
            figure     
            pcolor(1:NumberOfFiles,1:NumberOfIceLayers,y);
            shading interp
            colorbar
            ax1 = gca
            set(ax1,'Fontsize',20)
            ylabel(ax1,'Ice layers')
            title(ax1,strcat('Ice type','-',int2str(ii)))
            set(ax1,'XLim',[1 NumberOfFiles])
            set(ax1,'XTick', 1:XAxisStep:NumberOfFiles)
            set(ax1,'XTickLabel',DateStr(1:XAxisStep:NumberOfFiles,:))
            saveas(gca,strcat(FigureName,'_contour'),'fig')  
            %*****************************************************************************************************
            figure
            %Color initialization for time series plots
            red = 0;
            green = 0;
            blue = 1;
            %******************************************************************************************************
            [a,b] = size(int2str(NumberOfIceLayers));
            for iii = 1:NumberOfIceLayers 
               x(1:NumberOfFiles) = DataSet(3,3,iii,ii,1:NumberOfFiles);     
               plot(secs(1:NumberOfFiles),x(1:NumberOfFiles),'color',[red green blue]) 
               if (iii == 1)
                  ax1 = gca;
                  set(ax1,'Fontsize',20)
                  set(ax1,'XLim',[secs(1) secs(NumberOfFiles)])
                  ylabel(ax1,DataSetStrArray(i,:))
                  
                  set(ax1,'XTick', secs(1):DeltaT*3600*XAxisStep:secs(NumberOfFiles))
                  set(ax1,'XTickLabel',DateStr(1:XAxisStep:NumberOfFiles,:))
                  set(ax1,'YDir','reverse')
                  title(ax1,strcat('Ice type','-',int2str(ii)))
               end
               [d,e] = size(int2str(iii));
               
               if e < b
                  for i = 1:b-1
                    AStr = strcat(int2str(0));
                  end  
               else
                   AStr = ' ';
               end 
               ALegend(iii,:) = strcat(AStr,int2str(iii)); 
               hold on
               %Color coding - trying to go from blue to red................
               if (iii <= 1/3 * NumberOfIceLayers)
                  blue = max(1-iii/round(NumberOfIceLayers/3),0);
                  green = max(1-blue,0);
                  red = 0;
               end    
               if iii > 1/3 * NumberOfIceLayers & iii <= 2/3 * NumberOfIceLayers
                  blue = 0;
                  green = max(1-(iii-round(1/3*NumberOfIceLayers))/round(NumberOfIceLayers/3),0);
                  red = max(1-green,0);
               end
               if iii > 2/3 * NumberOfIceLayers
                  blue = 0;
                  green = 0;
                  red = (iii-round(2/3*NumberOfIceLayers))/round(NumberOfIceLayers/3);
               end
               %*****************************************************************************************
            end
            legend(ALegend)
            saveas(gca,FigureName,'fig')
         end
     end         
end


end
