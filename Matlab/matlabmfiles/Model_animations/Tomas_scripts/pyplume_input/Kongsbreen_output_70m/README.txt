
Here�s the results - sorry it�s got a little messy, but hopefully you can find a way to read these without too much difficulty! If there�s something weird in there or a slightly different format would be easier, let me know.

The files are:

kongsbreen_results_disch_ent_nb.csv � all of the initial conditions and results where a single value can be extracted. 
              Vol_flux (m3/s) is the volume flux calculated at 1m below the surface.
              Ent_flux (m3/s) is the difference between the discharge and the vol_flux ie. the amount of water entrained by the plume.
              Nb_depth (m) is the neutral buoyancy depth the plume reaches.

The remaining files are named: kongsbreen_results_[??].csv with each variable in a separate file, named as below. The rows are each timestep, which should match the timestamps in the disch file.
              
                        'z'     height above source (m)
                        'b_p'   plume radius (m)
                        'u_p'   vertical velocity (m/s)
                        't_p'   temperature (deg C)
                        's_p'   salinity (kg/m3)
                        'm_p'   melt rate (m/s)
                        't_a'   ambient salinity (kg/m3)
                        's_a'   ambient temperature (deg C)

I can also interpolate the results to a different vertical resolution if that help, 70 metres at 0.1 metres resolution produces quite a lot of data!!

If that looks good I�ll do the same for Kronebreen.
