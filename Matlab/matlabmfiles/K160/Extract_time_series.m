case_label = 'future';
switch case_label
    case 'subglacial'
        % Data file directory
        dir_data = '/tos-project3/NS9081K/NORSTORE_OSL_DISK/NS9081K/K160_bgc/Sim14/'
    case 'future'
        % Data file directory
        dir_data = '/tos-project3/NS9081K/NORSTORE_OSL_DISK/NS9081K/K160_bgc/Sim18/'
    case 'surface'
        % Data file directory
        dir_data = '/tos-project3/NS9081K/NORSTORE_OSL_DISK/NS9081K/K160_bgc/Sim16/'
    otherwise
        error('Not a valid case.')
end
prename = 'ocean_avg_';
file = struct('name',{});
file_len = 120;
index = 0;
for i=1:file_len
    index = index + 1; 
    if index < 10
      rfil = strcat(prename,'000',int2str(index),'.nc');
    end   
    if index >= 10 & i < 100
      rfil = strcat(prename,'00',int2str(index),'.nc');   
    end
    if index >=100
      rfil = strcat(prename,'0',int2str(index),'.nc');
    end  
    file(i).name = rfil; 
    disp(file(i).name);
    fname = [dir_data file(i).name]
    curr_time(i) = ncread(fname,'ocean_time');
    h0 = ncread(fname,'h');
    NO3 = ncread(fname,'NO3');
    phytoplankton = ncread(fname,'phytoplankton');
    X1(i,1) = curr_time(i);
    X1(i,2) = mean(h0(99:103,91));
    X1(i,3) = mean(NO3(99:103,91,1));
    X1(i,4) = mean(phytoplankton(99:103,91,35));
end
   
