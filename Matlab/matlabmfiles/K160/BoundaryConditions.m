function [NO3_west,NO3_east,NO3_north,NO3_south,NH4_west,NH4_east,NH4_north,NH4_south,PO4_west,PO4_east,PO4_north,PO4_south,Si_west,Si_east,Si_north,Si_south,chlo_west,chlo_east,chlo_north,chlo_south,phyt_west,phyt_east,phyt_north,phyt_south,phyn_west,phyn_east,phyn_north,phyn_south,phyp_west,phyp_east,phyp_north,phyp_south] = BoundaryConditions(z,mask_rho)

% East & West
for t  = 1:1526
   i = 1;   
   for j = 1:602  
     k = 35;
     depth = 0.0; 
     while depth < 50 & k > 1
       k = k-1;
       depth = abs(z(i,j,k));
     end
     if depth > 50 
        k = k + 1;
     end
     for l = 35:-1:k
        NO3_west(j,l,t) = 0.1312 * abs(z(i,j,l)) * mask_rho(i,j);
        NH4_west(j,l,t) = 0.0253 * abs(z(i,j,l)) * mask_rho(i,j);
        PO4_west(j,l,t) = 0.0126 * abs(z(i,j,l)) * mask_rho(i,j);
        Si_west(j,l,t)  = 0.0696 * abs(z(i,j,l)) * mask_rho(i,j);
        chlo_west(j,l,t) = (-0.0027*z(i,j,l)^2 + 0.1386 * abs(z(i,j,l)))* mask_rho(i,j);
        phyt_west(j,l,t) = chlo_west(j,l,t) / 0.0535; %0.0535 Maximum Chl to Carbon atio in Fennel's model
        phyn_west(j,l,t) = phyt_west(j,l,t) * (14*16) / (12*106);
        phyp_west(j,l,t) = phyt_west(j,l,t) * (31*1) / (12*106);
     end
     k = k - 1;
     if k > 1
     for l = k:-1:1
        NO3_west(j,l,t) = 9.86 * mask_rho(i,j);
        NH4_west(j,l,t) = 0.93 * mask_rho(i,j);
        PO4_west(j,l,t) = 0.79 * mask_rho(i,j);
        Si_west(j,l,t)  = 3.97 * mask_rho(i,j);
        chlo_west(j,l,t) = 0.06 * mask_rho(i,j) * mask_rho(i,j);
        phyt_west(j,l,t) = chlo_west(j,l,t) / 0.0535;%0.0535 Maximum Chl to Carbon atio in Fennel's model
        phyn_west(j,l,t) = phyt_west(j,l,t) * (14*16) / (12*106);
        phyp_west(j,l,t) = phyt_west(j,l,t) * (31*1) / (12*106);
     end
     end
   end %j
   i = 402;
   for j = 1:602  
     k = 35;
     depth = 0.0; 
     while depth < 50 & k > 1
       k = k-1;
       depth = abs(z(i,j,k));
     end
     if depth > 50 
        k = k + 1;
     end
     for l = 35:-1:k
        NO3_east(j,l,t) = 0.1312 * abs(z(i,j,l)) * mask_rho(i,j);
        NH4_east(j,l,t) = 0.0253 * abs(z(i,j,l)) * mask_rho(i,j);
        PO4_east(j,l,t) = 0.0126 * abs(z(i,j,l)) * mask_rho(i,j);
        Si_east(j,l,t)  = 0.0696 * abs(z(i,j,l)) * mask_rho(i,j);
        chlo_east(j,l,t) = (-0.0027*z(i,j,l)^2 + 0.1386 * abs(z(i,j,l)))* mask_rho(i,j);
        phyt_east(j,l,t) = chlo_east(j,l,t) / 0.0535; 
        phyn_east(j,l,t) = phyt_east(j,l,t) * (14*16) / (12*106);
        phyp_east(j,l,t) = phyt_east(j,l,t) * (31*1) / (12*106);
     end
     k = k - 1;
     if k > 1
     for l = k:-1:1
        NO3_east(j,l,t) = 9.86 * mask_rho(i,j);
        NH4_east(j,l,t) = 0.93 * mask_rho(i,j);
        PO4_east(j,l,t) = 0.79 * mask_rho(i,j);
        Si_east(j,l,t)  = 3.97 * mask_rho(i,j);
        chlo_east(j,l,t) = 0.06 * mask_rho(i,j) * mask_rho(i,j);
        phyt_east(j,l,t) = chlo_east(j,l,t) / 0.0535;
        phyn_east(j,l,t) = phyt_east(j,l,t) * (14*16) / (12*106);
        phyp_east(j,l,t) = phyt_east(j,l,t) * (31*1) / (12*106);
     end
     end
   end %j
   
   j = 1;   
   for i = 1:402  
     k = 35;
     depth = 0.0; 
     while depth < 50 & k > 1
       k = k-1;
       depth = abs(z(i,j,k));
     end
     if depth > 50 
        k = k + 1;
     end
     for l = 35:-1:k
        NO3_south(i,l,t) = 0.1312 * abs(z(i,j,l)) * mask_rho(i,j);
        NH4_south(i,l,t) = 0.0253 * abs(z(i,j,l)) * mask_rho(i,j);
        PO4_south(i,l,t) = 0.0126 * abs(z(i,j,l)) * mask_rho(i,j);
        Si_south(i,l,t)  = 0.0696 * abs(z(i,j,l)) * mask_rho(i,j);
        chlo_south(i,l,t) = (-0.0027*z(i,j,l)^2 + 0.1386 * abs(z(i,j,l)))* mask_rho(i,j);
        phyt_south(i,l,t) = chlo_south(i,l,t) / 0.0535; 
        phyn_south(i,l,t) = phyt_south(i,l,t) * (14*16) / (12*106);
        phyp_south(i,l,t) = phyt_south(i,l,t) * (31*1) / (12*106);
     end
     k = k - 1;
     if k > 1
     for l = k:-1:1
        NO3_south(i,l,t) = 9.86 * mask_rho(i,j);
        NH4_south(i,l,t) = 0.93 * mask_rho(i,j);
        PO4_south(i,l,t) = 0.79 * mask_rho(i,j);
        Si_south(i,l,t)  = 3.97 * mask_rho(i,j);
        chlo_south(i,l,t) = 0.06 * mask_rho(i,j) * mask_rho(i,j);
        phyt_south(i,l,t) = chlo_south(i,l,t) / 0.0535;
        phyn_south(i,l,t) = phyt_south(i,l,t) * (14*16) / (12*106);
        phyp_south(i,l,t) = phyt_south(i,l,t) * (31*1) / (12*106);
     end
     end
   end %i
   j = 602;
   for i = 1:402  
     k = 35;
     depth = 0.0; 
     while depth < 50 & k > 1
       k = k-1;
       depth = abs(z(i,j,k));
     end
     if depth > 50 
        k = k + 1;
     end
     for l = 35:-1:k
        NO3_north(i,l,t) = 0.1312 * abs(z(i,j,l)) * mask_rho(i,j);
        NH4_north(i,l,t) = 0.0253 * abs(z(i,j,l)) * mask_rho(i,j);
        PO4_north(i,l,t) = 0.0126 * abs(z(i,j,l)) * mask_rho(i,j);
        Si_north(i,l,t)  = 0.0696 * abs(z(i,j,l)) * mask_rho(i,j);
        chlo_north(i,l,t) = (-0.0027*z(i,j,l)^2 + 0.1386 * abs(z(i,j,l)))* mask_rho(i,j);
        phyt_north(i,l,t) = chlo_north(i,l,t) / 0.0535; 
        phyn_north(i,l,t) = phyt_north(i,l,t) * (14*16) / (12*106);
        phyp_north(i,l,t) = phyt_north(i,l,t) * (31*1) / (12*106);
     end
     k = k - 1;
     if k > 1
     for l = k:-1:1
        NO3_north(i,l,t) = 9.86 * mask_rho(i,j);
        NH4_north(i,l,t) = 0.93 * mask_rho(i,j);
        PO4_north(i,l,t) = 0.79 * mask_rho(i,j);
        Si_north(i,l,t)  = 3.97 * mask_rho(i,j);
        chlo_north(i,l,t) = 0.06 * mask_rho(i,j) * mask_rho(i,j);
        phyt_north(i,l,t) = chlo_north(i,l,t) / 0.0535;
        phyn_north(i,l,t) = phyt_north(i,l,t) * (14*16) / (12*106);
        phyp_north(i,l,t) = phyt_north(i,l,t) * (31*1) / (12*106);
     end
     end
   end %i
end %t
