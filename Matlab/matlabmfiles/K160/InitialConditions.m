function [NO3,NH4,PO4,Si,chlorophyll,phytoplankton,Phy_N,Phy_P] = InitialConditions(z,mask_rho)

for i = 1:402
   for j = 1:602
     k = 35;
     depth = 0.0; 
     while depth < 50 & k > 1
       k = k-1;
       depth = abs(z(i,j,k));
     end
     if depth > 50 
        k = k + 1;
     end
     for l = 35:-1:k
        NO3(i,j,l,1:2) = 0.1312 * abs(z(i,j,l)) * mask_rho(i,j);
        NH4(i,j,l,1:2) = 0.0253 * abs(z(i,j,l)) * mask_rho(i,j);
        PO4(i,j,l,1:2) = 0.0126 * abs(z(i,j,l)) * mask_rho(i,j);
        Si(i,j,l,1:2)  = 0.0696 * abs(z(i,j,l)) * mask_rho(i,j);
        chlorophyll(i,j,l,1:2) = (-0.0027*z(i,j,l)^2 + 0.1386 * abs(z(i,j,l)))* mask_rho(i,j);
        phytoplankton(i,j,l,1:2) = chlorophyll(i,j,l,1) / 0.0535; %0.0535 Maximum Chl to Carbon atio in Fennel's model
        Phy_N(i,j,l,1:2) = phytoplankton(i,j,l,1) * (14*16) / (12*106);
        Phy_P(i,j,l,1:2) = phytoplankton(i,j,l) * (31*1) / (12*106);
     end
     k = k - 1;
     if k > 1
     for l = k:-1:1
        NO3(i,j,l,1:2) = 9.86 * mask_rho(i,j);
        NH4(i,j,l,1:2) = 0.93 * mask_rho(i,j);
        PO4(i,j,l,1:2) = 0.79 * mask_rho(i,j);
        Si(i,j,l,1:2)  = 3.97 * mask_rho(i,j);
        chlorophyll(i,j,l,1:2) = 0.06 * mask_rho(i,j);
        phytoplankton(i,j,l,1:2) = chlorophyll(i,j,l,1) / 0.0535; %0.0535 Maximum Chl to Carbon atio in Fennel's model
        Phy_N(i,j,l,1:2) = phytoplankton(i,j,l,1) * (14*16) / (12*106);
        Phy_P(i,j,l,1:2) = phytoplankton(i,j,l,1) * (31*1) / (12*106);
     end
     end
   end
end
