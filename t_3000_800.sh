#!/bin/bash
#module load intel/13.0 
#module load netCDF/4.2.1.1-intel-13.0  
#module load Boost/1.52.0-intel-13.0-OpenMPI-1.6.2
module purge
module load Boost/1.63.0-intel-2017a-Python-2.7.13
module load netCDF/4.4.1.1-intel-2017a
#cd /global/work/mitya/metno/ERA-Interim_convert

if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi


homedir=/home/mitya
pathTools=/home/mitya/bin
path2Fimex=${homedir}/bin/fimex
outdir=/home/mitya/

xvals="0,3000,...,1884000"
yvals="0,3000,...,1794000"

filein=$1
fileinstrip=$(basename -- "$filein")
echo "$fileinstrip"
fileout=${fileinstrip%.*}_iso.nc

   ${path2Fimex} --input.file=$filein  --interpolate.projString="+proj=stere +lon_0=12 +lat_0=79.5 +k=0.933013 +R=6370000 +no_defs  +x_0=681347 +y_0=859641" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --inter\
polate.yAxisV=${yvals} --output.file=${outdir}/$fileout --interpolate.method=coord_nearestneighbor  --output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml








xvals="0,3000,...,1440800"
yvals="0,3000,...,1080800"


filein=$fileout
fileinstrip=$(basename -- "$filein")
echo "$fileinstrip"
fileout=${fileinstrip%.*}_3000.nc

   ${path2Fimex} --input.file=$filein --input.config=wrf_fimex.ncml  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals} --inter\
polate.yAxisV=${yvals} --output.file=${outdir}/$fileout --interpolate.method=coord_kdtree  --output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml

xvals="0,800,...,1440800"
yvals="0,800,...,1080800"

infile=$fileout
outfile=${filein%.*}_800_out.nc
    ${path2Fimex} --input.file=${outdir}/$infile  --interpolate.projString="+proj=stere +ellps=WGS84 +lat_0=90.0 +lat_ts=60.0 +x_0=1000000 +y_0=1600000 +lon_0=40" --interpolate.xAxisUnit=m --interpolate.yAxisUnit=m --interpolate.xAxisV=${xvals\
} --interpolate.yAxisV=${yvals} --output.file=${outdir}/$outfile --interpolate.method=bilinear  --output.config=/global/work/mitya/metno/ERA-Interim_convert/nc3_64bit.xml