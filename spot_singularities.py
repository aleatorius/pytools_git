#!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *
from netCDF4 import *
import sys, re, glob
from os import system
import argparse
import datetime
from datetime import date
import matplotlib.pyplot as plt
from pylab import *
from matplotlib import colors, cm
import numpy.ma as ma
import matplotlib.dates as mdates
parser = argparse.ArgumentParser(description='transect write 0.1')




parser.add_argument(
'-i', 
help='input file', 
dest='inf', 
action="store"
)
parser.add_argument(
'-v', 
help='variable', 
dest='var', 
action="store"
)
parser.add_argument(
'-grid', 
help='input grid file', 
dest='grid', 
action="store"
)
parser.add_argument(
'-clicks', 
help='input grid file', 
dest='clicks', 
action="store",
choices=('yes', 'no'), 
default="no"
)
args = parser.parse_args()

def unpack(ina):
    if ina.ndim == 0:
        print "is it scalar or 0d array?"
        outa = ina[()]
    else:
        outa = zeros(ina.shape)
        outa[:] = ina[:]
    return outa


files = [args.inf]
mask = []
mask_u = []
mask_v = []
mask_rho = []
var = []
bath = []
river_e = []
river_x = []
river_transport = []
river_num = []
river_direction = []
river_time = []
lat, lon = [],[]
vshape = []
river_salt = []

for i in files:
     print i
     f = Dataset(i)
     mask.append(unpack(f.variables['mask_rho']))
     bath = unpack(f.variables['h'])
     lat.append(unpack(f.variables['lat_rho']))
     lon.append(unpack(f.variables['lon_rho']))
     try:
          mask_u.append(unpack(f.variables['mask_u']))
     except:
          print "no mask u"
     try:
          mask_v.append(unpack(f.variables['mask_v']))
     except:
          print "no mask v"
     try:
          mask_rho.append(unpack(f.variables['mask_rho']))
     except:
          print "no mask rho"

     try:
          var = unpack(f.variables[args.var])
          print var.shape, "var shape, here"
          print var[0,:,2,100,200], "something"
     except:
          print "no var ", args.var
     
print mask_rho[0].shape, "mask_rhoshape"
#for i in range(0,35):
#     print i
#     maskedvar=ma.masked_where(mask_rho[0]==0, var[-1,0,i,:])
#     print np.amax(maskedvar), np.amin(maskedvar), "part",i
#3maskedvar=ma.masked_where(mask_rho[0]==0, var[0,0,-1,:])
#print np.amax(maskedvar), np.amin(maskedvar), "part 2"

e_pos=[]
x_pos=[]

for i,j in np.ndenumerate(ma.masked_where(mask_rho[0]==0, var[-1,-1,0,:])):
     if abs(j)>40. and mask_rho[0][i]==1:
          print i,j, bath[i]
          x_pos.append(i[0])
          e_pos.append(i[1])
print len(x_pos)

ep_pos=[]
xp_pos=[]
for i,j in np.ndenumerate(ma.masked_where(mask_rho[0]==0, bath)):
     if abs(j)==5. and mask_rho[0][i]==1:
#          print i,j, bath[i]
          xp_pos.append(i[0])
          ep_pos.append(i[1])
   
#print len(x_pos)
#salt=unpack(f.variables["salt"])[-1,0,-1,:]
salt=unpack(f.variables["salt"])[-1,0,-1,:]


fig = plt.figure(figsize=(12,8))
ax = fig.add_subplot(111)
palette = plt.cm.spectral
#p=ax.pcolormesh(ma.masked_outside(salt,-1e+36,1e+36), cmap=palette
#p=ax.pcolormesh(ma.masked_outside(bath,-1e+36,1e+36), cmap=palette)
p=ax.pcolormesh(ma.masked_where(mask_rho[0]==0,bath), cmap=palette)
plt.colorbar(p)
p.set_clim(vmin=0,vmax=30)
    #delta = 1
    #plt.Rectangle((river_x[hplot-1], river_e[hplot-1]),width=delta,height=delta,color='red') 
xp_pos[:]= [x - 1 for x in xp_pos]
    
ax.plot(e_pos,x_pos,'bs', markersize=1)
ax.plot(ep_pos,xp_pos,'g^', markersize=2)
plt.show()
exit()
g = Dataset("/global/work/thinf/metroms_input/s800_cice_processed/svalbard_800m_river.nc")
epos=unpack(g.variables["river_Eposition"])
xpos=unpack(g.variables["river_Xposition"])
rivpos=zip(epos,xpos)
for i in rivpos:
    print i, bath[i],salt[i], mask_rho[0][i]
exit()
#info = zip(river_num[0], river_e[0], river_x[0], river_direction[0],np.sum(river_transport[0], axis=0))



#sealand = (1.0,0.0)

fig = plt.figure(figsize=(12,8))



counter= 0
for hplot in [1]:
    rho_x, rho_e = [],[]
    ax = fig.add_subplot(111)
    maxval = np.amax(bath[hplot-1])
    minval = np.amin(bath[hplot-1])
    limit = max(abs(minval), abs(maxval))

#    for i,j in enumerate(river_x[hplot-1]):
#        if river_transport[hplot-1][args.num-1,i]>0:
#            bath[hplot-1][int(river_e[hplot-1][i]),int(river_x[hplot-1][i])] = limit+1000.
#            rho_x.append(river_x[hplot-1][i])
#            rho_e.append(river_e[hplot-1][i])
#        else:
#            if river_direction[hplot-1][i]==0:
#                bath[hplot-1][int(river_e[hplot-1][i]),int(river_x[hplot-1][i])-1] = limit+1000.
#                rho_x.append(river_x[hplot-1][i]-1)
#                rho_e.append(river_e[hplot-1][i])
#            else:
#                bath[hplot-1][int(river_e[hplot-1][i])-1,int(river_x[hplot-1][i])] = limit+1000.
#                rho_x.append(river_x[hplot-1][i])
#                rho_e.append(river_e[hplot-1][i]-1)
#    counter = 0
    palette = plt.cm.spectral
    palette.set_over('c', limit+100.)        
    p=ax.pcolormesh(ma.masked_where(mask[hplot-1]==0, bath[hplot-1]), cmap=palette)
    plt.colorbar(p)
    p.set_clim(vmin=minval,vmax=maxval)
    ax.plot(e_pos,x_pos,'ro', markersize=5)






if args.clicks=='yes':
    fig.canvas.mpl_connect('button_press_event', onclick)
else:
    pass


plt.show()




