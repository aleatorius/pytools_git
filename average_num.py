#!/global/apps/python/2.7.3/bin/python
##!/home/mitya/testenv/bin/python -B
import numpy as np
from numpy import *

import sys, re, glob
from os import system
import numpy.ma as ma
import datetime
from datetime import date
import argparse
parser = argparse.ArgumentParser(description='pyview 0.1')
parser.add_argument('-i', help='input file', dest='input', action="store")
#parser.add_argument('-o', help='output file', dest='output', action="store")
args = parser.parse_args()
input=args.input

sum = 0.
counter=0
inp_file = open(input, "r")
data = []
for line in inp_file.readlines():
	st = line.split()         
	sum = sum + float(st[1])
	data.append(st[1])
	counter = counter + 1

inp_file.close()
print "number of calls", counter
average = float(sum)/counter
print "average", average
sum2 = 0
for i in data:
#	print i, ((float(i) - average)**2)
	sum2 = sum2 + ((float(i) - average)**2)/counter
	
print "quadratic deviation from average", sum2
