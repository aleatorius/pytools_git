#!/bin/bash

#years="1993 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010" 
years="2014 2015" 
# 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009 2010'

months="01 02 03 04 05 06 07 08 09 10 11 12"
days="01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31" 

function validate_url(){
  if [[ `wget -S --spider $1  2>&1 | grep 'Remote file exists'` ]]; then
      return 0
  else
      return 1
  fi
}

for yy in $years; do
    dir=/cluster/shared/arcticfjord/cmems_$yy
    if [ ! -d "$dir" ]; then
	mkdir $dir
    fi
    echo $dir
    cd $dir
    for mm in $months; do
	for dd in $days; do
	    filein='http://thredds.met.no/thredds/fileServer/myocean/siw-tac/siw-metno-svalbard/'${yy}'/'${mm}'/ice_conc_svalbard_'$yy$mm$dd'1500.nc'
	    if `validate_url $filein`; then 
		wget $filein
	    else 
		echo "does not exist", $mm$dd, $filein > nofiles
	    fi
	done
    done
done
