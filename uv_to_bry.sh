#!/usr/bin/sh

for i in dump20*; do ncks -v uice,vice $i ${i}_uv; ncrename -O -v uice,uvel -v vice,vvel -d xi_rho,xi_t -d eta_rho,eta_t -d time,Time ${i}_uv  ; done
ncrcat dump20*nc_uv uv.nc
python ~/pytools_git/bry_from_topaz.py -i uv.nc
ncpdq --unpack uv.nc_bry BRY_uv.nc 
